<?php
$config['SYSTEM_SETTINGS'] = array(
    'SiteName' => __('Reports onlineplus'),
    'SiteSlogan' => __('Reports onlineplus'),
    'SiteCopyright' => __("&copy; Copyright For reportsonlineplus 2016"),
    'SupportEmail' => 'admin@reportsonlineplus.com',
    'SupportDisplayName' => __('Reports onlineplus Support Team'),
    'SupportContact' => __(''),
    'SupportContactAddress' => __('Reports onlineplus'),
    'HowtoVideo' => '',
    'MessageSubjects' => array(
        '0' => __('Copyright'),
        '1' => __('Downloads'),
        '2' => __('General Inquiry'),
        '3' => __('Payments'),
        '4' => __('PayPal'),
        '5' => __('Song Request'),
        '6' => __('Technical Support'),
        '7' => __('Other')
    ),
    'EMAIL_FROM' => 'admin@reportsonlineplus.com',
    'ADMIN_EMAIL' => 'michaelkrusz@gmail.com'
);

$config['LIST_NUM_RECORDS'] = array(
    'Superadmin' => 20,
    'Admin' => 20,
    'User' => 20,
    'AutoComplete' => 10,
);

$config['PROJECT_BASE_PATH'] = array('URL' => dirname(dirname(dirname($_SERVER['PHP_SELF']))));
$config['SERVER_HTTP_PROTOCOL'] = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)) ? "https://" : "http://");
$config['SERVER_DOMAIN_NAME'] = ((isset($_SERVER['SERVER_NAME']) && !empty($_SERVER['SERVER_NAME'])) ? $_SERVER['SERVER_NAME'] : "veridate.net");
$config['FULL_BASE_URL'] = array('URL' => $config['SERVER_HTTP_PROTOCOL'] . filter_var((isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $config['SERVER_DOMAIN_NAME']), FILTER_DEFAULT) . str_replace('\\', '/', dirname(dirname(dirname($_SERVER['PHP_SELF'])))));

$config['LOGIN_URL'] = array('URL' => $config['FULL_BASE_URL']['URL'] . '/login');
$config['SIGNUP_URL'] = array('URL' => $config['FULL_BASE_URL']['URL'] . '/signup');
$config['ADMIN_LOGIN_URL'] = array('URL' => $config['FULL_BASE_URL']['URL'] . '/admin/login');

$config['DATE_FORMAT'] = array('Date' => '%d/%m/%Y');
$config['DATETIME_FORMAT'] = array('DateTime' => '%d/%m/%Y %H:%M %p');
$config['DB_DATE_TIME_FORMAT'] = array('DateTime' => '%Y-%m-%d %H:%i:%s');
$config['DB_DATETIME_FORMAT'] = array('DateTime' => 'Y-m-d H:i:s');
$config['DISPLAY_DATETIME'] = array('DateTime' => 'd-m-Y H:i:s');
$config['DISPLAY_DATE'] = array('Date' => 'd-m-Y');
$config['INVITATION_DISPLAY_DATE'] = array('Date' => 'd/M/Y');

$config['TimeZones'] = array(
    'GMT -12:00' => '(GMT -12:00) Eniwetok, Kwajalein',
    'GMT -11:00' => '(GMT -11:00) Midway Island, Samoa',
    'GMT -10:00' => '(GMT -10:00) Hawaii',
    'GMT -9:00' => '(GMT -9:00) Alaska',
    'GMT -8:00' => '(GMT -8:00) Pacific Time (US & Canada)',
    'GMT -7:00' => '(GMT -7:00) Mountain Time (US & Canada)',
    'GMT -6:00' => '(GMT -6:00) Central Time (US & Canada), Mexico City',
    'GMT -5:00' => '(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima',
    'GMT -4:00' => '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz',
    'GMT -3:30' => '(GMT -3:30) Newfoundland',
    'GMT -3:00' => '(GMT -3:00) Brazil, Buenos Aires, Georgetown',
    'GMT -2:00' => '(GMT -2:00) Mid-Atlantic',
    'GMT -1:00' => '(GMT -1:00 hour) Azores, Cape Verde Islands',
    'GMT +0:00' => '(GMT) Western Europe Time, London, Lisbon, Casablanca',
    'GMT +1:00' => '(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris',
    'GMT +2:00' => '(GMT +2:00) Kaliningrad, South Africa',
    'GMT +3:00' => '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg',
    'GMT +3:30' => '(GMT +3:30) Tehran',
    'GMT +4:00' => '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi',
    'GMT +4:30' => '(GMT +4:30) Kabul',
    'GMT +5:00' => '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent',
    'GMT +5:30' => '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi',
    'GMT +5:45' => '(GMT +5:45) Kathmandu',
    'GMT +6:00' => '(GMT +6:00) Almaty, Dhaka, Colombo',
    'GMT +7:00' => '(GMT +7:00) Bangkok, Hanoi, Jakarta',
    'GMT +8:00' => '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong',
    'GMT +9:00' => '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk',
    'GMT +9:30' => '(GMT +9:30) Adelaide, Darwin',
    'GMT +10:00' => '(GMT +10:00) Eastern Australia, Guam, Vladivostok',
    'GMT +11:00' => '(GMT +11:00) Magadan, Solomon Islands, New Caledonia',
    'GMT +12:00' => '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka'
);
