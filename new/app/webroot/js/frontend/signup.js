(function () {
   
  // validate signup form on keyup and submit
  var validateSignUp = function(element) {
    
    
      $.validator.addMethod("emailRegex", function (value, element) {
          return this.optional(element) || /^((?=.*\d)|(?=.*["!#$%&'()*+,\-:;.<=>?@{|}~^_`\[\]\\]))(?=.*[a-zA-Z]).+$/i.test(value);
      }, "Use at least 1 letter & number or symbol.");    
    
      $(element).validate({
        rules: {
            'data[User][fname]': "required",
            'data[User][lname]': "required",
            'data[User][email]': {
              required: true,
              email: true
            },
            'data[User][password]': {
              required: true,
              minlength: 9
            },
     
            'data[User][confirm_password]': {
              required: true,
              equalTo: "#UserPassword"
            },
     
            'data[User][phone]': {
              required: true,
              number: true
            },
     
            'data[User][address]':"required",
            'data[User][country_id]':"required",
            'data[User][state_id]' :"required",
            'data[User][city]':"required",
            'data[User][zipcode]':"required",
            'data[User][question_id]':"required",
            'data[User][answer]':"required",
            'data[User][agree]': "required"
        },
        /*messages: {
            'data[User][fname]': "Please enter your firstname",
            'data[User][lname]': "Please enter your lastname",
            'data[User][email]': "Please enter a valid email address",
            'data[User][password]': {
              required: "Please provide a password",
              minlength: "Your password must be at least 9 characters long"
            },
            'data[User][confirm_password]': {
              required: "Please provide a password",
              equalTo: "Please enter the same password as above"
            },
     
            'data[User][phone]': "Please enter valid phone number",
            'data[User][address]': "Please enter address",
            'data[User][country_id]': "Please select country",
            'data[User][state_id]': "Please select state",
            'data[User][city]': "Please enter city",
            'data[User][zipcode]': "Please enter zipcode",
            'data[User][question_id]': "Please enter Security Question",
            'data[User][answer]': "Please enter your Answer",
            'data[User][agree]':"Please accept the term and conditions"   
        }*/
      });
    };

    var getStates = function(element) {
        var country_id = element.val();
        $.ajax({
            type: 'json',
            data: {countryID: country_id},
            url: SITE_URL + '/homes/getStates.json',
            success: function() {
              var popupTemplate = $("#editProfileData").html();
              resultData = _.template(popupTemplate, {data: data});
                    $('#profileDataContainer').html(resultData);
              },
            error: function() {

            }
        });
    };

    $(document).ready(function() {
        validateSignUp("#signupform");

        /*$(document).on('change','#country-list',function(){
          getStates($(this));
        });*/
    });

})();

function fetchStates(countryId) {
  if (countryId!="") {
    $.ajax({
        type: 'GET',
        url: SITE_URL + 'homes/fetchStates/' + countryId,
        success: function(response) {
          if (response!="") {
            $("#state-list").html(response);
          }
        }
    });
  }
}