<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class HomesController extends AppController {

	public $helper = array('Form');

/**
 * This controller uses the home layout file
 * @var string
 */
	public $layout = 'home';

/**
 * Displays a view
 * @return void
 */
	public function index() {
		
	}

/**
 * Method signUp to show the sign up page
 * @return void
 */
	public function signUp() {
		$this->set('title_for_layout', 'SP Registration');
		$this->loadModel('Country');
		$this->loadModel('Squestion');
		$countriesList = $this->Country->getAllCountries();
		$sQuestions = $this->Squestion->find('list');
		$this->set(compact('countriesList', 'sQuestions'));
		if ($this->request->is('post')
			|| $this->request->is('put')
			&& !empty($this->request->data)) {
			$this->loadModel('User');			
			$activationKey = md5(rand());
			$this->request->data['User']["user_type_id"] = 1;
			$this->request->data['User']["password"] = md5($this->request->data['User']["password"]);
			$this->request->data['User']["ip_address"] = $_SERVER["REMOTE_ADDR"];
			$this->request->data['User']["deactivate"] = 0;
	    $this->request->data['User']['activationkey'] = $activationKey;
			$this->User->set($this->request->data['User']);
			if($this->User->save($this->request->data,false)) {
				// Send email to user
				$lastInsertedId = $this->User->getLastInsertId();
				$this->loadModel('EmailTemplate');
				$registerTemplate = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>1)));
				$link = "<a href=" . Router::url('/', true) . "/homes/activate_account/". $lastInsertedId ."/".$activationKey.">Click here</a>";
				$registerTemplate['EmailTemplate']['mail_body'] = str_replace(array('#FIRSTNAME','#LINK'),array($this->request->data['User']['fname'],$link),$registerTemplate['EmailTemplate']['mail_body']);
	
				App::uses('CakeEmail', 'Network/Email');
				$Email = new CakeEmail();
				$Email->emailFormat('both');
				$Email->from([Configure::read('SYSTEM_SETTINGS.EMAIL_FROM') => Configure::read('SYSTEM_SETTINGS.SiteName')]);
				$Email->sender(Configure::read('SYSTEM_SETTINGS.EMAIL_FROM'), Configure::read('SYSTEM_SETTINGS.SiteName'));
				$Email->to(trim($this->request->data['User']['email']));
				//$Email->bcc(array($adminData['User']['email'] => $adminData['User']['first_name'].' '.$adminData['User']['last_name']));
				$Email->subject($registerTemplate['EmailTemplate']['mail_subject']);
				try {
						$Email->send($registerTemplate['EmailTemplate']['mail_body']);
				} catch(Exception $err){
				}
				$this->Session->setFlash(__('Thanks for registration with us, please check your mailbox to activate your account.'), 'default', 'success');
				$this->redirect(array('controller' => 'homes', 'action' => 'index', 'admin' => false));
			} else {
				$this->Session->setFlash(__('Error occured while registration with us. Please try again.'), 'default', 'error');
				$this->redirect(array('controller' => 'homes', 'action' => 'index', 'admin' => false));
			}
		}
	}
	
	public function fetchStates($countryId="") {
		$list = [];
		$options = "<option value=''>State</option>";
		if (!empty($countryId)) {
			$this->loadModel('State');
			$list = $this->State->fetchStates($countryId);
			if (!empty($list)) {
				foreach($list as $iter=>$collect) {
					$options = $options . "<option value='{$iter}'>{$collect}</option>";
				}
			}
		}
		echo $options;exit;
	}
	
	public function ajax_check_email($id = null) {
			$this->layout = false;
			$this->autoRender = false;
			$this->loadModel('User');
			if ($id == null) {
					$count = $this->User->find('count', array(
							'conditions' => array('User.email' => $this->params->query['data']['User']['email']),
							'recursive' => -1
					));
			} else {
					$count = $this->User->find('count', array(
							'conditions' => array(
									'User.id !=' => $id,
									'User.email' => $this->params->query['data']['User']['email']
							),
							'recursive' => -1
					));
			}
			if ($count == 0) {
					$validate = 'true';
			} else {
					$validate = 'This email is already exist';
			}
			echo json_encode($validate);exit;
	}	
	
}