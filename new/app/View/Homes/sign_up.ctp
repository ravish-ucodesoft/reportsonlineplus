<style>
   .star {color: red;}
</style>
<!-- Banner image start here -->
<div class="signup-section">
   <div class="reg-opacity">
      <div class="container">
         <div class="title-signup"><?php echo __('Fire Safety Service Providers Registration'); ?></div>
      </div>
   </div>
</div>

<!-- registraion form start here -->
<div class="container signup-process">
   <div class="col-lg-9 col-sm-12 col-md-9 col-xs-12 signup-form">
      <?php
        echo $this->Form->create(
                                  'User',
                                  array(
                                      'url' => array(
                                          'controller' => 'homes',
                                          'action' => 'signUp'
                                        ),
                                      'inputDefaults' => array(
                                          'label' => false,
                                          'div' => false
                                        ),
                                      'class' => 'form-horizontal',
                                      'id' => 'signupform'
                                    )
                                );
      ?>
         <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Name'); ?><span class="star">*</span></label>
            <div class="col-sm-5">
               <?php
                  echo $this->Form->input(
                    'fname',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('First Name')
                      )
                  );
               ?>
            </div>
            <div class="col-sm-5">
               <?php
                  echo $this->Form->input(
                    'lname',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Last Name')
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Email'); ?><span class="star">*</span></label>
            <div class="col-sm-5">
               <?php
                  echo $this->Form->input(
                    'email',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Email'),
                        'remote' => $this->Html->url(array('controller' => 'homes', 'action' => 'ajax_check_email', 'admin' => false))
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Password') ?><span class="star">*</span></label>
            <div class="col-sm-5">
               <?php
                  echo $this->Form->input(
                    'password',
                    array(
                        'type' => 'password',
                        'class' => 'form-control',
                        'placeholder' => __('Password'),
                        'emailRegex' => true
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Confirm Password'); ?><span class="star">*</span></label>
            <div class="col-sm-5">
               <?php
                  echo $this->Form->input(
                    'confirm_password',
                    array(
                        'type' => 'password',
                        'class' => 'form-control',
                        'placeholder' => __('Confirm Password'),
                        'emailRegex' => true
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Phone Number'); ?><span class="star">*</span></label>
            <div class="col-sm-5">
               <?php
                  echo $this->Form->input(
                    'phone',
                    array(
                        'type' => 'number',
                        'class' => 'form-control',
                        'placeholder' => __('Phone Number')
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Address'); ?><span class="star">*</span></label>
            <div class="col-sm-10">
               <?php
                  echo $this->Form->input(
                    'address',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Address')
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-5 col-sm-offset-2">
               <?php
                  echo $this->Form->input(
                    'country_id',
                    array(
                        'options' => $countriesList,
                        'empty' => __('Country'),
                        'class' => 'form-control',
                        'id' => 'country-list',
                        'onChange' => 'fetchStates(this.value)'
                      )
                  );
               ?>
            </div>
            <div class="col-sm-5">
               <?php
                  echo $this->Form->input(
                    'state_id',
                    array(
                        'empty' => __('State'),
                        'class' => 'form-control',
                        'id' => 'state-list'
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-5 col-sm-offset-2">
               <?php
                  echo $this->Form->input(
                    'city',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('City')
                      )
                  );
               ?>
            </div>
            <div class="col-sm-5">
               <?php
                  echo $this->Form->input(
                    'zip',
                    array(
                        'class' => 'form-control',
                        'placeholder' => __('Zip Code')
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Security Question'); ?><span class="star">*</span></label>
            <div class="col-sm-10">
               <?php
                  echo $this->Form->input(
                    'question_id',
                    array(
                        'options' => $sQuestions,
                        'empty' => __('-Please select your question-'),
                        'class' => 'form-control',
                     )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Answer'); ?><span class="star">*</span></label>
            <div class="col-sm-10">
               <?php
                  echo $this->Form->input(
                    'answer',
                    array(
                        'class' => 'form-control'
                      )
                  );
               ?>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10 chk">
               <div class="checkbox">
                  <div> <?php echo __('Agree with our term and conditions'); ?></div>
                  <?php
                    echo $this->Form->checkbox(
                          'agree',
                          array(
                              'hiddenField' => false,
                              'class' => 'chk-settings'
                            )
                          );
                  ?>
               </div>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
               <?php
                echo $this->Form->submit(
                    __('Register'),
                    array(
                        'class' => 'btn btn-orange btn-lg'
                      )
                  );
               ?>
            </div>
         </div>
      <?php
        echo $this->Form->end();
      ?>
   </div>
   <div class="col-lg-3 col-sm-12 col-md-3 col-xs-12 we-appreciate">
      <div class="text-orange"> <?php echo __('We appreciate'); ?> </div>
      <p>
        <?php echo __('your interest in our Inspection Reporting products and promise to respond to your request as quickly as possible.<br><br>
         ReportsOnlinePlus has strategic partners who utilize our state of art fire life safety reporting and tracking system that would be more than happy to speak with you about your current requests.<br><br>
         Please provide the following information so that we can begin the process of providing you with a Service Provider information that has the qualified personnel to service your request.'); ?>
      </p>
   </div>
</div>
<?php
  echo $this->Html->script(
      'frontend/signup',
      array(
          'block' => 'scriptBottom'
        )
    );
?>
<!--script type="javascript/text" id="stateOptions"></script-->