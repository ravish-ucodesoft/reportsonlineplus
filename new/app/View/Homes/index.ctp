<!-- Slider start here -->
<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
   <!-- Indicators -->
   <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
   </ol>

   <!-- Wrapper for slides -->
   <div class="text-section">
      <h2><?php echo __('Automated Inspection and Scheduling Process System'); ?></h2>
      <p>
         <?php
            echo $this->Html->link(
                   __('FREE REGISTER'),
                   'javascript:void(0)',
                   array(
                        'class' => 'btn btn-orange btn-lg'
                     )
               );
         ?>
      </p>
   </div>
   <div class="carousel-inner" role="listbox">
      <div class="item active">
         <?php
            echo $this->Html->image(
                  'slider-img.png',
                  array(
                        'alt' => 'chania'
                     )
               );
         ?>
      </div>
      <div class="item">
         <?php
            echo $this->Html->image(
                  'slider-img-3.png',
                  array(
                        'alt' => 'chania'
                     )
               );
         ?>
      </div>
      <div class="item">
         <?php
            echo $this->Html->image(
                  'slider-img-4.png',
                  array(
                        'alt' => 'chania'
                     )
               );
         ?>
      </div>
   </div>
</div>

<!-- thumbanil section -->

<div class="clearfix img-box-setting">
   <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 padding-left">
      <div class="ask-question thumbnail-setting">
         <div class="thumnail-text-seeting">
            <p><?php echo __('Do You have question about'); ?></p>
            <p><?php echo __('life safety inspections or Code?'); ?></p>
            <p>
               <?php
                  echo $this->Html->link(
                         __('Ask question'),
                         'javascript:void(0)',
                         array(
                              'class' => 'btn btn-white-border btn-lg'
                           )
                     );
               ?>
            </p>
         </div>
      </div>
   </div>
   <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4">
      <div class="free-site thumbnail-setting">
         <div class="thumnail-text-seeting">
            <p><?php echo __('Free Site Survey'); ?></p>
            <p class="margin-top">
               <?php
                  echo $this->Html->link(
                         __('Get free survey now'),
                         'javascript:void(0)',
                         array(
                              'class' => 'btn btn-white-border btn-lg'
                           )
                     );
               ?>
            </p>
         </div>
      </div>
   </div>
   <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 padding-right">
      <div class="are-you thumbnail-setting">
         <div class="thumnail-text-seeting">
            <p><?php echo __('Are you up to Date?'); ?></p>
            <p><?php echo __('With your Fire Safety'); ?></p>
            <p>
               <?php
                  echo $this->Html->link(
                         __('Register'),
                         'javascript:void(0)',
                         array(
                              'class' => 'btn btn-white-border btn-lg'
                           )
                     );
               ?>
            </p>
         </div>
      </div>
   </div>
</div>

<!-- why us report section -->

<div class="why-us">
   <div class="container">
      <div class="col-xs-12 col-lg-6 col-sm-6 col-md-6">
         <h3 class="text-orange"><?php echo __('Why use Report Online Plus?'); ?></h3>
         <p>
            <?php echo __('ReportsOnlinePlus is an Open Source Software Provider of Automated Fire Life Safety Inspection Process System that allows the Service Provider to schedule, view and track all their inspections from sale until certification. It also allows the Property Owner to view the same intricate process in real-time, to insure accuracy and completion of the scope.'); ?>
         </p>
         <div class="small-title"> <?php echo __('Major Attributes:'); ?></div>
         <ul>
            <li>
               <?php echo __('Online Accessible'); ?>
            </li>
            <li><?php echo __('Real Time Reporting'); ?></li>
            <li><?php echo __('Automatic Scheduling'); ?></li>
            <li><?php echo __('Deficiency/Work Order Tracking'); ?></li>
            <li><?php echo __('Complete Documentation Secure Storage'); ?></li>
         </ul>
      </div>
      <div class="col-xs-12 col-lg-6 col-sm-6 col-md-6">
         <?php
            echo $this->Html->image(
                   'u50.png',
                   array(
                        'class' => 'img-responsive',
                        'alt' => 'img'
                     )
               );
         ?>
      </div>
   </div>
</div>

<!-- we found fire safety section -->

<div class="we-found-section">
   <?php
      echo $this->Html->image(
             'banner-img-1.png',
             array(
                  'class' => 'img-responsive',
                  'alt' => 'img'
               )
         );
   ?>
</div>

<!-- find the perfect match section -->

<div class="chat-section">
   <div class="container">
      <div class="col-lg-6 col-xs-12 col-sm-6 col-md-6">
         <?php
            echo $this->Html->image(
                   'chat_image.png',
                   array(
                        'class' => 'img-responsive',
                        'alt' => 'img'
                     )
               );
         ?>
      </div>
      <div class="col-lg-6 col-xs-12 col-sm-6 col-md-6">
         <h4 class="text-orange"><?php echo __('Find the perfect match fast'); ?></h4>
         <p>
            <?php echo __('Property Owners have the ability to access their inspection reports, review deficiencies and recommendation, track changes of the health of their systems and produce the information on demand. In addition, Property Owners have the ability to locate a new service provider, that matches their specific needs, through our Recommendation Generation Portal.'); ?>
         </p>
      </div>
   </div>
</div>

<!-- we can provide -->

<div class="we-can-provide">
   <?php
      echo $this->Html->image(
          'banner-img-2.png',
          array(
               'class' => 'img-responsive',
               'alt' => 'img'
            )
      );
   ?>
</div>

<!-- registeration section -->
<div class="register-section">
   <div class="container">
      <div class="col-lg-6 col-sm-12 col-md-12 col-xs-12 text-center">
         <h5 class="text-orange"><?php echo __('Secure your House/Offices'); ?></h5>
         <p><?php echo __('Property Owners have the ability to access their inspection reports, review deficiencies and recommendation, track changes of the health of their systems and produce the information on demand. In addition, Property Owners have the ability to locate a new service provider, that matches their specific needs, through our Recommendation Generation Portal.'); ?></p>
         <a role="button" href="#" class="btn btn-orange btn-lg"><?php echo __('FREE REGISTER'); ?></a>
      </div>
      <div class="col-lg-6 col-sm-12 col-md-12 col-xs-12 text-center">
         <h5 class="text-orange"><?php echo __('Fire protection Service Companies'); ?></h5>
         <p><?php echo __('Increased productivity and efficiency helps to reduce overhead associated with inspections. Save time and materials associated with printing, faxing, and mailing paper inspection reports. Because inspectors can transfer data wirelessly from the field, you save gas and vehicle maintenance associated with daily trips to the office.'); ?></p>
         <a role="button" href="#" class="btn btn-orange btn-lg"><?php echo __('FREE REGISTER'); ?></a>
      </div>
   </div>
</div>

<!-- registraion popup -->
<div class="modal fade registraion" id="registraion">
   <div class="modal-dialog user-model">
      <div class="modal-content">
         <div class="modal-header text-center">
            <h4 class="modal-title"><?php echo __('Let\'s get started!'); ?></h4>
            <p><?php echo __('First,tell us what you\'re looking for.'); ?></p>
         </div>
         <div class="modal-body clearfix">
            <div class="col-sm-6 text-center">
               <i class="fa fa-fire-extinguisher"></i>
               <div class="iam-text"><?php echo __('I am'); ?></div>
               <div class="text-bold"><?php echo __('Fire Safety service Providers'); ?></div>
               <div class="provider-text"><?php echo __('I looking fir clients'); ?></div>
               <div>
                  <?php
                     echo $this->Html->link(
                           __('Get Clients'),
                           '/homes/signUp',
                           array(
                                 'class' => 'btn btn-orange'
                              )
                        );
                  ?>
               </div>
            </div>
            <div class="col-sm-6 text-center owner">
               <i class="fa fa-building-o"></i>
               <div class="iam-text"><?php echo __('I am'); ?></div>
               <div class="text-bold"><?php echo __('Property Owners'); ?></div>
               <div class="provider-text"><?php echo __('I want fire Safety inspection reports'); ?></div>
               <div>
                  <?php
                     echo $this->Html->link(
                           __('Hire'),
                           'javascript:void(0)',
                           array(
                                 'class' => 'btn btn-orange'
                              )
                        );
                  ?>
               </div>
            </div>
         </div>
         <div class="modal-footer">
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>