<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Reportsonline<?php echo !empty($title_for_layout) ? " :: " . $title_for_layout : ""?></title>
      <!-- Bootstrap -->
      <?php
         echo $this->Html->css(array(
               'bootstrap.min',
               'style',
            'pnotify/jquery.pnotify.default',
            'pnotify/jquery.pnotify.default.icons',
               
            )
         );
         echo $this->fetch('css');
      ?>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->
      <script type="text/javascript">
         var SITE_URL = '<?php echo Router::url('/', true);?>';
      </script>
   </head>
   <body>
      <?php
         $this->extend('/Common/view');

         $this->start('header');
         echo $this->element('header/home_header');
         $this->end();

         echo $content_for_layout;

         echo $this->element('frontend/footer_images');
         echo $this->element('frontend/footer');
         
         echo $this->Html->script(array(
               'jquery.min',
               'bootstrap.min',
               'frontend/common',
               'jquery.validate.js',
               'pnotify/jquery.pnotify',
            )
         );

         echo $this->fetch('script');
         echo $this->fetch('scriptBottom');
      ?>
      <script type="text/javascript">
         function flash(type, message, title) {
            $.pnotify({
                title: title,
                text: message,
                type: type, // success , info , error, notice
                width: "40%",
                addclass: "stack-custom2",
                stack: {"dir1": "down", "dir2": "right", "push": "top"},
                delay: 2000
            });
         }
         $(document).ready(function() {
             <?php
             if ($this->Session->check('Message.flash')) {
                 if (!$this->Session->check('Message.flash.params')) {
                     $message = 'info';
                 } else {
                     $message = $this->Session->read('Message.flash.params');
                 }
                 switch ($message) {
                     case 'success':
                         echo "flash('success','{$this->Session->flash()}','Success');";
                         break;
                     case 'error':
                         echo "flash('error','{$this->Session->flash()}','Error');";
                         break;
                     case 'info':
                         echo "flash('info','{$this->Session->flash()}','Information');";
                         break;
                     case 'notice':
                         echo "flash('notice','{$this->Session->flash()}','Notice');";
                         break;
                     default:
                         echo "flash('info','{$this->Session->flash()}','Information');";
                         break;
                 }
             }
             ?>
         });
      </script>      
   </body>
</html>