<!-- pantner logo section -->
<div class="bottom-section">
  <div class="container">
    <div class="col-xs-12">
       <?php
         echo $this->Html->link(
             $this->Html->image(
                  'patner-logo_02.png',
                  array(
                     'class' => 'img-responsive',
                     'alt' => 'img'
                  )
               ),
             'javascript:void(0)',
             array(
                  'escape' => false
               )
         );
      ?>
      <?php
         echo $this->Html->link(
             $this->Html->image(
                  'patner-logo_03.png',
                  array(
                     'class' => 'img-responsive',
                     'alt' => 'img'
                  )
               ),
             'javascript:void(0)',
             array(
                  'escape' => false
               )
         );
      ?>
      <?php
         echo $this->Html->link(
             $this->Html->image(
                  'patner-logo_05.png',
                  array(
                     'class' => 'img-responsive',
                     'alt' => 'img'
                  )
               ),
             'javascript:void(0)',
             array(
                  'escape' => false
               )
         );
      ?>
      <?php
         echo $this->Html->link(
             $this->Html->image(
                  'patner-logo_07.png',
                  array(
                     'class' => 'img-responsive',
                     'alt' => 'img'
                  )
               ),
             'javascript:void(0)',
             array(
                  'escape' => false
               )
         );
      ?>
      <?php
         echo $this->Html->link(
             $this->Html->image(
                  'patner-logo_09.png',
                  array(
                     'class' => 'img-responsive',
                     'alt' => 'img'
                  )
               ),
             'javascript:void(0)',
             array(
                  'escape' => false
               )
         );
      ?>
    </div>
  </div>
</div>