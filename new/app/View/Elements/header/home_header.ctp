<header class="header">
   <div class="container">
      <nav class="navbar navbar-default">
         <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only"><?php echo __('Toggle navigation'); ?></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <?php
                  echo $this->Html->link(
                        $this->Html->image(
                              'logo.png',
                              array(
                                    'class' => 'logo img-responsive',
                                    'alt' => 'logo'
                                 )
                           ),
                        '/',
                        array(
                              'class' => 'navbar-brand',
                              'escape' => false
                           )
                     );
               ?>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-right">
                  <li>
                     <?php
                        echo $this->Html->link(
                              __('Report Online Plus'),
                              'javascript:void(0)'
                           );
                     ?>
                  </li>
                  <li>
                     <?php
                        echo $this->Html->link(
                              __('Return on Investment'),
                              'javascript:void(0)'
                           );
                     ?>
                  </li>
                  <li>
                     <?php
                        echo $this->Html->link(
                              __('Provider Locator'),
                              'javascript:void(0)'
                           );
                     ?>
                  </li>
                  <li>
                     <?php
                        echo $this->Html->link(
                              __('FAQ'),
                              'javascript:void(0)'
                           );
                     ?>
                  </li>
                  <li>
                     <a data-target="#registraion" data-toggle="modal">
                     <button type="button" class="btn btn-orange"><?php echo __('REGISTER'); ?></button>
                     </a>
                  </li>
                  <li>
                     <a>
                     <button type="button" class="btn btn-blue"><?php echo __('LOGIN'); ?></button>
                     </a>
                  </li>
               </ul>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
   </div>
</header>