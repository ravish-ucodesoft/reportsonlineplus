<?php
App::uses('AppModel', 'Model');
class State extends AppModel{
	
	var $name = 'State';
	
	public function fetchStates($countryId="") {
		$cond = [];
		if (!empty($countryId)) {
			$cond = ['State.country_id' => $countryId];
		}
		$data = $this->find('list',
												['conditions' => $cond],
												['order' => 'State.name ASC'],
												['id', 'name']
											);
		return $data;
	}

}