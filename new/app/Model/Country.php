<?php
App::uses('AppModel', 'Model');
class Country extends AppModel{
	var $name = 'Country';
	public function getAllCountries() {
		$data = $this->find('list',
												['order' => 'Country.name ASC'],
												['id', 'name']
											);
		return $data;
	}
}