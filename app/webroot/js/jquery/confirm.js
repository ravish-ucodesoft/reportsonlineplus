/*
 * SimpleModal Confirm Modal Dialog
 * http://www.ericmmartin.com/projects/simplemodal/
 * http://code.google.com/p/simplemodal/
 *
 * Copyright (c) 2010 Eric Martin - http://ericmmartin.com
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Revision: $Id: confirm.js 254 2010-07-23 05:14:44Z emartin24 $
 */

jQuery(function ($) {
	
$(".confirm1").click(function (e) {
e.preventDefault();

	// you must use a callback function to perform the "yes" action
		confirm1("Are you sure you want to clear this saved schedule?", function () {
			window.location.href = $(".confirm1").attr("href");
		});
});

$(".confirm2").click(function (e) {
e.preventDefault();

	// you must use a callback function to perform the "yes" action
		confirm1("Are you sure you want to publish this weekly schedule?", function () {
			window.location.href = $(".confirm2").attr("href");
		});
});

	$('#confirm-dialog input.confirm, #confirm-dialog a.confirm').click(function (e) {
		e.preventDefault();

		// example of calling the confirm function
		// you must use a callback function to perform the "yes" action
		confirm("Continue to the SimpleModal Project page?", function () {
			return true;
		});
	});
});

function confirm1(message, callback) {
	$('#confirm').modal({
		closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
		position: ["20%",],
		overlayId: 'confirm-overlay',
		containerId: 'confirm-container', 
		onShow: function (dialog) {
			var modal = this;

			$('.message', dialog.data[0]).append(message);

			// if the user clicks "yes"
			$('.yes', dialog.data[0]).click(function () {
				// call the callback
				if ($.isFunction(callback)) {
					callback.apply();
				}				
				// close the dialog
				modal.close(); // or $.modal.close();
			
			});
		}
	});
//	return false;
}
function callback11(obj)
{
window.location.href =obj.href;
}
