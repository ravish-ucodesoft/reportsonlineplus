<?php
class Company extends AppModel {

  var $name='Company';
  public $belongsTo = array('Country','State'); 
  var $hasMany = array('UserCompanyPrincipalRecord' => 
                    array( 'classname'  => 'UserCompanyPrincipalRecord',
                           'conditions' => '',
                           'order'      => '',
                           'dependent'  => false,
                           'foreignKey' => 'company_id'
                    )
              ); 

var $validate = array(
         'name' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter company name.'
                )
          ),
         'address' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter company address.'
              )
          ),
	 'site_manager' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter Authorized/Designated Manager of the site'
              )
          ),
	 'phone_site_manager' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter direct phone of designated site manager'
              )
          ),
	 'email_site_manager' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter direct email of designated site manager'
              )
          ),
	 'company_bio' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter company bio'
              )
          )
  );  

     public function beforeSave() {
	  foreach($this->data[$this->alias] as $field => $value) {
        if($field !== 'owner_id') {
          $this->data[$this->alias]['field'] = $field;
          $this->data[$this->alias]['value'] = $value;
        }
     }
	  return true;
     }

     function setValidate($fields = null){    
	  if ($fields === null) {
			$this->validate = $this->_validate;
		} else {
			$this->validate = array();
			foreach ($fields['Company'] as $f=>$field['key']) {	
    		 if (isset($this->_validate[$f])) {
					 $this->validate[$f] = $this->_validate[$f];
			   }
		  }			 
		}
     }
}