<?php 
class Inquiry extends AppModel{
    var $name = 'Inquiry';
      var $validate  = array(
        'name' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Name.'
                )
         ),
         'email' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Email.'
                )
         ),
         'query' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter your Query.'
                )
         )
      );  
}