<?php 
class SubscriptionDuration extends AppModel{

var $name='SubscriptionDuration';

  var $validate  = array(
         'min_allowed_clients' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Minimum allowed Clients.'
                ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Please Enter Only Number.'
                )
          ),
         'max_allowed_clients' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Minimum allowed Clients.'
                ),    
             'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Please Enter Only Number.'
              )
          ),
         'min_allowed_site_address' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Minimum allowed Site Address.'
                ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Please Enter Only Number.'
                )
          ),
         'max_allowed_site_address' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Maximum allowed Site Address.'
                ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Please Enter Only Number.'
                )
          ),
          'plan_name' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Plan Name.'
                )
          ),
         'monthlycharges' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Monthly charges for Subscription.'
                ),
            'rule2' => array(
                'rule' => 'numeric',
                'message' => 'Please Enter Only Number'
                )
        ),        
        
        
    );  
    
    

}
?>