<?php
class Report extends AppModel{
    var $name='Report';
    //var $hasMany = array('Service');
    var $hasMany = array(
        'Service' => array(
            'className'    => 'Service',
            'foreignKey'    => 'report_id',
            'order'=>'Service.name',
            'conditions'=>array('Service.status'=>1),
	    'dependent'=> true
        ));

}