<?php 
	class Websitepage extends AppModel{
		var $name='Websitepage';
		var $validate=array(
                        'page_name'=>array(
				'notEmpty'=>array(
                                    'rule'=>'notEmpty',
                                    'message'=>'Please enter valid page name'
					)
				
				   ),
			'page_content'=>array(
				  'notEmpty'=>array(	
                                    'rule'=>'notEmpty',
                                    'message'=>'Please enter page content'
                                    )
				  ),
            
                        'meta_title'=>array(
                                    'rule'=>'notEmpty',
                                    'message'=>'Please enter meta title'
                                    ),       
                        'meta_description'=>array(
                                    'rule'=>'notEmpty',
                                    'message'=>'Please enter meta description'
                                    ),
                        'meta_keywords'=>array(
                                    'rule'=>'notEmpty',
                                    'message'=>'Please enter page keyword'
                                    ),
			'page_type'=>array(
                                    'rule'=>'notEmpty',
                                    'message'=>'Please enter page position'
                                    )
                        
                        );                  	
	}