<?php
class Contactus extends AppModel {
    var $useTable = false;
    var $_schema = array(
        'fname'		=>array('type'=>'string', 'length'=>100),
        'lname'		=>array('type'=>'string', 'length'=>100),  
        'email'		=>array('type'=>'string', 'length'=>255),
        'phone'		=>array('type'=>'string', 'length'=>100),        
        'comment'	=>array('type'=>'text')
    );
    
    var $validate = array(
        'fname' => array(
            'rule' => 'notEmpty',  
            'message' => 'Please enter first name',
            'last' => true
         ),
         'lname' => array(
            'rule' => 'notEmpty',  
            'message' => 'Please enter last name',
            'last' => true
         ),
        'email' => array(        
            'rule1'=> array(
                    'rule' => array('notEmpty'),
                     'required' => true,
                     'message' => 'Please enter valid email address'          			
                    ),
                
            'rule2' => array(
                    'rule' => 'email',
                    'required' => true,
                    'message' => 'Please enter valid email address.'
                    )
        ),
        'phone' => array(        
            'rule1'=> array(
                    'rule' => array('notEmpty'),
                     'required' => true,
                     'message' => 'Please enter phone number'          			
                    ),
                
            'rule2' => array(
                    'rule' => 'numeric',
                    'required' => true,
                    'message' => 'Please enter valid phone number.'
                    )
        ),        
        'comment' => array(
            'rule' => 'notEmpty',  
            'message' => 'Please enter comments',
            'last' => true
         ),
);
}
?>
