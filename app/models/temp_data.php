<?php 
class TempData extends AppModel
{
    
   var $name = 'TempData';
   var $useTable = 'temp_data';
   
   # CODE TO FIND THE START & END TIME TO CONVERT 12 HR FORMAT FROM 24 HR FORMAT OF DATABASE
   function afterFind($results) {
  	foreach ($results as $key => $val) {
    		if (isset($val['TempData']['shift_start_time'])) {
    			$results[$key]['TempData']['shift_start_time'] = $this->convertto12hourFormat($val['TempData']['shift_start_time']);
    		  }
        if (isset($val['TempData']['shift_end_time'])) {
    			$results[$key]['TempData']['shift_end_time'] = $this->convertto12hourFormat($val['TempData']['shift_end_time']);
        }
  	}
	return $results;
  }

  function convertto12hourFormat($dateString) {	     
	     return $time=DATE("g:i a",STRTOTIME($dateString));
  }
}
?>