<?php
class ExtinguisherReport extends AppModel{

var $name='ExtinguisherReport';
var $hasMany = array(
        'ExtinguisherReportRecord' => array(
            'className'    => 'ExtinguisherReportRecord',
            'foreignKey'    => 'extinguisher_report_id',
	    'dependent'	    => true
        ));
} ?>