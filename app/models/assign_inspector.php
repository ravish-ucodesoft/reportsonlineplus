<?php
class AssignInspector extends AppModel{

var $name='AssignInspector';
var $belongsTo = array(
       'Inspector' => array(
           'className'    => 'User',
           'foreignKey'    => 'inspector_id',
           'fields' =>array('fname','lname','id')
       ),
       'Client' => array(
           'className'    => 'User',
           'foreignKey'    => 'client_id',
           'fields' =>array('fname','lname','id')
       ),
       'Schedule' => array(
            'className'    => 'Schedule',
            'foreignKey'    => 'schedule_id'
        ),
       'Service' => array(
            'className'    => 'Service',
            'foreignKey'    => 'service_id'
        )
          ); 

/*var $validate1=array(
        'shift_start_time' => array(        
              'rule1'=> array(
              			'rule' => 'notEmpty',
              			'required' => true,
              			'message' => 'Please enter start date'          			
            		   )
                     	
        ),
        'shift_end_time' => array(            
              		'rule1' => array(
                      'rule' => 'notEmpty',
                      'required' => true,
                      'message' => 'Please enter end date.'
                      )
        ),
        'paid_hours' => array(
                  'rule1' => array(
                      'rule' => 'notEmpty',
                      'message' => 'Please enter paid hour.'
                      )
        ),        
         'position_id' => array(
                  'rule1' => array(
                      'rule' => 'notEmpty',
                      'message' => 'Please select position.'
                      )
        )
    );
*/
//function setValidate($fields = null) {    
//		if ($fields === null) {
//			$this->validate = $this->_validate;
//		} else {
//			$this->validate = array();
//			foreach ($fields['Schedule'] as $f=>$field['key']) {		
//				if (isset($this->_validate[$f])) {
//					$this->validate[$f] = $this->_validate[$f];
//				}
//			}			 
//		}
//	}
	 
	/*************************************************************************
	 *function      : confirmpass
	 *functionality : check password and confirm password field same or not
	 ************************************************************************/
   
   /*function confirmpass(){		
  		$valid = false;
  		if ($this->data['Schedule']['pwd'] ==  $this->data['Schedule']['cpwd']) 
  		{
  			$valid = true;
  		}
  		return $valid;
	 }
	 
	 # CODE TO FIND THE START & END TIME TO CONVERT 12 HR FORMAT FROM 24 HR FORMAT OF DATABASE
   function afterFind($results) {
  	foreach ($results as $key => $val) {
    		if (isset($val['Schedule']['shift_start_time'])) {
    			$results[$key]['Schedule']['shift_start_time'] = $this->convertto12hourFormat($val['Schedule']['shift_start_time']);
    		  }
        if (isset($val['Schedule']['shift_end_time'])) {
    			$results[$key]['Schedule']['shift_end_time'] = $this->convertto12hourFormat($val['Schedule']['shift_end_time']);
        }
  	}
	return $results;
  }

  function convertto12hourFormat($dateString) {	     
	     return $time=DATE("g:i a",STRTOTIME($dateString));
  }*/
	 
	 
}
?>