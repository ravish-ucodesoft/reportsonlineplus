<?php
class FirealarmReport extends AppModel{

var $name='FirealarmReport';
//public $hasOne = array('FaAlarmFunction');
var $hasOne = array(
        'FaAlarmFunction' => array(
            'className'    => 'FaAlarmFunction',            
	    'dependent'	    => true
        ));
 var $hasMany = array(
    'FaAlarmPanelSupervisoryFunction' => array(
    'className' => 'FaAlarmPanelSupervisoryFunction',
    'order' => 'FaAlarmPanelSupervisoryFunction.id ASC',
    'dependent'	    => true
    ),
    'FaAuxiliaryFunction' => array(
    'className' => 'FaAuxiliaryFunction',
    'order' => 'FaAuxiliaryFunction.id ASC',
    'dependent'	    => true
    ),
    'FaPumpSupervisoryFunction' => array(
    'className' => 'FaPumpSupervisoryFunction',
    'order' => 'FaPumpSupervisoryFunction.id ASC',
    'dependent'	    => true
    ),
    'FaGeneratorSupervisoryFunction' => array(
    'className' => 'FaGeneratorSupervisoryFunction',
    'order' => 'FaGeneratorSupervisoryFunction.id ASC',
    'dependent'	    => true
    ),
    'FaLocationDescription' => array(
    'className' => 'FaLocationDescription',
    'order' => 'FaLocationDescription.id ASC',
    'dependent'	    => true
    )
  ); 

} ?>