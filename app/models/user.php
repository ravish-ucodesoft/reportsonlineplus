<?php class User extends AppModel{



  var $name='User';

  var $service = false;

  var $hasOne = array('Company');

//var $belongsTo = array('Service');

public $belongsTo = array('Country','State','City',

                                            'BillCountry'=>array(

                                                                    'className'=>'Country',

                                                                    'foreignKey'=>'billto_country_id'

                                                                 ),

                                            'BillState'=>array(

                                                                    'className'=>'State',

                                                                    'foreignKey'=>'billto_state_id'

                                                                 ),
                                            'BillCity'=>array(

                                                                    'className'=>'City',

                                                                    'foreignKey'=>'billto_city_id'

                                                                 ),
                                            'Question'=>array(

                                                                    'className'=>'Squestion',

                                                                    'foreignKey'=>'question_id'

                                                                 )

                          );

var $actsAs = array('Containable');

var $hasMany = array(

    'SpSiteAddress'=>array(

        'className'=>'SpSiteAddress',

        'foreignKey'=>'client_id',

        'conditions'=>array('SpSiteAddress.idle'=>0)

    )

);

 

var $validateChangePass=array(

        'oldpwd' => array(

        

        'rule1'=> array(

          		'rule' => array('checkpass'),

          		 'required' => true,

          			'message' => 'Please enter correct current password'          			

        		),

            

		'rule2' => array(

                'rule' => 'notEmpty',

                'required' => true,

                'message' => 'Please enter password.'

                )

        ),

        'pwd' => array(

            'rule1'=> array(

          			'rule' => array('minLength',6),

          			'message' => 'Password must be at least 5 characters long'

        		   ),

        		'rule2' => array(

                'rule' => 'notEmpty',

                'required' => true,

                'message' => 'Please enter new password.'

                )

        ),

        'cpwd' => array(

            'rule1' => array(

                'rule' => 'notEmpty',

                'required' => true,

                'message' => 'Please enter confirm password.'

                ),

             'rule2' => array(

                'rule' => array('confirmpass'),

                'message' => 'Password and confirm password is not matching.'

                )

        )  

    );

var $validate = array(

         'fname' => array(

            'rule1' => array(

                'rule' => 'notEmpty',

                'message' => 'Please enter first name.'

                )

          ),

         'email' => array(

            'rule1' => array(

                'rule' => 'email',

                'message' => 'Please enter correct email.'

                ),

            'rule2' => array(

                'rule' => array('checkUniquedname','email'),

                'message' => 'Given email already exists.'

                ),                

            'rule3' => array(

                'rule' => 'notEmpty',

                'message' => 'Please enter email.'

                )

        ),

	/*'site_email' => array(

            'rule1' => array(

                'rule' => 'email',

                'message' => 'Please enter correct site email.'

                ),

            'rule2' => array(

                'rule' => array('checkUniquedsitename','site_email'),

                'message' => 'Given site email already exists.'

                ),                

            'rule3' => array(

                'rule' => 'notEmpty',

                'message' => 'Please enter email.'

                )

        ),*/

        'password' => array(

            'rule1' => array(

                'rule' => array('minLength', '5'),

                'message' => 'Password must be Mimimum 5 characters long.',

            ),

            'rule2' => array(

                'rule' => 'notEmpty',

                'message' => 'Password can not be empty.',

            )

	),

	'cpassword'=>array(

	    'rule1' => array(

                'rule' =>  array('matchPasswords'),

                'message' => 'Password and confirm password must match.',

	    )

	),

          'subscription_duration_id' => array(

            'rule1' => array(

                'rule' => 'notEmpty',

                'message' => 'Please select the subscription plan.'

              )

          )        

        

    );  



function setValidate($fields = null) {    



		if ($fields === null) {

			$this->validate = $this->_validate;

		} else {

			$this->validate = array();

			foreach ($fields['User'] as $f=>$field['key']) {		

				if (isset($this->_validate[$f])) {

					$this->validate[$f] = $this->_validate[$f];

				}

			}			 

		}

	}

/*************************************************************************

	compare password or confirm password

************************************************************************/

	function matchPasswords()

	{

		

		if(empty($this->data['User']['password']) && empty($this->data['User']['cpassword']))    {

			return true;

		}

		if(!empty($this->data['User']['password']) && !empty($this->data['User']['cpassword']))

		{

			if($this->data['User']['password'] == $this->data['User']['cpassword']) 

			{

				

				return true;

			}

			return false;

		}

	}

	/*************************************************************************

	 *function      : confirmEmail

	 *functionality : check email and confirm email field same or not

	 ************************************************************************/

   

   function confirmEmail(){		

  		$valid = false;

  		//pr($this->data);

  		if ($this->data['User']['email'] ==  $this->data['User']['cemail']) 

  		{

  			$valid = true;

  		}

  		return $valid;

	 }

	

	

	/*************************************************************************

	 *function      : confirmpass

	 *functionality : check password and confirm password field same or not

	 ************************************************************************/

   

   function confirmpass(){		

  		$valid = false;

  		//pr($this->data);

  		if ($this->data['User']['pwd'] ==  $this->data['User']['cpwd']) 

  		{

  			$valid = true;

  		}

  		return $valid;

	 }

	 

    public function checkUniquedname($data, $fieldName){

	     if(isset($this->data['User']['id']) && !empty($this->data['User']['id']))

	     {

	       if($data[$fieldName]==$this->field($fieldName,"email='".$this->data['User']['email']."' AND id != '".$this->data['User']['id']."'")){

					   return false;					

				   }else{

					   $valid = false;

					   if(isset($fieldName) && $this->hasField($fieldName))

					   {

						   $valid = $this->isUnique(array($fieldName => $data));

					   }

					   return $valid;

					   }

	     }

	     else

	     {

				   if($data[$fieldName]==$this->field($fieldName,"email='".$this->data['User']['email']."'")){

					   return false;					

				   }else{

					   $valid = false;

					   if(isset($fieldName) && $this->hasField($fieldName))

					   {

						   $valid = $this->isUnique(array($fieldName => $data));

					   }

					   return $valid;

					   }

	     }

    }



/*public function checkUniquedsitename($data, $fieldName) {

	     if(isset($this->data['User']['id']) && !empty($this->data['User']['id']))

	     {

	       if($data[$fieldName]==$this->field($fieldName,"email='".$this->data['User']['email']."' AND id != '".$this->data['User']['id']."'")){

					   return false;					

				   }else{

					   $valid = false;

					   if(isset($fieldName) && $this->hasField($fieldName))

					   {

						   $valid = $this->isUnique(array($fieldName => $data));

					   }

					   return $valid;

					   }

	     }

	     else

	     {

				   if($data[$fieldName]==$this->field($fieldName,"email='".$this->data['User']['email']."'")){

					   return false;					

				   }else{

					   $valid = false;

					   if(isset($fieldName) && $this->hasField($fieldName))

					   {

						   $valid = $this->isUnique(array($fieldName => $data));

					   }

					   return $valid;

					   }

		      }

}*/



function uniqueUserNamevalid() 

{	

                    $valid = false;

                    if(isset($this->data['User']['id']) && $this->data['User']['id']=='') {

        $res=$this->find('list',array('fields'=>array('username'),'conditions'=>array('username'=>$this->data['User']['username'])));

        $count=count($res);

        }	else {

        $count=0;

        }		

  

		if($count>0) 

		$valid = false;

		else

		$valid = true;		

		return $valid;

}





function afterFind($results) {

		foreach ($results as $key => $val) {

		if (isset($val['User']['service_id'])) {

			if($val['User']['service_id']!=NULL){			

				if($this->service == true){

					$results[$key]['User']['service_id']  = $this->ServiceName($val['User']['service_id']);

				}

			}	

		}

		}

		return $results;

}





function ServiceName($service_ids=null) {

		$arr=explode(',',$service_ids);

		App::import('model','Service');

		$service = new Service();

		$final_arr=array();

		foreach($arr as $key=>$val):

		$result = $service->find('first',array('fields'=>array('Service.name','Report.name'),'conditions'=>array('Service.id'=>$val)));

		//$final_arr[$result['Service']['name']]=$result['Report']['name'];

		$final_arr[$result['Report']['name']][]=$result['Service']['name'];

		endforeach;

		return $final_arr;

		}









}

?>