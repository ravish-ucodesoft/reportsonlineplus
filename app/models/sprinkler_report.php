<?php
class SprinklerReport extends AppModel{

    var $name='SprinklerReport';
  //  public $hasOne = array('SprinklerReportAlarm','SprinklerReportConnection','SprinklerReportControlvalve','SprinklerReportDrysystem','SprinklerReportGeneral','SprinklerReportPiping','SprinklerReportSpecialsystem','SprinklerReportWatersupply','SprinklerReportWetsystem');
     var $hasOne = array(
	    'SprinklerReportAlarm' => array(
            'className'    => 'SprinklerReportAlarm',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    'SprinklerReportConnection' => array(
            'className'    => 'SprinklerReportConnection',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    'SprinklerReportControlvalve' => array(
            'className'    => 'SprinklerReportControlvalve',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    'SprinklerReportDrysystem' => array(
            'className'    => 'SprinklerReportDrysystem',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    'SprinklerReportGeneral' => array(
            'className'    => 'SprinklerReportGeneral',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    'SprinklerReportPiping' => array(
            'className'    => 'SprinklerReportPiping',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    'SprinklerReportSpecialsystem' => array(
            'className'    => 'SprinklerReportSpecialsystem',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    'SprinklerReportWatersupply' => array(
            'className'    => 'SprinklerReportWatersupply',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    'SprinklerReportWetsystem' => array(
            'className'    => 'SprinklerReportWetsystem',
            'foreignKey'    => 'sprinkler_report_id',
	    'dependent'	    => true
	    ),
	    	    

	    
	); 
}