<?php
class Schedule extends AppModel{
    var $name='Schedule';
    var $hasMany = array(
        'ScheduleService' => array(
            'className'    => 'ScheduleService',
            'foreignKey'    => 'schedule_id',
	    'dependent'=> true
        ));
    var $belongsTo = array(
        'ServiceCall'=>array(
            'className'=>'ServiceCall',
            'foreignKey'=>'service_call_id'
        )
    );
}

?>