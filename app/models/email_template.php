<?php
class EmailTemplate extends AppModel{

var $name='EmailTemplate';

var $validate = array(
         'mail_subject' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter the Subject.'
                )
          ),
	 'mail_body' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter the Content.'
                )
          )
         
    );  
}
?>