<?php
class ReportCert extends AppModel{
    var $name='ReportCert';
    var $belongsTo = array(
            'Schedule' => array(
                'className'    => 'Schedule',
                'foreignKey'    => 'schedule_id'
            ),
            'SpBlankCertForm' => array(
                'className'    => 'SpBlankCertForm',
                'foreignKey'    => 'cert_id'
            )
        );	 

}

?>