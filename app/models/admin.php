<?php 

class Admin extends AppModel{

var $name='Admin';

var $validate = array(
         'fname' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter First Name.'
                )
          ),
         'username' => array(
            'rule1' => array(
                'rule' => 'isUnique',
                'message' => 'Please choose another Username, given Username already exists.'
                ),    
             'rule2' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Username.'
              )
          ),
         'email' => array(
            'rule1' => array(
                'rule' => 'email',
                //'required' => true,
                'message' => 'Please Enter correct Email.'
                ),
            'rule2' => array(
                'rule' => 'isUnique',
                'message' => 'Please Enter another Email, given Email already exists.'
                ),                
            'rule3' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Email.'
                )
        ),        
        'pwd' => array(
            'rule1'=> array(
          			'rule' => array('minLength',5),
          			'message' => 'Password Must be at least 5 Characters Long'
        		   ),
        		'rule2' => array(
                'rule' => 'notEmpty',
                //'required' => true,
                'message' => 'Please Enter Password.'
                )
        ),
        'cpwd' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Confirm Password.'
                ),
             'rule2' => array(
                'rule' =>array('confirmpass'),
                'message' => 'Password and Confirm Password is not match.'
                )
        )
    );  

function setValidate($fields = null) {    
		if ($fields === null) {
			$this->validate = $this->_validate;
		} else {
			$this->validate = array();
			foreach ($fields['Admin'] as $f=>$field['key']) {		
				if (isset($this->_validate[$f])) {
					$this->validate[$f] = $this->_validate[$f];
				}
			}			 
		}
	}
	
	
	/*************************************************************************
	 *function      : confirmpass
	 *functionality : check password and confirm password field same or not
	 ************************************************************************/
   
   function confirmpass(){	     
  		$valid = false;
  		if ($this->data['Admin']['pwd'] ==  $this->data['Admin']['cpwd']) 
  		{
  			$valid = true;
  		}
  		return $valid;
	 }
	 
	 
	function uniqueEmailvalid() 
	{	

		$valid = false;
	if(isset($this->data['Admin']['id']) && !empty($this->data['Admin']['id'])){
    $res=$this->find('list',array('fields'=>array('email'),'conditions'=>array('email'=>$this->data['Admin']['email'])));
    $count=count($res);
    }	else {
    $count=0;
    }		
  
		if($count>0) 
		$valid = false;
		else
		$valid = true;		
		return $valid;

}

  function uniqueUserNamevalid() 
  {	
  	  $valid = false;
  	  
      if(isset($this->data['Admin']['id']) && !empty($this->data['Admin']['id'])){
        $res=$this->find('list',array('fields'=>array('username'),'conditions'=>array('username'=>$this->data['Admin']['username'])));
        $count=count($res);
      }	else {
        $count=0;
      }		
    
  		if($count>0) 
  		 $valid = false;
  		else
  		 $valid = true;		
  		return $valid;
  }
	         	

}
?>