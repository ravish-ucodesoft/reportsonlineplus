<?php class Message extends AppModel {

var $name='Message';
//var $belongsTo=array('Employee');

# CODE TO FIND DATE CONVERSION
   function afterFind($results) {
  	foreach ($results as $key => $val) {
    		if (isset($val['Message']['created'])) {
    			$results[$key]['Message']['created'] = $this->MyDateFormat($val['Message']['created']);
    		  }
        
  	}
	return $results;
  }

  function MyDateFormat($dateString) {	     
	     return date('d-m-Y', strtotime($dateString));
  }
}
?>