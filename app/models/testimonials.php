<?php 
class Testimonials extends AppModel{
    var $name = 'Testimonials';
      var $validate  = array(
        'image' => array(
            'rule1' => array(
                'rule' => array(
                    'extension', array('jpeg', 'jpg', 'png', 'gif', 'bmp')
                  ),
                 'message' => 'Please upload image.'
                )
         ),
         'body' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter Testimonial  .'
                )
         ),
         'from' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter From.'
                )
         )
      );  
}    