<?php
class Code extends AppModel{

  var $name='Code';
  public $belongsTo = array('Report','CodeType');  
  var $validate = array(
          'report_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please select a report.'
                )
          ),
         'code' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter a code.'
                )
          ),
          'code_type_id' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please select a code type.'
                )
          ),
          'description' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter a description.'
                )
          ),
   );
}