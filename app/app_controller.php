<?php
class AppController extends Controller
{
	var $helpers =array('Form','Html', 'Javascript','Ajax','Crumb','Session');	
	var $components = array('Email','Session','Cookie','RequestHandler');
	//Pages that do not require Admin authentication
	var $nonLoginActions = array('admin_login');
	//var $nonLoginActionsAgent = array('agent_login');
	var $LoginActions = array('saved','changepassword','profile','groups');
  
  // Called before every controller action. A useful place to check for active sessions and check roles
  
	function beforeFilter(){
	 	 $this->footer_link();
		 $this->header_link();
		 $this->propertyOwnerLink();
		 $this->spDuringRegLink();
		 $country = $this->getCountries();
		 $this->loadModel('TrialOption');
                 $this->loadModel('Testimonial');
		 $testimonials = $this->Testimonial->find('all');
	 	 $this->set(compact('testimonials'));
		 $this->loadModel('AboutusText');
		 $abouttext = $this->AboutusText->find('first',array('fields'=>array('AboutusText.desc')));
		 $this->set(compact('abouttext'));
		if(isset($this->params['admin']) and !empty($this->params['admin'])){
			$this->layout='admin';
			if(!in_array($this->params['action'],$this->nonLoginActions)){
			$arrData = $this->_checkAdmin();
			// Set Session Admin User Name to display on header
			$this->set('adminName',$arrData['Admin']['username']);
			}
		} else if(isset($this->params['inspector']) and !empty($this->params['inspector'])){
			$this->layout='inspector';
		  	$inspectorses = $this->_checkAgent();
			$this->set('inspectorses',$inspectorses);
			$this->set('name',$inspectorses['User']['fname']." ".$inspectorses['User']['lname']);
			// Set Session Agent Name to display on header		  		
			}
		  else if(isset($this->params['client']) and !empty($this->params['client'])){
    			$this->layout='client';
    			$clientsession = $this->_checkClient();
    			$this->set('clientsession',$clientsession);
    			$this->set('clientname',$clientsession['User']['fname']." ".$clientsession['User']['lname']);	
                 }
		 else if(isset($this->params['sp']) and !empty($this->params['sp'])){
    			$this->layout='sp';
    			$spsession = $this->_checkSp();
			$this->set('spsession',$spsession);
    			$this->set('spname',$spsession['User']['fname']." ".$spsession['User']['lname']);
			$trialOptions = $this->TrialOption->find('all');
		       if(isset($spsession) && !empty($spsession)){
			 if($trialOptions[0]['TrialOption']['trial_status'] == "On"){
				if($spsession['User']['subscription_under_trial'] == "Yes"){
				 $today_date=date('Y-m-d');
				 $record_created=date('Y-m-d',strtotime('+60 DAY',strtotime($spsession['User']['trial_date'])));
				if($record_created>$today_date){
					if($this->params['action'] != 'sp_subscriptionplan'){
						if($this->params['action'] != "sp_logout"){
							$this->redirect('/sp/sps/subscriptionplan');
						}
					}
				   }
					
				 }	
			 }
			 
		       }		
	          }
	}
	/***********************************************************************
	 * function : _checkLogin
	 * functionality : check user login 
	 ************************************************************************/            	
	function _checkLogin(){
		$user = $this->Session->check('UserId');
 		$currentUrl=$this->params['url']['url'];
		if(!$user){
			$this->Session->write('reqUrl',$currentUrl);
			$user = $this->Session->read('Log');
			$user_type_id = $user['User']['user_type_id'];
			if($user_type_id==1)
			{
				$this->redirect('/homes/splogin');
			}
			if($user_type_id==2)
			{
				$this->redirect('/homes/ilogin');
			}
			if($user_type_id==3)
			{
				$this->redirect('/homes/clientlogin');
			}
			
			$this->redirect('/users/login');
			//$this->redirect('/homes/index');
		} else {
			return $this->Session->read('User');
		}
	}
	/***********************************************************************
	 * function : _checkManager
	 * functionality : check admin login 
	 ************************************************************************/
	function _checkAgent(){
		
		$user = $this->Session->check('Log');
		$currentUrl=$this->params['url']['url'];
		if(!$user){
			$this->Session->write('reqUrl',$currentUrl);			
			$this->redirect('/homes/inspectorlogin');
		} else {
			$this->Session->delete('reqUrl');
			return $this->Session->read('Log');
		}
	}

	/***********************************************************************
	 * function : _checkClient
	 * functionality : check client/property owner login
	 ************************************************************************/
	function _checkClient(){
		$user = $this->Session->check('Log');
		$currentUrl=$this->params['url']['url'];
		if(!$user){
			$this->Session->write('reqUrl',$currentUrl);			
			$this->redirect('/homes/clientlogin');
		} else {
			$this->Session->delete('reqUrl');
			return $this->Session->read('Log');
		}
	}
	
	/***********************************************************************
	 * function : _checkClient
	 * functionality : check client/property owner login
	 ************************************************************************/
	function _checkSp(){
		$user = $this->Session->check('Log');
		if(!$user){
			$this->Session->write('reqUrl',$currentUrl);			
			$this->redirect('/homes/splogin');
		} else {
			$this->Session->delete('reqUrl');
			return $this->Session->read('Log');
		}
		  
		
	}
	
	
	/***********************************************************************
	 * function : _checkAdmin
	 * functionality : check admin login 
	 ************************************************************************/
	function _checkAdmin(){
		
		$admin = $this->Session->check('Admin');
		$currentUrl=$this->params['url']['url'];
		if(!$admin){
			$this->Session->write('reqUrl',$currentUrl);
			//$this->redirect('/admin/admins/login');
			$this->redirect(array('controller'=>'admins','action'=>'login'));
		} else {
			$this->Session->delete('reqUrl');
			return $this->Session->read('Admin');
		}
	}
	
	
	
	function convertTo24Format($hour,$minute,$meridian)
	{	   
	   $for = $hour.":".$minute." ".$meridian ;	   
	   return $time=DATE("H:i",STRTOTIME($for));
  }
  
  function convertTo12Format($hour,$minute)
	{
     $for = $hour.":".$minute;	   
	   return $time=DATE("g:i a",STRTOTIME($for));
  }
  
  
  
	
	function getCountry()
	{
		$this->loadModel('Country');
		return $countrylist=$this->Country->find('list', array(
				'conditions' => '',				
				'fields' =>array('id','name'),
				'order'=>'Country.name',
				'limit'=>'',
				'page'=>''
				));	   
	}
/************************************************************************ 
# Get  Url of the site
************************************************************************/
	function selfURL(){
		$url = "http://" . $_SERVER['HTTP_HOST'];
		return $url;
	}
	
/************************************************************************ 
	//function for getting country list
************************************************************************/
   function getCountries(){
		$this->loadModel('Country');
		$data=$this->Country->find('list', array(
				'conditions' => "",				
				'fields' =>array('id','name'),
				'order'=>'Country.name',
				'limit'=>'',
				'page'=>''
				));
		return $data;
	
	}
	
	/************************************************************************ 
	//function for getting site addresses list for a SP
************************************************************************/
   function getSiteaddress($sp_id=null){
		$this->loadModel('SpSiteAddress');
		$this->SpSiteAddress->virtualFields = array(
							'full_address' => 'CONCAT(SpSiteAddress.site_name, "(", SpSiteAddress.site_address,")")'
							);
		$data=$this->SpSiteAddress->find('list', array(
				'conditions' => array("SpSiteAddress.sp_id"=>$sp_id),				
				'fields' =>array('id','full_address'),
				));
		return $data;
	
	}

        /************************************************************************ 
	//function for getting site addresses list for a SP with respect to client
************************************************************************/
   function getClientSiteaddress($client_id=null){
		$this->loadModel('SpSiteAddress');
		$this->SpSiteAddress->virtualFields = array(
							'full_address' => 'CONCAT(SpSiteAddress.site_name, "(", SpSiteAddress.site_address,")")'
							);
		$data=$this->SpSiteAddress->find('list', array(
				'conditions' => array("SpSiteAddress.client_id"=>$client_id,"SpSiteAddress.idle"=>0),				
				'fields' =>array('id','full_address'),
				));
		return $data;
	
	}

/************************************************************************ 
	//function for state list corresponding to country id
************************************************************************/
   function getState($cntid= null){
		$this->loadModel('State');
		$states = $this->State->find('list', array(
			'conditions' => array('State.country_id' => $cntid),
			'order'=>array('State.name ASC')
		));
		return $states;  
	}
/************************************************************************ 
	//function for city list corresponding to state id
	created by Manish Kumar on Nov 16,2012
************************************************************************/	
	function getCity($stateid= null){
		$this->loadModel('City');
		$cities = $this->City->find('list', array(
			'conditions' => array('City.state_id' => $stateid),
			'order'=>array('City.name ASC')
		));
		return $cities;  
	}	
	
	function getallState(){
		$this->loadModel('State');
		$states = $this->State->find('list');
		return $states;  
	}
/************************************************************************ 
	//function to get website pages links in footer
************************************************************************/	
	
	function footer_link(){
		$this->loadModel('Websitepage');		
		$footerdata = $this->Websitepage->find('all',array('conditions'=>array('Websitepage.status'=>1,'Websitepage.page_type'=>'F'),'order'=>array('Websitepage.ordering' => 'ASC'),'fields'=>array('page_name','id'),'limit'=>'6'));
		$this->set('footerlinks',$footerdata);
	
	}

/************************************************************************ 
	//function to get website pages links in header
************************************************************************/	
	
	function header_link(){
		$this->loadModel('Websitepage');		
		$headerdata = $this->Websitepage->find('all',array('conditions'=>array('Websitepage.status'=>1,'Websitepage.page_type'=>'H'),'order'=>array('Websitepage.ordering' => 'ASC'),'fields'=>array('page_name','id')));
		$this->set('headerlinks',$headerdata);
		
	
	}
/************************************************************************ 
	//function to get main website page links in home page
************************************************************************/	
	
	function propertyOwnerLink(){
		$this->loadModel('Websitepage');		
		$propertyOwnerLink = $this->Websitepage->find('all',array('conditions'=>array('Websitepage.status'=>1,'Websitepage.page_type'=>'P'),'order'=>array('Websitepage.ordering' => 'ASC'),'fields'=>array('page_name','id')));
		$this->set('propertyOwnerLink',$propertyOwnerLink);	
	}
/************************************************************************ 
	//function to get main website page links in home page
************************************************************************/	
	
	function spDuringRegLink(){
		$this->loadModel('Websitepage');		
		$spDuringRegLink = $this->Websitepage->find('all',array('conditions'=>array('Websitepage.status'=>1,'Websitepage.page_type'=>'D'),'order'=>array('Websitepage.ordering' => 'DESC'),'fields'=>array('page_name','id')));
		$this->set('spDuringRegLink',$spDuringRegLink);	
	}
	
	
  function getPosition() {
    $this->loadModel('Position');
   // return $this->Position->find('list',array('fields'=>array('id','designation')));
    return $this->Position->find('list', array(
				'conditions' => array('Position.company_id'=>$this->Session->read('Company_ID')),				
				'fields' =>array('id','designation'),
				'order'=>'Position.designation',
				'limit'=>'',
				'page'=>''
	));
    }
    
    
  function getAllClientOfSp($sp_id = null)
  {
	$this->loadModel('User');
	return $this->User->find('list', array(
				'conditions' => array('User.sp_id'=>$sp_id,'User.user_type_id'=>3),				
				'fields' =>array('id','fname'),
				'order'=>'User.fname',
				'limit'=>'',
				'page'=>''
	));
  }
    
    

 function getAllReports($sp_id = null)
 {
	$this->loadModel('Report');
	$reportData = $this->Report->find('all');	
	return  $reportData;
 }
 
 function getAllInspectorPermissions()
 {
	$this->loadModel('InspectorPermission');
	$permissionData = $this->InspectorPermission->find('list',array('fields' =>array('id','permission')));
	
	return  $permissionData;
 }
 
 function getAllReportsListing($sp_id = null)
 {
	$this->loadModel('Report');
	return $this->Report->find('list', array(
				'fields' =>array('id','name'),
				'order'=>'Report.name',
				'limit'=>'',
				'page'=>''
				));
 }
/************************************************************************ 
	//function for getting country list
************************************************************************/
  /* function getCountries(){
		$this->loadModel('Country');
		$data=$this->Country->find('list', array(
				'conditions' => "",				
				'fields' =>array('id','name'),
				'order'=>'Country.name',
				'limit'=>'',
				'page'=>''
				));
		return $data;  
	} */
/************************************************************************ 
	//function for city list corresponding to country id
************************************************************************/
  /* function getState($cntid= null){
		$this->loadModel('State');
		$states = $this->State->find('list', array(
			'conditions' => array('State.country_id' => $cntid),
			'order'=>array('State.name ASC'),
			'recursive' => -1
		));
		return $states;  
	}*/
}