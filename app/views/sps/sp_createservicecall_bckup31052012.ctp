<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core','ui/ui.datepicker','jquery-ui-1.8.20.custom.min'));
    echo $this->Html->script(array('timepicker/jquery-1.7.1.min','timepicker/jquery-ui-1.8.16.custom.min','timepicker/jquery-ui-timepicker-addon','timepicker/jquery-ui-sliderAccess'));
    
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.timepic').timepicker({
	ampm: true,
	hourMin: 8,
	hourMax: 16
	}); 
    
});
jQuery(function() {
        jQuery(".calender").datepicker({ dateFormat: 'yy-mm-dd',showOn: 'button', buttonImage: '/img/calendar.png', buttonImageOnly: true
    });});

function getStates(id,value){
    var optArr = $("#"+id).val();
	jQuery("#UserStateId").html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
                url  : "/users/getByCountry/" + value,
                success : function(opt){
			jQuery('#UserStateId').html(opt);
                }
        });
}
 
 function selectHelper(value,report_id,sp_id){
   // var optArr = $("#"+id).val();
	jQuery("#helperselect_"+report_id).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
                url  : "/users/gethelper/"+value+'/'+report_id+'/'+sp_id,
                success : function(opt){
			jQuery("#helperselect_"+report_id).html(opt);
                }
        });
 }
 function showAmountInput(id)
 {
     if($("#"+id).is(":checked")){
	$("#amount_"+id).fadeIn(500);
	$("#frequency_"+id).fadeIn(500);
    }
    else{
	$("#amount_"+id).fadeOut(500);
	$("#frequency_"+id).fadeIn(500);
    }
 }
 
  function majorServices(msId){
    if($("#"+msId).is(":checked")){
       $("#ul_"+msId).fadeIn(500);
       $("#submit_id").fadeIn(500);
       
    }else {
	$("#ul_"+msId).fadeOut(500);
    }     
  }
   function closebox(id)
   {
     $("#ul_"+id).fadeOut(500);
     $("#"+id).attr("checked", false);
   }
</script>
<style>
.error-message{
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: right;
    font-weight: bold;
    position: relative;
    top: 30px;
    white-space: nowrap;
}
input { width:250px; }
/* css for timepicker */
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
</style>
<div class="register-wrap">
<h1 class="main-hdng">Create service call for <?php echo ucfirst($name); ?></h1>
    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'createservicecall'),'id'=>'createservicecall'));
	  //echo $form->input('AssignInspector.s_id.client_id',array('type'=>'hidden','value'=>$cid));
    ?>
	<!--one complete form-->
    <div class="mini-form-wrap"><h2 class="form-top-chks">
	<?php foreach($rData as $rd){ ?>
    	<?php echo $form->input('AssignInspector.s_id.'.$rd['Report']['id'],array('id'=>$rd['Report']['id'],'type'=>'checkbox','label'=>false,'div'=>false,'style'=>'width:10px','hidden'=>false,'value'=>$rd['Report']['id'],'onclick'=>'majorServices(this.id);')); ?><label><?php echo $rd['Report']['name']; ?></label>&nbsp;&nbsp;
	<?php } ?></h2>
        <!--one complete form-->
		<?php
		    $j=1;
		    foreach($rData as $rd){
			$report_id = $rd['Report']['id'];
			$class="class='form-box width-34'";
		?>
	    <div <?php echo $class ?> id="ul_<?php echo $rd['Report']['id'] ?>" style="display:none;">
            <h2 href="#" class="mini-form-hdr"><?php echo $rd['Report']['name']; ?><a href="javascript:void(0);" style="float:right;padding-top:5px;" onclick="closebox(this.id);" id="<?php echo $rd['Report']['id']; ?>"><?php echo $html->image('close.gif'); ?></a></h2>
            <ul class="register-form min-hght502">  
            	<h3>Services</h3>
                <li>
                	<ul class="inner-chkbox-lst">
                    	<li>
                        <p class="innr-chk-lft"></p>
                        <p>&nbsp;<span class="pad-l95" style="padding-left:153px;">Quantity</span><span class="pad-l95" style="padding-left:100px;">Frequency</span></p>
                        </li>
			    <?php $i=1;
				    foreach($rd['Service'] as $data){
					    $option = $this->Common->getServicesInspector($report_id,$sp_id);
					    //$form->input
			    ?>
                        <li>
                        <p class="innr-chk-lft">
			    <?php if($option != "empty"){ ?>
			    <?php
					$data_id = $data['id'];
					echo $form->input('AssignInspector.s_id.'.$rd['Report']['id'].'.'.$i.'.service_id',array('id'=>$data['id'],'type'=>'checkbox','label'=>false,'div'=>false,'style'=>'width:10px','hidden'=>false,'value'=>$data['id'],"onclick"=>"showAmountInput($data_id);"))."<label>".$data['name']."</label>";
					} else {
					echo  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data['name'];
					}
			    ?>
			</p>
			<?php
				if($option != "empty"){
			 ?>
			 <p id="amount_<?php echo $data['id']; ?>" style="display:none; float:left;">
                            <span class="input-field small-fld">
				<?php echo $form->input('AssignInspector.s_id.'.$rd['Report']['id'].'.'.$i.'.quantity',array('type'=>'text','label'=>false,'div'=>false,'hidden'=>false,'value'=>'1','onblur'=>'if(this.value=="") this.value="1"' ,'onfocus'=>'if(this.value=="1") this.value=""')); ?>&nbsp;&nbsp;
				<?php
				    echo $form->select('AssignInspector.s_id.'.$rd['Report']['id'].'.'.$i.'.frequency',$frequency,'',array('id'=>'select_'.$report_id,'legend'=>false,'label'=>false,'empty'=>'--Frequency--',"class"=>"wdth-140 validate[required]",'style'=>'width:124px;'));
				?>
			    </span>
			    
			</p>
			
			    <?php } ?>
                        </li>
			<?php $i++; } ?>
                    </ul>
                </li>
				<h3 style="padding:15px 0px 5px 0;">Schedule</h3>
		<li style="padding-bottom:0;">
                    <p>Schedule Date<span style="font-size:10px; color:#f00;">*</span></p>
                    <p class="pad-l130">From<span style="font-size:10px; color:#f00;">*</span></p>
                    <p class="pad-l96">To<span style="font-size:10px; color:#f00;">*</span></p>
                </li>
		             
                <li>
		    <span class="input-field wdth-150">
			<input type="text" class="calender" value="" name="data[AssignInspector][s_id][<?php echo $report_id; ?>][schedule_date]"/>
		    </span> 
		    <a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>
		    <span class="input-field wdth-120 left"><input type="text" readonly=true class='timepic' value="" name="data[AssignInspector][s_id][<?php echo $report_id; ?>][schedule_from]"/></span>
		    <span class="input-field wdth-120 left no-marr"><input type="text" readonly=true class='timepic'  value="" name="data[AssignInspector][s_id][<?php echo $report_id; ?>][schedule_to]" /></span>               
                </li>
				<h3 style="padding:15px 0px 5px 0;">Qualified Inspector</h3>
		<li>
                    <p>Select Lead Inspector<span style="font-size:10px; color:#f00;">*</span></p>
                    <p style="padding-left:81px">
			<?php
			 $option = $this->Common->getServicesInspector($report_id,$sp_id);
			 echo $form->select('AssignInspector.s_id.'.$report_id.'.lead_inspector',$option,'',array('id'=>'select_'.$report_id,'legend'=>false,'label'=>false,'empty'=>'--Inspector--',"class"=>"wdth-140 validate[required]","onchange"=>"selectHelper(this.value,$report_id,$sp_id)",'style'=>'width:124px;')); ?>
			 <?php echo $html->link($html->image('manager/question.gif'),"javascript:void(0);",array('title'=>'Lead inspector in '.$rd['Report']['name'],'escape'=>false, 'class'=>'help-tip'))?>
		    </p>
                    <p class="pad-l96"></p>
                </li>
		<li>
                    <p>Helper Inspectors</p>
                    
			<?php
			 $helperOption = array();
			 echo $this->Form->input('AssignInspector.s_id.'.$report_id.'.helper_inspector',array('options' =>$helperOption,'id'=>'helperselect_'.$report_id,'class'=>'multi-select','label' => false,'empty'=>'Please Select','style'=>'height:80px;','multiple'=>'multiple'));
			 echo $html->link($html->image('manager/question.gif'),"javascript:void(0);",array('title'=>'Helper inspector in '.$rd['Report']['name'],'escape'=>false, 'class'=>'help-tip'))?>
		    <span style="clear:left; float:right; width:209px; font-style:italic; font-size:10px; padding-top:5px; color:#3D94DC;">Hold down the Ctrl (windows) / Command (Mac) button to select multiple options</span>
                    <p class="pad-l96"></p>
                </li>
            </ul>
        </div>
<?php $j++; } ?>
    <!--one complete form ends-->
    <!--one complete form-->
    <div class="form-box" style="width:90%;display:none;" id="submit_id">
	<ul>
	     <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
            </li>
        </ul>
    </div>
    <!--one complete form ends-->
<?php echo $form->end(); ?>
</div>