<style>
#box {
    border: none;
}
div.nextBtn{
    padding-top: 10px;
}

</style>
<div id="content">
    <div id="box" style="width:700px;padding-left:10px;">      
        <div class="form-box" style="width:90%">        
            <h2 style="font-weight:bold;">Max Site Addresses Limit Reached</h2>
            <div id="content">
                <?php echo nl2br($maxText['MaxLimitAlertText']['desc']);?>
                <div class="nextBtn">
                    <a id="nextbtn" class="blue-btn" href="javascript:void(0);" onclick="redirectPage();"><span>Next</span></a>
                </div>
            </div>
        </div>    
    </div>
</div>
<script type="text/javascript">
    function redirectPage(){
        parent.location.href = '<?php echo BASE_URL?>sp/sps/subscriptionplan';
    }
</script>