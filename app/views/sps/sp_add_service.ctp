<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#addclient").validationEngine();
});
function getStates(id,value){
    var optArr = $("#"+id).val();
	jQuery("#UserStateId").html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
                url  : "/users/getByCountry/" + value,
                success : function(opt){
			jQuery('#UserStateId').html(opt);
                }
        });
}
</script>
<style>
.error-message {
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: right;
    font-weight: bold;
    position: relative;
    top: 30px;
    white-space: nowrap;
}
</style>
<div id="content">
	      <div class='message'><?php echo $this->Session->flash();?></div>
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addService'),'id'=>'addclient')); ?>
              <div id="box" >
                	<h3 id="adduser">Add Service</h3>
                        <br/>
                        <table class="tbl">
			<tr><td width="20%">Select Report<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->select('Service.report_id',$reports,'',array('legend'=>false,'label'=>false,'class'=>'validate[required]','empty'=>'Please Select'));?></td>           
                        </tr>    
                        <tr><td width="20%">Service Name<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->input('Service.name', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?></td>           
                        </tr>   
                  
			<tr><td>&nbsp;</td>
                        <td>
			    <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;')); ?>
                        </td>                       
                        </tr>
			
                      </table>
                </div>
		<?php echo $form->end(); ?>
            </div>