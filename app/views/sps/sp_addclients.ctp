<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('validation/only_num');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#addclient").validationEngine();
    $('#UserPhone1').numeric();
    $('#UserPhone2').numeric();
    $('#UserPhone3').numeric();
    $('#UserZip').numeric();
    $('#SpSiteAddressAvailphone1').numeric();
    $('#SpSiteAddressAvailphone2').numeric();
    $('#SpSiteAddressAvailphone3').numeric();
    
    $('#UserBilltoPhone1').numeric();
    $('#UserBilltoPhone2').numeric();
    $('#UserBilltoPhone3').numeric();
    
    $('#SpSiteAddressSitePhone1').numeric();
    $('#SpSiteAddressSitePhone2').numeric();
    $('#SpSiteAddressSitePhone3').numeric();
    
    WireAutoTab('UserPhone1','UserPhone2',3);
    WireAutoTab('UserPhone2','UserPhone3',3);
    
    WireAutoTab('SpSiteAddressAvailphone1','SpSiteAddressAvailphone2',3);
    WireAutoTab('SpSiteAddressAvailphone2','SpSiteAddressAvailphone3',3);
    
    WireAutoTab('UserBilltoPhone1','UserBilltoPhone2',3);
    WireAutoTab('UserBilltoPhone2','UserBilltoPhone3',3);
    
    WireAutoTab('SpSiteAddressSitePhone1','SpSiteAddressSitePhone2',3);
    WireAutoTab('SpSiteAddressSitePhone2','SpSiteAddressSitePhone3',3);
});
  
  function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxuploaduserpic',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#ProfileImage").val(responseJSON.profile_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/profilepic/'+responseJSON.profile_image+'">');
	    }
            });           
    }        
	window.onload = createUploader;
</script>
<script>
//var states = null;
var states = '';
var cities = '';
    function getStates(id,value,stateid,cityid){
	
    var optArr = document.getElementById(id).value;    
	jQuery('#'+stateid).html("<option value=''>Select</option>");
	jQuery('#'+cityid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByCountry/" + value,
                success : function(data){
		    window.states = data;
		    var selectedOption = '';
		    var select = $('#'+stateid);
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    //$('option', select).remove();
		     
		    $.each(window.states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}

/*Function to fetch cities on change of states created by Manish Kumar On Nov 16, 2012*/
function getCities(id,value,cityid){
	
    var optArr = document.getElementById(id).value;    
	//jQuery('#'+cityid).html("<option value='' selected='selected'>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByState/" + value,
                success : function(resp){
		     var option = "";
		     option += "<option value=''>Select</option>";
    for (i=0; i<resp.length; i++) {
        option += "<option value='"+resp[i].id+"'>"+resp[i].name+"</option>";
    }
    $('#'+cityid).html(option);
                }
        });
}
 /*Function to fetch cities on chnage of states created by Manish Kumar On Nov 16, 2012*/

    function getStates1(id,value){

		    $('#UserCountryId1').val($('#UserCountryId').val())
		    var selectedOption = $('#UserStateId1').val();
		    var select = $('#UserStateId1');
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    $('option', select).remove();
		     
		    $.each(window.states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
    
}

// To Get auto filled site info if it is same as that of company 
function getCompanyData(){
          
   if($('#addlink').is(':checked')){    
	document.getElementById("UserSitePhone1").value= document.getElementById("UserPhone1").value;
	document.getElementById("UserSitePhone2").value= document.getElementById("UserPhone2").value;
	document.getElementById("UserSitePhone3").value= document.getElementById("UserPhone3").value;
	document.getElementById("UserSiteEmail").value= document.getElementById("UserEmail").value;
	document.getElementById("UserSiteAddress").value= document.getElementById("UserAddress").value;
	document.getElementById("UserSiteCity").value= document.getElementById("UserCity").value;
	document.getElementById("UserSiteZip").value= document.getElementById("UserZip").value;
	document.getElementById("UserCountryId1").value= document.getElementById("UserCountryId").value;
	getStates1('UserCountryId1',document.getElementById("UserCountryId").value);	
	document.getElementById("UserStateId1").value= document.getElementById("UserStateId").value;
	
	
  
   }

}
/*Auto Focus code*/
function WireAutoTab(CurrentElementID, NextElementID, FieldLength) {
    //Get a reference to the two elements in the tab sequence.
    var CurrentElement = $('#' + CurrentElementID);
    var NextElement = $('#' + NextElementID);
 
    CurrentElement.keyup(function(e) {
        //Retrieve which key was pressed.
        //var KeyID = (window.event) ? event.keyCode : e.keyCode;
 
        //If the user has filled the textbox to the given length and
        //the user just pressed a number or letter, then move the
        //cursor to the next element in the tab sequence.   
        /*if (CurrentElement.val().length >= FieldLength
            && ((KeyID >= 48 && KeyID <= 90) ||
            (KeyID >= 96 && KeyID <= 105)))*/
	if (CurrentElement.val().length >= FieldLength)
            NextElement.focus();
    });
}
/*Auto focus code*/

function resetBillTo(){
    $("#UserBilltoFname").val('');
    $("#UserBilltoLname").val('');
    $("#UserBilltoPhone1").val('');
    $("#UserBilltoPhone2").val('');
    $("#UserBilltoPhone3").val('');
    
    $("#UserBilltoEmail").val('');
    $("#UserBilltoAddress").val('');
    $("#UserBilltoCountryId").val('233');
    $("#UserBilltoCity").val('');
    $("#UserBilltoZip").val('');
    
    getStates('UserBilltoCountryId','233','UserBilltoStateId','UserBilltoCityId');
}

function resetSite(){
    $("#SpSiteAddressSiteName").val('');
    $("#UserBilltoLname").val('');
    $("#SpSiteAddressSitePhone1").val('');
    $("#SpSiteAddressSitePhone2").val('');
    $("#SpSiteAddressSitePhone3").val('');
    
    $("#SpSiteAddressSiteEmail").val('');
    $("#SpSiteAddressSiteAddress").val('');
    $("#UserSiteCountryId").val('233');
    $("#SpSiteAddressSiteCity").val('');
    $("#SpSiteAddressSiteZip").val('');
    
    getStates('UserSiteCountryId','233','UserSiteStateId','UserSiteCityId');
}

function setBilltoInfo(status){
    if(status==true){
	fname = $("#UserFname").val();
	lname = $("#UserLname").val();
	phone1 = $("#UserPhone1").val();
	phone2 = $("#UserPhone2").val();
	phone3 = $("#UserPhone3").val();
	usrEmail = $("#UserEmail").val();
	usrAddr = $("#UserAddress").val();
	usrCountryId = $("#UserCountryId").val();
	usrStateId = $("#UserStateId").val();
	usrCityId = $("#UserCityId").val();
	usrZip = $("#UserZip").val();
	
	if(fname!=""){
	    $("#UserBilltoFname").val(fname);
	}
	if(lname!=""){
	    $("#UserBilltoLname").val(lname);
	}
	if(phone1!=""){
	    $("#UserBilltoPhone1").val(phone1);
	}
	if(phone2!=""){
	    $("#UserBilltoPhone2").val(phone2);
	}
	if(phone3!=""){
	    $("#UserBilltoPhone3").val(phone3);
	}
	if(usrEmail!=""){
	    $("#UserBilltoEmail").val(usrEmail);
	}
	if(usrAddr!=""){
	    $("#UserBilltoAddress").val(usrAddr);
	}
	if(usrCountryId!=""){
	    $("#UserBilltoCountryId").val(usrCountryId);
	}
	if(usrStateId!=""){
	    stateData = $("#UserStateId").html();
	    $("#UserBilltoStateId").html(stateData);
	    $("#UserBilltoStateId").val(usrStateId);
	}
	if(usrCityId!=""){	    
	    cityData = $("#UserCityId").html();
	    $("#UserBilltoCityId").html(cityData);   
	    $("#UserBilltoCityId").val(usrCityId);
	}
	if(usrZip!=""){
	    $("#UserBilltoZip").val(usrZip);
	}
    }else{
	resetBillTo();
    }
}
function setResponsible1(status){
	if(status==true){
	fname = $("#SpSiteAddressSiteName").val();
	email = $("#SpSiteAddressSiteEmail").val();
	phone1 = $("#SpSiteAddressSitePhone1").val();
	phone2 = $("#SpSiteAddressSitePhone2").val();
	phone3 = $("#SpSiteAddressSitePhone3").val();
	if(fname!=""){
	    $("#SpSiteAddressResponsiblePersonName").val(fname);
	}    
	if(phone1!=""){
	    $("#SpSiteAddressAvailphone1").val(phone1);
	}
	if(phone2!=""){
	    $("#SpSiteAddressAvailphone2").val(phone2);
	}
	if(phone3!=""){
	    $("#SpSiteAddressAvailphone3").val(phone3);
	}
	if(email!=""){
	    $("#SpSiteAddressResponsiblePersonEmail").val(email);
	}
	}
	else{
	resetRes1();
    }
	}
function setResponsible2(status){
	if(status==true){
	fname = $("#UserFname").val();
	email = $("#UserEmail").val();
	phone1 = $("#UserPhone1").val();
	phone2 = $("#UserPhone2").val();
	phone3 = $("#UserPhone3").val();
	if(fname!=""){
	    $("#SpSiteAddressResponsiblePersonName").val(fname);
	}    
	if(phone1!=""){
	    $("#SpSiteAddressAvailphone1").val(phone1);
	}
	if(phone2!=""){
	    $("#SpSiteAddressAvailphone2").val(phone2);
	}
	if(phone3!=""){
	    $("#SpSiteAddressAvailphone3").val(phone3);
	}
	if(email!=""){
	    $("#SpSiteAddressResponsiblePersonEmail").val(email);
	}
	}
	else{
	resetRes1();
    }
	}
function resetRes1(){
	$("#SpSiteAddressResponsiblePersonName").val('');
	$("#SpSiteAddressAvailphone1").val('');
	$("#SpSiteAddressAvailphone2").val('');
	$("#SpSiteAddressAvailphone3").val('');
	$("#SpSiteAddressResponsiblePersonEmail").val('');

}

function setSiteInfo(status){
    if(status==true){
	fname = $("#UserFname").val();
	lname = $("#UserLname").val();
	phone1 = $("#UserPhone1").val();
	phone2 = $("#UserPhone2").val();
	phone3 = $("#UserPhone3").val();
	usrEmail = $("#UserEmail").val();
	usrAddr = $("#UserAddress").val();
	usrCountryId = $("#UserCountryId").val();
	usrStateId = $("#UserStateId").val();
	usrCityId = $("#UserCityId").val();
	usrZip = $("#UserZip").val();
	
	if(fname!=""){
	    $("#SpSiteAddressSiteName").val(fname+' '+lname);
	}    
	if(phone1!=""){
	    $("#SpSiteAddressSitePhone1").val(phone1);
	}
	if(phone2!=""){
	    $("#SpSiteAddressSitePhone2").val(phone2);
	}
	if(phone3!=""){
	    $("#SpSiteAddressSitePhone3").val(phone3);
	}
	if(usrEmail!=""){
	    $("#SpSiteAddressSiteEmail").val(usrEmail);
	}
	if(usrAddr!=""){
	    $("#SpSiteAddressSiteAddress").val(usrAddr);
	}
	if(usrCountryId!=""){
	    $("#UserSiteCountryId").val(usrCountryId);
	}
	if(usrStateId!=""){
	    stateData = $("#UserStateId").html();
	    $("#UserSiteStateId").html(stateData);   
	    $("#UserSiteStateId").val(usrStateId);
	}
	if(usrCityId!=""){
	    cityData = $("#UserCityId").html();
	    $("#UserSiteCityId").html(cityData);   
	    $("#UserSiteCityId").val(usrCityId);
	}
	if(usrZip!=""){
	    $("#SpSiteAddressSiteZip").val(usrZip);
	}
    }else{
	resetSite();
    }
}

</script>
<style>
.error-message{
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: right;
    font-weight: bold;
    position: static;
    top: 30px;
    white-space: nowrap;
}
input { width:250px; }
div.checkbox{
    float: left;
    padding-right: 5px;
}
div.checkbox input,div.checkbox label{
    width: auto;
}
input{
    padding: 6px;
    background: white;
}
</style>
<div class="register-wrap">
	<h1 class="main-hdng">Add Client</h1>
    <?php echo $form->create('', array('onsubmit' => 'return valid();', 'type'=>'POST', 'url'=>array('action'=>'addclients'),'id'=>'addclient')); ?>
    <?php echo $form->input('User.profilepic', array('id'=>'ProfileImage','type'=>'hidden',"div"=>false,"label"=>false));  ?>
	<!--one complete form-->
    <div class="form-box" style="width:46%;float:left">
        <!--<h2>Company/Bill Information</h2>-->
        <h2 style="background:#666; color:white; padding-left:10px; font-weight:bold">Client Information</h2>
        <ul class="register-form" style="min-height: 568px;">
	    <li>
                <label>Company Name<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.client_company_name',array('maxLength'=>'100','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"1" ,'onblur'=>'cname();')); ?></span></p>
				
            </li>
             <p style="color:red;margin-left: 160px;" id="UserClientCompanyNamep"></p> 
            <li>
                <label>First Name<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'',"div"=>false,"label"=>false,"tabindex"=>"2",'onblur'=>'fname();'));  ?></span></p>
            </li>
            <p style="color:red;margin-left: 160px;" id="UserFnamep"></p> 
            <li>
                <label>Last Name</label>
                <p><span class=""><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false,"tabindex"=>"3"));  ?></span></p>
            </li>
            <li>
                <label>Applicable Code Type<span>*</span></label>                
		<?php
		//$codeCount=0;
		foreach($codetype as $type){		    
		    $option[$type['CodeType']['id']]= $type['CodeType']['code_type'];
		    //echo $form->checkbox('User.code_type_id.'.$codeCount,array('value'=>$type['CodeType']['id'],'div'=>false,'label'=>false,'class'=>'validate[minCheckbox[1]]','tabindex'=>"4",'style'=>'width:10px')).'&nbsp;'.$type['CodeType']['code_type'];
		    //$codeCount++;
		}?>
		<?php echo $form->input('User.code_type_id.',array('type' => 'select','label'=>false,'div'=>false,'multiple'=>'checkbox','options'=>$option,"tabindex"=>"4",'onclick'=>'type();'));?>
            </li> 
            <p style="color:red;margin-left: 160px;" id="type"></p>           
            <li>
                <label>Phone<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.phone1', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"6",'onblur'=>'phone();'));  ?>-</span></p>
		<p><span class=""><?php echo $form->input('User.phone2', array('maxLength'=>'3','class'=>'text ',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"7",'onblur'=>'phone();'));  ?>-</span></p>
		<p><span class=""><?php echo $form->input('User.phone3', array('maxLength'=>'4','class'=>'text ',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"8",'onblur'=>'phone();'));  ?></span></p>
	      
		
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
            <p style="color:red;margin-left: 160px;" id="UserPhonep"></p>
            <li>
                <label>Email<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.email', array('maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'',"tabindex"=>"9",'onblur'=>'email();'));  ?></span></p>
            </li>
            <p style="color:red;margin-left: 160px;" id="UserEmailp"></p>
            <li>
                <label>Address<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.address',array('maxLength'=>'100','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"10",'onblur'=>'address();')); ?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserAddressp"></p>
	    <li>
                <label>Country<span>*</span></label>
                <p><span class=""><?php echo $this->Form->select('User.country_id', $country,'233', array('id'=>'UserCountryId','label' => false, 'div' => false, 'class' => 'select_bg','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserStateId','UserCityId');","tabindex"=>"11",'onblur'=>'phone();'));?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserCountryIdp"></p>
	    <li>
                <label>State<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.state_id',$state,'', array('id'=>'UserStateId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"12",'onchange'=>"getCities(this.id,this.value,'UserCityId');",'onblur'=>'state();'));?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserStateIdp"></p>
	    <li>
                <label>City<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.city_id','','', array('id'=>'UserCityId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"13"));?></span></p>
				
            </li>
	    <!--<li>
                <label>City<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.city',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"13")); ?></span></p>
				
            </li>-->
	    <li>
                <label>Zip<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.zip',array('maxLength'=>'50','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"14",'onblur'=>'zip();')); ?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserZipp"></p>
            
	     <li>
                <label>Company Logo/picture</label>
                <p><span class="">
		     <div id="demo"></div>
		    <ul id="separate-list"></ul>
		</span></p>
            </li>
	     <li>
                <label>&nbsp;</label>
                <p><span class="">
		     <div id="uploaded_picture"  style="text-align:left;"></div>
		</span></p>
            </li>
        </ul>
    </div>
	
	<!--one complete form-->
    <div class="form-box" style="width:46%; float:left">        
        <h2 style="background:#666; color:white; padding-left:10px; font-weight:bold">Work/Site Information</h2>
        <ul class="register-form">
	    <li style="background:e2e3e3;">
                <label><?php echo $form->checkbox('User.chkSame',array('id'=>'siteInfo','onchange'=>'setSiteInfo(this.checked);','tabindex'=>'33','style'=>'background:none;width:25px;border:none;'))?></label>
                <p style="padding-top: 7px;">Same as Client Information</p>
            </li>

            <li>
                <label>Site Name<span>*</span></label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.site_name', array('maxLength'=>'50','class'=>'',"div"=>false,"label"=>false,"tabindex"=>"34"));  ?></span></p>
            </li>  
            <p style="color:red;margin-left: 160px;" id="SpSiteAddressSiteNamep"></p>                                  
            <li>
                <label>Phone</label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.site_phone1', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"35"));  ?>-</span></p>
		<p><span class=""><?php echo $form->input('SpSiteAddress.site_phone2', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"36"));  ?>-</span></p>
		<p><span class=""><?php echo $form->input('SpSiteAddress.site_phone3', array('maxLength'=>'4','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"37"));  ?></span></p>
	      
		
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
            <p style="color:red;margin-left: 160px;" id="UserPhonep3"></p>
            <li>
                <label>Email<span>*</span></label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.site_email', array('maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'',"tabindex"=>"38"));  ?></span></p>
            </li>
            <p style="color:red;margin-left: 160px;" id="SpSiteAddressSiteEmailp"></p>
            <li>
                <label>Address<span>*</span></label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.site_address',array('maxLength'=>'100','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"39")); ?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="SpSiteAddressSiteAddressp"></p>
	    <li>
                <label>Country<span>*</span></label>
                <p><span class=""><?php echo $this->Form->select('SpSiteAddress.country_id', $country,'233', array('id'=>'UserSiteCountryId','label' => false, 'div' => false, 'class' => 'select_bg ','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserSiteStateId','UserSiteCityId');","tabindex"=>"40"));?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserSiteCountryIdp"></p>
	    <li>
                <label>State<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('SpSiteAddress.state_id',$state,'', array('id'=>'UserSiteStateId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"41",'onchange'=>"getCities(this.id,this.value,'UserSiteCityId');"));?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserSiteStateIdp"></p>
	    <li>
                <label>City<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('SpSiteAddress.city_id','','', array('id'=>'UserSiteCityId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"42"));?></span></p>
				
            </li>
	    <!--<li>
                <label>City<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_city',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"42")); ?></span></p>
				
            </li>-->
	    <li>
                <label>Zip<span>*</span></label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.site_zip',array('maxLength'=>'50','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"43")); ?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="SpSiteAddressSiteZipp"></p>
	    <li>
                <label>On-site person to contact<span>*</span></label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.site_contact_name',array('maxLength'=>'50','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"44",'onblur'=>'sur_onsite();')); ?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="SpSiteAddressSiteContactNamep"></p>
	    <li>
		<h2 style="padding-left:10px; font-weight:bold">Responsible contact information for scheduling</h2>        
	    </li>
	    <li style="background:e2e3e3;">
                <label>&nbsp;</label>
                <p style="padding-top: 7px;padding-right: 7px;">
<?php echo $form->checkbox('User.chkSame',array('id'=>'siteInfo','onchange'=>'setResponsible1(this.checked);','style'=>'background:none;width:25px;border:none;'))?>
                Same as Worksite</p> 
            
                <p style="padding: 8px;
    background: #3D94DC;
    color: white;
    border-radius: 50%;
    text-align: center;">OR</p>
                <p style="padding-left: 7px;padding-top: 7px;">
<?php echo $form->checkbox('User.chkSame',array('id'=>'siteInfo','onchange'=>'setResponsible2(this.checked);','style'=>'background:none;width:25px;border:none;'))?>
                Same as Client</p>
            </li>
	    <li>
                <label>Name<span>*</span></label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.responsible_person_name',array('type'=>'text','maxLength'=>'60','class'=>'','div'=>false,"label"=>false,"tabindex"=>"15",'onblur'=>'sur_name();')); ?></span></p>
            </li>
            <p style="color:red;margin-left: 160px;" id="SpSiteAddressResponsiblePersonNamep"></p>
	    <li>
                <label>Email<span>*</span></label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.responsible_person_email',array('type'=>'text','maxLength'=>'60','class'=>'','div'=>false,"label"=>false,"tabindex"=>"16",'onblur'=>'sur_email();')); ?></span></p>
            </li>
            <p style="color:red;margin-left: 160px;" id="SpSiteAddressResponsiblePersonEmailp"></p>
            <li>
                <label>Phone Number<span>*</span></label>
                <p><span class=""><?php echo $form->input('SpSiteAddress.availphone1', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"17",'onblur'=>'sur_phone();'));  ?>-</span></p>
		<p><span class=""><?php echo $form->input('SpSiteAddress.availphone2', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"18",'onblur'=>'sur_phone();'));  ?>-</span></p>
		<p><span class=""><?php echo $form->input('SpSiteAddress.availphone3', array('maxLength'=>'4','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"19",'onblur'=>'sur_phone();'));  ?></span></p>
            </li>
            <p style="color:red;margin-left: 160px;" id="UserPhonep4"></p>
        </ul>
    </div>
    <!--one complete form ends-->
	
	
    <!--one complete form ends-->
	<!--one complete form-->
    <!--<div class="form-box" style="width:90%">
        <h2>Site Location</h2>
        <ul class="register-form">
            <li>
                <label>Site Name<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.site_name', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"16"));  ?></span></p>
            </li>
	    <li>
                <label>Site Phone</label>
                <p><span class="input-field">
		<?php echo $form->input('User.site_phone1', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"17"));  ?>-</span></p>
		<p><span class="input-field"><?php echo $form->input('User.site_phone2', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"18"));  ?>-</span></p>
		<p><span class="input-field"><?php echo $form->input('User.site_phone3', array('maxLength'=>'4','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"19"));  ?></span></p>
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
	    <li>
                <label>Site Email<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.site_email',array('maxLength'=>'60','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"20")); ?></span></p>
            </li>
            <li>
                <label>Site Address<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.site_address', array('maxLength'=>'50','class'=>'',"div"=>false,"label"=>false,"tabindex"=>"21"));  ?></span></p>
		<span style="clear:left; float:left; width:252px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 403 Meco Drive, Wilmington, DE, United States</b></span>
            </li>
            <li>
                <label>Site Contact Name<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.site_cname', array('maxLength'=>'20',"div"=>false,"label"=>false,'class'=>'',"tabindex"=>"22"));  ?></span></p>
            </li>            
            <li>
                <label>Site Country<span>*</span></label>
                <p><span class=""><?php echo $this->Form->select('User.site_country_id', $country,'233', array('id'=>'UserCountryId1','label' => false, 'div' => false, 'class' => 'select_bg','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserStateId1','UserCityId1');","tabindex"=>"23"));?></span></p>
				
            </li>
	    <li>
                <label>Site State<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.site_state_id',$state,'', array('id'=>'UserStateId1','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-","tabindex"=>"24",'onchange'=>"getCities(this.id,this.value,'UserCityId1');" ));?></span></p>
				
            </li>
	    <li>
                <label>Site City<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.site_city',array('maxLength'=>'50','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"25")); ?></span></p>
				
            </li>
	    <li>
                <label>Site Zip<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.site_zip',array('maxLength'=>'50','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"26")); ?></span></p>
				
            </li>
	    <li>
		<label>&nbsp;</label>
		
	    </li>
	   
        </ul>
    </div> -->
    <!--one complete form ends-->
    <div style="clear:both; width:100%"></div>
    <!--one complete form-->
    <!--<div class="form-box" style="width:46%; float:left">-->
        <!--<h2>Responsible Personnel contact information</h2>-->
        
    <!--</div>-->
    <!--one complete form ends-->
    
    
    
    <!--one complete form-->
    <div class="form-box" style="width:46%; float:left">        
        <h2 style="background:#666; color:white; padding-left:10px; font-weight:bold">Bill To Information</h2>
        <ul class="register-form">	    
            <li>
                <label><?php echo $form->checkbox('User.chkSame',array('id'=>'billtoInfo','onchange'=>'setBilltoInfo(this.checked)','tabindex'=>'21','style'=>'background:none;width:25px;border:none;'))?></label>
                <p style="padding-top: 7px;">Same as Client Information</p>
            </li>
	    <li>
                <label>First Name<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.billto_fname', array('maxLength'=>'50','class'=>'',"div"=>false,"label"=>false,"tabindex"=>"22"));  ?></span></p>
            </li>
            <p style="color:red;margin-left: 160px;" id="UserBilltoFnamep"></p>
            <li>
                <label>Last Name</label>
                <p><span class=""><?php echo $form->input('User.billto_lname', array('maxLength'=>'50',"div"=>false,"label"=>false,"tabindex"=>"23"));  ?></span></p>
            </li>                        
            <li>
                <label>Phone</label>
                <p><span class=""><?php echo $form->input('User.billto_phone1', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"24"));  ?>-</span></p>
		<p><span class=""><?php echo $form->input('User.billto_phone2', array('maxLength'=>'3','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"25"));  ?>-</span></p>
		<p><span class=""><?php echo $form->input('User.billto_phone3', array('maxLength'=>'4','class'=>'text',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"26"));  ?></span></p>
	      
		
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
            <p style="color:red;margin-left: 160px;" id="UserPhonep2"></p>
            <li>
                <label>Email<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.billto_email', array('maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'',"tabindex"=>"27"));  ?></span></p>
            </li>
	    <p style="color:red;margin-left: 160px;" id="UserBilltoEmailp"></p>
	    <li>
                <label>Address<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.billto_address',array('maxLength'=>'100','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"28")); ?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserBilltoAddressp"></p>
	    <li>
                <label>Country<span>*</span></label>
                <p><span class=""><?php echo $this->Form->select('User.billto_country_id', $country,'233', array('id'=>'UserBilltoCountryId','label' => false, 'div' => false, 'class' => 'select_bg','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserBilltoStateId','UserBilltoCityId');","tabindex"=>"29"));?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserBilltoCountryIdp"></p>
	    <li>
                <label>State<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.billto_state_id',$state,'', array('id'=>'UserBilltoStateId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"30",'onchange'=>"getCities(this.id,this.value,'UserBilltoCityId');"));?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserBilltoStateIdp"></p>
	    <li>
                <label>City<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.billto_city_id','','', array('id'=>'UserBilltoCityId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"31"));?></span></p>
				
            </li>
	    <!--<li>
                <label>City<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.billto_city',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"31")); ?></span></p>
				
            </li>-->
	    <li>
                <label>Zip<span>*</span></label>
                <p><span class=""><?php echo $form->input('User.billto_zip',array('maxLength'=>'50','type'=>'text','class'=>'','div'=>false,"label"=>false,"tabindex"=>"32")); ?></span></p>
				
            </li>
            <p style="color:red;margin-left: 160px;" id="UserBilltoZipp"></p>
             <li>
                <label>&nbsp;</label>
                <p>Are there any additional worksite addresses for the client info or bill to info?</p>
            </li>                                    
            <li>
                <label>&nbsp;</label>
		<?php
		    $options = array('Yes'=>'Yes','No'=>'No');
		    $attributes = array('legend'=>false,'separator'=>'&nbsp;&nbsp;','tabindex'=>'45','style'=>'width:10px');		    
		?>
                <p><?php echo $form->radio('User.additional_sites',$options, $attributes);?></p>
            </li>
        </ul>
    </div>
    <!--one complete form ends-->

    
    <!--one complete form-->
    <div class="form-box" style="clear:both; float:right; margin-right:55px">
	<ul>
	     <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
            </li>
	    <li>
		<span style="clear:left; float:right; width:209px; font-style:italic; font-size:10px; padding-top:5px; color:#3D94DC;">Client credential will sent to client email after successfully added</span>
	    </li>
        </ul>
    </div>
    <!--one complete form ends-->
<?php echo $form->end(); ?>
</div>
<script type="text/javascript">
               <?php $arr = array(); foreach($state as $key => $value){ 
                  $arr[] =  '"'.$key.'":"'.$value.'"';
                } ?>
$(document).ready(function(){
//Declared States as gloabal
  window.states =  {<?php echo implode(",",$arr); ?>}
  jQuery('div.checkbox').children('input').attr("class","");
});
</script>

<script type="text/javascript">



	function valid(){
var cc_name = $("#UserClientCompanyName").val();
var cf_name = $("#UserFname").val();
var cu_code1 = $("#UserCodeTypeId1").is(":checked"); var cu_code2 = $("#UserCodeTypeId2").is(":checked");
var cu_phone1 = $("#UserPhone1").val(); var cu_phone2 = $("#UserPhone2").val(); var cu_phone3 = $("#UserPhone3").val();
var cu_email = $("#UserEmail").val();
var cu_address = $("#UserAddress").val();
var cu_country = $("#UserCountryId").val();
var cu_state = $("#UserStateId").val();
var cu_zip = $("#UserZip").val();
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var token = true;
		if(cc_name == '' || cc_name == null){
    $("#UserClientCompanyName").css("border", "2px solid red");
    $("#UserClientCompanyName").focus();
    $("#UserClientCompanyNamep").text("*Company name is required");
        token = false;
     	}
     	else 
     	{ $("#UserClientCompanyName").css("border", "2px solid green");
     	  $("#UserClientCompanyNamep").text(""); }

     	if(cf_name == '' || cf_name == null){
    $("#UserFname").css("border", "2px solid red");
    $("#UserFnamep").text("*First name is required");
        token = false;
     	}
     	else 
     	{ $("#UserFname").css("border", "2px solid green");
     	  $("#UserFnamep").text(""); }

     	if(cu_code1 != true && cu_code2 != true){
    $("#type").text("*Applicable Code Type is required");
        token = false;
     	}
     	else {
     	  $("#type").text(""); }

    if((cu_phone1 == '' || cu_phone1 == null) || (cu_phone2 == '' || cu_phone2 == null) || (cu_phone3 == '' || cu_phone3 == null)){
    $("#UserPhone1").css("border", "2px solid red");
    $("#UserPhone2").css("border", "2px solid red");
    $("#UserPhone3").css("border", "2px solid red");
    $("#UserPhonep").text("*Phone is required");
        token = false;
     	}
     	else 
     	{ 
    $("#UserPhone1").css("border", "2px solid green");
    $("#UserPhone2").css("border", "2px solid green");
    $("#UserPhone3").css("border", "2px solid green");
     	  $("#UserPhonep").text(""); }

    if(cu_email == '' || !regex.test(cu_email)){
    $("#UserEmail").css("border", "2px solid red");
    $("#UserEmailp").text("*E-mail is required");
    if(cu_email != '' && !regex.test(cu_email)){ $("#UserEmailp").text("*E-mail is not valid"); }
        token = false;
     	}
     	else 
     	{ $("#UserEmail").css("border", "2px solid green");
     	  $("#UserEmailp").text(""); }

    if(cu_address == '' || cu_address == null){
    $("#UserAddress").css("border", "2px solid red");
    $("#UserAddressp").text("*Address is required");
        token = false;
     	}
     	else 
     	{ $("#UserAddress").css("border", "2px solid green");
     	  $("#UserAddressp").text(""); }

    if(cu_country == '' || cu_country == null){
    $("#UserCountryId").css("border", "2px solid red");
    $("#UserCountryIdp").text("*Country is required");
        token = false;
     	}
     	else 
     	{ $("#UserCountryId").css("border", "2px solid green");
     	  $("#UserCountryIdp").text(""); }

    if(cu_state == '' || cu_state == null){
    $("#UserStateId").css("border", "2px solid red");
    $("#UserStateIdp").text("*State is required");
        token = false;
     	}
     	else 
     	{ $("#UserStateId").css("border", "2px solid green");
     	  $("#UserStateIdp").text(""); }

    if(cu_zip == '' || cu_zip == null){
    $("#UserZip").css("border", "2px solid red");
    $("#UserZipp").text("*Zip is required");
        token = false;
     	}
     	else 
     	{ $("#UserZip").css("border", "2px solid green");
     	  $("#UserZipp").text(""); }

 //second form Bill 

var bf_name = $("#UserBilltoFname").val();
var bu_phone1 = $("#UserBilltoPhone1").val(); var bu_phone2 = $("#UserBilltoPhone2").val(); var bu_phone3 = $("#UserBilltoPhone3").val();
var bu_email = $("#UserBilltoEmail").val();
var bu_address = $("#UserBilltoAddress").val();
var bu_country = $("#UserBilltoCountryId").val();
var bu_state = $("#UserBilltoStateId").val();
var bu_zip = $("#UserBilltoZip").val();   	

if(bf_name == '' || bf_name == null){
    $("#UserBilltoFname").css("border", "2px solid red");
    $("#UserBilltoFnamep").text("*First name is required");
        token = false;
     	}
     	else 
     	{ $("#UserBilltoFname").css("border", "2px solid green");
     	  $("#UserBilltoFnamep").text(""); }

if((bu_phone1 == '' || bu_phone1 == null) || (bu_phone2 == '' || bu_phone2 == null) || (bu_phone3 == '' || bu_phone3 == null)){
    $("#UserBilltoPhone1").css("border", "2px solid red");
    $("#UserBilltoPhone2").css("border", "2px solid red");
    $("#UserBilltoPhone3").css("border", "2px solid red");
    $("#UserPhonep2").text("*Phone is required");
        token = false;
     	}
     	else 
     	{ 
    $("#UserBilltoPhone1").css("border", "2px solid green");
    $("#UserBilltoPhone2").css("border", "2px solid green");
    $("#UserBilltoPhone3").css("border", "2px solid green");
     	  $("#UserPhonep2").text(""); }


if(bu_email == '' || !regex.test(bu_email)){
    $("#UserBilltoEmail").css("border", "2px solid red");
    $("#UserBilltoEmailp").text("*E-mail is required");
    if(bu_email != '' && !regex.test(bu_email)){ $("#UserBilltoEmailp").text("*E-mail is not valid"); }
        token = false;
     	}
     	else 
     	{ $("#UserBilltoEmail").css("border", "2px solid green");
     	  $("#UserBilltoEmailp").text(""); }

    if(bu_address == '' || bu_address == null){
    $("#UserBilltoAddress").css("border", "2px solid red");
    $("#UserBilltoAddressp").text("*Address is required");
        token = false;
     	}
     	else 
     	{ $("#UserBilltoAddress").css("border", "2px solid green");
     	  $("#UserBilltoAddressp").text(""); }

    if(bu_country == '' || bu_country == null){
    $("#UserBilltoCountryId").css("border", "2px solid red");
    $("#UserBilltoCountryIdp").text("*Country is required");
        token = false;
     	}
     	else 
     	{ $("#UserBilltoCountryId").css("border", "2px solid green");
     	  $("#UserBilltoCountryIdp").text(""); }

    if(bu_state == '' || bu_state == null){
    $("#UserBilltoStateId").css("border", "2px solid red");
    $("#UserBilltoStateIdp").text("*State is required");
        token = false;
     	}
     	else 
     	{ $("#UserBilltoStateId").css("border", "2px solid green");
     	  $("#UserBilltoStateIdp").text(""); }

    if(bu_zip == '' || bu_zip == null){
    $("#UserBilltoZip").css("border", "2px solid red");
    $("#UserBilltoZipp").text("*Zip is required");
        token = false;
     	}
     	else 
     	{ $("#UserBilltoZip").css("border", "2px solid green");
     	  $("#UserBilltoZipp").text(""); }

//third form work/site     	  

var sf_name = $("#SpSiteAddressSiteName").val();
var su_phone1 = $("#SpSiteAddressSitePhone1").val(); var su_phone2 = $("#SpSiteAddressSitePhone2").val();var su_phone3 = $("#SpSiteAddressSitePhone3").val();
var su_email = $("#SpSiteAddressSiteEmail").val();
var su_address = $("#SpSiteAddressSiteAddress").val();
var su_country = $("#UserSiteCountryId").val();
var su_state = $("#UserSiteStateId").val();
var su_zip = $("#SpSiteAddressSiteZip").val();  	

if(sf_name == '' || sf_name == null){
    $("#SpSiteAddressSiteName").css("border", "2px solid red");
    $("#SpSiteAddressSiteNamep").text("*Site name is required");
        token = false;
     	}
     	else 
     	{ $("#SpSiteAddressSiteName").css("border", "2px solid green");
     	  $("#SpSiteAddressSiteNamep").text(""); }

if((su_phone1 == '' || su_phone1 == null) || (su_phone2 == '' || su_phone2 == null) || (su_phone3 == '' || su_phone3 == null)){
    $("#SpSiteAddressSitePhone1").css("border", "2px solid red");
    $("#SpSiteAddressSitePhone2").css("border", "2px solid red");
    $("#SpSiteAddressSitePhone3").css("border", "2px solid red");
    $("#UserPhonep3").text("*Phone is required");
        token = false;
     	}
     	else 
     	{ 
    $("#SpSiteAddressSitePhone1").css("border", "2px solid green");
    $("#SpSiteAddressSitePhone2").css("border", "2px solid green");
    $("#SpSiteAddressSitePhone3").css("border", "2px solid green");
     	  $("#UserPhonep3").text(""); }


if(su_email == '' || !regex.test(su_email)){
    $("#SpSiteAddressSiteEmail").css("border", "2px solid red");
    $("#SpSiteAddressSiteEmailp").text("*E-mail is required");
    if(su_email != '' && !regex.test(su_email)){ $("#SpSiteAddressSiteEmailp").text("*E-mail is not valid"); }
        token = false;
     	}
     	else 
     	{ $("#SpSiteAddressSiteEmail").css("border", "2px solid green");
     	  $("#SpSiteAddressSiteEmailp").text(""); }

    if(su_address == '' || su_address == null){
    $("#SpSiteAddressSiteAddress").css("border", "2px solid red");
    $("#SpSiteAddressSiteAddressp").text("*Address is required");
        token = false;
     	}
     	else 
     	{ $("#SpSiteAddressSiteAddress").css("border", "2px solid green");
     	  $("#SpSiteAddressSiteAddressp").text(""); }

    if(su_country == '' || su_country == null){
    $("#UserSiteCountryId").css("border", "2px solid red");
    $("#UserSiteCountryIdp").text("*Country is required");
        token = false;
     	}
     	else 
     	{ $("#UserSiteCountryId").css("border", "2px solid green");
     	  $("#UserSiteCountryIdp").text(""); }

    if(su_state == '' || su_state == null){
    $("#UserSiteStateId").css("border", "2px solid red");
    $("#UserSiteStateIdp").text("*State is required");
        token = false;
     	}
     	else 
     	{ $("#UserSiteStateId").css("border", "2px solid green");
     	  $("#UserSiteStateIdp").text(""); }

    if(su_zip == '' || su_zip == null){
    $("#SpSiteAddressSiteZip").css("border", "2px solid red");
    $("#SpSiteAddressSiteZipp").text("*Zip is required");
        token = false;
     	}
     	else 
     	{ $("#SpSiteAddressSiteZip").css("border", "2px solid green");
     	  $("#SpSiteAddressSiteZipp").text(""); }

//extra option
 var su_onsite = $("#SpSiteAddressSiteContactName").val(); 
 var sur_name = $("#SpSiteAddressResponsiblePersonName").val();
 var sur_email = $("#SpSiteAddressResponsiblePersonEmail").val();
 var sur_phone1 = $("#SpSiteAddressAvailphone1").val(); var sur_phone2 = $("#SpSiteAddressAvailphone2").val();var sur_phone3 = $("#SpSiteAddressAvailphone3").val();
if(su_onsite == '' || su_onsite == null){
    $("#SpSiteAddressSiteContactName").css("border", "2px solid red");
    $("#SpSiteAddressSiteContactNamep").text("*On-site person to contact is required");
        token = false;
     	}
     	else 
     	{ $("#SpSiteAddressSiteContactName").css("border", "2px solid green");
     	  $("#SpSiteAddressSiteContactNamep").text(""); }

    
if(sur_name == '' || sur_name == null){
    $("#SpSiteAddressResponsiblePersonName").css("border", "2px solid red");
    $("#SpSiteAddressResponsiblePersonNamep").text("*Name is required");
        token = false;
     	}
     	else 
     	{ $("#SpSiteAddressResponsiblePersonName").css("border", "2px solid green");
     	  $("#SpSiteAddressResponsiblePersonNamep").text(""); }
if(sur_email == '' || !regex.test(sur_email)){
    $("#SpSiteAddressResponsiblePersonEmail").css("border", "2px solid red");
    $("#SpSiteAddressResponsiblePersonEmailp").text("*E-mail is required");
    if(sur_email != '' && !regex.test(sur_email)){ $("#SpSiteAddressResponsiblePersonEmailp").text("*E-mail is not valid"); }
        token = false;
     	}
     	else 
     	{ $("#SpSiteAddressResponsiblePersonEmail").css("border", "2px solid green");
     	  $("#SpSiteAddressResponsiblePersonEmailp").text(""); }
if((sur_phone1 == '' || sur_phone1 == null) || (sur_phone2 == '' || sur_phone2 == null) || (sur_phone3 == '' || sur_phone3 == null)){
    $("#SpSiteAddressAvailphone1").css("border", "2px solid red");
    $("#SpSiteAddressAvailphone2").css("border", "2px solid red");
    $("#SpSiteAddressAvailphone3").css("border", "2px solid red");
    $("#UserPhonep4").text("*Phone is required");
        token = false;
     	}
     	else 
     	{ 
    $("#SpSiteAddressAvailphone1").css("border", "2px solid green");
    $("#SpSiteAddressAvailphone2").css("border", "2px solid green");
    $("#SpSiteAddressAvailphone3").css("border", "2px solid green");
     	  $("#UserPhonep4").text(""); }

return token;
	}
</script>

<script type="text/javascript">
function cname()
{if($('#UserClientCompanyName').val() != ''){
$("#UserClientCompanyName").css("border", "2px solid green");$("#UserClientCompanyNamep").text("");
}else{ $("#UserClientCompanyName").css("border", "2px solid red");
        $("#UserClientCompanyNamep").text("*Company Name is required"); }
}

function fname()
{if($('#UserFname').val() != ''){
$("#UserFname").css("border", "2px solid green");$("#UserFnamep").text("");
}else{ $("#UserFname").css("border", "2px solid red");
        $("#UserFnamep").text("*First name is required"); }
}

function type()
{if($("#UserCodeTypeId1").is(":checked") != true && $("#UserCodeTypeId2").is(":checked") != true){
		$("#type").text("*Applicable Code Type is required");
}else{ $("#type").text(""); }
}

function phone()
{if(($('#UserPhone1').val() != '') && ($('#UserPhone2').val() != '') && ($('#UserPhone3').val() != '')){
$("#UserPhone1").css("border", "2px solid green");$("#UserPhone2").css("border", "2px solid green");$("#UserPhone3").css("border", "2px solid green");
$("#UserPhonep").text("");
}else{ 
            { $("#UserPhone1").css("border", "2px solid red"); }
        
            { $("#UserPhone2").css("border", "2px solid red"); }
        
            { $("#UserPhone3").css("border", "2px solid red"); }
        $("#UserPhonep").text( "*Phone number is required" ); }
}

function email()
{
    var email = $("#UserEmail").val();
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)){
        $("#UserEmail").css("border", "2px solid red");
        $("#UserEmailp").text("*Email is not valid");

}
else{ $("#UserEmail").css("border", "2px solid green");$("#UserEmailp").text(""); }
}

function address()
{if($('#UserAddress').val() != ''){
$("#UserAddress").css("border", "2px solid green");$("#UserAddressp").text("");
}else{ $("#UserAddress").css("border", "2px solid red");
        $("#UserAddressp").text("*Address is required"); }
}

function state()
{if($('#UserStateId').val() != ''){
$("#UserStateId").css("border", "2px solid green");$("#UserStateIdp").text("");
}else{ $("#UserStateId").css("border", "2px solid red");
        $("#UserStateIdp").text("*State is required"); }
}

function zip()
{if($('#UserZip').val() != ''){
$("#UserZip").css("border", "2px solid green");$("#UserZipp").text("");
}else{ $("#UserZip").css("border", "2px solid red");
        $("#UserZipp").text("*Zip is required"); }
}

function sur_onsite()
{if($('#SpSiteAddressSiteContactName').val() != ''){
$("#SpSiteAddressSiteContactName").css("border", "2px solid green");$("#SpSiteAddressSiteContactNamep").text("");
}else{ $("#SpSiteAddressSiteContactName").css("border", "2px solid red");
        $("#SpSiteAddressSiteContactNamep").text("*Zip is required"); }
}

function sur_name()
{if($('#SpSiteAddressResponsiblePersonName').val() != ''){
$("#SpSiteAddressResponsiblePersonName").css("border", "2px solid green");$("#SpSiteAddressResponsiblePersonNamep").text("");
}else{ $("#SpSiteAddressResponsiblePersonName").css("border", "2px solid red");
        $("#SpSiteAddressResponsiblePersonNamep").text("*Zip is required"); }
}

function sur_email()
{
    var email = $("#SpSiteAddressResponsiblePersonEmail").val();
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)){
        $("#SpSiteAddressResponsiblePersonEmail").css("border", "2px solid red");
        $("#SpSiteAddressResponsiblePersonEmailp").text("*Email is not valid");

}
else{ $("#SpSiteAddressResponsiblePersonEmail").css("border", "2px solid green");$("#SpSiteAddressResponsiblePersonEmailp").text(""); }
}

function sur_phone()
{if(($('#SpSiteAddressAvailphone1').val() != '') && ($('#SpSiteAddressAvailphone2').val() != '') && ($('#SpSiteAddressAvailphone3').val() != '')){
$("#SpSiteAddressAvailphone1").css("border", "2px solid green");$("#SpSiteAddressAvailphone2").css("border", "2px solid green");$("#SpSiteAddressAvailphone3").css("border", "2px solid green");
$("#UserPhonep4").text("");
}else{ 
            { $("#SpSiteAddressAvailphone1").css("border", "2px solid red"); }
        
            { $("#SpSiteAddressAvailphone2").css("border", "2px solid red"); }
        
            { $("#SpSiteAddressAvailphone3").css("border", "2px solid red"); }
        $("#UserPhonep4").text( "*Phone number is required" ); }
}
</script>