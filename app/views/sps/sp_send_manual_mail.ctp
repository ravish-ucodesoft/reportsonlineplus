<?php
    echo $this->Html->script('jquery.1.6.1.min');
  // echo $this->Html->script('jquery-ui.min.js'); 
    echo $this->Html->css('validationEngine.jquery');    
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');    
    echo $this->Html->script('validation/only_num');
    echo $this->Html->script('fckeditor'); 
?>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#sendMail").validationEngine();    
});

function setAll(obj){
    if(jQuery(obj).is(':checked')){
        jQuery(obj).parent('p').parent('li').next('li').find('input').attr('checked',true);
    }else{
        jQuery(obj).parent('p').parent('li').next('li').find('input').attr('checked',false);
    }
}
</script>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
    <?php echo $this->Form->create('', array('url'=>array('controller'=>'sps','action' => 'sendManualMail'),'id'=>'sendMail','name'=>'sendMail') );?>
    <section class="form-box" style="width:100%; margin: 0">
        <h2><strong>Resend Manual Mail</strong></h2>
        <ul class="register-form">                    
            <li>
                <label>&nbsp;</label>
                <p style="padding-left:20px;">
                    <?php                        
                        echo $this->Form->input('SendMail.selectall', array(
                            'type' => 'checkbox',                            
                            'label' => 'Select All',
                            'div'=>false,
                            'onchange'=>'setAll(this);'
                            ));
                    ?>
                </p>
            </li>
            <li>
                <label>Select Clients</label>
                <p><span class="input-field">
                    <?php
                        //echo $form->input('SendMail.to', array('type'=>'checkbox','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));
                        echo $this->Form->input('SendMail.to', array(
                            'type' => 'select',
                            'multiple' => 'checkbox',
                            'label' => false,
                            'div'=>false,                            
                            'options' => $optionsData
                            ));


                    ?>
                </span></p>
            </li>
            <li>
                <label>Subject</label>
                <p><span class="input-field"><?php echo $form->input('SendMail.subject', array('class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?></span></p>
            </li>
            <li>
                <label>Message</label>
                <p><span class="">
                    <?php echo $form->textarea('SendMail.message', array("div"=>false,"label"=>false,"class"=>"validate[required]",'rows' => 10,'cols' =>23));?>
                    <?php echo $fck->load('SendMailMessage');?>
                </span></p>
            </li>                    
            <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>                       
            </li>
        </ul>
    </section>
    <?php echo $form->end(); ?>
    <br/>   
</div>