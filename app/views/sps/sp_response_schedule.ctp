<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<style>
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#3E4061}
.normal{
    background-color: #e8e8e8;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
  	text-align: center;
  	font-weight:bold;
}
.normal td {
    background-color: #e8e8e8;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
  	text-align: center;
  	font-weight:bold;
}
.highlight td:hover { background:#fff; }
.highlight td {
    background-color: #e8e8e8;
    border: 1px solid #c6c6c6;
    color: #000000;
    cursor: pointer;
    font-size: 11px;
    text-align: center;
	  font-weight:bold;
}

#calbtn {background:url("../../../img/newdesign_img/nav_bg.png") 0 -7px #808080; }
#calbtn p { color:#fff}
.intp{font-size:9px;color:#2B92DD;}
#schw{height:80px; text-align:left; width:100%; margin:2px;border:1px solid #FFF; color:#000;}
.viewmode {font-weight:bold; text-align:left; font-size:14px; color:grey; float:left; margin-right:10px;}

.butn {float:right;padding-right:20px;margin-bottom:10px;}
.butn a { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
.butn a:hover { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<script type="text/javascript">
			$(function(){
				//Accordion
			    $("#accordion").accordion({ header: "h3"});
				$("#accordionClient").accordion({ header: "h3"});
			    jQuery(".ajax").colorbox();
			    $('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); },
					function() { $(this).removeClass('ui-state-hover'); }
			    );
			
				$('.boxtd').hover(
					function(){
						$(this).find('.createcall').show();
					},
					function(){
						$(this).find('.createcall').hide();
					});
				});


 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
		</script>
    <style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			
    </style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box" >
        <h3 id="adduser">These are the Service Calls that the client wants to rescheduled as follows:</h3>
        <br/>	
        <!-- AccordianView div ID Start-->
        <div id="accordingView">		
            <table class="tbl">                        
		<tr>
                    <td width="90%" align="left">
			<div id="accordion">
			<?php if(!empty($data))
			{
                            foreach($data as $key=>$res):?>
                                <div>
                                    <?php
                                    $getNoOfCompletedReportsOfserviceCall=$this->Common->getNoOfCompletedReportsOfserviceCall($res['ServiceCall']['id']);
                                    $getNoOfPendingReportsOfserviceCall=$this->Common->getNoOfPendingReportsOfserviceCall($res['ServiceCall']['id']);
                                    $getTotalReportsOfserviceCall=$this->Common->getTotalReportsOfserviceCall($res['ServiceCall']['id']);
                                    $notstarted=$getTotalReportsOfserviceCall-$getNoOfCompletedReportsOfserviceCall-$getNoOfPendingReportsOfserviceCall;
                                    ?>
				    <h3><a href="#"><?php echo date('m/d/Y H:i:s',strtotime($res['ServiceCall']['created'])); ?></a></h3>
				    <table class="inner" cellpadding="0" cellspacing="0">
                                        <tr><td colspan="6" style="background-color:#817679;color:#FFF;font-size:14px;"><strong><?php echo $this->Common->getClientName($res['ServiceCall']['client_id']);?> (Company:<?php echo $this->Common->getClientCompanyName($res['ServiceCall']['client_id']);?>)</strong></td></tr>
                                        <tr>
					    <th width="15%" style="background-color:#2B92DD;color:#FFF">Report</th>
					    <th width="15%" style="background-color:#2B92DD;color:#FFF">Lead Inspector</th>
					    <th width="20%" style="background-color:#2B92DD;color:#FFF">Helper</th>
					    <th width="20%" style="background-color:#2B92DD;color:#FFF" align="left">Sch Date(M/D/Y)</th>
					    <!--<th width="10%" style="background-color:#2B92DD;color:#FFF">Status</th>
					    <th width="20%" style="background-color:#2B92DD;color:#FFF">&nbsp;</th>-->
					</tr>				
					<?php foreach($res['Schedule'] as $key1=>$schedule):?>
					<tr style="background-color:#e2eaed;">
                                            <td><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
                                            <td><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
                                            <td><?php $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
                                            echo $this->Common->getHelperName($helper_ins_arr); ?></td>
                                            <td>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:ia', $schedule['schedule_from_1']).'-'.$time->format('g:ia', $schedule['schedule_to_1']); ?>
                                                <br/>
                                                    <?php if(!empty($schedule['schedule_date_2'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:ia', $schedule['schedule_from_2']).'-'.$time->format('g:ia', $schedule['schedule_to_2'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_3'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:ia', $schedule['schedule_from_3']).'-'.$time->format('g:ia', $schedule['schedule_to_3'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_4'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:ia', $schedule['schedule_from_4']).'-'.$time->format('g:ia', $schedule['schedule_to_4'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_5'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:ia', $schedule['schedule_from_5']).'-'.$time->format('g:ia', $schedule['schedule_to_5'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_6'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:ia', $schedule['schedule_from_6']).'-'.$time->format('g:ia', $schedule['schedule_to_6'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_7'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:ia', $schedule['schedule_from_7']).'-'.$time->format('g:ia', $schedule['schedule_to_7'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_8'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:ia', $schedule['schedule_from_8']).'-'.$time->format('g:ia', $schedule['schedule_to_8'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_9'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:ia', $schedule['schedule_from_9']).'-'.$time->format('g:ia', $schedule['schedule_to_9'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_10'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:ia', $schedule['schedule_from_10']).'-'.$time->format('g:ia', $schedule['schedule_to_10'])?>
                                                    <?php } ?>
                                            </td>
                                            <!--<td align='left'>--><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
                                                <?php
                                                 //echo $schedule['report_id']."--".$res['ServiceCall']['id']."---".$res['ServiceCall']['client_id']."--".$res['ServiceCall']['sp_id'];
                                                      $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
                                                      $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
                                                     if($getCompleted != "" && $getstarted == 1){
                                                        //echo "<a href=''></a>";
                                                         ?>
                                                        <!--<a class="linktoreport" href="/sp/reports/<?php //echo $this->Common->getReportLinkName($schedule['report_id']);?>?reportID=<?php //echo $schedule['report_id']; ?>&clientID=<?php //echo $res['ServiceCall']['client_id']; ?>&spID=<?php //echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php //echo $res['ServiceCall']['id']; ?>"><blink>Completed on <?php //echo date('m/d/Y',strtotime($getCompleted)); ?></blink></a>-->
                                                     <?php
                                                    }
                                                     /*if($getstarted == 0 && $getCompleted == ""){
                                                        echo "<blink>Not Started Yet</blink>";
                                                      }
                                                      else if($getstarted == 1 && $getCompleted == ""){
                                                        echo "<blink>Pending</blink>";
                                                      }*/
                                                ?>
                                            <!--</td>-->
                                            <!--<td valign="top">
                                                <a class="ajax" title="Log History" href="/sp/sps/viewLog?reportID=<?php //echo $schedule['report_id']; ?>&clientID=<?php //echo $res['ServiceCall']['client_id']; ?>&spID=<?php //echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php //echo $res['ServiceCall']['id']; ?>"><?php //echo $html->image('icon_log.png');?></a>
                                               <?php //$linkname=$this->Common->getReportViewLink($schedule['report_id']); ?>
                                                <a onclick='PopupCenter("/sp/reports/<?php //echo $linkname;?>?reportID=<?php //echo $schedule['report_id']; ?>&clientID=<?php //echo $res['ServiceCall']['client_id']; ?>&spID=<?php //echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php //echo $res['ServiceCall']['id']; ?>","","1000","800");' id="reviewanswer" href="javascript:void(0);"><?php //echo $html->image('icon_preview.png',array('title'=>'Review the Report'));?></a>
                                                <a class="ajax" title="Would you like to change the inspectors ?" href="/sp/schedules/changeInspector?reportID=<?php //echo $schedule['report_id']; ?>&clientID=<?php //echo $res['ServiceCall']['client_id']; ?>&spID=<?php //echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php //echo $res['ServiceCall']['id']; ?>">Change Inspector</a>
                                            </td>-->
					</tr>
					<tr>
                                            <td colspan="5">
                                                <div class="service-hdline">Services Taken</div>
                                                <ul class="services-tkn"><li>
                                                <?php $i=0;$j=1;
                                                for($i=0;$i<count($schedule['ScheduleService']);$i++)
                                                {
                                                        echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
                                                <?php if($j%4==0){
                                                        echo '</li></ul><ul class="services-tkn"><li>';
                                                }
                                                else{
                                                        echo '</li><li>';
                                                }
                                                 $j++;
                                                }
                                                ?>		
					    </td>
                                        </tr>
                                    <?php endforeach;?>
				</table>				
                            </div>
			<?php endforeach;
			}else{
                            echo "No Record Found";
			}
			?>				
                        </div> <!-- Accordian div closed-->
                    </td>
                </tr>
            </table>
        </div> <!-- AccordianView div ID closed-->		
    </div>
</div>