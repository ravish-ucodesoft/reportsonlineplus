<style type="text/css">
div.showThanksMsg{ width: 100%; font-size: 13px; text-align: center; padding-top: 100px; min-height: 300px;}
div.links{ padding: 10px;}
div.links a{ text-decoration: underline; padding: 0 10px;}
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box">
	<h3 id="adduser"><?php echo $userData['User']['client_company_name'].' ( '.$userData['User']['fname'].' '.$userData['User']['lname'].' ) ';?> Successfully Added.</h3>
    </div>
    <div class="showThanksMsg">
        Client is Successfully Created.                    
        <?php $cid=$userData['User']['id'];?>
        <div class="links">
            <?php echo $html->link('Add a new client',array('controller'=>'sps','action'=>'addclients'));?>
            <?php echo $html->link('Add a new site address for this client',array('controller'=>'sps','action'=>'addSiteAddress',base64_encode($cid)));?>
            <?php echo $html->link('Set up an inspection for the client',array('controller'=>'sps','action'=>'createservicecall',base64_encode($cid)));?>
            <?php echo $html->link('Return to Dashboard',array('controller'=>'sps','action'=>'dashboard'));?>
        </div>
    </div>
</div>    