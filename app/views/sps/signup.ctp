            <!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	
                    <!-- Inner Left Starts -->
                   
                    <!-- Inner Left Ends -->
                    
                    <!-- Inner Right Starts -->
                    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'signup'))); ?>
                    <div class="inner-right" style="width:600px">
                    	<h2>User Registration Form</h2>
                        <div class="content inner-form" style="width:600px">
                        	<h4 class="form-head">Your Details</h4>
                            <div class="form" style="width:600px">
                            	<label>First Name :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Owner.fname', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Owner.fname'); ?></span><br />
                                <label>Last Name :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Owner.lname', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Owner.lname'); ?></span><br />
                               
                                <label>Email :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Owner.email', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Owner.email'); ?></span><br />
                                 <label>Confirm Email :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Owner.cemail', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Owner.cemail'); ?></span><br />
                            </div>
                            <h4 class="form-head" >Your Company Details</h4>
                            <div class="form" style="width:600px">
                            	<label>Company Name :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Company.name', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Company.name'); ?></span><br />
                                <label>Address :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Company.address', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                
                                </div><span class="system negative" id="err1"><?php echo $form->error('Company.address'); ?></span><br />
                                <label>City :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Company.city', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Company.city'); ?></span><br />
                                <label>State :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Company.state', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Company.state'); ?></span><br />
                                <label>Zip :</label>
                                <div class="input-box">
                                <span><?php echo $form->text('Company.zip', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Company.zip'); ?></span><br />
                                <label>Phone :</label>
                                <div class="input-box">
                                	<span><?php echo $form->text('Company.phone', array('maxLength'=>'50','class'=>'text'));  ?></span>
                                	
                                </div><span class="system negative" id="err1"><?php echo $form->error('Company.phone'); ?></span><br />
                                </div><br>
                                <h4 class="form-head">Your Company Details</h4>
                                <div class="form" style="width:600px">
                                    <label>CC Type :</label>
                                    <?php
                                      $option=array('Visa'=>'Visa',
                                                    'Mastercard'=>'Mastercard',
                                                    'AmericanExpress'=>'AmericanExpress',
                                                    'DiscoverCard'=>'DiscoverCard'
                                                  );
                                      echo $form->input('Company.cardtype', array('options' =>$option,'label'=>false));
                                    ?>
								                    <br />
                                    <label>CC Number :</label>
                                    <div class="input-box">
                                        	<span><?php echo $form->text('Company.cardnumber', array('maxLength'=>'16','class'=>'text'));  ?></span>
                                
                                    </div>	<span class="system negative" id="err1"><?php echo $form->error('Company.cardnumber'); ?></span><br />
                                    <label>CCV :</label>
                                    <div class="input-box">
                                       	<span><?php echo $form->text('Company.cardcvnumber', array('maxLength'=>'4','class'=>'text'));  ?></span>
                                	
                                    </div><span class="system negative" id="err1"><?php echo $form->error('Company.cardcvnumber'); ?></span><br />
                                    
                                    <label>Subscription Charges :</label>
                                    <div >
                                       	
                                         <table width="45%" cellspacing="2" cellpadding="2" style="border:1px solid grey;">
                                         <tr style="background-color:#818285;color:#FFFFFF">                          															
                          								<th width='20%'>Employee</th>
                          								<th width='40%'>Monthly Charges</th>                          								
                                          <th width='40%'>Yearly Charges</th>                          							  
                          							</tr>
                                         <?php 
                                       	
                                         foreach($SubscriptionData as $subscription):
                                         
                                        $sid=$subscription['SubscriptionDuration']['id']; 
                                      
  
                                        if($sid==$this->data['Owner']['subscription_duration_id'])
                                        {
                                        ?>
                                        <tr class="first">
                                                                                 							
                        								 <td align="center">
                                         <?php echo $subscription['SubscriptionDuration']['min_allowed_employees'].'-'.$subscription['SubscriptionDuration']['max_allowed_employees']; ?>
                                         </td>
                                         <td align="center">
                                         
                                         <?php
                  if( $this->data['Owner']['subscription_duration_plan']=='Monthly') {
                                         echo $form->radio('Owner.subscription_duration_monthly',array('value'=>''),array('onclick'=>'setFlg("Monthly",'.$sid.');','legend'=>false,'div'=>false,'label'=>false,'checked'=>true));?><?php echo "$ ".$subscription['SubscriptionDuration']['monthlycharges']; 
                                         }
                                         else
                                         {
                                         echo $form->radio('Owner.subscription_duration_monthly',array('value'=>''),array('onclick'=>'setFlg("Monthly",'.$sid.');','legend'=>false,'div'=>false,'label'=>false));?><?php echo "$ ".$subscription['SubscriptionDuration']['monthlycharges'];
                                         }
                                         
                                         ?>
                                         </td>
                        							   <td align="center">
                                         <?php
                                          if($this->data['Owner']['subscription_duration_plan']=='Yearly') {
                                          echo $form->radio('Owner.subscription_duration_monthly',array('value'=>''),array('onclick'=>'setFlg("Yearly",'.$sid.');','legend'=>false,'div'=>false,'label'=>false,'checked'=>true));?><?php echo "$ ".$subscription['SubscriptionDuration']['1_year']; 
                                          } else {
                                          echo $form->radio('Owner.subscription_duration_monthly',array('value'=>''),array('onclick'=>'setFlg("Yearly",'.$sid.');','legend'=>false,'div'=>false,'label'=>false));?><?php echo "$ ".$subscription['SubscriptionDuration']['1_year'];                                          
                                          }
                                          
                                          ?>
                                         </td>
                                         </tr>
                                        
                                        <?php
                                        } else {
                                        ?>
                                        
                                         <tr class="first">	
                                         							
                        								 <td align="center">
                                         <?php echo $subscription['SubscriptionDuration']['min_allowed_employees'].'-'.$subscription['SubscriptionDuration']['max_allowed_employees']; ?>
                                         </td>
                                         <td align="center">
                                         <?php echo $form->radio('Owner.subscription_duration_monthly',array('value'=>''),array('onclick'=>'setFlg("Monthly",'.$sid.');','legend'=>false,'div'=>false,'label'=>false));?><?php echo "$ ".$subscription['SubscriptionDuration']['monthlycharges']; ?>
                                         </td>
                        							   <td align="center">
                                         <?php echo $form->radio('Owner.subscription_duration_monthly',array('value'=>''),array('onclick'=>'setFlg("Yearly",'.$sid.');','legend'=>false,'div'=>false,'label'=>false));?><?php echo "$ ".$subscription['SubscriptionDuration']['1_year']; ?>
                                         </td>
                                         </tr>
                                         <?php
                                       	 }
                                       	endforeach;
                                       	 echo $form->input("Owner.subscription_duration_plan",array("id"=>"flg",'type'=>'hidden'));
                                         echo $form->input("Owner.subscription_duration_id",array("id"=>"SubId",'type'=>'hidden'));
   
                                       	 
                                         ?>
                                         </table>
                                         <?php
                                         //echo $form->text('Owner.amount', array('maxLength'=>'4','class'=>'text'));  ?>
                                	
                                    </div><span class="system negative" id="err1" style="float:right;"><?php echo $form->error('Owner.subscription_duration_id'); ?></span><br />
                                     
                                     
                                     <!--<label>Amount :</label>
                                    <div class="input-box">
                                       	<span><?php echo $form->text('Owner.amount', array('maxLength'=>'4','class'=>'text'));  ?></span>
                                	
                                    </div><span class="system negative" id="err1"><?php echo $form->error('Owner.amount'); ?></span><br />
                                    -->
                                    
                                    <label>Expiry Year :</label>
                                    <?php
                                      $year=array();
                                      $i=date('Y');
                                      while($i<(date('Y')+7))
                                      {
                                         $year[$i]=$i;
                                         $i++; 
                                      }
                                      $month=array();
                                      $i=1;
                                      while($i<13)
                                      {
                                         $month[$i]=$i;
                                         $i++; 
                                      } 
                                      echo $form->input('Company.month', array('options' =>$month,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0'));
                                      echo $form->input('Company.year', array('options' =>$year,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0'));
                                    ?>
                                 
                            </div>
                            
                            <div class="btn-sec">
                            <?php
                            echo $form->submit('signup-btn.gif');
                            ?>
                            
                           </div>
                        </div>
                    </div>
                    	<?php echo $form->end(); ?>
                    	<?php  echo $this->element('right_section_front'); ?>
                    <!-- Inner Right Ends -->
                                                            
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends -->
            <script language="javascript">
            function setFlg(obj,val)
            {
              document.getElementById("flg").value=obj;
              document.getElementById("SubId").value=val;
            }
           
            </script>