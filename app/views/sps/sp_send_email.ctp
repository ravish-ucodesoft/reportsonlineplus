<?php 
    echo $this->Html->script('jquery.1.6.1.min.js');     
?>

<?php echo $this->Html->script(array('validation/jquery.validate','validation/jquery.validation.functions')); // FOR validations ?>
<script type='text/Javascript'>
jQuery(document).ready(function() {
    jQuery('#composeMszForm').validate();
});
    function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
</script>

<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;     
}
#box {
    border: none;
}
.error{
color:red;
}

</style>
<div id="content">
     <div id="box" style="width:700px;padding-left:10px;">
      <?php echo $form->create('',array('controller'=>'sps','action' => 'sendEmail','method'=>'post','id'=>'composeMszForm','name'=>'ListForm'));
                echo $form->input('Message.receiver_id',array('type' => 'hidden','value'=>$clientId));	             
                echo $form->input('Message.sender_id',array('type' => 'hidden','value'=>$session->read('Spid'))); 
      ?>
     <div class="form-box" style="width:90%">
     <?php $client_name = $this->Common->getclientName($clientId); ?>
        <h2 style="font-weight:bold;">Send Email To <?php echo $client_name; ?></h2>
        <ul class="register-form">
        <li>
			    <label>To:<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('Message.message_to',array('size'=>'45','maxlength'=>'100','readonly'=>true,'div'=>false,'label'=>false,'value'=>$data['User']['email'],'class'=>'required')); ?></span></p>
			</li>
        <li>
			    <label>Email Subject:<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('Message.message_subject',array('size'=>'45','maxlength'=>'100','div'=>false,'label'=>false,'value'=>'','class'=>'required')); ?></span></p>
			</li>
			<li>
			    <label>Email Body:<span>*</span></label>
			    <p><span class=""><?php echo $form->input('Message.message_body',array('type'=>'textarea','rows'=>7,'cols'=>35,'label'=>false,'div'=>false,'id'=>'ContentMessage','class'=>'required','onKeyUp' =>"return limit('ContentMessage',2000);")); ?></span></p>
			
      </li>	
      <li>
			<label>&nbsp;</label>
			   <div id="essay_Error" class='error'></div>
        <div>
        <small>&nbsp;<label id='limitCounter'>0</label>&nbsp;characters entered&nbsp;|&nbsp;<label id='limitCounterLeft'>2000</label>&nbsp;characters remaining</small>
        </div>   
			</li>	
			<li>
			<label>&nbsp;</label>
			    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Send',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>   
			</li>
        </ul>
    </div>
    <!--one complete form ends-->

  <?php echo $form->end(); ?>
</div>
</div>
