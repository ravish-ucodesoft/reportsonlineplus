<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core.js','jquery-ui-1.8.20.custom.min.js'));
    
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
?>
<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;     
}
#box {
    border: none;
}
.ui-widget-content {
width:100%
}
table{width:100%;}
.inner td {font-size:12px;color:#3E4061}
.thheading{ background-color:#59B2E9;color:#FFF;}
</style>
<script type="text/javascript">
			$(function(){
				//Accordion
			    $("#accordion").accordion({ header: "h3"});
			    $('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); },
					function() { $(this).removeClass('ui-state-hover'); }
			    );    
			});
 function limit(field1, chars) {
   var field="ExtinguisherReportNotificationNotification_"+field1;
  
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error'+field1).innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error'+field1).innerHTML='';}
        document.getElementById('limitCounter'+field1).innerHTML=len;
        document.getElementById('limitCounterLeft'+field1).innerHTML=chars-len;
    }
</script>
<style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			.ui-accordion-content{
			    
			}
</style>
<script type="text/javascript">
   function createUploader(val){       
         var uploader = new qq.FileUploader({
                element: document.getElementById('demo_'+val),
                listElement: document.getElementById('separate-list_'+val),
                action: '/sps/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment_"+val).val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture"+val).html(responseJSON.attachment);
	    }
            });           
    }        
window.onload = createUploader;
    
    function calltoggle(val)
    {
    var linkId="link1_"+val;
	     $("#div1_"+val).show();
	     $("#div2_"+val).hide();
	     
    } 
      function calltoggle1(val)
    {
    var linkId="link2_"+val;
	   $("#div2_"+val).show();
	   $("#div1_"+val).hide();
    } 
    function calltoggle2(val)
    {
   
	     $("#div3_"+val).show();
	     $("#div4_"+val).hide();
	     createUploader(val);
	     
    } 
      function calltoggle3(val)
    {
  
	   $("#div3_"+val).hide();
	   $("#div4_"+val).show();
    } 
    
  function checkUploadedAttachment(uid){  
	var uploadVal = jQuery("#uploadattachment_"+uid).val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
function checkNotification(uid){
	var notifyVal = jQuery("#ExtinguisherReportNotificationNotification_"+uid).val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }
 
function confirmattach(attachId,reportId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId,reportId);    
  }
  else{
    return false;
  }
 } 
 function RemoveAttachment(attachId,reportId){
       jQuery.ajax({
                type : "GET",
                url  : "/sps/delattachment/" + attachId+"/"+reportId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#tr_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}
function confirmnotifier(notifierId,reportId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId,reportId);    
  }
  else{
    return false;
  }
 }

function RemoveNotifier(notifierId,reportId){
       jQuery.ajax({
                type : "GET",
                url  : "/sps/delnotifier/" + notifierId+"/"+reportId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#tr_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
} 
  
</script>
<div id="content">
     <div id="box" style="width:710px;">
     <div id="deleteFlashmessage" style="display:none;">
	      <div class="notification msginfo">
		     <a class="close"></a>
		     <p style="text-align:center;color:red">Record has been deleted successfully</p>
	      </div>
	      </div>
     <div class="form-box" style="width:100%">
        <h2 style="font-weight:bold;">Total Service Calls of <?php echo ucwords($result['User']['site_name']) .' : '.count($data) ; ?></h2>
	
   <table class="tbl">
                        <tr>
                        </tr>
			<tr>
			    <td width="70%" align="left">
				<div id="accordion">
				<?php if(!empty($data))
				{ //pr($data);
					foreach($data as $key=>$res):?>
					<div>
					<h3><a href="#"><?php echo date('m/d/Y H:i:s',strtotime($res['ServiceCall']['created'])); ?></a></h3>
					<table class="inner" cellpadding="0" cellspacing="0">
						<tr>
						<th width="20%" class="thheading">Report</th>
						<th width="20%" class="thheading">Lead Inspector</th>
						<th width="20%" class="thheading">Helper</th>
						<th width="30%" class="thheading" align="center">Sch Date(m/d/Y)</th>
					   
						<th width="5%" class="thheading"><span style="font-size:12px;font-weight:bold;">Attachments</span></th>
						<th width="5%" class="thheading"><span style="font-size:12px;font-weight:bold;">Notifications</span></th>
						
						</tr>	
            	
						<?php foreach($res['Schedule'] as $key1=>$schedule): ?>
						
						<tr style="background-color:#e2eaed;">
							<td rowspan="2" ><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
							<td rowspan="2"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
							<td rowspan="2"><?php
							$helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
							echo $this->Common->getHelperName($helper_ins_arr); ?></td>
							<td rowspan="2" >
							
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:ia', $schedule['schedule_from_1']).'-'.$time->format('g:ia', $schedule['schedule_to_1']); ?>
<br/>
							    <?php if(!empty($schedule['schedule_date_2'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:ia', $schedule['schedule_from_2']).'-'.$time->format('g:ia', $schedule['schedule_to_2'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_3'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:ia', $schedule['schedule_from_3']).'-'.$time->format('g:ia', $schedule['schedule_to_3'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_4'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:ia', $schedule['schedule_from_4']).'-'.$time->format('g:ia', $schedule['schedule_to_4'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_5'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:ia', $schedule['schedule_from_5']).'-'.$time->format('g:ia', $schedule['schedule_to_5'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_6'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:ia', $schedule['schedule_from_6']).'-'.$time->format('g:ia', $schedule['schedule_to_6'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_7'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:ia', $schedule['schedule_from_7']).'-'.$time->format('g:ia', $schedule['schedule_to_7'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_8'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:ia', $schedule['schedule_from_8']).'-'.$time->format('g:ia', $schedule['schedule_to_8'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_9'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:ia', $schedule['schedule_from_9']).'-'.$time->format('g:ia', $schedule['schedule_to_9'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_10'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:ia', $schedule['schedule_from_10']).'-'.$time->format('g:ia', $schedule['schedule_to_10'])?>
							    <?php } ?>
							</td>
						
              <td style="text-align:center"><?php echo $html->link($this->Common->getReportattachmentcount($schedule['report_id'],$res['ServiceCall']['id']),'javascript:void(0);',array('id'=>'link_'.$schedule['id'],'class' => '','onclick'=>"calltoggle(".$schedule['id'].")"));?></td>
              <td style="text-align:center"><?php echo $html->link($this->Common->getReportnotificationcount($schedule['report_id'],$res['ServiceCall']['id']),'javascript:void(0);',array('id'=>'link2_'.$schedule['id'],'class' => '','onclick'=>"calltoggle1(".$schedule['id'].")"));?></td>	
             						
						</tr>
						<tr style="background-color:#e2eaed;">
              <td  style="text-align:center"><?php echo $html->link($html->image('icon_attachment.gif',array('alt'=>'Add Attachment','title'=>'Add Attachment')),'javascript:void(0);',array('id'=>'link3_'.$schedule['id'],'escape'=>false,'onclick'=>"calltoggle2(".$schedule['id'].")")); ?></td>
              <td  style="text-align:center"><?php echo $html->link($html->image('icon_notify.gif',array('alt'=>'Add Notification','title'=>'Add Notification')),'javascript:void(0);',array('id'=>'link4_'.$schedule['id'],'escape'=>false,'onclick'=>"calltoggle3(".$schedule['id'].")")); ?></td>
            </tr>
						<tr><td colspan="6">
						<div class="service-hdline"><span style="font-weight:bold;font-size:12px;">Services Taken</span></div>
						<ul class="services-tkn"><li>
						<?php $i=0;$j=1;
						
						for($i=0;$i<count($schedule['ScheduleService']);$i++)
						{
							echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
					        <?php if($j%4==0){
							echo '</li></ul><ul class="services-tkn"><li>';
						}
						else{
							echo '</li><li>';
						}
						 $j++;
						}
						?>
						</td></tr>
					<tr><td colspan="6">
					<!-- TOGGLE DIV1 START -->
					<div id="div1_<?php echo $schedule['id']; ?>" style="display:none " >
            <div class="service-hdline"><span style="font-size:12px;font-weight:bold;">Attachments</span></div>
						<ul class="services-tkn"><li style="width:100%">
						<table cellpadding="2" cellspacing="2" width="100%">
        			<tr>
        				<th>
        				  Name
        				</th>
        				<th>
        				  Created on
        				</th>
        				<th>
        				  Created by
        				</th>
        				<th>
        				   Action
        				</th>
        			</tr>
        		
        			<?php
               $sp = $this->Session->read('Log'); 
            $data= $this->Common->getAttachmentName($res['ServiceCall']['id'],$schedule['report_id']);
      
	    if(!empty($data)){
            foreach($data as $value){  ?>                 
             	<tr id="tr_<?php echo $value['id'] ?>">
	       <td> <?php echo $html->link($value['attach_file'],array("controller"=>"sps","action"=>"download",$value['attach_file']),array()); ?>
            </td>
               <td><?php echo date('m/d/Y',strtotime($value['created'])) ?></td>
               <td><?php echo $this->Common->getClientName($value['user_id']).'('. $value['added_by'].')'; ?></td>
               <td><?php if($value['user_id']==$sp['User']['id']){ ?>
					       <a href="javascript:void(0)" onclick="confirmattach('<?php echo $value['id'] ?>','<?php echo $value['report_id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a>				         
              <?php } ?>
              </td>
              </tr>
            <?php   }} ?>
					</table>	
						</li></ul>
          </div>
          <!-- TOGGLE DIV1 END -->
          <!-- TOGGLE DIV2 START -->
          <div id="div2_<?php echo $schedule['id']; ?>" style="display:none">
            <div class="service-hdline"><span style="font-size:12px;font-weight:bold;">Notifications</span></div>
						<ul class="services-tkn"><li style="width:100%" >
						<table cellpadding="2" cellspacing="2" width="100%">
        			<tr>
        				<th>
        				  Name
        				</th>
        				<th>
        				  Created on
        				</th>
        				<th>
        				  Created by
        				</th>
        				<th>
        				   Action
        				</th>
        			</tr>
        		
        			<?php
               $sp = $this->Session->read('Log');  
            $data= $this->Common->getNotificationName($res['ServiceCall']['id'],$schedule['report_id']);
      
	    if(!empty($data)){
            foreach($data as $value){  ?>                 
             	<tr id="tr_<?php echo $value['id'] ?>">
	       <td> <?php echo $value['notification']; ?>
            </td>
               <td><?php echo date('m/d/Y',strtotime($value['created'])) ?></td>
               <td><?php echo $this->Common->getClientName($value['user_id']).'('. $value['added_by'].')'; ?></td>
               <td><?php if($value['user_id']==$sp['User']['id']){ ?>
					       <a href="javascript:void(0)" onclick="confirmnotifier('<?php echo $value['id'] ?>','<?php echo $value['report_id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a>				         
              <?php } ?>
              </td>
              </tr>
            <?php   }} ?>
					</table>
						</li></ul>
          </div>
          <!-- TOGGLE DIV2 END -->
						
						</td></tr> 
						<tr><td colspan="6">
           <div id="div3_<?php echo $schedule['id']; ?>" style="display:none">
                <!-- This contains the ATTACHMENT FORM -->
            	<div style=''>		
            		<div id='' style='padding:20px; background:#fff;' >
            		<?php echo $form->create('', array('type'=>'POST','action'=>'add_attachment','name'=>'attachment','id'=>'attachment',"onsubmit"=>"return checkUploadedAttachment(".$schedule['id'].");")); ?>
            		<?php echo $form->input('Report.attach_file', array('id'=>'uploadattachment_'.$schedule['id'],'type'=>'hidden','value'=>'')); ?>
            		<?php echo $form->input('Report.servicecall_id',array('type'=>'hidden','value'=>$res['ServiceCall']['id'])); ?>
            		<?php echo $form->input('Report.report_id',array('type'=>'hidden','value'=>$schedule['report_id'])); ?>
            		<?php echo $form->input('Report.sp_id',array('type'=>'hidden','value'=>$res['ServiceCall']['sp_id'])); ?>
            
            		
            			<p><strong>Upload Attachment</strong></p>
            		<p>
            		    <div id="demo_<?php echo $schedule['id']; ?>"></div>
            		    <ul id="separate-list_<?php echo $schedule['id']; ?>"></ul>
            		</p>
            		<p><div id="uploaded_picture_<?php echo $schedule['id']; ?>" style="text-align:center;padding-left:90px;"></div></p>
            		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
            				
            		</div>
            		
            		<?php echo $form->end(); ?>
            	</div>
           <!-- END ATTACHMENT FORM --> 
           </div>
            <div id="div4_<?php echo $schedule['id']; ?>" style="display:none">
                  <!-- This contains notification form-->
            	<div style=''>		
            		<div id='' style='padding:20px; background:#fff;'>
            		<?php echo $form->create('', array('type'=>'POST', 'action'=>'add_notification','name'=>'notification','id'=>'notification',"onsubmit"=>"return checkNotification(".$schedule['id'].");")); ?>
            		<?php //echo $form->input('ExtinguisherReportNotification.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
            		<?php echo $form->input('Report.servicecall_id',array('type'=>'hidden','value'=>$res['ServiceCall']['id'])); ?>
            		<?php echo $form->input('Report.report_id',array('type'=>'hidden','value'=>$schedule['report_id'])); ?>
            		<?php echo $form->input('Report.sp_id',array('type'=>'hidden','value'=>$res['ServiceCall']['sp_id'])); ?>
            		<?php //echo $form->input('ExtinguisherReportNotification.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
            		<p><strong>Notification</strong></p>
            		<p>
            		    <?php echo $form->input('Report.notification',array('type'=>'textarea','rows'=>5,'cols'=>40,'label'=>false,'id'=>'ExtinguisherReportNotificationNotification_'.$schedule['id'],'onKeyUp' =>"return limit(".$schedule['id'].",500);")); ?>
            		</p>
					<p>
                    <div id="essay_Error<?php echo $schedule['id'] ?>" class='error'></div>
			<div>
			<small>&nbsp;<label id='limitCounter<?php echo $schedule['id'] ?>'>0</label>&nbsp;characters entered&nbsp;|&nbsp;<label id='limitCounterLeft<?php echo $schedule['id'] ?>'>500</label>&nbsp;characters remaining</small>
			</div>   
		</p>
            		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
            		
            		
            		</div>
            		
            		<?php echo $form->end(); ?>
            	</div>
           <!-- notification form END-->
            
            </div>
            </td></tr>
						<?php endforeach;?>
					</table>				
					</div>	
					<?php endforeach;
					
				} else {
				   }?>
    </div>
   </td></tr></table>
    <!--one complete form ends-->    
      </div>
</div>