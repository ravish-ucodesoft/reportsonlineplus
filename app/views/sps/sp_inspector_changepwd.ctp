<?php
    echo $this->Html->script('jquery.1.6.1.min');    
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');    
?>
<?php echo $html->css('newdesign_css/style'); ?>
<script type="text/javascript">
jQuery(document).ready(function(){    
    jQuery("#inspectionForm").validationEngine();
});
</script>
<!-- Inner Starts-->
<div id="content">
     
     <div id="box" style="width:700px;height:500px;padding-left:10px;">  
    <h3 id="adduser"><?php echo $html->image($this->Common->getUserProfilePhoto($result['User']['profilepic']),array('style'=>'vertical-align:middle')); ?>&nbsp;&nbsp;<span style="vertical-align:top"><?php echo ucfirst($result['User']['fname'].' '.$result['User']['lname']); ?> (Inspector)</span></h3>
    <div class='message'><?php echo $this->Session->flash();?></div>    
    <?php echo $form->create('User',array('id'=>'inspectionForm','url'=>array('controller'=>'sps','action'=>'inspector_changepwd')));?>
    <?php echo $form->input('User.id',array('type'=>'hidden','value'=>$result['User']['id'],'label'=>false, 'div'=>false));?>
        <h1>For Security reason,the password is encrypted,If you want to change the password,please use the form below</h1>
        <ul class="sign-form">
	     <li>
	       <label>New Password<span class="astric">*</span> </label>
	       <div class="input-cont">		 
                 <?php echo $form->input('User.password',array('type'=>'password','maxLength'=>10,'class'=>'search_bg validate[required]','label'=>false, 'div'=>false));?>
	       </div>
	     </li>
	     <li>
	       <label>Confirm Password<span class="astric">*</span> </label>
	       <div class="input-cont">		 
                 <?php echo $form->input('User.confirmpassword',array('type'=>'password','maxLength'=>10,'class'=>'search_bg validate[required,equals[UserPassword]]','label'=>false, 'div'=>false));?>
	       </div>
	     </li>
	     
             <li>
                <label>&nbsp;</label>
                <section class="login_btn">
                    <span>
                        <input name="Submit" type="submit" value="Submit" class="" />
		    </span> 
                </section>
	     </li>
             
             <li>
                <label>&nbsp;</label>
                <section class="">
                    <span>An email notification will send to <?php echo ucfirst($result['User']['fname'].' '.$result['User']['lname']); ?> (<?php echo $result['User']['email']; ?>) also for the new password.</span> 
                </section>
	     </li>
        </ul>
        <?php echo $form->end(); ?>
        </div>
</div>

