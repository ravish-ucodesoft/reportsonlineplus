<?php
//pr($this->data);
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('validation/only_num');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
 jQuery("#addclient").validationEngine();
    $('#UserPhone1').numeric();
    $('#UserPhone2').numeric();
    $('#UserPhone3').numeric();
    $('#UserZip').numeric();
    /*$('#SpSiteAddressAvailphone1').numeric();
    $('#SpSiteAddressAvailphone2').numeric();
    $('#SpSiteAddressAvailphone3').numeric();
    $('#SpSiteAddressSitePhone1').numeric();
    $('#SpSiteAddressSitePhone2').numeric();
    $('#SpSiteAddressSitePhone3').numeric();*/
    $('#UserBilltoPhone1').numeric();
    $('#UserBilltoPhone2').numeric();
    $('#UserBilltoPhone3').numeric();        
    
    WireAutoTab('UserPhone1','UserPhone2',3);
    WireAutoTab('UserPhone2','UserPhone3',3);    
    
    WireAutoTab('UserBilltoPhone1','UserBilltoPhone2',3);
    WireAutoTab('UserBilltoPhone2','UserBilltoPhone3',3);
    
    /*
    WireAutoTab('SpSiteAddressAvailphone1','SpSiteAddressAvailphone2',3);
    WireAutoTab('SpSiteAddressAvailphone2','SpSiteAddressAvailphone3',3);    
    WireAutoTab('SpSiteAddressSitePhone1','SpSiteAddressSitePhone2',3);
    WireAutoTab('SpSiteAddressSitePhone2','SpSiteAddressSitePhone3',3);
    */
});
  
  function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxuploaduserpic',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#ProfileImage").val(responseJSON.profile_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/profilepic/'+responseJSON.profile_image+'">');
	    }
            });           
    }        
	window.onload = createUploader;
</script>
<script>
//var states = null;
    function getStates(id,value,stateid,cityid){
	
    var optArr = document.getElementById(id).value;    
	jQuery('#'+stateid).html("<option value=''>Select</option>");
	jQuery('#'+cityid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByCountry/" + value,
                success : function(data){
		    states = data;
		    var selectedOption = '';
		    var select = $('#'+stateid);
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    //$('option', select).remove();
		     
		    $.each(states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}
 
/*Function to fetch cities on change of states created by Manish Kumar On Nov 16, 2012*/
function getCities(id,value,cityid){
	
    var optArr = document.getElementById(id).value;    
	jQuery('#'+cityid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByState/" + value,
                success : function(data){
		    window.cities = data;
		    var selectedOption = '';
		    var select = $('#'+cityid);
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    //$('option', select).remove();
		     
		    $.each(window.cities, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}
 
    function getStates1(id,value){

		    $('#UserCountryId1').val($('#UserCountryId').val())
		    var selectedOption = $('#UserStateId1').val();
		    var select = $('#UserStateId1');
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    $('option', select).remove();
		     
		    $.each(states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
    
}

// To Get auto filled site info if it is same as that of company 
function getCompanyData(){
          
   if($('#addlink').is(':checked')){
	document.getElementById("UserSitePhone1").value= document.getElementById("UserPhone1").value;
	document.getElementById("UserSitePhone2").value= document.getElementById("UserPhone2").value;
	document.getElementById("UserSitePhone3").value= document.getElementById("UserPhone3").value;
	document.getElementById("UserSiteEmail").value= document.getElementById("UserEmail").value;
	document.getElementById("UserSiteAddress").value= document.getElementById("UserAddress").value;
	document.getElementById("UserSiteCity").value= document.getElementById("UserCity").value;
	document.getElementById("UserSiteZip").value= document.getElementById("UserZip").value;
	document.getElementById("UserCountryId1").value= document.getElementById("UserCountryId").value;
	getStates1('UserCountryId1',document.getElementById("UserCountryId").value);	
	document.getElementById("UserStateId1").value= document.getElementById("UserStateId").value;   }

}

/*Auto Focus code*/
function WireAutoTab(CurrentElementID, NextElementID, FieldLength) {
    //Get a reference to the two elements in the tab sequence.
    var CurrentElement = $('#' + CurrentElementID);
    var NextElement = $('#' + NextElementID);
 
    CurrentElement.keyup(function(e) {
        //Retrieve which key was pressed.
        //var KeyID = (window.event) ? event.keyCode : e.keyCode;
 
        //If the user has filled the textbox to the given length and
        //the user just pressed a number or letter, then move the
        //cursor to the next element in the tab sequence.   
        /*if (CurrentElement.val().length >= FieldLength
            && ((KeyID >= 48 && KeyID <= 90) ||
            (KeyID >= 96 && KeyID <= 105)))*/
	if (CurrentElement.val().length >= FieldLength)
            NextElement.focus();
    });
}
/*Auto focus code*/

function resetBillTo(){
    $("#UserBilltoFname").val('');
    $("#UserBilltoLname").val('');
    $("#UserBilltoPhone1").val('');
    $("#UserBilltoPhone2").val('');
    $("#UserBilltoPhone3").val('');
    
    $("#UserBilltoEmail").val('');
    $("#UserBilltoAddress").val('');
    $("#UserBilltoCountryId").val('233');
    $("#UserBilltoCity").val('');
    $("#UserBilltoZip").val('');
    
    getStates('UserBilltoCountryId','233','UserBilltoStateId','UserBilltoCityId');
}

/*functon resetSite(){
    $("#SpSiteAddressSiteName").val('');
    $("#UserBilltoLname").val('');
    $("#SpSiteAddressSitePhone1").val('');
    $("#SpSiteAddressSitePhone2").val('');
    $("#SpSiteAddressSitePhone3").val('');
    
    $("#SpSiteAddressSiteEmail").val('');
    $("#SpSiteAddressSiteAddress").val('');
    $("#UserSiteCountryId").val('233');
    $("#SpSiteAddressSiteCity").val('');
    $("#SpSiteAddressSiteZip").val('');
    
    getStates('UserSiteCountryId','233','UserSiteStateId','UserSiteCityId');
}*/

function setBilltoInfo(status){
    if(status==true){
	fname = $("#UserFname").val();
	lname = $("#UserLname").val();
	phone1 = $("#UserPhone1").val();
	phone2 = $("#UserPhone2").val();
	phone3 = $("#UserPhone3").val();
	usrEmail = $("#UserEmail").val();
	usrAddr = $("#UserAddress").val();
	usrCountryId = $("#UserCountryId").val();
	usrStateId = $("#UserStateId").val();
	usrCityId = $("#UserCityId").val();
	usrZip = $("#UserZip").val();
	
	if(fname!=""){
	    $("#UserBilltoFname").val(fname);
	}
	if(lname!=""){
	    $("#UserBilltoLname").val(lname);
	}
	if(phone1!=""){
	    $("#UserBilltoPhone1").val(phone1);
	}
	if(phone2!=""){
	    $("#UserBilltoPhone2").val(phone2);
	}
	if(phone3!=""){
	    $("#UserBilltoPhone3").val(phone3);
	}
	if(usrEmail!=""){
	    $("#UserBilltoEmail").val(usrEmail);
	}
	if(usrAddr!=""){
	    $("#UserBilltoAddress").val(usrAddr);
	}
	if(usrCountryId!=""){
	    $("#UserBilltoCountryId").val(usrCountryId);
	}
	if(usrStateId!=""){
	    stateData = $("#UserStateId").html();
	    $("#UserBilltoStateId").html(stateData);
	    $("#UserBilltoStateId").val(usrStateId);
	}
	if(usrCityId!=""){
	    cityData = $("#UserCityId").html();
	    $("#UserBilltoCityId").html(cityData);   
	    $("#UserBilltoCityId").val(usrCityId);
	}
	if(usrZip!=""){
	    $("#UserBilltoZip").val(usrZip);
	}
    }else{
	resetBillTo();
    }
}

/*function setSiteInfo(status){
    if(status==true){
	fname = $("#UserFname").val();
	lname = $("#UserLname").val();
	phone1 = $("#UserPhone1").val();
	phone2 = $("#UserPhone2").val();
	phone3 = $("#UserPhone3").val();
	usrEmail = $("#UserEmail").val();
	usrAddr = $("#UserAddress").val();
	usrCountryId = $("#UserCountryId").val();
	usrStateId = $("#UserStateId").val();
	usrCityId = $("#UserCity").val();
	usrZip = $("#UserZip").val();
	
	if(fname!=""){
	    $("#SpSiteAddressSiteName").val(fname+' '+lname);
	}    
	if(phone1!=""){
	    $("#SpSiteAddressSitePhone1").val(phone1);
	}
	if(phone2!=""){
	    $("#SpSiteAddressSitePhone2").val(phone2);
	}
	if(phone3!=""){
	    $("#SpSiteAddressSitePhone3").val(phone3);
	}
	if(usrEmail!=""){
	    $("#SpSiteAddressSiteEmail").val(usrEmail);
	}
	if(usrAddr!=""){
	    $("#SpSiteAddressSiteAddress").val(usrAddr);
	}
	if(usrCountryId!=""){
	    $("#UserSiteCountryId").val(usrCountryId);
	}
	if(usrStateId!=""){
	    stateData = $("#UserStateId").html();
	    $("#UserSiteStateId").html(stateData);   
	    $("#UserSiteStateId").val(usrStateId);
	}
	if(usrCityId!=""){
	    $("#SpSiteAddressSiteCity").val(usrCityId);
	}
	if(usrZip!=""){
	    $("#SpSiteAddressSiteZip").val(usrZip);
	}
    }else{
	resetSite();
    }
}*/
</script>
<style>
.error-message{
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: right;
    font-weight: bold;
    position: static;
    top: 30px;
    white-space: nowrap;
}
input { width:250px; }
div.checkbox{
    float: left;
    padding-right: 5px;
}
div.checkbox input,div.checkbox label{
 width: auto;
}
</style>
<div class="register-wrap">
    <h1 class="main-hdng">Edit Client Information</h1>
    <?php
     echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'editclient'),'id'=>'editclient'));
     echo $form->input('User.id',array('type'=>'hidden'));
     //echo $form->input('SpSiteAddress.id',array('type'=>'hidden','value'=>$this->data['SpSiteAddress'][0]['id']));
     //echo $form->input('Company.id',array('type'=>'hidden'));
     ?>
     <?php echo $form->input('User.oldprofilepic', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$this->data['User']['profilepic']));  ?>
     <?php echo $form->input('User.newprofilepic', array('id'=>'ProfileImage','type'=>'hidden',"div"=>false,"label"=>false));  ?>
     <!--one complete form-->
     <div class="form-box" style="width:46%;float:left">
        <h2 style="background:#666; color:white; padding-left:10px; font-weight:bold">Client Information</h2>
        <ul class="register-form">
	    <li>
                <label>Company Name<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.client_company_name',array('maxLength'=>'100','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"1")); ?></span></p>				
            </li>
            <li>
                <label>First Name<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"2"));  ?></span></p>
            </li>
            <li>
                <label>Last Name</label>
                <p><span class="input-field"><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false,"tabindex"=>"3"));  ?></span></p>
            </li>
            <li>
                <label>Applicable Code Type<span>*</span></label>                
	        <?php foreach($codetype as $type){
			$option[$type['CodeType']['id']]= $type['CodeType']['code_type']; ?>
	        <?php }
	        $selectedCode = explode(',',$this->data['User']['code_type_id']);
	        ?>
	        <?php echo $form->input('User.code_type_id.',array('type' => 'select','label'=>false,'div'=>false,'multiple'=>'checkbox','options'=>$option,'selected'=>$selectedCode,"tabindex"=>"4"));?>
            </li>            
            <li>
                <label>Phone</label>
		<?php 
		    $phone = explode('-',$this->data['User']['phone']);		
		?>
                <p><span class="input-field"><?php echo $form->input('User.phone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"value"=>$phone[0],"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"6"));  ?>-</span></p>
		<p><span class="input-field"><?php echo $form->input('User.phone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"value"=>$phone[1],"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"7"));  ?>-</span></p>
		<p><span class="input-field"><?php echo $form->input('User.phone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"value"=>$phone[2],"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"8"));  ?></span></p>   
		
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
            <li>
                <label>Email<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.email', array('maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'validate[required,custom[email]]'));  ?></span></p>
            </li>
            <li>
                <label>Address<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.address',array('maxLength'=>'100','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?></span></p>
				
            </li>
	    <li>
                <label>Country<span>*</span></label>
                <p><span class=""><?php echo $this->Form->select('User.country_id', $country,$this->data['User']['country_id'], array('id'=>'UserCountryId','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserStateId','UserCityId');"));?></span></p>
				
            </li>
	    <li>
                <label>State<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.state_id',$state,$this->data['User']['state_id'], array('id'=>'UserStateId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-","tabindex"=>"12",'onchange'=>"getCities(this.id,this.value,'UserCityId');" ));?></span></p>
				
            </li>	    
	    <li>
                <label>City<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.city_id',$city,$this->data['User']['city_id'], array('id'=>'UserCityId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"13"));?></span></p>				
            </li>
	    <!--<li>
                <label>City<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.city',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?></span></p>				
            </li>-->
	    <li>
                <label>Zip<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.zip',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?></span></p>
				
            </li>
	     <li>
                <label>Profile pic</label>
                <p><span class="">
		     <div id="demo"></div>
		    <ul id="separate-list"></ul>
		</span></p>
            </li>
	     <li>
                <label>&nbsp;</label>
                <p><span class="">
		     <div id="uploaded_picture"  style="text-align:left;"><?php echo $html->image($this->Common->getUserProfilePhoto($this->data['User']['profilepic']));?></div>
		</span></p>
            </li>
        </ul>
    </div>
    <!--one complete form ends-->
    
    <!--one complete form-->
    <!--<div class="form-box" style="width:46%; float:left">        
        <h2 style="background:#666; color:white; padding-left:10px; font-weight:bold">Work/Site Information</h2>
        <ul class="register-form">
	    <li style="background:e2e3e3;">
                <label><?php //echo $form->checkbox('User.chkSame',array('id'=>'siteInfo','onchange'=>'setSiteInfo(this.checked);','tabindex'=>'33','style'=>'background:none;width:25px;border:none;'))?></label>
                <p style="padding-top: 7px;">Same as Client Information</p>
            </li>
            <li>
                <label>Site Name<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_name', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"34",'value'=>$this->data['SpSiteAddress'][0]['site_name']));  ?></span></p>
            </li>                                    
            <li>
                <label>Phone</label>
		<?php //$sitePhone = explode('-',$this->data['SpSiteAddress'][0]['site_phone'])?>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_phone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"35",'value'=>$sitePhone[0]));  ?>-</span></p>
		<p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_phone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"36",'value'=>$sitePhone[1]));  ?>-</span></p>
		<p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_phone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"37",'value'=>$sitePhone[2]));  ?></span></p>		
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
            <li>
                <label>Email<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_email', array('maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'validate[required,custom[email]]',"tabindex"=>"38",'value'=>$this->data['SpSiteAddress'][0]['site_email']));  ?></span></p>
            </li>
            <li>
                <label>Address<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_address',array('maxLength'=>'100','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"39",'value'=>$this->data['SpSiteAddress'][0]['site_address'])); ?></span></p>
				
            </li>
	    <li>
                <label>Country<span>*</span></label>
                <p><span class=""><?php //echo $this->Form->select('SpSiteAddress.country_id', $country,$this->data['SpSiteAddress'][0]['country_id'], array('id'=>'UserSiteCountryId','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserSiteStateId','UserSiteCityId');","tabindex"=>"40"));?></span></p>
				
            </li>
	    <li>
                <label>State<span>*</span></label>
                <p><span class=""><?php  //echo $this->Form->select('SpSiteAddress.state_id',$state2,$this->data['SpSiteAddress'][0]['state_id'], array('id'=>'UserSiteStateId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"41",'onchange'=>"getCities(this.id,this.value,'UserSiteCityId');"));?></span></p>
				
            </li>
	    <li>
                <label>City<span>*</span></label>
                <p><span class=""><?php  //echo $this->Form->select('SpSiteAddress.city_id',$city2,$this->data['SpSiteAddress'][0]['city_id'], array('id'=>'UserSiteCityId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"42"));?></span></p>
				
            </li>-->
	    <!--<li>
                <label>City<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_city',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"42")); ?></span></p>
				
            </li>-->
	    <!--<li>
                <label>Zip<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_zip',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"43",'value'=>$this->data['SpSiteAddress'][0]['site_zip'])); ?></span></p>
				
            </li>
	    <li>
                <label>On-site person to contact<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.site_contact_name',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"44",'value'=>$this->data['SpSiteAddress'][0]['site_contact_name'])); ?></span></p>
				
            </li>
	    <li>
		<h2 style="padding-left:10px; font-weight:bold">Responsible contact information for scheduling</h2>        
	    </li>
	    <li>
                <label>Name<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.responsible_person_name',array('type'=>'text','maxLength'=>'60','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"15",'value'=>$this->data['SpSiteAddress'][0]['responsible_person_name'])); ?></span></p>
            </li>
	    <li>
                <label>Email<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.responsible_person_email',array('type'=>'text','maxLength'=>'60','class'=>'validate[required,custom[email]]','div'=>false,"label"=>false,"tabindex"=>"16",'value'=>$this->data['SpSiteAddress'][0]['responsible_person_email'])); ?></span></p>
            </li>
            <li>
                <label>Phone Number<span>*</span></label>
		<?php //$siteAvailPhone=explode('-',$this->data['SpSiteAddress'][0]['responsible_contact']);?>
                <p><span class="input-field"><?php //echo $form->input('SpSiteAddress.availphone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"17",'value'=>$siteAvailPhone[0]));  ?>-</span></p>
		<p><span class="input-field"><?php //echo $form->input('SpSiteAddress.availphone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"18",'value'=>$siteAvailPhone[1]));  ?>-</span></p>
		<p><span class="input-field"><?php //echo $form->input('SpSiteAddress.availphone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"19",'value'=>$siteAvailPhone[2]));  ?></span></p>
            </li>
        </ul>
    </div>-->
    <!--one complete form ends-->
    
	<!--one complete form-->
   <!-- <div class="form-box" style="width:90%">
        <h2>Site Location</h2>
        <ul class="register-form">
            <li>
                <label>Site Name<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.site_name', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?></span></p>
            </li>
	    <li>
                <label>Site Phone</label>
                <p><span class="input-field">
		<?php //echo $this->data['User']['phone']; 
		//$sitephone = explode('-',$this->data['User']['site_phone']);
		
		?>
		<?php //echo $form->input('User.site_phone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"value"=>$sitephone[0],"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"6"));  ?>-</span></p>
		<p><span class="input-field"><?php //echo $form->input('User.site_phone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"value"=>$sitephone[1],"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"7"));  ?>-</span></p>
		<p><span class="input-field"><?php //echo $form->input('User.site_phone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"value"=>$sitephone[2],"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"8"));  ?></span></p>
	      
		
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
	    <li>
                <label>Site Email<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.site_email',array('maxLength'=>'100','type'=>'text','class'=>'validate[required,custom[email]]','div'=>false,"label"=>false)); ?></span></p>
            </li>
            <li>
                <label>Site Address<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.site_address', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?></span></p>
		<span style="clear:left; float:left; width:252px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 403 Meco Drive, Wilmington, DE, United States</b></span>
            </li>
            <li>
                <label>Site Contact Name<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.site_cname', array('maxLength'=>'20',"div"=>false,"label"=>false,'class'=>'validate[required]'));  ?></span></p>
            </li>            
            <li>
                <label>Site Country<span>*</span></label>
                <p><span class=""><?php //echo $this->Form->select('User.site_country_id', $country,$this->data['User']['site_country_id'], array('id'=>'UserCountryId1','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserStateId1');"));?></span></p>
				
            </li>
	    <li>
                <label>Site State<span>*</span></label>
                <p><span class=""><?php  //echo $this->Form->select('User.site_state_id',$state1,$this->data['User']['site_state_id'], array('id'=>'UserStateId1','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ));?></span></p>
				
            </li>
	    <li>
                <label>Site City<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.site_city',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?></span></p>
				
            </li>
	    <li>
                <label>Site Zip<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.site_zip',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?></span></p>
				
            </li>
	    <li>
		<label>&nbsp;</label>
		
	    </li>
	   
        </ul>
    </div> -->
    <!--one complete form ends-->
    
    <!--one complete form-->
    <!--<div class="form-box" style="width:90%">
        <h2>Responsible Personnel contact information</h2>
        <ul class="register-form">
            <li>
                <label>Most Available Phone Number<span>*</span></label>
               <?php 	//$availphone = explode('-',$this->data['User']['responsible_contact']); ?>
                <p><span class="input-field"><?php //echo $form->input('User.availphone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"value"=>$availphone[0],"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"25"));  ?>-</span></p>
		<p><span class="input-field"><?php //echo $form->input('User.availphone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"value"=>$availphone[1],"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"26"));  ?>-</span></p>
		<p><span class="input-field"><?php //echo $form->input('User.availphone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"value"=>$availphone[2],"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"27"));  ?></span></p>
            </li>
            <li>
                <label>Personnel Name<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.responsible_person_name',array('type'=>'text','maxLength'=>'60','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"28")); ?></span></p>
            </li>
        </ul>
    </div>-->
    <!--one complete form ends-->
    
    <!--<div style="clear:both; width:100%"></div>-->
    
    <!--one complete form-->
    <div class="form-box" style="width:46%; float:left">        
        <h2 style="background:#666; color:white; padding-left:10px; font-weight:bold">Bill To Information</h2>
        <ul class="register-form">	    
            <li>
                <label><?php echo $form->checkbox('User.chkSame',array('id'=>'billtoInfo','onchange'=>'setBilltoInfo(this.checked)','tabindex'=>'21','style'=>'background:none;width:25px;border:none;'))?></label>
                <p style="padding-top: 7px;">Same as Client Information</p>
            </li>
	    <li>
                <label>First Name<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.billto_fname', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"22"));  ?></span></p>
            </li>
            <li>
                <label>Last Name</label>
                <p><span class="input-field"><?php echo $form->input('User.billto_lname', array('maxLength'=>'50',"div"=>false,"label"=>false,"tabindex"=>"23"));  ?></span></p>
            </li>                        
            <li>
                <label>Phone</label>
		<?php $billtoPhone = explode('-',$this->data['User']['billto_phone']);?>
                <p><span class="input-field"><?php echo $form->input('User.billto_phone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"24",'value'=>$billtoPhone[0]));  ?>-</span></p>
		<p><span class="input-field"><?php echo $form->input('User.billto_phone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"25",'value'=>$billtoPhone[1]));  ?>-</span></p>
		<p><span class="input-field"><?php echo $form->input('User.billto_phone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"26",'value'=>$billtoPhone[2]));  ?></span></p>
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
            <li>
                <label>Email<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.billto_email', array('maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'validate[required,custom[email]]',"tabindex"=>"27"));  ?></span></p>
            </li>
	    
	    <li>
                <label>Address<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.billto_address',array('maxLength'=>'100','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"28")); ?></span></p>
				
            </li>
	    <li>
                <label>Country<span>*</span></label>
                <p><span class=""><?php echo $this->Form->select('User.billto_country_id', $country,$this->data['User']['billto_country_id'], array('id'=>'UserBilltoCountryId','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserBilltoStateId','UserBilltoCityId');","tabindex"=>"29"));?></span></p>
				
            </li>
	    <li>
                <label>State<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.billto_state_id',$state1,$this->data['User']['billto_state_id'], array('id'=>'UserBilltoStateId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"30",'onchange'=>"getCities(this.id,this.value,'UserBilltoCityId');"));?></span></p>
				
            </li>
	    <li>
                <label>City<span>*</span></label>
                <p><span class=""><?php  echo $this->Form->select('User.billto_city_id',$city1,$this->data['User']['billto_city_id'], array('id'=>'UserBilltoCityId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"31"));?></span></p>
				
            </li>
	    <!--<li>
                <label>City<span>*</span></label>
                <p><span class="input-field"><?php //echo $form->input('User.billto_city',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"31")); ?></span></p>
				
            </li>-->
	    <li>
                <label>Zip<span>*</span></label>
                <p><span class="input-field"><?php echo $form->input('User.billto_zip',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"32")); ?></span></p>
				
            </li>
             <li>
                <label>&nbsp;</label>
                <p>Additional sites to the Clients address or Bill to?</p>
            </li>                                    
            <li>
                <label>&nbsp;</label>
		<?php
		    $options = array('Yes'=>'Yes','No'=>'No');
		    $attributes = array('legend'=>false,'separator'=>'&nbsp;&nbsp;','tabindex'=>'45','style'=>'width:10px');		    
		?>
                <p><?php echo $form->radio('User.additional_sites',$options, $attributes);?></p>
            </li>
        </ul>
    </div>
    <!--one complete form ends-->
    
    <!--one complete form-->
    <div class="form-box" style="clear:both; float:right; margin-right:55px">
	<ul>
	     <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
            </li>	    
        </ul>
    </div>
    <!--one complete form ends-->
<?php echo $form->end(); ?>
</div>

<script type="text/javascript">
 <?php $arr = array(); foreach($state as $key => $value){ 
   $arr[] =  '"'.$key.'":"'.$value.'"';
 } ?>
$(document).ready(function(){
//Declared States as gloabal
  window.states =  {<?php echo implode(",",$arr); ?>}
  jQuery('div.checkbox').children('input').attr("class","validate[minCheckbox[1]]");
});
</script>