<div style="min-width:800px; min-height:300px">
<table class="inner" cellpadding="2" cellspacing="2">
    <tr><td width="100%" style="background-color:#ffff83;color:#000;font-size:14px;"><strong><?php echo $this->Common->getReportName($reportId).' Report';?></strong></td></tr>    
    <tr>
        <th width="15%" style="background-color:#2B92DD;color:#FFF">Log History</th>    
    </tr>
    <?php if(!empty($logData)){?>
    <?php foreach($logData  as $logdata){ ?>
    <tr>
       <td width="100%"><?php echo $html->image('circle-sel.png').'&nbsp;&nbsp;<b>'.$this->Common->getClientName($logdata['InspectorLog']['user_id']). "</b> has ".$logdata['InspectorLog']['action']." on <b>".$time->Format('m-d-Y H:i:s',$logdata['InspectorLog']['created']).'</b>'; ?> </td>
    </tr>
    <?php } }else{ ?>
    <tr>
       <td align="center" width="100%"><?php echo "No Record Found"; ?> </td>
    </tr>
    <?php } ?>
</table>
</div>