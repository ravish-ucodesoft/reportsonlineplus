<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core.js','jquery-ui-1.8.20.custom.min.js')); 
?>
<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;
    width: 200px;
}
#box {
    border: none;
}
table {
    border-collapse: collapse;
    margin: 1px;
    width: 98%;
}
.form-box{ min-width: 380px;}
.register-form{ min-height:290px;}

</style>
<script type="text/javascript">
    $(function(){
	//Accordion
	$("#accordion").accordion({ header: "h3"});
	$('#dialog_link, ul#icons li').hover(
	    function() { $(this).addClass('ui-state-hover'); },
	    function() { $(this).removeClass('ui-state-hover'); }
	);    
    });
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}
    .ui-accordion-content{ }
</style>
<div id="content">
     <div id="box" style="width:800px;padding-left:10px;">
     <div class="form-box" style="width:45%; float: left;">
        <h2 style="font-weight:bold;">Client Information</h2>
        <ul class="register-form">
            <li>
                <label>Company Name</label>
                <p><?php echo $result['User']['client_company_name']?></p>
            </li>
	    <li>
                <label>First Name</label>
                <p><?php echo $result['User']['fname']?></p>
            </li>
            <li>
                <label>Last Name</label>
                <p><?php echo $result['User']['lname']?></p>
            </li>
            <li>
                <label>Phone</label>
                <p><?php echo $result['User']['phone']?></p>
            </li>
	    <li>
                <label>Address</label>
                <p><?php echo $result['User']['address']?></p>
            </li>
            <li>
                <label>Email</label>
                <p><?php echo $result['User']['email']?></p>
            </li>
	    <li>
                <label>Country</label>
                <p><?php echo $result['Country']['name']?></p>
            </li>
            <li>
                <label>State</label>
                <p><?php echo $result['State']['name']?></p>
            </li>
	    <li>
                <label>City</label>
                <p><?php echo $result['User']['city']?></p>
            </li>
	    <li>
                <label>Zip</label>
                <p><?php echo $result['User']['zip']?></p>
            </li>
        </ul>
    </div>
    <!--one complete form ends-->
    
    <div class="form-box" style="width:45%; float: left;">
        <h2 style="font-weight:bold;">BillTo Information</h2>
        <ul class="register-form">
            <li>
                <label>First Name</label>
                <p><?php echo $result['User']['billto_fname']?></p>
            </li>
            <li>
                <label>Last Name</label>
                <p><?php echo $result['User']['billto_lname']?></p>
            </li>
            <li>
                <label>Phone</label>
                <p><?php echo $result['User']['billto_phone']?></p>
            </li>
	    <li>
                <label>Address</label>
                <p><?php echo $result['User']['billto_address']?></p>
            </li>
            <li>
                <label>Email</label>
                <p><?php echo $result['User']['billto_email']?></p>
            </li>
	    <li>
                <label>Country</label>
                <p><?php echo $result['BillCountry']['name']?></p>
            </li>
            <li>
                <label>State</label>
                <p><?php echo $result['BillState']['name']?></p>
            </li>
	    <li>
                <label>City</label>
                <p><?php echo $result['User']['billto_city']?></p>
            </li>
	    <li>
                <label>Zip</label>
                <p><?php echo $result['User']['billto_zip']?></p>
            </li>
        </ul>
    </div>
    
    <!--one complete form-->
    <!--<div class="form-box" style="width:90%">
        <h2 style="font-weight:bold;">Site Information</h2>
        <ul class="register-form">
            <li>
                <label>Site Name</label>
                <p><?php //echo $result['User']['site_name']?></p>
            </li>
	    <li>
                <label>Site Phone</label>
                <p><?php //echo $result['User']['site_phone']?></p>
            </li>
	    <li>
                <label>Site Email</label>
                <p><?php //echo $result['User']['site_email']?></p>
            </li>
            <li>
                <label>Site Address</label>
                <p><?php //echo $result['User']['site_address']?></p>
            </li>
            <li>
                <label>Site Contact Name</label>
                <p><?php //echo $result['User']['site_cname']?></p>
            </li>
            <li>
                <label>Country</label>
                <p><?php //echo $this->Common->getCountryName($result['User']['site_country_id'])?></p>
            </li>
            <li>
                <label>State</label>
                <p><?php //echo $this->Common->getStateName($result['User']['site_state_id'])?></p>
            </li>
	    <li>
                <label>City</label>
                <p><?php //echo $result['User']['site_city']?></p>
            </li>
	    <li>
                <label>Zip</label>
                <p><?php //echo $result['User']['site_zip']?></p>
            </li>
	    <li>
		    <label>&nbsp;</label>		
	    </li>	   
        </ul>
    </div>-->
    <!--one complete form ends-->
    <!--one complete form-->
    <div class="form-box" style="width:90%">
        <h2 style="font-weight:bold;">Responsible contact information for scheduling</h2>
        <ul class="register-form">
            <li>
                <label>Name</label>
                <p><?php echo $result['User']['responsible_person_name']?></p>
            </li>
	    <li>
                <label>Email</label>
                <p><?php echo $result['User']['responsible_person_email']?></p>
            </li>
	    <li>
                <label>Phone Number</label>
                <p><?php echo $result['User']['responsible_contact']?></p>
            </li>
        </ul>
    </div>
    
    <!--one complete form-->
    <div class="form-box" style="width:90%">
        <h2 style="font-weight:bold;">Site Address Information</h2>
	<?php
	    if(sizeof($result['SpSiteAddress'])>0){
	    foreach($result['SpSiteAddress'] as $siteData){
	?>
	<h3><?php echo ucwords($siteData['site_name']);?></h3>
        <ul class="register-form">
            <li>
                <label>Site Name</label>
                <p><?php echo $siteData['site_name'];?></p>
            </li>
	    <li>
                <label>Site Phone</label>
                <p><?php echo $siteData['site_phone']?></p>
            </li>
	    <li>
                <label>Site Email</label>
                <p><?php echo $siteData['site_email']?></p>
            </li>
            <li>
                <label>Site Address</label>
                <p><?php echo $siteData['site_address']?></p>
            </li>
            <li>
                <label>Site Contact Name</label>
                <p><?php echo $siteData['site_contact_name']?></p>
            </li>
            <li>
                <label>Country</label>
                <p><?php echo $this->Common->getCountryName($siteData['country_id'])?></p>
            </li>
            <li>
                <label>State</label>
                <p><?php echo $this->Common->getStateName($siteData['state_id'])?></p>
            </li>
	    <li>
                <label>City</label>
                <p><?php echo $siteData['site_city']?></p>
            </li>
	    <li>
                <label>Zip</label>
                <p><?php echo $siteData['site_zip']?></p>
            </li>
	    <li>
		    <label>&nbsp;</label>		
	    </li>	   
        </ul>
	<?php }}else{?>
	<ul class="register-form">
            <li>
                <label>&nbsp;</label>
                <p>No site addresses have benn added for this client.</p>
            </li>
	</ul>    
	<?php }?>
    </div>
    <!--one complete form ends-->
    
     <div class="form-box" style="width:90%">
        <h2 style="font-weight:bold;">Total Service Calls of <?php echo ucwords($result['User']['site_name']) .' : '.count($data) ; ?></h2>
	 <table class="tbl">
                        <tr>
                        </tr>
			<tr>
			    <td width="90%" align="left">
				<div id="accordion">
				<?php if(!empty($data))
				{
					foreach($data as $key=>$res):?>
					<div>
					<h3><a href="#"><?php echo date('m/d/Y H:i:s',strtotime($res['ServiceCall']['created'])); ?></a></h3>
					<table class="inner" cellpadding="0" cellspacing="0" width="100%">
						<tr>
						<th width="20%" style="background-color:#2B92DD;color:#FFF">Report</th>
						<th width="20%" style="background-color:#2B92DD;color:#FFF">Lead Inspector</th>
						<th width="35%" style="background-color:#2B92DD;color:#FFF">Helper</th>
						<th width="25%" style="background-color:#2B92DD;color:#FFF" align="center">Sch Date(m/d/Y)</th>
						<th width="25%" style="background-color:#2B92DD;color:#FFF" align="center">&nbsp;</th>
						</tr>		
						<?php foreach($res['Schedule'] as $key1=>$schedule):?>
						<tr style="background-color:#e2eaed;">
							<td ><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
							<td><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
							<td><?php
							$helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
							echo $this->Common->getHelperName($helper_ins_arr); ?></td>
							<td>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:ia', $schedule['schedule_from_1']).'-'.$time->format('g:ia', $schedule['schedule_to_1']); ?>
<br/>
							    <?php if(!empty($schedule['schedule_date_2'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:ia', $schedule['schedule_from_2']).'-'.$time->format('g:ia', $schedule['schedule_to_2'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_3'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:ia', $schedule['schedule_from_3']).'-'.$time->format('g:ia', $schedule['schedule_to_3'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_4'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:ia', $schedule['schedule_from_4']).'-'.$time->format('g:ia', $schedule['schedule_to_4'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_5'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:ia', $schedule['schedule_from_5']).'-'.$time->format('g:ia', $schedule['schedule_to_5'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_6'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:ia', $schedule['schedule_from_6']).'-'.$time->format('g:ia', $schedule['schedule_to_6'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_7'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:ia', $schedule['schedule_from_7']).'-'.$time->format('g:ia', $schedule['schedule_to_7'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_8'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:ia', $schedule['schedule_from_8']).'-'.$time->format('g:ia', $schedule['schedule_to_8'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_9'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:ia', $schedule['schedule_from_9']).'-'.$time->format('g:ia', $schedule['schedule_to_9'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_10'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:ia', $schedule['schedule_from_10']).'-'.$time->format('g:ia', $schedule['schedule_to_10'])?>
							    <?php } ?>
							</td>
							<td align='left'><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
							    <?php
							     //echo $schedule['report_id']."--".$res['ServiceCall']['id']."---".$res['ServiceCall']['client_id']."--".$res['ServiceCall']['sp_id'];
								  $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
								  $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
								 if($getCompleted != "" && $getstarted == 1){
								    echo "<a href=''></a>";
								     ?>
								    <a style="text-decoration: blink;" href="/sp/reports/<?php echo $this->Common->getReportLinkName($schedule['report_id']);?>?reportID=<?php echo $schedule['report_id']; ?>&clientID=<?php echo $res['ServiceCall']['client_id']; ?>&spID=<?php echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php echo $res['ServiceCall']['id']; ?>"><blink>Completed on <?php echo date('m/d/Y',strtotime($getCompleted)); ?></blink></a>
								 <?php
								}
								 if($getstarted == 0 && $getCompleted == ""){
								    echo "<blink>Not Started Yet</blink>";
								  }
								  else if($getstarted == 1 && $getCompleted == ""){
								    echo "<blink>Started</blink>";
								  }
							    ?>
							</td>							
						</tr>
						<tr><td colspan="5">
						<div class="service-hdline">Services Taken</div>
						<ul class="services-tkn"><li>
						<?php $i=0;$j=1;
						for($i=0;$i<count($schedule['ScheduleService']);$i++){
							echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
					        <?php if($j%4==0){
							echo '</li></ul><ul class="services-tkn"><li>';
						}
						else{
							echo '</li><li>';
						}
						 $j++;
						}
						?>
						</td></tr>
						<?php endforeach;?>
					</table>				
					</div>	
					<?php endforeach;
					} else {
					} ?>
    </div>
    <!--one complete form ends-->    
      </div>
</div>