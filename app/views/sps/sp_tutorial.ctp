<?php
echo $this->Html->script('jquery.1.6.1.min');    
    echo $this->Html->css(array('gallery/colorbox')); 
    echo $this->Html->script(array('gallery/jquery.colorbox'));  
   echo $this->Html->css(array('facebox'));
   echo $this->Html->script(array('facebox'));
?>
<script type="text/javascript" charset="utf-8">

		jQuery(document).ready(function(){			  
			  jQuery(".group1").each(function(){
				    //   alert(this.id);
			  });
			  jQuery(".ajax").colorbox();
			  
			  jQuery(".group1").colorbox({rel:'group1', transition:"fade",
				onComplete: function()
				{
				       jQuery("#colorbox").css({'top':'20px'});
					   //jQuery("#photoTag-wrap_150").css({'top':'0px'});
					  // jQuery("#cboxLoadedContent").attr("class","photoTag"); loadtag(this.id);
						
				    }
			  });
			  // Get all the thumbnail
		jQuery('div.thumbnail-item').mouseenter(function(e) {
		   // alert("yes");

			// Calculate the position of the image tooltip
			x = e.pageX - $(this).offset().left;
			y = e.pageY - $(this).offset().top;
			//alert(x+y); 

			// Set the z-index of the current item, 
			// make sure it's greater than the rest of thumbnail items
			// Set the position and display the image tooltip
			jQuery(this).css('z-index','15')
			.children("div.tooltip")
			.css({'top': y + 10,'right': x + 20,'display':'block','position':'fixed'});
			
		}).mousemove(function(e) {
			
			// Calculate the position of the image tooltip			
			x = e.pageX - $(this).offset().left;
			y = e.pageY - $(this).offset().top;
			
			// This line causes the tooltip will follow the mouse pointer
			jQuery(this).children("div.tooltip").css({'top': y + 10,'right': x + 20});
			
		}).mouseleave(function() {
			
			// Reset the z-index and hide the image tooltip 
			jQuery(this).css('z-index','1')
			.children("div.tooltip")
			.animate({"opacity": "hide"}, "fast");
		});
		});
</script>
<style>
table tr td{
    border:none;
}
.tbl td {font-size:14px; font-weight:bold; color:#25587E; }
a {color:#333333;}
#photoTag-add_150{
display:none;
}
#photoTag-taglist_150{display:none;}
#photoTag-removeTag36{
display:none;
}
#preview{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}
/*#easy_zoom{
	width:350px;
	height:350px;	
	border:5px solid #eee;
	background:#fff;
	color:#333;
	position:absolute;
	top:300px;
	left:450px;
	overflow:hidden;
	-moz-box-shadow:0 0 10px #777;
	-webkit-box-shadow:0 0 10px #777;
	box-shadow:0 0 10px #777;
	/* vertical and horizontal alignment used for preloader text */
	line-height:100px;
	text-align:center;
	}*/
#photoTag-add_150{
display:none;
}
#photoTag-removeTag36{
display:none;
}
#photoTag-wrap_150{
	top:0px;
}

.tooltip { 
	/* by default, hide it */
	display: none; 
	/* allow us to move the tooltip */
	position: absolute; 
	/* align the image properly */
	padding: 8px 0 0 8px;
}

.tooltip span.overlay { 
		/* the png image, need ie6 hack though */
		/* put this overlay on the top of the tooltip image */
		position: absolute; 
		top: 0px; 
		left: 0px; 
		display: block; 
		
}
.thumbnail-item { 
	/* position relative so that we can use position absolute for the tooltip */
	position: relative; 
	float: left;  
	margin: 0px 5px; 
}

.thumbnail-item a { 
	display: block; 
}

.thumbnail-item img.thumbnail {
	border:3px solid #ccc;	
}
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php //echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box">
	<h3 id="adduser">Educational Tutorial</h3>
	
	
    </div>
  
        <div id="box">
            
        
                    <table width="100%" style="border:none;" >
			<tr><td><?php echo $textData['TutorialText']['tutorial']; ?></td></tr>
                        <tr>
			    <td><h3 id="adduser">Screenshots [Click on the screenshot title to view screenshot]</h3></td>
			    
			</tr>
			<tr>
			    <td style="vertical-align:top;">
				<?php foreach($snapshotData as $data){ ?>
				<a style="color:#1C94C4" href="/img/snapshots/<?php echo $data['TutorialScreenshot']['image']?>" id="<?php echo $data['TutorialScreenshot']['id'] ?>" class="group1">
								<?php echo $data['TutorialScreenshot']['title']; ?></a><br/>
				
				<?php } ?>
			    </td>
			   
			</tr>
			<tr>
			    <td colspan="2">
				<h3>Videos</h3> 
			    </td>
			</tr>
			<tr>
			    <td style="vertical-align:top;">
				<?php foreach($videoData as $vData){  ?>
				<a href="javascript:void(0)" onclick="return playVideo('<?php echo $vData['TutorialVideo']['video']?>','<?php echo $vData['TutorialVideo']['video_thumb']?>','0'); " title="Click to View the Video"><img src="<?php echo $vData['TutorialVideo']['video_thumb']?>" width="100px;height:80px;"></a>
				<br/><br/>
				<?php } ?>
			    </td>
			    
			</tr>
		    </table>                
        </div>
</div>
<script type="text/javascript">
function playVideo(vidPath,thumbPath)
{	var str = '';
	str+='<div id="video">';
	//str+='<object width="220" height="21" type="application/x-shockwave-flash" data="/img/OriginalThinMusicPlayer.swf">';
	//str+='<param name="movie" value="/img/OriginalThinMusicPlayer.swf" />';
	//str+='<param name="flashvars" value="autoPlay=true&firstColor=0072bc&mediaPath='+audPath+'" />';
	//str+='</object>';
	str+='<object width="550" height="400">';
	
	str+='<param name="movie" value="/players/player_a.swf">';
	str+='<param name="flashvars" value="video='+vidPath+'&streamer=true">';
	str+='<embed src="/players/player_v.swf?video='+vidPath+'&streamer=true" width="550" height="400">';
	//str+='<embed src="/players/player_v.swf?video='+vidPath+'&streamer=true" width="550" height="400">';
	str+='</embed>';
	
	str+='</object>';
	str+='</div>';
	jQuery.facebox(str);
	
}

</script>