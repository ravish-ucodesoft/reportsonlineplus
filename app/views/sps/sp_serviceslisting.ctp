<?php
       echo $this->Html->script('jquery.1.6.1.min');
       echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
       echo $this->Html->script(array('jquery-ui-1.8.20.custom.min.js')); 
?>
<script type="text/javascript">
			$(function(){
			   
				// Accordion
			    $("#accordion").accordion({ header: "h3"});
			    
			    $('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); },
					function() { $(this).removeClass('ui-state-hover'); }
			    );    
			});
		</script>
    <style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			.ui-accordion-content{
			    
			}
		</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"><?php echo $html->link('Add Service',array('controller'=>'sps','action'=>'addService'),array("style"=>"padding-right:20px;")); ?></div>
	      
	      
              <div id="box" >
                	<h3 id="adduser">Service Listing</h3>
                        <br/>
                         <?php echo $this->Form->create('', array('controller'=>'sps','action' => 'serviceslisting','name'=>'webpages','id'=>'WebsitepagesShowForm') );?>
                        <table class="tbl">
                        <tr>
                        <!--<th style="width:10%;text-align:left;"><input type="checkbox"  name='checkall' onclick='checkedAllpage();' id='checkall'></th>-->
			    <th width="70%">Report Name</th>
			    <!--<th width="20%">Service Name</th>
			    <th width="20%">Status</th>-->		    
			   
                        </tr>
			
			<tr>
			    <td width="70%" align="left">
				<?php foreach($result  as $res) {
				    $cid = base64_encode($res['Report']['id']);
				 
				  
				?>
				<div id="accordion">
				<div>
				  <h3><a href="#"><?php echo $res['Report']['name']; ?></a></h3>
									
				<div>				    
			    	   <?php foreach($data as $data1){
					if($res['Report']['id'] == $data1['Service']['report_id']){
						echo "<table> <tr><td width='50%'>".$data1['Service']['name']."</td><td>".  $html->link('Edit '.$html->image('user_edit.png',array('alt'=>'Servive Edit','title'=>'Servive Edit')),array('controller'=>'sps','action'=>'editService',$cid),array('escape'=>false))."</td></tr></table>"; 
					}
				   }
				   ?>
				</div>
				<div></div>
				<?php } ?>
			    
			    </td>
                        </tr>
			
                      </table>
                      <table>
                              
                        <table> 
                      <?php echo $this->Form->end(); ?>
                </div>
	
            </div>
          
<script>
function prompt_activepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	
	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
	
      }
      
      if(flag==1)
      {
	      if(confirm('Are you sure you want to make this service active ?')){
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one service to active");
      return false;
      }
}

function prompt_inactivepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	
	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
      }
      
      if(flag==1)
      {
      
	      if(confirm('Are you sure you want to make this service inactive ?')){
		//document.webpages.action = '/navcake/admin/websitepages/inactive';
		//document.webpages.submit;
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one service to inactive");
      return false;
      }
}

function checkedAllpage ()
      {        
        if (document.getElementById('checkall').checked == false)
        {
        checked = false
        }
        else
        {
        checked = true
        }
        for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++)
        {
          document.getElementById('WebsitepagesShowForm').elements[i].checked = checked;
        }
      }
</script>             