<?php
    echo $this->Html->script('jquery.1.6.1.min');  
    echo $this->Html->css('validationEngine.jquery');    
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');    
    echo $this->Html->script('validation/only_num');    
?>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#reminderForm").validationEngine();    
});
</script>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
    <?php echo $this->Form->create('', array('url'=>array('controller'=>'sps','action' => 'setReminderMail',$this->params['pass'][0]),'id'=>'reminderForm','name'=>'reminderForm') );?>
    <section class="form-box" style="width:100%; margin: 0">
        <h2><strong>Set Reminder Mail</strong></h2>
        <ul class="register-form">                                
            <li>
                <label>Select Days</label>
                <p>
                    <?php
                        $optionsData = array(''=>'Select','1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,'10'=>10);                        
                        echo $this->Form->input('ReminderMail.days', array(
                            'type' => 'select',                            
                            'label' => false,
                            'div'=>false,                            
                            'options' => $optionsData,
                            'class'=>'validate[required]'
                            ));
                    ?>
                </p>
            </li>            
            <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>                       
            </li>
        </ul>
    </section>
    <?php echo $form->end(); ?>
    <br/>   
</div>