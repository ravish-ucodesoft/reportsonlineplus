<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#subscription").validationEngine();
});
</script>
<div class="register-wrap">
	<div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
	<h1 class="main-hdng"></h1>
      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'subscriptionplan'),'id'=>'subscription')); ?>
	<!--one complete form-->
    <div class="form-box" style="width:90%">
        <h2>Subscription</h2>
        <ul class="register-form">
            <li>
                <label>CCType<span>*</span></label>
                <p><span>
		    <?php
			$option=array('Visa'=>'Visa',
				'Mastercard'=>'Mastercard',
				'AmericanExpress'=>'AmericanExpress',
				'DiscoverCard'=>'DiscoverCard'
			      );
			echo $form->input('Company.cardtype', array('options' =>$option,'label'=>false,'class'=>'validate[required]','empty'=>'please select'));
                    ?>
		</span></p>
            </li>
            <li>
                <label>CCNo</label>
                <p><span class="input-field"><?php echo $form->input('Company.cardnumber', array('maxLength'=>'16','class'=>'text validate[required,custom[number]]',"div"=>false,"label"=>false));  ?></span></p>
            </li>
            <li>
                <label>CCV<span>*</span></label>
                <p><span class="input-field">
                      <?php echo $form->input('Company.cardcvnumber', array('maxLength'=>'4','class'=>'text validate[required,custom[number]]',"div"=>false,"label"=>false));  ?>
                </span></p>
            </li>
            <li>
                <label>Expiry Date<span>*</span></label>
                <p>
		<span>
		<?php
			$year=array();
			$i=date('Y');
		      while($i<(date('Y')+7))
		      {
			  $year[$i]=$i;
			  $i++; 
		      }
			  $month=array();
			  $i=1;
		      while($i<13)
		      {
			  $month[$i]=$i;
			  $i++; 
		      }
		       echo $this->Form->select('Company.month', $month,'', array('label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:150px;',"tabindex"=>"10",'empty'=>'please select'))."&nbsp;&nbsp;";
		       echo $this->Form->select('Company.year', $year,'', array('label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:150px;',"tabindex"=>"10",'empty'=>'please select'));
			  //echo $form->input('Company.month', array('options' =>$month,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0','class'=>'validate[required]','empty'=>'please select'));
			  //echo $form->input('Company.year', array('options' =>$year,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0','class'=>'validate[required]','empty'=>'please select'));
		?>
		</span></p>
				
            </li>
            <li>
                <label>Subscription Plan</label>
                <p>
		 <span>
		    <table cellpadding="0" cellspacing="0" border="1"> 
			<tr>
			   <th>No. of Site Addresses</th>
			   <th>Plan Name</th>
			   <th>Monthly Charges</th>                                            
			</tr>
		       <?php 
			foreach($SubscriptionData as $subscription){
			    if($subscription['SubscriptionDuration']['id']==1) continue;
			    $val = $subscription['SubscriptionDuration']['id']."-".$subscription['SubscriptionDuration']['monthlycharges']."-Monthly";
			     if($subscription_duration_monthly == $val){
				$select = 'checked="checked"';
			     }else {
				$select = "";
			     }
			?>
			<tr> 							
			<td>
			<?php echo $subscription['SubscriptionDuration']['min_allowed_site_address'].'-'.$subscription['SubscriptionDuration']['max_allowed_site_address']; ?>
			</td>
			<td>
			<?php echo $subscription['SubscriptionDuration']['plan_name']; ?>
			</td>
			<td>
			<input type="radio" <?php echo $select; ?> value="<?php echo $subscription['SubscriptionDuration']['id']."-".$subscription['SubscriptionDuration']['monthlycharges']; ?>-Monthly" id="subscription_plan" name="data[User][subscription_duration_monthly]" style="width:0px;" class="validate[required]"><?php echo "$ ".$subscription['SubscriptionDuration']['monthlycharges']; ?>
			</td>                        		
			</tr>
			<?php
			   }
			?>
                    </table>
		 </span></p>
            </li>
            <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
            </li>
        </ul>
    </div>
    <!--one complete form ends-->
</div>
<style type="text/css">
    div#flashMessage{
	font-size: 16px;
    }
    td{
	text-align: center;
    }
</style>    