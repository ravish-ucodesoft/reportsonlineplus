<?php
    //echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core.js','jquery-ui-1.8.20.custom.min.js')); 
?>
<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;
    width: 200px;
}
#box {
    border: none;
}
table {
    border-collapse: collapse;
    margin: 1px;
    width: 98%;
}
.form-box{ min-width: 380px;}
.butn {float:right;padding-right:20px;margin-bottom:10px;}
.butn a { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
.butn a:hover { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<script type="text/javascript">
    $(function(){
	//Accordion
	$("#setFormat").accordion({ header: "h3",active:false,collapsible: true});
	$("#setFormatIdle").accordion({ header: "h3",active:false,collapsible: true});
	$('#dialog_link, ul#icons li').hover(
	    function() { $(this).addClass('ui-state-hover'); },
	    function() { $(this).removeClass('ui-state-hover'); }
	);    
    });
    function PopupCenter(pageURL, title,w,h) {
	    var left = (screen.width/2)-(w/2);
	    var top = (screen.height/2)-(h/2);
	    var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}
    div.iconContainer{ text-align: right; width: 95%;}
</style>
<div id="content">
     <div id="box" style="width:800px;padding-left:10px;">
	<div class="iconContainer">
	    <?php $cid = base64_encode($result['User']['id']);?>
	    <?php echo $html->link($html->image('map.png',array('alt'=>'Address Map','title'=>'Address Map')),'javascript:void(0)',array('escape'=>false,'onclick'=>"PopupCenter('".$html->url(array('controller'=>'sps','action'=>'client_mapdirection',$cid))."','','760','490');")); ?>
	    <?php echo $html->link($html->image('nuvola_add16.png',array('alt'=>'Add Site Address','title'=>'Add Site Address')),array('controller'=>'sps','action'=>'addSiteAddress',$cid),array('escape'=>false)); ?>
	    <?php echo $html->link($html->image('icon-phone.gif',array('alt'=>'Create a Service Call?','title'=>'Create a Service Call?')),array('controller'=>'sps','action'=>'createservicecall',$cid),array('escape'=>false)); ?>
	    <?php echo $html->link($html->image('messageIcon.png',array('alt'=>'Send Message','title'=>'Send Message')),array('controller'=>'messages','action'=>'sendMessage',$cid),array('escape'=>false,'class'=>'ajax')); ?>
	    <?php echo $html->link($html->image('email-icon.gif',array('alt'=>'Send Email','title'=>'Send Email')),array('controller'=>'sps','action'=>'sendEmail',$cid),array('escape'=>false,'class'=>'ajax')); ?>
	</div>
	<div class="form-box" style="width:45%; float: left;">
	    <h2 style="font-weight:bold;">Client Information</h2>
	    <ul class="register-form">
		<li>
		    <label>Company Name</label>
		    <p><?php echo $result['User']['client_company_name']?></p>
		</li>
		<li>
		    <label>First Name</label>
		    <p><?php echo $result['User']['fname']?></p>
		</li>
		<li>
		    <label>Last Name</label>
		    <p><?php echo $result['User']['lname']?></p>
		</li>
		<li>
		    <label>Phone</label>
		    <p><?php echo $result['User']['phone']?></p>
		</li>
		<li>
		    <label>Address</label>
		    <p><?php echo $result['User']['address']?></p>
		</li>
		<li>
		    <label>Email</label>
		    <p><?php echo $result['User']['email']?></p>
		</li>
		<li>
		    <label>Country</label>
		    <p><?php echo $result['Country']['name']?></p>
		</li>
		<li>
		    <label>State</label>
		    <p><?php echo $result['State']['name']?></p>
		</li>
		<li>
		    <label>City</label>
		    <?php $userCity = ($result['City']['name']!="")?$result['City']['name']:$result['User']['city'];?>
		    <p><?php echo $userCity;?></p>
		</li>
		<li>
		    <label>Zip</label>
		    <p><?php echo $result['User']['zip']?></p>
		</li>
	    </ul>
	</div>
	<!--one complete form ends-->
    
	<div class="form-box" style="width:45%; float: left;">
	    <h2 style="font-weight:bold;">BillTo Information</h2>
	    <ul class="register-form">
		<li>
		    <label>First Name</label>
		    <p><?php echo $result['User']['billto_fname']?></p>
		</li>
		<li>
		    <label>Last Name</label>
		    <p><?php echo $result['User']['billto_lname']?></p>
		</li>
		<li>
		    <label>Phone</label>
		    <p><?php echo $result['User']['billto_phone']?></p>
		</li>
		<li>
		    <label>Address</label>
		    <p><?php echo $result['User']['billto_address']?></p>
		</li>
		<li>
		    <label>Email</label>
		    <p><?php echo $result['User']['billto_email']?></p>
		</li>
		<li>
		    <label>Country</label>
		    <p><?php echo $result['BillCountry']['name']?></p>
		</li>
		<li>
		    <label>State</label>
		    <p><?php echo $result['BillState']['name']?></p>
		</li>
		<li>
		    <label>City</label>
		    <?php $billCity = ($result['BillCity']['name']!="")?$result['BillCity']['name']:$result['User']['billto_city'];?>
		    <p><?php echo $billCity;?></p>
		</li>
		<li>
		    <label>Zip</label>
		    <p><?php echo $result['User']['billto_zip']?></p>
		</li>
	    </ul>
	</div>
    
    
	<!--one complete form-->
	<!--<div class="form-box" style="width:90%;">
	    <h2 style="font-weight:bold;">Responsible contact information for scheduling</h2>
	    <ul class="register-form">
		<li>
		    <label>Name</label>
		    <p><?php //echo $result['User']['responsible_person_name']?></p>
		</li>
		<li>
		    <label>Email</label>
		    <p><?php //echo $result['User']['responsible_person_email']?></p>
		</li>
		<li>
		    <label>Phone Number</label>
		    <p><?php //echo $result['User']['responsible_contact']?></p>
		</li>
	    </ul>
	</div>-->
    
	<!--one complete form-->
	<div class="form-box" style="width:90%">
	    <h2 style="font-weight:bold;">Site Address Information(Total: <?php echo $allCount;?>, Active: <?php echo sizeof($activeAddr);?>, Idle: <?php echo sizeof($idleAddr);?> )</h2>
	</div>
	
	<div class="form-box" style="width:90%">
	    <h2 style="font-weight:bold;">Active Site Addresses</h2>
	    <div id="setFormat">
	    <?php
		if(sizeof($activeAddr)>0){
		foreach($activeAddr as $siteData){
	    ?>	    
	    <h3><a href="javascript:void(0);"><?php echo ucwords($siteData['SpSiteAddress']['site_name']);?></a></h3>
	    <div>	
		<ul class="register-form">
		    <li>
			<label>Site Name</label>
			<p><?php echo $siteData['SpSiteAddress']['site_name'];?></p>
		    </li>
		    <li>
			<label>Site Phone</label>
			<p><?php echo $siteData['SpSiteAddress']['site_phone']?></p>
		    </li>
		    <li>
			<label>Site Email</label>
			<p><?php echo $siteData['SpSiteAddress']['site_email']?></p>
		    </li>
		    <li>
			<label>Site Address</label>
			<p><?php echo $siteData['SpSiteAddress']['site_address']?></p>
		    </li>
		    <li>
			<label>Site Contact Name</label>
			<p><?php echo $siteData['SpSiteAddress']['site_contact_name']?></p>
		    </li>
		    <li>
			<label>Country</label>
			<p><?php echo $this->Common->getCountryName($siteData['SpSiteAddress']['country_id'])?></p>
		    </li>
		    <li>
			<label>State</label>
			<p><?php echo $this->Common->getStateName($siteData['SpSiteAddress']['state_id'])?></p>
		    </li>
		    <li>
			<label>City</label>
			<?php $siteCity = ($siteData['City']['name']!="")?$siteData['City']['name']:$siteData['SpSiteAddress']['site_city'];?>
			<p><?php echo $siteCity;?></p>
		    </li>
		    <li>
			<label>Zip</label>
			<p><?php echo $siteData['SpSiteAddress']['site_zip']?></p>
		    </li>
		    <li>
			<h2 style="font-weight:bold;">Responsible contact information for scheduling</h2>		
		    </li>
		    <li>
		    <label>Name</label>
			<p><?php echo $siteData['SpSiteAddress']['responsible_person_name']?></p>
		    </li>
		    <li>
			<label>Email</label>
			<p><?php echo $siteData['SpSiteAddress']['responsible_person_email']?></p>
		    </li>
		    <li>
			<label>Phone Number</label>
			<p><?php echo $siteData['SpSiteAddress']['responsible_contact']?></p>
		    </li>
		    <li>
			<div class="butn">
			    <?php echo $html->link('Make This Site Idle',array('controller'=>'sps','action'=>'makeidle',$siteData['SpSiteAddress']['id']),array('title'=>'Make this site address idle','confirm'=>'Are you sure that you want to make this site Idle ? by making this Idle - you will not have access to any of the information other then the site name and option to make active again. Once Idle - you will have no access to the reports or site information.'));?>
			    <?php echo $html->link('Delete',array('controller'=>'sps','action'=>'makedelete',$siteData['SpSiteAddress']['id']),array('title'=>'Delete','confirm'=>'Are you sure that you want to delete all documents and history associated with this site?'));?>
			</div>    
		    </li>
		    <li>
			<label>&nbsp;</label>		
		    </li>	   
		</ul>
	    </div>	
	    <?php }}else{?>
	    <ul class="register-form">
		<li>		    
		    No Active site address has been added for this client.
		</li>
	    </ul>    
	    <?php }?>
	    </div>
	</div>
	<!--one complete form ends-->
	
	<div class="form-box" style="width:90%">
	    <h2 style="font-weight:bold;">Idle Site Addresses</h2>
	    <div id="setFormatIdle">
	    <?php
		if(sizeof($idleAddr)>0){
		foreach($idleAddr as $siteData){
	    ?>	    
	    <h3><a href="javascript:void(0);"><?php echo ucwords($siteData['SpSiteAddress']['site_name']);?></a></h3>
	    <div>	
		<ul class="register-form">
		    <li>
			<label>Site Name</label>
			<p><?php echo $siteData['SpSiteAddress']['site_name'];?></p>
		    </li>
		    <li>
			<label>Site Phone</label>
			<p><?php echo $siteData['SpSiteAddress']['site_phone']?></p>
		    </li>
		    <li>
			<label>Site Email</label>
			<p><?php echo $siteData['SpSiteAddress']['site_email']?></p>
		    </li>
		    <li>
			<label>Site Address</label>
			<p><?php echo $siteData['SpSiteAddress']['site_address']?></p>
		    </li>
		    <li>
			<label>Site Contact Name</label>
			<p><?php echo $siteData['SpSiteAddress']['site_contact_name']?></p>
		    </li>
		    <li>
			<label>Country</label>
			<p><?php echo $this->Common->getCountryName($siteData['SpSiteAddress']['country_id'])?></p>
		    </li>
		    <li>
			<label>State</label>
			<p><?php echo $this->Common->getStateName($siteData['SpSiteAddress']['state_id'])?></p>
		    </li>
		    <li>
			<label>City</label>
			<p><?php echo $siteData['SpSiteAddress']['site_city']?></p>
		    </li>
		    <li>
			<label>Zip</label>
			<p><?php echo $siteData['SpSiteAddress']['site_zip']?></p>
		    </li>
		    <li>
			<h2 style="font-weight:bold;">Responsible contact information for scheduling</h2>		
		    </li>
		    <li>
		    <label>Name</label>
			<p><?php echo $siteData['SpSiteAddress']['responsible_person_name']?></p>
		    </li>
		    <li>
			<label>Email</label>
			<p><?php echo $siteData['SpSiteAddress']['responsible_person_email']?></p>
		    </li>
		    <li>
			<label>Phone Number</label>
			<p><?php echo $siteData['SpSiteAddress']['responsible_contact']?></p>
		    </li>
		    <li>
			<div class="butn">
			    <?php echo $html->link('Request to Make This Site Active Again',array('controller'=>'sps','action'=>'request_activation',$siteData['SpSiteAddress']['id']),array('title'=>'Request to make this site address active again','confirm'=>'Are you sure that you want to make this site active again ? by making this request - your request will send to administrator of reportsonlineplus.'));?>
			    <?php echo $html->link('Delete',array('controller'=>'sps','action'=>'makedelete',$siteData['SpSiteAddress']['id']),array('title'=>'Delete','confirm'=>'Are you sure that you want to delete all documents and history associated with this site?'));?>
			</div>    
		    </li>
		    <li>
			<label>&nbsp;</label>		
		    </li>	   
		</ul>
	    </div>	
	    <?php }}else{?>
	    <ul class="register-form">
		<li>		    
		    No Idle site address has been added for this client.
		</li>
	    </ul>    
	    <?php }?>
	    </div>
	</div>
	<!--one complete form ends-->
    </div>     
</div>