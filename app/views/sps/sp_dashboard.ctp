    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Year');
        data.addColumn('number', 'Inspections');
        data.addRows(12);
        data.setValue(0, 0, 'January');
        data.setValue(0, 1, <?php echo $final_value[0];?>);
        data.setValue(1, 0, 'Feburary');
        data.setValue(1, 1, <?php echo $final_value[1];?>);
        data.setValue(2, 0, 'March');
        data.setValue(2, 1, <?php echo $final_value[2];?>);
        data.setValue(3, 0, 'April');
        data.setValue(3, 1, <?php echo $final_value[3];?>);
        data.setValue(4, 0, 'May');
        data.setValue(4, 1, <?php echo $final_value[4];?>);
        data.setValue(5, 0, 'June');
        data.setValue(5, 1, <?php echo $final_value[5];?>);
        data.setValue(6, 0, 'July');
        data.setValue(6, 1, <?php echo $final_value[6];?>);
        data.setValue(7, 0, 'Augest');
        data.setValue(7, 1, <?php echo $final_value[7];?>);
        data.setValue(8, 0, 'September');
        data.setValue(8, 1, <?php echo $final_value[8];?>);
        data.setValue(9, 0, 'October');
        data.setValue(9, 1, <?php echo $final_value[9];?>);
        data.setValue(10, 0, 'November');
        data.setValue(10, 1, <?php echo $final_value[10];?>);
        data.setValue(11, 0, 'December');
        data.setValue(11, 1, <?php echo $final_value[11];?>);

        var options = {
          title: 'Schedule Inspections',
          hAxis: {title: 'Months', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
	<style>

.steps_dash li { float:left; margin:5px; list-style:none;font-size: 12px;}
.step_box { width:120px; min-height:86px; text-align:center; color:#999; padding:40px 10px 10px; border-radius:70px; -moz-border-radius:70px; -webkit-border-radius:70px; -o-border-radius:70px; border:2px solid #c6c6c6; overflow:hidden;
box-shadow:2px 5px 6px -8px #000000; 
-moz-box-shadow:2px 5px 6px -8px #000000;
-o-box-shadow:2px 5px 6px -8px #000000;
-webkit-box-shadow:2px 5px 6px -8px #000000;
}
.step_box:hover { background:#0770CA; color:#fff; text-shadow:1px 1px 1px #000}
.steps_dash li a { text-decoration:none; float:left}
.start { border:2px solid #c6c6c6; background:#efefef; color:#666}
.end { border:2px solid #c6c6c6; background:#efefef; color:#666}
.pad_top_10 { padding-top:10px;}
.arrow_step { background:url(/app/webroot/img/arrow_step.png) no-repeat center right; padding-right:15px; float:left; width:28px; height:90px; padding-top:50px}
</style>
<div style="margin:auto; width:98%">
<div class="left-wrapper" style="width:25%">
<ul>
      <li><a href="/sp/sps/dashboard">DashBoard</a></li>
      <li><a href="/sp/schedules/schedule">Schedule</a></li>
      <li><a href="/sp/sps/changepasswd">Change Password</a></li>
      <li><a href="/sp/sps/clientlisting">Clients<span style="color:red;">(<?php echo $count_clients ?>)</span></a></li>
      <li><a href="/sp/sps/inspectorlisting">Inspectors<span style="color:red;">(<?php echo $count_inspectors ?>)</span></a></li>
      <li><a href="/sp/messages/messages">Messages<span style="color:red;">(<?php echo $count_messages ?>)</span></a></li>
      <li><a href="/sp/sps/cronschedule">Cron Schedule</a></li>
      <li><a href="/sp/messages/general_company_announcement">Company Announcement</a></li>
      <li><a href="/sp/reportPrints/reportText">Report Text</a></li>
      <li><a href="/sp/messages/blank_cert_form">Certificates</a></li>
      <li><a href="/sp/sps/logout">Logout</a></li>
    </ul>
</div>
  <div class="mdl-wrapper" style="width:70%">
      
		<ul class="steps_dash">
   <li>
      <a href="/sp/sps/dashboard"><section class="step_box start"><h1>Your Dashboard</h1></section></a>
      <div class="arrow_step"></div>
   </li>
   <li>
      <a href="/sp/sps/addInspector"><section class="step_box"><h3 class="pad_top_10">Create your Inspectors</h3></section></a>
      <div class="arrow_step"></div>
   </li>
   <li>
      <a href="/sp/sps/addclients"><section class="step_box"><h3 class="pad_top_10">Add Your Clients</h3></section></a>
      <div class="arrow_step"></div>
   </li>
   <li>
      <a href="/sp/sps/addclients"><section class="step_box"><h3 class="pad_top_10">Add Client's Site Address</h3></section></a>
      <div class="arrow_step"></div>
   </li>
   <?php $setDate=date('Y-m-d',strtotime(date('Y-m-d').'+1 day'));?>
    <li>
      <a href="/sp/schedules/createServiceCallByCalendar/<?php echo $setDate?>"><section class="step_box"><h3>Create a Service Call for your client</h3></section></a>
      <div class="arrow_step"></div>
   </li>
    <li>
      <a href="/sp/schedules/schedule"><section class="step_box"><h3>Now you can view schedule for your clients</h3></section></a>
      <div class="arrow_step"></div>
  </li>
   <li>
      <a href="/sp/schedules/completedschedule"><section class="step_box"><h3>At Last View Inspection Completed by your inspector</h3></section></a>
   </li>
</ul>
	
     <div style="clear:both"></div>
      <div style="float:left;padding-right:10px;text-decoration:blink;"><a href="/sp/sps/tutorial"><strong>Click Here</strong></a> to view educational tutorial</div>
      <div style="clear:both"></div>
      <!--<div style="float:left;padding-right:10px;"><strong>You can add maximum <font style="color:red;size:14px;"><?php echo $max_site_address['SubscriptionDuration']['max_allowed_site_address']?></font> site address as free trial period. </strong><?php echo $html->link('Click Here To Add',array('controller'=>'sps','action'=>'view_site_address'));?></div>-->
      <div style="clear:both"></div>
      <div style="float:left;padding-right:10px;"><strong>Total amount of inspection(Number of service calls created till <?php echo date('m/d/Y')?>):</strong><a href="/sp/schedules/schedule"><span style="color:red;font-size:16px;font-weight:bold;"><?php echo $count_servicecalls; ?></span></a></div>
      <div style="width:100%">
      <div style="clear:both;text-align:left;">
      </div>
       <?php //if($sum != 0){ ?>
      <div  style="float:right;padding-right:10px;width:60%;">
	    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'dashboard') ,'name'=>'searchgraphform')); ?>
       <ul>
            <li>
                <label>&nbsp;</label>
                <p>
		  <span class=""><?php echo $this->Form->select('User.year', $preMonths,$year, array('id'=>'UserCountryId','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:150px;','empty'=>'-Select Year-'));?></span>
		  <span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span>
		
		</p>
            </li>
       </ul>
	    <?php echo $form->end(); ?>
	    
      </div>
      <?php //} ?>
      </div>
   <?php if($sum != 0){ ?>
    <table width="100%" cellpadding="3" cellspacing="3" border="0" class="tbl">
      
       <tr>
	   <td>
		  <div id="chart_div" style="width: 690px; height: 300px;"></div>
		  </div>
	    
	   </td>		
       </tr>
       
    </table>
    <div style="clear:both;text-align:left;"><h2 style="color: #222222;
    font-size: 14px;
    font-weight: normal;
    padding: 10px 0;"></h2></div>
    <table width="100%" cellpadding="3" cellspacing="3" border="0" class="tbl">
       <tr>
	   <td>January</td>
	   <td>Feburary</td>
           <td>March</td>
           <td>April</td>
           <td>May</td>
	   <td>June</td>
	   <td>July</td>
	   <td>Augest</td>
           <td>September</td>
	   <td>October</td>
	   <td>November</td>
	   <td>December</td>
	   <td><strong>Total</strong></td>
      </tr>
       <tr>
	   <td><?php //echo $final_value[0]; ?><?php echo $html->link($final_value[0],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
	   <td><?php //echo $final_value[1]; ?><?php echo $html->link($final_value[1],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
           <td><?php //echo $final_value[2]; ?><?php echo $html->link($final_value[2],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
           <td><?php //echo $final_value[3]; ?><?php echo $html->link($final_value[3],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
           <td><?php //echo $final_value[4]; ?><?php echo $html->link($final_value[4],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
	   <td><?php //echo $final_value[5]; ?><?php echo $html->link($final_value[5],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
	   <td><?php //echo $final_value[6]; ?><?php echo $html->link($final_value[6],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
	   <td><?php //echo $final_value[7]; ?><?php echo $html->link($final_value[7],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
           <td><?php //echo $final_value[8]; ?><?php echo $html->link($final_value[8],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
	   <td><?php //echo $final_value[9]; ?><?php echo $html->link($final_value[9],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
	   <td><?php //echo $final_value[10]; ?><?php echo $html->link($final_value[10],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
	   <td><?php //echo $final_value[11]; ?><?php echo $html->link($final_value[11],array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></td>
	   </td><td><strong><?php //echo $sum; ?><?php echo $html->link($sum,array('controller'=>'schedules','action'=>'schedule','view:accordion'),array('style'=>'color:#25587E')); ?></strong></td>
      </tr>
    </table>
    </div>
<div style="clear:both;text-align:left;"><h2 style="color: #222222;
    font-size: 14px;
    font-weight: normal;
    padding: 10px 0;"></h2></div>
    <?php }else { ?>
    <table width="100%" cellpadding="3" cellspacing="3" border="0" class="tbl">
       <tr>
	   <td>
		 No Inspection Found
	   </td>
	   <td><a href="/sp/sps/dashboard">Go Back</a></td>
       </tr>
       </table>
    <?php } ?>
    <div style="clear:both;text-align:left;"><h2 style="color: #222222;
    font-size: 14px;
    font-weight: normal;
    padding: 10px 0;">Personel Information</h2></div>
    <table width="100%" cellpadding="3" cellspacing="3" border="0" class="tbl">
       <tr>
	   <td width="30%">Name</td><td><?php echo ucfirst($spsession['User']['fname']." ".$spsession['User']['lname']); ?></td>		
       </tr>
       <tr>
	   <td>Email</td><td><?php echo $spsession['User']['email']; ?></td>		
       </tr>
    </table>
    <div style="clear:both;text-align:left;"><h2 style="color: #222222;
    font-size: 14px;
    font-weight: normal;
    padding: 10px 0;">Company Information</h2></div>
    <table width="100%" cellpadding="3" cellspacing="3" border="0" class="tbl">
       <tr>
	   <td width="30%">Name</td><td><?php echo ucfirst($spsession['Company']['name']); ?></td>		
       </tr>
       <tr>
	   <td>Address</td><td><?php echo $spsession['User']['address']; ?></td>		
       </tr>
       <tr>
	   <td>Country</td><td><?php echo $spsession['Country']['name']; ?></td>		
       </tr>
       <tr>
	   <td>State</td><td><?php echo $spsession['State']['name']; ?></td>		
       </tr>
       <tr>
	   <td>City</td><td><?php echo $spsession['User']['city']; ?></td>		
       </tr>
       <tr>
	   <td>Zip</td><td><?php echo $spsession['User']['zip']; ?></td>		
       </tr>
       <tr>
	   <td>Phone</td><td><?php echo $spsession['User']['phone']; ?></td>		
       </tr>
        <tr>
	   <td>Type of Business that you frequently provide inspections</td>
	   <td>
      <?php $arr=explode(',',$spsession['Company']['type_business']);
											
      foreach($arr as $key=>$val):
      foreach($businessType as $key1=>$val1):
      
      
      if($val== $key1)
      {
	      echo $val1."</br>";
      }
      
      endforeach;
      
      endforeach;
		      ?>
	   </td>
       </tr>
	<tr>
	    <td>Applicable Services that you specialize </td>
	    <td>
	    <?php $arrReport=explode(',',$spsession['Company']['interest_service']);
	    
	    foreach($arrReport as $key=>$val):
	    foreach($reportType as $key1=>$val1):
	    
	    
	    if($val== $key1)
	    {
		    echo $val1."</br>";
	         
	    }
	   
	    endforeach;
	   
	    endforeach;
	    ?></td>		
       </tr>
	<?php //pr($spsession); die();?>
	<tr>
	   <td>Website</td><td><?php echo $spsession['Company']['website']; ?></td>		
       </tr><?php //pr($spsession);die;?>
	<?php foreach($spsession['Company']['UserCompanyPrincipalRecord'] as $key=>$val){ ?>
	    <tr>
		  <td><?php echo $pName = $this->Common->getPrincipalName($val['user_company_principal_id']); ?></td><td><?php echo $val['manager_name']." ".$val['manager_email']; ?></td>		
	    </tr>
	<?php } ?>
	<!-- <tr>
	   <td>Site Manager Name</td><td><?php echo $spsession['Company']['site_manager']; ?></td>		
       </tr>
	<tr>
	   <td>Site Manager Phone</td><td><?php echo $spsession['Company']['phone_site_manager']; ?></td>		
       </tr>
	<tr>
	   <td>Site Manager Email</td><td><?php echo $spsession['Company']['email_site_manager']; ?></td>		
       </tr>
	<tr>
	   <td>Company Bio</td><td><?php echo $spsession['Company']['company_bio']; ?></td>		
       </tr>
	<tr>
	   <td>Salemanager Name</td><td><?php echo $spsession['Company']['principle_name_salemanager']; ?></td>		
       </tr>
	<tr>
	   <td>Salemanager Email</td><td><?php echo $spsession['Company']['principle_email_salemanager']; ?></td>		
       </tr>
	<tr>
	   <td>Servicemanager Name</td><td><?php echo $spsession['Company']['principle_name_servicemanager']; ?></td>		
       </tr>
	<tr>
	   <td>Servicemanager Email</td><td><?php echo $spsession['Company']['principle_email_servicemanager']; ?></td>		
       </tr>
	<tr>
	   <td>Inspection Manager Name</td><td><?php echo $spsession['Company']['principle_name_inspectionmanager']; ?></td>		
       </tr>
	<tr>
	   <td>Inspection Manager Email</td><td><?php echo $spsession['Company']['principle_email_inspectionmanager']; ?></td>		
       </tr>
	<tr>
	   <td>Scheduler Name</td><td><?php echo $spsession['Company']['principle_name_scheduler']; ?></td>		
       </tr>
	<tr>
	   <td>Scheduler Email</td><td><?php echo $spsession['Company']['principle_email_scheduler']; ?></td>		
       </tr>
	<tr>
	   <td>Deficiency Manager Name</td><td><?php echo $spsession['Company']['principle_name_deficiencymanager']; ?></td>		
       </tr>
	<tr>
	   <td>Deficiency Manager Email</td><td><?php echo $spsession['Company']['principle_email_deficiencymanager']; ?></td>		
	</tr> -->
	<tr>
	   <td colspan="2" align="right;">
		  <!--<div class="btn2"><span class="lt"><?php echo $html->link('<span>Edit info</span>',array('controller'=>'sps','action'=>'editinfo',base64_encode($spsession['User']['id'])))?></span><?php echo $html->image('user_edit.png',array('alt'=>'User Edit','title'=>'User Edit'));?></div>-->
		  <a href="/sp/sps/editinfo/<?php echo base64_encode($spsession['User']['id']);?>" class="blue-btn"><span>Edit info</span></a>
	   </td>		
       </tr>
    </table>
  </div>
  
  <div class="right-wrapper">
	
  </div>
  <br/><br/>
  
  </div>