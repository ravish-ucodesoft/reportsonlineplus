<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<style>
a, a:visited {
    color: #2281CF;
	text-decoration:none;
}
a:hover {
    color: #3688B8;
	text-decoration:underline;	
}
.blue-btn:hover{color:#fff !important;}
#button1 { border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; -o-border-radius:4px;}
#button1:hover { border:1px solid #0684da; background:url("../../img/newdesign_img/nav_active.png"); color:#fff !important; cursor:pointer }
</style>

<script type="text/javascript">

/*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
</script>



<div id="content">
<div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
<div class="add_client"><b><?php echo $html->link('Add New Client/Customer',array('controller'=>'sps','action'=>'addclients'),array("style"=>"padding-right:20px;")); ?></b></div>

              <div id="box" >
		<h3 id="adduser">Client Listing</h3>
		

		<!--<div style="float:right;padding-right:10px;"><p class="bttm-btns"><a href="javascript:void(0);" class="blue-btn" id="filterbtn"><span>Filter</span></a></p></div><br/>-->
		<?php
		if(isset($this->passedArgs) && !empty($this->passedArgs))
		{
		  $style="'width:100%;display:block'";
		}
		else
		{
		  $style="'width:100%;display:none'";
		}
		?>
		<!--<div id="filter" class="form-box" style=<?php echo $style;?>>
		   
		  <?php /*$fname ='';
				  $lname='';
				  $address='';
				  $email='';
				  $phone='';
				  $site_name='';
				  $site_address='';
				  $site_cname='';
				  $site_phone='';
				  $site_email='';
				if(isset($this->passedArgs['User.fname'])){
					$fname = $this->passedArgs['User.fname'];
				} 
				if(isset($this->passedArgs['User.lname'])){
					$lname = $this->passedArgs['User.lname'];
				}
				if(isset($this->passedArgs['User.address'])){
					$address = $this->passedArgs['User.address'];
				}
				if(isset($this->passedArgs['User.email'])){
					$email = $this->passedArgs['User.email'];
				}
				if(isset($this->passedArgs['User.phone'])){
					$phone = $this->passedArgs['User.phone'];
				}
				if(isset($this->passedArgs['User.site_name'])){
					$site_name = $this->passedArgs['User.site_name'];
				}
				if(isset($this->passedArgs['User.site_email'])){
					$site_email = $this->passedArgs['User.site_email'];
				}
				if(isset($this->passedArgs['User.site_phone'])){
					$site_phone = $this->passedArgs['User.site_phone'];
				}
				if(isset($this->passedArgs['User.site_address'])){
					$site_address = $this->passedArgs['User.site_address'];
				}
				if(isset($this->passedArgs['User.site_cname'])){
					$site_cname = $this->passedArgs['User.site_cname'];
				}*/
		   
		   ?>
		   
		   <?php //echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'clientlisting'),'id'=>'clientlisting','onsubmit'=>'return validate();','div'=>false,'label'=>false)); ?>
		   <table width="100%">
		    <tr><td width="50%" valign="top">
		   
		   <ul class="register-form" style="border:none;">
            <li>
                <label>First Name</label>
                <p><span class="input-field"><?php //echo $form->input('User.fname', array('maxLength'=>'50','class'=>'',"div"=>false,"label"=>false,'value'=>$fname));  ?></span></p>
            </li>
            <li>
                <label>Last Name</label>
                <p><span class="input-field"><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false,'value'=>$lname));  ?></span></p>
            </li>
            <li>
                <label>Phone</label>
                <p><span class="input-field"><?php echo $form->input('User.phone', array('maxLength'=>'20',"div"=>false,"label"=>false,'class'=>'','value'=>$phone));  ?></span></p>
            </li>
            <li>
                <label>Email</label>
                <p><span class="input-field"><?php echo $form->input('User.email', array('maxLength'=>'50',"div"=>false,"label"=>false,'class'=>'','value'=>$email));  ?></span></p>
            </li>
            <li>
                <label>Address</label>
                <p><span class="input-field"><?php echo $form->input('User.address',array('maxLength'=>'50','type'=>'text','class'=>'','div'=>false,"label"=>false,'value'=>$address)); ?></span></p>
				
            </li>
	     
        </ul></td>
		<td>
	    <ul class="register-form" style="border:none;">
            <li>
                <label>Site Name</label>
                <p><span class="input-field"><?php echo $form->input('User.site_name', array('maxLength'=>'50','class'=>'',"div"=>false,"label"=>false,'value'=>$site_name));  ?></span></p>
            </li>
            <li>
                <label>Site Address</label>
                <p><span class="input-field"><?php echo $form->input('User.site_address', array('maxLength'=>'50',"div"=>false,"label"=>false,'value'=>$site_address));  ?></span></p>
            </li>
            <li>
                <label>Site Contact Name</label>
                <p><span class="input-field"><?php echo $form->input('User.site_cname', array('maxLength'=>'20',"div"=>false,"label"=>false,'class'=>'','value'=>$site_cname));  ?></span></p>
            </li>
            <li>
                <label>Site Phone</label>
                <p><span class="input-field"><?php echo $form->input('User.site_phone', array('maxLength'=>'50',"div"=>false,"label"=>false,'class'=>'','value'=>$site_phone));  ?></span></p>
            </li>
            <li>
                <label>Site Email</label>
                <p><span class="input-field"><?php echo $form->input('User.site_email',array('maxLength'=>'50','type'=>'text','class'=>'','div'=>false,"label"=>false,'value'=>$site_email)); ?></span></p>		
            </li>
	    
	    <li>
                <label>&nbsp;</label>
                <p class="bttm-btns" style="float:right;margin-right:33px;"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
            </li>
	     
        </ul>
	    
	</td></tr> </table>
		   <?php //echo $form->end(); ?>
		</div>-->
        <table class="tbl" style="border-collapse:collapse;">
          <thead>
	        <tr>
		    <th width="3%">S.No</th>
		    <!--<th width="20%">Client Name</th>-->
		    <th width="10%"><?php echo $paginator->sort('Company Name','User.client_company_name');?></th>
		    <th width="20%"><?php echo $paginator->sort('Company Address','User.address');?></th>
		    <th width="8%">Site Addresses</th>		    
		    <!--<th width="12%">Inspections</th>->
		    <!--<th width="8%">Attachments</th>
		    <th width="8%">Work Orders</th>
		    <th width="15%">Status</th>
		    <th width="5%">Quotes</th>-->
		    <th width="12%">Approved Calls</th>
		    <th width="8%">Pending Calls</th>
		    <th width="8%">Completed Reports</th>
		    <th width="10%">In Progress Reports</th>
		    <th width="10%">Not Started Reports</th>
		    <th width="18%">Action &amp; Notes</th>
		</tr>
	  </thead>
	 
			<?php
			    $i = 1;
			    foreach($result  as $res){
			    $cid = base64_encode($res['User']['id']);
			    $getStatus = $this->Common->getStatus($res['User']['id']);
			    if($getStatus != "empty"){
				$getStatus = $getStatus;
			     }else {
				$getStatus = "";
			     }
	
    	?>
    	
			<tr>
				<td  align="center" style="vertical-align:middle"><?php echo $i; ?></td>
				<!--<td  align="center" style="vertical-align:middle"><?php //echo $html->link(ucwords($res['User']['fname'].' '.$res['User']['lname']),array('controller'=>'sps','action'=>'client_detail',$cid),array('class' => 'ajax')); ?></td>-->
				<td  align="center" style="vertical-align:middle"><?php echo $html->link(ucwords($res['User']['client_company_name']),array('controller'=>'sps','action'=>'client_detail',$cid),array('class' => 'ajax')); ?></td>
				<td  align="center" style="vertical-align:middle">
				<?php
				    $userCity = ($res['City']['name']!="")?$res['City']['name']:$res['User']['city'];
				?>
				<?php $addr= $res['User']['address'].','.$userCity.'<br/>'.$res['State']['name'].'-'.$res['User']['zip'].'<br/>'.$res['Country']['name']; ?>
				
				
				<?php echo $html->link($addr,'javascript:void(0)',array('escape'=>false,'onclick'=>"PopupCenter('".$html->url(array('controller'=>'sps','action'=>'client_mapdirection',$cid))."','','760','490');")); ?></td>
				<td  align="center" style="vertical-align:middle"><a href="/sp/sps/sitelist/A/<?php echo $res['User']['id'];?>">Total:<?php echo sizeof($res['SpSiteAddress']); ?></a><br/><a href="/sp/sps/sitelist/A/<?php echo $res['User']['id'];?>">Active:<?php echo $this->Common->active_siteaddress($res['User']['id']);?></a><br/><a href="/sp/sps/sitelist/I/<?php echo $res['User']['id'];?>">Idle:<?php echo $this->Common->idle_siteaddress($res['User']['id']);?></a></td>
				<!--<td  align="center" style="vertical-align:middle"><?php //echo $res['User']['responsible_contact']; ?></td>-->
				<?php
				    $count = $this->Common->getNoOfServicesApproved($res['User']['id']);
				    $pendingcount = $this->Common->getNoOfServicesPending($res['User']['id']);
				?>
				<td  align="center" style="vertical-align:middle">
				<?php				
				if($count == "empty"){
				    echo "NA";
				}else{
				    echo $html->link($count,array('controller'=>'sps','action'=>'serviceCallDetail',$cid),array('class' => 'ajax'));
				}
				?>
				</td>
				<td  align="center" style="vertical-align:middle">
				<?php				
				if($count == "empty"){
				    echo "NA";
				}else{
				    echo $html->link($pendingcount,array('controller'=>'sps','action'=>'pendingServiceCallDetail',$cid),array('class' => 'ajax'));
				}
				?>
				</td>
				<td  align="center" style="vertical-align:middle">
				<?php				
				if($count == "empty"){
				    echo "NA";
				}else{
				    $completed = $this->Common->getNoOfCompletedReports($res['User']['id']);
				    echo $html->link(@$completed,array('controller'=>'sps','action'=>'serviceCallDetail','reportType:completed',$cid),array('class' => 'ajax','title'=>'Completed Reports'));
				}
				?>
				</td>
				<td  align="center" style="vertical-align:middle">
				<?php				
				if($count == "empty"){
				    echo "NA";
				}else{
				    $pending = $this->Common->getNoOfPendingReports($res['User']['id']);
				    echo $html->link(@$pending,array('controller'=>'sps','action'=>'serviceCallDetail','reportType:in_progress',$cid),array('class' => 'ajax','title'=>'In Progress Reports'));
				}
				?>
				</td>
				<td  align="center" style="vertical-align:middle">
				<?php				
				if($count == "empty"){
				    echo "NA";
				}else{
				    $TotalReports = $this->Common->getTotalReports($res['User']['id']);
				    echo $html->link(@$TotalReports-$completed-$pending,array('controller'=>'sps','action'=>'serviceCallDetail','reportType:not_started',$cid),array('class' => 'ajax','title'=>'Not Started Reports'));
				}
				?>
				</td>
				<?php			        
                                        //echo '<br/><span style="color:red;font-size:9px">Reports:</span>';  
					
					
					
				     ?>
				<!--<td  align="center" style="vertical-align:middle"><?php //$count1 = $this->Common->getTotalAttachemts($res['User']['id']); if($count1 == "empty"){ echo "NA"; }else { echo $html->link($count1,array('controller'=>'sps','action'=>'attachmentDetail',$cid),array('class' => 'ajax')); } ?></td>
				<td  align="center" style="vertical-align:middle"><?php //echo "NA"; ?></td>			    
				<td  align="center" style="vertical-align:middle">
				
		<table>
		  <?php //echo $form->create('', array('id'=>'addclient'.$res['User']['id'],'div'=>false)); ?>
				      <?php //echo $form->input('ClientCheck.id',array('type'=>'hidden','id'=>'userid','div'=>false,'label'=>false,'value'=>$res['User']['id'])); ?>     
		<tr><td style=" border:0px;vertical-align:top">
	  	<?php //echo $form->select('ClientCheck.check_id',$checkdata,$getStatus,array('id'=>'select_','legend'=>false,'label'=>false,'empty'=>'--Select--',"class"=>"validate[required]",'style'=>'width:90px;')); ?>
		</span></td>
    <td style="border:0px;"><?php //echo $form->submit('Save',array('id'=>'button1','style'=>'width:50px;'))."<span id='loader".$res['User']['id']."'></span>"; ?></td>
    </tr>
		  <?php //echo $form->end(); ?>
		</table>
		</td>
		<td  align="left" style="vertical-align:top">NA</td>-->
		  <td  align="center" style="vertical-align:middle">
				<?php echo $html->link($html->image('user_edit.png',array('alt'=>'Edit Client Basic Information','title'=>'Edit Client Basic Information')),array('controller'=>'sps','action'=>'editclient',$cid),array('escape'=>false)); ?>
				<?php echo $html->link($html->image('resend.gif',array('alt'=>'Resend Credential','title'=>'Resend Credential')),array('controller'=>'sps','action'=>'credentialmailclient',$cid),array('escape'=>false,'confirm'=>'Are you sure you want to resend the credentials ?')); ?>
				<?php echo $html->link($html->image('nuvola_add16.png',array('alt'=>'Add Site Address','title'=>'Add Site Address')),array('controller'=>'sps','action'=>'addSiteAddress',$cid),array('escape'=>false)); ?>
				<?php echo $html->link($html->image('icon-phone.gif',array('alt'=>'Create a Service Call?','title'=>'Create a Service Call?')),array('controller'=>'sps','action'=>'createservicecall',$cid),array('escape'=>false)); ?>
				<?php echo $html->link($html->image('messageIcon.png',array('alt'=>'Send Message','title'=>'Send Message')),array('controller'=>'messages','action'=>'sendMessage',$cid),array('escape'=>false,'class'=>'ajax')); ?>
				<?php echo $html->link($html->image('email-icon.gif',array('alt'=>'Send Email','title'=>'Send Email')),array('controller'=>'sps','action'=>'sendEmail',$cid),array('escape'=>false,'class'=>'ajax')); ?>
				<?php //echo $html->link("Create a Service Call?",array('controller'=>'sps','action'=>'createservicecall',$cid),array('escape'=>false)); ?>
				<br/>
				<?php echo $html->link('Change Password',array('controller'=>'sps','action'=>'client_changepwd',$cid),array('escape'=>false,'class'=>'ajax')); ?>
			</td>
            </tr>
            <script type="text/javascript">
                jQuery("#addclient<?php echo $res['User']['id']; ?>").submit(function() {
                  var data = jQuery("#addclient<?php echo $res['User']['id']; ?>").serialize();
                  var uid = <?php echo $res['User']['id']; ?>;
                	$('#loader<?php echo $res['User']['id']; ?>').show('slow');
	                $('#loader<?php echo $res['User']['id']; ?>').html('<span style="color:#3688B8; font-weight:bolder;">Saving...</span>');
                  $.ajax({
                    	type: "post",
                    	url: "/sps/addstatus/"+uid,
                    	data: data,
                    	success: function(response){
			     if(response == 1){
				  $('#loader<?php echo $res['User']['id']; ?>').hide('slow'); 
			     }
			}
                    });
                  return false;
                });   
            </script>
		<?php $i++; } ?>
            </table>
                <?php echo $form->end(); ?>
                </div>
	      <?php
		echo $this->element('pagination',array('paging_action'=>'/sps/clientlisting/')); ?>	
            </div>
  <script type="text/javascript">
    jQuery(document).ready(function(){
	    jQuery(".ajax").colorbox();
    });

jQuery("#filterbtn").click(function () {
jQuery("#filter").toggle("slow");
});

function validate()
{
    if(document.getElementById('UserFname').value=='' && document.getElementById('Userlname').value=='' && document.getElementById('UserPhone').value=='' && document.getElementById('UserEmail').value=='' && document.getElementById('UserAddress').value=='' && document.getElementById('UserSiteName').value=='' && document.getElementById('UserSiteEmail').value=='' && document.getElementById('UserSiteCname').value=='' && document.getElementById('UserSiteAddress').value=='' && document.getElementById('UserSitePhone').value=='')
    {
	alert('Please enter atleast one criteria to search');
	return false;
    }
    return true;

}
</script>