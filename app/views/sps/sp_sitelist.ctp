<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js')); 
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<style>
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#3E4061}
.register-form {
    border: 0px;
    overflow: hidden;
    padding: 10px;
}
.heading{text-align:center;font-size:16px;font-weight:bold;color:3E4061;width:100%;}
.butn {float:right;padding-right:20px;margin-bottom:10px;}
.butn a { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
.butn a:hover { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<script type="text/javascript">
	var states = '';
    function getStates(id,value,stateid){
	
    var optArr = document.getElementById(id).value;    
	jQuery('#'+stateid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByCountry/" + value,
                success : function(data){
		    window.states = data;
		    var selectedOption = '';
		    var select = $('#'+stateid);
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    $('option', select).remove();
		     
		    $.each(window.states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}
            
            
            
            $(function(){
				//Accordion
			    $("#accordion").accordion({ header: "h3",active:false,collapsible: true});
			    $(".ajax").colorbox();
			    $('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); },
					function() { $(this).removeClass('ui-state-hover'); }
			    );
			    
			    $("form#addclient").find('input').each(function(){
				$(this).attr('readonly','readonly');
			    });
			    
			    $("form#addclient").find('select').each(function(){
				$(this).attr('disabled','disabled');
			    });
			});
	    function PopupCenter(pageURL, title,w,h) {
	    var left = (screen.width/2)-(w/2);
	    var top = (screen.height/2)-(h/2);
	    var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }
		</script>
    <style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			.ui-accordion-content{
			    
			}
			div.iconContainer{ text-align: right; width: 98%; padding-bottom: 15px;}
		</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
              <div id="box">	      
            <h3 id="adduser">Site Addresses (<?php echo $type;?>)</h3>
            <br/>
	    <div class="iconContainer">
		<?php $cid = base64_encode($client_id);?>
		<?php echo $html->link($html->image('map.png',array('alt'=>'Address Map','title'=>'Address Map')),'javascript:void(0)',array('escape'=>false,'onclick'=>"PopupCenter('".$html->url(array('controller'=>'sps','action'=>'client_mapdirection',$cid))."','','760','490');")); ?>
		<?php echo $html->link($html->image('nuvola_add16.png',array('alt'=>'Add Site Address','title'=>'Add Site Address')),array('controller'=>'sps','action'=>'addSiteAddress',$cid),array('escape'=>false)); ?>
		<?php echo $html->link($html->image('icon-phone.gif',array('alt'=>'Create a Service Call?','title'=>'Create a Service Call?')),array('controller'=>'sps','action'=>'createservicecall',$cid),array('escape'=>false)); ?>
		<?php echo $html->link($html->image('messageIcon.png',array('alt'=>'Send Message','title'=>'Send Message')),array('controller'=>'messages','action'=>'sendMessage',$cid),array('escape'=>false,'class'=>'ajax')); ?>
		<?php echo $html->link($html->image('email-icon.gif',array('alt'=>'Send Email','title'=>'Send Email')),array('controller'=>'sps','action'=>'sendEmail',$cid),array('escape'=>false,'class'=>'ajax')); ?>
	    </div>
			<div class="butn">
                <?php if($type=='Active')
                {?>
                <a href="/sp/sps/sitelist/I/<?php echo $client_id;?>">Idle List</a></div>
                <?php }
                else{ ?>
                    <a href="/sp/sps/sitelist/A/<?php echo $client_id;?>">Active List</a></div>
                <?php }
                ?>
                
		    <div class="butn"><a href="javascript:void(0);" onclick="javascript:history.go(-1);">Back</a></div>
            <br/>
            <div class="heading"><?php echo $type;?> Site Addresses of <?php echo $this->Common->getClientCompanyName($client_id);?></div>
            <table class="tbl">
			<tr>
			    <td width="70%" align="left">
				
				<div id="accordion">
				<?php if(!empty($sitelists))
				      {
					foreach($sitelists as $key=>$res): ?>
					<div>
					<h3><a href="#"><?php echo $res['SpSiteAddress']['site_name']; ?></a></h3>
					
                    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'sitelist'),'id'=>'addclient')); ?>
                    <?php echo $form->input('SpSiteAddress.id', array('id'=>'ProfileImage','value'=>$res['SpSiteAddress']['id'],'type'=>'hidden',"div"=>false,"label"=>false));?>
                    <table class="inner" cellpadding="0" cellspacing="0">
                    <tr>
                    <td>
						<ul class="register-form">
                        <li>
                            <label>Site Name<span>*</span></label>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_name', array('value'=>$res['SpSiteAddress']['site_name'],'maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"34"));  ?></span></p>
                        </li>
                         <li>
                            <label>Phone</label>
                            <?php $arrphone=explode('-',$res['SpSiteAddress']['site_phone']);?>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_phone1', array('value'=>@$arrphone[0],'maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"35"));  ?>-</span></p>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_phone2', array('value'=>@$arrphone[1],'maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"36"));  ?>-</span></p>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_phone3', array('value'=>@$arrphone[2],'maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"37"));  ?></span></p>
                            <span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
                        </li>
                        <li>
                            <label>Email<span>*</span></label>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_email', array('value'=>$res['SpSiteAddress']['site_email'],'maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'validate[required,custom[email]]',"tabindex"=>"38"));  ?></span></p>
                        </li>
                        <li>
                            <label>Address<span>*</span></label>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_address',array('value'=>$res['SpSiteAddress']['site_address'],'maxLength'=>'100','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"39")); ?></span></p>
                            
                        </li>
                        <li>
                            <label>Country<span>*</span></label>
                            <p><span class=""><?php echo $this->Form->select('SpSiteAddress.country_id', $country,$res['SpSiteAddress']['country_id'], array('id'=>'UserSiteCountryId','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserSiteStateId');","tabindex"=>"40",'value'=>$res['SpSiteAddress']['country_id']));?></span></p>
                                
                        </li>
                        <li>
                            <label>State<span>*</span></label>
                            <p><span class=""><?php  echo $this->Form->select('SpSiteAddress.state_id',$state,$res['SpSiteAddress']['state_id'], array('id'=>'UserSiteStateId'.$res['SpSiteAddress']['id'],'label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ,"tabindex"=>"41"));?></span></p>
                        </li>
                        <script type="text/javascript">
                        $(function(){
                              getStates(id,<?php echo $res['SpSiteAddress']['country_id']?>,stateid);
                              var optArr = document.getElementById(id).value;    
	jQuery('#'+UserSiteStateId<?php $res['SpSiteAddress']['id']?>).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByCountry/" + value,
                success : function(data){
		    window.states = data;
		    var selectedOption = '';
		    var select = $('#'+stateid);
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    $('option', select).remove();
		     
		    $.each(window.states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}
                        );
                        </script>
                        <li>
                            <label>City<span>*</span></label>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_city',array('value'=>$res['SpSiteAddress']['site_city'],'maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"42")); ?></span></p>
                        </li>
                        <li>
                            <label>Zip<span>*</span></label>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_zip',array('value'=>$res['SpSiteAddress']['site_zip'],'maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"43")); ?></span></p>
                        </li>
                        <li>
                            <label>On-site person to contact<span>*</span></label>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_contact_name',array('value'=>$res['SpSiteAddress']['site_contact_name'],'maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"44")); ?></span></p>
				        </li>
						
						<li><p><b>Responsible contact information for scheduling</b></p></li>
						<li>
                            <label>Name<span>*</span></label>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.responsible_person_name', array('value'=>$res['SpSiteAddress']['responsible_person_name'],'maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"34"));  ?></span></p>
                        </li>
						
						 <li>
                            <label>Email<span>*</span></label>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.responsible_person_email', array('value'=>$res['SpSiteAddress']['responsible_person_email'],'maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'validate[required,custom[email]]',"tabindex"=>"38"));  ?></span></p>
                        </li>
						
						
                         <li>
                            <label>Phone</label>
                            <?php $arrphone1=explode('-',$res['SpSiteAddress']['responsible_contact']);?>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.responsible_contact1', array('value'=>@$arrphone1[0],'maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"35"));  ?>-</span></p>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.responsible_contact2', array('value'=>@$arrphone1[1],'maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"36"));  ?>-</span></p>
                            <p><span class="input-field"><?php echo $form->input('SpSiteAddress.responsible_contact3', array('value'=>@$arrphone1[2],'maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"37"));  ?></span></p>
                            <span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
                        </li>
                       
						
                        <li>
                            <label>&nbsp;</label>
                            
                            <p>
                                <!--<span class="blue-btn"><span><?php echo $form->submit('Delete',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span>-->
                                
                                <?php if($type=='Active'){?>
                                <div class="butn"><span style="float:right;"><?php echo $html->link('Make This Site Idle',array('controller'=>'sps','action'=>'makeidle',$res['SpSiteAddress']['id']),array('title'=>'Make this site address idle','confirm'=>'Are you sure that you want to make this site Idle ? by making this Idle - you will not have access to any of the information other then the site name and option to make active again. Once Idle - you will have no access to the reports or site information.'));?></span></div>
                                <?php } else
                                {
                                  ?>  <div class="butn"><span style="float:right;"><?php echo $html->link('Request to Make This Site Active Again',array('controller'=>'sps','action'=>'request_activation',$res['SpSiteAddress']['id']),array('title'=>'Request to make this site address active again','confirm'=>'Are you sure that you want to make this site active again ? by making this request - your request will send to administrator of reportsonlineplus.'));?></span></div>
                               <?php }
                                ?>
                                
                               <div class="butn"><span style="float:right;"><?php echo $html->link('Delete',array('controller'=>'sps','action'=>'makedelete',$res['SpSiteAddress']['id']),array('title'=>'Delete','confirm'=>'Are you sure that you want to delete all documents and history associated with this site?'));?></span></div></p>
                        </li>
                        </ul>
                        </td>
                        </tr>
                    </table>
                    <?php echo $form->end(); ?>
					</div>
					<?php endforeach;
				}else {
				    echo "No Record Found";
				} ?>				
                </div>
                </td>
            </tr></table>
</div>
</div>
