<?php
    echo $this->Html->script('jquery.1.6.1.min');
  // echo $this->Html->script('jquery-ui.min.js'); 
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('validation/only_num');
      #FOR COLOR PICKER
    echo $this->Html->css('colorpicker/colorpicker.css');
    echo $this->Html->css('colorpicker/layout.css');
    echo $this->Html->script(array('colorpicker/colorpicker.js','colorpicker/eye.js','colorpicker/utils.js','colorpicker/layout.js?ver=1.0.2'));
    
?>

<script type="text/javascript">
$('#colorpickerField1').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val(hex);
		$(el).ColorPickerHide();
	},
	onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});



jQuery(document).ready(function(){
    jQuery("#addclient").validationEngine();
    $('#UserSitePhone1').numeric();
    $('#UserSitePhone2').numeric();
    $('#UserSitePhone3').numeric();
});
function getStates(id,value){
    var optArr = $("#"+id).val();
	jQuery("#UserStateId").html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
                url  : "/users/getByCountry/" + value,
                success : function(opt){
			jQuery('#UserStateId').html(opt);
                }
        });
}
function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxuploaduserpic',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#ProfileImage").val(responseJSON.profile_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/profilepic/'+responseJSON.profile_image+'">');
	    }
            });           
    }        
	window.onload = createUploader;
</script>
<script>


function showservices(rid)
{
  
  var shwid= "li_"+rid;
    if(document.getElementById(rid).checked){
  // Insert code here.

$("#li_"+rid).fadeIn(500);
    }else{
$("#li_"+rid).fadeOut(500);  
    } 

}
function closebox(id)
   {
     $("#li_"+id).fadeOut(500);
     $("#"+id).attr("checked", false);
   }
function toggleChecked(status,obj) {    
$(obj).parent('p').parent('ul').children(".checkbox").each( function() {
$(this).attr("checked",status);
})
}
	
	</script>
<style>
.error-message {
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: right;
    font-weight: bold;
    position: static;
    top: 30px;
    white-space: nowrap;
}
input { width:250px;}
.qq-uploader {
    margin-left: 2px;
}
</style>

<section class="register-wrap">
	<h1 class="main-hdng">Add Inspector</h1>
    <!--one complete form-->
    <div class='message' style="text-align:center;font-weight:bold;"><?php echo $this->Session->flash();?></div>
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addInspector'),'id'=>'addclient')); ?>
               <?php echo $form->input('Company.country_id', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$userData['Company']['Country']['id']));  ?>
	        <?php echo $form->input('Company.state_id', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$userData['Company']['State']['id']));  ?>
		<?php echo $form->input('User.profilepic', array('id'=>'ProfileImage','type'=>'hidden',"div"=>false,"label"=>false));  ?>
    <section class="form-box" style="width:90%;">
        <h2>Personal Information</h2>
        <ul class="register-form">
            <li>
                <label>First Name</label>
                <p><span class="input-field"><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?></span></p>
            </li>
            <li>
                <label>Last Name</label>
                <p><span class="input-field"><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false,"tabindex"=>"2"));  ?></span></p>
            </li>
            <!--<li>
                <label>Qualification</label>
                <p><span class="input-field"><?php //echo $form->input('User.qualification', array('maxLength'=>'50',"div"=>false,"label"=>false,"tabindex"=>"3")); ?></span></p>
            </li>-->
             <li>
                <label>Phone</label>
                <p><span class="input-field">
		<?php echo $form->input('User.site_phone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"4"));  ?>-</span></p>
		<p><span class="input-field"><?php echo $form->input('User.site_phone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"5"));  ?>-</span></p>
		<p><span class="input-field"><?php echo $form->input('User.site_phone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"6"));  ?></span></p>
	      
		
		<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
            </li>
            <li>
                <label>Email</label>
                <p><span class="input-field"><?php echo $form->input('User.email', array('maxLength'=>'100',"div"=>false,"label"=>false,'class'=>'validate[required,custom[email]]',"tabindex"=>"7")); ?></span></p>
            </li>
            <li>
                <label>Profile pic</label>
                <p><span class="">
		     <div id="demo"></div></span>
		    <ul id="separate-list"></ul>
		</p>
            </li>
	     <li>
                <label>&nbsp;</label>
                <p><span class="">
		     <div id="uploaded_picture"  style="text-align:left;"><?php echo $html->image($this->Common->getUserProfilePhoto($this->data['User']['profilepic']));?></div>
		</span></p>
            </li>
	     <li>
                <label>Specific notes about the inspector</label>
                
                <p><span class=""><?php echo $form->input('User.notes',array('type'=>'textarea','class'=>'validate[required]','div'=>false,"label"=>false,'rows'=>2,'cols'=>32,"tabindex"=>"9")); ?></span></p>
            </li>
	     
	      <li>
                <label>Calendar Color Identifier</label>
		
                <p><span class="input-field"><?php echo $form->input('User.calendar_color_code', array('maxLength'=>'6','id'=>'colorpickerField1',"div"=>false,"label"=>false,'value'=>'EFFEFE')); ?></span></p><?php echo $html->link($html->image("bluearrow.gif",array('width'=>20,'height'=>20)),'javascript:void(0)', array('title'=>'Click on the color box, slide the arrows up and down to the desired range and click on the right side button to save the color','escape' => false));?>&nbsp;<span style="color:#2281CF; font-size:11px; vertical-align: text-top;">Click on the color box, slide the arrows up and down to the desired range and click on the right side button to save the color</span>
            </li>
	     
	      
	     
	      
        </ul>
    </section>
    
	 <!--PERMISSION START-->
    <section class="form-box" style="width:90%;">
      <h2>Permissions</h2>
      <p style="padding-bottom:10px">The following list represents the actions that this particular inspector is allowed to perform. The inspector will only be allowed to perform the function that are approved and checked.</h2>
        <ul class="register-form">	
        <p><b>Click to  check all : </b><input type="checkbox" style="width:50px" onclick="toggleChecked(this.checked,this)" tabindex="10">  </p>    
        <?php foreach($permissions as $key=>$perm){ 
		echo $form->checkbox('User.permissions.'.$key,array('class'=>'checkbox','id'=>$key,'value'=>$key,'label'=>false,"tabindex"=>"11",'div'=>false,'hiddenField' => false,'style'=>'width:20px;border:0px;')).'&nbsp;'."<span style='font-size:13px;'>".$perm."</span>"."<br/>";
		}
		
		?>
            
        </ul>    
    
    </section>    
   <!--PERMISSION END--> 
	
	
    <!--one complete form ends-->
    <section class="form-box" style="width:90%;">
      <h2>Qualification</h2>
      <p style="padding-bottom:10px">The following list represents the particular inspections that the inspector is allowed to or qualified to perform. The inspector will only be allowed to perform the inspections that are approved and checked.</h2>
        <ul class="register-form">
        <p><b>Click to  check all : </b><input type="checkbox" style="width:50px" onclick="toggleChecked(this.checked,this)" tabindex="12">  </p> 
        <?php foreach($rData as $rd){ ?>
            
                <?php echo $form->input('User.check.'.$rd['Report']['id'],array('id'=>$rd['Report']['id'],'type'=>'checkbox','class'=>'checkbox','label'=>false,'div'=>false,"tabindex"=>"13",'hidden'=>false,'style'=>'width:20px;border:0px;','onClick'=> 'showservices('.$rd['Report']['id'].')')).'&nbsp;'."<span style='font-size:13px;'>".$rd['Report']['name']."</span>"."&nbsp;&nbsp;"; ?>
                <?php } ?>
          <?php foreach($rData as $rd){ ?>  
            <li id="li_<?php echo $rd['Report']['id'] ?>" style="display:none">
               </br>     </br>
          <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr><h2 href="#" class="mini-form-hdr"><?php echo $rd['Report']['name']; ?><a href="javascript:void(0);" style="float:right;padding-top:5px;" onclick="closebox(this.id);" id="<?php echo $rd['Report']['id']; ?>"><?php echo $html->image('close.gif'); ?></a></h2></tr>
				<tr>
				<?php $i=1;
				    foreach($rd['Service'] as $data){
				?>
				    <td width="25%" style="padding:6px;">
				    <?php echo $form->input('User.service_id.'.$rd['Report']['id'].'.'.$data['id'],array('id'=>$data['id'],'class'=>'group1_'.$rd['Report']['id'],'type'=>'checkbox','label'=>false,'div'=>false,'hidden'=>false,'style'=>'width:10px','value'=>$data['id']))."&nbsp;"."<span style='color:#2281D4'>".$data['name']."</span>"; ?>
				    
				    <?php if($i%4==0)
					{ ?>
				   </td> </tr><?php $i=1; } else { $i++; } } ?>
				
				</table>
            </li>
        	<?php } ?> 
          <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
               
            </li>   
        </ul>    
    
    </section>
    <?php echo $form->end(); ?>
    </section>