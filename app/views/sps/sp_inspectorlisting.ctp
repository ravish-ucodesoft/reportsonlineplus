<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<?php  echo $this->Html->script(array('//code.jquery.com/jquery-1.12.0.min.js')); ?>
<?php echo $this->Html->script(array('https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js')); ?>
<?php // echo $this->Html->css(array('https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css')); ?>
<script>
	$(document).ready(function() {
    $('#example').DataTable({
    	"paging":   false,
        "info":     false,
        "searching": false
    });
} );
</script>

<style>
a, a:visited {
    color: #2281CF;
	text-decoration:none;
}
a:hover {
    color: #3688B8;
	text-decoration:underline;	
}
.blue-btn:hover{color:#fff !important;}
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:10px;"><b><?php echo $html->link('Add New Inspector',array('controller'=>'sps','action'=>'addInspector'),array("style"=>"padding-right:20px;")); ?></b></div>
	      
	      <?php //echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addclients'),'id'=>'addclient')); ?>
              <div id="box" >
                	<h3 id="adduser">Inspector Listing</h3>
                	<div style="float:right;padding-right:10px;"><p class="bttm-btns"><a href="javascript:void(0);" class="blue-btn" id="filterbtn"><span>Filter</span></a></p></div><br/>
                    		<?php
		if(isset($this->passedArgs) && !empty($this->passedArgs))
		{
		  $style="'width:100%;display:block'";
		}
		else
		{
		  $style="'width:100%;display:none'";
		}
		?>
		<div id="filter" class="form-box" style=<?php echo $style;?>>
		   
		  <?php $fname ='';
				  $lname='';				  
				  $email='';
				  $qualification='';
				  $check_1='';
				  $check_2='';
				  $check_3='';
				  $check_4='';
				  $check_5='';
				  
				  
				if(isset($this->passedArgs['User.fname'])){
					$fname = $this->passedArgs['User.fname'];
				} 
				if(isset($this->passedArgs['User.lname'])){
					$lname = $this->passedArgs['User.lname'];
				}
				if(isset($this->passedArgs['User.qualification'])){
					$qualification = $this->passedArgs['User.qualification'];
				}
				if(isset($this->passedArgs['User.email'])){
					$email = $this->passedArgs['User.email'];
				}	
        if(isset($this->passedArgs['User.check_1'])){
					$check_1 = $this->passedArgs['User.check_1'];
				}
        if(isset($this->passedArgs['User.check_2'])){
					$check_2 = $this->passedArgs['User.check_2'];
				}	
        if(isset($this->passedArgs['User.check_3'])){
					$check_3 = $this->passedArgs['User.check_3'];
				}	
        if(isset($this->passedArgs['User.check_4'])){
					$check_4 = $this->passedArgs['User.check_4'];
				}	
        if(isset($this->passedArgs['User.check_5'])){
					$check_5 = $this->passedArgs['User.check_5'];
				}		
		   
		   ?>
		   
		   <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'inspectorlisting'),'id'=>'clientlisting','onsubmit'=>'return validate();','div'=>false,'label'=>false)); ?>
		   <table width="100%" >
		    <tr><td width="50%" valign="top" border="">
		   
		   <ul class="register-form" style="border:none;">
            <li>
                <label>First Name</label>
                <p><span class="input-field"><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'',"div"=>false,"label"=>false,'value'=>$fname));  ?></span></p>
            </li>
            <li>
                <label>Last Name</label>
                <p><span class="input-field"><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false,'value'=>$lname));  ?></span></p>
            </li> 
                   
      </ul></td><td>
	    <ul class="register-form" style="border:none;">
             <!--<li>
                <label>Qualification</label>
                <p><span class="input-field"><?php //echo $form->input('User.qualification', array('maxLength'=>'20',"div"=>false,"label"=>false,'class'=>'','value'=>$qualification));  ?></span></p>
            </li>-->
             <li>
                <label>Email</label>
                <p><span class="input-field"><?php echo $form->input('User.email', array('maxLength'=>'50',"div"=>false,"label"=>false,'class'=>'','value'=>$email));  ?></span></p>
            </li>         
            
	     
        </ul>
	    
	</td></tr>
	
	<tr><td colspan="2"><ul>
	     <li>
              <label>&nbsp;</label>
              <p style="text-align:center;  padding-right:134px"><span class="">
                <?php echo "Qualification:&nbsp;&nbsp;"; foreach($rptdata as $rd){   ?>
                  
               <?php  if($rd['Report']['id']== $check_1 || $rd['Report']['id']== $check_2 || $rd['Report']['id']== $check_3 || $rd['Report']['id']== $check_4 || $rd['Report']['id']== $check_5){
              
             echo $form->input('User.check_'.$rd['Report']['id'],array('id'=>$rd['Report']['id'],'type'=>'checkbox','label'=>false,'div'=>false,'value'=>$rd['Report']['id'],'hidden'=>false,'style'=>'width:10px;','checked'=>'checked')).'&nbsp;'."<span style='font-size:13px;'>".$rd['Report']['name']."</span>"."&nbsp;&nbsp;"; 
               
               }else {
              echo $form->input('User.check_'.$rd['Report']['id'],array('id'=>$rd['Report']['id'],'type'=>'checkbox','label'=>false,'div'=>false,'value'=>$rd['Report']['id'],'hidden'=>false,'style'=>'width:10px;')).'&nbsp;'."<span style='font-size:13px;'>".$rd['Report']['name']."</span>"."&nbsp;&nbsp;";
               }?> 
                
                <?php } ?>
              </span></p>
          </ul>  </li> 
  </td></tr>
	<tr><td colspan="2">
	<ul><li>
    <label>&nbsp;</label>
    <p class="bttm-btns" style="float:right;margin-right:33px;"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
  </li></ul>
  </td></tr>
   </table>
		   <?php echo $form->end(); ?>
		    
		    
		</div>
		
		    
                        
                        
                        <br/>
                        <table class="tbl table table-striped table-bordered" id="example">
                 <thead>        <tr>
			   
			    <th width="15%">Name</th>			    
			    <th width="10%">Phone</th>			    
			    <th width="15%">Email</th>
				<th width="15%">Qualification</th>
				<th width="10%">Calendar Schedule</th>
				<th width="10%">Permissions</th>
				<th width="8%">Color Code</th>
				<th width="10%">Notes</th>
			    <th width="20%">Action</th>
			   
                        </tr> </thead>
                    
                       
			<?php $ii =1; foreach($result  as $res) { ?>
				 <?php if(!isset($test) || $test != $res['User']['id']) {
$test = $res['User']['id'];
			    $cid = base64_encode($res['User']['id']);
			    
			?>
			<tr>
			    
			    <td width="15%" align="center" style="vertical-align:middle"><?php echo $html->image($this->Common->getUserProfilePhoto($res['User']['profilepic']),array('style'=>'vertical-align:middle')); ?><br/><span style="padding-bottom:5px;vertical-align:top"><?php echo $html->link($res['User']['fname']." ".$res['User']['lname'],array('controller'=>'sps','action'=>'inspector_detail',$cid),array('class' => 'ajax')); ?></span></td>			    
			    <td width="10%" align="center" valign="middle"><?php echo $res['User']['phone']; ?></td>  
			    <td width="15%" align="center" valign="middle"><?php echo $res['User']['email']; ?></td>
				
				
			    <td width="10%" align="left" valign="middle">
          <?php 
            $servid = explode(",",$res['User']['report_id']);
            foreach($servid as $value){       
              echo $this->Common->getReportName($value)."</br>"; ?><?php 
            } ?>
          </td>
				
				<td width="10%" align="center" valign="middle"><?php echo $html->link('Calendar Schedule',array('controller'=>'schedules','action'=>'inspector_schedule',$res['User']['id']),array('title'=>'Click to see all schedules for ' .$res['User']['fname']. ' in calendar view')); ?></td>
				
				
				<td width="10%" align="left" valign="middle">
				
          <?php 
            $permissionid = explode(",",$res['User']['permissions']);
            
            echo '<button class="ppermission" id="'.$ii.'" onclick="per(this.id);">Permissions</button>';
			echo '<div class="pp" id="pp_'.$ii.'">';
            foreach($permissionid as $value){       
              echo $this->Common->getPermissionName($value)."</br>"; 
            
              ?><?php 
            }
            $ii++;
            echo '</div>'; 
            ?>
            
          </td>
				
			<td width="5%" align="center" valign="middle"> <div style=" width:30px; height:30px; background-color: <?php echo $res['User']['calendar_color_code']; ?> ;">&nbsp;</div></td>	
			<td width="15%">
				<?php echo $res['User']['notes']; ?>
			</td>
			    <td width="25%" align="center">
				<?php echo $html->link($html->image('user_edit.png',array('alt'=>'Inspector Edit','title'=>'Edit')),array('controller'=>'sps','action'=>'editInspector',$cid),array('escape'=>false)); ?>
				<?php echo $html->link($html->image('resend.gif',array('alt'=>'Inspector Edit','title'=>'Resend Credentials')),array('controller'=>'sps','action'=>'credentialmail',$cid),array('escape'=>false,'confirm'=>'Are you sure you want to resend the credentials ?')); ?>
				<?php //echo $html->link('Resend Credentials',array('controller'=>'sps','action'=>'credentialmail',$cid),array('')); ?>
				<?php echo $html->link($html->image('delete.gif',array('alt'=>'Inspector Delete','title'=>'Delete Inspector')),array('controller'=>'sps','action'=>'deleteInspector',$res['User']['id']),array('escape'=>false,"onClick"=>"javascript:return confirm('Are you sure you want to delete this inspector ? All the information related to inspector will also be deleted,Are you Sure ?');")); ?>
				<br/>
				<?php echo $html->link('Change Password',array('controller'=>'sps','action'=>'inspector_changepwd',$cid),array('escape'=>false,'class'=>'ajax')); ?>
			    </td>
                        </tr>
			<?php } } ?>
                      </table>
                </div>
                     <?php
		echo $this->element('pagination',array('paging_action'=>'/sps/inspectorlisting/')); ?>
		        </div>
	 	
            </div>
<script>
function per(idd){
	//alert(idd);
        $("#pp_"+idd).toggle("slow");
}
</script>

<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery(".ajax").colorbox();
});
            
jQuery("#filterbtn").click(function () {
jQuery("#filter").toggle("slow");
});

function validate()
{
    if(document.getElementById('UserFname').value=='' && document.getElementById('Userlname').value=='' && document.getElementById('UserPhone').value=='' && document.getElementById('UserEmail').value=='' && document.getElementById('UserAddress').value=='' && document.getElementById('UserSiteName').value=='' && document.getElementById('UserSiteEmail').value=='' && document.getElementById('UserSiteCname').value=='' && document.getElementById('UserSiteAddress').value=='' && document.getElementById('UserSitePhone').value=='')
    {
	alert('Please enter atleast one criteria to search');
	return false;
    }
    return true;

}
</script>            
<style type="text/css">
	.ppermission{
		width: 100%;
		background: #086cc7;
		border: 2px solid #086cc7;
		color: white;
	}
	.pp{
		display: none;
	}
</style>