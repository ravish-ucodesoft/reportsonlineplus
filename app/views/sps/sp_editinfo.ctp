<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#editinfo").validationEngine();
});

 function limit(field, chars) {
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
    
    function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxupload',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#CompanyImage").val(responseJSON.company_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/company_logo_temp/'+responseJSON.company_image+'" height="100px" width="200px">');
	    }
            });           
    }        
	
	function createUploader1(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo1'),
                listElement: document.getElementById('separate-list1'),
                action: '/users/ajaxuploaduserpic',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#ProfileImage").val(responseJSON.profile_image);$("#separate-list1").hide();
		$("#uploaded_picture1").html('<img src="/img/profilepic/'+responseJSON.profile_image+'">');
	    }
            });           
    }        
	window.onload = function(){
	    createUploader1();
	    createUploader();
	};
	
    function getStates(id,value,stateid){
	var optArr = document.getElementById(id).value;    
	jQuery('#'+stateid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByCountry/" + value,
                success : function(data){
		    states = data;
		    var selectedOption = '';
		    var select = $('#'+stateid);
		    if(select.prop){
		      var options = select.prop('options');
		    }
		    else{
		      var options = select.attr('options');
		    }
		    $('option', select).remove();
		    $.each(states, function(val, text){
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}

function RemoveRecord(recordId){
       jQuery.ajax({
                type : "GET",
                url  : "/sps/delPrincipalRecord/" + recordId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#tr_'+recordId).remove();
		       jQuery("#deleteFlashmessage").show();   
		    }
                }
        });
}
 function confirmbox(rid){
  var r = confirm("Are you sure you want to delete this record");
  if(r==true){
     RemoveRecord(rid);  
  }
  else{
    return false;
  }
 }
 
function AddRow(divid){
    var counter = jQuery('#optioncount').val();
    counter = parseInt(counter) + 1;
    jQuery('#optioncount').val(counter);
    var OptionHtml = '<div id="option_'+counter+'">';
	OptionHtml = OptionHtml+'<table width="100%" cellpadding="2" cellspacing="2">';
	OptionHtml = OptionHtml+'<tr><td>Company Principal</td>';
	OptionHtml = OptionHtml+'<td>';
	OptionHtml = OptionHtml+'<select name="data[UserCompanyPrincipalRecord]['+counter+'][user_company_principal_id]" style="width:220px;">';
	OptionHtml = OptionHtml+'<option value="">-select-</option>';
    <?php foreach($cmpPrincipal as $key=>$val){ $value = $val['UserCompanyPrincipal']['id']; $detail = $val['UserCompanyPrincipal']['name'] ?>
	OptionHtml = OptionHtml+'<option value="<?php echo $value; ?>"><?php echo $detail; ?></option>';
    <?php } ?>
	OptionHtml = OptionHtml+'</select></td></tr>';
	OptionHtml = OptionHtml+'<tr><td>&nbsp;</td><td><input type="text" name="data[UserCompanyPrincipalRecord]['+counter+'][manager_name]" value="Enter name" class="text" style="width:150px:padding-right:10px;">&nbsp;<input type="text" value="Enter email" name="data[UserCompanyPrincipalRecord]['+counter+'][manager_email]" class="text" id="email_id_" style="width:150px;"></td></tr>';
	OptionHtml = OptionHtml+'<tr><td>&nbsp;&nbsp;</td><td><input type="checkbox" value="yes" class="checkbox" name="data[UserCompanyPrincipalRecord]['+counter+'][new_client]">New client Created</td></tr>';
	OptionHtml = OptionHtml+'<tr><td>&nbsp;&nbsp;</td><td><input type="checkbox" value="yes" class="checkbox" name="data[UserCompanyPrincipalRecord]['+counter+'][new_scheduled]">New inspection scheduled</td></tr>';
	OptionHtml = OptionHtml+'<tr><td>&nbsp;&nbsp;</td><td><input type="checkbox" value="yes" class="checkbox" name="data[UserCompanyPrincipalRecord]['+counter+'][previous_report]">Change in proviously saved report etc.</td></tr>';
	OptionHtml = OptionHtml+'</table></div>';
	jQuery('#DivOption').append(OptionHtml);    
 }

function RemoveRow(divid){
    jQuery('#option_'+divid).remove();
}
</script>
<style>

input { width:250px;}
</style>
<div id="content" >
    <div style="font-weight:bold;color:red;text-align:center;display:none;" id="deleteFlashmessage">Record has been deleted successfully</div>
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'editinfo'),'id'=>'editinfo')); ?>
	      <?php echo $form->input('User.id', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$this->data['User']['id']));  ?>
	      <?php echo $form->input('Company.id', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$this->data['Company']['id']));  ?>
	      <?php echo $form->input('Company.company_logo_old', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$this->data['Company']['company_logo']));  ?>
	      <?php echo $form->input('Company.company_logo_new', array('id'=>'CompanyImage','type'=>'hidden',"div"=>false,"label"=>false));  ?>
	      <?php echo $form->input('User.oldprofilepic', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>@$this->data['User']['profilepic']));  ?>
	      <?php echo $form->input('User.newprofilepic', array('id'=>'ProfileImage','type'=>'hidden',"div"=>false,"label"=>false));  ?>
	      <?php echo $this->Form->hidden('Company.optioncount',array('value'=>'1','id'=>'optioncount')); ?>
              <div id="box" >
                	<h3 id="adduser">Edit Personnel Information</h3>
	                
                        <br/>
                        <table class="tbl">
                        <tr><td width="20%">First Name<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?>           
                        </tr>   
                        
                        <tr><td>Last Name<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false));  ?></td>
                        </tr>
			<tr><td>Email<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.email', array('maxLength'=>'50','read-only'=>true,'class'=>'text validate[required,custom[email]]',"div"=>false,"label"=>false));  ?></td>
                        </tr>
			<tr><td>Profile pic</td>
                        <td>
			    <div id="demo1"></div>
			    <ul id="separate-list1"></ul>
                        </td>                       
                        </tr>
			<tr><td>&nbsp;</td>
                        <td>
			    <div id="uploaded_picture1"  style="text-align:left;"><?php echo $html->image($this->Common->getUserProfilePhoto($this->data['User']['profilepic']));?></div>
                        </td>                       
                        </tr>
              
                      </table>
                </div>

               <div id="box" >
                	<h3 id="adduser">Edit Company Information</h3>
                        <br/>
                        <table class="tbl">
                        <tr><td width="20%">Company Name<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->input('Company.name', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?>           
                        </tr>   
                        
                        <tr><td>Address<span class="astric">*</span> </td>
                        <td><?php echo $form->input('User.address', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?></td>
                        </tr>                         
                        
                        <tr><td>Country</td>
                        <td><?php echo $this->Form->select('User.country_id', $country,$this->data['User']['country_id'], array('id'=>'UserCountryId','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserStateId');"));?></td>                       
                        </tr>
			<tr><td>State</td>
                        <td><?php  echo $this->Form->select('User.state_id',$state,$this->data['User']['state_id'], array('id'=>'UserStateId','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-" ));?></td>                       
                        </tr>
			<tr><td>City</td>
                        <td><?php echo $form->input('User.city',array('maxLength'=>100,'type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?></td>                       
                        </tr>
			<tr><td>Zip</td>
                        <td><?php echo $form->input('User.zip',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?></td>                       
                        </tr>
			<tr><td>Phone<span class="reqd">*</span>:</td>
                        <td><?php $phone = explode('-',$this->data['User']['phone']);
			 echo $form->input('User.phone1', array('maxLength'=>'3','value'=>$phone[0],'class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;'));  ?>-
					<?php echo $form->input('User.phone2', array('maxLength'=>'3','value'=>$phone[1],'class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;'));  ?>-
					<?php echo $form->input('User.phone3', array('maxLength'=>'4','value'=>$phone[2],'class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;'));  ?></td>
                        </tr>
			
			<tr><td>Website</td>
                        <td><?php echo $form->input('Company.website', array('maxLength'=>'50',"div"=>false,"label"=>false));  ?></td>                       
                        </tr>
			
			<tr><td>Authorized/Designated Manager of the site<span class="reqd">*</span></td>
                        <td><?php echo $form->input('Company.site_manager', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?></td>                       
                        </tr>
			
			<tr><td>Direct Phone of Designated Site Manager<span class="reqd">*</span></td>
                        <td><?php echo $form->input('Company.phone_site_manager', array('maxLength'=>'50','class'=>'validate[required,custom[number]]',"div"=>false,"label"=>false)); ?></td>                       
                        </tr>
			
			<tr><td>Direct email of Designated Site Manager<span class="reqd">*</span></td>
                        <td><?php echo $form->input('Company.email_site_manager', array('maxLength'=>'50','class'=>'validate[required,custom[email]]',"div"=>false,"label"=>false)); ?></td>                       
                        </tr>
			
			<tr><td valign="top">Company Bio<span class="reqd">*</span></td>
                        <td>
			    <?php echo $form->input('Company.company_bio', array('type'=>"textarea",'class'=>'validate[required]',"div"=>false,"label"=>false,'rows'=>7,'cols'=>43,'onKeyUp' =>"return limit(this.id,200);"));  ?><br/>
			    <div style="float:left;">
			         <span id='limitCounter'>0</span>&nbsp;characters entered&nbsp;|&nbsp;<span id='limitCounterLeft'>200</span>&nbsp;characters remaining
			    </div>
			    <div id="essay_Error" class='error'>&nbsp;</div>
			
			</td>                       
                        </tr>
			
			<tr><td>Company Logo<span class="reqd">*</span></td>
                        <td>
			    <div id="demo"></div>
			    <ul id="separate-list"></ul>
			    <div id="Errormsg" style="padding-left:265px;color:red;font-weight:bold;"></div>
			</td>                       
                        </tr>
			
			<tr><td>&nbsp;</td>
                        <td><div id="uploaded_picture"  style="float:left;"></div></td>                       
                        </tr>
			
			<tr><td>&nbsp;</td>
                        <td><div id="already_uploaded_picture"  style="float:left;"><?php echo $html->image('/img/company_logo/'.$spsession['Company']['company_logo'],array('height'=>100,'width'=>200)); ?></div></td>                       
                        </tr>
			<tr>
			    <td colspan="2">
				<table width="100%" cellpadding="2" cellspacing="2" border="0">
				    <tr>
					<th width="20%">
					    Principal Name
					</th>
					<th width="20%">
					    Manager Name
					</th>
					<th width="20%">
					    Manager Email
					</th>
					<th width="10%">
					    Create Client
					</th>
					<th width="10%">
					    Scheduled Client
					</th>
					<th width="10%">
					    Previous Report
					</th>
					<th width="10%">
					    Action
					</th>
				    </tr>
				</table>
			    </td>
                        </tr>
			<tr>
			    <td colspan="2">
				<table width="100%" cellpadding="2" cellspacing="2" border="0">
				    <?php foreach($this->data['Company']['UserCompanyPrincipalRecord'] as $key=>$val){
					    $rid = $val['id'];	
				    ?>
				    <tr id="tr_<?php echo $rid; ?>">
					<td align="center">
					    <?php echo $pName = $this->Common->getPrincipalName($val['user_company_principal_id']); ?>
					</td>
					<td align="center">
					    <?php echo $val['manager_name']; ?>
					</td>
					<td align="center">
					    <?php echo $val['manager_email']; ?>
					</td>
					<td align="center">
					    <?php echo $val['new_client']; ?>
					</td>
					<td align="center">
					    <?php echo $val['new_scheduled']; ?>
					</td>
					<td align="center">
					    <?php echo $val['previous_report']; ?>
					</td>
					<td>
					    <a href="javascript:void(0)" onclick="confirmbox(<?php echo $rid; ?>);"><?php echo $html->image('minus.png'); ?></a>
					</td>
				    </tr>
				    <?php } ?>
				</table>
			    </td>
                        </tr>
			<tr>
			    <td colspan="2">
				<div id="DivOption">
				    
				</div>
			    </td>
			</tr>
			<tr>
			    <td colspan="2" align="center">
				 <a href="#"><strong><?php echo $this->Html->link('Add More','javascript:void(0)',array('escape'=>false,'id'=>'AddOption','onclick'=>'AddRow(1)','class'=>'up_arrow'));?></strong></a>
			    </td>
	                  </tr>
                        <tr><td></td><td>
			    <div align="right">
			    <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
			    </div>
			    </td>
	                  </tr>                         
              
                      </table>
                      	<?php echo $form->end(); ?>
                </div>

              
	
            </div>