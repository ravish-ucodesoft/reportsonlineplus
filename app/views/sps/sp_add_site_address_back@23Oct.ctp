<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');   
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script('validation/only_num');
?>
<style>
.error-message{
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: right;
    font-weight: bold;
    position: static;
    top: 30px;
    white-space: nowrap;
}
input { width:250px; }
</style>
<script>
$(document).ready(function(){
		    $('#SpSiteAddressSitePhone1').numeric();
		    $('#SpSiteAddressSitePhone2').numeric();
		    $('#SpSiteAddressSitePhone3').numeric();
		    $('#SpSiteAddressSiteZip').numeric();
		 });
</script>
<div class="register-wrap">
	<h1 class="main-hdng">Add Site Address</h1>
        <div class="form-box" style="width:90%">
            <h2>Site Address</h2>
			<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'add_site_address'),'id'=>'addclient')); ?>
            <ul class="register-form">
                    <li>
                        <label>Site Name<span>*</span></label>
                        <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_name', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"16"));  ?></span></p>
                    </li>
                    <li>
                        <label>Site Phone</label>
                        <p><span class="input-field">
                        <?php echo $form->input('SpSiteAddress.site_phone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"17"));  ?>-</span></p>
                        <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_phone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"18"));  ?>-</span></p>
                        <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_phone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:65px;float:none;',"tabindex"=>"19"));  ?></span></p>
                        <span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
                    </li>
                    <li>
                        <label>Site Email<span>*</span></label>
                        <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_email',array('maxLength'=>'60','type'=>'text','class'=>'validate[required,custom[email]]','div'=>false,"label"=>false,"tabindex"=>"20")); ?></span></p>
                    </li>
                    <li>
                        <label>Site Address<span>*</span></label>
                        <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_address', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"21"));  ?></span></p>
                        <span style="clear:left; float:left; width:252px; font-style:italic; font-size:10px; padding-top:5px;padding-left:160px; color:#3D94DC;"><b>Eg: 403 Meco Drive, Wilmington, DE, United States</b></span>
                    </li>
                    <li>
                        <label>Site Contact Name<span>*</span></label>
                        <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_contact_name', array('maxLength'=>'20',"div"=>false,"label"=>false,'class'=>'validate[required]',"tabindex"=>"22"));  ?></span></p>
                    </li>            
                    <li>
                        <label>Site Country<span>*</span></label>
                        <p><span class=""><?php echo $this->Form->select('SpSiteAddress.country_id', $country,'233', array('id'=>'UserCountryId1','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:250px;','onchange'=>"getStates(this.id,this.value,'UserStateId1');","tabindex"=>"23"));?></span></p>
                                        
                    </li>
                    <li>
                        <label>Site State<span>*</span></label>
                        <p><span class=""><?php  echo $this->Form->select('SpSiteAddress.state_id',$state,'', array('id'=>'UserStateId1','label' => false, 'class' => 'select_bg','style'=>'width:250px;','empty' => "-Please select-","tabindex"=>"24" ));?></span></p>
                                        
                    </li>
                    <li>
                        <label>Site City<span>*</span></label>
                        <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_city',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"25")); ?></span></p>
                                        
                    </li>
                    <li>
                        <label>Site Zip<span>*</span></label>
                        <p><span class="input-field"><?php echo $form->input('SpSiteAddress.site_zip',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,"tabindex"=>"26")); ?></span></p>
                                        
                    </li>
                    <li>
                        <label>&nbsp;</label>
                        
                    </li>
            </ul>    
        </div>
        <div class="form-box" style="width:90%">
            <ul>
                 <li>
                    <label>&nbsp;</label>
                    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
                </li>
                
            </ul>
        </div>
<?php echo $form->end(); ?>
</div>        