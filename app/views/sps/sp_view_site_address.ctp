<?php
    echo $this->Html->script('jquery.1.6.1.min');    
    echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox'));    
?>
<script type="text/javascript">
    $(function(){
	jQuery(".ajax").colorbox();
    });    
</script>
<style>
a, a:visited {
    color: #2281CF;
	text-decoration:none;
}
a:hover {
    color: #3688B8;
	text-decoration:underline;	
}
.blue-btn:hover{color:#fff !important;}
table {
    border-collapse: separate;
    margin: 5px;
    width: 98%;
}
div#flashMessage{
    font-size: 16px;
}
</style>
<div id='content'>
        <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
        <div class='message' style="text-align:center;">You can add as maximum <?php echo $maxallowed['SubscriptionDuration']['max_allowed_site_address']?> site addresses as a free trial</div>
		
	<div style="float:right;margin-top:10px;">
	    <b>
		<?php
		    $totalAddress = count($siteaddress);
		    if($totalAddress>=$maxallowed['SubscriptionDuration']['max_allowed_site_address']){
			$urlFor = array('controller'=>'sps','action'=>'showMessage');
			$attrStyle = array("style"=>"padding-right:20px;","class"=>"ajax");
		    }else{
			$urlFor = array('controller'=>'sps','action'=>'add_site_address');
			$attrStyle = array("style"=>"padding-right:20px;");
		    }
		?>
		<?php //echo $html->link('Add New Site Address',array('controller'=>'sps','action'=>'add_site_address'),array("style"=>"padding-right:20px;")); ?>
		<?php echo $html->link('Add New Site Address',$urlFor,$attrStyle); ?>
	    </b>
	</div>
        <div id="box" >
		<h3 id="adduser">Site Address Listing</h3>
                <table style="border:none;" width="100%" cellpadding="2" cellspacing="2">
                        <thead>
                                <tr>
                                    <th width="2%">S.No</th>
                                    <th width="10%">Site Name</th>
                                    <th width="5%">Site Phone</th>
                                    <th width="10%">Site Email</th>
                                    <th width="10%">Site Address</th>
                                    <th width="10%">Site Contact Name</th>
                                    <th width="10%">Site Country</th>
                                    <th width="5%">Site State</th>
                                    <th width="5%">Site City</th>
                                    <th width="5%">Site Zip</th>
				    <th width="10%">Action &amp; Notes</th>
                                </tr>
                        </thead>
                        <?php $i=1;
                         foreach($siteaddress as $siteaddinfo) { ?>
			 <tr>
                                <td align="center" style="vertical-align:top"><?php echo $i; ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $siteaddinfo['SpSiteAddress']['site_name']; ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $siteaddinfo['SpSiteAddress']['site_phone']; ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $siteaddinfo['SpSiteAddress']['site_email']; ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $siteaddinfo['SpSiteAddress']['site_address']; ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $siteaddinfo['SpSiteAddress']['site_contact_name']; ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $this->Common->getCountryName($siteaddinfo['SpSiteAddress']['country_id']); ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $this->Common->getStateName($siteaddinfo['SpSiteAddress']['state_id']); ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $siteaddinfo['SpSiteAddress']['site_city']; ?></td>
                                <td align="center" style="vertical-align:top"><?php echo $siteaddinfo['SpSiteAddress']['site_zip']; ?></td>
				<td  align="center" style="vertical-align:top">
				    <?php echo $html->link($html->image('user_edit.png',array('alt'=>'Edit Site Address','title'=>'Edit site Address')),array('controller'=>'sps','action'=>'edit_site_address',($siteaddinfo['SpSiteAddress']['id'])),array('escape'=>false)); ?>
				</td>
			 </tr>	
                        <?php $i++;
						
			} ?>
                </table>
        </div>
</div>    