<?php echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));?>
<style type="text/css">
    .register-form > li > label {   
        padding-top: 3px;    
        width: 120px;	 
    }
    .register-form > li > p {   
        padding-left: 10px;     
    }
    #box {
        border: none;
    }
    table {
        border-collapse: collapse;
        margin: 1px;
        width: 100%;
    }
    div.iconContainer{ text-align: right; width: 95%;}
    div#accordion{ min-height: 300px;}
</style>

<script type="text/javascript">
    jQuery(document).ready(function(){
	 jQuery("#accordion").accordion({ header: "h3",active:false,collapsible: true});
    });
</script>
<div id="content">
    <div id="box" style="width:800px;padding-left:10px;">
        <div class="iconContainer">
            <?php $cid = base64_encode($client_id);?>
            <?php echo $html->link($html->image('map.png',array('alt'=>'Address Map','title'=>'Address Map')),'javascript:void(0)',array('escape'=>false,'onclick'=>"PopupCenter('".$html->url(array('controller'=>'sps','action'=>'client_mapdirection',$cid))."','','760','490');")); ?>
            <?php echo $html->link($html->image('nuvola_add16.png',array('alt'=>'Add Site Address','title'=>'Add Site Address')),array('controller'=>'sps','action'=>'addSiteAddress',$cid),array('escape'=>false)); ?>
            <?php echo $html->link($html->image('icon-phone.gif',array('alt'=>'Create a Service Call?','title'=>'Create a Service Call?')),array('controller'=>'sps','action'=>'createservicecall',$cid),array('escape'=>false)); ?>
            <?php echo $html->link($html->image('messageIcon.png',array('alt'=>'Send Message','title'=>'Send Message')),array('controller'=>'messages','action'=>'sendMessage',$cid),array('escape'=>false,'class'=>'ajax')); ?>
            <?php echo $html->link($html->image('email-icon.gif',array('alt'=>'Send Email','title'=>'Send Email')),array('controller'=>'sps','action'=>'sendEmail',$cid),array('escape'=>false,'class'=>'ajax')); ?>
	</div>	
        <div class="form-box" style="width:95%">
            <h2 style="font-weight:bold;">Pending Service Calls</h2>
            <table class="tbl">
                <tr></tr>
		<tr>
                    <td width="70%" align="left">
			<div id="accordion">
                            <?php if(!empty($data))
                                  {
                                    foreach($data as $key=>$res):                                    
				?>
					<div>
					<!--<h3><a href="#" style="color: #fff"><?php //echo 'Name:'.$res['ServiceCall']['name'].' <span style="color:purple; font-size:10px;">[Service Call Date : '.date('m/d/Y H:i:s',strtotime($res['ServiceCall']['created'])).']</span>'; ?></a></h3>-->
					<h3><a href="#" style="color: #000 !important;"><?php echo 'Client Name : '.$this->Common->getClientCompanyName($res['ServiceCall']['client_id']).'<span style="color:grey; font-size:10px;"> [Site Name- '.$this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']).']</span><span style="color:#000; font-size:10px;"> [Call Name- '.$res['ServiceCall']['name'].']</span><span style="color:grey; font-size:10px;"> [Sch. requested- '.date('m/d/Y',strtotime($res['ServiceCall']['created'])).']</span>' ?></a></h3>
					<table class="inner" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
						<th width="20%" style="background-color:#2B92DD;color:#FFF">Report</th>
						<th width="20%" style="background-color:#2B92DD;color:#FFF">Lead Inspector</th>
						<th width="35%" style="background-color:#2B92DD;color:#FFF">Helper</th>
						<th width="25%" style="background-color:#2B92DD;color:#FFF" align="center">Sch Date(m/d/Y)</th>
						<th width="25%" style="background-color:#2B92DD;color:#FFF" align="center">&nbsp;</th>
                                            </tr>		
                                            <?php foreach($res['Schedule'] as $key1=>$schedule):?>
                                            <tr style="background-color:#e2eaed;">
                                                <td style="font-size: 16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
                                                <td><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
                                                <td><?php
                                                $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
                                                echo $this->Common->getHelperName($helper_ins_arr); ?></td>
                                                <td>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:ia', $schedule['schedule_from_1']).'-'.$time->format('g:ia', $schedule['schedule_to_1']); ?>
<br/>
                                                    <?php if(!empty($schedule['schedule_date_2'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:ia', $schedule['schedule_from_2']).'-'.$time->format('g:ia', $schedule['schedule_to_2'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_3'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:ia', $schedule['schedule_from_3']).'-'.$time->format('g:ia', $schedule['schedule_to_3'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_4'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:ia', $schedule['schedule_from_4']).'-'.$time->format('g:ia', $schedule['schedule_to_4'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_5'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:ia', $schedule['schedule_from_5']).'-'.$time->format('g:ia', $schedule['schedule_to_5'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_6'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:ia', $schedule['schedule_from_6']).'-'.$time->format('g:ia', $schedule['schedule_to_6'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_7'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:ia', $schedule['schedule_from_7']).'-'.$time->format('g:ia', $schedule['schedule_to_7'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_8'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:ia', $schedule['schedule_from_8']).'-'.$time->format('g:ia', $schedule['schedule_to_8'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_9'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:ia', $schedule['schedule_from_9']).'-'.$time->format('g:ia', $schedule['schedule_to_9'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_10'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:ia', $schedule['schedule_from_10']).'-'.$time->format('g:ia', $schedule['schedule_to_10'])?>
                                                    <?php } ?>
                                                </td>
                                                <td align='left'><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
                                                    <?php
                                                     //echo $schedule['report_id']."--".$res['ServiceCall']['id']."---".$res['ServiceCall']['client_id']."--".$res['ServiceCall']['sp_id'];
                                                          $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
                                                          $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
                                                         if($getCompleted != "" && $getstarted == 1){
                                                            //echo "<a href=''></a>";
                                                            echo $html->image('fineimages/finished.png');
                                                             ?>
                                                            <!--<a style="text-decoration: blink;" href="/sp/reports/<?php echo $this->Common->getReportLinkName($schedule['report_id']);?>?reportID=<?php echo $schedule['report_id']; ?>&clientID=<?php echo $res['ServiceCall']['client_id']; ?>&spID=<?php echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php echo $res['ServiceCall']['id']; ?>"><blink>Completed on <?php echo date('m/d/Y',strtotime($getCompleted)); ?></blink></a>-->
                                                         <?php
                                                        }
                                                         if($getstarted == 0 && $getCompleted == ""){
                                                            echo $html->image('fineimages/not_started.png');
                                                          }
                                                          else if($getstarted == 1 && $getCompleted == ""){
                                                            echo $html->image('fineimages/pending.png');
                                                          }
                                                    ?>
                                                </td>							
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <div class="service-hdline">Inspection Devices</div>
                                                    <ul class="services-tkn"><li>
                                                    <?php $i=0;$j=1;						
                                                    for($i=0;$i<count($schedule['ScheduleService']);$i++)
                                                    {
							echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
                                                        <?php if($j%4==0){
                                                            echo '</li></ul><ul class="services-tkn"><li>';
                                                        }else{
                                                            echo '</li><li>';
                                                        }
                                                        $j++;
                                                    }
                                                    ?>
						</td>
                                            </tr>
					    <tr><td colspan="5"></td></tr>
                                            <?php endforeach;?>
					</table>				
                                    </div>	
                                <?php endforeach;					
                            } else {?>
                              No RECORD FOUND  
                        <?php }?>
                        </div>
                        <!--one complete form ends-->
                    </td>
                </tr>
            </table>    
        </div>
    </div>
</div>    