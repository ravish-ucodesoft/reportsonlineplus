<?php function getPage($url) {
            
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$data = curl_exec($ch);
		
		curl_close($ch);
		
		return $data;
	}
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($result['User']['address']).'&output=json&sensor=true';
		// make the HTTP request
		$data = getPage($url);
		// parse the json response
		$jsondata = json_decode($data,true);
		// if we get a placemark array and the status was good, get the addres
		$lat=""; $lng="";
		if(is_array($jsondata )&& $jsondata ['status']=='OK'){
		 	$lat=$jsondata['results']['0']['geometry']['location']['lat'];
		 	$lng=$jsondata['results']['0']['geometry']['location']['lng'];
		}
		$rez['lat']=$lat;
		$rez['lng']=$lng;
		
		//return $rez;

?>
<?php echo $html->script(array("jquery1.7.2"));?>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<?php echo $html->script(array("gmap3.min"));?>

<script type="text/javascript">
         $(document).ready(function(){
	    $('#map_canvas').gmap3(
	      { action: 'addMarker',
		latLng: [<?php echo $rez['lat']?>,<?php echo $rez['lng']?>],
		map:{
		  center: true,
		  zoom: 14
		},
		marker:{
		  options:{
		    draggable:true
		  },
		  events:{
		    click: function(marker){
		      $(this).gmap3({
			action:'getAddress',
			latLng:marker.getPosition(),
			callback:function(results){
			  var map = $(this).gmap3('get'),
			      infowindow = $(this).gmap3({action:'get', name:'infowindow'}),
			      //content = results && results[1] ? results && results[1].formatted_address : 'no address';
			      content = '<?php echo $result['User']['address']; ?>';
			  if (infowindow){
			    infowindow.open(map, marker);
			    infowindow.setContent(content);
			  } else {
			    $(this).gmap3({action:'addinfowindow', anchor:marker, options:{content: content}});
			  }
			}
		      });
		    }
		  }
		}
	      }
	    )
	});
</script>
<div id="map_canvas" style="height:490px;width:760px;"></div>