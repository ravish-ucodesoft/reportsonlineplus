<?php
    echo $this->Html->script('jquery.1.6.1.min');
  // echo $this->Html->script('jquery-ui.min.js'); 
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('validation/only_num');
    
?>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#CertForm").validationEngine();   
});

function deleteRecord(fileName){
    jQuery.ajax({
        'url': '/sp/sps/deleteQuote/'+fileName,
        'success': function(msg){
            if(msg == 'OK'){
                jQuery("#showFile").remove();
            }    
        }
    });
}
</script>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
            <?php echo $this->Form->create('', array('url'=>array('controller'=>'sps','action' => 'sendQuote?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),'id'=>'QuoteForm','name'=>'CertForm','enctype'=>'multipart/form-data') );?>
            <section class="form-box" style="width:100%; margin: 0">
                <h2><strong>Quote for the report <?php echo $this->Common->getReportName($reportId);?></strong></h2>
                <ul class="register-form">
                    <?php if(isset($chkRecord) && $chkRecord['Quote']['attachment']!="" && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'quotefile'.DS.$chkRecord['Quote']['attachment'])){?>
                    <li id="showFile">
                        <label>&nbsp;</label>
                        <p>
                           Quote has already been submitted. (<?php echo $html->link($chkRecord['Quote']['title'],array('controller'=>'sps','action'=>'download_file',$chkRecord['Quote']['attachment'],'quotefile'));?>)<?php echo $html->link('DELETE','javascript:void(0)',array('id'=>'delBtn','style'=>'padding-left:20px; color:red', 'onclick'=>'deleteRecord("'.$chkRecord['Quote']['attachment'].'");'));?> 
                        </p>
                    </li>
                    <?php }?>
                    <li>
                        <label>Title(Appropriate Name)</label>
                        <p><span class="input-field"><?php echo $form->input('Quote.title', array('maxLength'=>'100','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?></span></p>
                    </li>
                    <li>
                        <label>Upload Quote Form</label>
                        <p><span class=""><?php echo $form->file('Quote.attachment', array("div"=>false,"label"=>false,"class"=>"validate[required]"));  ?></span></p>
                        <span>(Only pdf,xls,doc,docx,xlsx file extension)</span>
                    </li>                    
                    <li>
                        <label>&nbsp;</label>
                        <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
                       
                    </li> 
                             
                  
                </ul>
            </section>
            <?php echo $form->end(); ?>
             <br/>   
</div>