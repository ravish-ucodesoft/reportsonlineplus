<style type="text/css">
div.showThanksMsg{ width: 100%; font-size: 13px; text-align: center; padding-top: 100px; min-height: 300px;}
div.links{ padding: 10px;}
div.links a{ text-decoration: underline; padding: 0 10px;}
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box">
	<h3 id="adduser">Service Call for <?php echo $cname;?></h3>
    </div>
    <div class="showThanksMsg">
        Service Call is Successfully Created and will remain in the pending service call section untill client accept it.
        <div class="links">
            <?php echo $html->link('Go to Calendar View',array('controller'=>'schedules','action'=>'schedule'));?>
            <?php echo $html->link('Create another Service call for this client',array('controller'=>'sps','action'=>'createservicecall',base64_encode($cid)));?>
            <?php echo $html->link('Go to Client Listing',array('controller'=>'sps','action'=>'clientlisting'));?>
        </div>    
    </div>
</div>    