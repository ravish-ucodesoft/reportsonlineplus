<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    
?>

<style>
.inner{
	width:100%;
}
.tbl td {font-size:14px; font-weight:bold; color:#25587E}
.newTbl td{font-size:12px; color:#25587E}
td{
    border:none;
}
</style>
<script type="text/javascript">
    function calltoggle(linkId,uId)
    {
	//linkId = "acceptLink_"+uId
	//alert(linkId);
	if(linkId == "acceptLink_"+uId)
	{
	    $("#newcallbox_"+uId).show();
	    $("#existingcallbox_"+uId).hide();
	    acceptValidate(uId);
	    
	}
	if(linkId == "declineLink_"+uId)
	{
	    $("#existingcallbox_"+uId).show();
	    $("#newcallbox_"+uId).hide();
	    declineValidate(uId);
	}
    }
    function declineValidate(uId){	
	 $("#declineForm"+uId).validationEngine();
	   
    }
    function acceptValidate(uId){
	$("#acceptForm"+uId).validationEngine();
    }
   
</script>

<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box">
	<h3 id="adduser">Lead Requests</h3>
	<?php if(!empty($leadData)){ ?>
	<h3 id="adduser">Hello Michael, ReportOnlinePlus have send you lead</h3>
    </div>
    <?php foreach($leadData as $leadData){	?>
        <div id="box" style="border:1px solid grey" >                 
                  
            <table class="tbl" cellpadding="2" cellspacing="2">
		<tr>
                    <td>
			<table class="newTbl">
			    <tr>
				<td width="20%">Client Name:</td><td><?php echo $this->Common->getClientName($leadData['Lead']['client_id']); ?></td>
			    </tr>
			    <tr>
				<td width="20%">Last Date To Respond:</td><td><?php echo $leadData['Lead']['time_given']; ?></td>
			    </tr>
			    <tr>
				<td colspan="2"><i>If you don't respond before the above given time, the request will be automatically declined</i></td>
			    </tr>
			</table>
		    </td>
                </tr>
                <tr>
                    <td>For more information, kindly contact ReportOnlinePlus</td>
                </tr>
		<?php if($leadData['Lead']['status']==1){ ?>
		<tr>
                    <td style="color:red">Request Accepted</td>
                </tr>
		<?php }elseif($leadData['Lead']['status']==2){ ?>
		<tr>
                    <td style="color:red">Request Declined</td>
                </tr>
		<?php }else{ ?>
                <tr>
                    <td>If you want to accept this Request <?php echo $this->Html->link('Click Here','javascript:void(0)',array('title'=>'Accept it','id'=>'acceptLink_'.$leadData['Lead']['id'],'onclick'=>"calltoggle(this.id,".$leadData['Lead']['id'].");")); ?></td>
                </tr>
                <tr>
                    <td>If you want to Decline this Request <?php echo $this->Html->link('Click Here','javascript:void(0)',array('title'=>'Decline it','id'=>'declineLink_'.$leadData['Lead']['id'],'onclick'=>"calltoggle(this.id,".$leadData['Lead']['id'].");")); ?></td>
                 </tr>
		<?php  } ?>
		<tr>
                    <td>
			<div id="newcallbox_<?php echo $leadData['Lead']['id'];?>" style="display:none;">
			    
			    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'acceptedRequest'),'id'=>'acceptForm'.$leadData['Lead']['id'])); ?>
			    <?php echo $form->input('AcceptedLead.id', array('id'=>'','type'=>'hidden',"div"=>false,"label"=>false,'value'=>$leadData['Lead']['id']));  ?>
			    <?php echo $form->input('AcceptedLead.client_id', array('id'=>'','type'=>'hidden',"div"=>false,"label"=>false,'value'=>$leadData['Lead']['client_id']));  ?>
			    <?php echo $form->input('AcceptedLead.sp_id', array('id'=>'','type'=>'hidden',"div"=>false,"label"=>false,'value'=>$leadData['Lead']['sp_id']));  ?>
			    <?php echo $form->input('AcceptedLead.time_given', array('id'=>'ProfileImage','type'=>'hidden',"div"=>false,"label"=>false,'value'=>$leadData['Lead']['time_given']));  ?>
				<!--one complete form-->
			    <div class="form-box" style="width:90%">
				<h2>Pay Now</h2>
				<ul class="register-form">
				    <li>
					<label>CCType<span>*</span></label>
					<p><span class="">
					    <?php
					    $option=array('Visa'=>'Visa',
                                                    'Mastercard'=>'Mastercard',
                                                    'AmericanExpress'=>'AmericanExpress',
                                                    'DiscoverCard'=>'DiscoverCard'
                                                  );
					    echo $form->input('AcceptedLead.cardtype', array('options' =>$option,'label'=>false,'class'=>'validate[required]','empty'=>'please select'));
                                    ?>
					</span></p>
				    </li>
				    <li>
					<label>CCNo.</label>
					<p><span class="input-field"><?php echo $form->input('AcceptedLead.cardnumber', array('maxLength'=>'16','class'=>'text validate[required,custom[number]]',"div"=>false,"label"=>false));  ?></span></p>
				    </li>
				    <li>
					<label>CCV</label>
					<p><span class="input-field"><?php echo $form->input('AcceptedLead.cardcvnumber', array('maxLength'=>'4','class'=>'text validate[required,custom[number]]',"div"=>false,"label"=>false));  ?></span></p>
				    </li>
				    <li>
					<label>CC Expiry Date</label>
					<p><span class="">
					     <?php
					  $year=array();
					  $i=date('Y');
					while($i<(date('Y')+7))
					{
					    $year[$i]=$i;
					    $i++; 
					}
					    $month=array();
					    $i=1;
					while($i<13)
					{
					    $month[$i]=$i;
					    $i++; 
					} 
					    echo $form->input('AcceptedLead.exp_month', array('options' =>$month,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0','class'=>'validate[required]','empty'=>'please select'));
					    echo $form->input('AcceptedLead.exp_year', array('options' =>$year,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0','class'=>'validate[required]','empty'=>'please select'));
					?>
					</span></p>
				    </li>
				    
				    <li>
					<label>&nbsp;</label>
					<p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
				    </li>
				</ul>
			    </div>
			   <?php echo $form->end(); ?>
			</div>
			<div id="existingcallbox_<?php echo $leadData['Lead']['id'];?>" style="display:none;">
			<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'declinedRequest'),'id'=>'declineForm'.$leadData['Lead']['id'])); ?>
			    <?php echo $form->input('Lead.id', array('id'=>'','type'=>'hidden',"div"=>false,"label"=>false,'value'=>$leadData['Lead']['id']));  ?>
			    <?php echo $form->input('Lead.client_id', array('id'=>'','type'=>'hidden',"div"=>false,"label"=>false,'value'=>$leadData['Lead']['client_id']));  ?>
			    <?php echo $form->input('Lead.sp_id', array('id'=>'','type'=>'hidden',"div"=>false,"label"=>false,'value'=>$leadData['Lead']['sp_id']));  ?>
			    <?php echo $form->input('Lead.time_given', array('id'=>'ProfileImage','type'=>'hidden',"div"=>false,"label"=>false,'value'=>$leadData['Lead']['time_given']));  ?>
				    <!--one complete form-->
				<div class="form-box" style="width:90%">
				    <h2>Reason to decline request</h2>
				    <ul class="register-form">
					<li>
					    <label>&nbsp;</label>
					    <p><span class=""><?php echo $form->input('Lead.declinereason', array('maxLength'=>'','type'=>'textarea','rows'=>6,'cols'=>30,'class'=>'validate[required]',"div"=>false,"label"=>false));  ?></span></p>
					</li>					
					<li>
					    <label>&nbsp;</label>
					    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
					</li>
				    </ul>
				</div>
			   <?php echo $form->end(); ?>
			</div>
		    </td>
                </tr>
            </table>				
        </div>
	<?php } ?>    		
				
	<?php }else{ ?>
	<?php echo "<p style='color:red; font-size:15px; text-align:center' >No lead sent yet</p>"; } ?>
			
</div>
	
				
		