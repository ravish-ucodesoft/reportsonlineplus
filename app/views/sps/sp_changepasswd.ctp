<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#changepwdform").validationEngine();
});
</script>
<style>
.message{text-align:center;color:#54A41A;font-weight:bold;}
</style>
<div id="content" >
	      <div class='message'><?php echo $this->Session->flash();?></div>
              <div id="box" >
	      
                	<h3 id="adduser">Change Password</h3>
                	    <?php echo $form->create('Agent', array('type'=>'POST', 'action'=>'changepasswd','name'=>'changepwdform','id'=>'changepwdform')); ?>					
			    <?php echo $form->hidden('User.id',array('value'=>$spsession['User']['id'])); ?> 
                        <br/>
                        <table class="tbl">
                        <tr><td width="20%">Old Password<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->input('User.oldpassword',array('maxLength'=>'10','type'=>'password','class'=>'validate[required]','div'=>false,"label"=>false)); ?>             
                        </td>
                        </tr>   
                        
                        <tr><td>New Password<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.password',array('id'=>'password','maxLength'=>'10','type'=>'password','class'=>'validate[required]','div'=>false,"label"=>false)); ?></td>
                        </tr>                         
                        
                        <tr><td>Confirm Password<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.cpwd',array('type'=>'password','class'=>'validate[required,equals[password]]','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>                 
                     
                     <tr><td></td><td>
                      <div align="left">
                      <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
                      </div>
                      </td>
                      </tr>
                      </table>
                    	<?php echo $form->end(); ?>
                </div>
</div>