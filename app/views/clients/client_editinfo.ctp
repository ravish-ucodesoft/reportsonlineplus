<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#editinfo").validationEngine();
});

 function limit(field, chars) {
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
    
    function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxupload',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#CompanyImage").val(responseJSON.company_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/company_logo_temp/'+responseJSON.company_image+'" height="100px" width="200px">');
	    }
            });           
    }        
	window.onload = createUploader;
</script>
<script>
function getStates(id,value){
    var optArr = $("#"+id).val();
	jQuery("#UserStateId").html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
                url  : "/users/getByCountry/" + value,
                success : function(opt){
			jQuery('#UserStateId').html(opt);
                }
        });
}
 
 function showDropDown(id){
  if($("#"+id).is(":checked")){
       $("#tbl_"+id).show();
    }else {
	$("#tbl_"+id).hide();
    }
 }
</script>
<div id="content" >
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'editinfo'),'id'=>'editinfo')); ?>
	      <?php echo $form->input('User.id', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$this->data['User']['id']));  ?>
	      
              <div id="box" >
                	<h3 id="adduser">Edit Personnel Information</h3>
	                
                        <br/>
                        <table class="tbl">
                        <tr><td width="20%">First Name<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?>           
                        </tr>   
                        
                        <tr><td>Last Name<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false));  ?></td>
                        </tr>
                        <tr><td>Phone<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.phone', array('maxLength'=>'50',"div"=>false,"label"=>false));  ?></td>
                        </tr>
                        <tr><td>Email<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.email', array('maxLength'=>'50','readonly'=>true,"div"=>false,"label"=>false));  ?></td>
                        </tr>
			<tr><td colspan="4" style="font-size:15px;"><strong>Company Information</strong></td></tr>
			
			<tr><td>Company Name<span class="reqd">*</span>:</td>
                        <td colspan="3"><?php echo $form->input('Company.name',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>
			
			<tr><td>Company Contact Phone Number<span class="reqd">*</span>:</td>
                        <td colspan="3"><?php echo $form->input('Company.phone',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'maxLength'=>'20')); ?>
                        </td>                       
                        </tr>
			
			<tr><td>Company Street Address:</td>
                        <td colspan="3"><?php echo $form->input('Company.address',array('type'=>'text','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>
			
			<tr><td>Country<span class="reqd">*</span>:</td>
                        <td colspan="3"><?php echo $form->input('Company.country_id',array('options' =>$country,'id'=>'UserCountryId','label'=>false,"style"=>"width:208px;",'empty'=>'Please Select',"onchange"=>"getStates(this.id,this.value);")); ?>
                        </td>                       
                        </tr>
			
			<tr><td>state<span class="reqd">*</span>:</td>
                        <td colspan="3">
			    <?php // $state = ""; ?>
			    <?php  echo $this->Form->input('Company.state_id',array('options' =>$state,'id'=>'UserStateId','label' => false,'style'=>'width:208px;','empty'=>'Please Select'));?>
                        </td>                       
                        </tr>
			
			<tr><td>City</td>
                        <td colspan="3"><?php echo $form->input('Company.city',array('type'=>'text','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>
			
						</table>						
						</td>
						
						
						
						
						<!--<td  style="display:block;" >
					        </td>
						<td width="25%"></td>
						<td width="25%"></td>-->
					    </tr>
				
			
			</table>
			<br/>
			
			
			
                                        <tr><td></td><td>
			    <div align="right">
			    <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
			    </div>
			    </td>
	                  </tr>                         
              
                      </table>
                      	<?php echo $form->end(); ?>
                </div>

              
	
            </div>