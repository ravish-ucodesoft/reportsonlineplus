<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#changepwdform").validationEngine();
});
</script>
<style>
.message{text-align:center;color:#54A41A;font-weight:bold;}
</style>
<div class="register-wrap">
	      <div class='message'><?php echo $this->Session->flash();?></div>
              <div id="box" >
	<?php echo $form->create('', array('type'=>'POST', 'action'=>'changepasswd','name'=>'changepwdform','id'=>'changepwdform')); ?>					
			    <?php echo $form->hidden('User.id',array('value'=>$clientsession['User']['id'])); ?>
		<div class="form-box" style="width:90%;">
		    <br/>
		    <h2>Change Password</h2>
		    <ul class="register-form">
			<li>
			    <label>Old Password<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('User.oldpassword',array('maxLength'=>'10','type'=>'password','class'=>'validate[required]','div'=>false,"label"=>false)); ?></span></p>
			</li>
			<li>
			    <label>New Password<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('User.password',array('id'=>'password','maxLength'=>'10','type'=>'password','class'=>'validate[required]','div'=>false,"label"=>false)); ?></span></p>
			</li>
			<li>
			    <label>Confirm Password<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('User.cpwd',array('type'=>'password','class'=>'validate[required,equals[password]]','div'=>false,"label"=>false)); ?></span></p>
			</li>
			<li>
			<label>&nbsp;</label>
			    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>   
			</li>
			</ul>
		    </div>
	      <?php echo $form->end(); ?>
                </div>
</div>


