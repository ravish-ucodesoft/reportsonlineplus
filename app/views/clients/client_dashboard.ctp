<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    /*** alert box**/    
    echo $this->Html->css('alertbox/jquery.alerts.css');
    echo $this->Html->css('jquery/jquery.ui.draggable.js');
    echo $this->Html->script('alertbox/jquery.alert.js');
	echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox')); 
?>
<script type="text/javascript">
$(function(){ 
	  <?php if(!empty($annoucedata['SpGeneralCompanyAnnouncement']['for_clients'])&& $likeprompt==0) { ?>
	  $.fn.colorbox({href:"annoucement", open:true});
	  /*if (document.cookie.indexOf('visited=true') == -1) {
        var fifteenDays = 1000*60*60*24*15;
        var expires = new Date((new Date()).valueOf() + fifteenDays);
        document.cookie = "visited=true;expires=" + expires.toUTCString();
        $.fn.colorbox({href:"annoucement", open:true});
    }
	  */
	  
	  
	  
	  <?php } ?>
});

</script>
<style type="text/css">
    div#topRowsContainer{
	width: 99%;	
	float: left;
	padding-right: 1%;
	text-align: right;
    }
    div.topRow{
	float:left;
	padding-bottom:2px;
	width: 100%;
	font-size: 14px;
    }
</style>
<div style="margin:auto; width:98%">
<?php echo $this->element('client_left_menu'); ?>
  <div class="mdl-wrapper" style="width:60%">
    <div id="topRowsContainer">
	<div class="topRow">
	    <strong><?php echo $html->image('fineimages/not_started.png');?></strong>
	    <?php echo $html->link('<span style="color:red;font-size:16px;font-weight:bold;">'.$this->Common->getNotStartedReportsClient($session_client_id).'</span>',array('controller'=>'clients','action'=>'schedule','not_started'),array('escape'=>false));?>
	</div>
	<div class="topRow">
	    <strong><?php echo $html->image('fineimages/pending.png');?></strong>
	    <?php echo $html->link('<span style="color:#8b8b8b;font-size:16px;font-weight:bold;">'.$this->Common->getAllPendingReportsClient($session_client_id).'</span>',array('controller'=>'clients','action'=>'schedule','pending'),array('escape'=>false));?>
	</div>
	<div class="topRow">
	    <strong><?php echo $html->image('fineimages/finished.png');?></strong>
	    <?php echo $html->link('<span style="color:#28a701;font-size:16px;font-weight:bold;">'.$this->Common->getAllCompleteReportsClient($session_client_id).'</span>',array('controller'=>'clients','action'=>'schedule','completed'),array('escape'=>false));?>
	</div>	
    </div>
    
    <!---Service Provider Info------->
    <div style="clear:both;text-align:left;"><h1>Service Provider</h1></div>
    <table class="tbl input-chngs1" width="100%">
	<tr>
	    <td align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder;"><?php echo ucwords($spInfo['Company']['name']);?></span></td>								
	</tr>		 
	<tr>
	    <td align="center" style="border:none;"><?php echo $this->Html->image('company_logo/'.$spInfo['Company']['company_logo']); ?></td>
	</tr>
    </table>  
    <!---Service Provider Info------->
    
    <!---Notification Panel------->
    <div style="clear:both;text-align:left;"><h1>Notifications</h1></div>
    <table width="100%" cellpadding="3" cellspacing="3" border="0" class="tbl">
	<tr>
	   <td width="30%">Pending Inspection Requests</td><td><?php echo $html->link($pendingCall,array('controller'=>'clients','action'=>'pendingSchedule'));?></td>		
       </tr>       
       <tr>
	   <td>Completed Reports</td><td><?php echo $html->link($this->Common->getAllCompleteReportsClient($session_client_id),array('controller'=>'clients','action'=>'schedule','completed')); ?></td>		
       </tr>
       <tr>
	   <td>Open Quotes</td><td><?php echo $html->link($quote,'javascript:void(0);'); ?></td>		
       </tr>
       <tr>
	   <td>Services orders</td><td><?php echo $html->link($totalOrders,array('controller'=>'clients','action'=>'schedule')); ?></td>		
       </tr>       
    </table>
    <!---Notification Panel------->
    
    <div style="clear:both;text-align:left;"><h1>Personel Information</h1></div>
    <table width="100%" cellpadding="3" cellspacing="3" border="0" class="tbl">
    <?php //pr($clientsession); ?>
       <tr>
	   <td width="30%">Name</td><td><?php echo ucfirst($clientsession['User']['fname']." ".$clientsession['User']['lname']); ?></td>		
       </tr>       
       <tr>
	   <td>Email</td><td><?php echo $clientsession['User']['email']; ?></td>		
       </tr> 
      <!-- Direct Client info Start-->     
       <?php if ($clientsession['User']['direct_client']=="Yes"){ ?>
      <tr>
       	  <td>Site Name </td><td ><?php echo $clientsession['User']['site_name'];  ?></td> </tr>
        </tr>
        <tr>
       	  <td>Site Street Address </td><td ><?php echo $clientsession['User']['site_address'];  ?></td> </tr>
        </tr>
        <tr>
       	  <td>City, State, Zip </td><td ><?php echo $clientsession['User']['site_city_state_zip'];  ?></td> </tr>
        </tr>
        <tr>
       	  <td>Daytime Phone Number </td><td ><?php echo $clientsession['User']['site_phone'];  ?></td> </tr>
        </tr>

        <tr style="vertical-align:top">
       	  <td>Type of Business </td><td>
					
					<?php $arr=explode(',',$clientsession['Company']['type_business']);
				
					foreach($arr as $key=>$val):
					foreach($businessType as $key1=>$val1):
					

					if($val== $key1)
					{
						echo $val1."</br>";
					}
					
					endforeach;
					
					endforeach;
							?>	
				</td>
        </tr>                       		
        <tr>
       	  <td >Square Footage </td><td ><?php echo $clientsession['Company']['sqr_footage'];  ?></td> 
        </tr>
        <tr>
       	  <td  style="vertical-align:top" >Inspection/PM Service of interest </td>
					
					<td>
					<?php $arrReport=explode(',',$clientsession['Company']['interest_service']);
				
					foreach($arrReport as $key=>$val):
					foreach($reportType as $key1=>$val1):
					

					if($val== $key1)
					{
						echo $val1."</br>";
					}
					
					endforeach;
					
					endforeach;
							?>
					
				</td>
        </tr> 
     </table>
       <!-- Direct Client info end-->
      <?php }else{ ?>     
      <tr>
	   <td>Phone</td><td><?php echo $clientsession['User']['phone']; ?></td>		
       </tr>
        <tr>
          <td>Address</td><td><?php echo $clientsession['User']['address']?></td>
        </tr>        
    </table>
    </br>
    
    <?php } ?>
  </div>
  </div>
  
  <div class="right-wrapper">
	
  </div>
  <br/><br/>
  
  </div>      
     
	   		
  