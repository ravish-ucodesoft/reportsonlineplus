<?php    
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core','ui/ui.datepicker','jquery-ui-1.8.20.custom.min'));    
    
    $timeArray = array('12:00 am','12:30 am','01:00 am','01:30 am','02:00 am','02:30 am','03:00 am','03:30 am','04:00 am','04:30 am','05:00 am','05:30 am','06:00 am','06:30 am','07:00 am','07:30 am','08:00 am','08:30 am','09:00 am','09:30 am','10:00 am','10:30 am','11:00 am','11:30 am','12:00 pm','12:30 pm','01:00 pm','01:30 pm','02:00 pm','02:30 pm','03:00 pm','03:30 pm','04:00 pm','04:30 pm','05:00 pm','05:30 pm','06:00 pm','06:30 pm','07:00 pm','07:30 pm','08:00 pm','08:30 pm','09:00 pm','09:30 pm','10:00 pm','10:30 pm','11:00 pm','11:30 pm');
    $iniTimer = array(''=>'Select');
    foreach($timeArray as $times){
	$timesplits[$times] = $times;
    }
    $timesplits = array_merge($iniTimer,$timesplits);
?>
<style>
    .inner{	width:100%;}
    .inner td {font-size:12px;color:#2B92DD}
</style>
<script type="text/javascript">
    $(function(){
	// Accordion
	
			    
	$('#dialog_link, ul#icons li').hover(
            function() { $(this).addClass('ui-state-hover'); },
            function() { $(this).removeClass('ui-state-hover'); }
	);       
    });
    
    jQuery(document).ready(function(){
        loadDatePicker();
        //jQuery('.timepic').timepicker({
        //    ampm: true,
        //    hourMin: 0,
        //    hourMax: 23
        //});
	
	jQuery("#client_schedule_form").validationEngine();

    });    
    function loadDatePicker()
    {
       jQuery(".calender").datepicker({
            dateFormat: 'mm/dd/yy',
            showOn: 'button',
            minDate:1 ,
            buttonImage: '/img/calendar.gif',
            buttonImageOnly: true,
	    numberOfMonths: 3
        });
    }
    
    function submitForm(){
	$("#client_schedule_form").submit();
    }
    function cancelForm(){
	$("#cboxClose").trigger('click');
    }
    
/*Set Schedule to timer on change of schedule from timer*/
  /*Created On Jan 03, 2013 By Manish Kumar*/
  function setTotimer(fromObj){
    curVal = jQuery(fromObj).val();
    if(curVal==''){
	setData = '<option value=""></option>';
	jQuery(fromObj).parent('span').next('span').children('select').html(setData);
	return false;
    }
    data = 'fromVal='+curVal;
    jQuery.ajax({
	'url':'/sp/schedules/setScheduleTime/',
	'type':'post',
	'data':data,
	'success':function(msg){
	    if(msg!='error'){
		jQuery(fromObj).parent('span').next('span').children('select').html(msg);
	    }else{
		jQuery(fromObj).val('');
	    }
	}
    });
  }
  /*Set Schedule to timer on change of schedule from timer*/    
</script>
<style type="text/css">    			    
    /* css for timepicker */
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

/*demo page css*/
/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
.demoHeaders { margin-top: 2em; }
#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
ul#icons {margin: 0; padding: 0;}
ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
ul#icons span.ui-icon {float: left; margin: 0 4px;}
.ui-accordion-content{
    
}
    .butn {float:right;padding-right:20px;margin-bottom:10px;}
    .butn a { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
    .butn a:hover { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
    #ui-datepicker-div,.ui-timepicker-div{ z-index: 9999 !important;}
    /*.input-field {
        background: url("../../img/input-field-lft.png") no-repeat scroll left top transparent;
        display: block;
        height: 28px;
        padding-left: 5px;
        float: left;
        width: 75px;
    }
    .input-field input {
        background: url("../../img/input-field-rt.png") no-repeat scroll right top transparent;
        border: 0 none;
        color: #555555;
        float: left;
        font-size: 12px;
        height: 18px;
        padding: 5px 5px 5px 0;
        width: 240px;
    }*/
    span.input-field{
        margin-right: 10px;
    }
    span input.calender{
        width: 75px;
    }
    span input.timepic{
        width: 57px;
    }
    div.allSpans{
        float: left;
        width: 100%;
        padding-bottom: 5px;
    }
    div#fieldsContainer{
	width: 90%;	
	padding: 5px 0;
    }
    div.lblClass{	
	width: 100%;
	font-weight: bold;
	color: #000;
	font-size: 14px;
	padding-bottom: 5px;
    }
    div.txtAreaContainer{	
	width: 100%;
	text-align: left;
    }
</style>
</style>
<div id="content">
<?php //pr($data);?>
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>      
    <div id="box" >
        <h3 id="adduser">Schedule as Service Calls</h3>
        <br/>
        <table class="tbl">
            <tr></tr>
            <tr>
                <td width="70%" align="left">		    
                    <div id="accordion">				
                        <?php
                            if(!empty($data))
                            {
                                $res= $data;                                    
				$serviceCall_ID=$res['ServiceCall']['id'];
				$client_ID=$res['ServiceCall']['client_id'];
				$sp_ID=$res['ServiceCall']['sp_id'];
                        ?>
			<div>
			    <?php echo $form->create('ClientSchedule',array('url'=>array('controller'=>'clients','action'=>'declineRequest',base64_encode($res['ServiceCall']['id'])),'id'=>'client_schedule_form'));?>
			    <div id="fieldsContainer">
				<div class="lblClass">
				    Please Specify the reason in the box below and provide your suitable schedule in the table below:
				</div>
				<div class="txtAreaContainer">
				    <?php echo $form->textarea('ServiceCall.declined_reason',array('rows'=>'3','cols'=>'50','class'=>'validate[required]'));?>
				</div>
			    </div>
                            <h3 style="background: -moz-linear-gradient(center top , #FFFFFF 0%, #E5E5E5 100%) repeat scroll 0 0 transparent !important; border: 2px solid #C6C6C6 !important"><a href="#" style="color: #EB8F00"><?php echo 'Name : '.$res['ServiceCall']['name'].' <span style="color:#EA7060">[Service Call Date : '.date('m/d/Y H:i:s a',strtotime($res['ServiceCall']['created'])).']</span>'; ?></a></h3>
			    
                            <table class="inner" cellpadding="0" cellspacing="0" width="100%">
                                <tr style="background:url('../../img/newdesign_img/nav_bg.png') bottom;">
                                    <th width="15%" align="center" style="color:#FFF">Report</th>
                                    <th width="15%" align="center" style="color:#FFF">Lead Inspector</th>
                                    <th width="10%" align="center" style="color:#FFF">Helper</th>
                                    <th width="20%" align="center" style="color:#FFF" align="center">Sch Date(m/d/Y)</th>                                    
                                    <th width="40%" align="center" style="color:#FFF" align="center">&nbsp;</th>                                    
                                </tr>
				<?php
                                $k=0;
                                foreach($res['Schedule'] as $key1=>$schedule):
                                $k++;
                                ?>
				<tr style="background-color:#eaeaea;">
                                    <?php //echo $form->input('ClientSchedule.'.$k.'.service_call_id',array('type'=>'hidden','value'=>$schedule['service_call_id']));?>
				    <?php echo $form->input('ClientSchedule.'.$k.'.report_id',array('type'=>'hidden','value'=>$schedule['report_id']));?>
				    <?php echo $form->input('ClientSchedule.'.$k.'.lead_inspector_id',array('type'=>'hidden','value'=>$schedule['lead_inspector_id']));?>
                                    <td align="center"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
                                    <td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
                                    <td align="center">
                                        <?php
                                            $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
                                            echo $this->Common->getHelperName($helper_ins_arr);
                                        ?>
                                    </td>
                                    <td align="center">
					<?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:ia', $schedule['schedule_from_1']).'-'.$time->format('g:ia', $schedule['schedule_to_1']); ?><br/>
                                        <?php if(!empty($schedule['schedule_date_2'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:ia', $schedule['schedule_from_2']).'-'.$time->format('g:ia', $schedule['schedule_to_2'])?><br/>
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_3'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:ia', $schedule['schedule_from_3']).'-'.$time->format('g:ia', $schedule['schedule_to_3'])?><br/>
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_4'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:ia', $schedule['schedule_from_4']).'-'.$time->format('g:ia', $schedule['schedule_to_4'])?><br/>
                                        <?php } ?>
					<?php if(!empty($schedule['schedule_date_5'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:ia', $schedule['schedule_from_5']).'-'.$time->format('g:ia', $schedule['schedule_to_5'])?><br/>
                                        <?php } ?>
					<?php if(!empty($schedule['schedule_date_6'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:ia', $schedule['schedule_from_6']).'-'.$time->format('g:ia', $schedule['schedule_to_6'])?><br/>
					<?php } ?>
					<?php if(!empty($schedule['schedule_date_7'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:ia', $schedule['schedule_from_7']).'-'.$time->format('g:ia', $schedule['schedule_to_7'])?><br/>
                                        <?php } ?>
					<?php if(!empty($schedule['schedule_date_8'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:ia', $schedule['schedule_from_8']).'-'.$time->format('g:ia', $schedule['schedule_to_8'])?><br/>
					<?php } ?>
					<?php if(!empty($schedule['schedule_date_9'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:ia', $schedule['schedule_from_9']).'-'.$time->format('g:ia', $schedule['schedule_to_9'])?><br/>
					<?php } ?>
					<?php if(!empty($schedule['schedule_date_10'])) { ?>
                                            <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:ia', $schedule['schedule_from_10']).'-'.$time->format('g:ia', $schedule['schedule_to_10'])?>
					<?php } ?>
                                    </td>                                    
                                    <td align="center">
                                        <div class="allSpans">
                                            <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_1]"/>
                                            </span> 
                                            <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
					    <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_1]','onchange'=>'setTotimer(this);'));?></span>
					    <span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_1]'));?></span>
                                        </div>
                                        <?php if(!empty($schedule['schedule_date_2'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_2]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
						<span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_2]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_2]'));?></span>
                                            </div>    
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_3'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_3]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
                                                <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_3]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_3]'));?></span>
                                            </div>    
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_4'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_4]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
                                                <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_4]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_4]'));?></span>
                                            </div>    
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_5'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_5]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
                                                <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_5]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_5]'));?></span>
                                            </div>    
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_6'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_6]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
                                                <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_6]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_6]'));?></span>
                                            </div>    
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_7'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_7]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
                                                <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_7]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_7]'));?></span>
                                            </div>    
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_8'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_8]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
                                                <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_8]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_8]'));?></span>
                                            </div>    
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_9'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_9]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
                                                <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_9]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_9]'));?></span>
                                            </div>    
                                        <?php } ?>
                                        <?php if(!empty($schedule['schedule_date_10'])) { ?>
                                            <div class="allSpans">
                                                <span class="input-field wdth-150" style="width: 100px; float: left;">
                                                    <input type="text" class="calender validate[required]" value="" name="data[ClientSchedule][<?php echo $k?>][schedule_date_10]"/>
                                                </span> 
                                                <!--<a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>-->
                                                <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_from_10]','onchange'=>'setTotimer(this);'));?></span>
						<span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','style'=>'background-color:#fff;','name'=>'data[ClientSchedule]['.$k.'][schedule_to_10]'));?></span>
                                            </div>    
                                        <?php } ?>
                                    </td>                                    
				</tr>
				<tr>
                                    <td colspan="6" align="left">
                                        <div class="service-hdline">Services Taken</div>
                                            <ul class="services-tkn">
                                                <li>
						<?php
                                                    $i=0;$j=1;
                                                    for($i=0;$i<count($schedule['ScheduleService']);$i++)
                                                    {
							echo $html->image('tick.png');
                                                ?>
                                                        <?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
                                                <?php
                                                            if($j%4==0)
                                                            {
                                                                echo '</li></ul><ul class="services-tkn"><li>';
                                                            }
                                                            else
                                                            {
                                                                echo '</li><li>';
                                                            }
                                                            $j++;
                                                    }
						?>
                                    </td>
                                </tr>
				<?php endforeach;?>                                
                                <tr>
                                    <td colspan="5">							
                                        <div class="butn">
                                            <?php echo $html->link('Submit','javascript:void(0);', array('onclick'=>'submitForm();'));?>
                                            <?php echo $html->link('Cancel','javascript:void(0);', array('onclick'=>'cancelForm();')); ?>
                                        </div>							
                                    </td>
                                </tr>                               
                            </table>
			    <?php echo $form->end();?>
                        </div>
                        <?php }?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>    