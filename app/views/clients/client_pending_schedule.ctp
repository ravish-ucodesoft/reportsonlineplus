<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox')); 
?>
<style>
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#2B92DD}
</style>
<script type="text/javascript">
    $(function(){
       
	    // Accordion
	$("#accordion").accordion({ header: "h3",active:false,collapsible: true});
	
	$('#dialog_link, ul#icons li').hover(
		    function() { $(this).addClass('ui-state-hover'); },
		    function() { $(this).removeClass('ui-state-hover'); }
	);
	
	jQuery(".ajax").colorbox();
    });
			
    function chkConfirm(obj){
	if(!confirm("Are you sure that you want to decline this service call?")){				
	    return false;
	}else{
	    jQuery(obj).next('a').trigger('click');
	    return false;
	}
    }
			
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}
    .butn {float:right;padding-right:20px;margin-bottom:10px;}
    .butn a { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
    .butn a:hover { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<div class="linkContainer">
    <div class="butn">
	<?php echo $html->link('Approved Service Calls',array('controller'=>'clients','action'=>'schedule'));?>	
    </div>
    <!--<div class="butn">
	<?php //echo $html->link('Response Service Calls',array('controller'=>'sps','action'=>'responseSchedule'));?>	
    </div>-->
</div>
<div id="content">
    <?php //pr($data);?>
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>	      
    <div id="box" >
	<h3 id="adduser">Pending schedule as Service Calls</h3>
	<br/>
        <table class="tbl">
	    <tr></tr>
	    <tr>
		<td width="70%" align="left">
		    <div id="accordion">				
			<?php if(!empty($data))
			{
			    foreach($data as $key=>$res):
				$serviceCall_ID=$res['ServiceCall']['id'];
				$client_ID=$res['ServiceCall']['client_id'];
				$sp_ID=$res['ServiceCall']['sp_id']; ?>
				<div>
				    <h3><a href="#" style="color: #000 !important;"><?php echo 'Name : '.$res['ServiceCall']['name'].' <span style="color:grey">[Sch. requested : '.date('m/d/Y',strtotime($res['ServiceCall']['created'])).']</span>'; ?></a></h3>
				    <table class="inner" cellpadding="0" cellspacing="0" width="100%">
					<tr style="background:url('../../img/newdesign_img/nav_bg.png') bottom;">
					    <th width="15%" align="center" style="color:#FFF">Report</th>
						<th width="15%" align="center" style="color:#FFF">Lead Inspector</th>
						<th width="20%" align="center" style="color:#FFF">Helper</th>
						<th width="20%" align="center" style="color:#FFF">Sch Date(m/d/Y)</th>
						<?php if($res['ServiceCall']['call_status']==2){?>
						<th width="20%" align="center" style="color:#FFF">Requested Sch Date(m/d/Y)</th>
						<?php }?>						
					</tr>
					<?php
					$clientScheduleCounter = 0;
					foreach($res['Schedule'] as $key1=>$schedule):
					?>
					<tr style="background-color:#eaeaea;">
					    <td align="center" style="font-size: 16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
					    <td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
					    <td align="center"><?php
					    $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
					    echo $this->Common->getHelperName($helper_ins_arr); ?></td>
					    <td align="center">
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:ia', $schedule['schedule_from_1']).'-'.$time->format('g:ia', $schedule['schedule_to_1']); ?>
<br/>
						<?php if(!empty($schedule['schedule_date_2'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:ia', $schedule['schedule_from_2']).'-'.$time->format('g:ia', $schedule['schedule_to_2'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_3'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:ia', $schedule['schedule_from_3']).'-'.$time->format('g:ia', $schedule['schedule_to_3'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_4'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:ia', $schedule['schedule_from_4']).'-'.$time->format('g:ia', $schedule['schedule_to_4'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_5'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:ia', $schedule['schedule_from_5']).'-'.$time->format('g:ia', $schedule['schedule_to_5'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_6'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:ia', $schedule['schedule_from_6']).'-'.$time->format('g:ia', $schedule['schedule_to_6'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_7'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:ia', $schedule['schedule_from_7']).'-'.$time->format('g:ia', $schedule['schedule_to_7'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_8'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:ia', $schedule['schedule_from_8']).'-'.$time->format('g:ia', $schedule['schedule_to_8'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_9'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:ia', $schedule['schedule_from_9']).'-'.$time->format('g:ia', $schedule['schedule_to_9'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_10'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:ia', $schedule['schedule_from_10']).'-'.$time->format('g:ia', $schedule['schedule_to_10'])?>
						<?php } ?>
					    </td>
					    <?php
					    if($res['ServiceCall']['call_status']==2){
						$clientSchedule = $res['ClientSchedule'][$clientScheduleCounter];							    
					    ?>
					    <td align="center">
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_1'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_1']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_1']); ?>
<br/>
						<?php if(!empty($clientSchedule['schedule_date_2'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_2'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_2']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_2'])?><br/>
						<?php } ?>
						<?php if(!empty($clientSchedule['schedule_date_3'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_3'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_3']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_3'])?><br/>
						<?php } ?>
						<?php if(!empty($clientSchedule['schedule_date_4'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_4'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_4']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_4'])?><br/>
						<?php } ?>
						<?php if(!empty($clientSchedule['schedule_date_5'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_5'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_5']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_5'])?><br/>
						<?php } ?>
						<?php if(!empty($clientSchedule['schedule_date_6'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_6'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_6']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_6'])?><br/>
						<?php } ?>
						<?php if(!empty($clientSchedule['schedule_date_7'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_7'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_7']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_7'])?><br/>
						<?php } ?>
						<?php if(!empty($clientSchedule['schedule_date_8'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_8'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_8']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_8'])?><br/>
						<?php } ?>
						<?php if(!empty($clientSchedule['schedule_date_9'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_9'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_9']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_9'])?><br/>
						<?php } ?>
						<?php if(!empty($clientSchedule['schedule_date_10'])) { ?>
						<?php echo date('m/d/Y',strtotime($clientSchedule['schedule_date_10'])).' '.$time->format('g:ia', $clientSchedule['schedule_from_10']).'-'.$time->format('g:ia', $clientSchedule['schedule_to_10'])?>
						<?php } ?>
					    </td>
					    <?php }?>					    
					</tr>
					<tr>
					    <td colspan="6" align="left">
						<div class="service-hdline">Inspection Devices</div>
						<ul class="services-tkn">
						    <li>
						    <?php $i=0;$j=1;
						    for($i=0;$i<count($schedule['ScheduleService']);$i++)
						    {
							    echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
						      <?php if($j%4==0)
						    {
							    echo '</li></ul><ul class="services-tkn"><li>';
						    }
						    else
						    {
							    echo '</li><li>';
						    }
						     $j++;
						    }
						    ?>
					    </td>
					</tr>
					<?php $clientScheduleCounter++;endforeach;?>
					<?php if($res['ServiceCall']['call_status']==0){?>
					<tr>
					    <td colspan="5">							
						<div class="butn">
						    <?php echo $html->link('Approve',array('controller'=>'clients','action'=>'changeststatus',base64_encode($res['ServiceCall']['id']),base64_encode('1')),array('title'=>'Approve','onclick'=>'if(!confirm("Are you sure that you want to approve this service call?")){return false;}'));?>
						    <?php echo $html->link('Declined','javascript:void(0);',array('title'=>'Declined','onclick'=>'chkConfirm(this);'));?>
						    <?php echo $html->link('Declined',array('controller'=>'clients','action'=>'declineRequest',base64_encode($res['ServiceCall']['id'])),array('class'=>'ajax','title'=>'Declined','style'=>'display:none'));?>
						    <?php //echo $html->link('Declined',array('controller'=>'clients','action'=>'declineRequest',base64_encode($res['ServiceCall']['id'])),array('id'=>'clickLink_'.$serviceCall_ID,'class'=>'ajax','style'=>'display:none'));?>
						    <?php //echo $html->link('Declined',array('controller'=>'clients','action'=>'changeststatus',base64_encode($res['ServiceCall']['id']),base64_encode('2')),array('title'=>'Declined','confirm'=>'Are you sure that you want to decline this service call?'));?>
						</div>							
					    </td>
					</tr>
					<?php }else if($res['ServiceCall']['call_status']==2){?>
					<tr>
					    <td colspan="5" style="font-size: 14px; color: #000; font-weight: bold; text-align: right;">							
						Change in schedule request has already been sent.
					    </td>
					</tr>
					<?php }?>
					<tr><td colspan="5"></td></tr>
				    </table>					
				</div>
			<?php endforeach; }else{?>
                        <div style="padding-left: 10px;">No Record Found</div>
                        <?php }?>
		    </div>
		</td>
	    </tr>
	</table>
    </div>
</div>    