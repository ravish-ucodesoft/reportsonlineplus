<?php
    echo $this->Html->script('jquery.1.6.1.min');  
    echo $this->Html->css('validationEngine.jquery');    
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#QuoteForm").validationEngine();
    jQuery("li#acceptLi").hide();
    jQuery("li#deniedLi").hide();
    jQuery("li#noLi").hide();
    jQuery("li#submitLi").hide();
    
});

function deleteRecord(fileName){
    jQuery.ajax({
        'url': '/inspector/messages/deleteFile/'+fileName,
        'success': function(msg){
            if(msg=='OK'){
                jQuery("#showFile").remove();
            }    
        }
    });
}
function setResponse(responseValue){
    if(responseValue=='a'){
        jQuery("li#acceptLi").show();
        jQuery("li#deniedLi").hide();
        jQuery("li#noLi").hide();
        jQuery("li#submitLi").show();
    }else if(responseValue=='d'){
        jQuery("li#acceptLi").hide();
        jQuery("li#deniedLi").show();
        jQuery("li#noLi").hide();
        jQuery("li#submitLi").show();
    }else if(responseValue=='n'){
        jQuery("li#acceptLi").hide();
        jQuery("li#deniedLi").hide();
        jQuery("li#noLi").show();
        jQuery("li#submitLi").show();
    }
}
</script>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
            <?php echo $this->Form->create('', array('url'=>array('controller'=>'clients','action' => 'viewQuote?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),'id'=>'QuoteForm','name'=>'QuoteForm','enctype'=>'multipart/form-data') );?>
            <section class="form-box" style="width:100%; margin: 0">
                <h2><strong>Quote for the report <?php echo $this->Common->getReportName($reportId);?></strong></h2>
                <ul class="register-form">
                    <?php if(is_array($quoteData) && $quoteData['Quote']['attachment']!="" && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'quotefile'.DS.$quoteData['Quote']['attachment'])){?>
                    <li id="showFile">
                        <label>Quote form </label>
                        <p style="padding-top: 7px;">
                           <?php echo $html->link($quoteData['Quote']['title'],array('controller'=>'sps','action'=>'download_file',$quoteData['Quote']['attachment'],'quotefile','sp'=>true));?>                           
                        </p>
                    </li>
                    <?php }?>
                    <?php if($quoteData['Quote']['client_response']==""){?>
                    <li>
                        <label style="width: 650px; text-align: left;">You would have to print, sign scan and attach this document to the report/quote folder.</label>
                    </li>
                    <li>
                        <?php
                            $options = array('a'=>'Accept','d'=>'Denied','n'=>'No wish to fix the deficiencies');
                            $attributes=array('legend'=>false,'separator'=>'&nbsp;&nbsp;','onclick'=>'setResponse(this.value);');  
                        ?>
                        <label style="width: 650px; text-align: left;"><?php echo $this->Form->radio('Quote.client_response',$options,$attributes);?></label>
                    </li>
                    <li id="acceptLi">
                        <label>Upload signed quote form</label>
                        <p><span class=""><?php echo $form->file('Quote.signed_form', array("div"=>false,"label"=>false,"class"=>"validate[required]"));  ?></span></p>
                        <span>(Only pdf,xls,doc,docx,xlsx file extension)</span>
                    </li>                    
                    <li id="deniedLi">
                        <label>Upload work orders</label>
                        <p><span class=""><?php echo $form->file('Quote.work_order_form', array("div"=>false,"label"=>false,"class"=>"validate[required]"));  ?></span></p>
                        <span>(Only pdf,xls,doc,docx,xlsx file extension)</span>
                    </li>
                    <li id="noLi" style="padding-top: 10px;">                        
                        <span style="font-size: 12px; color:red">Deficiencies and recommendations have been found and Sp have identified them and you want to perform the functions in different manner</span>
                    </li>
                    <li id="submitLi">
                        <label>&nbsp;</label>
                        <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
                    </li>
                    <?php }else{?>
                    <li>                        
                        <label style="width: 650px; text-align: left; font-weight: bold;">Submitted Response:</label>
                    </li>
                    <?php
                        if($quoteData['Quote']['client_response']=='a'){
                            $status = 'Accepted';
                        }else if($quoteData['Quote']['client_response']=='d'){
                            $status = 'Denied';
                        }else if($quoteData['Quote']['client_response']=='n'){
                            $status = 'No wish to fix the deficiencies';
                        }
                    ?>
                    <li>
                        <label>Status:</label>
                        <p style="padding-top: 7px;"><?php echo $status;?></p>
                    </li>
                    <?php if($quoteData['Quote']['client_response']=='a'){?>
                    <li>
                        <label>Signed form:</label>
                        <p style="padding-top: 7px;"><?php echo $html->link('Download',array('controller'=>'sps','action'=>'download_file',$quoteData['Quote']['signed_form'],'quotefile','sp'=>true))?></p>
                    </li>
                    <?php }else if($quoteData['Quote']['client_response']=='d'){?>
                    <li>
                        <label>Work Order form:</label>
                        <p style="padding-top: 7px;"><?php echo $html->link('Download',array('controller'=>'sps','action'=>'download_file',$quoteData['Quote']['work_order_form'],'quotefile','sp'=>true))?></p>
                    </li>
                    <?php }?>
                    <?php }?>
                </ul>
            </section>
            <?php echo $form->end(); ?>
             <br/>   
</div>