<?php echo $this->Html->script('fckeditor'); ?>
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Add Banner</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addbanner') ,'name'=>'addform','id'=>'addform','enctype' => 'multipart/form-data')); ?>
		
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <table width="100%">
			<tr><td>Upload an Image(514px X 384px): <span class="star">*</span></td>
			<td><?php echo $this->Form->input('Banner.banner_img',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			</tr>
			<tr><td>Title: <span class="star">*</span></td>
			<td><?php echo $this->Form->input('Banner.title',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false,'size'=>50)); ?></td>
			</tr>
			<tr><td>Description: <span class="star">*</span></td>
			<td>
				<div class="row">
				<label>Page Content : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:750px;">
					<?php echo $form->input('Banner.description', array('type'=>'textarea','rows' => 10,'cols' =>40,'label'=>false));  
					      echo $fck->load('BannerDescription');
					?>
					</span>
				</div>
				</div>
				<?php //echo $this->Form->input('Banner.description',array('type'=>'textarea','label'=>'','readonly'=>'','rows'=>6,'cols'=>40,'div'=>false,'label'=>false)); ?>
			</td>
			</tr>
					
				
		</table>
			<!--[if !IE]>start row<![endif]-->
		<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save');  ?></span>
    			<!--span class="button red_button"><a href="/wellnesscenter1/websitepages/pagelisting" ><span><span style="color:white">Cancel</span></span></a></span-->
			<span class="button red_button"><a href="javascript:history.go(-1);" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
		</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
