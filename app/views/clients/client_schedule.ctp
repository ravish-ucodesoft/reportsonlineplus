<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox')); 
?>
<style>
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#2B92DD}
</style>
<script type="text/javascript">
    $(function(){
       
	    // Accordion
	$("#accordion").accordion({ header: "h3",active:false,collapsible: true});
	
	$('#dialog_link, ul#icons li').hover(
		    function() { $(this).addClass('ui-state-hover'); },
		    function() { $(this).removeClass('ui-state-hover'); }
	);
	
	jQuery(".ajax").colorbox();
    }); 
			
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}
    .butn {float:right;padding-right:20px;margin-bottom:10px;}
    .butn a { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
    .butn a:hover { color:#fff; background:url("../../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<div class="linkContainer">
    <div class="butn">
	<?php echo $html->link('Pending Service Calls',array('controller'=>'clients','action'=>'pendingSchedule'));?>	
    </div>
    <!--<div class="butn">
	<?php //echo $html->link('Response Service Calls',array('controller'=>'sps','action'=>'responseSchedule'));?>	
    </div>-->
</div>
<div id="content">
    <?php //pr($data);?>
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>	      
    <div id="box" >
	<h3 id="adduser">Approved Schedule as Service Calls</h3>
	<br/>
        <table class="tbl">
	    <tr></tr>
	    <tr>
		<td width="70%" align="left">
		    <div id="accordion">				
			<?php if(!empty($data))
			{
			    foreach($data as $key=>$res):
				$serviceCall_ID=$res['ServiceCall']['id'];
				$client_ID=$res['ServiceCall']['client_id'];
				$sp_ID=$res['ServiceCall']['sp_id'];
				
				$setParam = (isset($this->params['pass'][0]))?$this->params['pass'][0]:"";
				$chkPending = $this->Common->checkPendingServiceCall($serviceCall_ID);
				$chkNotStarted = $this->Common->checkNotStartedServiceCall($serviceCall_ID);
				$chkCompleted = $this->Common->checkCompleteServiceCall($serviceCall_ID);
				if($setParam == 'not_started'){
				    if($chkNotStarted==false){
					continue;
				    }
				}
				if($setParam == 'pending'){
				    if($chkPending==false){
					continue;
				    }
				}
				if($setParam == 'completed'){
				    if($chkCompleted==false){
					continue;
				    }
				}
			?>
				
				
				<div>
				    <?php $chkLeaders = $this->Common->getLeadIns($res['ServiceCall']['id']);?>
				    <h3><a href="#" style="color: #000 !important;"><?php echo 'Name : '.$res['ServiceCall']['name'].' <span style="color:grey">[Sch. requested : '.date('m/d/Y',strtotime($res['ServiceCall']['created'])).']</span><span style="color:#000"> [Lead Inspectors-'.$chkLeaders.']</span>'.'<span style="color:grey"> [Approval Date-'.date('m/d/Y',strtotime($res['ServiceCall']['modified'])).']</span>'; ?></a></h3>
				    <table class="inner" cellpadding="0" cellspacing="0" width="100%">
					<tr style="background:url('../../img/newdesign_img/nav_bg.png') bottom;">
					    <th width="17%" align="center" style="color:#FFF">Report</th>
						<th width="17%" align="center" style="color:#FFF">Lead Inspector</th>
						<th width="22%" align="center" style="color:#FFF">Helper</th>
						<th width="22%" align="center" style="color:#FFF">Sch Date(m/d/Y)</th>						
						<th width="17%" align="center" style="color:#FFF">Status</th>
						<th width="17%" align="center" style="color:#FFF">&nbsp;</th>						
					</tr>
					<?php
					$clientScheduleCounter = 0;
					foreach($res['Schedule'] as $key1=>$schedule):
					    $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
					    $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
					    
					    if($setParam=='not_started'){
						if(!($getstarted == 0 && $getCompleted == "")){
						    continue;
						}
					    }
					    if($setParam=='pending'){
						if(!($getstarted == 1 && $getCompleted == "")){
						    continue;
						}
					    }
					    if($setParam=='completed'){
						if(!($getCompleted != "" && $getstarted == 1)){
						    continue;
						}
					    }
					?>
					<tr style="background-color:#eaeaea;">
					    <td align="center" style="font-size: 16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
					    <td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
					    <td align="center"><?php
					    $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
					    echo $this->Common->getHelperName($helper_ins_arr); ?></td>
					    <td align="center">
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:i a', $schedule['schedule_from_1']).'-'.$time->format('g:i a', $schedule['schedule_to_1']); ?>
<br/>
						<?php if(!empty($schedule['schedule_date_2'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:i a', $schedule['schedule_from_2']).'-'.$time->format('g:i a', $schedule['schedule_to_2'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_3'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:i a', $schedule['schedule_from_3']).'-'.$time->format('g:i a', $schedule['schedule_to_3'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_4'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:i a', $schedule['schedule_from_4']).'-'.$time->format('g:i a', $schedule['schedule_to_4'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_5'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:i a', $schedule['schedule_from_5']).'-'.$time->format('g:i a', $schedule['schedule_to_5'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_6'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:i a', $schedule['schedule_from_6']).'-'.$time->format('g:i a', $schedule['schedule_to_6'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_7'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:i a', $schedule['schedule_from_7']).'-'.$time->format('g:i a', $schedule['schedule_to_7'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_8'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:i a', $schedule['schedule_from_8']).'-'.$time->format('g:i a', $schedule['schedule_to_8'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_9'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:i a', $schedule['schedule_from_9']).'-'.$time->format('g:i a', $schedule['schedule_to_9'])?><br/>
						<?php } ?>
						<?php if(!empty($schedule['schedule_date_10'])) { ?>
						<?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:i a', $schedule['schedule_from_10']).'-'.$time->format('g:i a', $schedule['schedule_to_10'])?>
						<?php } ?>
					    </td>
					    
					    
					    <td align='center'>
						<?php
						    $finishFlag = "";
						    if($getCompleted != "" && $getstarted == 1){
							echo "<a href=''></a>";
							$finishFlag = "finish";
						?>
						<!--<blink>Completed on <?php //echo date('m/d/Y',strtotime($getCompleted)); ?></blink>-->
						<?php echo $html->image('fineimages/finished.png');?>
						<?php
						    }
						    if($getstarted == 0 && $getCompleted == ""){
						       //echo "<blink>Not Started Yet</blink>";
						       echo $html->image('fineimages/not_started.png');
						    }
						    else if($getstarted == 1 && $getCompleted == ""){
						       //echo "<blink>In Progress</blink>";
						       echo $html->image('fineimages/pending.png');
						    }
						?>
					    </td>
					    <td align="center">
						<?php
						//if($getCompleted != "" && $getstarted == 1){						    
						    //echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/client/reports/'.$this->Common->getReportLinkName($schedule['report_id']).'?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
						    if( $finishFlag != ""){
							if($schedule['report_id']==1){
							    //echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/inspector/reports/firealarmReportPreview?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
							    echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/reports/firealarmFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
							}else if($schedule['report_id']==2){
							    echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/reportPrints/sprinklerFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
							}else if($schedule['report_id']==3){
							    echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/reportPrints/kitchenhoodFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
							}else if($schedule['report_id']==4){
							    echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/reports/emergencyexitFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
							}else if($schedule['report_id']==5){
							    echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/reports/extinguisherFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
							}else if($schedule['report_id']==6){
							    echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/reportPrints/specialhazardFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
							}else{
							    echo $html->link($html->image('fineimages/view_report.png',array('title'=>'View Report','alt'=>'View Report')),'/reports/firealarmFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],array('escape'=>false));
							}
						    }else{
							echo '&nbsp;';
						    }
						?>
						<span style='vertical-align:bottom'>
						    <?php $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$serviceCall_ID,$client_ID,$sp_ID);
						    if($getCompleted != ""){
							$chkQuote = $this->Common->getQuoteDoc($schedule['report_id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id'],$res['ServiceCall']['id']);
							if(is_array($chkQuote)){							
							?>
							<br/>
							<!--<blink>Completed on <?php //echo $getCompleted; ?></blink>-->
							<?php echo $html->link($html->image('fineimages/view_quote.png'),array('controller'=>'clients','action'=>'viewQuote?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id']),array('title'=>'View Quote','escape'=>false));?>
						    <?php }} ?>
						<?php //}?>    
					    </td>						
					</tr>
					<tr>
					    <td colspan="6" align="left">
						<div class="service-hdline">Inspection Devices</div>
						<ul class="services-tkn">
						    <li>
						    <?php $i=0;$j=1;
						    for($i=0;$i<count($schedule['ScheduleService']);$i++)
						    {
							    echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
						      <?php if($j%4==0)
						    {
							    echo '</li></ul><ul class="services-tkn"><li>';
						    }
						    else
						    {
							    echo '</li><li>';
						    }
						     $j++;
						    }
						    ?>
					    </td>
					</tr>
					<tr><td colspan="6"></td></tr>
					<?php $clientScheduleCounter++;endforeach;?>					
				    </table>					
				</div>
			<?php endforeach; }else{?>
			<div style="padding-left: 10px;">No Record Found</div>
			<?php }?>
		    </div>
		</td>
	    </tr>
	</table>
    </div>
</div>    