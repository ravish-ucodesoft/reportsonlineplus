<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#step2").validationEngine();
});
</script>
<!-- Begin: Container -->
<section id="container_page_bg">
      <section id="container_wrap">
	      <section id="content">
             <!-- Aside -->
             <aside id="cont_l">
               <section class="sidemenu">
                <section class="sidemenu_top"></section>
                <section class="sidemenu_mid">
                  <h2><?php echo $tabData['MainTab']['title1'];?></h2>
                  <p>
		    <?php echo $tabData['MainTab']['description1'];?><br/>
		    <?php if($this->params['controller'] == "users" && $this->params['action'] == "step2"){ ?>
		    <!-- PayPal Logo -->
					<table border="0" cellpadding="10" cellspacing="0" align="center">
					    <tr>
						   <td align="left">
							  <script type="text/javascript">
							  function openBpWindow(){
								 window.open('https://www.paypal-apac.com/buyer-protection.html?size=180x113&url=' + escape(window.location.hostname) + '&page=' + escape(window.location.pathname),'olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=260');};
								 document.write("<a href=\"javascript:openBpWindow()\"><img  src=\"https://www.paypal-apac.com/images/logos/PayPal-Buyer-Protection-Logo-180p-x-113p.gif\" border=\"0\" alt=\"Acceptance Mark\"></a>");
								 document.write("<img  src=\"https://www.paypal-apac.com/tracking/?size=180x113&url=" + escape(window.location.hostname) + "&page=" + escape(window.location.pathname) + "\" border=\"0\" alt=\"\" width=\"1\" height=\"1\">")
							  </script>
						   </td>
					    </tr>
					</table>
		    <!-- PayPal Logo -->
		  <?php } ?>
		  </p>			      
                </section>
                <section class="sidemenu_btm"></section>
               </section>
	     </aside>
	     
             <!-- Aside -->
             <!-- Content right -->
             <section class="cont_wrap">
                <section class="cont_top"></section>
                    <section class="cont_mid">
	                       <h2>Step1 form for Service Provider</h2>
			      <div class='message'><?php echo $this->Session->flash();?></div>
                           <section class="form">
                             <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'step2'),'id'=>'step2')); ?>
			     <?php echo $form->input('User.option',array('type'=>'hidden','value'=>$option)); ?>
                                <section class="level_wrap">
				    <?php if($option == "On") { ?>
                                <h3>You have the 60 DAY'S trial period,Please choose your plan from below</h3>
				<?php } else { ?>
				    <h3>Payment Page</h3>
				<?php } ?>
                                    <ul class="sign-form">
					<?php
					    if($option == "On"){
					     $style = 'style="display:none;"';	
					    }else {
					     $style = 'style="display:block;"';	
					    }
					?>
				    <div <?php echo $style; ?>>
				    <li>
				      <label>CCType<span class="astric">*</span> </label>
				      <div class="input_bg">
					<?php
					    $option=array('Visa'=>'Visa',
                                                    'Mastercard'=>'Mastercard',
                                                    'AmericanExpress'=>'AmericanExpress',
                                                    'DiscoverCard'=>'DiscoverCard'
                                                  );
					    echo $form->input('Company.cardtype', array('options' =>$option,'label'=>false,'class'=>'validate[required]','empty'=>'please select'));
                                    ?>
				      </div>
				    </li>
				    <li>
				      <label>CCNo.</label>
				      <div class="input_bg">
					<?php echo $form->input('Company.cardnumber', array('maxLength'=>'16','class'=>'text validate[required,custom[number]]',"div"=>false,"label"=>false));  ?>
				      </div>
				    </li>
				    <li>
				      <label>CCV<span class="astric">*</span> </label>
				      <div class="input_bg">
					<?php echo $form->input('Company.cardcvnumber', array('maxLength'=>'4','class'=>'text validate[required,custom[number]]',"div"=>false,"label"=>false));  ?>
				      </div>
				    </li>
				    <li>
				      <label>Expiry Date<span class="astric">*</span> </label>
				      <div class="input_bg">
                                         <?php
					  $year=array();
					  $i=date('Y');
					while($i<(date('Y')+7))
					{
					    $year[$i]=$i;
					    $i++; 
					}
					    $month=array();
					    $i=1;
					while($i<13)
					{
					    $month[$i]=$i;
					    $i++; 
					} 
					    echo $form->input('Company.month', array('options' =>$month,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0','class'=>'validate[required]','empty'=>'please select'));
					    echo $form->input('Company.year', array('options' =>$year,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0','class'=>'validate[required]','empty'=>'please select'));
					?>
                                      </div>
				      </li>
				    </div>
				    <li>
                                      <label>Subscription Plan<span class="astric">*</span> </label>
                                      <div class="input_bg">
                                        <table cellpadding="0" cellspacing="0" border="1"> 
                                         <tr>
					    <th>No of Site Addresses</th>
                                            <th>Plan Name</th>
                                            <th>Monthly Charges</th>                                            
                                         </tr>
					<?php 
                                         foreach($SubscriptionData as $subscription){ ?>
                                         <tr> 							
                        		 <td>
                                         <?php echo $subscription['SubscriptionDuration']['min_allowed_site_address'].'-'.$subscription['SubscriptionDuration']['max_allowed_site_address']; ?>
                                         </td>
					 <td>
                                         <?php echo $subscription['SubscriptionDuration']['plan_name']; ?>
                                         </td>
                                         <td>
					 <input type="radio" value="<?php echo $subscription['SubscriptionDuration']['id']."-".$subscription['SubscriptionDuration']['monthlycharges']; ?>-Monthly" id="subscription_plan" name="data[User][subscription_duration_monthly]" style="width:0px;" class="validate[required]"><?php echo "$ ".$subscription['SubscriptionDuration']['monthlycharges']; ?>
                                         </td>                        		
                                         </tr>
                                         <?php
					    }
                                         ?>
                                        </table>
                                      </div>
                                    </li>
				    <li>
                                      <label> &nbsp;</label>
                                      <div class="input_bg">
                                         <div class="btn1"><span class="lt"><input name="Submit" type="submit" value="Submit" class=""/></span></div>
                                         <div class="btn2"><span class="lt"><input name="reset" type="reset" value="Reset" /></span></div>
                                      </div>
                                    </li>
				    
				  </ul>
                                </section>
                         <?php echo $form->end(); ?>
                           </section>
                    </section>
                <section class="cont_btm"></section>
             </section>
             <!-- Content right -->
	    
          </section>