<?php
    //echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('password_meter/jquery.pstrength-min.1.2');
    echo $this->Html->script('validation/only_num');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.password').pstrength();
    jQuery("#signup").validationEngine();
    $('#UserPhone1').numeric();
    $('#UserPhone2').numeric();
    $('#UserPhone3').numeric();
    $('#UserZip').numeric();
    
    WireAutoTab('UserPhone1','UserPhone2',3);
    WireAutoTab('UserPhone2','UserPhone3',3);
});
   
    function showbox(id)
    { 
	var checkid= "service_"+id;
    if(document.getElementById(checkid).checked){
	$("#others").fadeIn(500);
    }else{
	$("#others").fadeOut(500);  
    }
}
    function getStates(id,value,stateid){
	
    var optArr = document.getElementById(id).value;    
	jQuery('#'+stateid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByCountry/" + value,
                success : function(data){
		    states = data;
		    var selectedOption = '';
		    var select = $('#'+stateid);
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    $('option', select).remove();
		     
		    $.each(states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}

function chkuser()
{
    // getting the value that user typed
    var checkString    = $("#UserEmail").val();
    // forming the queryString
    var data  = 'user='+ checkString;
    var flag=0;    
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
if (reg.test(checkString))
  flag=1; 
 else
 flag=0; 
    // if checkString is not empty
    if(checkString && flag==1) {
        // ajax call
        $.ajax({
            type: "POST",
            url: "/users/chkuser",
            data: data,
            beforeSend: function(html) { // this happen before actual call
                $("#results").html('');
            },
            success: function(response){ // this happen text we get result
		 $(".error-message").html('');
                if(response === "0")
                {
		   $("#UserEmail").css("border", "2px solid green");
		   $("#UserEmailp").text( "" );
                    $("#results").show();
                    $("#results").append('<img src="/img/frontend/yes-icon.png"><span style="color:green">Available</span>');
                    $("#emailchk").val(1);
                    return true;
                }
                else
                {
                	$("#UserEmail").css("border", "2px solid red");
                	 $("#UserEmailp").text( "" );
		    $("#results").show();
		    $("#results").append('<img src="/img/frontend/delete.png" style="padding-top:3px;"><span style="color:red">Already exist</span>');
		    $("#emailchk").val(0);
		    return false;
                }
            }
        });
    }
}
// end of registration page jquery validation

/*Auto Focus code*/
function WireAutoTab(CurrentElementID, NextElementID, FieldLength) {
    //Get a reference to the two elements in the tab sequence.
    var CurrentElement = $('#' + CurrentElementID);
    var NextElement = $('#' + NextElementID);
 
    CurrentElement.keyup(function(e) {
        //Retrieve which key was pressed.
        //var KeyID = (window.event) ? event.keyCode : e.keyCode;
 
        //If the user has filled the textbox to the given length and
        //the user just pressed a number or letter, then move the
        //cursor to the next element in the tab sequence.   
        /*if (CurrentElement.val().length >= FieldLength
            && ((KeyID >= 48 && KeyID <= 90) ||
            (KeyID >= 96 && KeyID <= 105)))*/
	if (CurrentElement.val().length >= FieldLength)
            NextElement.focus();
    });
}
/*Auto focus code*/
 function valid(){
 	var name = $("#UserFname").val();
 	var address = $("#UserAddress").val();
 	var country = $("#UserCountryId").val();
 	var city = $("#UserCity").val();
 	var zip = $("#UserZip").val();
 	var phone1 = $("#UserPhone1").val();
 	var phone2 = $("#UserPhone2").val();
 	var phone3 = $("#UserPhone3").val();
 	var email = $("#UserEmail").val();
 	var password = $("#password").val();
 	var ValidPassword = $("#ValidPassword").val();
 	var term = $("#ttermss").is(":checked");
 	var squestion = $("#UserQuestionId").val();
 	var sanswer = $("#UserAnswer").val();
 	var token = true;
 	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

 	if(name == '' || name == null){
 		$("#UserFname").css("border", "2px solid red");
 		$("#UserFnamep").text("*First name is required");
 		token = false;
 	 }else { $("#UserFname").css("border", "2px solid green");$("#UserFnamep").text(""); }

 	if(address == '' || address == null){
 		$("#UserAddress").css("border", "2px solid red");
 		$("#UserAddressp").text( "*Address is required" );
 		token = false;
 	}else { $("#UserAddress").css("border", "2px solid green");$("#UserAddressp").text(""); }
 	if(country == '' || country == null){
 		$("#UserCountryId").css("border", "2px solid red");
 		$("#UserCountryIdp").text( "*Country is required" );
 		token = false;
 	}else { $("#UserCountryId").css("border", "2px solid green");$("#UserCountryIdp").text(""); }
 	if(city == '' || city == null){
 		$("#UserCity").css("border", "2px solid red");
 		$("#UserCityp").text( "*City is required" );
 		token = false;
 	}else { $("#UserCity").css("border", "2px solid green");$("#UserCityp").text(""); }
 	if(zip == '' || zip == null){
 		$("#UserZip").css("border", "2px solid red");
 		$("#UserZipp").text( "*Zip is required" );
 		token = false;
 	}
 	else { $("#UserZip").css("border", "2px solid green");$("#UserZipp").text(""); }
 	if((phone1 == '' || phone1 == null) || (phone2 == '' || phone2 == null) || (phone3 == '' || phone3 == null)){
 		if(phone1 == '' || phone1 == null)
 			{ $("#UserPhone1").css("border", "2px solid red"); }
 		if(phone2 == '' || phone2 == null)
 			{ $("#UserPhone2").css("border", "2px solid red"); }
 		if(phone3 == '' || phone3 == null)
 			{ $("#UserPhone3").css("border", "2px solid red"); }
 		$("#UserPhonep").text( "*Phone number is required" );
 		token = false;
 	}
 	else { $("#UserPhone1").css("border", "2px solid green");$("#UserPhone2").css("border", "2px solid green");$("#UserPhone3").css("border", "2px solid green");$("#UserPhonep").text(""); }
 	if(email == '' || email == null){
 		$("#UserEmail").css("border", "2px solid red");
 		$("#UserEmailp").text( "*E-mail is required" );
 		token = false;
 	}
 	else { if(!regex.test(email)){
	$("#UserEmail").css("border", "2px solid red");
 		$("#UserEmailp").text( "*E-mail is not valid" );
 		token = false;
}else { $("#UserEmail").css("border", "2px solid green");$("#UserEmailp").text(""); } }


 	if(password == '' || password == null){
 		$("#password").css("border", "2px solid red");
 		$("#passwordp").text( "*Password is required" );
 		token = false;
}
else { $("#password").css("border", "2px solid green");$("#passwordp").text(""); }
 	if(ValidPassword == '' || ValidPassword == null){
 		$("#ValidPassword").css("border", "2px solid red");
 		$("#ValidPasswordp").text( "*Confirm Password is required" );
 		token = false;
 	}
 	else { $("#ValidPassword").css("border", "2px solid green");$("#ValidPasswordp").text(""); }
 if(password != ValidPassword){  
$("#ValidPassword").css("border", "2px solid red");
 $("#ValidPasswordp").text( "*Confirm Password not match " );
 		token = false;
 }
if(squestion == '' || squestion == null){
 		$("#UserQuestionId").css("border", "2px solid red");
 		$("#UserQuestionIdp").text("*Security Question is required");
 		token = false;
 	 }else { $("#UserQuestionId").css("border", "2px solid green");$("#UserQuestionIdp").text(""); }
if(sanswer == '' || sanswer == null){
 		$("#UserAnswer").css("border", "2px solid red");
 		$("#UserAnswerp").text("*Security Answer is required");
 		token = false;
 	 }else { $("#UserAnswer").css("border", "2px solid green");$("#UserAnswerp").text(""); }

 	if(term == false){
 		//$("#form-validation-field-0").css("border", "2px solid red");
 		$("#tterms").text("*Please accept the term and conditions");
 		
 		token = false;
 	}
 	else { $("#tterms").text(""); }

return token;
 }
</script>
<style>
/*.sign-form .input_bg
{display: inline-block; position: relative;}*/
.password {
width:200px;
border:1px solid #74B2E2;color:#023e6f;
/*float:left; */
margin-bottom: 2px;
}
.pstrength-minchar{
display:none;
font-size : 10px;
float:left;
}
#validpassword_text{
    float:left;
}
#ValidConfirmPassword{margin-bottom:10px;}
#UserPassword_bar{margin-top:-10px;width:65%;border:0px solid white;}
.error-message {
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: left;
    font-weight: bold;
    position: static;
    top: 30px;
    white-space: nowrap;
}
.message{text-align:center;color:#54A41A;font-weight:bold;}
.sign-form li label {
    color: #013A57;
    display: block;
    float: left;
    font-size: 12px;
    line-height: 25px;
    margin-right: 40px;
    text-align: left;
    width: 232px;
}
.note {
    color: #FF0000;
    display: inline-block;
    font-size: 11px;
    line-height: 25px;
    margin-left: 275px;
}
.qq-uploader {
    margin-left: 6px;
    position: relative;
    width: 200px;
}
.pstrength-bar{
	border: 0px solid white !important;
}



</style>

<!-- Inner Starts-->
<section class="inner_full">
   <section class="inner_l">  
    <h2>Signup form for Service Provider</h2>
    <div class='message'><?php echo $this->Session->flash();?></div>
     ReportsOnlinePlus:
<p>
We appreciate your interest in our Inspection Reporting products and promise to respond to your request as quickly as possible.
</p>
<p>
ReportsOnlinePlus has strategic partners who utilize our state of art fire life safety reporting and tracking system that would be more than happy to speak with you about your current requests.
</p>
<p>
Please provide the following information so that we can begin the process of providing you with a Service Provider information that has the qualified personnel to service your request.</p>
<?php echo $form->create('', array('onsubmit' => 'return valid();','type'=>'POST', 'url'=>array('action'=>'signup'),'id'=>'signup')); ?>
	    <ul class="sign-form">
	    <!--<li>
	       <label>Company Name<span class="astric">*</span> </label>
	       <div class="input-cont">
		  <?php echo $form->input('Company.name', array('maxLength'=>100,'class'=>'search_bg',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?>
	       </div>
	     </li>-->
	     <li>
	       <label> Your First Name<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <?php echo $form->input('User.fname', array('maxLength'=>100,'class'=>'search_bg ',"div"=>false,"label"=>false,"tabindex"=>"2",'onblur'=>'fname();'));  ?>
	       </div>
	       
	     </li>
	     <p style="color:red;margin-left: 275px;" id="UserFnamep"></p>
	     <li>
	       <label>Your Last Name </label>
	       <div class="input-cont">
		 <?php echo $form->input('User.lname', array('maxLength'=>100,'class'=>'search_bg',"div"=>false,"label"=>false,"tabindex"=>"3"));  ?>
	       </div>
	     </li>	     
	     <li>
	       <label>Address<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.address', array('maxLength'=>100,'class'=>'search_bg',"div"=>false,"label"=>false,"tabindex"=>"4",'onblur'=>'address();'));  ?>		 
	       </div>
	     </li>
	     <p style="color:red;margin-left: 275px;" id="UserAddressp"></p>
	     <li>
	       <label>Country<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $this->Form->select('User.country_id', $country,'233', array('id'=>'UserCountryId','label' => false,"tabindex"=>"5", 'div' => false, 'class' => 'search_bg ','style'=>'width:213px;','onchange'=>"getStates(this.id,this.value,'UserStateId');"));?>
	       </div>
	     </li>	
	     <p style="color:red;margin-left: 275px;" id="UserCountryIdp"></p>     
	     <li>
		<label>State </label>
	       <div class="input-cont">
		<?php  echo $this->Form->select('User.state_id',$state,'', array('id'=>'UserStateId',"tabindex"=>"6",'label' => false, 'class' => 'search_bg','style'=>'width:213px;','empty' => "-Please select-" ));?>
		</div>
	     </li>

	     <li>
		<label>City<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.city',array('maxLength'=>100,'type'=>'text','class'=>'search_bg ','div'=>false,"label"=>false,"tabindex"=>"7",'onblur'=>'city();')); ?>		
		</div>
	     </li>
	     <p style="color:red;margin-left: 275px;" id="UserCityp"></p> 
	     <li>
	       <label>Zip<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.zip',array('maxLength'=>'50','type'=>'text','class'=>'search_bg ','div'=>false,"label"=>false,"tabindex"=>"8",'onblur'=>'zip();')); ?>		 
	       </div>
	     </li>
	       <p style="color:red;margin-left: 275px;" id="UserZipp"></p> 
	     <li>
	       <label>Phone Number<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.phone1', array('maxLength'=>'3','class'=>'search_bg vsmall ',"div"=>false,"label"=>false,"tabindex"=>"9",'style'=>'width:50px;float:none;',"tabindex"=>"9",'onblur'=>'phone();'));  ?>-
		<?php echo $form->input('User.phone2', array('maxLength'=>'3','class'=>'search_bg vsmall ',"div"=>false,"label"=>false,"tabindex"=>"10",'style'=>'width:50px;float:none;',"tabindex"=>"10",'onblur'=>'phone();'));  ?>-
		<?php echo $form->input('User.phone3', array('maxLength'=>'4','class'=>'search_bg vsmall ',"div"=>false,"label"=>false,"tabindex"=>"11",'style'=>'width:50px;float:none;',"tabindex"=>"11",'onblur'=>'phone();'));  ?>
		<!--<span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:275px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>-->
		 
	       </div>
	     </li>
	     <p style="color:red;margin-left: 275px;" id="UserPhonep"></p> 
	     <li>
	       <label>Email<span class="astric">*</span> </label>
	       <div class="input-cont">
		    <?php echo $form->input('User.email', array("tabindex"=>"12",'maxLength'=>100,'class'=>'search_bg ',"div"=>false,"label"=>false,'onblur'=>'chkuser();'));  ?>		 
	       </div>
	       
	     </li>
	     <div style="margin-left: 275px;" id="results"></div>
	     <p style="color:red;margin-left: 275px;" id="UserEmailp"></p> 
	     <li>
	       <label>Password<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <?php echo $form->input('User.password', array("tabindex"=>"13",'id'=>'password','maxLength'=>50,'class'=>'search_bg password',"type"=>"password","div"=>false,"label"=>false,'onblur'=>'ppassword();'));  ?>		 
	       </div>
	     </li>
	     <p style="color:red;margin-left: 275px;" id="passwordp"></p> 
	     <li>
	       <label>Confirm Password<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.cpassword', array("tabindex"=>"14",'id'=>'ValidPassword','maxLength'=>50,'class'=>'search_bg ',"type"=>"password","div"=>false,"label"=>false,'onblur'=>'cpassword();'));  ?>        		 
	       </div>
	     </li>
	     <p style="color:red;margin-left: 275px;" id="ValidPasswordp"></p> 
	     <li>
		<label>Security Question<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php  echo $this->Form->select('User.question_id',$question,'', array("tabindex"=>"6",'label' => false, 'class' => 'search_bg','style'=>'width:218px;','empty' => "-Please select your question-" ,'onblur'=>'squestion();'));?>
		</div>
	     </li>
	     <p style="color:red;margin-left: 275px;" id="UserQuestionIdp"></p> 
	     <li>
	       <label>Answer<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.answer', array('maxLength'=>100,'class'=>'search_bg',"div"=>false,"label"=>false,"tabindex"=>"4",'onblur'=>'sanswer();'));  ?>		 
	       </div>
	     </li>
	     <p style="color:red;margin-left: 275px;" id="UserAnswerp"></p>
		 <!--
	     <li>
	       <label>Type of Business that you frequently provide inspections <br/>(Choose any that apply or Click "ALL")</label>
	       <div class="input-cont">
		    <?php foreach($businessType as $key=>$BType){ ?>					
			<input name="data[Company][type_business][<?php echo $key;?>]" id="<?php echo $key;?>" type="checkbox" value="<?php echo $key;?>" class="checkbox" style="float:none;"/><?php echo $BType; ?></br>
		    <?php } ?>
		    
	       </div>
	     </li>	     
	     <li>
	       <label>Applicable Services that you specialize  <br/>(Choose any that apply or Click "ALL")</label>
	       <div class="input-cont">
		    <?php foreach($reportType as $key1=>$rType){ ?>		
		    <?php if ($key1 ==9){
			  $func = "showbox($key)" ;
		    }else {
		    $func = '';
		    } ?>			
		    <input name="data[Company][interest_service][<?php echo $key1;?>]" id="service_<?php echo $key1;?>" type="checkbox" value="<?php echo $key1;?>" class="checkbox" style="float:none;" onclick = "<?php echo $func; ?>" /><?php echo $rType; ?></br>
		    <?php } ?>
		    
	       </div>
	     </li>
	     <li style= "display:none" id="others">
		<label>&nbsp;</label>
		<div class="input_bg">
		  <?php echo $form->input('Company.interest_service_other', array('maxLength'=>'150','class'=>'search_bg','div'=>false,'label'=>false));  ?>
		</div>
	      </li>-->
	       <li>
		<label> &nbsp;</label>
		<div class="input_bg">
		  <input name="" type="checkbox" onclick="terms();" value="yes" class="checkbox " id="ttermss"/> Agree with our term and conditions
		</div>
	      </li>
	      <p style="color:red;margin-left: 275px;" id="tterms"></p> 
	     <li>
	     <label>&nbsp;</label>
	     <section class="login_btn">
			 <span>
			<input name="Submit" type="submit" value="Register" class="" /> 		     
		     </span> 
	      </section>
	     </li>	     
	   </ul>
	  <?php echo $form->end(); ?>
			 </section>
     <?php echo $this->element('right_panel');?>
</section>
<section class="clear"></section>
<!-- Inner Ends-->

<script>
function fname()
{if($('#UserFname').val() != ''){
$("#UserFname").css("border", "2px solid green");$("#UserFnamep").text("");
}else{ $("#UserFname").css("border", "2px solid red");
 		$("#UserFnamep").text("*First name is required"); }
}

function address()
{if($('#UserAddress').val() != ''){
$("#UserAddress").css("border", "2px solid green");$("#UserAddressp").text("");
}else{ $("#UserAddress").css("border", "2px solid red");
 		$("#UserAddressp").text( "*Address is required" ); }
}

function city()
{if($('#UserCity').val() != ''){
$("#UserCity").css("border", "2px solid green");$("#UserCityp").text("");
}else{ $("#UserCity").css("border", "2px solid red");
 		$("#UserCityp").text( "*City is required" ); }
}

function zip()
{if($('#UserZip').val() != ''){
$("#UserZip").css("border", "2px solid green");$("#UserZipp").text("");
}else{ $("#UserZip").css("border", "2px solid red");
 		$("#UserZipp").text( "*Zip is required" ); }
}

function phone()
{if(($('#UserPhone1').val() != '') && ($('#UserPhone2').val() != '') && ($('#UserPhone3').val() != '')){
$("#UserPhone1").css("border", "2px solid green");$("#UserPhone2").css("border", "2px solid green");$("#UserPhone3").css("border", "2px solid green");
$("#UserPhonep").text("");
}else{ 
 			{ $("#UserPhone1").css("border", "2px solid red"); }
 		
 			{ $("#UserPhone2").css("border", "2px solid red"); }
 		
 			{ $("#UserPhone3").css("border", "2px solid red"); }
 		$("#UserPhonep").text( "*Phone number is required" ); }
}

function ppassword()
{if($('#password').val() != ''){
$("#password").css("border", "2px solid green");$("#passwordp").text("");
}else{ $("#password").css("border", "2px solid red");
 		$("#passwordp").text( "*Password is required" ); }
}

function cpassword()
{if($('#password').val() == $('#ValidPassword').val()){
$("#ValidPassword").css("border", "2px solid green");$("#ValidPasswordp").text("");
}else{
	$("#ValidPassword").css("border", "2px solid red")
	$("#ValidPasswordp").text("*Confirm password is not same");
}
}

function squestion()
{if($('#UserQuestionId').val() != ''){
$("#UserQuestionId").css("border", "2px solid green");$("#UserQuestionIdp").text("");
}else{ $("#UserQuestionId").css("border", "2px solid red");
 		$("#UserQuestionIdp").text("*Security Question is required"); }
}

function sanswer()
{if($('#UserAnswer').val() != ''){
$("#UserAnswer").css("border", "2px solid green");$("#UserAnswerp").text("");
}else{ $("#UserAnswer").css("border", "2px solid red");
 		$("#UserAnswerp").text("*Security Answer is required"); }
}

function terms()
{if($("#ttermss").is(":checked") != false){
$("#tterms").text("");
}else{ $("#tterms").text("*Please accept the term and conditions"); }
}

</script>