<?php
    //echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('password_meter/jquery.pstrength-min.1.2');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.password').pstrength();
    jQuery("#signupstep2").validationEngine();
});

 function limit(field, chars) {
        if (document.getElementById(field).value.length > chars){
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
    
    function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxupload',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
		    $("#CompanyImage").val(responseJSON.company_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/company_logo_temp/'+responseJSON.company_image+'" height="100px" width="200px">');
	    }
            });           
    }        
	window.onload = createUploader;
    function checkimageupload()
    {
	   if($("#CompanyImage").val() == ""){
	     $("#Errormsg").html('Please upload a picture');
	     return false;
	   }
	   else {
	     $("#Errormsg").html('');
	     return true;
	   }
    }
				    
    function AddRow(divid){
    var counter = jQuery('#optioncount').val();
    counter = parseInt(counter) + 1;
    jQuery('#optioncount').val(counter);
    var OptionHtml = '<div id="option_'+counter+'"><li><label>&nbsp;</label><div class="input_bg">';
    OptionHtml = OptionHtml+'<select name="data[UserCompanyPrincipalRecord]['+counter+'][user_company_principal_id]" style="width:220px;">';
     OptionHtml = OptionHtml+'<option value="">-select-</option>';
    <?php foreach($cmpPrincipal as $key=>$val){ $value = $val['UserCompanyPrincipal']['id']; $detail = $val['UserCompanyPrincipal']['name'] ?>
    OptionHtml = OptionHtml+'<option value="<?php echo $value; ?>"><?php echo $detail; ?></option>';
    <?php } ?>
    OptionHtml = OptionHtml+'</select></div></li>';
    OptionHtml = OptionHtml+'<li><label>&nbsp;</label><div class="input_bg">';
    OptionHtml = OptionHtml+'<input type="text" name="data[UserCompanyPrincipalRecord]['+counter+'][manager_name]" value="Enter name" class="text" id="name_id_" style="width:150px:padding-right:10px;">&nbsp;<input type="text" value="Enter email" name="data[UserCompanyPrincipalRecord]['+counter+'][manager_email]" class="text" id="email_id_" style="width:150px;">';
    OptionHtml = OptionHtml+'</div></li>';
    OptionHtml = OptionHtml+'<li><label>&nbsp;</label><div class="input_bg">';
    OptionHtml = OptionHtml+'<input type="checkbox" value="yes" class="checkbox" name="data[UserCompanyPrincipalRecord]['+counter+'][new_client]">New client Created';
    OptionHtml = OptionHtml+'</div></li>';
    OptionHtml = OptionHtml+'<li><label>&nbsp;</label><div class="input_bg">';
    OptionHtml = OptionHtml+'<input type="checkbox" value="yes" class="checkbox" name="data[UserCompanyPrincipalRecord]['+counter+'][new_scheduled]">New inspection scheduled';
    OptionHtml = OptionHtml+'</div></li>';
    OptionHtml = OptionHtml+'<li><label>&nbsp;</label><div class="input_bg">';
    OptionHtml = OptionHtml+'<input type="checkbox" value="yes" class="checkbox" name="data[UserCompanyPrincipalRecord]['+counter+'][previous_report]">Change in proviously saved report etc.';
    OptionHtml = OptionHtml+'</div></li><a href="javascript:void(0)" onclick="RemoveRow('+counter+')"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></div>';
    jQuery('#DivOption').append(OptionHtml);    
}
function RemoveRow(divid){
    jQuery('#option_'+divid).remove();
}
</script>
<style>
/*.sign-form .input_bg
{display: inline-block; position: relative;}*/
.password {
width:185px;
border:1px solid #74B2E2;color:#023e6f;
float:left;
}
.pstrength-minchar{
display:none;
font-size : 10px;
float:left;
}
#validpassword_text{
    float:left;
}
#ValidConfirmPassword{margin-bottom:10px;}
#UserPassword_bar{margin-top:-10px;width:65%;border:0px solid white;}
.error-message {
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: left;
    font-weight: bold;
    position: static;
    top: 30px;
    white-space: nowrap;
}
.message{text-align:center;color:#54A41A;font-weight:bold;}
.sign-form li label {
    color: #013A57;
    display: block;
    float: left;
    font-size: 12px;
    line-height: 25px;
    margin-right: 40px;
    text-align: left;
    width: 232px;
}
.note {
    color: #FF0000;
    display: inline-block;
    font-size: 11px;
    line-height: 25px;
    margin-left: 275px;
}
.qq-uploader {
    margin-left: 6px;
    position: relative;
    width: 200px;
}
</style>

<!-- Inner Starts-->
<section class="inner_full">
   <section class="inner_l">
    <div class='message'><?php echo $this->Session->flash();?></div>
    <h2><?php echo $tabData['MainTab']['title1'];?></h2>    
     <p><?php echo $tabData['MainTab']['description1'];?></p>
    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'step1'),'id'=>'signupstep2')); ?>
    <?php echo $form->input('Company.company_logo', array('id'=>'CompanyImage','type'=>'hidden',"div"=>false,"label"=>false));  ?>	
    <?php echo $this->Form->hidden('Company.optioncount',array('value'=>'1','id'=>'optioncount')); ?>
	
	    <ul class="sign-form">
	     <li>
	       <label> Website<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <?php echo $form->input('Company.website', array('maxLength'=>100,'class'=>'search_bg',"div"=>false,"label"=>false));  ?>
	       </div>
	     </li>
	     <li>
	       <label>Authorized/Designated Manager of the site<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <?php echo $form->input('Company.site_manager', array('maxLength'=>100,'class'=>'search_bg validate[required]',"div"=>false,"label"=>false));  ?>
	       </div>
	     </li>
	     <li>
	       <label>Direct Phone of Designated Site Manager<span class="astric">*</span> </label>
	       <div class="input-cont">
		  <?php echo $form->input('Company.phone_site_manager', array('maxLength'=>100,'class'=>'search_bg validate[required,custom[number]]',"div"=>false,"label"=>false)); ?>
	       </div>
	     </li>
	     <li>
	       <label>Direct email of Designated Site Manager<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('Company.email_site_manager', array('maxLength'=>100,'class'=>'search_bg validate[required,custom[email]]',"div"=>false,"label"=>false)); ?>		 
	       </div>
	     </li>
	     <li>
	       <label>Company Bio<span class="astric">*</span> </label>
	       <div class="input_cont">
		<?php echo $form->input('Company.company_bio', array('type'=>"textarea",'class'=>'search_bg validate[required]',"div"=>false,"label"=>false,'rows'=>7,'cols'=>43,'onKeyUp' =>"return limit(this.id,1000);"));  ?>
	       </div>
	       <div style="float:right;padding-right:124px;">
		<span id='limitCounter'>0</span>&nbsp;characters entered&nbsp;|&nbsp;<span id='limitCounterLeft'>1000</span>&nbsp;characters remaining
	       </div>
	       <div id="essay_Error" class='error'>&nbsp;</div>
	     </li>	     
	     <li>
		<label>Company Logo<span class="astric">*</span> </label>
		<div class="input_cont">
		    <div id="demo"></div>
		    <ul id="separate-list"></ul>
		</div>
		<div><i>Please provide the below information based on who within your organization will be designated to receive email notifications that track the inspection process. (Please complete all the information even if the same person has multiple responsibilities.</i></div>
		<div id="Errormsg" style="padding-left:265px;color:red;font-weight:bold;"></div>
	     </li>
	     <li>
		<label></label>
		<div class="input_cont">
		    <div id="uploaded_picture"  style="text-align:center;padding-left:90px;"></div>
		</div>
	     </li>
	     <li>
		<label>Company Principals<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->select('UserCompanyPrincipalRecord.1.user_company_principal_id',$companyPrincipal,1,array('id'=>'select_','legend'=>false,'label'=>false,'empty'=>'--Select--',"class"=>"search_bg validate[required]",'style'=>'width:220px;')); ?>
		</div>
	     </li>
	     
	     <li>
		<label>&nbsp;</label>
		<div class="input-cont">
		  <?php echo $form->input('UserCompanyPrincipalRecord.1.manager_name', array('type'=>'text','maxLength'=>100,'class'=>'search_bg','id'=>'name_id','div'=>false,'label'=>false,'style'=>'width:150px;padding-right:10px;','value'=>'Enter name','onblur'=>'if(this.value=="") this.value="Enter name"' ,'onfocus'=>'if(this.value=="Enter name") this.value=""'));  ?>
		  &nbsp;&nbsp;<?php echo $form->input('UserCompanyPrincipalRecord.1.manager_email', array('type'=>'text','class'=>'search_bg','id'=>'email_id','maxLength'=>100,"div"=>false,"label"=>false,"style"=>"width:150px;","value"=>"Enter email",'onblur'=>'if(this.value=="") this.value="Enter email"' ,'onfocus'=>'if(this.value=="Enter email") this.value=""')); ?>
		</div>
	      </li>
	      <li>
		<label> &nbsp;</label>
		<div class="input-cont">
		  <input name="data[UserCompanyPrincipalRecord][1][new_client]" type="checkbox" value="yes" class="checkbox" /> New Client Created
		</div>
	      </li>
	      <li>
		<label> &nbsp;</label>
		<div class="input-cont">
		  <input type="checkbox" value="yes" class="checkbox" name="data[UserCompanyPrincipalRecord][1][new_scheduled]"/> New inspection scheduled
		</div>
	      </li>
	      <li>
		<label> &nbsp;</label>
		<div class="input-cont">
		  <input name="data[UserCompanyPrincipalRecord][1][previous_report]" type="checkbox" value="yes" class="checkbox" /> Change in proviously saved report etc.
		</div>
	      </li>
		  <div id="DivOption">
		      
		  </div>
	      <li>
		<label> &nbsp;</label>
		<div class="input-cont">
		  <a href="#"><strong><?php echo $this->Html->link('Add More','javascript:void(0)',array('escape'=>false,'id'=>'AddOption','onclick'=>'AddRow(1)','class'=>'up_arrow'));?></strong></a>
		</div>
	      </li>	     
	     <li>
	     <label>&nbsp;</label>
	     <section class="login_btn">
			 <span>			 
			<input name="Submit" type="submit" value="Register" class="" onclick ="return checkimageupload();" /> 		     
		     </span> 
	      </section>
	     </li>	     
	   </ul>
	  <?php echo $form->end(); ?>
			 </section>
     <?php echo $this->element('right_panel');?>
</section>
<section class="clear"></section>
<!-- Inner Ends-->