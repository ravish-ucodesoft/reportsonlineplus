<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    //echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('password_meter/jquery.pstrength-min.1.2');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.password').pstrength();
    jQuery("#signup").validationEngine();
});

function showbox(id)
{ 

var checkid= "service_"+id;

    if(document.getElementById(checkid).checked){
  // Insert code here.

$("#others").fadeIn(500);
    }else{
$("#others").fadeOut(500);  
    } 

}
    function getStates(id,value,stateid){
	
    var optArr = document.getElementById(id).value;    
	jQuery('#'+stateid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByCountry/" + value,
                success : function(data){
		    states = data;
		    var selectedOption = '';
		    var select = $('#'+stateid);
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    $('option', select).remove();
		     
		    $.each(states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}
 
function chkuser()
{
    // getting the value that user typed
    var checkString    = $("#UserEmail").val();
    // forming the queryString
    var data  = 'user='+ checkString;
    var flag=0;    
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
if (reg.test(checkString))
  flag=1; 
 else
 flag=0; 
    // if checkString is not empty
    if(checkString && flag==1) {
        // ajax call
        $.ajax({
            type: "POST",
            url: "/users/chkuser",
            data: data,
            beforeSend: function(html) { // this happen before actual call
                $("#results").html('');
            },
            success: function(response){ // this happen after we get result
		 $(".error-message").html('');
                if(response === "0")
                {
		   
                    $("#results").show();
                    $("#results").append('<img src="/img/frontend/yes-icon.png"><span style="color:green">Email ID is available</span>');
                    $("#emailchk").val(1);
                    return true;
                }
                else
                {
		    $("#results").show();
		    $("#results").append('<img src="/img/frontend/delete.png" style="padding-top:3px;"><span style="color:red">This Email is already exist</span>');
		    $("#emailchk").val(0);
		    return false;
                }
            }
        });
    }
}

</script>
<style>


/* -------------
 * Regsiter
 * ------------- */
.sign-form{ width:652px; margin:0px auto; clear:both}
.sign-form li{display:block; padding:8px 0; overflow:hidden;}
.sign-form h4{ color:#e92333; font-style:normal; font-size:11px; font-weight:normal;}
.sign-form li label{float:left; width:200px; display:block; text-align:left; color:#c6c6c6; font-size:14px; padding-top:8px; font-weight:bold }
.sign-form li .input-cont{ float:left; width:322px;}
.input-cont textarea{ font-weight:lighter}
.sign-form li a{ color:#1e3b69; font-size:11px; text-decoration:underline;}
.astric{color:#FF0000; font-weight:normal; font-size:14px;}
.vsmall { width:50px !important}
.small { width:80px !important}
.lsmall { width:130px !important}
.search_bg { border:1px solid #c6c6c6; -moz-border-radius:4px; webkit-border-radius:4px; border-radius:4px;  width:200px; padding:8px; color:#929292; background:#f4f4f4 }
.search_bg:hover{ border:1px solid blue; color:#666}
.search_bg:focus {border:1px solid #000; background:#FFF; color:#000 }
.search_bg:focus { box-shadow:0px 0px 5px 3px #0492e5}
.select_bg { border:1px solid #c6c6c6; -moz-border-radius:4px; -webkit-border-radius:4px; border-radius:4px; padding:8px; color:#929292; background:#f9f9f9; margin-right:5px }
.select_bg:hover{ border:1px solid #9a9a9a; color:#666}
.select_bg:focus {border:1px solid #6c8e3a; background:#FFF; color:#000 }
.input-cont input[type='radio'] { width:auto; vertical-align:middle; margin:6px}
.tooltip { float:left;}

.reg-text { color:#666}
.reg-text h3 { font-weight:lighter; margin:5px 0; padding:0; color:#000; font-family: 'GishaRegular'; font-weight:lighter; text-transform:uppercase}
.reg-text p { margin:0 0 20px 0}
.reg-text a { color:#6c8f3b}
.reg-text ul { margin: 0 0 20px 0}
.reg-text li { margin:5px 0; }

.check_wrap { padding:2px 0}
.already_reg { font-size:16px}
.link { text-decoration:underline; color:#fff !important; font-weight:bold; font-size:16px !important;}
.link:hover { text-decoration:none}
.inner_l h4 {border-bottom: 1px solid #666666; font-weight: lighter;  margin-bottom: 20px;  margin-top: -9px; padding-bottom: 10px;  text-shadow: 1px 1px 0 #000000;}


/*.sign-form .input_bg
{display: inline-block; position: relative;}*/
.password {
width:185px;
border:1px solid #74B2E2;color:#023e6f;
float:left;
}
.pstrength-minchar{
display:none;
font-size : 10px;
float:left;
}
.pstrength-bar{
float: left;
font-size: 1px;
height: 5px;
margin-left: 270px;
width: 20%;
}
#validpassword_text{
    float:left;
}
#ValidConfirmPassword{margin-bottom:10px;}
#UserPassword_bar{margin-top:-10px;width:65%;border:0px solid white;}
.error-message {
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: left;
    font-weight: bold;
    position: static;
    top: 30px;
    white-space: nowrap;
}
.message{text-align:center;color:#54A41A;font-weight:bold;}
.sign-form li label {
    color: #013A57;
    display: block;
    float: left;
    font-size: 12px;
    line-height: 25px;
    margin-right: 40px;
    text-align: left;
    width: 232px;
}
.note {
    color: #FF0000;
    display: inline-block;
    font-size: 11px;
    line-height: 25px;
    margin-left: 275px;
}
.qq-uploader {
    margin-left: 6px;
    position: relative;
    width: 200px;
}
</style>

<!-- Inner Starts-->
<section class="inner_full">
   <section class="inner_l">  
     <h1>Create a new account</h1>
     <h4>Service Provider Recommendation</h4>
     <div class='message'><?php echo $this->Session->flash();?></div>
     ReportsOnlinePlus:
<p>
We appreciate your interest in our Inspection Reporting products and promise to respond to your request as quickly as possible.
</p>
<p>
ReportsOnlinePlus has strategic partners who utilize our state of art fire life safety reporting and tracking system that would be more than happy to speak with you about your current requests.
</p>
<p>
Please provide the following information so that we can begin the process of providing you with a Service Provider information that has the qualified personnel to service your request.</p>
	    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'clientsignup'),'id'=>'signup')); ?>
	    <ul class="sign-form">
	     <li>
	       <label> Your First Name<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <?php echo $form->input('User.fname', array('maxLength'=>100,'class'=>'search_bg validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?>
	       </div>
	     </li>
	     <li>
	       <label>Your Last Name<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <?php echo $form->input('User.lname', array('maxLength'=>100,'class'=>'search_bg',"div"=>false,"label"=>false,"tabindex"=>"2"));  ?><input type="text" class="search_bg"/>
	       </div>
	     </li>
	     <li>
	       <label>Applicable Code Type<span class="astric">*</span> </label>
	       <div class="input-cont">
		  <?php foreach($codetype as $type){
		    $option[$type['CodeType']['id']]= $type['CodeType']['code_type']; ?>
			 <?php	}  ?>
		  <?php echo $form->radio('User.code_type_id', $option,array('div'=>false,'label'=>false,'legend'=>false,'style'=>'width:30px',"tabindex"=>"3",'class'=>'validate[required]'));  ?>		  
	       </div>
	     </li>
	     <li>
	       <label>Site Name<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <?php echo $form->input('User.site_name', array('maxLength'=>100,'class'=>'search_bg validate[required]',"div"=>false,"label"=>false,"tabindex"=>"3"));  ?>		 
	       </div>
	     </li>
	     <li>
	       <label>Site Street Address<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.site_address', array('maxLength'=>100,'class'=>'search_bg',"div"=>false,"label"=>false,"tabindex"=>"4"));  ?>		 
	       </div>
	     </li>
	     <li>
	       <label>Site Country<span class="astric">*</span> </label>
	       <div class="input-cont">
		    <?php echo $this->Form->select('User.site_country_id', $country,'233', array('id'=>'UserCountryId','label' => false, 'div' => false, 'class' => 'search_bg validate[required]','style'=>'width:213px;','onchange'=>"getStates(this.id,this.value,'UserStateId');"));?>		  
	       </div>
	     </li>
	     <li>
		<label>Site City<span class="astric">*</span> </label>
	       <div class="input-cont">
	       <?php echo $form->input('User.site_city',array('maxLength'=>100,'type'=>'text','class'=>'validate[required] search_bg','div'=>false,"label"=>false)); ?> 		
		</div>
	     </li>
	     <li>
	       <label>Site Zip<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.site_zip',array('maxLength'=>100,'type'=>'text','class'=>'search_bg validate[required]','div'=>false,"label"=>false)); ?>		 
	       </div>
	     </li>
	     
	     <li>
	       <label>Daytime Phone Number<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.site_phone1', array('maxLength'=>'3','class'=>'search_bg vsmall validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"6"));  ?>-
					<?php echo $form->input('User.site_phone2', array('maxLength'=>'3','class'=>'search_bg vsmall validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"7"));  ?>-
					<?php echo $form->input('User.site_phone3', array('maxLength'=>'4','class'=>'search_bg vsmall validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"8"));  ?>
				      <span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:275px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
		 
	       </div>
	     </li>
	     
	     <li>
	       <label>Email Address<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.email', array('maxLength'=>100,'class'=>'search_bg validate[required,custom[email]]',"div"=>false,"label"=>false,"tabindex"=>"9",'onblur'=>'chkuser();'));  ?>		 
	       </div>
	     </li>
	     
	     <li>
	       <label>Password<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <?php echo $form->input('User.password', array('id'=>'password','maxLength'=>50,'class'=>'search_bg password validate[required,minSize[6]]',"type"=>"password","div"=>false,"label"=>false,"tabindex"=>"10"));  ?>		 
	       </div>
	     </li>
	     
	     <li>
	       <label>Confirm Password<span class="astric">*</span> </label>
	       <div class="input-cont">
		<?php echo $form->input('User.cpassword', array('id'=>'ValidPassword','maxLength'=>50,'class'=>'search_bg validate[required,equals[password]]',"type"=>"password","div"=>false,"label"=>false,"tabindex"=>"11"));  ?>		 
	       </div>
	     </li>
	     <li>
	       <label>Type of Business <br />(Choose any that apply)<span class="astric">*</span> </label>
	       <div class="input-cont">
		     <?php foreach($businessType as $key=>$BType){ ?>					
			<input name="data[Company][type_business][<?php echo $key;?>]" id="<?php echo $key;?>" type="checkbox" value="<?php echo $key;?>" class="checkbox" style="float:none;"/ tabindex="12"><?php echo $BType; ?></br>
			<?php } ?>
		     <!--<div class="check_wrap"><input name="" type="checkbox" value="" />   Healthcare/Nursing Facility</div>
		     <div class="check_wrap"><input name="" type="checkbox" value="" />   School/Campus Environment</div>
		     <div class="check_wrap"><input name="" type="checkbox" value="" />  Industrial/Commercial Site</div>
		     <div class="check_wrap"><input name="" type="checkbox" value="" />  Utility or Service Company</div>
		     <div class="check_wrap"><input name="" type="checkbox" value="" />   Restaurant/Dining</div>
		     <div class="check_wrap"> <input name="" type="checkbox" value="" />  Lodging</div>
		     <div class="check_wrap"><input name="" type="checkbox" value="" />  Management Company</div>
		     <div class="check_wrap"><input name="" type="checkbox" value="" />   Office/Retail Building</div>
		     <div class="check_wrap"><input name="" type="checkbox" value="" />   Retail Store</div>-->
	       </div>
	     </li>
	     <li>
	       <label>Square Footage<span class="astric">*</span> </label>
	       <div class="input-cont">
		    <?php echo $form->input('Company.sqr_footage', array('maxLength'=>50,'class'=>'search_bg',"div"=>false,"label"=>false,"tabindex"=>"13"));  ?>		 
	       </div>
	     </li>
	     <li>
	       <label>Inspection/PM Service of interest <bR />(Choose any that apply)<span class="astric">*</span> </label>
	       <div class="input-cont">
		    <?php foreach($reportType as $key1=>$rType){ ?>		
		    <?php if ($key1 ==9){
			  $func = "showbox($key)" ;
		    }else {
		    $func = '';
		    } ?>			
		    <input name="data[Company][interest_service][<?php echo $key1;?>]" id="service_<?php echo $key1;?>" type="checkbox" value="<?php echo $key1;?>" class="checkbox" style="float:none;" onclick = "<?php echo $func; ?>" tabindex="14" /><?php echo $rType; ?></br>
		    <?php } ?>
		    <!--<div class="check_wrap"><input name="" type="checkbox" value="" />   Fire Alarm</div>
		    <div class="check_wrap"><input name="" type="checkbox" value="" />   Sprinkler</div>
		    <div class="check_wrap"><input name="" type="checkbox" value="" />   Special Hazard</div>
		    <div class="check_wrap"><input name="" type="checkbox" value="" />   Kitchen System</div>
		    <div class="check_wrap"><input name="" type="checkbox" value="" />   Security & Intrusion Detection</div>
		    <div class="check_wrap"><input name="" type="checkbox" value="" />   Extinguishers</div>
		    <div class="check_wrap"><input name="" type="checkbox" value="" />   Emergency Lighting/Exit Signs</div>
		    <div class="check_wrap"><input name="" type="checkbox" value="" />   Office/Retail Building</div>
		    <div class="check_wrap"><input name="" type="checkbox" value="" />   Other (please list details below):</div>-->
	       </div>
	     </li>
	     <li>
	    <?php
		$options=array('1'=>'Would you like ReportsOnline Plus to send your information to qualified service providers in your area that are qualified to perform the services that you requested?','2'=>'Would you like ReportsOnline Plus to send you information about qualified service providers that can perform the work listed ?');
		$attributes=array('legend'=>false,'separator'=>'<br/><br/><br/>','style'=>'width:10px;border:0px;','label'=>false,'div'=>false,'hiddenField'=>false,'class'=>'validate[required]');
		echo $form->radio('User.direct_client_preference',$options,$attributes);
	    ?>

	     </li>
	     
	     <li>
		<label>About me<span class="astric">*</span> </label>
	       <div class="input-cont">
		 <textarea class="search_bg"></textarea>
		</div>
	     </li>
	     <li>
	     <label>&nbsp;</label>
	     <section class="login_btn">
		    <span>			 
			<input type="submit" value="Register" onclick ="return checkimageupload();"  />
		    </span> 
	      </section>
	     </li>
	     <li>
	       <label>&nbsp;</label>
	       <section class="already_reg">
		   <p> Already Registered, Please <?php echo $html->link('Click Here',array('controller'=>'homes','action'=>'clientlogin'))?></p>
	       </section>
	     </li>
	   </ul>
	  <?php echo $form->end(); ?>
			 </section>
     <section class="inner_r">
	 <!-- Questions? -->
	 <section class="ques_bg">
	      <section class="ques_txt">
		     <h1>Do you have questions  about life safety inspections  or Code?</h1>
		     <a href="#" class="clickhere">Click here</a>
	      </section>
	 </section>
	 
	 <!-- Free Site Survey -->
	 <section class="survey_bg">
	      <section class="survey_txt">
		     <h1>Free Site Survey</h1>
		     <section class="grey_btn"><a href="#" class="clickhere"><span>Click here</span></a></section>
	      </section>
	 </section>
	 
	 <!-- Up to date -->
	 <section class="uptodate_bg">
	      <section class="uptodate_txt">
		     <h1>Are you up to Date?</h1>
		     <p>with your Fire Safety</p>
		     <section class="red_btn"><a href="#" class="clickhere"><span>Click here</span></a></section>
	      </section>
	 </section>
     </section>
</section>
<section class="clear"></section>
<!-- Inner Ends-->


<!-- Begin: Container -->
<section id="container_page_bg">
      <section id="container_wrap">
	      <section id="content">
             <!-- Aside -->
             <aside id="cont_l">
               <section class="sidemenu">
                <section class="sidemenu_top"></section>
                <section class="sidemenu_mid">
                  <h2><?php echo $tabData['MainTab']['title2'];?></h2>
                  <p><?php echo $tabData['MainTab']['description2'];?></p>
                </section>
                <section class="sidemenu_btm"></section>
               </section>
             </aside>
             <!-- Aside -->
             <!-- Content right -->
             <section class="cont_wrap">
                <section class="cont_top"></section>
                    <section class="cont_mid">
	                       <h2>Signup form for Client</h2>
			      <div class='message'><?php echo $this->Session->flash();?></div>
                           <section class="form">
                             <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'clientsignup'),'id'=>'signup')); ?>
			     
                                <section class="level_wrap">
                                <h1 style="margin-bottom:0px;">Service Provider Recommendation</h1>
				<h5 style="color:red">ReportsOnlinePlus:</h5>
				<p>We appreciate your interest in our Inspection Reporting products and promise to respond to your request as quickly as possible.</p>
                                <p>ReportsOnlinePlus has strategic partners who utilize our state of art fire life safety reporting and tracking system that would be more than happy to speak with you about your current requests.</p>
				<p>Please provide the following information so that we can begin the process of providing you with a Service Provider information that has the qualified personnel to service your request.</p>
				    <ul class="sign-form">
				    <li>
				      <label>Your First Name<span class="astric">*</span> </label>
				      <div class="input_bg">
					<?php echo $form->input('User.fname', array('maxLength'=>100,'class'=>'text validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?>
				      </div>
				    </li>
				    <li>
				      <label>Your Last Name </label>
				      <div class="input_bg">
					<?php echo $form->input('User.lname', array('maxLength'=>100,'class'=>'text',"div"=>false,"label"=>false,"tabindex"=>"2"));  ?>
				      </div>
				    </li>
				    <li>
				      <label>Applicable Code Type<span class="astric">*</span></label>
				      <div class="">
					 <?php foreach($codetype as $type){
					             $option[$type['CodeType']['id']]= $type['CodeType']['code_type']; ?>
						          <?php	}  ?>
					           <?php echo $form->radio('User.code_type_id', $option,array('div'=>false,'label'=>false,'legend'=>false,'style'=>'width:30px',"tabindex"=>"3",'class'=>'validate[required]'));  ?>
				      </div>
				    </li>
				    <li>
				      <label>Site Name<span class="astric">*</span></label>
				      <div class="input_bg">
					<?php echo $form->input('User.site_name', array('maxLength'=>100,'class'=>'text validate[required]',"div"=>false,"label"=>false,"tabindex"=>"3"));  ?>
				      </div>
				    </li>
				    <li>
				      <label>Site Street Address</label>
				      <div class="input_bg">
					<?php echo $form->input('User.site_address', array('maxLength'=>100,'class'=>'text',"div"=>false,"label"=>false,"tabindex"=>"4"));  ?>
				      </div>
				    </li>
				    <li>
				      <label>Site Country<span class="astric">*</span></label>
				      <div class="input_bg">
					<?php echo $this->Form->select('User.site_country_id', $country,'233', array('id'=>'UserCountryId','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:213px;','onchange'=>"getStates(this.id,this.value,'UserStateId');"));?>
				      </div>
				    </li>
				     <li>                
				    <li>
					<label>Site State<span class="astric">*</span></label>
					<div class="input_bg">
					<?php  echo $this->Form->select('User.site_state_id',$state,'', array('id'=>'UserStateId','label' => false, 'class' => 'select_bg','style'=>'width:213px;','empty' => "-Please select-" ));?>
					</div>							
				    </li>
				    <li>
					<label>Site City<span class="astric">*</span></label>
					<div class="input_bg">
					   <?php echo $form->input('User.site_city',array('maxLength'=>100,'type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?> 
					</div>			
				    </li>
				    <li>
					<label>Site Zip<span class="astric">*</span></label>
					<div class="input_bg">
					   <?php echo $form->input('User.site_zip',array('maxLength'=>100,'type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?>
					</div>			
				    </li>
				     <li>
				      <label>Daytime Phone Number:<span class="astric">*</span></label>
				      <div class="input_bg">
					<?php echo $form->input('User.site_phone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"6"));  ?>-
					<?php echo $form->input('User.site_phone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"7"));  ?>-
					<?php echo $form->input('User.site_phone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"8"));  ?>
				      <span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:275px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
					  </div>
				    </li>
				    
				    <li>
				      <label>Email Address<span class="astric">*</span> </label>
				      <div class="input_bg">
					<?php echo $form->input('User.email', array('maxLength'=>100,'class'=>'text validate[required,custom[email]]',"div"=>false,"label"=>false,"tabindex"=>"9",'onblur'=>'chkuser();'));  ?>
				      </div>
					   <div id="results"></div>
				    </li>
				    <li>
				      <label>Password<span class="astric">*</span> </label>
				      <div class="input_bg">
					<?php echo $form->input('User.password', array('id'=>'password','maxLength'=>50,'class'=>'text password validate[required,minSize[6]]',"type"=>"password","div"=>false,"label"=>false,"tabindex"=>"10"));  ?>
				      </div>
				      <div class="note">Password Must Be At Least 5 
					Characters</div>  
				      </li>
				    <li>
				      <label>Confirm Password<span class="astric">*</span> </label>
				      <div class="input_bg">
					<?php echo $form->input('User.cpassword', array('id'=>'ValidPassword','maxLength'=>50,'class'=>'text validate[required,equals[password]]',"type"=>"password","div"=>false,"label"=>false,"tabindex"=>"11"));  ?>
				      </div>
				    </li>
				    
				     <li>
				      <label>Type of Business<span class="astric">*</span> <br/>(Choose any that apply)</label>
				      <div class="hang-me">
					<?php foreach($businessType as $key=>$BType){ ?>					
					<input name="data[Company][type_business][<?php echo $key;?>]" id="<?php echo $key;?>" type="checkbox" value="<?php echo $key;?>" class="checkbox" style="float:none;"/ tabindex="12"><?php echo $BType; ?></br>
					<?php } ?>
				      </div>
				    </li>
					 <li>
				      <label>Square Footage:</label>
				      <div class="input_bg">
					<?php echo $form->input('Company.sqr_footage', array('maxLength'=>50,'class'=>'text',"div"=>false,"label"=>false,"tabindex"=>"13"));  ?>
				      </div>
				    </li>
					 
					 <li>
				      <label>Inspection/PM Service of interest<span class="astric">*</span> <br/>(Choose any that apply)</label>
				      <div class="hang-me">
					<?php foreach($reportType as $key1=>$rType){ ?>		
					<?php if ($key1 ==9){
					      $func = "showbox($key)" ;
					}else {
					$func = '';
					} ?>			
					<input name="data[Company][interest_service][<?php echo $key1;?>]" id="service_<?php echo $key1;?>" type="checkbox" value="<?php echo $key1;?>" class="checkbox" style="float:none;" onclick = "<?php echo $func; ?>" tabindex="14" /><?php echo $rType; ?></br>
					<?php } ?>
				      </div>
				    </li>
				    <li style= "display:none" id="others">
				      <label>&nbsp;</label>
				      <div class="input_bg">
					<?php echo $form->input('Company.interest_service_other', array('maxLength'=>'150','class'=>'text','div'=>false,'label'=>false));  ?>
				      </div>
				    </li>
				  <li>
					
					<li>
				      
				      <div class="input_bg">
						    <?php
							$options=array('1'=>'Would you like ReportsOnline Plus to send your information to qualified service providers in your area that are qualified to perform the services that you requested?','2'=>'Would you like ReportsOnline Plus to send you information about qualified service providers that can perform the work listed ?');
							$attributes=array('legend'=>false,'separator'=>'<br/><br/><br/>','style'=>'width:10px;border:0px;','label'=>false,'div'=>false,'hiddenField'=>false,'class'=>'validate[required]');
							echo $form->radio('User.direct_client_preference',$options,$attributes);
							?>

					<?php //echo $form->input('User.direct_client_preference', array('type'=>'radio','class'=>'text',"div"=>false,"label"=>false));  ?>
				      </div>
				    </li>
					
                                      <label> &nbsp;</label>
                                      <div class="input_bg">
                                        
                                      </div>
                                    </li>
                                    <li>
                                      <label> &nbsp;</label>
                                      <div class="input_bg">
                                         <div class="btn1"><span class="lt"><input name="Submit" type="submit" value="Submit" class="" onclick ="return checkimageupload();" /></span></div>
                                         <div class="btn2"><span class="lt"><input name="reset" type="reset" value="Reset" /></span></div>
                                      </div>
                                    </li>
				    <li>
                                      <label> &nbsp;</label>
                                      <div class="input_bg">
                                         Already Registered, Please <?php echo $html->link('Click Here',array('controller'=>'homes','action'=>'clientlogin'))?>
                                      </div>
                                    </li>
                                  </ul>
                                </section>       
                         <?php echo $form->end(); ?>
                           </section>
                    </section>
                <section class="cont_btm"></section>
             </section>
             <!-- Content right -->
          </section>