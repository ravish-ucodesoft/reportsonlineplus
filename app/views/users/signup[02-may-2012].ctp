<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script('password_meter/jquery.pstrength-min.1.2');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.password').pstrength();
    jQuery("#UserSignupForm").validationEngine();
});
function getStates(id,value){
    var optArr = $("#"+id).val();
	jQuery("#UserStateId").html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
                url  : "/users/getByCountry/" + value,
                success : function(opt){
			jQuery('#UserStateId').html(opt);
                }
        });
}
</script>
<style>
.password {
width:185px;
border:1px solid #74B2E2;color:#023e6f;
float:left;
}
.pstrength-minchar {
    display:none;
font-size : 10px;
float:left;
}
#validpassword_text{
    float:left;
}
#ValidConfirmPassword { margin-bottom:10px;}
#ValidPassword_bar{margin-top:33px;width:65%;border:0px solid white;}
.error-message{
 background-image: url("/img/negative.gif");
    color: #B86464;
	background-position: left bottom;
    background-repeat: no-repeat;
    display: block;
    float: left;
    font-weight: bold;
    padding: 10px 0 0 19px;
    white-space: nowrap;
    position:absolute;
    
}
</style>
            <!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	
                    <!-- Inner Left Starts -->
                   
                    <!-- Inner Left Ends -->
                    
                    <!-- Inner Right Starts -->
                    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'signup'))); ?>
                    <div class="inner-right" style="width:600px">
                    	<h2 style="line-height:4">User Registration Form</h2>
                        <div class="content inner-form" style="width:600px">
                        	<h4 class="form-head">Your Details</h4>
                            <div class="form" style="width:600px">
                            	<label>First Name :</label>
                                <div class="input-box">
                                <span><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'text validate[required]',"div"=>false,"label"=>false));  ?></span>
                                </div><br />
                                <label>Last Name :</label>
                                <div class="input-box">
                                	<span><?php echo $form->input('User.lname', array('maxLength'=>'50','class'=>'text',"div"=>false,"label"=>false));  ?></span>
                                	
                                </div><br />
                               
                                <label>Email :</label>
                                <div class="input-box">
                                	<span><?php echo $form->input('User.email', array('maxLength'=>'50','class'=>'text validate[required,custom[email]]',"div"=>false,"label"=>false));  ?></span>
                                	
                                </div><br />
                                 <label>Password:</label>
                                <div class="input-box">
                                	<span><?php echo $form->input('User.password', array('maxLength'=>'50','class'=>'text password validate[required,minSize[6]]',"type"=>"password","div"=>false,"label"=>false));  ?></span>
                                	
                                </div><br />
				 <label>Confirm Password:</label>
                                <div class="input-box">
                                	<span><?php echo $form->input('User.cpassword', array('id'=>'ValidPassword','maxLength'=>'50','class'=>'text validate[required,equals[UserPassword]]',"type"=>"password","div"=>false,"label"=>false));  ?></span>
                                	
                                </div><br />
                            </div>
                            <h4 class="form-head" >Your Company Details</h4>
                            <div class="form" style="width:600px">
                            	<label>Company Name :</label>
                                <div class="input-box">
                                	<span><?php echo $form->input('Company.name', array('maxLength'=>'50','class'=>'text validate[required]',"div"=>false,"label"=>false));  ?></span>
                                	
                                </div><br />
                                <label>Address :</label>
                                <div class="input-box">
                                	<span><?php echo $form->input('Company.address', array('maxLength'=>'50','class'=>'text validate[required]',"div"=>false,"label"=>false));  ?></span>
                                
                                </div><br />
				<label>Country :</label>
				<?php echo $form->input('Company.country_id',array('options' =>$country,'id'=>'UserCountryId','label'=>false,"style"=>"width:300px;",'empty'=>'Please Select','onchange'=>"getStates(this.id,this.value);")); ?>
                                <br />
				<label>State :</label>
                               <!-- <div class="input-box">
				<span><?php //echo $form->text('Company.state', array('maxLength'=>'50','class'=>'text'));  ?></span>-->
				<?php $state = ""; ?>
				 <?php  echo $this->Form->input('Company.state_id',array('options' =>$state,'id'=>'UserStateId','label' => false,'style'=>'width:300px;','empty'=>'Please Select'));?>
                                <br />
                                <label>City :</label>
                                <div class="input-box">
                                <span><?php echo $form->input('Company.city', array('maxLength'=>'50','class'=>'text',"div"=>false,"label"=>false));  ?></span>	
                                </div><br/>
                                <label>Zip :</label>
                                <div class="input-box">
                                <span><?php echo $form->input('Company.zip', array('maxLength'=>'6','class'=>'text',"div"=>false,"label"=>false));  ?></span>
                                	
                                </div><br/>
                                <label>Phone :</label>
                                <div class="input-box">
                                	<span><?php echo $form->input('Company.phone', array('maxLength'=>'15','class'=>'text',"div"=>false,"label"=>false));  ?></span>
                                	
                                </div><br />
                                </div><br>
                                <h4 class="form-head">Your Company Details</h4>
                                <div class="form" style="width:600px">
                                    <label>CC Type :</label>
                                    <?php
                                      $option=array('Visa'=>'Visa',
                                                    'Mastercard'=>'Mastercard',
                                                    'AmericanExpress'=>'AmericanExpress',
                                                    'DiscoverCard'=>'DiscoverCard'
                                                  );
                                      echo $form->input('Company.cardtype', array('options' =>$option,'label'=>false));
                                    ?>
								                    <br />
                                    <label>CC Number :</label>
                                    <div class="input-box">
                                        	<span><?php echo $form->input('Company.cardnumber', array('maxLength'=>'16','class'=>'text validate[required]',"div"=>false,"label"=>false));  ?></span>
                                
                                    </div><br />
                                    <label>CCV :</label>
                                    <div class="input-box">
                                       	<span><?php echo $form->input('Company.cardcvnumber', array('maxLength'=>'4','class'=>'text validate[required]',"div"=>false,"label"=>false));  ?></span>
                                    </div><br />
                                    <label>Subscription Charges :</label>
                                    <div>
                                         <table width="45%" cellspacing="2" cellpadding="2" style="border:1px solid grey;">
                                         <tr style="background-color:#818285;color:#FFFFFF">                          															
                        		  <th width='20%'>Employee</th>
					  <th width='20%'>Plan Name</th>
                			  <th width='40%'>Monthly Charges</th>                          								
                                          <th width='40%'>Yearly Charges</th>                          							  
                          		</tr>
                                         <?php 
                                         foreach($SubscriptionData as $subscription): ?>
                                         <tr class="first"> 							
                        		 <td align="center">
                                         <?php echo $subscription['SubscriptionDuration']['min_allowed_clients'].'-'.$subscription['SubscriptionDuration']['max_allowed_clients']; ?>
                                         </td>
					 <td align="center">
                                         <?php echo $subscription['SubscriptionDuration']['plan_name']; ?>
                                         </td>
                                         <td align="center">
                                         <?php echo $form->radio('User.subscription_duration_monthly',array('value'=>''),array('legend'=>false,'div'=>false,'label'=>false,'class'=>'validate[required]'));?><?php echo "$ ".$subscription['SubscriptionDuration']['monthlycharges']; ?>
                                         </td>
                        		 <td align="center">
                                         <?php echo $form->radio('User.subscription_duration_monthly',array('value'=>''),array('legend'=>false,'div'=>false,'label'=>false,'class'=>'validate[required]'));?><?php echo "$ ".$subscription['SubscriptionDuration']['1_year']; ?>
                                         </td>
                                         </tr>
                                         <?php
					    endforeach;
                                         ?>
                                         </table>
					</div><br/>
                                    
					<label>Expiry Year :</label>
					<?php
					  $year=array();
					  $i=date('Y');
					while($i<(date('Y')+7))
					{
					    $year[$i]=$i;
					    $i++; 
					}
					    $month=array();
					    $i=1;
					while($i<13)
					{
					    $month[$i]=$i;
					    $i++; 
					} 
					    echo $form->input('Company.month', array('options' =>$month,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0'));
					    echo $form->input('Company.year', array('options' =>$year,'label'=>false,'div'=>false,'style'=>'width:142px; margin:0 15px 0 0'));
					?>
                                 
                            </div>
                            <div class="btn-sec">
                            <?php
					      echo $form->submit('signup-btn.gif');
                            ?>
                           </div>
                        </div>
                    </div>
                    	<?php echo $form->end(); ?>
                    	<?php  echo $this->element('right_section_front'); ?>
                    <!-- Inner Right Ends -->                              
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends -->