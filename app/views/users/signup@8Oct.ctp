<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('password_meter/jquery.pstrength-min.1.2');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.password').pstrength();
    jQuery("#signup").validationEngine();
});
   
    function showbox(id)
    { 
	var checkid= "service_"+id;
    if(document.getElementById(checkid).checked){
	$("#others").fadeIn(500);
    }else{
	$("#others").fadeOut(500);  
    }
}
    function getStates(id,value,stateid){
	
    var optArr = document.getElementById(id).value;    
	jQuery('#'+stateid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/users/getByCountry/" + value,
                success : function(data){
		    states = data;
		    var selectedOption = '';
		    var select = $('#'+stateid);
		    if(select.prop) {
		      var options = select.prop('options');
		    }
		    else {
		      var options = select.attr('options');
		    }
		    $('option', select).remove();
		     
		    $.each(states, function(val, text) {
			options[options.length] = new Option(text, val);
		    });
		    select.val(selectedOption);
			//jQuery('#UserStateId').html(opt);
                }
        });
}

function chkuser()
{
    // getting the value that user typed
    var checkString    = $("#UserEmail").val();
    // forming the queryString
    var data  = 'user='+ checkString;
    var flag=0;    
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
if (reg.test(checkString))
  flag=1; 
 else
 flag=0; 
    // if checkString is not empty
    if(checkString && flag==1) {
        // ajax call
        $.ajax({
            type: "POST",
            url: "/users/chkuser",
            data: data,
            beforeSend: function(html) { // this happen before actual call
                $("#results").html('');
            },
            success: function(response){ // this happen after we get result
		 $(".error-message").html('');
                if(response === "0")
                {
		   
                    $("#results").show();
                    $("#results").append('<img src="/img/frontend/yes-icon.png"><span style="color:green">Email ID is available</span>');
                    $("#emailchk").val(1);
                    return true;
                }
                else
                {
		    $("#results").show();
		    $("#results").append('<img src="/img/frontend/delete.png" style="padding-top:3px;"><span style="color:red">This Email is already exist</span>');
		    $("#emailchk").val(0);
		    return false;
                }
            }
        });
    }
}
// end of registration page jquery validation
 
</script>
<style>
/*.sign-form .input_bg
{display: inline-block; position: relative;}*/
.password {
width:185px;
border:1px solid #74B2E2;color:#023e6f;
float:left;
}
.pstrength-minchar{
display:none;
font-size : 10px;
float:left;
}
#validpassword_text{
    float:left;
}
#ValidConfirmPassword{margin-bottom:10px;}
#UserPassword_bar{margin-top:-10px;width:65%;border:0px solid white;}
.error-message {
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: left;
    font-weight: bold;
    position: static;
    top: 30px;
    white-space: nowrap;
}
.message{text-align:center;color:#54A41A;font-weight:bold;}
.sign-form li label {
    color: #013A57;
    display: block;
    float: left;
    font-size: 12px;
    line-height: 25px;
    margin-right: 40px;
    text-align: left;
    width: 232px;
}
.note {
    color: #FF0000;
    display: inline-block;
    font-size: 11px;
    line-height: 25px;
    margin-left: 275px;
}
.qq-uploader {
    margin-left: 6px;
    position: relative;
    width: 200px;
}
</style>
<!-- Begin: Container -->
<section id="container_page_bg">
      <section id="container_wrap">
	      <section id="content">
             <!-- Aside -->
             <aside id="cont_l">
               <section class="sidemenu">
                <section class="sidemenu_top"></section>
                <section class="sidemenu_mid">
                  <h2><?php echo $tabData['MainTab']['title1'];?></h2>
                  <p><?php echo $tabData['MainTab']['description1'];?></p>
			      
                </section>
                <section class="sidemenu_btm"></section>
               </section>
             </aside>
             <!-- Aside -->
             <!-- Content right -->
             <section class="cont_wrap">
                <section class="cont_top"></section>
                    <section class="cont_mid">
	                       <h2>Signup form for Service Provider</h2>
			      <div class='message'><?php echo $this->Session->flash();?></div>
                           <section class="form">
                             <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'signup'),'id'=>'signup')); ?>
                                <section class="level_wrap">
                                <h4>Your Details</h4>
                                    <ul class="sign-form">
				    <li>
				      <label>First Name<span class="astric">*</span> </label>
				      <div class="input_bg">
					<?php echo $form->input('User.fname', array('maxLength'=>100,'class'=>'text validate[required]',"div"=>false,"label"=>false));  ?>
				      </div>
				    </li>
				    <li>
				      <label>Last Name</label>
				      <div class="input_bg">
					<?php echo $form->input('User.lname', array('maxLength'=>100,'class'=>'text',"div"=>false,"label"=>false));  ?>
				      </div>
				    </li>
				    <li>
                                      <label>Company Name<span class="astric">*</span> </label>
                                      <div class="input_bg">
                                        <?php echo $form->input('Company.name', array('maxLength'=>100,'class'=>'validate[required]',"div"=>false,"label"=>false));  ?>
                                      </div>
                                    </li>
				    <li>
                                      <label>Address<span class="astric">*</span> </label>
                                      <div class="input_bg">
                                        <?php echo $form->input('User.address', array('maxLength'=>100,'class'=>'validate[required]',"div"=>false,"label"=>false));  ?>
                                      </div>
                                    </li>
				    <li>
					<label>Country<span class="astric">*</span></label>
					<div class="input_bg">
					<?php echo $this->Form->select('User.country_id', $country,'233', array('id'=>'UserCountryId','label' => false, 'div' => false, 'class' => 'select_bg validate[required]','style'=>'width:213px;','onchange'=>"getStates(this.id,this.value,'UserStateId');"));?>
					</div>
				    </li>
				    <li>
					<label>State<span class="astric">*</span></label>
					<div class="input_bg">
					<?php  echo $this->Form->select('User.state_id',$state,'', array('id'=>'UserStateId','label' => false, 'class' => 'select_bg','style'=>'width:213px;','empty' => "-Please select-" ));?>
					</div>
							
				    </li>
				    <li>
					<label>City<span class="astric">*</span></label>
					<div class="input_bg">
					<?php echo $form->input('User.city',array('maxLength'=>100,'type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?>
					</div>
							
				    </li>
				    <li>
					<label>Zip<span class="astric">*</span></label>
					<div class="input_bg">
					<?php echo $form->input('User.zip',array('maxLength'=>'50','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?>
					</div>
							
				    </li>
				     <li>
				      <label>Phone Number:<span class="astric">*</span></label>
				      <div class="input_bg">
					<?php echo $form->input('User.phone1', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"6"));  ?>-
					<?php echo $form->input('User.phone2', array('maxLength'=>'3','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"7"));  ?>-
					<?php echo $form->input('User.phone3', array('maxLength'=>'4','class'=>'text validate[required,custom[integer]]',"div"=>false,"label"=>false,'style'=>'width:50px;float:none;',"tabindex"=>"8"));  ?>
				      <span style="clear:left; float:left; width:209px; font-style:italic; font-size:10px; padding-top:5px;padding-left:275px; color:#3D94DC;"><b>Eg: 215-740-1008</b></span>
					  </div>
				    </li>
				    <li>
				      <label>Email<span class="astric">*</span> </label>
				      <div class="input_bg">
					<?php echo $form->input('User.email', array('maxLength'=>100,'class'=>'text validate[required,custom[email]]',"div"=>false,"label"=>false,'onblur'=>'chkuser();'));  ?>
				      </div>
					  <div id="results"></div>
				    </li>
				    <li>
				      <label>Type of Business that you frequently provide inspections<span class="astric">*</span> <br/>(Choose any that apply or Click "ALL")</label>
				      <div class="hang-me">
					<?php foreach($businessType as $key=>$BType){ ?>					
					<input name="data[Company][type_business][<?php echo $key;?>]" id="<?php echo $key;?>" type="checkbox" value="<?php echo $key;?>" class="checkbox" style="float:none;"/><?php echo $BType; ?></br>
					<?php } ?>
				      </div>
				    </li>
				     <li>
				      <label>Applicable Services that you specialize 
					<span class="astric">*</span> <br/>(Choose any that apply or Click "ALL")</label>
				      <div class="hang-me">
					<?php foreach($reportType as $key1=>$rType){ ?>		
					<?php if ($key1 ==9){
					      $func = "showbox($key)" ;
					}else {
					$func = '';
					} ?>			
					<input name="data[Company][interest_service][<?php echo $key1;?>]" id="service_<?php echo $key1;?>" type="checkbox" value="<?php echo $key1;?>" class="checkbox" style="float:none;" onclick = "<?php echo $func; ?>" /><?php echo $rType; ?></br>
					<?php } ?>
				      </div>
				    </li>
				     <li style= "display:none" id="others">
				      <label>&nbsp;</label>
				      <div class="input_bg">
					<?php echo $form->input('Company.interest_service_other', array('maxLength'=>'150','class'=>'text','div'=>false,'label'=>false));  ?>
				      </div>
				    </li>
				     <li>
                                      <label> &nbsp;</label>
                                      <div class="input_bg">
                                        <input name="" type="checkbox" value="yes" class="checkbox" /> Agree with our term and conditions
                                      </div>
                                    </li>
				    <li>
                                      <label> &nbsp;</label>
                                      <div class="input_bg">
                                         <div class="btn1"><span class="lt"><input name="Submit" type="submit" value="Submit" class="" onclick ="return checkimageupload();" /></span></div>
                                         <div class="btn2"><span class="lt"><input name="reset" type="reset" value="Reset" /></span></div>
                                      </div>
                                    </li>
				  </ul>
				</section>
                         <?php echo $form->end(); ?>
                           </section>
                    </section>
                <section class="cont_btm"></section>
             </section>
             <!-- Content right -->
          </section>