<?php 
class CalendarHelper extends Helper
{

    function getday($tstamp,$weekday)
     {
      $curentday =date('D',$tstamp);
      
      //for first block Mon
      if($weekday==1){
          if($curentday=='Sun') {
          $tempstamp=$tstamp;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 2*24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 3*24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 4*24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 5*24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 6*24 * 60 * 60;
          }
      }
      //for second block thu
      if($weekday==2){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp -2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 5 * 24 * 60 * 60;
          }
      }
      //for second block Wed
       if($weekday==3){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          }
      }
      
      //for second block Thu
       if($weekday==4){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          }
      }
      //for second block Fri
       if($weekday==5){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 24 * 60 * 60;;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp ;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp -  24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          }
      }
        //for second block Sat
       if($weekday==6){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          }
      }
      
      //for second block Sat
       if($weekday==7){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 6 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp;
          }
      }
       $rdate=date('M-d - D',$tempstamp);
      return $rdate; 
     }
     
     
     function getFullNameday($tstamp,$weekday)
     {
      $curentday =date('D',$tstamp);
      
      //for first block Mon
      if($weekday==1){
          if($curentday=='Sun') {
          $tempstamp=$tstamp;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 2*24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 3*24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 4*24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 5*24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 6*24 * 60 * 60;
          }
      }
      //for second block thu
      if($weekday==2){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp -2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 5 * 24 * 60 * 60;
          }
      }
      //for second block Wed
       if($weekday==3){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          }
      }
      
      //for second block Thu
       if($weekday==4){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          }
      }
      //for second block Fri
       if($weekday==5){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 24 * 60 * 60;;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp ;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp -  24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          }
      }
        //for second block Sat
       if($weekday==6){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          }
      }
      
      //for second block Sat
       if($weekday==7){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 6 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp;
          }
      }
       $rdate=date('M-d,Y  l',$tempstamp);
      return $rdate; 
     }
     
     
     
     
     
     
     
     
     
     
     
     function getdateFromDay($tstamp,$weekday)
     {
      $curentday =date('D',$tstamp);
      
      //for first block Mon
      if($weekday==1){
          if($curentday=='Sun') {
          $tempstamp=$tstamp;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 2*24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 3*24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 4*24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 5*24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 6*24 * 60 * 60;
          }
      }
      //for second block thu
      if($weekday==2){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp -2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 5 * 24 * 60 * 60;
          }
      }
      //for second block Wed
       if($weekday==3){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          }
      }
      
      //for second block Thu
       if($weekday==4){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          }
      }
      //for second block Fri
       if($weekday==5){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 24 * 60 * 60;;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp ;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp -  24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          }
      }
        //for second block Sat
       if($weekday==6){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          }
      }
      
      //for second block Sat
       if($weekday==7){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 6 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp;
          }
      }
       $rdate=date('Y-m-d',$tempstamp);
      return $rdate; 
     }


     
     /*************************************************************************
      *function : getdateYMD()      
      *
      *functionality: return date in Y-M-D  
      ************************************************************************/ 
   
   function getWeekday($week_no)
   {
      if($week_no==1){
      $weekday='Sunday';
      }else if($week_no==2){
      $weekday='Monday';
      }else if($week_no==3){
      $weekday='Tuesday';
      }else if($week_no==4){
      $weekday='Wednesday';
      }else if($week_no==5){
      $weekday='Thursday';
      }else if($week_no==6){
      $weekday='Friday';
      }else if($week_no==7){
      $weekday='Saturday';
      }
     return @$weekday; 
   }
   
   
      
   function getdateYMD($tstamp,$weekday)
     {
      $curentday =date('D',$tstamp);
      
      //for first block Mon
      if($weekday==1){
        
          if($curentday=='Sun') {
          $tempstamp=$tstamp;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 2*24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 3*24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 4*24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 5*24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 6*24 * 60 * 60;
          }
      }
      //for second block thu
      if($weekday==2){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp -2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 5 * 24 * 60 * 60;
          }
      }
      //for second block Wed
       if($weekday==3){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          }
      }
      
      //for second block Thu
       if($weekday==4){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          }
      }
      //for second block Fri
       if($weekday==5){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 24 * 60 * 60;;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp ;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp -  24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          }
      }
        //for second block Sat
       if($weekday==6){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          }
      }
      
      //for second block Sat
       if($weekday==7){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 6 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp;
          }
      }
       $rdate=date('Y-m-d',$tempstamp);
      return $rdate; 
     }
                                              
     
     function calmenu($tstamp) {
     
     App::import('Helper', 'Html');
     $this->html=new HtmlHelper();
     $weekdiff=7 * 24 * 60 * 60;
     $tstamp=$this->getweekfirstday($tstamp);
     
     $tspre1=$tstamp - $weekdiff;
     $tspre2=$tstamp - 2 * $weekdiff;
     $tspre3=$tstamp - 3 * $weekdiff;
     $tspre4=$tstamp - 4 * $weekdiff;
     $tspre5=$tstamp - 5 * $weekdiff;
     $tspre6=$tstamp - 6 * $weekdiff;
     
     
     $tsnext1=$tstamp + $weekdiff;
     $tsnext2=$tstamp + 2 * $weekdiff;
     $tsnext3=$tstamp + 3 * $weekdiff;
     $tsnext4=$tstamp + 4 * $weekdiff;
     $tsnext5=$tstamp + 5 * $weekdiff;
     $tsnext6=$tstamp + 6 * $weekdiff;
     
     $table='<table border="0" width="100%" style="border-collapse: collapse; margin-bottom: 2px;">
              <tbody>
              <tr onmouseout="this.className=\'normal\'" onmouseover="this.className=\'highlight\'" class="navrow">
                   <td onclick="redirecturl(\''.$tspre6.'\')">'.date('M-d',$tstamp - 6*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre5.'\')">'.date('M-d',$tstamp - 5*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre4.'\')">'.date('M-d',$tstamp - 4*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre3.'\')">'.date('M-d',$tstamp - 3*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre2.'\')">'.date('M-d',$tstamp - 2*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre1.'\')">'.date('M-d',$tstamp - $weekdiff).'</td>
                   <td id="calbtn" ><p align="center" id="inputField">Week of '.date('M-d, Y',$tstamp). $this->html->image('/img/calendar.gif').'</p></td>
                   <td onclick="redirecturl(\''.$tsnext1.'\')">'.date('M-d',$tstamp + $weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext2.'\')">'.date('M-d',$tstamp + 2*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext3.'\')">'.date('M-d',$tstamp + 3*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext4.'\')">'.date('M-d',$tstamp + 4*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext5.'\')">'.date('M-d',$tstamp + 5*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext6.'\')">'.date('M-d',$tstamp + 6*$weekdiff).'</td>
              </tr></tbody>
              </table>';   
              return $table;
     }
     
     
     function calmenumonth($tstamp) {
     
     App::import('Helper', 'Html');
     $this->html=new HtmlHelper();
      $weekdiff=30 * 24 * 60 * 60;
     $tstamp=$this->getmonthtimestamp($tstamp);
     $month_name= date('F',$tstamp);
     $month_no=date('m',$tstamp);
    // $month_name=array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
     $pre_month1=$tstamp - 1 * $weekdiff;
     $pre_month2=$tstamp - 2 * $weekdiff;
     $pre_month3=$tstamp - 3 * $weekdiff;
     $pre_month4=$tstamp - 4 * $weekdiff;
     
     $next_month1=$tstamp + 1 * $weekdiff;
     $next_month2=$tstamp + 2 * $weekdiff;
     $next_month3=$tstamp + 3 * $weekdiff;
     $next_month4=$tstamp + 4 * $weekdiff;
     
     $table='<table border="0" width="100%" style="border-collapse: collapse; margin-bottom: 2px;">
              <tbody>
              <tr onmouseout="this.className=\'normal\'" onmouseover="this.className=\'highlight\'" class="navrow">
                   
                   <td  class="normal" onclick="redirecturl(\''.$pre_month4.'\')">'.date('F',$pre_month4).'</td>
                   <td class="normal" onclick="redirecturl(\''.$pre_month3.'\')">'.date('F',$pre_month3).'</td>
                   <td class="normal" onclick="redirecturl(\''.$pre_month2.'\')">'.date('F',$pre_month2).'</td>
                   <td class="normal" onclick="redirecturl(\''.$pre_month1.'\')">'.date('F',$pre_month1).'</td>
                   <td class="normal" id="calbtn" ><p align="center" id="inputField">'.date('F-Y',$tstamp).' </p></td>
                   <td class="normal" onclick="redirecturl(\''.$next_month1.'\')">'.date('F',$next_month1).'</td>
                   <td class="normal" onclick="redirecturl(\''.$next_month2.'\')">'.date('F',$next_month2).'</td>
                   <td class="normal" onclick="redirecturl(\''.$next_month3.'\')">'.date('F',$next_month3).'</td>
                   <td class="normal" onclick="redirecturl(\''.$next_month4.'\')">'.date('F',$next_month4).'</td>
                   
              </tr></tbody>
              </table>';   
              return $table;
     }
     
     /*Created By Manish  Kumar on January 07, 2013*/
     function calmenuday($tstamp) {
     
     App::import('Helper', 'Html');
     $this->html=new HtmlHelper();
      $daydiff=24 * 60 * 60;
     //$tstamp=$this->getdaytimestamp($tstamp);
    
     $pre_day1=$tstamp - 1 * $daydiff;
     $pre_day2=$tstamp - 2 * $daydiff;
     $pre_day3=$tstamp - 3 * $daydiff;     
     
     $next_day1=$tstamp + 1 * $daydiff;
     $next_day2=$tstamp + 2 * $daydiff;
     $next_day3=$tstamp + 3 * $daydiff;     
     
     $table='<table border="0" width="100%" style="border-collapse: collapse; margin-bottom: 2px;">
              <tbody>
              <tr onmouseout="this.className=\'normal\'" onmouseover="this.className=\'highlight\'" class="navrow">
                   
                   
                   <td onclick="redirecturl(\''.$pre_day3.'\')">'.date('M-d, Y',$pre_day3).'</td>
                   <td onclick="redirecturl(\''.$pre_day2.'\')">'.date('M-d, Y',$pre_day2).'</td>
                   <td onclick="redirecturl(\''.$pre_day1.'\')">'.date('M-d, Y',$pre_day1).'</td>
                   <td id="calbtn" ><p align="center" id="inputField">Day of '.date('M-d, Y',$tstamp). $this->html->image('/img/calendar.gif').'</p></td>
                   <td onclick="redirecturl(\''.$next_day1.'\')">'.date('M-d, Y',$next_day1).'</td>
                   <td onclick="redirecturl(\''.$next_day2.'\')">'.date('M-d, Y',$next_day2).'</td>
                   <td onclick="redirecturl(\''.$next_day3.'\')">'.date('M-d, Y',$next_day3).'</td>
                   
                   
              </tr></tbody>
              </table>';   
              return $table;
     }
     
    private function getmonthtimestamp($tstamp)
     {
      $curentday =date('Y-m-d',$tstamp);
     list($y,$m,$d)=explode('-',$curentday);
     
     return mktime(0, 0, 0, $m, 15, $y);
     }   
     
     
     
     private function getweekfirstday($tstamp)
     {
     $curentday =date('D',$tstamp);
     
       if($curentday=='Sun') {
          $tempstamp=$tstamp;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 2*24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 3*24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 4*24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 5*24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 6*24 * 60 * 60;
          }
          return $tempstamp;
          
     }

     function getday_week($tstamp,$weekday)
     {
      $curentday =date('D',$tstamp);

      //for first block Mon
      if($weekday==1){
          if($curentday=='Sun') {
          $tempstamp=$tstamp;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 2*24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 3*24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 4*24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 5*24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 6*24 * 60 * 60;
          }
      }
      //for second block thu
      if($weekday==2){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp -2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 5 * 24 * 60 * 60;
          }
      }
      //for second block Wed
       if($weekday==3){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          }
      }

      //for second block Thu
       if($weekday==4){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          }
      }
      //for second block Fri
       if($weekday==5){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 24 * 60 * 60;;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp ;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp -  24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          }
      }
        //for second block Sat
       if($weekday==6){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          }
      }

      //for second block Sat
       if($weekday==7){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 6 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp;
          }
      }
       $rdate=date('D',$tempstamp);
      return $rdate;
     }

}
?>