<?php class CommonHelper extends Helper {
      var $helpers = array('Html');
    
  function getPositionName($position_id=null)
  {
      $res=mysql_query("SELECT designation FROM `positions` WHERE id ='$position_id'");
          $affected_row=mysql_num_rows($res);
          if($affected_row >0)
          {  
              $row=mysql_fetch_array($res); 
              return $row['designation'];      
          }
          else
          {
              return false;
          }  
  }  
  
  #OK
  
 
 //this is the function to get the agent info of a particular client
  function getAssignedAgentName($client_id = null) // pass receiver_type and sender_id
  {
      if($client_id != "")
      {
        $row = mysql_query("select * from users inner join assign_inspectors on assign_inspectors.agent_id = users.id where assign_inspectors.client_id = '".$client_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
            $res = mysql_fetch_array($row);
            return $res['fname'].' '.$res['lname'];  
        }else {
        return "empty";
      }    
      }
  }
  //this is the function to get the agent info of a particular client
  function getAssignedAgentId($client_id = null) // pass receiver_type and sender_id
  {
      if($client_id != "")
      {
        $row = mysql_query("select * from assign_inspectors where assign_inspectors.client_id = '".$client_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
            $res = mysql_fetch_array($row);
            return $res['agent_id'];  
        }else {
        return "empty";
      }    
      }
  }
  
   function getUserProfilePhoto($imageName=null) {
     if($imageName != '') {
        if(file_exists(WWW_ROOT . USERPROFILEPIC . $imageName)) {
         return USERPROFILEPIC . $imageName;
        } else {
          return USERPROFILEPIC . 'noimage.gif';
        }
      } else {
        return USERPROFILEPIC . 'noimage.gif';
      }
    }
    
     function getClientSchedule($cid = null)
     {
        $row = mysql_query("select * from schedules where schedules.client_id  = '".$cid."'");
        $affected_row=mysql_num_rows($row);
         if($affected_row>0)
         {
            $res = mysql_fetch_array($row);
            return  $res;    
         }
         else{
            return "empty";
         }
     }
//this is the function to get the Inspectors regarding particular service
 function getServicesInspector($report_id = null,$sp_id = null)
 {
      $row = mysql_query("select * from users where sp_id = '".$sp_id."' AND user_type_id = 2 AND FIND_IN_SET('".$report_id."', report_id)");
      $affected_row=mysql_num_rows($row);
      if($affected_row>0)
      {
        while($res = mysql_fetch_array($row))
        {
            $option[$res['id']] = $res['fname']." ".$res['lname'];
        }
        return $option;
      }
      else {
        return 0;
      }
 }

//this is the function to get the Inspectors regarding particular service provider
 function getServicesProviderInspector($sp_id = null)
 {
      $row = mysql_query("select * from users where sp_id = '".$sp_id."' AND user_type_id = 2");
      $affected_row=mysql_num_rows($row);
      if($affected_row>0)
      {
        while($res = mysql_fetch_array($row))
        {
            $option[$res['id']] = $res['fname']." ".$res['lname'];
        }
        return $option;
      }
      else {
        return 0;
      }
 } 
 //this is the function to get the report records of a particular inspector
  function getClientReports($client_id = null)
  {
     $row = mysql_query("select DISTINCT report_id from assign_inspectors where client_id = '".$client_id."'");
     $affected_row=mysql_num_rows($row);
      if($affected_row>0)
      {
        while($res = mysql_fetch_array($row))
        {
            $arr[] = $res['report_id'];
        }
        return $arr;
      }else {
        return "empty";
      }
  }
  
  
  
  function getMyClientReports($inspector_id=null,$client_id = null)
  {
     $row = mysql_query("select DISTINCT report_id from assign_inspectors where  client_id = '".$client_id."'  AND inspector_id = '".$inspector_id."'");
     $affected_row=mysql_num_rows($row);
      if($affected_row>0)
      {
        while($res = mysql_fetch_array($row))
        {
            $arr[] = $res['report_id'];
        }
        return $arr;
      }else {
        return "empty";
      }
  }
  
  
//this is the function to get the report Name of particular client
   function getReportName($report_id = null)
   {
        $row = mysql_query("select name from reports where id = '".$report_id."'");
        $affected_row=mysql_num_rows($row);
       if($affected_row>0)
       {
        while($res = mysql_fetch_array($row))
        {
            $name = $res['name'];
        }
        return $name;
       }else {
        return "empty";
      }  
   }
   
//this is the function to get the permission Name 
   function getPermissionName($perm_id = null)
   {
        $row = mysql_query("select permission from inspector_permissions where id = '".$perm_id."'");
        $affected_row=mysql_num_rows($row);
       if($affected_row>0)
       {
        while($res = mysql_fetch_array($row))
        {
            $name = $res['permission'];
        }
        return $name;
       }else {
        return "NA";
      }  
   }
   
//this is the function to get the  service id of particular client
    function getClientServices($report_id = null,$client_id = null)
    {
         //echo $sql = "select * from assign_inspectors where client_id = '".$client_id."' AND report_id = '".$report_id."'";
     $row = mysql_query("select * from assign_inspectors where client_id = '".$client_id."' AND report_id = '".$report_id."'");
     $affected_row=mysql_num_rows($row);
      if($affected_row>0)
      {
        while($res = mysql_fetch_array($row))
        {
            $arr[$res['id']] = $res['service_id'];
        }
        return $arr;
      }else {
        return "empty";
      }
    }
    
    
    //this is the function to get the  service id of particular client
    function getMyClientServices($inspector_id=null,$report_id = null,$client_id = null)
    {
     
     $row = mysql_query("select * from assign_inspectors where client_id = '".$client_id."' AND inspector_id = '".$inspector_id."' AND report_id = '".$report_id."'");
     $affected_row=mysql_num_rows($row);
      if($affected_row>0)
      {
        while($res = mysql_fetch_array($row))
        {
            $arr[$res['id']] = $res['service_id'];
        }
        return $arr;
      }else {
        return "empty";
      }
    }
    
//this is the function to get the  service Name of particular client    

     #Made by vishal
     function getInspectorNameAssigned($service_id = null)
     {
        $row = mysql_query("select AssignInspector.inspector_id,User.fname,User.lname from assign_inspectors as AssignInspector LEFT JOIN users as User ON User.id=AssignInspector.inspector_id where AssignInspector.service_id = '".$service_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
         while($res = mysql_fetch_array($row))
         {
             $name = $res['fname'].' '.$res['lname'];
         }
         return $name;
        }else {
         return "empty";
       } 
     }
     
     function getScheduledata($service_id = null)
     {
        $row = mysql_query("select Schedule.schedule_date,Schedule.estimated_hrs,AssignInspector.schedule_id from assign_inspectors as AssignInspector LEFT JOIN schedules as Schedule ON Schedule.id=AssignInspector.schedule_id where AssignInspector.service_id = '".$service_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
         while($res = mysql_fetch_array($row))
         {
             $result['schedule_date'] = $res['schedule_date'];
             $result['estimated_hrs'] = $res['estimated_hrs'];
         }
         return $result;
        }else {
         return "empty";
       } 
     }
  # function to get service provider's name    
     function getServiceProviderName($spid = null){
        if(!empty($spid)){
          $res=mysql_query("SELECT fname,lname FROM `users` WHERE id ='$spid'");
          $affected_row=mysql_num_rows($res);
          if($affected_row >0){  
              $row=mysql_fetch_array($res); 
              return $row['fname'].' '.$row['lname'];      
          }
          else
          {
              return false;
          }  
        }
     }
     
     #OK OK OK
     function getServiceName($service_id = null)
     {
        $row = mysql_query("select name from services where id = '".$service_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
         while($res = mysql_fetch_array($row))
         {
             $name = $res['name'];
         }
         return $name;
        }else {
         return "empty";
       } 
     }
     
     
     function getReportLinkName($report_id = null)
     {
        $row = mysql_query("select ctp_name from report_names where report_id = '".$report_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
         while($res = mysql_fetch_array($row))
         {
             $name = $res['ctp_name'];
         }
         return $name;
        }else {
         return "empty";
       } 
     }
     
     function getClientName($client_id=null) // pass receiver_type and sender_id
    { 
      if(!empty($client_id)){
          $res=mysql_query("SELECT fname,lname FROM `users` WHERE id ='$client_id'");
          $affected_row=mysql_num_rows($res);
          if($affected_row >0){  
              $row=mysql_fetch_array($res); 
              return ucwords($row['fname'].' '.$row['lname']);      
          }
          else
          {
              return false;
          }  
      }     
     }
     
     function getClientCompanyName($client_id=null) // pass receiver_type and sender_id
    { 
      if(!empty($client_id)){
          $res=mysql_query("SELECT client_company_name FROM `users` WHERE id ='$client_id'");
          $affected_row=mysql_num_rows($res);
          if($affected_row >0){  
              $row=mysql_fetch_array($res); 
              return ucwords($row['client_company_name']);      
          }
          else
          {
              return false;
          }  
      }     
     }
    
    //Function  getSiteInfo
    //Desc: to get the site address name using service_addr_id
    function getSiteInfo($serviceaddr_id=null) // pass receiver_type and sender_id
    { 
        if(!empty($serviceaddr_id)){
            App::import("Model","SpSiteAddress");
            $this->SpSiteAddress =  new SpSiteAddress();
            $res=$this->SpSiteAddress->findById($serviceaddr_id);            
            if(is_array($res)){
                $cityName = ($res['City']['name']!="")?$res['City']['name']:$res['SpSiteAddress']['site_city'];
                //$returnData = ucwords($res['SpSiteAddress']['site_name']).', '.$res['SpSiteAddress']['site_address'].', '.$cityName.', '.$res['State']['name'].', '.$res['Country']['name'];
                $returnData = ucwords($res['SpSiteAddress']['site_name']).', '.$res['SpSiteAddress']['site_address'].', '.$cityName.', '.$res['State']['name'];
                //return ucwords($res['SpSiteAddress']['site_name']);
                return $returnData;
            }
            else
            {
                return false;
            }  
        }     
    }
     
     
     function getUserColorCode($client_id=null) // pass receiver_type and sender_id
    { 
      if(!empty($client_id)){
          $res=mysql_query("SELECT calendar_color_code FROM `users` WHERE id ='$client_id'");
          $affected_row=mysql_num_rows($res);
          if($affected_row >0){  
              $row=mysql_fetch_array($res); 
              return $row['calendar_color_code'];      
          }
          else
          {
              return false;
          }  
      }     
     }
     
     
     
     
     function getClientNameByServiceCallID($service_call_id=null){
        $row = mysql_query("select client_id from service_calls where id = '".$service_call_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
         while($res = mysql_fetch_array($row))
         {
             $client_id = $res['client_id'];
             $res=mysql_query("SELECT fname,lname FROM `users` WHERE id ='$client_id'");
            $affected_row=mysql_num_rows($res);
            if($affected_row >0){  
                $row=mysql_fetch_array($res); 
                return ucwords($row['fname'].' '.$row['lname']);      
            }
            else
            {
                return false;
            }             
         }
         
        }else {
         return "empty";
       } 
     }
     
     function getCompanyNameByServiceCallID($service_call_id=null){
        $row = mysql_query("select client_id from service_calls where id = '".$service_call_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
         while($res = mysql_fetch_array($row))
         {
             $client_id = $res['client_id'];
             $res=mysql_query("SELECT client_company_name FROM `users` WHERE id ='$client_id'");
            $affected_row=mysql_num_rows($res);
            if($affected_row >0){  
                $row=mysql_fetch_array($res); 
                return ucwords($row['client_company_name']);
            }
            else
            {
                return false;
            }             
         }
         
        }else {
         return "empty";
       } 
     }
     
     function getHelperName($helper_ids=null) // pass receiver_type and sender_id
    { 
      if(!empty($helper_ids)){
        
          $str='';
          foreach($helper_ids as $helper_ids):
          $res=mysql_query("SELECT fname,lname FROM `users` WHERE id ='$helper_ids'");
          $row=mysql_fetch_array($res);          
          $str.=ucwords($row['fname']).' '.$row['lname'] .',';
          endforeach;
          return substr($str,0,-1);
          
      }     
     }
     
     function getServiceTakenBasedonScheduleID($schedule_id=null)
     {
        $row = mysql_query("select * from schedule_services where schedule_id = '".$schedule_id."'");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
            $result1=array();
            for($i=0;$i<count($affected_row);$i++)
            {
                //$res = mysql_fetch_array($row);
                while($res = mysql_fetch_array($row))
                {
                $result['id'] = $res['id'];
                $result['service_id'] = $res['service_id'];
                $result['amount'] = $res['amount'];
                $result['frequency'] = $res['frequency'];                
                }
            $result1[$i]=$result;    
            }
          
         return $result1;
        }else {
         return "empty";
       } 
     }
      
      //This function helps to find the number of a services of a client
      function getNoOfServicesApproved($clientId = null)
      {
        $row = mysql_query("select * from service_calls where client_id = '".$clientId."' AND call_status = 1");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0){
              return $affected_row; 
        }
        else{
              return "empty";
        }
      }
      
      //This function helps to find the number of pending services of a client
      function getNoOfServicesPending($clientId = null)
      {
        $row = mysql_query("select * from service_calls where client_id = '".$clientId."' AND call_status = 0");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0){
              return $affected_row; 
        }
        else{
              return 0;
        }
      }
      
   // this function is used to get the status of the client that shows in client listing   
    function getStatus($clientId = null){
        $row = mysql_query("select * from client_checks where client_id = '".$clientId."'");
        $affected_row = mysql_num_rows($row);
        if($affected_row>0){
            $res = mysql_fetch_array($row);
              return $res['check_id']; 
         }
         else{
              return "empty";
         }
    }
// this function is used to get total no of  attachments of the client for a report
   function getTotalAttachemts($clientId = null){
   $total=0;
        $row = mysql_query("select * from service_calls where client_id = '".$clientId."'");
      
        while($data=mysql_fetch_assoc($row)){     
          $row1 = mysql_query("select count(*) as count from attachments where servicecall_id = '".$data['id']."'");    
         // $row2 = mysql_query("select count(*) as extinguishercount from extinguisher_report_attachments where servicecall_id = '".$data['id']."'");
         // $row3 = mysql_query("select count(*) as kitchenhoodcount from kitchenhood_report_attachments where servicecall_id = '".$data['id']."'");
         // $row4 = mysql_query("select count(*) as specialhazardcount from specialhazard_report_attachments where servicecall_id = '".$data['id']."'");    
         // $row5 = mysql_query("select count(*) as sprinklercount from sprinkler_report_attachments where servicecall_id = '".$data['id']."'");
         // $row6 = mysql_query("select count(*) as firealarmcount from firealarm_report_attachments where servicecall_id = '".$data['id']."'");
          $row1=mysql_fetch_assoc($row1);
         // $row2=mysql_fetch_assoc($row2);
        //  $row3=mysql_fetch_assoc($row3);
         // $row4=mysql_fetch_assoc($row4);
         // $row5=mysql_fetch_assoc($row5);
          //$row6=mysql_fetch_assoc($row6);
          
     // $total = $total+$row1['emergencycount']+$row2['extinguishercount']+$row3['kitchenhoodcount']+$row4['specialhazardcount']+$row5['sprinklercount']+$row6['firealarmcount'];
       $total = $total+$row1['count'];
       }
 return $total;
    } 
    
  function getAttachmentName($serviceCallId=null,$reportId=null){
        /* switch($reportId){
            case "1":
             $sql = mysql_query("select * from firealarm_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "2":
             $sql = mysql_query("select * from sprinkler_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;            
            case "3":
             $sql = mysql_query("select * from kitchenhood_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "4":
             $sql = mysql_query("select * from emergencyexit_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "5":
             $sql = mysql_query("select * from extinguisher_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "6":
             $sql = mysql_query("select * from specialhazard_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
         } */
        $sql = mysql_query("select * from attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
          if(!empty($sql)){
          while($row=mysql_fetch_array($sql))  {
            $data[]= $row;
          }               
          return $data; 
          } 
  } 
  
    function getNotificationName($serviceCallId=null,$reportId=null){
        /*switch($reportId){
            case "1":
             $sql = mysql_query("select * from firealarm_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "2":
             $sql = mysql_query("select * from sprinkler_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;            
            case "3":
             $sql = mysql_query("select * from kitchenhood_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "4":
             $sql = mysql_query("select * from emergencyexit_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "5":
             $sql = mysql_query("select * from extinguisher_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "6":
             $sql = mysql_query("select * from specialhazard_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
         } */
         
          //$res=mysql_query($sql);         
          $sql = mysql_query("select * from notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
          if(!empty($sql)){
          while($row=mysql_fetch_array($sql))  {
            $data[]= $row;
          }               
          return $data; 
          } 
  }
   
   function getReportattachmentcount($reportId = null, $serviceCallId=null){
  
      /*   switch($reportId){
            case "1":
             $row = mysql_query("select count(*) as count from firealarm_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "2":
             $row = mysql_query("select count(*) as count from sprinkler_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;            
            case "3":
             $row = mysql_query("select count(*) as count from kitchenhood_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "4":
             $row = mysql_query("select count(*) as count from emergencyexit_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "5":
             $row = mysql_query("select count(*) as count from extinguisher_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "6":
             $row = mysql_query("select count(*) as count from specialhazard_report_attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
         } */
            $row = mysql_query("select count(*) as count from attachments WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            $row2=mysql_fetch_assoc($row);         
            return $row2['count'];
     
        
       }
     function getReportnotificationcount($reportId = null, $serviceCallId=null){
  
        /* switch($reportId){
            case "1":
             $row = mysql_query("select count(*) as count from firealarm_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "2":
             $row = mysql_query("select count(*) as count from sprinkler_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;            
            case "3":
             $row = mysql_query("select count(*) as count from kitchenhood_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "4":
             $row = mysql_query("select count(*) as count from emergencyexit_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "5":
             $row = mysql_query("select count(*) as count from extinguisher_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break;
            case "6":
             $row = mysql_query("select count(*) as count from specialhazard_report_notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");
            break; 
         } */
            $row = mysql_query("select count(*) as count from notifications WHERE servicecall_id ='$serviceCallId' AND report_id='".$reportId."'");      
            $row2=mysql_fetch_assoc($row);
            return $row2['count'];
       }
       
      function getReportCompleted($reportId = null,$serviceCallId=null,$clientId = null,$spId = null){
            switch($reportId){
                case "1":
                    $sql = "select finish_date from firealarm_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
                case "2":
                    $sql = "select finish_date from sprinkler_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
                case "3":
                    $sql = "select finish_date from kitchenhood_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
                case "4":
                    $sql = "select finish_date from emergencyexit_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
                case "5":
                    $sql = "select finish_date from extinguisher_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;               
                case "6":
                    $sql = "select finish_date from specialhazard_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
            }
                $res = mysql_query($sql);
                if(!empty($res)){
                $affected_row = mysql_num_rows($res);
                if($affected_row>0){
                 $row=mysql_fetch_row($res);
                 return $row[0];
                }  
             }
    }
    
    function getReportStarted($reportId = null,$serviceCallId=null,$clientId = null,$spId = null){
            switch($reportId){
                case "1":
                    $sql = "select finish_date from firealarm_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
                case "2":
                    $sql = "select finish_date from sprinkler_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
                case "3":
                    $sql = "select finish_date from kitchenhood_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
                case "4":
                    $sql = "select finish_date from emergencyexit_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;                
                case "5":
                    $sql = "select finish_date from extinguisher_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;                
                case "6":
                    $sql = "select finish_date from specialhazard_reports WHERE servicecall_id ='$serviceCallId' AND report_id ='".$reportId."' AND client_id ='".$clientId."' AND sp_id ='".$spId."'";
                    break;
        }
        
            $res = mysql_query($sql);
            $affected_row=mysql_num_rows($res);
          if($affected_row>0){
                return "1";
          }else{
                return "0";
        }
    }
    // return no of client found in service provider
    function getno_of_client($id=null){
        
        App::import("Model","User");
        $this->User =  new User();
	
       return $numberofclient = $this->User->find('count', array('conditions' => array('User.user_type_id' =>3,'User.sp_id'=>$id,'User.direct_client'=>'No'),'recursive'=>-1));
        
    }
    
     // return no of inspector found in service provider
    function getno_of_inspector($id=null){
        
        App::import("Model","User");
        $this->User =  new User();
        
        return $numberofinspection = $this->User->find('count', array('conditions' => array('User.user_type_id' =>2,'User.sp_id'=>$id),'recursive'=>-1));
 
    }
  
//this is the function to get the signature Name of particular client
   function getInspectorSignature($report_id = null,$servicecallId = null ,$inspectorId = null)
   {
        $row = mysql_query("select signature_image from signatures where report_id  = '".$report_id."' AND servicecall_id= '".$servicecallId."' AND inspector_id= '".$inspectorId."'");
        $affected_row=mysql_num_rows($row);
       if($affected_row>0)
       {
        while($res = mysql_fetch_array($row))
        {
            $name = $res['signature_image'];
        }
        return $name;
       }else {
        return "empty";
      }  
   }
   //this is the function to get the signature Name of particular client
   function getSignatureCreatedDate($report_id = null,$servicecallId = null ,$inspectorId = null)
   {
        $row = mysql_query("select created from signatures where report_id  = '".$report_id."' AND servicecall_id= '".$servicecallId."' AND inspector_id= '".$inspectorId."'");
        $affected_row=mysql_num_rows($row);
       if($affected_row>0)
       {
        while($res = mysql_fetch_array($row))
        {
            $cDate = $res['created'];
        }
        return $cDate;
       }else {
        return "empty";
      }  
   }
   # this function is used to get name of the country on the basis of country_id
   function getCountryName($countryId=null){
        $row = mysql_query("select name from countries where id = '".$countryId."'");
        $affected_row=mysql_num_rows($row);
       if($affected_row>0)
       {
        while($res = mysql_fetch_array($row))
        {
            $name = $res['name'];
        }
        return $name;
       }else {
        return "empty";
      }   
   }
   # this function is used to get name of the country on the basis of country_id
   function getStateName($stateId=null){
        $row = mysql_query("select name from states where id = '".$stateId."'");
        $affected_row=mysql_num_rows($row);
       if($affected_row>0)
       {
        while($res = mysql_fetch_array($row))
        {
            $name = $res['name'];
        }
        return $name;
       }else {
        return "empty";
      }   
   }
   
   
   function checkPermission($permission_id=null,$inspector_id=null)
   {
        switch($permission_id){
                case "1":
                    $msg="Add an attachment to the report";
                    break;
                case "2":
                    $msg="Add a Notification to the report";
                    break;
                case "3":
                    $msg="Review the report";
                    break;
                case "4":
                    $msg="Fill the report";
                    break;                
                case "5":
                    $msg="Signature on the report";
                    break;                
                case "6":
                    $msg="Fill any deficiency/recommendation to the report";
                    break;
        }
        $row = mysql_query("select fname,lname from users where id = '".$inspector_id."' AND FIND_IN_SET('".$permission_id."', permissions)");
        $affected_row=mysql_num_rows($row);
        if($affected_row>0)
        {
            return 1;
        }
        else
        {
            return 'Sorry ! You donot have the permission to '.$msg.' .';
        }        
   }
   
   function getReportViewLink($report_id=null)
   {
         switch($report_id){
                case "1": //FIREALARM
                    $link="firealarmView";
                    break;
                case "2": //SPRINKLER
                    $link="sprinklerView";
                    break;
                case "3": //KITCHENHOOD
                    $link="kitchenhoodView";
                    break;
                case "4":  //EMERGENCY/EXIT
                    $link="emergencyexitView";
                    break;                
                case "5":  //EXTINGUISHER
                    $link="extinguisherView";
                    break;                
                case "6": //SPECIAL HAZARD
                    $link="specialhazardView";
                    break;
        }
        return $link;
   }
 # to get list of service providers  
   function getServiceProviderList(){
        $row = mysql_query("select * from users where user_type_id = 1 AND deactivate = 1");
      $affected_row=mysql_num_rows($row);
      if($affected_row>0)
      {
        while($res = mysql_fetch_array($row))
        {
            $option[$res['id']] = $res['fname']." ".$res['lname'];
        }
        return $option;
      }
      else {
        return 0;
      }
   }
  # to get count of accepted leads by a sp 
    function acceptLeadCount($id=null){
        $row = mysql_query("select count(*) as count from leads where sp_id = '".$id."' AND status = '1' ");    
        $row1=mysql_fetch_assoc($row);        
        return $row1['count'];
    }  
   # to get count of declined leads by a sp
   function declineLeadCount($id=null){
        $row = mysql_query("select count(*) as count from leads where sp_id = '".$id."' AND status = '2' ");    
        $row1=mysql_fetch_assoc($row);        
        return $row1['count'];
    }
    # to get count of declined leads by a sp
   function pendingLeadCount($id=null){
        $row = mysql_query("select count(*) as count from leads where sp_id = '".$id."' AND status = '0' ");    
        $row1=mysql_fetch_assoc($row);        
        return $row1['count'];
    }
    
    
    
    #Made by vishal on 19-07-2012
    # to get count of completed report of particular client
    function getNoOfCompletedReports($clientID=null)
    {
        $total=0;        
        $row_specialhazard_reports = mysql_query("select count(sh.id) as count from specialhazard_reports sh,service_calls where sh.servicecall_id=service_calls.id and sh.client_id = '".$clientID."' AND sh.finish_date is NOT NULL AND service_calls.call_status=1");
        $row1=mysql_fetch_assoc($row_specialhazard_reports);
        
        $row_emergencyexit_reports = mysql_query("select count(ee.id) as count from emergencyexit_reports ee,service_calls where ee.servicecall_id=service_calls.id and ee.client_id = '".$clientID."' AND ee.finish_date is NOT NULL AND service_calls.call_status=1");
        $row2=mysql_fetch_assoc($row_emergencyexit_reports);
       
        $row_extinguisher_reports = mysql_query("select count(ex.id) as count from extinguisher_reports ex,service_calls where ex.servicecall_id=service_calls.id and ex.client_id = '".$clientID."' AND ex.finish_date is NOT NULL AND service_calls.call_status=1");
        $row3=mysql_fetch_assoc($row_extinguisher_reports);
        
        $row_firealarm_reports = mysql_query("select count(fa.id) as count from firealarm_reports fa,service_calls where fa.servicecall_id=service_calls.id and fa.client_id = '".$clientID."' AND fa.finish_date is NOT NULL AND service_calls.call_status=1");
        $row4=mysql_fetch_assoc($row_firealarm_reports);
       
        $row_kitchenhood_reports = mysql_query("select count(kh.id) as count from kitchenhood_reports kh,service_calls where kh.servicecall_id=service_calls.id and kh.client_id = '".$clientID."' AND kh.finish_date is NOT NULL AND service_calls.call_status=1");
        $row5=mysql_fetch_assoc($row_kitchenhood_reports);
        
        $row_sprinkler_reports = mysql_query("select count(sk.id) as count from sprinkler_reports sk,service_calls where sk.servicecall_id=service_calls.id and sk.client_id = '".$clientID."' AND sk.finish_date is NOT NULL AND service_calls.call_status=1");
        $row6=mysql_fetch_assoc($row_sprinkler_reports);
        return $total=$total+$row1['count']+$row2['count']+$row3['count']+$row4['count']+$row5['count']+$row6['count'];
        //return $total=$total+$row1['count'];
    }
    
    # to get count of completed report of particular client
    function getNoOfPendingReports($clientID=null)
    {
        $row_specialhazard_reports = mysql_query("select count(sh.id) as count from specialhazard_reports sh,service_calls where sh.servicecall_id=service_calls.id and sh.client_id = '".$clientID."' AND sh.finish_date is NULL AND service_calls.call_status=1");
        $row1=mysql_fetch_assoc($row_specialhazard_reports);
        
        $row_emergencyexit_reports = mysql_query("select count(ee.id) as count from emergencyexit_reports ee,service_calls where ee.servicecall_id=service_calls.id and ee.client_id = '".$clientID."' AND ee.finish_date is NULL AND service_calls.call_status=1");
        $row2=mysql_fetch_assoc($row_emergencyexit_reports);
       
        $row_extinguisher_reports = mysql_query("select count(ex.id) as count from extinguisher_reports ex,service_calls where ex.servicecall_id=service_calls.id and ex.client_id = '".$clientID."' AND ex.finish_date is NULL AND service_calls.call_status=1");
        $row3=mysql_fetch_assoc($row_extinguisher_reports);
        
        $row_firealarm_reports = mysql_query("select count(fa.id) as count from firealarm_reports fa,service_calls where fa.servicecall_id=service_calls.id and fa.client_id = '".$clientID."' AND fa.finish_date is NULL AND service_calls.call_status=1");
        $row4=mysql_fetch_assoc($row_firealarm_reports);
       
        $row_kitchenhood_reports = mysql_query("select count(kh.id) as count from kitchenhood_reports kh,service_calls where kh.servicecall_id=service_calls.id and kh.client_id = '".$clientID."' AND kh.finish_date is NULL AND service_calls.call_status=1");
        $row5=mysql_fetch_assoc($row_kitchenhood_reports);
        
        $row_sprinkler_reports = mysql_query("select count(sk.id) as count from sprinkler_reports sk,service_calls where sk.servicecall_id=service_calls.id and sk.client_id = '".$clientID."' AND sk.finish_date is NULL AND service_calls.call_status=1");
        $row6=mysql_fetch_assoc($row_sprinkler_reports);
        return $row1['count']+$row2['count']+$row3['count']+$row4['count']+$row5['count']+$row6['count'];
    }
    
    # to get all inspection which are not being started of particular client
    function getTotalReports($clientID=null)
    {
        $row_service_calls = mysql_query("select id from service_calls where client_id = '".$clientID."' AND call_status=1");
        $affected_row=mysql_num_rows($row_service_calls);
        if($affected_row>0)
        {
          $total=0;  
          while($res = mysql_fetch_array($row_service_calls))
          {
              $row_schedule_reports = mysql_query("select count(sh.id) as count from schedules sh,service_calls where sh.service_call_id=service_calls.id AND service_calls.call_status=1 AND sh.service_call_id = '".$res['id']."'");
              $row5=mysql_fetch_assoc($row_schedule_reports);
              $total=$total+$row5['count'];
          }
          return $total;
        }
        else {
          return 0;
        }
    }
    
    
    
    
    # to get count of completed reports of particular serviceCallID
    function getNoOfCompletedReportsOfserviceCall($serviceCallID=null)
    {
        $total=0;        
        $row_specialhazard_reports = mysql_query("select count(sh.id) as count from specialhazard_reports sh,service_calls where sh.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NOT NULL");
        $row1=mysql_fetch_assoc($row_specialhazard_reports);
        
        $row_emergencyexit_reports = mysql_query("select count(ee.id) as count from emergencyexit_reports ee,service_calls where ee.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NOT NULL");
        $row2=mysql_fetch_assoc($row_emergencyexit_reports);
       
        $row_extinguisher_reports = mysql_query("select count(ex.id) as count from extinguisher_reports ex,service_calls where ex.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NOT NULL");
        $row3=mysql_fetch_assoc($row_extinguisher_reports);
        
        $row_firealarm_reports = mysql_query("select count(fa.id) as count from firealarm_reports fa,service_calls where fa.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NOT NULL");
        $row4=mysql_fetch_assoc($row_firealarm_reports);
       
        $row_kitchenhood_reports = mysql_query("select count(kh.id) as count from kitchenhood_reports kh,service_calls where kh.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NOT NULL");
        $row5=mysql_fetch_assoc($row_kitchenhood_reports);
        
        $row_sprinkler_reports = mysql_query("select count(sp.id) as count from sprinkler_reports sp,service_calls where sp.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NOT NULL");
        $row6=mysql_fetch_assoc($row_sprinkler_reports);
        return $total=$total+$row1['count']+$row2['count']+$row3['count']+$row4['count']+$row5['count']+$row6['count'];
        //return $total=$total+$row1['count'];
    }
    
    # to get count of completed report of particular client
    function getNoOfPendingReportsOfserviceCall($serviceCallID=null)
    {
        $row_specialhazard_reports = mysql_query("select count(sh.id) as count from specialhazard_reports sh,service_calls where sh.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NULL");
        $row1=mysql_fetch_assoc($row_specialhazard_reports);
        
        $row_emergencyexit_reports = mysql_query("select count(ee.id) as count from emergencyexit_reports ee,service_calls where ee.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NULL");
        $row2=mysql_fetch_assoc($row_emergencyexit_reports);
       
        $row_extinguisher_reports = mysql_query("select count(ex.id) as count from extinguisher_reports ex,service_calls where ex.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NULL");
        $row3=mysql_fetch_assoc($row_extinguisher_reports);
        
        $row_firealarm_reports = mysql_query("select count(fa.id) as count from firealarm_reports fa,service_calls where fa.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NULL");
        $row4=mysql_fetch_assoc($row_firealarm_reports);
       
        $row_kitchenhood_reports = mysql_query("select count(kh.id) as count from kitchenhood_reports kh,service_calls where kh.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NULL");
        $row5=mysql_fetch_assoc($row_kitchenhood_reports);
        
        $row_sprinkler_reports = mysql_query("select count(sp.id) as count from sprinkler_reports sp,service_calls where sp.servicecall_id=service_calls.id AND service_calls.call_status=1 AND servicecall_id = '".$serviceCallID."' AND finish_date is NULL");
        $row6=mysql_fetch_assoc($row_sprinkler_reports);
        return $row1['count']+$row2['count']+$row3['count']+$row4['count']+$row5['count']+$row6['count'];
    }
    
    # to get all inspection which are not being started of particular client
    function getTotalReportsOfserviceCall($serviceCallID=null)
    {
        /*$row_service_calls = mysql_query("select id from service_calls where client_id = '".$clientID."'");
        $affected_row=mysql_num_rows($row_service_calls);
        if($affected_row>0)
        {
          $total=0;  
          while($res = mysql_fetch_array($row_service_calls))
          {
              $row_schedule_reports = mysql_query("select count(id) as count from schedules where service_call_id = '".$res['id']."'");
              $row5=mysql_fetch_assoc($row_schedule_reports);
              $total=$total+$row5['count'];
          }
          return $total;
        }
        else {
          return 0;
        }*/
        
        $row_schedule_reports = mysql_query("select count(sh.id) as count from schedules sh,service_calls where sh.service_call_id=service_calls.id AND service_calls.call_status=1 AND service_call_id = '".$serviceCallID."'");
        $row5=mysql_fetch_assoc($row_schedule_reports);
        return $row5['count'];
        
    }
    // return no of devices serviced
      function get_comission_type($code_id = null){
        
        App::import("Model","CodeType");
        $this->CodeType =  new CodeType();
        $codeRecord =  $this->CodeType->find('first', array('conditions' => array('CodeType.id' =>$code_id)));
         return $codeRecord['CodeType']['code_type'];
        }
        
      // return no of devices serviced
      function get_total_device($fa_id=null,$deviceid=null){
        
        App::import("Model","FaLocationDescription");
        $this->FaLocationDescription =  new FaLocationDescription();
       
        
        return $numberofdevice =  $this->FaLocationDescription->find('count', array('conditions' => array('FaLocationDescription.firealarm_report_id' =>$fa_id,'FaLocationDescription.fa_alarm_device_type_id'=>$deviceid),'recursive'=>-1));
        
        }
        // return no of devices passed
      function get_pass_device($fa_id=null,$deviceid=null){
        
        App::import("Model","FaLocationDescription");
        $this->FaLocationDescription =  new FaLocationDescription();
       
        
        return $numberofdevice =  $this->FaLocationDescription->find('count', array('conditions' => array('FaLocationDescription.firealarm_report_id' =>$fa_id,'FaLocationDescription.fa_alarm_device_type_id'=>$deviceid,'FaLocationDescription.functional_test'=>'Pass'),'recursive'=>-1));
        
        }
         // return no of devices deficient
      function get_deficient_device($fa_id=null,$deviceid=null){
        
        App::import("Model","FaLocationDescription");
        $this->FaLocationDescription =  new FaLocationDescription();      
        
        return $numberofdevice =  $this->FaLocationDescription->find('count', array('conditions' => array('FaLocationDescription.firealarm_report_id' =>$fa_id,'FaLocationDescription.fa_alarm_device_type_id'=>$deviceid,'FaLocationDescription.functional_test'=>'Deficient'),'recursive'=>-1));
        
        }
           // return no of devices recoomend
      function get_recomend_device($fa_id=null,$deviceid=null){
        App::import("Model","FaLocationDescription");
        $this->FaLocationDescription =  new FaLocationDescription();
        return $numberofdevice =  $this->FaLocationDescription->find('count', array('conditions' => array('FaLocationDescription.firealarm_report_id' =>$fa_id,'FaLocationDescription.fa_alarm_device_type_id'=>$deviceid,'FaLocationDescription.functional_test'=>'Recommend'),'recursive'=>-1));
        }
        
        function  getPrincipalName($principal_id = null){
            //$this->loadModel('UserCompanyPrincipal');
            //App::import("Model","UserCompanyPrincipal");
            //$this->UserCompanyPrincipal =  new UserCompanyPrincipal();
            $sql = "select name from user_company_principals where id = '".$principal_id."'";
            $res = mysql_query($sql);
            //$principal_name =  $this->find('first',array('conditions'=>array('UserCompanyPrincipal.id'=>$principal_id),'fields'=>'UserCompanyPrincipal.name'));
            $row = mysql_fetch_row($res);
            return  $row[0];
        }
        
        
    // return active siteaddreeses of client
    function active_siteaddress($client_id=null){
        
        App::import("Model","SpSiteAddress");
        $this->SpSiteAddress =  new SpSiteAddress();
        
        return $activesiteaddress = $this->SpSiteAddress->find('count', array('conditions' => array('SpSiteAddress.client_id'=>$client_id,'SpSiteAddress.idle'=>0),'recursive'=>1));
 
    }
    
    // return active siteaddreeses of client
    function idle_siteaddress($client_id=null){
        
        App::import("Model","SpSiteAddress");
        $this->SpSiteAddress =  new SpSiteAddress();
        
        return $idlesiteaddress = $this->SpSiteAddress->find('count', array('conditions' => array('SpSiteAddress.client_id'=>$client_id,'SpSiteAddress.idle'=>1),'recursive'=>1));
 
    }
    
    // return active siteaddreeses of client
    function vacation_effect_previous_servicecall($InspectorId=null,$VacationSdate=null,$VacationEdate=null){
        
        App::import("Model","Schedule");
        $this->Schedule =  new Schedule();
        
        return $resultalreadyaligned=$this->Schedule->Query("SELECT * FROM `schedules` WHERE
                                                     `lead_inspector_id`='".$InspectorId."' AND (`schedule_date_1` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_2` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_3` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_4` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_5` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_6` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_7` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_8` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_9` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											OR `schedule_date_10` BETWEEN '".$VacationSdate."' AND '".$VacationEdate."'
											) 											
											");
 
    }
    
    //this is the function to get remaining days for the report submission
    //Manish Kumar
    //October 31,2012
   function getRemainingDays($id = null)
   {        
        App::import("Model","Schedule");
        $this->Schedule =  new Schedule();
        
        $daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.id'=>$id),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
        $daysRemaining = $daysData[0]['days'];
        if($daysRemaining!=null || $daysRemaining!=""){
            $daysRemaining = (($daysRemaining>=0)?$daysRemaining:'Submission date expired');
            return $daysRemaining;
        }else{
            return "";
        }
   }
   
   //Function to get the blank cert names for a particular report
    //Manish Kumar
    //November 19,2012
   function getAllCerts($spid = null,$reportId = null)
   {        
        App::import("Model","SpBlankCertForm");
        $this->SpBlankCertForm =  new SpBlankCertForm();
        
        $certData = $this->SpBlankCertForm->find('all',array('conditions'=>array('SpBlankCertForm.report_id'=>$reportId,'SpBlankCertForm.sp_id'=>$spid,'SpBlankCertForm.status'=>'Active')));        
        
        return $certData;
        
   }
   
   //Function to get the cert names to be attached accrding to schedule
    //Manish Kumar
    //November 19,2012
   function getAllSchCerts($scheduleId = null)
   {        
        App::import("Model","ReportCert");
        $this->ReportCert =  new ReportCert();
        
        $certData = $this->ReportCert->find('all',array('conditions'=>array('ReportCert.schedule_id'=>$scheduleId,'ReportCert.inspector_fill_status'=>0)));        
        
        return $certData;
        
   }
   //Function to get the number of pending reports for inspector
    //Manish Kumar
    //November 20,2012
   function getAllPendingReports($inspectorId = null)
   {        
        $totalNo = 0;
        
        App::import("Model","FirealarmReport");
        $this->FirealarmReport =  new FirealarmReport();        
        $fireNo = $this->FirealarmReport->find('count',array('conditions'=>array('FirealarmReport.inspector_id'=>$inspectorId,'FirealarmReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","EmergencyexitReport");
        $this->EmergencyexitReport =  new EmergencyexitReport();        
        $emergencyexitNo = $this->EmergencyexitReport->find('count',array('conditions'=>array('EmergencyexitReport.inspector_id'=>$inspectorId,'EmergencyexitReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","ExtinguisherReport");
        $this->ExtinguisherReport =  new ExtinguisherReport();        
        $extinguisherNo = $this->ExtinguisherReport->find('count',array('conditions'=>array('ExtinguisherReport.inspector_id'=>$inspectorId,'ExtinguisherReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","KitchenhoodReport");
        $this->KitchenhoodReport =  new KitchenhoodReport();        
        $kitchenhoodNo = $this->KitchenhoodReport->find('count',array('conditions'=>array('KitchenhoodReport.inspector_id'=>$inspectorId,'KitchenhoodReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","SpecialhazardReport");
        $this->SpecialhazardReport =  new SpecialhazardReport();        
        $specialhazardNo = $this->SpecialhazardReport->find('count',array('conditions'=>array('SpecialhazardReport.inspector_id'=>$inspectorId,'SpecialhazardReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","SprinklerReport");
        $this->SprinklerReport =  new SprinklerReport();        
        $sprinklerNo = $this->SprinklerReport->find('count',array('conditions'=>array('SprinklerReport.inspector_id'=>$inspectorId,'SprinklerReport.finish_date'=>NULL),'recursive'=>-1));
        
        $totalNo = $totalNo+$fireNo+$emergencyexitNo+$extinguisherNo+$kitchenhoodNo+$specialhazardNo+$sprinklerNo;
        return $totalNo;
   }
   
   //Function to get the number of completed reports for inspector
    //Manish Kumar
    //November 20,2012
   function getAllCompleteReports($inspectorId = null){
    
        $totalNo = 0;
        
        App::import("Model","FirealarmReport");
        $this->FirealarmReport =  new FirealarmReport();        
        $fireNo = $this->FirealarmReport->find('count',array('conditions'=>array('FirealarmReport.inspector_id'=>$inspectorId,'FirealarmReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","EmergencyexitReport");
        $this->EmergencyexitReport =  new EmergencyexitReport();        
        $emergencyexitNo = $this->EmergencyexitReport->find('count',array('conditions'=>array('EmergencyexitReport.inspector_id'=>$inspectorId,'EmergencyexitReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","ExtinguisherReport");
        $this->ExtinguisherReport =  new ExtinguisherReport();        
        $extinguisherNo = $this->ExtinguisherReport->find('count',array('conditions'=>array('ExtinguisherReport.inspector_id'=>$inspectorId,'ExtinguisherReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","KitchenhoodReport");
        $this->KitchenhoodReport =  new KitchenhoodReport();        
        $kitchenhoodNo = $this->KitchenhoodReport->find('count',array('conditions'=>array('KitchenhoodReport.inspector_id'=>$inspectorId,'KitchenhoodReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","SpecialhazardReport");
        $this->SpecialhazardReport =  new SpecialhazardReport();        
        $specialhazardNo = $this->SpecialhazardReport->find('count',array('conditions'=>array('SpecialhazardReport.inspector_id'=>$inspectorId,'SpecialhazardReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","SprinklerReport");
        $this->SprinklerReport =  new SprinklerReport();        
        $sprinklerNo = $this->SprinklerReport->find('count',array('conditions'=>array('SprinklerReport.inspector_id'=>$inspectorId,'SprinklerReport.finish_date !='=>NULL),'recursive'=>-1));
        
        $totalNo = $totalNo+$fireNo+$emergencyexitNo+$extinguisherNo+$kitchenhoodNo+$specialhazardNo+$sprinklerNo;
        return $totalNo;
   }
   
   //Function to get the number of reports to be started for inspector
    //Manish Kumar
    //November 21,2012
   function getNotStartedReports($inspectorId = null){    
        App::import("Model","Schedule");
        $this->Schedule =  new Schedule();
        $findSchData = $this->Schedule->find('all',array('conditions'=>array('Schedule.lead_inspector_id'=>$inspectorId,'ServiceCall.call_status'=>1)));
        $totalCount = count($findSchData);
        $totalNo = $totalCount;
        if($totalCount>0){
            foreach($findSchData as $data){
                $reportId = $data['Schedule']['report_id'];
                $serviceId = $data['Schedule']['service_call_id'];
                switch($reportId){
                    case 1:
                        App::import("Model","FirealarmReport");
                        $this->FirealarmReport =  new FirealarmReport();        
                        $fireNo = $this->FirealarmReport->find('count',array('conditions'=>array('FirealarmReport.inspector_id'=>$inspectorId,'FirealarmReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$fireNo;
                        break;
                    case 2:
                        App::import("Model","SprinklerReport");
                        $this->SprinklerReport =  new SprinklerReport();        
                        $sprinklerNo = $this->SprinklerReport->find('count',array('conditions'=>array('SprinklerReport.inspector_id'=>$inspectorId,'SprinklerReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$sprinklerNo;
                        break;
                    case 3:
                        App::import("Model","KitchenhoodReport");
                        $this->KitchenhoodReport =  new KitchenhoodReport();        
                        $kitchenhoodNo = $this->KitchenhoodReport->find('count',array('conditions'=>array('KitchenhoodReport.inspector_id'=>$inspectorId,'KitchenhoodReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$kitchenhoodNo;
                        break;
                    case 4:
                        App::import("Model","EmergencyexitReport");
                        $this->EmergencyexitReport =  new EmergencyexitReport();        
                        $emergencyexitNo = $this->EmergencyexitReport->find('count',array('conditions'=>array('EmergencyexitReport.inspector_id'=>$inspectorId,'EmergencyexitReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$emergencyexitNo;
                        break;
                    case 5:
                        App::import("Model","ExtinguisherReport");
                        $this->ExtinguisherReport =  new ExtinguisherReport();        
                        $extinguisherNo = $this->ExtinguisherReport->find('count',array('conditions'=>array('ExtinguisherReport.inspector_id'=>$inspectorId,'ExtinguisherReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$extinguisherNo;
                        break;
                    case 6:
                        App::import("Model","SpecialhazardReport");
                        $this->SpecialhazardReport =  new SpecialhazardReport();        
                        $specialhazardNo = $this->SpecialhazardReport->find('count',array('conditions'=>array('SpecialhazardReport.inspector_id'=>$inspectorId,'SpecialhazardReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$specialhazardNo;
                        break;
                    default:
                        break;
                }
            }
        }
        return $totalNo;
   }
   
   //Function to get the number of pending reports for clients
    //Manish Kumar
    //November 21,2012
   function getAllPendingReportsClient($clientId = null)
   {        
        $totalNo = 0;
        
        App::import("Model","FirealarmReport");
        $this->FirealarmReport =  new FirealarmReport();        
        $fireNo = $this->FirealarmReport->find('count',array('conditions'=>array('FirealarmReport.client_id'=>$clientId,'FirealarmReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","EmergencyexitReport");
        $this->EmergencyexitReport =  new EmergencyexitReport();        
        $emergencyexitNo = $this->EmergencyexitReport->find('count',array('conditions'=>array('EmergencyexitReport.client_id'=>$clientId,'EmergencyexitReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","ExtinguisherReport");
        $this->ExtinguisherReport =  new ExtinguisherReport();        
        $extinguisherNo = $this->ExtinguisherReport->find('count',array('conditions'=>array('ExtinguisherReport.client_id'=>$clientId,'ExtinguisherReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","KitchenhoodReport");
        $this->KitchenhoodReport =  new KitchenhoodReport();        
        $kitchenhoodNo = $this->KitchenhoodReport->find('count',array('conditions'=>array('KitchenhoodReport.client_id'=>$clientId,'KitchenhoodReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","SpecialhazardReport");
        $this->SpecialhazardReport =  new SpecialhazardReport();        
        $specialhazardNo = $this->SpecialhazardReport->find('count',array('conditions'=>array('SpecialhazardReport.client_id'=>$clientId,'SpecialhazardReport.finish_date'=>NULL),'recursive'=>-1));
        
        App::import("Model","SprinklerReport");
        $this->SprinklerReport =  new SprinklerReport();        
        $sprinklerNo = $this->SprinklerReport->find('count',array('conditions'=>array('SprinklerReport.client_id'=>$clientId,'SprinklerReport.finish_date'=>NULL),'recursive'=>-1));
        
        $totalNo = $totalNo+$fireNo+$emergencyexitNo+$extinguisherNo+$kitchenhoodNo+$specialhazardNo+$sprinklerNo;
        return $totalNo;
   }
   
   //Function to get the number of completed reports for client
    //Manish Kumar
    //November 21,2012
   function getAllCompleteReportsClient($clientId = null){
    
        $totalNo = 0;
        
        App::import("Model","FirealarmReport");
        $this->FirealarmReport =  new FirealarmReport();        
        $fireNo = $this->FirealarmReport->find('count',array('conditions'=>array('FirealarmReport.client_id'=>$clientId,'FirealarmReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","EmergencyexitReport");
        $this->EmergencyexitReport =  new EmergencyexitReport();        
        $emergencyexitNo = $this->EmergencyexitReport->find('count',array('conditions'=>array('EmergencyexitReport.client_id'=>$clientId,'EmergencyexitReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","ExtinguisherReport");
        $this->ExtinguisherReport =  new ExtinguisherReport();        
        $extinguisherNo = $this->ExtinguisherReport->find('count',array('conditions'=>array('ExtinguisherReport.client_id'=>$clientId,'ExtinguisherReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","KitchenhoodReport");
        $this->KitchenhoodReport =  new KitchenhoodReport();        
        $kitchenhoodNo = $this->KitchenhoodReport->find('count',array('conditions'=>array('KitchenhoodReport.client_id'=>$clientId,'KitchenhoodReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","SpecialhazardReport");
        $this->SpecialhazardReport =  new SpecialhazardReport();        
        $specialhazardNo = $this->SpecialhazardReport->find('count',array('conditions'=>array('SpecialhazardReport.client_id'=>$clientId,'SpecialhazardReport.finish_date !='=>NULL),'recursive'=>-1));
        
        App::import("Model","SprinklerReport");
        $this->SprinklerReport =  new SprinklerReport();        
        $sprinklerNo = $this->SprinklerReport->find('count',array('conditions'=>array('SprinklerReport.client_id'=>$clientId,'SprinklerReport.finish_date !='=>NULL),'recursive'=>-1));
        
        $totalNo = $totalNo+$fireNo+$emergencyexitNo+$extinguisherNo+$kitchenhoodNo+$specialhazardNo+$sprinklerNo;
        return $totalNo;
   }
   
   //Function to get the number of reports to be started for client
    //Manish Kumar
    //November 21,2012
   function getNotStartedReportsClient($clientId = null){
    
        App::import("Model","Schedule");
        $this->Schedule =  new Schedule();
        $findSchData = $this->Schedule->find('all',array('conditions'=>array('ServiceCall.client_id'=>$clientId,'ServiceCall.call_status'=>1)));
        $totalCount = count($findSchData);
        $totalNo = $totalCount;
        if($totalCount>0){
            foreach($findSchData as $data){
                $reportId = $data['Schedule']['report_id'];
                $serviceId = $data['Schedule']['service_call_id'];
                switch($reportId){
                    case 1:
                        App::import("Model","FirealarmReport");
                        $this->FirealarmReport =  new FirealarmReport();        
                        $fireNo = $this->FirealarmReport->find('count',array('conditions'=>array('FirealarmReport.client_id'=>$clientId,'FirealarmReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$fireNo;                        
                        break;
                    case 2:
                        App::import("Model","SprinklerReport");
                        $this->SprinklerReport =  new SprinklerReport();        
                        $sprinklerNo = $this->SprinklerReport->find('count',array('conditions'=>array('SprinklerReport.client_id'=>$clientId,'SprinklerReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$sprinklerNo;
                        break;
                    case 3:
                        App::import("Model","KitchenhoodReport");
                        $this->KitchenhoodReport =  new KitchenhoodReport();        
                        $kitchenhoodNo = $this->KitchenhoodReport->find('count',array('conditions'=>array('KitchenhoodReport.client_id'=>$clientId,'KitchenhoodReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$kitchenhoodNo;
                        break;
                    case 4:
                        App::import("Model","EmergencyexitReport");
                        $this->EmergencyexitReport =  new EmergencyexitReport();        
                        $emergencyexitNo = $this->EmergencyexitReport->find('count',array('conditions'=>array('EmergencyexitReport.client_id'=>$clientId,'EmergencyexitReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$emergencyexitNo;
                        break;
                    case 5:
                        App::import("Model","ExtinguisherReport");
                        $this->ExtinguisherReport =  new ExtinguisherReport();        
                        $extinguisherNo = $this->ExtinguisherReport->find('count',array('conditions'=>array('ExtinguisherReport.client_id'=>$clientId,'ExtinguisherReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$extinguisherNo;
                        break;
                    case 6:
                        App::import("Model","SpecialhazardReport");
                        $this->SpecialhazardReport =  new SpecialhazardReport();        
                        $specialhazardNo = $this->SpecialhazardReport->find('count',array('conditions'=>array('SpecialhazardReport.client_id'=>$clientId,'SpecialhazardReport.servicecall_id'=>$serviceId),'recursive'=>-1));
                        $totalNo = $totalNo-$specialhazardNo;
                        break;
                    default:
                        break;
                }
            }
        }
        return $totalNo;
   }
   
//Function checkCompleteServiceCall
//desc: To check whether reports in service call completed or not
//Manish Kumar On Nov 30, 2012
    function checkCompleteServiceCall($servicecallId = null){
        App::import("Model","Schedule");
        $this->Schedule =  new Schedule();
        $reports = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$servicecallId,'ServiceCall.call_status'=>1)));
        $serviceFlag = false;
        if(count($reports)>0){
            foreach($reports as $reportData){
                $spId = $reportData['ServiceCall']['sp_id'];
                $clientId = $reportData['ServiceCall']['client_id'];
                $reportId = $reportData['Schedule']['report_id'];
                $serviceId = $reportData['Schedule']['service_call_id'];
                $chkComplete = $this->getReportCompleted($reportId,$serviceId,$clientId,$spId);
                if($chkComplete!=""){
                    $serviceFlag = true;
                    break;
                }
            }
        }
        return $serviceFlag;
    }
    
//Function checkPendingServiceCall
//desc: To check whether reports in service call pending or not
//Manish Kumar On Nov 30, 2012
    function checkPendingServiceCall($servicecallId = null){
        App::import("Model","Schedule");
        $this->Schedule =  new Schedule();
        $reports = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$servicecallId,'ServiceCall.call_status'=>1)));
        $serviceFlag = false;
        if(count($reports)>0){
            foreach($reports as $reportData){
                $spId = $reportData['ServiceCall']['sp_id'];
                $clientId = $reportData['ServiceCall']['client_id'];
                $reportId = $reportData['Schedule']['report_id'];
                $serviceId = $reportData['Schedule']['service_call_id'];
                
                $getCompleted = $this->getReportCompleted($reportId,$serviceId,$clientId,$spId);
                $getstarted = $this->getReportStarted($reportId,$serviceId,$clientId,$spId);
                
                if($getstarted == 1 && $getCompleted == ""){
                    $serviceFlag = true;
                    break;
                }
            }
        }
        return $serviceFlag;
    }
    
//Function checkNotStartedServiceCall
//desc: To check whether reports in service call not started or not
//Manish Kumar On Nov 30, 2012
    function checkNotStartedServiceCall($servicecallId = null){
        App::import("Model","Schedule");
        $this->Schedule =  new Schedule();
        $reports = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$servicecallId,'ServiceCall.call_status'=>1)));
        $serviceFlag = false;
        if(count($reports)>0){
            foreach($reports as $reportData){
                $spId = $reportData['ServiceCall']['sp_id'];
                $clientId = $reportData['ServiceCall']['client_id'];
                $reportId = $reportData['Schedule']['report_id'];
                $serviceId = $reportData['Schedule']['service_call_id'];
                
                $getCompleted = $this->getReportCompleted($reportId,$serviceId,$clientId,$spId);
                $getstarted = $this->getReportStarted($reportId,$serviceId,$clientId,$spId);
                
                if($getstarted == 0 && $getCompleted == ""){
                    $serviceFlag = true;
                    break;
                }
            }
        }
        return $serviceFlag;
    }
//Function getQuoteDoc
//desc: To get the info of 
//Manish Kumar On Dec 03, 2012    
    function getQuoteDoc($reportId=null,$clientId = null, $spId = null, $serviceId = null){
        App::import('Model','Quote');
        $this->Quote = new Quote();
        $clientQuote = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$serviceId, 'Quote.report_id'=>$reportId, 'Quote.client_id'=>$clientId, 'Quote.sp_id'=>$spId)));
        return $clientQuote;
    }
    
//Function getLeadIns 
//desc: to fetch the lead inspectors for service call
//Created By: Manish Kumar on dec 10, 2012
    function getLeadIns($servicecall_id = null){
        App::import('Model','Schedule');
        $this->Schedule = new Schedule();
        App::import('Model','User');
        $this->User = new User();
        $schData = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$servicecall_id)));
        $leadIns = array();
        if(count($schData)>0){
            foreach($schData as $schs){
                $userData = $this->User->findById($schs['Schedule']['lead_inspector_id'],array('User.fname','User.lname'));
                $leadIns[]=$userData['User']['fname'].' '.$userData['User']['lname'];
            }
        }        
        if(count($leadIns)>0){
            $leadIns = array_unique($leadIns);
            $leadIns = implode(',',$leadIns);
        }
        return $leadIns;
        
    }
    
//Function getServiceCallName
//desc: To retuen the name of service call from service call id
//created By: Manish Kumar
    function getServiceCallName($servicecall_id = null){
        $serviceCallName = '';
        if($servicecall_id!=null){
            App::import('Model','ServiceCall');
            $this->ServiceCall = new ServiceCall();
            $serviceData = $this->ServiceCall->findById($servicecall_id,array('ServiceCall.name'));
            if(is_array($serviceData)){
                $serviceCallName = $serviceData['ServiceCall']['name'];
            }
        }
        return $serviceCallName;
    }
    
//Function getServiceDataByServiceId
//Desc to get the service call data
//created By: Manish Kumar
    function getServiceDataByServiceId($callId = null){
        $serviceData="";
        if($callId!=null && $callId!=""){
            App::import('Model','ServiceCall');
            $this->ServiceCall = new ServiceCall();
            $serviceData = $this->ServiceCall->findById($callId,array('ServiceCall.id','ServiceCall.client_id','ServiceCall.sp_id'));            
        }
        return $serviceData;
    }
    
//Function getSiteNameByServiceCallID
//Desc to find the site name form service call data
//Created By Manish Kumar
    function getSiteNameByServiceCallID($serviceCallId = null){
        $siteName = '';
        if($serviceCallId!=null){
            APP::import('Model','ServiceCall');
            $this->ServiceCall = new ServiceCall();
            $data = $this->ServiceCall->findById($serviceCallId,array('ServiceCall.sp_site_address_id'));
            APP::import('Model','SpSiteAddress');
            $this->SpSiteAddress = new SpSiteAddress();
            $siteData = $this->SpSiteAddress->findById($data['ServiceCall']['sp_site_address_id'],array('SpSiteAddress.site_name'));
            $siteName = $siteData['SpSiteAddress']['site_name'];
        }
        return $siteName;
    }
    
//Function chkScheduleService
//Desc To check whether service is checked or not
//Created By Manish Kumar
    function chkScheduleService($scheduleId,$serviceId,$identifyValue)
    {
        $returnValue = '';
        APP::import('Model','ScheduleService');
        $this->ScheduleService = new ScheduleService();
        $schService = $this->ScheduleService->find('first',array('conditions'=>array('ScheduleService.service_id'=>$serviceId,'ScheduleService.schedule_id'=>$scheduleId)));
        if(!empty($schService)){
            if($identifyValue=='onlyCheck'){
                $returnValue = true;
            }else if($identifyValue=='frequency'){
                $returnValue = $schService['ScheduleService']['frequency'];
            }else if($identifyValue=='amount'){
                $returnValue = $schService['ScheduleService']['amount'];
            }else{
                $returnValue="";
            }
        }else{
            $returnValue="";
        }
        return $returnValue;
    }
//Function: getScheduleDays
//Desc: To calculate the days of schedule
//Created By Manish Kumar On Dec 27, 2012
    function getScheduleDays($scheduleId){
        $getDays = 1;
        APP::import('Model','Schedule');
        $this->Schedule = new Schedule();
        $schData = $this->Schedule->findById($scheduleId);
        if(!empty($schData)){
            if($schData['Schedule']['schedule_date_10']!=NULL){
                $getDays = 10;
            }else if($schData['Schedule']['schedule_date_9']!=NULL){
                $getDays = 9;
            }else if($schData['Schedule']['schedule_date_8']!=NULL){
                $getDays = 8;
            }else if($schData['Schedule']['schedule_date_7']!=NULL){
                $getDays = 7;
            }else if($schData['Schedule']['schedule_date_6']!=NULL){
                $getDays = 6;
            }else if($schData['Schedule']['schedule_date_5']!=NULL){
                $getDays = 5;
            }else if($schData['Schedule']['schedule_date_4']!=NULL){
                $getDays = 4;
            }else if($schData['Schedule']['schedule_date_3']!=NULL){
                $getDays = 3;
            }else if($schData['Schedule']['schedule_date_2']!=NULL){
                $getDays = 2;
            }else if($schData['Schedule']['schedule_date_1']!=NULL){
                $getDays = 1;
            }else{
                $getDays = 0;
            }
        }
        return $getDays;
    }
//Function: fetchAttachedCerts
//Desc: To fetch all the attached certificates
//Created By Manish Kumar On Dec 27, 2012    
    function fetchAttachedCerts($schId){
        $setReports = array();
        APP::import('Model','ReportCert');
        $this->ReportCert = new ReportCert();
        $reports = $this->ReportCert->find('list',array('conditions'=>array('ReportCert.schedule_id'=>$schId),'fields'=>array('ReportCert.cert_id')));
        if(!empty($reports)){
            $setReports = $reports;
        }
        return $setReports;
    }
    
    function missingTextExplanation($serviceCallId,$reportId){        
        APP::import('Model','ExplanationText');
        $this->ExplanationText = new ExplanationText();
        $data = $this->ExplanationText->find('first',array('conditions'=>array('ExplanationText.servicecall_id'=>$serviceCallId,'ExplanationText.report_id'=>$reportId),'fields'=>array('ExplanationText.missing_reason','ExplanationText.missing_file')));
        return $data;
    }
    
    function additionalTextExplanation($serviceCallId,$reportId){        
        APP::import('Model','ExplanationText');
        $this->ExplanationText = new ExplanationText();
        $data = $this->ExplanationText->find('first',array('conditions'=>array('ExplanationText.servicecall_id'=>$serviceCallId,'ExplanationText.report_id'=>$reportId),'fields'=>array('ExplanationText.additional_reason','ExplanationText.additional_file')));
        return $data;
    }
    
//Function checkNotStartedPastServiceCall
//desc: To check whether reports in service call not started or not
//Manish Kumar On Nov 30, 2012
    function checkNotStartedPastServiceCall($servicecallId = null){
        App::import("Model","Schedule");
        $this->Schedule =  new Schedule();
        $reports = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$servicecallId,'ServiceCall.call_status'=>1)));
        $serviceFlag = false;
        if(count($reports)>0){
            foreach($reports as $reportData){
                $spId = $reportData['ServiceCall']['sp_id'];
                $clientId = $reportData['ServiceCall']['client_id'];
                $reportId = $reportData['Schedule']['report_id'];
                $serviceId = $reportData['Schedule']['service_call_id'];
                
                $getCompleted = $this->getReportCompleted($reportId,$serviceId,$clientId,$spId);
                $getstarted = $this->getReportStarted($reportId,$serviceId,$clientId,$spId);
                $remDays = $this->getRemainingDays($reportData['Schedule']['id']);
                
                if($getstarted == 0 && $getCompleted == "" && $remDays=='Submission date expired'){
                    $serviceFlag = true;
                    break;
                }
            }
        }
        return $serviceFlag;
    }    
}