<style>
.error-message{
	position:relative;color:#B86464;	
}
</style>
<?php echo $this->Html->script('fckeditor'); ?>


<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Edit Website Page</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'editpage') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
		<?php echo $form->hidden('Websitepage.id'); ?>
                <!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <div class="row">
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		    </span>
  		 </div>	 
		<div class="row">
				<label>Page Name : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:500px; margin-bottom:20px">
					<?php echo $form->text('Websitepage.page_name', array('maxLength'=>'50','class'=>'text',"div"=>false,"label"=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Websitepage.page_name'); ?></span>
				</div>
		</div>                
                <div class="row">
				<label>Page Meta Title : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:500px; margin-bottom:20px">
					<?php echo $form->text('Websitepage.meta_title', array('maxLength'=>'50','class'=>'text',"div"=>false,"label"=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Websitepage.meta_title'); ?></span>
				</div>
		</div>
   
	    
                <div class="row">
				<label>Page Meta Keyword : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:500px; margin-bottom:20px">
					<?php echo $form->text('Websitepage.meta_keywords', array('maxLength'=>'50','class'=>'text',"div"=>false,"label"=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Websitepage.meta_keywords'); ?></span>
				</div>
		</div>
                <div class="row">
				<label>Meta Description : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:500px; margin-bottom:20px">
					<?php echo $form->text('Websitepage.meta_description', array('maxLength'=>'50','class'=>'text',"div"=>false,"label"=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Websitepage.meta_description'); ?></span>
				</div>
		</div>
		<div class="row">
				<label>Page position : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					
					<?php echo $this->Form->radio('Websitepage.page_type',array('H'=>'Header','F'=>'Footer','P'=>'Property Owner','D'=>'Sp header during registration'),array('id'=>'radiobtn','class'=>'','legend'=>false,'label'=>false,'div' => false)); ?>	
					<span class="system negative" id="err1"><?php echo $form->error('Websitepage.page_type'); ?></span>
				</div>
		</div>
				
		<div class="row">
				<label>Page Content : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:750px;">
					<?php echo $form->input('Websitepage.page_content', array('type'=>'textarea','rows' => 10,'cols' =>23,'label'=>false));  
					      echo $fck->load('WebsitepagePageContent');
					?>
					</span>
				</div>
		</div>
			<!--[if !IE]>start row<![endif]-->
		<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="list" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
		</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
