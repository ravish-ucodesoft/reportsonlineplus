<?php 
	echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Website Pages</h2>					
					<div id="product_list_menu">
						<a href="/admin/websitepages/manageMainTabs" class="update"><span><span><em>Manage Home Page Tabs</em><strong></strong></span></span></a>
						<a href="/admin/websitepages/addpage" class="update"><span><span><em>Add New</em><strong></strong></span></span></a>
						
					</div>				
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
                            <?php echo $this->Form->create('', array('controller'=>'websitepages','action' => 'pagelisting','name'=>'webpages','id'=>'WebsitepagesShowForm') );?>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<th><input type="checkbox"  name='checkall' onclick='checkedAllpage();' id='checkall'></th>								
								<th width='30%'><?php echo $paginator->sort('Page Name','page_name');?></th>
								<th width='20%'>Order</th>
								<th width='15%'><?php echo $paginator->sort('Page Position','page_type');?></th>
								<th width='10%'><?php echo $paginator->sort('Status','status');?></th>
								<th width='20%'>Last Edited</th>
								<th width='5%'>Action</th>
							</tr>
							<?php  if(count($websitePageName)) { ?>
						<tbody>		
							<?php $i=1; foreach ($websitePageName as $page): ?>							
							      <tr class="first">
								<td><input type="checkbox" name="box[]" value="<?php echo $page['Websitepage']['id']; ?>" onclick='uncheck(this);'></td>
								<td><?php echo $page['Websitepage']['page_name']; ?></td>
								<td><Input type="hidden" name="orderingids[]" value="<?php echo $page['Websitepage']['id']; ?>"><input type="text" width='20px' style="width:40px" class='required formElement1' name="orderbox[]" value="<?php echo $page['Websitepage']['ordering']; ?>" ></td>
								<td><?php if($page['Websitepage']['page_type'] == 'F'){ echo 'Footer';}elseif($page['Websitepage']['page_type'] == 'H'){ echo 'Header'; }elseif($page['Websitepage']['page_type'] == 'D'){ echo "SP Header"; }else{echo 'Property Owner';} ?></td>
								<td><?php echo $page['Websitepage']['status']=='1'?__('Active'):__('Inactive'); ?></td>
								<td><?php echo $time->format('m-d-Y',$page['Websitepage']['modified']);?></td>
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:120px'>			
											<li><?php echo $html->link('Edit',array('controller'=>'websitepages','action'=>'editpage',$page['Websitepage']['id']),array('class'=>'edit')) ?></li>
										
										<!--	<li><?php echo $html->link('Delete',array('controller'=>'Companies','action'=>'subscriptionchargedelete',$page['Websitepage']['id']),array('class'=>'delete'),'Are you sure you want to delete Subscription Charge?') ?></li> -->
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>
                        
						</tbody>
                        <table>
                            <tr>
                                <td><?php echo $this->Form->submit("Active",array('id'=>'active','div'=>false,'class'=>'iconlink','name' => 'active',"onClick"=>'return prompt_activepage();')); ?></td>
                                <td><?php echo $this->Form->submit("Inactive",array('id'=>'Inactive','div'=>false,'class'=>'iconlink','name' => 'inactive',"onClick"=>'return prompt_inactivepage();')); ?></td>
                                <td><?php echo $this->Form->submit("Save",array('id'=>'save','div'=>false,'class'=>'iconlink','name' => 'save')); ?></td>
                            </tr>    
                        <table>                       
<?php } else { ?>
	<tr class="first"><td colspan='6'>There no subscription</td> </tr>
<?php } ?>
</table>
                        <?php echo $this->Form->end(); ?>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->					
				
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->
<script>
function prompt_activepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	

	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
	
      }
      
      if(flag==1)
      {
	      if(confirm('Are you sure you want to make this page active ?')){
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one page to active");
      return false;
      }
}

function prompt_inactivepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	
	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
      }
      
      if(flag==1)
      {
      
	      if(confirm('Are you sure you want to make this page inactive ?')){
		//document.webpages.action = '/navcake/admin/websitepages/inactive';
		//document.webpages.submit;
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one page to inactive");
      return false;
      }
}

function checkedAllpage ()
      {        
        if (document.getElementById('checkall').checked == false)
        {
        checked = false
        }
        else
        {
        checked = true
        }
        for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++)
        {
          document.getElementById('WebsitepagesShowForm').elements[i].checked = checked;
        }
      }
</script> 
                                
                                