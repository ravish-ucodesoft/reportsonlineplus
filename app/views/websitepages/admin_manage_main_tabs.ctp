<style>
.error-message{
	position:relative;color:#B86464;	
}
</style>

<?php echo $this->Html->script('fckeditor'); ?>

<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Edit Tabs Description</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'manageMainTabs') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
		<?php echo $form->hidden('MainTab.id'); ?>
                <!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <div class="row">
  		   
  		 </div>
		  <div class="row">
				<label style="width:150px;">Tittle 1 : </label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:500px; margin-bottom:20px">
					<?php echo $form->text('MainTab.title1', array('maxLength'=>'100','class'=>'text',"div"=>false,"label"=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('MainTab.title1'); ?></span>
				</div>
		</div>
		  
	    
                <div class="row">
				<label style="width:150px;">Description 1 :</label>
				<div class="inputs" style="float:left">
					<span  class="input_wrapper" style="width:700px;">
					<?php //echo $form->input('MainTab.description1', array('type'=>'textarea','rows' => 10,'cols' =>50,'label'=>false,'div'=>false));  
					     
					?>
					<?php echo $form->input('MainTab.description1', array('type'=>'textarea','rows' => 10,'cols' =>23,'label'=>false));  
					      echo $fck->load('MainTabDescription1');
					?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('MainTab.description1'); ?></span>
				</div>
		</div>
		 <div class="row">
				<label style="width:150px;">Tittle 2 : </label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:500px; margin-bottom:20px">
					<?php echo $form->text('MainTab.title2', array('maxLength'=>'100','class'=>'text',"div"=>false,"label"=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('MainTab.title2'); ?></span>
				</div>
		</div>
		
				
		<div class="row">
				<label style="width:150px;">Description 2 :</label>
				
				<div class="inputs" style="float:left">
					<span  class="input_wrapper" style="width:700px;" >
					<?php //echo $form->input('MainTab.description2', array('type'=>'textarea','rows' => 10,'cols' =>50,'label'=>false,'div'=>false));  
					     
					?>
					<?php echo $form->input('MainTab.description2', array('type'=>'textarea','rows' => 10,'cols' =>23,'label'=>false));  
					      echo $fck->load('MainTabDescription2');
					?>
					</span>
				</div>
		</div>
			<!--[if !IE]>start row<![endif]-->
		<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="pagelisting" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
		</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
