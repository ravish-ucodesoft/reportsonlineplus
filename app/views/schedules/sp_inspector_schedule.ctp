<?php
    $action = ((isset($this->params['named']['view']))?'inspector_schedule/view:'.$this->params['named']['view']:'inspector_schedule');
    $selaction = ((isset($this->params['pass'][0]))?$this->params['pass'][0]:'');
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));    
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<?php echo $this->Html->css(array('tooltip')); ?>
<?php echo $this->Html->script(array('tooltip')); ?>
<style>
.inspector{text-align:center;font-size:16px;color:#3E4061;}
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#3E4061}
.normal{
    background-color: #FFDC7B;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
	text-align: center;
	font-weight:bold;
}
.normal td {
    background-color: #FFDC7B;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
	text-align: center;
	font-weight:bold;
}
.highlight td {
    background-color: #EFFEFE;
    border: 1px solid black;
    color: #000000;
    cursor: pointer;
    font-size: 11px;
    text-align: center;
	font-weight:bold;
}
.intp{font-size:9px;color:#2B92DD;}
#schw{height:auto;text-align:left;width:98%;margin:1px;border:1px solid #FFF;color:#000;}
.viewmode {font-weight:bold;text-align:left;font-size: 14px;color:grey;}
</style>
<script type="text/javascript">

$(function(){	
    $("#accordion").accordion({ header: "h3"});
    $("#accordionClient").accordion({ header: "h3"});
    jQuery(".ajax").colorbox();
    $('#dialog_link, ul#icons li').hover(
	function() { $(this).addClass('ui-state-hover'); },
	function() { $(this).removeClass('ui-state-hover'); }
    );
    $('.boxtd').hover(
	    function(){
		    $(this).find('.createcall').show();
	    },
	    function(){
		    $(this).find('.createcall').hide();
	    }
    );
});
function switchView(id,parameter)
{
    
	
	if(id=='accordingView')
	{
	    if(parameter=='cl'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    if(parameter=='calender'){
		  
		$("#calendarView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#accordingView").slideUp(500);
	    }
		
		
	}
	if(id=='clientView'){
	    if(parameter=='c'){
		$("#calendarView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#clientView").slideUp(500);
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    
	}
	else
	{
	    if(parameter=='c'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
		//$("#calendarView").slideUp(500);
		//$("#accordingView").slideDown(500);
	}
}
 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
function setUrl(insId){
    window.location.href = '/sp/schedules/<?php echo $action;?>/'+insId;
}
function setClick(){
    window.location.href = '/sp/schedules/schedule/view:accordion';
}
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}			
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box" >
	<h3 id="adduser">Inspector Schedule as Service Calls</h3>
        <br/>
	<?php if(isset($this->params['named']['view']) && !empty($this->params['named']['view'])){?>
	<div style="width:100%; text-align: center; padding-bottom: 10px;">
	    <?php
		$options = array();
		if(!empty($allInspector)){
		    foreach($allInspector as $insDatas){
			$options[$insDatas['User']['id']] = $insDatas['User']['fname'].' '.$insDatas['User']['lname'];
		    }
		}
		echo 'Select Inspector : '.$form->select('',$options,$selaction,array('name'=>'selectIns','onchange'=>'setUrl(this.value);'));
	    ?>
	</div>
	<?php }?>
	
	<?php if(isset($this->params['pass'][0]) && !empty($this->params['pass'][0])){?>
	<div class="inspector" >Schedules For Inspector <strong><?php echo ucwords($inspector['User']['fname'].' '.$inspector['User']['lname']);?></strong></div>
	
	<!-- clientView div ID Start-->
	<div id="clientView" style="display:none">		    
	</div>
	<!-- clientView div ID end-->
	    
	<!-- AccordianView div ID Start-->
	<div id="accordingView" style="display:none">
	</div>
	<!-- Accordian div closed-->			
	    
	    
	<!-- calendarView div ID closed-->
	<div id="calendarView" style="display:block">
	    <div class="viewmode">You are watching Monthly View</div>
	    <div class="viewmode"><a href="/sp/schedules/inspector_weekly_view/<?php echo $inspector['User']['id']?>">Switch to Weekly View</a></div>
	    <?php $arr=array();		
		foreach($data as $rec){
		    foreach($rec['Schedule'] as $sch){
			unset($sch['ScheduleService']);
			$arr[] = $sch;
		    }				
		}		
	    ?>			
	    <!--Calendar Start-->
	    <table width="100%" cellpadding='0' cellspacing='0' border='0'>          
		<tr>
		    <td align="center" style="padding-left:10px;color:#550707"><strong><?php echo ucwords($inspector['User']['fname'].' '.$inspector['User']['lname']);?></strong> engaged in the inspection.</td>		  
		</tr>        
		<tr><td><?php echo $calendar->calmenumonth($tstamp); ?></td></tr>       
		<?php $first_arr=array('Jan'=>'Jan','Mar'=>'Mar','May'=>'May','Jul'=>'Jul','Aug'=>'Aug','Oct'=>'Oct','Dec'=>'Dec');		
		if(in_array(date('M',$tstamp),$first_arr)) {			
		    $val=31;
		    $pad='4';
		}else{
		    $val=30;
		    $pad='5';
		}     
		?>                  
		<div style="width:99%;overflow: auto;">   
		    <table width="100%" cellspacing="0" cellpadding='0'>			
			<tbody>
			    <tr>  
				<?php					
				for($i=1;$i<=$val;$i++){
				    $flag[$i]=0;
				    if($i<10) {  $i='0'.$i; }
				?>                  
				    <td valign="top" style="height:100px;" width="14%" class="boxtd">
				    <div style='width:100%; text-align: center; margin:2px 0 10px 0; border: 1px solid #EEEEEE; background: #A7CDF0'><b><?php echo $i.' '.date('M ',$tstamp); ?></b></div>
				<?php
				    $dateM=date('m',$tstamp);
				    $dateY=date('Y',$tstamp);
				    $dateD=$i;    
				    $shift_date=$dateY.'-'.$dateM.'-'.$dateD;					
				    foreach($arr as $key1=>$schedule):
					$approvalReason = (($schedule['ServiceCall']['bypass_mail']==1)?'Direct Scheduling':'Schedule set with customers Approved notification');
					$siteInfo = $this->Common->getSiteInfo($schedule['ServiceCall']['sp_site_address_id']);
					$siteInfoParts = explode(',',$siteInfo);
					$siteName = $siteInfoParts[0];
					if($shift_date==$schedule['schedule_date_1'] || $shift_date==$schedule['schedule_date_2'] || $shift_date==$schedule['schedule_date_3'] || $shift_date==$schedule['schedule_date_4'] || $shift_date==$schedule['schedule_date_5'] || $shift_date==$schedule['schedule_date_6'] || $shift_date==$schedule['schedule_date_7'] || $shift_date==$schedule['schedule_date_8'] || $shift_date==$schedule['schedule_date_9'] || $shift_date==$schedule['schedule_date_10'])
					{
					    $flag[$i]=1;						
				?>					   
					    <div id='schw' style="background-color:<?php echo $this->Common->getUserColorCode($schedule['lead_inspector_id']);?>">
					    <?php
						$txtToShow='<strong>Name:</strong>'.$schedule['ServiceCall']['name'];
						$txtToShow.='<br/><strong>Lead:</strong>'.$this->Common->getClientName($schedule['lead_inspector_id']);
						$txtToShow.='<br/><strong>Site Name:</strong>'.$siteName;
						$txtToShow.='<br/><strong>Report:</strong>'.$this->Common->getReportName($schedule['report_id']);
						$txtToShow.='<br/><strong>Client:</strong>'.$this->Common->getCompanyNameByServiceCallID($schedule['service_call_id']);
						$txtToShow.='<br/><strong>Days left for report submission:</strong>'.$this->Common->getRemainingDays($schedule['id']);
						$txtToShow.='<br/><strong>Approval Reason:</strong>'.$approvalReason;
					    ?>
						<p onclick="setClick();" onmouseover="tooltip.show('<?php echo $txtToShow;?>');" onmouseout="tooltip.hide();" class="intp">
						    <strong>Name:</strong><?php echo $schedule['ServiceCall']['name'];?><br/>
						    <!--<strong>Lead:</strong><?php //echo $this->Common->getClientName($schedule['lead_inspector_id']);?><br/>-->
						    <strong>Site Name:</strong><?php echo $siteName;?>
						    <br/>
						    <strong>Report:</strong><?php echo $this->Common->getReportName($schedule['report_id']);?>
						    <br/>
						    <!--<strong>Client:</strong><a href="javascript:void(0);" onclick="switchView('accordingView','cl');"><?php //echo $this->Common->getCompanyNameByServiceCallID($schedule['service_call_id']);?></a>
						    <br/>
						    <strong>Days left for report submission:</strong><?php //echo $this->Common->getRemainingDays($schedule['id']);?>
						    <br/>
						    <strong>Approval Reason:</strong><?php //echo $approvalReason;?>-->
						</p>					   
					    </div>
				<?php
					}					   
				    endforeach;
				    if($shift_date>date('Y-m-d'))
				    {
				?>		<a href="/sp/schedules/createServiceCallByCalendar/<?php echo $shift_date ?>" title="Create a Service Call"><div id='schw'></div></a>
					    <a href="/sp/schedules/createServiceCallByCalendar/<?php echo $shift_date ?>" title="Create a Service Call" class="createcall" style="display:none;">Create a service call</a>
				<?php
				    }		   
				?>                   
				    </td>
				<?php if($i%7==0){?>
			    </tr>
			    <tr>
				<?php
				    }
				}
				?>
				    <td colspan='<?php echo $pad;?>'></td>
			    </tr>
			</tbody>
		    </table>
		</div>
	    </table>    
	    <br/>
	</div>
	<!-- calendarView div ID closed-->
	<?php }else{?>
	<div style="width: 100%; text-align: center; font-size: 16px;"><b>Please select the inspector from the list to view the schedule.</b></div>
	<?php }?>
    </div>
</div>
    
<script type="text/javascript">
function redirecturl(tstamp)
{
 window.location='/sp/schedules/<?php echo $action;?>/<?php echo $inspector['User']['id'] ?>/'+tstamp;
}
</script>