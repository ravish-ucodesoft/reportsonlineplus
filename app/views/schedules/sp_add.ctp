<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js')); 
?>
<style type="text/css">
#ui-datepicker-div{
   z-index: 9999;
}
</style>
<script type="text/javascript">
 jQuery(function() {
        jQuery("#ScheduleScheduleDate").datepicker({ dateFormat: 'yy-mm-dd',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });});
  function validationErrors(){
	if($("#ScheduleScheduleDate").val() == ""){
		alert("Please select schedule date");
		return false;
	}else if($("#stime").val() == ""){
		alert("Please select schedule time");
		return false;
	} else if($("#sformat").val() == ""){
                alert("Please select schedule format");
		return false;					       
	} else if($("#suser").val() == ""){
                alert("Please select schedule client");
		return false;					       
	} else if($("#ScheduleComments").val() == ""){
                alert("Please enter schedule comments");
		return false;					       
	} else{
		return true;			       
	}
  }

  function limit(field, chars){
        if (document.getElementById(field).value.length > chars){
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){ document.getElementById('essay_Error').innerHTML=''; }
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
 }
</script>
<div id="content">
     <div id="box">
               <h3 id="adduser">Schedule a Audit Meeting</h3>
	           <?php  
                        echo $form->create('Schedule', array('type'=>'POST', 'url'=>array('action'=>'add'),'name'=>'loginform','id'=>'form')); 
			 echo $form->input('Schedule.client_id',array('type'=>'hidden','value'=> $cid));
			  echo $form->input('Schedule.agent_id',array('type'=>'hidden','value'=> $agent_id));
                   ?>
                   <br>
		     <label for="OwnerOldpwd">Schedule date</label>
		    <?php echo $form->input('Schedule.schedule_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','readonly'=>'readonly')); ?>
		    <br/><br/>
                   <label for="OwnerOldpwd">Schedule Time</label>
		   <?php for($i=1;$i<13;$i++){
                           $time[$i] = $i;
                         }
			 $format = array("AM"=>"AM","PM"=>"PM");
                         echo $form->input('Schedule.schedule_time',array('options' =>$time,'div'=>false,'label'=>false,"style"=>"width:100px;",'empty'=>'Select Time','id'=>'stime'));
			 echo $form->input('Schedule.format',array('options' =>$format,'div'=>false,'label'=>false,"style"=>"width:150px;",'empty'=>'Select Format','id'=>'sformat'));
		   ?>
                   <br /><br />
		   <label for="OwnerOldpwd">Select Services</label>
                   <br /><br />
                   
                   <label for="OwnerCpwd">Comments</label>
                   <?php echo $form->input('Schedule.comments',array('type'=>'text','rows'=>2,'div'=>false,'label'=>false,'onKeyUp' =>"return limit(this.id,200);")); ?>                     
                   <br /><br />
		   <div style="float:right;padding-right:124px;">
                   <span id='limitCounter'>0</span>&nbsp;characters entered&nbsp;|&nbsp;<span id='limitCounterLeft'>200</span>&nbsp;characters remaining
                   </div>
                   <div id="essay_Error" class='error'>&nbsp;</div>
		   <br /> 
                   <div align="left">
                   <?php echo $form->submit('Submit',array('id'=>'button1', 'style'=>'width:150px;margin-left:110px;',"onclick"=>"return validationErrors();")); ?> &nbsp;                 
                   </div>
                    <br/>
                   <?php echo $form->end(); ?>
       </div>
</div>