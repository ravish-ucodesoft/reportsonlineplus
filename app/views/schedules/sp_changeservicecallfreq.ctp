
<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;     
}
#box {
    border: none;
}
table {
    border-collapse: collapse;
    margin: 1px;
    width: 98%;
}
</style>
<div id="content">
     <div id="box" style="width:700px;padding-left:10px;">
     <div class="form-box" style="width:90%">
        <!--<h2 style="font-weight:bold;">Service Calls of <?php echo $result['User']['fname']." ".$result['User']['lname']; ?></h2> -->
	 <table class="tbl" width="100%">
                        
			<tr>
			    <td width="90%" align="left">
				<div id="accordion">
				<?php if(!empty($data))
				{
              // pr($data);
					?>
					
					<h3><a href="#"><?php echo date('m/d/Y H:i:s',strtotime($data['ServiceCall']['created'])); ?></a></h3>
					<table class="inner" cellpadding="0" cellspacing="0" width="100%">
                  <tr><td colspan="5" style="background-color:#817679;color:#FFF;font-size:14px;"><strong><?php echo $this->Common->getClientName($data['ServiceCall']['client_id']); ?></strong></td>
						<tr>
						<th width="20%" style="background-color:#2B92DD;color:#FFF">Report</th>
						<th width="20%" style="background-color:#2B92DD;color:#FFF">Lead Inspector</th>
						<th width="35%" style="background-color:#2B92DD;color:#FFF">Helper</th>
						<th width="25%" style="background-color:#2B92DD;color:#FFF" align="center">Sch Date(m/d/Y)</th>
						<th width="25%" style="background-color:#2B92DD;color:#FFF" align="center">&nbsp;</th>
						</tr>		
						<?php foreach($data['Schedule'] as $key1=>$schedule):?>
						<tr style="background-color:#e2eaed;">
							<td ><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
							<td><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
							<td><?php
							$helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
							echo $this->Common->getHelperName($helper_ins_arr); ?></td>
							<td>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:ia', $schedule['schedule_from_1']).'-'.$time->format('g:ia', $schedule['schedule_to_1']); ?>
<br/>
							    <?php if(!empty($schedule['schedule_date_2'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:ia', $schedule['schedule_from_2']).'-'.$time->format('g:ia', $schedule['schedule_to_2'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_3'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:ia', $schedule['schedule_from_3']).'-'.$time->format('g:ia', $schedule['schedule_to_3'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_4'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:ia', $schedule['schedule_from_4']).'-'.$time->format('g:ia', $schedule['schedule_to_4'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_5'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:ia', $schedule['schedule_from_5']).'-'.$time->format('g:ia', $schedule['schedule_to_5'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_6'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:ia', $schedule['schedule_from_6']).'-'.$time->format('g:ia', $schedule['schedule_to_6'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_7'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:ia', $schedule['schedule_from_7']).'-'.$time->format('g:ia', $schedule['schedule_to_7'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_8'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:ia', $schedule['schedule_from_8']).'-'.$time->format('g:ia', $schedule['schedule_to_8'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_9'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:ia', $schedule['schedule_from_9']).'-'.$time->format('g:ia', $schedule['schedule_to_9'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_10'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:ia', $schedule['schedule_from_10']).'-'.$time->format('g:ia', $schedule['schedule_to_10'])?>
							    <?php } ?>
							</td>
							<td align='left'><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
							    <?php
							     //echo $schedule['report_id']."--".$res['ServiceCall']['id']."---".$res['ServiceCall']['client_id']."--".$res['ServiceCall']['sp_id'];
								  $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$data['ServiceCall']['id'],$data['ServiceCall']['client_id'],$data['ServiceCall']['sp_id']);
								  $getstarted = $this->Common->getReportStarted($schedule['report_id'],$data['ServiceCall']['id'],$data['ServiceCall']['client_id'],$data['ServiceCall']['sp_id']);
								 if($getCompleted != "" && $getstarted == 1){
								    echo "<a href=''></a>";
								     ?>
								    <a style="text-decoration: blink;" href="/sp/reports/<?php echo $this->Common->getReportLinkName($schedule['report_id']);?>?reportID=<?php echo $schedule['report_id']; ?>&clientID=<?php echo $data['ServiceCall']['client_id']; ?>&spID=<?php echo $data['ServiceCall']['sp_id']; ?>&serviceCallID=<?php echo $data['ServiceCall']['id']; ?>"><blink>Completed on <?php echo date('m/d/Y',strtotime($getCompleted)); ?></blink></a>
								 <?php
								}
								 if($getstarted == 0 && $getCompleted == ""){
								    echo "<blink>Not Started Yet</blink>";
								  }
								  else if($getstarted == 1 && $getCompleted == ""){
								    echo "<blink>Pending</blink>";
								  }
							    ?>
							</td>							
						</tr>
						<tr><td colspan="5">
						<div class="service-hdline">Services Taken</div>
						<ul class="services-tkn"><li>
						<?php $i=0;$j=1;
						
						for($i=0;$i<count($schedule['ScheduleService']);$i++)
						{
							echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
					        <?php if($j%4==0){
							echo '</li></ul><ul class="services-tkn"><li>';
						}
						else{
							echo '</li><li>';
						}
						 $j++;
						}
						?>
						</td></tr>
						<?php endforeach;?>
					</table>				
						
					<?php					
				} else {
				   }?>
    </div>
    <!--one complete form ends-->
    
    
    
    <!--Form start -->
    <?php echo $form->create('', array('type'=>'POST','controller'=>'schedules','action'=>'changeservicecallfreq','inputDefaults' => array('label' => false,'div' => false))); ?>
            <?php echo $form->input('ServiceCall.id',array('type'=>'hidden','value'=>$data['ServiceCall']['id']));?>
	 
            <table class="tbl input-chngs1" width="100%">
		
         <tr><td colspan="2">&nbsp;</td></tr>
         <tr>            
         <td><?php $optiondata = array('No'=>'No,i do not want to auto-populate','Y'=>'Yearly','H'=>'Half Yearly','Q'=>'Quarterly');?>
                <p style="font-size:12px; color:#817679;"> Would you like to change the auto-populate frequency of this service call?</p></td><td><?php echo $form->select('',$optiondata,$data['ServiceCall']['service_call_frequency'],array('id'=>'','name'=>'data[ServiceCall][service_call_frequency]','legend'=>false,'label'=>false,"class"=>"wdth-140","onchange"=>"",'style'=>'width:150px;')); ?> </td></tr>
          <tr><td colspan="2">
		<span style="clear:left; float:right; font-style:italic; font-size:10px; padding-top:5px; color:#3D94DC;"> System will auto-populate the inspection schedule before 10 days from the selected interval</span>
      </td>
       </tr>
         <tr>            
         <td colspan="2">
         <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
         </td>
       </tr>
	    </table>
            <?php echo $form->end(); ?>
        
    
    
    
      </div>
</div>