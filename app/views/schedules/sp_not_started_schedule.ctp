<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));    
    
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<?php echo $this->Html->css(array('tooltip')); ?>
<?php echo $this->Html->script(array('tooltip')); ?>
<style>
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#3E4061}
.normal{
    background-color: #e8e8e8;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
  	text-align: center;
  	font-weight:bold;
}
.normal td {
    background-color: #e8e8e8;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
  	text-align: center;
  	font-weight:bold;
}
.highlight td:hover { background:#fff; }
.highlight td {
    background-color: #e8e8e8;
    border: 1px solid #c6c6c6;
    color: #000000;
    cursor: pointer;
    font-size: 11px;
    text-align: center;
	  font-weight:bold;
}

#calbtn {background:url("../../../img/newdesign_img/nav_bg.png") 0 -7px #808080; }
#calbtn p { color:#fff}
.intp{font-size:9px;color:#2B92DD;}
#schw{height:auto; text-align:left; width:100%; margin:2px;border:1px solid #FFF; color:#000;}
.viewmode {font-weight:bold; text-align:left; font-size:14px; color:grey; float:left; margin-right:10px;}

.butn {float:right;padding-right:20px;margin-bottom:10px;}
.butn a { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
.butn a:hover { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<script type="text/javascript">
			$(function(){
				//Accordion
			    $("#accordion").accordion({ header: "h3",active:false,collapsible: true});
				$("#accordionClient").accordion({ header: "h3",active:false,collapsible: true});
			    jQuery(".ajax").colorbox();
			    $('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); },
					function() { $(this).removeClass('ui-state-hover'); }
			    );
			
				$('.boxtd').hover(
					function(){
						$(this).find('.createcall').show();
					},
					function(){
						$(this).find('.createcall').hide();
					});
				var viewType = '<?php echo @$this->params['named']['view']?>';
				if(viewType!="" && viewType!=undefined){
				    $("#accordingView").show();
				    $("#clientView").hide();
				    $("#calendarView").hide();
				}
				
				});

function switchView(id,parameter)
{
    
	
	if(id=='accordingView')
	{
	    if(parameter=='cl'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Client View');
	    }
	    if(parameter=='calender'){
		  
		$("#calendarView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#accordingView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Calendar View');
	    }
		
		
	}
	if(id=='clientView'){
	    if(parameter=='c'){
		$("#calendarView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#clientView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Calendar View');
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Accordian View');
	    }
	    
	}
	else
	{
	    if(parameter=='c'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Client View');
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Accordian View');
	    }
		//$("#calendarView").slideUp(500);
		//$("#accordingView").slideDown(500);
	}
}
 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
		</script>
    <style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			div.linkContainer{ width: 100%;}
			
    </style>
<!--<div class="linkContainer">-->
    <!--<div class="butn">
	<?php //echo $html->link('Pending Service Calls',array('controller'=>'schedules','action'=>'pendingSchedule'));?>	
    </div>-->
    <!--<div class="butn">
	<?php //echo $html->link('Response Service Calls',array('controller'=>'sps','action'=>'responseSchedule'));?>	
    </div>-->
<!--</div>-->
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
    <div style="float:right;margin-top:7px;"></div>    	
    <div id="box" >	    
        <h3 id="adduser">Not Started Schedule as Service Calls</h3>
        <br/>		
	<!-- AccordianView div ID Start-->
	<div id="accordingView">		
            <table class="tbl">                        
		<tr>
                    <td width="90%" align="left">
			<div id="accordion">
			<?php if(!empty($data))
			{
                            $chkService=false;
                            $s=0;
                            foreach($data as $key=>$res):
                            $chkService=$this->Common->checkNotStartedPastServiceCall($res['ServiceCall']['id']);
                            if($chkService){$s++;
                        ?>
                                <div>				
                                    <h3><a href="#" style="color: #000 !important;"><?php echo 'Client Name : '.$this->Common->getClientCompanyName($res['ServiceCall']['client_id']);?><span style="color:grey; font-size:10px;"> [Site Name-<?php echo $this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']);?>]</span><span style="color:#000; font-size:10px;"> [Call Name-<?php  echo $res['ServiceCall']['name'];?>]</span><span style="color:grey; font-size:10px;"> [Sch. requested-<?php echo date('m/d/Y',strtotime($res['ServiceCall']['created']))?>]</span></a></h3>
				    <table class="inner" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="6" style="background:url('../../../img/newdesign_img/nav_active.png'); color:#FFF; font-size:14px;"><strong><?php echo $this->Common->getClientName($res['ServiceCall']['client_id']);?> (Company:<?php echo $this->Common->getClientCompanyName($res['ServiceCall']['client_id']);?>)</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" style="background-color:#fff;color:#817679;font-size:14px;"><strong><?php echo 'Site Address : '.$this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']);?></strong></td>
                                        </tr>
                                        <tr style="background:url('../../../img/newdesign_img/nav_bg.png') bottom;">
                                            <th width="15%" align="center" style="color:#FFF">Report</th>
                                            <th width="10%" align="center" style="color:#FFF">Lead Inspector</th>
                                            <th width="18%" align="center" style="color:#FFF">Helper</th>
                                            <th width="17%" align="center" style="color:#FFF">Sch Date(M/D/Y)</th>
                                            <th width="15%" align="center" style="color:#FFF">Submission Days left</th>
                                            <th width="10%" align="center" style="color:#FFF">Status</th>					    
                                        </tr>				
                                        <?php foreach($res['Schedule'] as $key1=>$schedule):?>
                                        <?php
                                            $remDays = $this->Common->getRemainingDays($schedule['id']);
                                            $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
                                            $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);                                        
                                            if($getstarted == 0 && $getCompleted == ""){
                                        ?>
                                        <tr style="background-color:#eaeaea;">
                                            <td align="center" style="font-size: 16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
                                            <td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
                                            <td align="center"><?php $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
                                            echo $this->Common->getHelperName($helper_ins_arr); ?></td>
                                            <td align="center">
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:i a', $schedule['schedule_from_1']).'-'.$time->format('g:i a', $schedule['schedule_to_1']); ?>
                                                <br/>
                                                    <?php if(!empty($schedule['schedule_date_2'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:i a', $schedule['schedule_from_2']).'-'.$time->format('g:i a', $schedule['schedule_to_2'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_3'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:i a', $schedule['schedule_from_3']).'-'.$time->format('g:i a', $schedule['schedule_to_3'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_4'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:i a', $schedule['schedule_from_4']).'-'.$time->format('g:i a', $schedule['schedule_to_4'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_5'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:i a', $schedule['schedule_from_5']).'-'.$time->format('g:i a', $schedule['schedule_to_5'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_6'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:i a', $schedule['schedule_from_6']).'-'.$time->format('g:i a', $schedule['schedule_to_6'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_7'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:i a', $schedule['schedule_from_7']).'-'.$time->format('g:i a', $schedule['schedule_to_7'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_8'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:i a', $schedule['schedule_from_8']).'-'.$time->format('g:i a', $schedule['schedule_to_8'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_9'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:i a', $schedule['schedule_from_9']).'-'.$time->format('g:i a', $schedule['schedule_to_9'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_10'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:i a', $schedule['schedule_from_10']).'-'.$time->format('g:i a', $schedule['schedule_to_10'])?>
                                                    <?php } ?>
                                            </td>                                            
                                            <td align="center"><?php echo $remDays;?><?php if($getstarted == 0 && $getCompleted == "" && $remDays=='Submission date expired'){echo ' ('.$html->link('Edit',array('controller'=>'schedules','action'=>'changeSchedule',$schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']),array('escape'=>false,'title'=>'Change Schedule','class'=>'ajax','style'=>'color:red')).')';}?></td>
                                            <td align='center'><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
                                                <?php                                                      
                                                     if($getCompleted != "" && $getstarted == 1){                                                        
                                                        echo $html->image('fineimages/finished.png');
                                                      ?>                                                       
                                                     <?php
                                                        }
                                                     if($getstarted == 0 && $getCompleted == ""){                                                       
                                                        echo $html->image('fineimages/not_started.png');
                                                      }
                                                      else if($getstarted == 1 && $getCompleted == ""){                                                        
                                                        echo $html->image('fineimages/pending.png');
                                                      }
                                                ?>
                                            </td>						    
                                        </tr>
					<tr><td colspan="6" align="left">
                                                <div class="service-hdline">Inspection Devices</div>
                                                <ul class="services-tkn"><li>
                                                <?php $i=0;$j=1;
                                                for($i=0;$i<count($schedule['ScheduleService']);$i++)
                                                {
                                                        echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
                                                <?php if($j%4==0){
                                                        echo '</li></ul><ul class="services-tkn"><li>';
                                                }
                                                else{
                                                        echo '</li><li>';
                                                }
                                                 $j++;
                                                }
                                                ?>		
					</td></tr>					    
                                        <?php
                                            $certs = $this->Common->getAllSchCerts($schedule['id']);						
                                            if(sizeof($certs)>0){
                                        ?>
					    <tr><td colspan="6">
					    <div class="service-hdline">Inspection Certificates for the Inspector</div>
					    <ul class="services-tkn"><li>
					    <?php $cr=1;
					    foreach($certs as $cert)
					    {
						    echo $html->image('tick.png');?><?php echo $html->link($cr.'.'.$cert['SpBlankCertForm']['title'],array("controller"=>"messages","action"=>"download_cert_form",$cert['SpBlankCertForm']['cert_form']),array('title'=>'DownLoad/View')); ?>
					    <?php if($cr%4==0){
						    echo '</li></ul><ul class="services-tkn"><li>';
					    }
					    else{
						    echo '</li><li>';
					    }
					     $cr++;
					    }
					    ?>		
					    </td></tr>
                                        <?php						
                                        }		
                                        ?>
                                        <tr><td colspan="6"></td></tr>
                                        <?php }?>
                                        <?php endforeach;?>
				    </table>				
				</div>
                            <?php }
                                endforeach;
                                if($s==0){
                                    echo 'No Record Found';
                                }
                        }
                        else{
                            echo "No Record Found";
                        }
			?>				
                        </div>
                        <!-- Accordian div closed-->
                    </td>
                </tr>
            </table>
        </div>
        <!-- AccordianView div ID closed-->	
    </div>
</div>
<script type="text/javascript">
function redirecturl(tstamp)
{
 window.location='/sp/schedules/schedule/'+tstamp;
}
</script>