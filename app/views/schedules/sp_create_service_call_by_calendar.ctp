<?php
error_reporting(0);
    echo $this->Html->script('jquery.1.6.1.min');
    //echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->css('toolttip');
    echo $this->Html->script(array('ui/ui.core','ui/ui.datepicker','jquery-ui-1.8.20.custom.min'));
    echo $this->Html->script(array('timepicker/jquery-1.7.1.min'));
    echo $this->Html->script(array('timepicker/jquery-ui-1.8.16.custom.min','timepicker/jquery-ui-timepicker-addon','timepicker/jquery-ui-sliderAccess'));
    //echo $this->Html->script('languages/jquery.validationEngine-en.js');
    //echo $this->Html->script('jquery.validationEngine.js');
    
     /*** alert box**/
    
    echo $this->Html->css('alertbox/jquery.alerts.css');
    echo $this->Html->css('jquery/jquery.ui.draggable.js');
    echo $this->Html->script('alertbox/jquery.alert.js');
    
    $timeArray = array('12:00 am','12:30 am','01:00 am','01:30 am','02:00 am','02:30 am','03:00 am','03:30 am','04:00 am','04:30 am','05:00 am','05:30 am','06:00 am','06:30 am','07:00 am','07:30 am','08:00 am','08:30 am','09:00 am','09:30 am','10:00 am','10:30 am','11:00 am','11:30 am','12:00 pm','12:30 pm','01:00 pm','01:30 pm','02:00 pm','02:30 pm','03:00 pm','03:30 pm','04:00 pm','04:30 pm','05:00 pm','05:30 pm','06:00 pm','06:30 pm','07:00 pm','07:30 pm','08:00 pm','08:30 pm','09:00 pm','09:30 pm','10:00 pm','10:30 pm','11:00 pm','11:30 pm');
    $iniTimer = array(''=>'Select');
    foreach($timeArray as $times){
	$timesplits[$times] = $times;
    }
    $timesplits = array_merge($iniTimer,$timesplits);
?>
<script type="text/javascript">
function allitems(status){
	if(status == true){
	$("#1").click();
	$("#2").click();
	$("#3").click();
	$("#4").click();
	$("#5").click();
	$("#6").click();
		$("#ul_1").enable();
     	$("#ul_1").fadeIn(500);
        $("#ul_1").fadeIn(500); 
        $("#ul_2").enable();
     	$("#ul_2").fadeIn(500);
        $("#ul_2").fadeIn(500);
        $("#ul_3").enable();
     	$("#ul_3").fadeIn(500);
        $("#ul_3").fadeIn(500);
        $("#ul_4").enable();
     	$("#ul_4").fadeIn(500);
        $("#ul_4").fadeIn(500);
        $("#ul_5").enable();
     	$("#ul_5").fadeIn(500);
        $("#ul_5").fadeIn(500);
        $("#ul_6").enable();
     	$("#ul_6").fadeIn(500);
        $("#ul_6").fadeIn(500); 
	
}else{ 
	$("#1").click();
	$("#2").click();
	$("#3").click();
	$("#4").click();
	$("#5").click();
	$("#6").click();

		$("#ul_1").css("display", "none");
		
		$("#ul_2").css("display", "none");
     	
        $("#ul_3").css("display", "none");
     	$("#ul_4").css("display", "none");
        $("#ul_5").css("display", "none");
       
        $("#ul_6").css("display", "none");
        $(".form-box").css("display", "none");
	 }

}

function showhidehelper(obj)
{
	 var el = document.getElementById(obj);
	if ( el.style.display != 'none' ) {
		el.style.display = 'none';
	}
	else {
		el.style.display = '';
	}
}

jQuery(document).ready(function(){
disableAll();
loadDatePicker();
    jQuery('.timepic').timepicker({
	ampm: true,
	hourMin: 0,
	hourMax: 23,
	onSelect : function(){
	    setMultiTime(jQuery(this).attr('name'),jQuery(this).val());
	}
    });
    /*jQuery("#createservicecall").validationEngine();*/    
    
    
    jQuery("#createservicecall").submit(function(){
	var chkFlag=0;
	
	if(jQuery("#service_call_name").val()==""){
	    chkFlag = 1;
	    jAlert('Please enter the Service Call name');
	    jQuery("#service_call_name").focus();
	    return false;
	}
	if(jQuery("#service_client_id").val()==""){
	    chkFlag = 1;
	    jAlert('Please select the client first');
	    jQuery("#service_client_id").focus();
	    return false;
	}
	
	if(jQuery("#site_address_id").val()==""){
	    chkFlag = 1;
	    jAlert('Please select the site address');
	    jQuery("#site_address_id").focus();
	    return false;
	}
	
	jQuery(".schFrq:visible").each(function(){ 
	    if(jQuery(this).val()==""){ 
		chkFlag = 1;
		elementId=jQuery(this).attr('id');
		lastNo = elementId.charAt(elementId.length-1); 
		systemName = setSystem(lastNo);
		jAlert('Please select Schedule Frequency for '+systemName);
		jQuery("#freqselect_"+lastNo).focus();
		return false;
	    }	    
	});
	if(chkFlag == 1){
	    return false;
	}
	
	jQuery(".l_inspector:visible").each(function(){ 
	    if(jQuery(this).val()==""){ 
		chkFlag = 1;
		elementId=jQuery(this).attr('id');
		lastNo = elementId.charAt(elementId.length-1); 
		systemName = setSystem(lastNo);
		jAlert('Please select the lead inspector for '+systemName);
		jQuery("#selectinspector_"+lastNo).focus();
		return false;
	    }	    
	});
	if(chkFlag == 1){
	    return false;
	}
	jQuery(".calender:visible").each(function(){
	    var fieldName = jQuery(this).attr('name');
	    if(jQuery(this).val()=="" && fieldName.indexOf("schedule_date_1")!=-1){
		chkFlag = 1;		
		var elementCount = fieldName.charAt(5);
		systemName = setSystem(elementCount);
		jAlert('Please fill first Schedule date for '+systemName);
		jQuery(this).focus();		
		return false;
	    }
	});
	if(chkFlag == 1){ 
	    return false;
	}
	
	jQuery(".timepic:visible").each(function(){
	    var fieldName = jQuery(this).attr('name');
	    if(jQuery(this).val()=="" && fieldName.indexOf("schedule_from_1")!=-1){
		chkFlag = 1;		
		var elementCount = fieldName.charAt(5);
		systemName = setSystem(elementCount);
		jAlert('Please fill from time for '+systemName);
		jQuery(this).focus();		
		return false;
	    }else if(jQuery(this).val()=="" && fieldName.indexOf("schedule_to_1")!=-1){
		chkFlag = 1;		
		var elementCount = fieldName.charAt(5);
		systemName = setSystem(elementCount);
		jAlert('Please fill to time for '+systemName);
		jQuery(this).focus();		
		return false;
	    }
	});
	if(chkFlag == 1){
	    return false;
	}
	
	jQuery(".submission_calender:visible").each(function(){
	    var fieldName = jQuery(this).attr('name');
	    var elementCount = fieldName.charAt(5);
	    systemName = setSystem(elementCount);
	    if(jQuery(this).val()==""){
		chkFlag = 1;		
		jAlert('Please fill last submission date for '+systemName);
		jQuery(this).focus();		
		return false;
	    }else{ 
		scheduleDate = jQuery(this).parent('span').parent('li').parent('ul').find(".calender:visible:last").val();
		submissionDate = jQuery(this).val();
		
		var firstValue = scheduleDate.split('/');
		var secondValue = submissionDate.split('/');
		var firstDate=new Date();
		firstDate.setFullYear(firstValue[2],(firstValue[0] - 1 ),firstValue[1]);
		var secondDate=new Date();
		secondDate.setFullYear(secondValue[2],(secondValue[0] - 1 ),secondValue[1]);
		if (firstDate > secondDate){
		    chkFlag = 1;
		    jAlert('Submission date should be greater than scheduled date for '+systemName);
		    jQuery(this).focus();		
		    return false;
		}		
	    }
	});
	if(chkFlag == 1){ 
	    return false;
	}
    });
    
    jQuery(".consecutive").click(function(){
	if(jQuery(this).is(':checked')){
	    var dateField = jQuery(this).parent('li').next('li').next('li').find('input.calender');
	    var timeFromField = jQuery(this).parent('li').next('li').next('li').find('input.timepic:first');
	    var timeToField = jQuery(this).parent('li').next('li').next('li').find('input.timepic:last');
	    var dateFieldName = dateField.attr('name');
	    var dateFieldValue = dateField.val();
	    var timeFromFieldName = timeFromField.attr('name');
	    var timeFromFieldValue = timeFromField.val();
	    var timeToFieldName = timeToField.attr('name');
	    var timeToFieldValue = timeToField.val();
	    if(dateFieldValue!=""){
		setMultiDate(dateFieldName,dateFieldValue);
	    }
	    if(timeFromFieldValue!=""){
		setMultiTime(timeFromFieldName,timeFromFieldValue);
	    }
	    if(timeToFieldValue!=""){
		setMultiTime(timeToFieldName,timeToFieldValue);
	    }
	}
    });
    
 });

 function setSystem(lastPart){
    switch(lastPart){
		case "1":
		    var systemName = 'Fire Alarm';
		    break;
		case "2":
		    var systemName = 'Sprinkler/Suppresion';
		    break;
		case "3":
		    var systemName = 'Kitchen';
		    break;
		case "4":
		    var systemName = 'Emergency/Exit Lights';
		    break;
		case "5":
		    var systemName = 'Extinguishers';
		    break;
		case "6":
		    var systemName = 'Special Hazard';
		    break;
		default:
		    var systemName = '';
		    break;
	    }
	    return systemName;
}
    /* Function to change option values of select box on the basis of selected value of other select*/
  function changeoptions(selectedValue){
  jQuery("#changevalue").html("<option value=''>Select</option>");
  	jQuery.ajax({
                type : "GET",
                	
                url  : "/sps/getselectarray/" + selectedValue,
                success : function(opt){ 
                if(selectedValue != 'No'){ 
                 $("#showselect").show(); 
                 }else{
                 $("#showselect").hide(); 
                 }             
			           $('#changevalue').html(opt);
			          
                }
        });
    
  } 


function loadDatePicker()
{
   jQuery(".calender").datepicker({
	dateFormat: 'mm/dd/yy',
	showOn: 'button',
	minDate:1 ,
	buttonImage: '/img/calendar.gif',
	buttonImageOnly: true,
	numberOfMonths: 3,
	onSelect: function(){
	    setMultiDate(jQuery(this).attr('name'),jQuery(this).val());
	    chkInspectorVacation(this,'calDate');
	}
    });
   
   jQuery(".submission_calender").datepicker({
	dateFormat: 'mm/dd/yy',
	showOn: 'button',
	minDate:1 ,
	buttonImage: '/img/calendar.gif',
	buttonImageOnly: true,
	numberOfMonths: 3
    });
}

function getStates(id,value){
    var optArr = $("#"+id).val();
	jQuery("#UserStateId").html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
                url  : "/users/getByCountry/" + value,
                success : function(opt){
			jQuery('#UserStateId').html(opt);
                }
        });
}
 
 function selectHelper(value,report_id,sp_id){
   // var optArr = $("#"+id).val();
	jQuery("#helperselect_"+report_id).html("<option value=''>Select</option>");
	jQuery("#loaderimg").html('<img src="/app/webroot/img/ajax-loader.gif">');
	jQuery.ajax({
                type : "GET",
                url  : "/users/gethelper/"+value+'/'+report_id+'/'+sp_id,
                success : function(opt){
			jQuery("#loaderimg").html('');
			jQuery("#helperselect_"+report_id).html(opt);
			
                }
        });
 }
 function showAmountInput(id)
 {
     if($("#"+id).is(":checked")){
	$("#amount_"+id).fadeIn(500);
	$("#frequency_"+id).fadeIn(500);
    }
    else{
	$("#amount_"+id).fadeOut(500);
	$("#frequency_"+id).fadeIn(500);
    }
 }
 
  function majorServices(msId){
    if($("#"+msId).is(":checked")){
	$("#ul_"+msId+" *").enable();
       $("#ul_"+msId).fadeIn(500);
       $("#submit_id").fadeIn(500);
       
    }else {
	$("#ul_"+msId+" *").disable();
	$("#ul_"+msId).fadeOut(500);
	$("#submit_id").fadeOut(500);
    }     
  }
   function closebox(id)
   {
    var flag = false;
     $("#ul_"+id).fadeOut(500);
     $("#"+id).attr("checked", false);
     if(($("#1").is(":checked") == true) || ($("#2").is(":checked") == true) || ($("#3").is(":checked") == true) || ($("#4").is(":checked") == true) || ($("#5").is(":checked") == true)){
	flag = true;
     }else { flag = false; } 
    if(flag == false){
    $("#submit_id").fadeOut(500);
	
    }
   }
   function closebox2(id)
   {
     $("#exist_"+id).fadeOut(500);
      jQuery.ajax({
                type : "GET",
                url  : "/sps/deleteservicecal/"+id,
                success : function(opt){
			if(opt==1){
			jQuery("#deleteFlashmessage").fadeIn(500);
			jQuery("#deleteFlashmessage").fadeOut(5000);   
			}
			
                }
        });
   }
    function confirmbox(id)
 {
  var r = confirm("Are you sure you want to delete this service call?");
  
  if (r==true)
  {
     closebox2(id);
    // return true;
      
  }
  else
  {
    return false;
  }
 }
    function calltoggle(val)
    {
	//alert(val)
	if(val == "new")
	{
	    $("#newcallbox").show();
	    $("#existingcallbox").hide();
	}
	if(val == "existing")
	{
	    $("#existingcallbox").show();
	    $("#newcallbox").hide();
	}
    }
   
 function createDiv(value,reportId){
    if(value>1){
	jQuery("li#concurrent_"+reportId).show();
    }else{
	jQuery("li#concurrent_"+reportId).hide();
    }
  jQuery('#DivOption_'+reportId).empty();  
  for (i=1; i<value; i++)
  {
  var j=i+1;
  var OptionHtml = '<span class="input-field wdth-150">';    
    OptionHtml = OptionHtml+'<input type="text" class="calender validate[required]" value="" name="data['+reportId+'][Schedule][schedule_date_'+j+']"/>';
    OptionHtml = OptionHtml+'</span>';
    OptionHtml = OptionHtml+'<a href="#" title="calendar" class="calendar-img"></a>';
    OptionHtml = OptionHtml+'<span class="input-field wdth-120 left" style="background:none;"><select id="ScheduleChkAll" onchange="setTotimer(this);" class="validate[required]" name="data['+reportId+'][Schedule][schedule_from_'+j+']"><option value=""></option>';
    <?php foreach($timeArray as $times){?>
    OptionHtml = OptionHtml+'<option value="<?php echo $times;?>"><?php echo $times;?></option>';
    <?php }?>
    OptionHtml = OptionHtml+'</select></span>';
    OptionHtml = OptionHtml+'<span class="input-field wdth-120 left no-marr" style="background:none;"><select id="ScheduleChkAll" class="validate[required]" name="data['+reportId+'][Schedule][schedule_to_'+j+']"><option value=""></option></select></span>';               
   jQuery('#DivOption_'+reportId).append(OptionHtml); 
   loadDatePicker();
  jQuery('.timepic').timepicker({
	ampm: false,
	hourMin: 0,
	hourMax: 23,
	onSelect: function(){
	    setMultiTime(jQuery(this).attr('name'),jQuery(this).val());
	}
    });
  }
  
 }
 function checkAvailability(id){
   var inspectorid = $("#selectinspector_"+id).val();
   if(inspectorid == ''){
    jAlert('Please select a lead inspector', 'Check Availibility');
   // alert("Please select a lead inspector");
    return false;
	
   }

    jQuery.ajax({
                type : "POST",
		data: $('#createservicecall').serialize(),
                url  : "/sps/checkInspectorAvail/"+inspectorid,
                success : function(opt){		    
			//alert(opt);
            jAlert(opt);
		}
        });

 }
 
 function setMultiTime(elementName, elementValue){
    var timeFieldName = elementName;
    var selectedTime = elementValue;	    
    var timeCounter = timeFieldName.charAt(5);
    var timeLastCount = timeFieldName.charAt(timeFieldName.length-2);
   
    if(jQuery("#multibox_"+timeCounter).attr('checked') && timeLastCount==1){
	var daysCount = parseInt(jQuery("#seqselect_"+timeCounter).val()); 				
	if(daysCount>1){
	    for(fieldCount=1; fieldCount<daysCount; fieldCount++){
		if(timeFieldName.indexOf('schedule_from_')!=-1){
		    document.getElementsByName("data["+timeCounter+"][Schedule][schedule_from_"+(fieldCount+1)+"]")[0].setAttribute('value',selectedTime);
		}else{
		    document.getElementsByName("data["+timeCounter+"][Schedule][schedule_to_"+(fieldCount+1)+"]")[0].setAttribute('value',selectedTime);
		}    
	    }
	}
    }
 }
 
 function setMultiDate(elementName, elementValue){
    var fieldName = elementName;
    var selectedDate = elementValue;
    var dateParts = selectedDate.split("/");	    
    var counter = fieldName.charAt(5);
    var lastCount = fieldName.charAt(fieldName.length-2);
   
    if(jQuery("#multibox_"+counter).attr('checked') && lastCount==1){
	var daysCount = parseInt(jQuery("#seqselect_"+counter).val()); 				
	if(daysCount>1){
	    for(fieldCount=1; fieldCount<daysCount; fieldCount++){ 
		var dateLiteral = 'date'+fieldCount;
		dateLiteral = new Date(dateParts[2],dateParts[0]-1,dateParts[1]);
		//dateLiteral.setDate(dateParts[1]);
		dateLiteral.setDate(dateLiteral.getDate()+fieldCount);
		var leadMonth = ((dateLiteral.getMonth()+1)<10)?'0'+(dateLiteral.getMonth()+1):(dateLiteral.getMonth()+1);
		var leadDate = (dateLiteral.getDate()<10)?'0'+dateLiteral.getDate():dateLiteral.getDate();
		//dateToSet = dateLiteral.getMonth()+1+"/"+dateLiteral.getDate()+"/"+dateLiteral.getFullYear();
		dateToSet = leadMonth+"/"+leadDate+"/"+dateLiteral.getFullYear();
		document.getElementsByName("data["+counter+"][Schedule][schedule_date_"+(fieldCount+1)+"]")[0].setAttribute('value',dateToSet);
		
		/*Chk inspector vacation if dates are selected through consecutive days*/
		chkInspectorVacation(document.getElementsByName("data["+counter+"][Schedule][schedule_date_"+(fieldCount+1)+"]")[0],'calDate');
		/*Chk inspector vacation if dates are selected through consecutive days*/
	    }
	}
    }
 }
 jQuery.fn.center = function ($) {
  var w = jQuery(window);
  this.css({
    'display':'block',
    'top':Math.abs(((w.height() - this.outerHeight()) / 2) + w.scrollTop()),
    'left':Math.abs(((w.width() - this.outerWidth()) / 2) + w.scrollLeft())
  });
  return this;
}

function setUrl(value){
    setValue = value;    
    if(setValue==""){	
	jQuery(".siteAddressLink").attr("href","javascript:void(0);").removeAttr('target');
	return false;
    }
    jQuery.ajax({
                type : "GET",
		dataType: 'html',
                url  : "/schedules/getEncodeId/" + value,
                success : function(data){		    
		    if(data!=null){
			jQuery(".siteAddressLink").attr("href","/sp/sps/addSiteAddress/"+data);
		    }
                }
        });
 
}

function chkUrl(urlToChk){
    if(urlToChk=='javascript:void(0);'){
	alert("Please select the client first");
	return false;
    }else{
	jQuery(".siteAddressLink").attr('target','_blank');
    }
}

function getSites(id,value,siteid){	
	var winWidth = jQuery(window).width();	
	//jQuery('.loadingImage').css({'top':'400px','left':'400px','display':'block'});
	jQuery('.loadingImage').center();
	var optArr = document.getElementById(id).value;    
	jQuery('#'+siteid).html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
		dataType: 'json',
                url  : "/schedules/getSiteByClient/" + value,
                success : function(data){
		    jQuery('.loadingImage').hide();		    
		    if(data!=null){ 
			window.sites = data;
			var selectedOption = '';
			var select = $('#'+siteid);
			if(select.prop) {
			  var options = select.prop('options');
			}
			else {
			  var options = select.attr('options');
			}
			$('option', select).remove();
			 
			$.each(window.sites, function(val, text) {
			    options[options.length] = new Option(text, val);
			});
			select.val(selectedOption);
		    }	
                }
        });
}
function toggleChecked(status,obj) {    
    $(obj).parent('span').parent('p').parent('li').parent('ul').find("input:checkbox").each( function() {
	$(this).attr("checked",selecttatus);
    })
  }
  
  /*
   function: chkInspectorVacation,
   desc: To check the available inspector for service call,
   created by: Manish Kumar,
   on: Oct 27, 2012
  */
  function chkInspectorVacation(obj,chkElement){ 
    if(chkElement=='inspector'){
	insElement = jQuery(obj);
	helperIns = insElement.parent('p').parent('li').next('li').find('select');	
	dateElement = insElement.parent('p').parent('li').parent('ul').find('input.calender:first');
	allDateElement = insElement.parent('p').parent('li').parent('ul').find('input.calender');
	
	inspectorId=jQuery(obj).val();
	serviceDate = dateElement.val();
    }else{
	serviceDate=jQuery(obj).val();
	
	insElement = jQuery(obj).parent('span').parent('li').parent('ul').find('select.l_inspector:first');
	helperIns = insElement.parent('p').parent('li').next('li').find('select');
	allDateElement = jQuery(obj).parent('span').parent('li').parent('ul').find('input.calender');
	
	inspectorId = insElement.val();
    }    
    if(inspectorId=="" || serviceDate==""){
	return false;
    }
    var dataToSend = 'insId='+inspectorId+'&serveDate='+serviceDate;
    $.ajax({
	url: '/sps/checkVacations/',
	type: 'post',
	dataType: 'json',
	data: dataToSend,
	success: function(msg){
	    if(msg.fineMsg=='OK'){
		alert(msg.insName+' is on vacation leave from '+msg.startDate+' to '+msg.endDate);
		insElement.val('');
		allDateElement.val('');
		helperIns.html('<option value="">Please Select</option>');
		return false;
	    }else if(msg.fineMsg=='NA'){
		showMsg=msg.insName+' is already engaged for an inspection on '+msg.insDate+'.Do you want to continue?';
		if(!confirm(showMsg)){		    
		    //alert(msg.insName+' is already engaged for an inspection on '+msg.insDate);
		    insElement.val('');
		    /*Store the selected date in another variable*/
		    firstSelectedDate=dateElement.val();
		    /*Store the selected date in another variable*/
		    allDateElement.val('');
		    
		    /*Set again the first selected date*/
		    if(chkElement=='inspector'){
			dateElement.val(firstSelectedDate);
		    }
		    /*Set again the first selected date*/
		    helperIns.html('<option value="">Please Select</option>');
		    return false;	    
		}else{
		    return true;
		}
	    }else{
		return true;
	    }
	}
    });
  }
  /*function chkInspectorVacation ends here*/
  
  /*Set Schedule to timer on change of schedule from timer*/
  /*Created On Jan 03, 2013 By Manish Kumar*/
  function setTotimer(fromObj){
    curVal = jQuery(fromObj).val();
    if(curVal==''){
	setData = '<option value=""></option>';
	jQuery(fromObj).parent('span').next('span').children('select').html(setData);
	return false;
    }
    data = 'fromVal='+curVal;
    jQuery.ajax({
	'url':'/sp/schedules/setScheduleTime/',
	'type':'post',
	'data':data,
	'success':function(msg){
	    if(msg!='error'){
		jQuery(fromObj).parent('span').next('span').children('select').html(msg);
	    }else{
		jQuery(fromObj).val('');
	    }
	}
    });
  }
  /*Set Schedule to timer on change of schedule from timer*/
  
  function setFocusData(obj){
    if(jQuery(obj).val()=="1"){
	jQuery(obj).val('');
    }
  }
  function setBlurData(obj){
    if(jQuery(obj).val()==""){
	jQuery(obj).val('1');
    }else{
var check_id;
$(obj).parent('span').parent('p').parent('li').parent('ul').find("input:checkbox:first").each( function() {
check_id = $(this)[0].id;
    })
var tmp = check_id.split("_");
var tmp_no = tmp.pop();

    	var total_no = parseInt(jQuery("#ulr_"+tmp_no).val());
    	var old_no = parseInt(jQuery(obj).val());
    	total_no = (total_no + old_no);
    	jQuery("#ulr_"+tmp_no).val(total_no);
	boxVal = parseInt(jQuery(obj).val());
	if(boxVal>1){
	    jQuery(obj).parent('span').parent('p').prev('p').children('input:checkbox').attr('checked','checked');
	}
    }
  }
  
  
  function disableAll(){
    for(var k=1;k<=6;k++){
	$("#ul_"+k+" *").disable();
    }
  }
  /*Disable and Enable functions starts here*/
    $.fn.disable = function() {
	return this.each(function() {
	    if (typeof this.disabled != "undefined") this.disabled = true;
	});
    }
    
    $.fn.enable = function() {
	return this.each(function() {
	    if (typeof this.disabled != "undefined") this.disabled = false;
	});
    }
  /*Disable and Enable functions ends here*/
  
  function helperAvailability(helperObj){  
    /*alert(jQuery(helperObj).val());
    aaho="13,35";
    setValues=aaho.split(',');    
    jQuery(helperObj).val(setValues);*/
    
    helperElement = jQuery(helperObj);	
    dateElement = helperElement.parent('div').parent('li').parent('ul').find('input.calender:first');
    serviceDate = dateElement.val();
    allHelpers=jQuery(helperObj).val();
    helperIds=allHelpers;
    intialHelper=helperIds;
    for(k=0;k<helperIds.length;k++){
	helperInspectorId=helperIds[k];          
	if(helperInspectorId=="" || serviceDate==""){
	    return false;
	}
	var dataToSend = 'insId='+helperInspectorId+'&serveDate='+serviceDate;
	$.ajax({
	    url: '/sps/checkVacations/'+Math.random(),
	    type: 'post',
	    dataType: 'json',
	    data: dataToSend,
	    success: function(msg){
		if(msg.fineMsg=='OK'){
		    alert(msg.insName+' is on vacation leave from '+msg.startDate+' to '+msg.endDate);
		    removeArrayElement(intialHelper,msg.insId);		
		    helperElement.val(intialHelper);
		    return false;
		}else if(msg.fineMsg=='NA'){
		    showMsg=msg.insName+' is already engaged for an inspection on '+msg.insDate+'.Do you want to continue?';
		    if(!confirm(showMsg)){
			removeArrayElement(intialHelper,msg.insId);		
			helperElement.val(intialHelper);		    
			return false;	    
		    }else{
			return true;
		    }
		}else{
		    return true;
		}
	    }
	});
    }
  }
  
  function removeArrayElement(array, itemToRemove) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] === itemToRemove) {
            array.splice(i, 1);
            break;
        }
    }
}
</script>
<style>
.error-message{
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: right;
    font-weight: bold;
    position: relative;
    top: 30px;
    white-space: nowrap;
}
li.chkContainer{
    display: none;
}
input { width:250px; }
/* css for timepicker */
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {flosetTotimerat: left; margin: 0 4px;}
			.ui-accordion-content{
			    
			}
			.register-form li .wdth-140 { float:none !important}
</style>

<div class="register-wrap">
   
      <!-- 2st toggling div (For New call)-->
    <div id="newcallbox" >
    
    
    
    
    
	<h1 class="main-hdng">Schedule an inspection </h1>
    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'createServiceCallByCalendar'),'id'=>'createservicecall')); ?>
    <?php echo $this->Form->hidden('Schedule.optioncount',array('value'=>'1','id'=>'optioncount')); 
	  //echo $form->input('AssignInspector.s_id.client_id',array('type'=>'hidden','value'=>$cid));
	  //echo $form->input('ServiceCall.client_id',array('type'=>'hidden','value'=>$cid));
	  echo $form->input('ServiceCall.sp_id',array('type'=>'hidden','value'=>$sp_id));
    ?>
    
     <div style="width:100%;display:block;float:left;margin:20px;">
	<ul>
	    <li>
                <label style="float: left; width: 120px;"><b>Inspection Site ID <span class="question">?</span></b>&nbsp;</label>		
		<?php echo $form->input('ServiceCall.name',array('id'=>'service_call_name','name'=>'data[ServiceCall][name]','div'=>false,'label'=>false,"class"=>"validate[required]",'style'=>'width:250px;')); ?>		
            </li>
	     <li style="padding-top: 10px;">
                <label style="float: left; width: 120px;"><b>Select Client:</b> </label>		
		<?php echo $form->select('ServiceCall.client_id',$clientCompanyData,'',array('id'=>'service_client_id','name'=>'data[ServiceCall][client_id]','legend'=>false,'label'=>false,"class"=>"validate[required]",'style'=>'width:250px;','onchange'=>'getSites("service_client_id",this.value,"site_address_id"); setUrl(this.value);')); ?>
		(<?php echo $html->link('Click here to enter a NEW Client',array('controller'=>'sps','action'=>'addclients'),array('target'=>'_blank'));?>)
	
            </li>
        </ul>
	
    </div>
     <br/><br/>
     <?php
     if(empty($siteAddresses))
     {
        echo 'Please add the site address first for which you want to create the service call';
        echo $html->link('(Click here to enter a NEW Site Address)','javascript:void(0);',array('class'=>'siteAddressLink','onclick'=>'chkUrl(this.href);'));
     }else{
        
     
     ?>
     <div style="width:100%;display:block;float:left;margin:20px;">
	<ul>
	     <li>
                <label>Select Site Address(for which you want to schedule an inspection): </label>		
		<?php echo $form->select('ServiceCall.sp_site_address_id',null,'',array('id'=>'site_address_id','name'=>'data[ServiceCall][sp_site_address_id]','legend'=>false,'label'=>false,"class"=>"validate[required]",'style'=>'width:250px;')); ?>
		<?php echo $html->link('(Click here to enter a NEW Site Address)','javascript:void(0);',array('class'=>'siteAddressLink','onclick'=>'chkUrl(this.href);'));?>
	
            </li>
        </ul>
	
    </div>
    <div><input style="width:11px;margin-left: 30px;" type="checkbox" id="all_items" onclick="allitems(this.checked)">
    <span style="margin-left: 4px;">All items</span></div>
    
    
	<!--one complete form-->
	
    <div id ="div1" class="mini-form-wrap" style="overflow:visible !important;"><h2 class="form-top-chks">
	
	<?php foreach($rData as $rd){
	    $r_id = $rd['Report']['id'];
	?>
    	<?php echo $form->input('',array('id'=>$rd['Report']['id'],"name"=>"data[$r_id][Schedule][report_id]",'type'=>'checkbox','label'=>false,'div'=>false,'style'=>'width:10px','hidden'=>false,'value'=>$rd['Report']['id'],'onclick'=>'majorServices(this.id);')); ?><label><?php echo $rd['Report']['name']; ?></label>&nbsp;&nbsp;
	<?php } ?></h2>
        <!--one complete form-->
		<?php
		    $j=1;
		    foreach($rData as $rd){
			$report_id = $rd['Report']['id'];
			$class="class='form-box width-34'";
		?>
	    <div <?php echo $class ?> id="ul_<?php echo $rd['Report']['id'] ?>" style="display:none;">
            <h2 href="#" class="mini-form-hdr"><?php echo $rd['Report']['name']; ?><a href="javascript:void(0);" style="float:right;padding-top:5px;" onclick="closebox(this.id);" id="<?php echo $rd['Report']['id']; ?>"><?php echo $html->image('close.gif'); ?></a></h2>
            <ul class="register-form min-hght502">  
            	<h3>Services</h3>
		<?php $option = $this->Common->getServicesInspector($report_id,$sp_id);
		if($option==0)
		{
		    echo '<li>No Qualified Inspector for this service</li>';
		}
		else
		{
		?>
                <li>
                	<ul class="inner-chkbox-lst">
                    	<li>
                        <p class="innr-chk-lft"></p>
                        <p><span class="pad-l95" style="padding-left:0px;"><?php echo $form->checkbox('Schedule.chkAll',array('id'=>'chkAll_'.$j,'class'=>'doChk','onclick'=>'toggleChecked(this.checked,this);'));?>Check All</span><span class="pad-l95" style="padding-left:100px;">Quantity</span><span class="pad-l95" style="padding-left:100px;">Frequency</span></p>
                        </li>
			    <?php $i=1;
				    foreach($rd['Service'] as $data){
					    $option = $this->Common->getServicesInspector($report_id,$sp_id);
			    ?>
                        <li>
                        <p class="innr-chk-lft">
			    <?php if($option != "empty"){ ?>
			    <?php
					$data_id = $data['id'];
					echo $form->input('',array('id'=>$data['id'],"name"=>"data[$report_id][Schedule][ScheduleService][$i][service_id]",'type'=>'checkbox','label'=>false,'div'=>false,'style'=>'width:10px','hidden'=>false,'value'=>$data['id'],"onclick"=>"showAmountInput($data_id);"))."<label>".$data['name']."</label>";
					} else {
					echo  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$data['name'];
					}
			    ?>
			</p>
			<?php
				if($option != "empty"){
			 ?>
			 <p id="amount_<?php echo $data['id']; ?>" style="display:block; float:left;">
                            <span class="input-field small-fld">
				<?php echo $form->input('',array('type'=>'text',"name"=>"data[$report_id][Schedule][ScheduleService][$i][amount]",'label'=>false,'div'=>false,'hidden'=>false,'value'=>'1','onblur'=>'setBlurData(this);','onfocus'=>'setFocusData(this);')); ?>&nbsp;&nbsp;
				<?php
				    echo $form->select('',$frequency,'',array('id'=>'select_'.$report_id,'legend'=>false,'label'=>false,'empty'=>false,"name"=>"data[$report_id][Schedule][ScheduleService][$i][frequency]","class"=>"wdth-140 validate[required]",'style'=>'width:124px;'));
				?>
			    </span>
			</p>
			    <?php } ?>
                        </li>
			<?php $i++; } ?>
                    </ul>
                </li>
		
				
	
		
	Total number : <input style="text-align: center;background: #F1F6F9;
    border: none;" value="0" id="ulr_<?php echo $rd['Report']['id'] ?>" type="text" readonly="readonly">
		
    <h3 style="padding:15px 0px 5px 0;">Schedule</h3>
    <li>
	<p>Schedule Frequency</p>
	<?php $freqOption = array("5th year"=>"5th year","3rd year"=>"3rd year","Annually"=>"Annually","Semi-Annually"=>"Semi-Annually","Quarterly"=>"Quarterly","Monthly"=>"Monthly","Weekly"=>"Weekly"); ?>
	<p style="padding-left:65px"><?php echo $form->select('',$freqOption,'',array('id'=>'freqselect_'.$report_id,'name'=>"data[".$report_id."][Schedule][sch_freq]",'legend'=>false,'label'=>false,"class"=>"wdth-140 validate[required] schFrq",'style'=>'width:124px;','empty'=>"Please Select")); ?></p>
    </li>
    <li>
    <p>How Many Days?</p>
      <?php $option1 = array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'); ?>
		<p style="padding-left:81px">	<?php echo $form->select('',$option1,'1',array('id'=>'seqselect_'.$report_id,'name'=>"scheduleno",'legend'=>false,'label'=>false,"class"=>"wdth-140 validate[required]","onchange"=>"createDiv(this.value,	$report_id)",'style'=>'width:124px;','empty'=>false)); ?></p>
    </li>
    <li class="chkContainer" id="concurrent_<?php echo $report_id;?>">	
	<?php echo $this->Form->checkbox('',array('id'=>'multibox_'.$report_id,'style'=>'float:left;width:20px;','name'=>"data[$report_id][User][multiDays]",'class'=>'consecutive')); ?>
	<p>Consecutive Days</p>
    </li>
		<li style="padding-bottom:0;">
                    <p>Schedule Date<span style="font-size:10px; color:#f00;">*</span></p>
                    <p class="pad-l130">From<span style="font-size:10px; color:#f00;">*</span></p>
                    <p class="pad-l96">To<span style="font-size:10px; color:#f00;">*</span></p>
    </li>
		             
                <li>
		    <span class="input-field wdth-150">
			<input type="text" class="calender validate[required]" value="<?php echo $selectedDate ?>" name="data[<?php echo $report_id; ?>][Schedule][schedule_date_1]"/>
		    </span> 
		    <a href="#" title="calendar" class="calendar-img"><?php //echo $html->image('calendar.png'); ?></a>		    
		    <!--<span class="input-field wdth-120 left"><input type="text" readonly=true class='timepic validate[required]' value="" name="data[<?php echo $report_id; ?>][Schedule][schedule_from_1]"/></span>-->
		    <span class="input-field wdth-120 left" style="background: none;"><?php echo $form->select('',$timesplits,'',array('class'=>'validate[required]','name'=>'data['.$report_id.'][Schedule][schedule_from_1]','onchange'=>'setTotimer(this);'));?></span>
		    <!--<span class="input-field wdth-120 left no-marr"><input type="text" readonly=true class='timepic validate[required]'  value="" name="data[<?php echo $report_id; ?>][Schedule][schedule_to_1]" /></span>-->
		    <span class="input-field wdth-120 left no-marr" style="background: none;"><?php echo $form->select('',$iniTimer,'',array('class'=>'validate[required]','name'=>'data['.$report_id.'][Schedule][schedule_to_1]'));?></span>
                </li>
		
		
		<li id="DivOption_<?php echo $report_id; ?>"></li>
		
		<h3 style="padding:15px 0px 5px 0;">Qualified Inspector</h3>
		<li style="position:relative">
                    <p>Select Lead Inspector<span style="font-size:10px; color:#f00;">*</span></p>
                    <p style="padding-left:81px">
			<?php
			 $option = $this->Common->getServicesInspector($report_id,$sp_id);
			 echo $form->select('',$option,'',array('id'=>'selectinspector_'.$report_id,'name'=>"data[$report_id][Schedule][lead_inspector]",'legend'=>false,'label'=>false,'empty'=>'--Inspector--',"class"=>"l_inspector wdth-140 validate[required]","onchange"=>"selectHelper(this.value,$report_id,$sp_id); chkInspectorVacation(this,'inspector');",'style'=>'width:124px;')); ?>
			 <?php echo $html->link($html->image('manager/question.gif'),"javascript:void(0);",array('title'=>'Lead inspector in '.$rd['Report']['name'],'escape'=>false, 'class'=>'help-tip'))?>
		    </p>
                    <p class="pad-l96" id="loaderimg" style="position:absolute; right:0;"></p>
                </li>
		
		<li id="helperreqd_<?php echo $report_id;?>">	
		<?php echo $this->Form->checkbox('',array('id'=>'helperreqd_'.$report_id,'style'=>'float:left;width:20px;','name'=>"data[$report_id][User][helperreqd]",'onclick'=>"showhidehelper('helperLI_$report_id');")); ?>
		<p>Helper Required ? </p>
		</li>
		
		
		<li style="display:none;" id="helperLI_<?php echo $report_id;?>">
                    <p>Helper Inspectors</p>
                    
			<?php
			 $helperOption = array();
			 echo $this->Form->input('',array('options' =>$helperOption,'id'=>'helperselect_'.$report_id,'name'=>"data[$report_id][Schedule][helper_inspector]",'class'=>'multi-select','label' => false,'empty'=>'Please Select','style'=>'height:80px;','multiple'=>'multiple','onchange'=>'helperAvailability(this);'));
			 echo $html->link($html->image('manager/question.gif'),"javascript:void(0);",array('title'=>'Helper inspector in '.$rd['Report']['name'],'escape'=>false, 'class'=>'help-tip'))?>
		    <span style="clear:left; float:right; width:209px; font-style:italic; font-size:10px; padding-top:5px; color:#3D94DC;">Hold down the Ctrl (windows) / Command (Mac) button to select multiple options</span>
                    <p class="pad-l96"></p>
                </li>
		
		<!--<li><?php //echo $html->link($html->image('checkavail.png'),"javascript:void(0);",array('id'=>'availcheck_'.$report_id,'title'=>'Check whether the lead inspector is available for the given schedule dates','escape'=>false, 'class'=>'',"onclick"=>"checkAvailability($report_id)"))?></li>-->
		<li>
		    <p>Last date of submission the report <span style="font-size:10px; color:#f00;">*</span></p>		    
		</li>
		<li>
		    <span class="input-field wdth-150">
			<?php echo $form->input('',array('name'=>'data['.$report_id.'][Schedule][submission_date]','type'=>'text','class'=>'submission_calender','label'=>false,'div'=>false,'id'=>''));?>			
		    </span>
		</li>
		
		<?php
		    $certs = $this->Common->getAllCerts($sp_id,$report_id);
		    if(sizeof($certs)>0){
		?>
		<li>
		    <p>Certificates to be attached :</p>		    
		</li>
		<?php
			$c = 1;
			foreach($certs as $cert){
		?>
		<li>
		    <?php echo $this->Form->checkbox('',array('style'=>'float:left;width:20px;','name'=>"data[$report_id][ReportCert][$c][cert_id]",'value'=>$cert['SpBlankCertForm']['id'])); ?>
		    <p><?php echo $html->link($cert['SpBlankCertForm']['title'],array("controller"=>"messages","action"=>"download_cert_form",$cert['SpBlankCertForm']['cert_form']),array('title'=>'DownLoad/View'));?></p>
		</li>
		<?php
			$c++;}
		    }
		?>
		
		
		<?php } # END IF ?>
		
		
            </ul>
        </div>
<?php $j++; } ?>
    </div>
    <!--one complete form ends-->
    <!--one complete form-->
    <div class="form-box" style="width:90%;display:none;" id="submit_id">
	<ul>
	    <li>
          <label>&nbsp;</label>
	    <?php $radioOption = array('Yes'=>'Yes','No'=>'No');?>
	    <p style="font-size:16px; color:#A43708;"> Bypass the acceptance email to customer? <?php echo $form->radio('ServiceCall.bypass',$radioOption,array('legend'=>false,'label'=>false,"class"=>"wdth-140",'style'=>'width:20px;','value'=>'No')); ?> </p> 	    
        </li>
	     <li>
          <label>&nbsp;</label>
		        <?php $optiondata = array('No'=>'No,i do not want to auto-populate','W'=>'Weekly','M'=>'Monthly','Q'=>'Quarterly','H'=>'Half Yearly','Y'=>'Yearly');?>
          <p style="font-size:16px; color:#A43708;"> Would you like to create this service call automatically in future. <?php echo $form->select('',$optiondata,'No',array('id'=>'selectPopulateTime','name'=>'data[ServiceCall][service_call_frequency]','legend'=>false,'label'=>false,"class"=>"wdth-140","onchange"=>"changeoptions(this.value)",'style'=>'width:250px;')); ?> </p>
		      <span style="clear:left; float:right; font-style:italic; font-size:10px; padding-top:5px; color:#3D94DC;"> System will auto-populate the inspection schedule before 10 days from the selected interval</span>
		      <p>&nbsp;</p>
        </li>
        <li id="showselect" style="display:none;">
          <label>&nbsp;</label>
		        <?php $selectoption=array(); //$optiondata = array('No'=>'No,i do not want to auto-populate','W'=>'Weekly','M'=>'Monthly','Q'=>'Quarterly','H'=>'Half Yearly','Y'=>'Yearly'); ?>
          <p style="font-size:16px; color:#A43708;">How many days before the maturity of this service call you want to recieve notification<?php // echo $form->select('',$selectoption,'',array('name'=>'data[ServiceCall][service_call_frequency]','legend'=>false,'id'=>'changevalue','label'=>false,"class"=>"wdth-140","onchange"=>"",'style'=>'width:250px;')); ?>
           <?php  echo $this->Form->select('ServiceCall.reminder_day',$selectoption,'', array('id'=>'changevalue','label' => false, 'class' => 'select_bg1','style'=>'width:98px;','empty'=>'--please select--'));?> 
          </p>
          
		      <span style="clear:left; float:right; font-style:italic; font-size:10px; padding-top:5px; color:#3D94DC;"></span>
		      <p>&nbsp;</p>
        </li>
        </ul>
	<ul>
	     <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
            </li>
        </ul>
    </div>
    <!--one complete form ends-->
    
    <div style="clear:both"></div>
  
 <?php } ?>
<?php echo $form->end(); ?>     
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
  $("span.question").hover(function () {
    $(this).append('<div class="tooltip"><p>“Inspection Site ID / specific identifier” represents a Service Providers specific client identifier, billing code or a type of internal code.</p></div>');
    $(this).show(1000);
  }, function () {
    $("div.tooltip").remove();
  });
});



</script>