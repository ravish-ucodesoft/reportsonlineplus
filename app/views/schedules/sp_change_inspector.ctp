<?php echo $this->Html->css('reports'); ?>
<style>
.summary-tbl2 p {
    float: left;
    margin-bottom: 0;
    margin-left: 5px;
    width: 10%;
    font-size: 10px;
    padding-left: 7px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
.lbtxt{text-align:left;width:40%}
.input-chngs1 input {
    margin-right: 3px;
    width: 136px;
}
.lbl{
vertical-align:top;
}

</style>
<script type="text/javascript">
 function selectHelper(value,report_id,sp_id){
   // var optArr = $("#"+id).val();
	jQuery("#helperselect_"+report_id).html("<option value=''>Select</option>");
	jQuery("#loaderimg").html('<img src="/app/webroot/img/ajax-loader.gif">');
	jQuery.ajax({
                type : "GET",
                url  : "/users/gethelper/"+value+'/'+report_id+'/'+sp_id,
                success : function(opt){
			jQuery("#loaderimg").html('');
			jQuery("#helperselect_"+report_id).html(opt);
			
                }
        });
 }
 </script>
<section style="min-width:600px">
    <section class="register-wrap">
    <div class="headingtxt">Change Inspector</div>
        <div style="width:100%;float:left;padding-right:10px;">
            <?php echo $form->create('', array('type'=>'POST','controller'=>'schedules','action'=>'changeInspector','name'=>'specialhazardReport','id'=>'specialhazardReportId','inputDefaults' => array('label' => false,'div' => false))); ?>
            <?php echo $form->input('Schedule.report_id',array('type'=>'hidden','value'=>$reportId));
	  echo $form->input('Schedule.service_call_id',array('type'=>'hidden','value'=>$servicecallId)); 
    echo $form->input('Schedule.id',array('type'=>'hidden','value'=>$scheduleData['Schedule']['id']));   ?>
            <table class="tbl input-chngs1" width="100%">
		<tr>
                    <td width="40%" class="lbl">Current Lead:</td>
                    <td width="60%"><?php echo $this->Common->getClientName($scheduleData['Schedule']['lead_inspector_id']);?></td></td>
                </tr>
                <tr>
                    <td class="lbl">Current Helper:</td>
                    <td><?php
							$helper_ins_arr=explode(',',$scheduleData['Schedule']['helper_inspector_id']);
							echo $this->Common->getHelperName($helper_ins_arr); ?></td>
                </tr>
                <tr>
                    <td class="lbl">Report Name:</td>
                    <td><?php echo $this->Common->getReportName($reportId); ?></td>
                </tr>
                <tr>
                    <td class="lbl">Client Name:</td>
                    <td><?php echo $this->Common->getClientName($clientId); ?></td>
                </tr>
                <tr>
                    <td class="lbl">Status:</td>
                    <td>
                    <?php
							     //echo $schedule['report_id']."--".$res['ServiceCall']['id']."---".$res['ServiceCall']['client_id']."--".$res['ServiceCall']['sp_id'];
								  $getCompleted = $this->Common->getReportCompleted($reportId,$servicecallId,$clientId,$spId);
								  $getstarted = $this->Common->getReportStarted($reportId,$servicecallId,$clientId,$spId);
								 if($getCompleted != "" && $getstarted == 1){
								    echo "<a href=''></a>";
								     ?>
								   <?php echo "<b>Completed</b>"; ?>
								 <?php
								}
								 if($getstarted == 0 && $getCompleted == ""){
								    echo "<b>Not Started Yet</b>";
								  }
								  else if($getstarted == 1 && $getCompleted == ""){
								    echo "<b>Pending</b>";
								  }
							    ?>
                    </td>
                </tr>
                 
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
		    <td  class="lbl">Lead Inspector:</td>
		    <td>
        	<?php
			     $option = $this->Common->getServicesInspector($reportId,$spId);
			     echo $form->select('',$option,'',array('id'=>'select_'.$reportId,'name'=>"data[Schedule][lead_inspector]",'legend'=>false,'label'=>false,'empty'=>'--Inspector--',"class"=>"wdth-140 validate[required]","onchange"=>"selectHelper(this.value,$reportId,$spId)",'style'=>'width:124px;')); 
          ?>
			   <?php echo $html->link($html->image('manager/question.gif'),"javascript:void(0);",array('title'=>'Lead inspector in '.$this->Common->getReportName($reportId),'escape'=>false, 'class'=>'help-tip'))?></td>
		</tr>
		<tr>
		    <td class="lbl">Helper:</td>
		    <td>
          <?php
    			 $helperOption = array();
    			 echo $this->Form->input('',array('options' =>$helperOption,'id'=>'helperselect_'.$reportId,'name'=>"data[Schedule][helper_inspector]",'class'=>'','label' => false,'empty'=>'Please Select','style'=>'height:80px;width:124px;','multiple'=>'multiple'));
    			 echo $html->link($html->image('manager/question.gif'),"javascript:void(0);",array('title'=>'Helper inspector in '.$this->Common->getReportName($reportId),'escape'=>false, 'class'=>'help-tip'))?>
		    
        </td>
    </tr>	
    <tr><td colspan="2"><span style="clear:left; float:right; width:209px; font-style:italic; font-size:10px; padding-top:5px; color:#3D94DC;">Hold down the Ctrl (windows) / Command (Mac) button to select multiple options</span></td></tr>	
    <tr>
      <td colspan="2"><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:80px;float:right;')); ?></td>
    </tr>
	    </table>
            <?php echo $form->end(); ?>
        </div>
    </section>
</section>