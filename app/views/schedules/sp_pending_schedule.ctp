<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<style>
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#3E4061}
.normal{
    background-color: #e8e8e8;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
  	text-align: center;
  	font-weight:bold;
}
.normal td {
    background-color: #e8e8e8;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
  	text-align: center;
  	font-weight:bold;
}
.highlight td:hover { background:#fff; }
.highlight td {
    background-color: #e8e8e8;
    border: 1px solid #c6c6c6;
    color: #000000;
    cursor: pointer;
    font-size: 11px;
    text-align: center;
	  font-weight:bold;
}

#calbtn {background:url("../../../img/newdesign_img/nav_bg.png") 0 -7px #808080; }
#calbtn p { color:#fff}
.intp{font-size:9px;color:#2B92DD;}
#schw{height:80px; text-align:left; width:100%; margin:2px;border:1px solid #FFF; color:#000;}
.viewmode {font-weight:bold; text-align:left; font-size:14px; color:grey; float:left; margin-right:10px;}

.butn {float:right;padding-right:20px;margin-bottom:10px;}
.butn a { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
.butn a:hover { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>

 <style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			div.linkContainer{ width: 100%;}
			
    </style>


<script type="text/javascript">
    $(function(){
	//Accordion
			    $("#accordion").accordion({ header: "h3",active:false,collapsible: true});
				$("#accordionClient").accordion({ header: "h3",active:false,collapsible: true});
				$(".ajax").colorbox();
    });
    jQuery(document).ready(function(){
        loadDatePicker();
        jQuery('.timepic').timepicker({
            ampm: true,
            hourMin: 0,
            hourMax: 23
        });
	//jQuery("#client_schedule_form").validationEngine();
    });
    function loadDatePicker()
    {
       jQuery(".calender").datepicker({
            dateFormat: 'mm/dd/yy',
            showOn: 'button',
            minDate:1 ,
            buttonImage: '/img/calendar.gif',
            buttonImageOnly: true,
	    numberOfMonths: 3
        });
    }
    function submitForm(formId){
	$("#"+formId).submit();
    }
    function cancelForm(){
	window.location.href = '<?php echo BASE_URL;?>sp/schedules/schedule';
    }
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}
    span.input-field{margin-right: 10px;}
    span input.calender{ width: 75px;}
    span input.timepic{ width: 57px;}
    div.allSpans{ float: left; width: 100%; padding-bottom: 5px;}
    div#fieldsContainer{ width: 100%; padding: 5px 0;}
    div.lblClass{ width: 300px; font-weight: bold; color: #000; font-size: 14px; padding-bottom: 5px;}
    div.txtAreaContainer{ width: 500px; font-weight: bold; color: #2B92DD; font-size: 14px;}
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box" >
        <h3 id="adduser">These are the Service Calls that are waiting for the client's approval</h3>
        <br/>
	<div class="butn" style="text-align: right; width: 99%; padding-right: 1%;">
	    <?php echo $html->link('Resend Approval Mail Manually',array('controller'=>'sps','action'=>'sendManualMail'),array('class'=>'ajax','title'=>'Resend Approval Mail Manually','escape'=>false));?>
	</div>
	<br/>
        <!-- AccordianView div ID Start-->
        <div id="accordingView">		
            <table class="tbl">		
		<tr>
                    <td width="90%" align="left">
			<div id="accordion">
			<?php if(!empty($data))
			{
			    $scheduleCount = 0;
                            foreach($data as $key=>$res):
				$scheduleCount++;
			?>
                                <div>
				
				    <?php if($res['ServiceCall']['call_status']==2){?>
				    <div id="fieldsContainer">
					<div class="lblClass">
					    Declined Reason:
					</div>
					<div class="txtAreaContainer">
					    <?php echo nl2br($res['ServiceCall']['declined_reason']);?>
					</div>
				    </div>
				    <?php }?>
					<?php
					$getNoOfCompletedReportsOfserviceCall=$this->Common->getNoOfCompletedReportsOfserviceCall($res['ServiceCall']['id']);
					$getNoOfPendingReportsOfserviceCall=$this->Common->getNoOfPendingReportsOfserviceCall($res['ServiceCall']['id']);
					$getTotalReportsOfserviceCall=$this->Common->getTotalReportsOfserviceCall($res['ServiceCall']['id']);
					$notstarted=$getTotalReportsOfserviceCall-$getNoOfCompletedReportsOfserviceCall-$getNoOfPendingReportsOfserviceCall;
					?>
				    <!--<h3><a href="#"><?php //echo 'Name : '.$res['ServiceCall']['name'].' <span style="color:#EA7060">[Service Call Date : '.date('m/d/Y H:i:s a',strtotime($res['ServiceCall']['created'])).']</span>'; ?></a></h3>-->
				    <h3><a href="#" style="color: #000 !important;"><?php echo 'Client Name : '.$this->Common->getClientCompanyName($res['ServiceCall']['client_id']).'<span style="color:grey"> [Site Name- '.$this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']).']</span><span style="color:#000"> [Call Name- '.$res['ServiceCall']['name'].']</span><span style="color:grey"> [Sch. requested- '.date('m/d/Y',strtotime($res['ServiceCall']['created'])).']</span>' ?></a></h3>
				    <?php echo $form->create('Schedule',array('id'=>'scheduleForm_'.$scheduleCount,'url'=>array('controller'=>'schedules','action'=>'resubmitServiceCall',base64_encode($res['ServiceCall']['id'])),'inputDefaults'=>array('label'=>false,'div'=>false)));?>
					 <div class="butn"><a href="/sp/schedules/resendapprovalemail/<?php echo base64_encode($res['ServiceCall']['id'])?>">Resend Approval Mail</a></div>
					 <div class="butn"><a href="/sp/schedules/editServiceCall/<?php echo base64_encode($res['ServiceCall']['id'])?>">Edit Service Call</a></div>
					 <div class="butn"><a href="/sp/schedules/deleteServiceCall/<?php echo base64_encode($res['ServiceCall']['id'])?>" onclick="return window.confirm('Are you sure to delete this service call ?');">Delete This Service Call</a></div>
					 <div class="butn"><?php echo $html->link('Set Reminder',array('controller'=>'sps','action'=>'setReminderMail',base64_encode($res['ServiceCall']['id'])),array('class'=>'ajax'));?></div>
					 <table class="inner" cellpadding="0" cellspacing="0">
					
					<tr>
					    <td colspan="6" style="background-color:#fff;color:#817679;font-size:14px;"><strong><?php echo 'Site Address : '.$this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']);?></strong></td>
					</tr>
					<tr style="background:url('../../img/newdesign_img/nav_bg.png') bottom;">
					    <th width="15%" align="center" style="color:#FFF">Report</th>
					    <th width="15%" align="center" style="color:#FFF">Lead Inspector</th>
					    <th width="20%" align="center" style="color:#FFF">Helper</th>
					    <th width="20%" align="center" style="color:#FFF">Sch Date(M/D/Y)</th>
					    <?php if($res['ServiceCall']['call_status']==2){?>
					    <th width="20%" align="center" style="color:#FFF">Client's Sch Date(M/D/Y)</th>
					    <?php }?>
					    <!--<th width="10%" style="background-color:#2B92DD;color:#FFF">Status</th>
					    <th width="20%" style="background-color:#2B92DD;color:#FFF">&nbsp;</th>-->
					</tr>				
					<?php
					$clientScheduleCounter = 0;					
					foreach($res['Schedule'] as $key1=>$schedule):
					?>
					<tr style="background-color:#eaeaea;">
                                            <td align="center" style="font-size:16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
                                            <td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
                                            <td align="center"><?php $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
                                            echo $this->Common->getHelperName($helper_ins_arr); ?></td>
                                            <td align="center">
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:i a', $schedule['schedule_from_1']).'-'.$time->format('g:i a', $schedule['schedule_to_1']); ?>
                                                <br/>
                                                    <?php if(!empty($schedule['schedule_date_2'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:i a', $schedule['schedule_from_2']).'-'.$time->format('g:i a', $schedule['schedule_to_2'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_3'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:i a', $schedule['schedule_from_3']).'-'.$time->format('g:i a', $schedule['schedule_to_3'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_4'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:i a', $schedule['schedule_from_4']).'-'.$time->format('g:i a', $schedule['schedule_to_4'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_5'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:i a', $schedule['schedule_from_5']).'-'.$time->format('g:i a', $schedule['schedule_to_5'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_6'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:i a', $schedule['schedule_from_6']).'-'.$time->format('g:i a', $schedule['schedule_to_6'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_7'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:i a', $schedule['schedule_from_7']).'-'.$time->format('g:i a', $schedule['schedule_to_7'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_8'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:i a', $schedule['schedule_from_8']).'-'.$time->format('g:i a', $schedule['schedule_to_8'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_9'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:i a', $schedule['schedule_from_9']).'-'.$time->format('g:i a', $schedule['schedule_to_9'])?><br/>
                                                    <?php } ?>
                                                    <?php if(!empty($schedule['schedule_date_10'])) { ?>
                                                    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:i a', $schedule['schedule_from_10']).'-'.$time->format('g:i a', $schedule['schedule_to_10'])?>
                                                    <?php } ?>
                                            </td>
					    
					    <?php
						if($res['ServiceCall']['call_status']==2){
						    $clientSchedule = $res['ClientSchedule'][$clientScheduleCounter];
					    ?>
					    <td align="center">
						    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.id',array('type'=>'hidden','value'=>$schedule['id']));?>					    
						    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_1',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_1']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_1',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_1'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_1',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_1'])));?>							    
							</span>
						    </div>
                                                    <?php if(!empty($clientSchedule['schedule_date_2'])) { ?>
						    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_2',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_2']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_2',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_2'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_2',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_2'])));?>							    
							</span>
						    </div>                                                    
                                                    <?php } ?>
                                                    <?php if(!empty($clientSchedule['schedule_date_3'])) { ?>
                                                    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_3',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_3']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_3',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_3'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_3',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_3'])));?>							    
							</span>
						    </div>
                                                    <?php } ?>
                                                    <?php if(!empty($clientSchedule['schedule_date_4'])) { ?>
                                                    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_4',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_4']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_4',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_4'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_4',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_4'])));?>
							</span>
						    </div>
                                                    <?php } ?>
                                                    <?php if(!empty($clientSchedule['schedule_date_5'])) { ?>
                                                    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_5',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_5']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_5',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_5'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_5',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_5'])));?>
							</span>
						    </div>
                                                    <?php } ?>
                                                    <?php if(!empty($clientSchedule['schedule_date_6'])) { ?>
                                                    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_6',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_6']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_6',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_6'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_6',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_6'])));?>
							</span>
						    </div>
                                                    <?php } ?>
                                                    <?php if(!empty($clientSchedule['schedule_date_7'])) { ?>
                                                    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_7',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_7']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_7',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_7'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_7',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_7'])));?>
							</span>
						    </div>
                                                    <?php } ?>
                                                    <?php if(!empty($clientSchedule['schedule_date_8'])) { ?>
                                                    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_8',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_8']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_8',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_8'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_8',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_8'])));?>
							</span>
						    </div>
                                                    <?php } ?>
                                                    <?php if(!empty($clientSchedule['schedule_date_9'])) { ?>
                                                    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_9',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_9']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_9',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_9'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_9',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_9'])));?>
							</span>
						    </div>
                                                    <?php } ?>
                                                    <?php if(!empty($clientSchedule['schedule_date_10'])) { ?>
                                                    <div class="allSpans">
							<span class="input-field wdth-150" style="width: 100px; float: left;">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_date_10',array('type'=>'text','class'=>'calender validate[required]','value'=>date('m/d/Y',strtotime($clientSchedule['schedule_date_10']))));?>
							</span> 							
							<span class="input-field wdth-120 left">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_from_10',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_from_10'])));?>
							</span>
							<span class="input-field wdth-120 left no-marr">
							    <?php echo $form->input('Schedule.'.$clientScheduleCounter.'.schedule_to_10',array('readonly'=>true,'class'=>'timepic validate[required]','value'=>$time->format('g:i a', $clientSchedule['schedule_to_10'])));?>
							</span>
						    </div>
                                                    <?php } ?>
                                            </td>
					    <?php }?>
                                            
					</tr>
					<tr>
                                            <td colspan="5" align="left">
                                                <div class="service-hdline">Inspection Devices</div>
                                                <ul class="services-tkn"><li>
                                                <?php $i=0;$j=1;
                                                for($i=0;$i<count($schedule['ScheduleService']);$i++)
                                                {
                                                        echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
                                                <?php if($j%4==0){
                                                        echo '</li></ul><ul class="services-tkn"><li>';
                                                }
                                                else{
                                                        echo '</li><li>';
                                                }
                                                 $j++;
                                                }
                                                ?>		
					    </td>
                                        </tr>
					
					<?php
					    $certs = $this->Common->getAllSchCerts($schedule['id']);						
					    if(sizeof($certs)>0){
					?>
					<tr><td colspan="5" align="left">
					<div class="service-hdline">Inspection Certificates for the Inspector</div>
					<ul class="services-tkn"><li>
					<?php $cr=1;
					foreach($certs as $cert)
					{
						echo $html->image('tick.png');?><?php echo $html->link($cr.'.'.$cert['SpBlankCertForm']['title'],array("controller"=>"messages","action"=>"download_cert_form",$cert['SpBlankCertForm']['cert_form']),array('title'=>'DownLoad/View')); ?>
					<?php if($cr%4==0){
						echo '</li></ul><ul class="services-tkn"><li>';
					}
					else{
						echo '</li><li>';
					}
					 $cr++;
					}
					?>		
					</td></tr>
					<?php						
					}		
					?>
					<tr>
					    <td colspan="5"></td>
					</tr>
                                    <?php				    
				    $clientScheduleCounter++;
				    endforeach;
				    ?>
				    <?php if($res['ServiceCall']['call_status']==2){?>
				    <tr>
					<td colspan="5">							
					    <div class="butn">
						<?php echo $html->link('Submit','javascript:void(0);', array('onclick'=>'submitForm("scheduleForm_'.$scheduleCount.'");'));?>
						<?php echo $html->link('Cancel','javascript:void(0);', array('onclick'=>'cancelForm();')); ?>
					    </div>							
					</td>
				    </tr>
				    <?php }?>
				    
				</table>
					 <?php echo $form->end();?>
         </div>
			<?php endforeach;
			}else{
                echo "No Record Found";
			}
			?>				
                        </div> <!-- Accordian div closed-->
                    </td>
                </tr>
            </table>
        </div> <!-- AccordianView div ID closed-->		
    </div>
</div>