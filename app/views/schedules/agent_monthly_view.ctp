<?php echo $html->css('calendar/jquery.fancybox-1.3.2'); ?>
<?php echo $html->script('jquery/jquery.fancybox-1.3.2'); ?>
	
<script type="text/javascript">
	window.onload = function(){
	    $("a.emp").fancybox();
      };
function confirmbox()
  {
     var r = confirm("Are you sure you want to delete this schedule ?");
  if (r==true)
  {
     return true;
      
  }
  else
  {
    return false;
  }
 }
</script>

<div id="content" >
     <div id="box">
        <!--<h3>Monthly Schedule View<span style="float:right;padding-right:10px;"><?php echo $html->link($html->image('build_button.png'),array('controller'=>'schedules','action'=>'weekly_view'),array('escape'=>false));?>&nbsp;&nbsp;&nbsp;<a onclick="printcontect()" href="javascript:void(0)">Print</a></span></h3>-->
        <table width="100%" cellpadding='0' cellspacing='0' border='0'>          
          <tr>
          <td align="left" style="padding-left:10px;">
	  <?php echo $html->link('<strong>Monthly View</strong>',array('controller'=>'schedules','action'=>'monthly_view'),array('alt'=>'Monthly View','title'=>'Monthly View','escape'=>false)); ?></td>
        </td>
        </tr>
        </table>
        <table width="100%" cellpadding='0' cellspacing='0' border='0'>
          <tr><td>          
          <?php echo $calendar->calmenumonth($tstamp); ?>            
          </td></tr>
       </table>
<?php $first_arr=array('Jan'=>'Jan','Mar'=>'Mar','May'=>'May','July'=>'July','Aug'=>'Aug','Oct'=>'Oct','Dec'=>'Dec');
 if(in_array(date('M',$tstamp),$first_arr)) {
                  $val=31;
                  $pad='4';
                  }else{
                  $val=30;
                  $pad='5';
                  }     
?>        
          
       <div style="width:99%;overflow: auto;">   
          <table width="100%" cellspacing="0" cellpadding='0'>
						<!--<thead> 
							<tr>
                  
                  <?php 
                  for($i=1;$i<=$val;$i++){ 
                      if($i<10){ $i='0'.$i;}
                  
                  ?>                  
                      <th style="width:100px"><?php echo $i.' '.date('M ',$tstamp); ?></th>
                       <?php
                  if($i%7==0){
                  ?>
                  <?php } } ?>                  
              </tr>
						</thead>-->
	    <tbody>
	     <tr>  
		  <?php for($i=1;$i<=$val;$i++){ 
                  if($i<10) {  $i='0'.$i; }
                  ?>                  
                  <td valign="top">
                  <div style='width:100%; text-align: center; margin:2px 0 10px 0; border: 1px solid #E8E7E1; background: #F7F6F0'><b><?php echo $i.' '.date('M ',$tstamp); ?></b></div>
                  <?php 
                    if(count($alldata)>0) {
                     foreach($alldata as $ds) : ?>                 
                      <?php               
                       $dateM=date('m',$tstamp);
                       $dateY=date('Y',$tstamp);
                       $dateD=$i;    
                       $shift_date=$dateY.'-'.$dateM.'-'.$dateD;
                       if($shift_date==$ds['Schedule']['schedule_date'])
                       { ?> 
                       <div id='schw' style="height:80px;text-align:center;">
                       <p style="color:#8090A7"><strong><?php echo $ds['Schedule']['schedule_time'].'-'.$ds['Schedule']['format']; ?></strong></p>
                       <p style="color:#000"><strong>
		       <?php echo ucwords($ds['User']['fname']).' '.ucwords(substr($ds['User']['lname'],0,1)); ?>
		       <p><?php echo $html->link($html->image('/img/deleteicon.jpg'),array('controller'=>'schedules','action'=>'delschedule',$ds['Schedule']['id']),array('escape'=>false,"onclick"=>"return confirmbox();")); ?></p>
                        </div>     
                       <?php
                       }                              
                   ?>
                   <?php  endforeach; } ?>
                   <a  class='emp' href='/agent/schedules/add/<?php echo date('Y',$tstamp).'-'.date('m',$tstamp).'-'.$i; ?>'><div style='height:100px;'>&nbsp;</div></a>
                  </td>
                  <?php if($i%7==0){
                  ?>
                  </tr><tr>
                  <?php
                  }
                   } ?>
                  <td colspan='<?php echo $pad;?>'></td>
            </tr>
            </tbody>
	    </table>
         </div>			
        </div>       
</div>
<script>
function redirecturl(tstamp)
{
 window.location='/agent/schedules/monthly_view/'+tstamp;
}
</script>