<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));    
    
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<?php echo $this->Html->css(array('tooltip')); ?>
<?php echo $this->Html->script(array('tooltip')); ?>
<style>
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#3E4061}
.normal{
    background-color: #e8e8e8;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
  	text-align: center;
  	font-weight:bold;
}
.normal td {
    background-color: #e8e8e8;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
  	text-align: center;
  	font-weight:bold;
}
.highlight td:hover { background:#fff; }
.highlight td {
    background-color: #e8e8e8;
    border: 1px solid #c6c6c6;
    color: #000000;
    cursor: pointer;
    font-size: 11px;
    text-align: center;
	  font-weight:bold;
}

#calbtn {background:url("../../../img/newdesign_img/nav_bg.png") 0 -7px #808080; }
#calbtn p { color:#fff}
.intp{font-size:9px;color:#2B92DD;}
#schw{height:auto; text-align:left; width:100%; margin:2px;border:1px solid #FFF; color:#000;}
.viewmode {font-weight:bold; text-align:left; font-size:14px; color:grey; float:left; margin-right:10px;}

.butn {float:right;padding-right:20px;margin-bottom:10px;}
.butn a { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
.butn a:hover { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<script type="text/javascript">
			$(function(){
				//Accordion
			    $("#accordion").accordion({ header: "h3",active:false,collapsible: true});
				$("#accordionClient").accordion({ header: "h3",active:false,collapsible: true});
			    jQuery(".ajax").colorbox();
			    $('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); },
					function() { $(this).removeClass('ui-state-hover'); }
			    );
			
				$('.boxtd').hover(
					function(){
						$(this).find('.createcall').show();
					},
					function(){
						$(this).find('.createcall').hide();
					});
				var viewType = '<?php echo @$this->params['named']['view']?>';
				if(viewType!="" && viewType!=undefined){
				    $("#accordingView").show();
				    $("#clientView").hide();
				    $("#calendarView").hide();
				}
				
				});

function switchView(id,parameter)
{
    
	
	if(id=='accordingView')
	{
	    if(parameter=='cl'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Client View');
	    }
	    if(parameter=='calender'){
		  
		$("#calendarView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#accordingView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Calendar View');
	    }
		
		
	}
	if(id=='clientView'){
	    if(parameter=='c'){
		$("#calendarView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#clientView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Calendar View');
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Accordian View');
	    }
	    
	}
	else
	{
	    if(parameter=='c'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Client View');
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
		$("#adduser").html('Approved Schedule as Service Calls : Accordian View');
	    }
		//$("#calendarView").slideUp(500);
		//$("#accordingView").slideDown(500);
	}
}
 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
		</script>
    <style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			div.linkContainer{ width: 100%;}
			
    </style>
<div class="linkContainer">
    <div class="butn">
	<?php echo $html->link('Pending Service Calls',array('controller'=>'schedules','action'=>'pendingSchedule'));?>	
    </div>
    <!--<div class="butn">
	<?php //echo $html->link('Response Service Calls',array('controller'=>'sps','action'=>'responseSchedule'));?>	
    </div>-->
</div>    
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
    <div style="float:right;margin-top:7px;"></div>    	
        <div id="box" >	    
        <h3 id="adduser">Approved Schedule as Service Calls : Calendar View</h3>
        <br/>
		
		<!-- clientView div ID Start-->
		<div id="clientView" style="display:none">
		    <div class="butn"><a href="javascript:void(0);" onclick="switchView('clientView','c');">Switch to Calendar View</a></div>
		    <div class="butn"><a href="javascript:void(0);" onclick="switchView('clientView','a');">Switch to Accordian View</a></div>		
		<br/>
		<table class="tbl" width="100%">                        
		<tr>
			    <td width="100%" align="left">
				<div id="accordionClient">
				<?php if(!empty($Clientdata))
				{
				    foreach($Clientdata as $key=>$resClient):?>		    
					   
				    <h3><a href="#" style="color:#000 !important;"><?php echo $this->Common->getClientName($key);?> <span style="color: grey">(Service Calls : <?php echo count($resClient);?>)</span>&nbsp;<span style="color:#000;"><i>Company:<?php echo $this->Common->getClientCompanyName($key);?></i></span></a></h3>
					<div>
					<?php  foreach($resClient as $resClient1):
					
					    $getNoOfCompletedReportsOfserviceCall=$this->Common->getNoOfCompletedReportsOfserviceCall($resClient1['ServiceCall']['id']);
					    $getNoOfPendingReportsOfserviceCall=$this->Common->getNoOfPendingReportsOfserviceCall($resClient1['ServiceCall']['id']);
					    $getTotalReportsOfserviceCall=$this->Common->getTotalReportsOfserviceCall($resClient1['ServiceCall']['id']);
					    $notstarted=$getTotalReportsOfserviceCall-$getNoOfCompletedReportsOfserviceCall-$getNoOfPendingReportsOfserviceCall;
					    $approvalReason = (($resClient1['ServiceCall']['bypass_mail']==1)?'Direct Scheduling':'Schedule set with customers Approved notification');
					    ?>
						
				    <table class="inner" cellpadding="0" cellspacing="0" width="90%" >
					    <tr>
						<td colspan="6" style="background:url('../../../img/newdesign_img/nav_active.png');color:#FFF;font-size:14px;"><strong><?php echo 'Name : '.$resClient1['ServiceCall']['name'].' <span style="font-size:10px">[Sch. requested : '.date('m/d/Y',strtotime($resClient1['ServiceCall']['created'])).']</span>'; ?> <span style="font-size:10px">[Completed-<?php  echo @$getNoOfCompletedReportsOfserviceCall;?> , Not Started Yet-<?php echo @$notstarted;?> , Pending-<?php echo @$getNoOfPendingReportsOfserviceCall;?>]</span> <span style="font-size:10px">[Approval Status : <?php echo $approvalReason;?>]</span></strong></td>
						<!--<td style="background:url('../../img/newdesign_img/nav_active.png');color:#FFF;font-size:9px;" align="right"><span style="color:#FFF;font-size:9px;">Created : <?php //echo $resClient1['ServiceCall']['created_type']?></span></td>-->
					    </tr>
					    <tr>
						<td colspan="6" style="background-color:#fff;color:#817679;font-size:14px;"><strong><?php echo 'Site Address : '.$this->Common->getSiteInfo($resClient1['ServiceCall']['sp_site_address_id']);?></strong></td>
					    </tr>
					    <tr style="background:url('../../../img/newdesign_img/nav_bg.png') bottom;">
					    <th width="15%" align="center" style="color:#FFF">Report</th>
					    <th width="15%" align="center" style="color:#FFF">Lead Inspector</th>
					    <th width="20%" align="center" style="color:#FFF">Helper</th>
					    <th width="20%" align="center" style="color:#FFF">Sch Date(M/D/Y)</th>
					    <th width="5%" align="center" style="color:#FFF">Days left</th>
					    <th width="10%" align="center" style="color:#FFF">Status</th>					    
					    </tr>
						<?php foreach($resClient1['Schedule'] as $key1=>$schedule):?>
					    <tr style="background-color:#eaeaea;">
						    <td align="center" style="font-size: 16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
						    <td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
						    <td align="center"><?php $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
						    echo $this->Common->getHelperName($helper_ins_arr); ?></td>
						    <td align="center">
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:i a', $schedule['schedule_from_1']).'-'.$time->format('g:i a', $schedule['schedule_to_1']); ?>
							<br/>
							    <?php if(!empty($schedule['schedule_date_2'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:i a', $schedule['schedule_from_2']).'-'.$time->format('g:i a', $schedule['schedule_to_2'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_3'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:i a', $schedule['schedule_from_3']).'-'.$time->format('g:i a', $schedule['schedule_to_3'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_4'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:i a', $schedule['schedule_from_4']).'-'.$time->format('g:i a', $schedule['schedule_to_4'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_5'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:i a', $schedule['schedule_from_5']).'-'.$time->format('g:i a', $schedule['schedule_to_5'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_6'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:i a', $schedule['schedule_from_6']).'-'.$time->format('g:i a', $schedule['schedule_to_6'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_7'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:i a', $schedule['schedule_from_7']).'-'.$time->format('g:i a', $schedule['schedule_to_7'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_8'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:i a', $schedule['schedule_from_8']).'-'.$time->format('g:i a', $schedule['schedule_to_8'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_9'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:i a', $schedule['schedule_from_9']).'-'.$time->format('g:i a', $schedule['schedule_to_9'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_10'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:i a', $schedule['schedule_from_10']).'-'.$time->format('g:i a', $schedule['schedule_to_10'])?>
							    <?php } ?>
							</td>
							<?php
							    $remDays = $this->Common->getRemainingDays($schedule['id']);
							    $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$resClient1['ServiceCall']['id'],$resClient1['ServiceCall']['client_id'],$resClient1['ServiceCall']['sp_id']);
							    $getstarted = $this->Common->getReportStarted($schedule['report_id'],$resClient1['ServiceCall']['id'],$resClient1['ServiceCall']['client_id'],$resClient1['ServiceCall']['sp_id']);
							?>
							<td align="center"><?php echo $remDays;?><?php if($getstarted == 0 && $getCompleted == "" && $remDays=='Submission date expired'){echo ' ('.$html->link('Edit',array('controller'=>'schedules','action'=>'changeSchedule',$schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']),array('escape'=>false,'title'=>'Change Schedule','class'=>'ajax','style'=>'color:red')).')';}?></td>
							<td align='center'><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
							<?php
							 //echo $schedule['report_id']."--".$res['ServiceCall']['id']."---".$res['ServiceCall']['client_id']."--".$res['ServiceCall']['sp_id'];
							      
							     if($getCompleted != "" && $getstarted == 1){
								echo $html->image('fineimages/finished.png');
								//echo "<a href=''></a>";
								 ?>
								<!--<a class="linktoreport" href="/sp/reports/<?php //echo $this->Common->getReportLinkName($schedule['report_id']);?>?reportID=<?php //echo $schedule['report_id']; ?>&clientID=<?php //echo $resClient1['ServiceCall']['client_id']; ?>&spID=<?php //echo $resClient1['ServiceCall']['sp_id']; ?>&serviceCallID=<?php //echo $resClient1['ServiceCall']['id']; ?>"><blink>Completed on <?php //echo date('m/d/Y',strtotime($getCompleted)); ?></blink></a>-->
							     <?php								
							    }
							     if($getstarted == 0 && $getCompleted == ""){
								//echo "<blink>Not Started Yet</blink>";
								echo $html->image('fineimages/not_started.png');
							      }
							      else if($getstarted == 1 && $getCompleted == ""){
								//echo "<blink>Pending</blink>";
								echo $html->image('fineimages/pending.png');
							      }
							?>
						    </td>						    
					    </tr>
					    <tr><td colspan="6" align="left">
					    <div class="service-hdline">Inspection Devices</div>
					    <ul class="services-tkn"><li>
					    <?php $i=0;$j=1;
					    for($i=0;$i<count($schedule['ScheduleService']);$i++)
					    {
						    echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
					    <?php if($j%4==0){
						    echo '</li></ul><ul class="services-tkn"><li>';
					    }
					    else{
						    echo '</li><li>';
					    }
					     $j++;
					    }
					    ?>
		
					    </td></tr>
					    <?php
						$certs = $this->Common->getAllSchCerts($schedule['id']);						
						if(sizeof($certs)>0){
					    ?>
					    <tr><td colspan="6" align="left">
					    <div class="service-hdline">Inspection Certificates for the Inspector</div>
					    <ul class="services-tkn"><li>
					    <?php $cr=1;
					    foreach($certs as $cert)
					    {
						    echo $html->image('tick.png');?><?php echo $html->link($cr.'.'.$cert['SpBlankCertForm']['title'],array("controller"=>"messages","action"=>"download_cert_form",$cert['SpBlankCertForm']['cert_form']),array('title'=>'DownLoad/View')); ?>
					    <?php if($cr%4==0){
						    echo '</li></ul><ul class="services-tkn"><li>';
					    }
					    else{
						    echo '</li><li>';
					    }
					     $cr++;
					    }
					    ?>		
					    </td></tr>
					    <?php						
					    }		
					    ?>
					    <tr>
						<td colspan="6"></td>
					    </tr>
					    <?php endforeach;?>
						
					    
				    </table>
					<?php endforeach; ?>
					</div>
					<?php endforeach; ?>
				<?php	
				}
				else{
					echo "No Record Found";
				}
				
				
				?>
			
		    </div> <!-- Accordian div closed for client view-->
			</td>
		</tr></table>
		
		
		</div><!-- clientView div ID end-->
		
		
		
		<!-- AccordianView div ID Start-->
		<div id="accordingView" style="display:none">
		<div class="butn"><a href="javascript:void(0);" onclick="switchView('accordingView','calender');">Switch to Calendar View</a></div>
		<div class="butn"><a href="javascript:void(0);" onclick="switchView('accordingView','cl');">Switch to Client View</a></div>
        
		<br/>
		<table class="tbl">                        
		<tr>
			    <td width="90%" align="left">
				<div id="accordion">
				<?php if(!empty($data))
				{
				    foreach($data as $key=>$res):?>
				    <div>
					    <?php
					    $getNoOfCompletedReportsOfserviceCall=$this->Common->getNoOfCompletedReportsOfserviceCall($res['ServiceCall']['id']);
					    $getNoOfPendingReportsOfserviceCall=$this->Common->getNoOfPendingReportsOfserviceCall($res['ServiceCall']['id']);
					    $getTotalReportsOfserviceCall=$this->Common->getTotalReportsOfserviceCall($res['ServiceCall']['id']);
					    $notstarted=$getTotalReportsOfserviceCall-$getNoOfCompletedReportsOfserviceCall-$getNoOfPendingReportsOfserviceCall;
					    $approvalReason = (($res['ServiceCall']['bypass_mail']==1)?'Direct Scheduling':'Schedule set with customers Approved notification');
					    ?>
				    <!--<h3><a href="#"><?php //echo 'Name : '.$res['ServiceCall']['name'];?><span style="color:purple; font-size:10px;"> [Sch. requested-<?php //echo date('m/d/Y H:i:s a',strtotime($res['ServiceCall']['created']))?>]</span><span style="color:#EA7060; font-size:10px;"> [Completed-<?php  //echo @$getNoOfCompletedReportsOfserviceCall;?> , Not Started Yet-<?php //echo @$notstarted;?> , Pending-<?php //echo @$getNoOfPendingReportsOfserviceCall;?>]</span><span style="color:green; font-size:10px;"> [Approval Date-<?php //echo date('m/d/Y H:i:s a',strtotime($res['ServiceCall']['modified']))?>]</span><span style="color:#000; font-size:10px;"> [Approval Reason-<?php //echo $approvalReason;?>]</span></a></h3>-->
				    <h3><a href="#" style="color: #000 !important;"><?php echo 'Client Name : '.$this->Common->getClientCompanyName($res['ServiceCall']['client_id']);?><span style="color:grey; font-size:10px;"> [Site Name-<?php echo $this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']);?>]</span><span style="color:#000; font-size:10px;"> [Call Name-<?php  echo $res['ServiceCall']['name'];?>]</span><span style="color:grey; font-size:10px;"> [Sch. requested-<?php echo date('m/d/Y',strtotime($res['ServiceCall']['created']))?>]</span></a></h3>
				    <table class="inner" cellpadding="0" cellspacing="0">
					    <tr>
						<td colspan="6" style="background:url('../../../img/newdesign_img/nav_active.png'); color:#FFF; font-size:14px;"><strong><?php echo $this->Common->getClientName($res['ServiceCall']['client_id']);?> (Company:<?php echo $this->Common->getClientCompanyName($res['ServiceCall']['client_id']);?>)</strong></td>
					    </tr>
					    <tr>
						<td colspan="6" style="background-color:#fff;color:#817679;font-size:14px;"><strong><?php echo 'Site Address : '.$this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']);?></strong></td>
					    </tr>
					    <tr style="background:url('../../../img/newdesign_img/nav_bg.png') bottom;">
					    <th width="15%" align="center" style="color:#FFF">Report</th>
					    <th width="10%" align="center" style="color:#FFF">Lead Inspector</th>
					    <th width="18%" align="center" style="color:#FFF">Helper</th>
					    <th width="17%" align="center" style="color:#FFF">Sch Date(M/D/Y)</th>
					    <th width="15%" align="center" style="color:#FFF">Submission Days left</th>
					    <th width="10%" align="center" style="color:#FFF">Status</th>					    
					    </tr>				
					    <?php foreach($res['Schedule'] as $key1=>$schedule):?>
					    <tr style="background-color:#eaeaea;">
						    <td align="center" style="font-size: 16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
						    <td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
						    <td align="center"><?php $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
						    echo $this->Common->getHelperName($helper_ins_arr); ?></td>
						    <td align="center">
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:i a', $schedule['schedule_from_1']).'-'.$time->format('g:i a', $schedule['schedule_to_1']); ?>
							<br/>
							    <?php if(!empty($schedule['schedule_date_2'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:i a', $schedule['schedule_from_2']).'-'.$time->format('g:i a', $schedule['schedule_to_2'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_3'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:i a', $schedule['schedule_from_3']).'-'.$time->format('g:i a', $schedule['schedule_to_3'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_4'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:i a', $schedule['schedule_from_4']).'-'.$time->format('g:i a', $schedule['schedule_to_4'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_5'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:i a', $schedule['schedule_from_5']).'-'.$time->format('g:i a', $schedule['schedule_to_5'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_6'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:i a', $schedule['schedule_from_6']).'-'.$time->format('g:i a', $schedule['schedule_to_6'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_7'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:i a', $schedule['schedule_from_7']).'-'.$time->format('g:i a', $schedule['schedule_to_7'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_8'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:i a', $schedule['schedule_from_8']).'-'.$time->format('g:i a', $schedule['schedule_to_8'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_9'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:i a', $schedule['schedule_from_9']).'-'.$time->format('g:i a', $schedule['schedule_to_9'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_10'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:i a', $schedule['schedule_from_10']).'-'.$time->format('g:i a', $schedule['schedule_to_10'])?>
							    <?php } ?>
							</td>
							<?php
							    $remDays = $this->Common->getRemainingDays($schedule['id']);
							    $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							    $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							?>
							<td align="center"><?php echo $remDays;?><?php if($getstarted == 0 && $getCompleted == "" && $remDays=='Submission date expired'){echo ' ('.$html->link('Edit',array('controller'=>'schedules','action'=>'changeSchedule',$schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']),array('escape'=>false,'title'=>'Change Schedule','class'=>'ajax','style'=>'color:red')).')';}?></td>
							<td align='center'><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
							<?php
							 //echo $schedule['report_id']."--".$res['ServiceCall']['id']."---".$res['ServiceCall']['client_id']."--".$res['ServiceCall']['sp_id'];
							      
							     if($getCompleted != "" && $getstarted == 1){
								//echo "<a href=''></a>";
								echo $html->image('fineimages/finished.png');
								 ?>
								<!--<a class="linktoreport" href="/sp/reports/<?php echo $this->Common->getReportLinkName($schedule['report_id']);?>?reportID=<?php echo $schedule['report_id']; ?>&clientID=<?php echo $res['ServiceCall']['client_id']; ?>&spID=<?php echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php echo $res['ServiceCall']['id']; ?>"><blink>Completed on <?php echo date('m/d/Y',strtotime($getCompleted)); ?></blink></a>-->
							     <?php
							    }
							     if($getstarted == 0 && $getCompleted == ""){
								//echo "<blink>Not Started Yet</blink>";
								echo $html->image('fineimages/not_started.png');
							      }
							      else if($getstarted == 1 && $getCompleted == ""){
								//echo "<blink>In Progress</blink>";
								echo $html->image('fineimages/pending.png');
							      }
							?>
						    </td>						    
					    </tr>
					    <tr><td colspan="6" align="left">
					    <div class="service-hdline">Inspection Devices</div>
					    <ul class="services-tkn"><li>
					    <?php $i=0;$j=1;
					    for($i=0;$i<count($schedule['ScheduleService']);$i++)
					    {
						    echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
					    <?php if($j%4==0){
						    echo '</li></ul><ul class="services-tkn"><li>';
					    }
					    else{
						    echo '</li><li>';
					    }
					     $j++;
					    }
					    ?>
		
					    </td></tr>
					    
					    <?php
						$certs = $this->Common->getAllSchCerts($schedule['id']);						
						if(sizeof($certs)>0){
					    ?>
					    <tr><td colspan="6">
					    <div class="service-hdline">Inspection Certificates for the Inspector</div>
					    <ul class="services-tkn"><li>
					    <?php $cr=1;
					    foreach($certs as $cert)
					    {
						    echo $html->image('tick.png');?><?php echo $html->link($cr.'.'.$cert['SpBlankCertForm']['title'],array("controller"=>"messages","action"=>"download_cert_form",$cert['SpBlankCertForm']['cert_form']),array('title'=>'DownLoad/View')); ?>
					    <?php if($cr%4==0){
						    echo '</li></ul><ul class="services-tkn"><li>';
					    }
					    else{
						    echo '</li><li>';
					    }
					     $cr++;
					    }
					    ?>		
					    </td></tr>
					    <?php						
					    }		
					    ?>
					    <tr><td colspan="6"></td></tr>
					    <?php endforeach;?>
				    </table>				
				    </div>
				    <?php endforeach;
					
				}
				else{
					echo "No Record Found";
				}
				
				
				?>
				
		    </div> <!-- Accordian div closed-->
			</td>
		</tr></table>
		</div> <!-- AccordianView div ID closed-->
		
		
		<!-- calendarView div ID closed-->
		<div id="calendarView" style="display:block">
			<div class="viewmode">You are watching Monthly View</div>
			<div class="viewmode"><a href="/sp/schedules/weekly_view">&raquo; Switch to Weekly View</a></div>
			<div class="viewmode"><a href="/sp/schedules/day_view">&raquo; Switch to Day View</a></div>
			<?php $arr=array();
			//echo count($data); //pr($data);echo '-------------';			
			foreach($data as $rec){
				foreach($rec['Schedule'] as $sch){
					unset($sch['ScheduleService']);
					$arr[] = $sch;
				}				
			}
			//pr($arr);
		
		  ?>
			<div class="butn"><a href="javascript:void(0);" onclick="switchView('calendarView','c');">Switch to Client View</a></div>
		    
			<div class="butn"><a href="javascript:void(0);" onclick="switchView('calendarView','a');">Switch to Accordian View</a></div>	
        <!--Calendar Start-->
		<table width="100%" cellpadding='0' cellspacing='0' border='0'>          
        <tr>
          <td align="center" style="padding-left:10px;color:#550707">Inspectors who are engaged in the inspection.
			</td>
		  </td>
        </tr>        
        <tr><td><?php echo $calendar->calmenumonth($tstamp); ?></td></tr>
       
		<?php $first_arr=array('Jan'=>'Jan','Mar'=>'Mar','May'=>'May','Jul'=>'Jul','Aug'=>'Aug','Oct'=>'Oct','Dec'=>'Dec');
		
		 if(in_array(date('M',$tstamp),$first_arr)) {
			
						  $val=31;
						  $pad='4';
						  }else{
						  $val=30;
						  $pad='5';
						  }     
		?>        
          
       <div style="width:99%;overflow: auto;">   
        <table width="100%" cellspacing="0" cellpadding='0'>			
	    <tbody>
	     <tr>  
		  <?php
				//$flag=false;
		  for($i=1;$i<=$val;$i++){
			$flag[$i]=0;
                  if($i<10) {  $i='0'.$i; }
                  ?>                  
                  <td valign="top" style="height:100px;" width="14%" class="boxtd">
                  <div style='width:100%; text-align: center; margin:2px 0 10px 0; border: 1px solid #EEEEEE; background: lightgrey'><b><?php echo $i.' '.date('M ',$tstamp); ?></b></div>
                  <?php
					   $dateM=date('m',$tstamp);
                       $dateY=date('Y',$tstamp);
                       $dateD=$i;    
                       $shift_date=$dateY.'-'.$dateM.'-'.$dateD;
					   //$shift_date1=$dateM.'/'.$dateD.'/'.$dateY;
                    //if(count($alldata)>0) {
                     foreach($arr as $key1=>$schedule):					                       
                       if($shift_date==$schedule['schedule_date_1'] || $shift_date==$schedule['schedule_date_2'] || $shift_date==$schedule['schedule_date_3'] || $shift_date==$schedule['schedule_date_4'] || $shift_date==$schedule['schedule_date_5'] || $shift_date==$schedule['schedule_date_6'] || $shift_date==$schedule['schedule_date_7'] || $shift_date==$schedule['schedule_date_8'] || $shift_date==$schedule['schedule_date_9'] || $shift_date==$schedule['schedule_date_10'])
                       {
						$flag[$i]=1;
						$approvalReason = (($schedule['ServiceCall']['bypass_mail']==1)?'Direct Scheduling':'Schedule set with customers Approved notification');
						$siteInfo = $this->Common->getSiteInfo($schedule['ServiceCall']['sp_site_address_id']);
						$siteInfoParts = explode(',',$siteInfo);
						$siteName = $siteInfoParts[0];
						?>
					   
                       <div id='schw' style="background-color:<?php echo $this->Common->getUserColorCode($schedule['lead_inspector_id']);?>">
			<?php
			    $txtToShow='<strong>Name:</strong>'.$schedule['ServiceCall']['name'];
			    $txtToShow.='<br/><strong>Lead:</strong>'.$this->Common->getClientName($schedule['lead_inspector_id']);
			    $txtToShow.='<br/><strong>Site Name:</strong>'.$siteName;
			    $txtToShow.='<br/><strong>Report:</strong>'.$this->Common->getReportName($schedule['report_id']);
			    $txtToShow.='<br/><strong>Client:</strong>'.$this->Common->getCompanyNameByServiceCallID($schedule['service_call_id']);
			    $txtToShow.='<br/><strong>Days left for report submission:</strong>'.$this->Common->getRemainingDays($schedule['id']);
			    $txtToShow.='<br/><strong>Approval Reason:</strong>'.$approvalReason;
			?>		   
                       <p onclick="switchView('calendarView','a');" onmouseover="tooltip.show('<?php echo $txtToShow;?>');" onmouseout="tooltip.hide();" class="intp">
		       <strong>Name:</strong><?php echo $schedule['ServiceCall']['name'];?><br/>
		       <!--<strong>Lead:</strong><?php //echo $this->Common->getClientName($schedule['lead_inspector_id']);?><br/>-->
					   <!--<strong>Helpers:</strong><?php //$helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
						//echo $this->Common->getHelperName($helper_ins_arr); ?>
						<br/>-->
						<strong>Site Name:</strong><?php echo $siteName;?>
						<br/>
						<strong>Report:</strong><?php echo $this->Common->getReportName($schedule['report_id']);?>
						<!--<br/>
						<strong>Client:</strong><a href="javascript:void(0);" onclick="switchView('accordingView','cl');"><?php echo $this->Common->getCompanyNameByServiceCallID($schedule['service_call_id']);?></a>
						<br/>
						<strong>Days left for report submission:</strong><?php //echo $this->Common->getRemainingDays($schedule['id']);?>
						<br/>
						<strong>Approval Reason:</strong><?php //echo $approvalReason;?>-->
					   </p>
					   
					   
					   
                        </div>     
                       <?php
                       }
					   
                   endforeach; // }
				  
				// if(@$flag[$i]==0) 
				 // { 
					if($shift_date>date('Y-m-d'))
					{
					?><a href="/sp/schedules/createServiceCallByCalendar/<?php echo $shift_date ?>" title="Schedule an inspection"><div id='schw'></div></a>
				   <a href="/sp/schedules/createServiceCallByCalendar/<?php echo $shift_date ?>" title="Schedule an inspection" class="createcall" style="display:none;">Schedule an inspection</a>
				   <?php
					}
				  // }
				   
				   ?>
                   
                  </td>
                  <?php if($i%7==0){
                  ?>
                  </tr><tr>
                  <?php
                  }
                   } ?>
                  <td colspan='<?php echo $pad;?>'></td>
            </tr>
            </tbody>
	    </table>
		</div>
		<br/>
		</div><!-- calendarView div ID closed-->
		
		
		
		</div>
		</div>
<script type="text/javascript">
function redirecturl(tstamp)
{
 window.location='/sp/schedules/schedule/'+tstamp;
}
</script>