<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<style>
.inspector{text-align:center;font-size:16px;color:#3E4061;}
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#3E4061}
.normal{
    background-color: #FFDC7B;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
	text-align: center;
	font-weight:bold;
}
.normal td {
    background-color: #FFDC7B;
    border: 1px solid #EEEEEE;
    color: #000000;
    font-size: 11px;
	text-align: center;
	font-weight:bold;
}
.highlight td {
    background-color: #EFFEFE;
    border: 1px solid black;
    color: #000000;
    cursor: pointer;
    font-size: 11px;
    text-align: center;
	font-weight:bold;
}
.intp{font-size:9px;color:#2B92DD;}
#schw{height:auto;text-align:left;width:98%;margin:1px;border:1px solid #FFF;color:#000;}
.viewmode {font-weight:bold;text-align:left;font-size: 14px;color:grey;}
</style>
<script type="text/javascript">
			$(function(){
				//Accordion
			    $("#accordion").accordion({ header: "h3"});
				$("#accordionClient").accordion({ header: "h3"});
			    jQuery(".ajax").colorbox();
			    $('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); },
					function() { $(this).removeClass('ui-state-hover'); }
			    );
                
                $('.boxtd').hover(
					function(){
						//alert('y');
						$(this).find('.createcall').show();
					},
					function(){
						$(this).find('.createcall').hide();
					});

			});

function switchView(id,parameter)
{
    
	
	if(id=='accordingView')
	{
	    if(parameter=='cl'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    if(parameter=='calender'){
		  
		$("#calendarView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#accordingView").slideUp(500);
	    }
		
		
	}
	if(id=='clientView'){
	    if(parameter=='c'){
		$("#calendarView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#clientView").slideUp(500);
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    
	}
	else
	{
	    if(parameter=='c'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
		//$("#calendarView").slideUp(500);
		//$("#accordingView").slideDown(500);
	}
}
 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
		</script>
    <style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			
    </style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
        <div id="box" >
        <h3 id="adduser">Weekly Schedule as Service Calls</h3>
        <br/>
		<div class="inspector" >Weekly Schedules For Inspector <strong><?php echo ucwords($inspector['User']['fname'].' '.$inspector['User']['lname']);?></strong></div>
		<!-- clientView div ID Start-->
		<div id="clientView" style="display:none">
		
		</div><!-- clientView div ID end-->
		
		
		
		<!-- AccordianView div ID Start-->
		<div id="accordingView" style="display:none">
		
		</div> <!-- AccordianView div ID closed-->
		
		
		<!-- calendarView div ID closed-->
		<div id="calendarView" style="display:block">
			<div class="viewmode">You are watching Weekly View</div>
			<div class="viewmode"><a href="/sp/schedules/inspector_schedule/<?php echo $inspector['User']['id']?>">Switch to Monthly View</a></div>
		
			
        <!--Calendar Start-->
		<table width="100%" cellpadding='0' cellspacing='0' border='0'>          
        <tr>
          <td align="center" style="padding-left:10px;color:#550707">Inspectors who are engaged in the inspection.</td>
		</tr>        
        <tr><td><?php echo $calendar->calmenu($tstamp); ?></td></tr>
        <tr><td colspan="2">
        
        <?php $arr=array();
			//echo count($data); //pr($data);echo '-------------';
			
			foreach($data as $rec){
				foreach($rec['Schedule'] as $sch){
					unset($sch['ScheduleService']);
					$arr[] = $sch;
				}				
			}
			//pr($arr);
		
		  ?>
        
          <table width="100%" >
			<thead> 
			<tr>
                  
                <th width="14%"><?php echo $calendar->getday($tstamp,1); ?></th>
                <th><?php echo $calendar->getday($tstamp,2); ?></th>
                <th><?php echo $calendar->getday($tstamp,3); ?></th>
                <th><?php echo $calendar->getday($tstamp,4); ?></th>
                <th><?php echo $calendar->getday($tstamp,5); ?></th>
                <th><?php echo $calendar->getday($tstamp,6); ?></th>
                <th><?php echo $calendar->getday($tstamp,7); ?></th>
            </tr>
			</thead>
			<tbody>
            
	     <tr>
           <?php 
           for($i=1;$i<=7;$i++){
           $dateYMD=$calendar->getdateYMD($tstamp,$i);?>
            <td valign="top" style="height:100px;" width="14%" class="boxtd" style="cursor:pointer;">
                  <div style='width:100%; text-align: center; margin:2px 0 10px 0; border: 1px solid #EEEEEE;min-height:500px; '>
                 <?php
                  foreach($arr as $key1=>$schedule):
		    $approvalReason = (($schedule['ServiceCall']['bypass_mail']==1)?'Direct Scheduling':'Schedule set with customers Approved notification');
		    $siteInfo = $this->Common->getSiteInfo($schedule['ServiceCall']['sp_site_address_id']);
		    $siteInfoParts = explode(',',$siteInfo);
		    $siteName = $siteInfoParts[0];
                    
                   
                    
                   if($dateYMD==@$schedule['schedule_date_1'] || $dateYMD==@$schedule['schedule_date_2'] || $dateYMD==@$schedule['schedule_date_3'] || $dateYMD==@$schedule['schedule_date_4'] || $dateYMD==@$schedule['schedule_date_5'] || $dateYMD==@$schedule['schedule_date_6'] || $dateYMD==@$schedule['schedule_date_7'] || $dateYMD==@$schedule['schedule_date_8'] || $dateYMD==@$schedule['schedule_date_9'] || $dateYMD==@$schedule['schedule_date_10'])
                       {
                         ?><div id='schw' style="background-color:<?php echo $this->Common->getUserColorCode($schedule['lead_inspector_id']);?>">
					   
                       <p class="intp"><strong>Name:</strong><?php echo $schedule['ServiceCall']['name'];?><br/>
						    <strong>Lead:</strong><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?><br/>						    
						    <strong>Site Name:</strong><?php echo $siteName;?>
						    <br/>
						    <strong>Report:</strong><?php echo $this->Common->getReportName($schedule['report_id']);?>
						    <br/>
						    <strong>Client:</strong><a href="javascript:void(0);" onclick="switchView('accordingView','cl');"><?php echo $this->Common->getCompanyNameByServiceCallID($schedule['service_call_id']);?></a>
						    <br/>
						    <strong>Days left for report submission:</strong><?php echo $this->Common->getRemainingDays($schedule['id']);?>
						    <br/>
						    <strong>Approval Reason:</strong><?php echo $approvalReason;?>
					   </p>
                        </div>  
                       <?php }
                       
                  endforeach;
                  
                  if($dateYMD>date('Y-m-d'))
					{
					?><a href="/sp/schedules/createServiceCallByCalendar/<?php echo $dateYMD ?>" title="Create a Service Call"><div id='schw'></div></a>
				   <a href="/sp/schedules/createServiceCallByCalendar/<?php echo $dateYMD ?>" title="Create a Service Call" class="createcall" style="display:none;">Create a service call</a>
                   <?php
					}
                  
            
           ?></td>
           <?php
           }
           ?>
            
            
         </tr>
        
            </tbody>    
          </table>
          </td></tr>
        </table>
		
       
		<br/>
		</div><!-- calendarView div ID closed-->
		
		
		
		</div>
		</div>
<script type="text/javascript">
function redirecturl(tstamp)
{
 window.location='/sp/schedules/inspector_weekly_view/<?php echo $inspector['User']['id'] ?>/'+tstamp;
}
</script>