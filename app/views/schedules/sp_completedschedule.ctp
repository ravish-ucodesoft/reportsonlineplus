<?php 
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js')); 
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<style>
.inner{
	width:100%;
	background:#fff
}
.inner td {font-size:12px;color:#3E4061; border:1px solid #ccc}
.register-form {
    border: 0px;
    overflow: hidden;
    padding: 10px;
}
.middle_img img { vertical-align:middle}
.services-tkn img { vertical-align:middle}
.butn {float:right;padding-right:20px;margin-bottom:10px;}
.butn a { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
.butn a:hover { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<script type="text/javascript">
    $(function(){
	    //Accordion
	$("#accordion").accordion({ header: "h3",active:false,collapsible: true});
	$("#accordionClient").accordion({ header: "h3",active:false,collapsible: true});
	    
	$(".ajax").colorbox();
	$('#dialog_link, ul#icons li').hover(
		    function() { $(this).addClass('ui-state-hover'); },
		    function() { $(this).removeClass('ui-state-hover'); }
	);    
    });
    /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
    function switchView(id,parameter)
    {	    
	    if(id=='accordingView')
	    {
		if(parameter=='cl'){
		    $("#clientView").slideDown(500);
		    $("#accordingView").slideUp(500);
		    $("#adduser").html('Service Calls (Completed Inspection) : Client View');
		}	    
	    }
	    if(id=='clientView'){		
		if(parameter=='a'){
		    $("#accordingView").slideDown(500);
		    $("#clientView").slideUp(500);
		    $("#calendarView").slideUp(500);
		    $("#adduser").html('Service Calls (Completed Inspection) : Accordian View');
		}		
	    }
	    else
	    {
		if(parameter=='c'){
		    $("#clientView").slideDown(500);
		    $("#accordingView").slideUp(500);
		    $("#calendarView").slideUp(500);
		    $("#adduser").html('Service Calls (Completed Inspection) : Client View');
		}
		if(parameter=='a'){
		    $("#accordingView").slideDown(500);
		    $("#clientView").slideUp(500);
		    $("#calendarView").slideUp(500);
		    $("#adduser").html('Service Calls (Completed Inspection) : Accordian View');
		}  
	    }
    }
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}    
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box" >
        <h3 id="adduser">Service Calls (Completed Inspection) : Accordian View</h3>
	<?php echo $form->create('ServiceCall',array('id'=>'searchForm','type'=>'get','url'=>array('controller'=>'schedules','action'=>'completedschedule')));?>
	    <div style="width: 100%">
		<div style="float: left; width: auto; text-align: right; padding-right: 10px; padding-top: 5px;">
		    Search for the Completed Inspection by the Client Name : 
		</div>
		<div style="float: left; width: 15%">
		    <?php			
			$selectedClient = (isset($this->params['url']['client_id']))?$this->params['url']['client_id']:"";			
		    ?>
		    <?php echo $form->select('',$clientCompanyData,$selectedClient,array('id'=>'service_client_id','name'=>'client_id','legend'=>false,'label'=>false,"class"=>"validate[required]",'style'=>'width:180px; float:left; padding:3px;')); ?>		    
		</div>
		<div style="float: left; width: 20%">
		    <p class="bttm-btns" style="float: left; padding-top: 0px;"><span class="blue-btn"><span><?php echo $form->submit('Search',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
		</div>
	    </div>
	    <?php echo $form->end();?>
	    <br/>
	<?php //echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'completedschedule'),'id'=>'search')); ?>
	    <!--<div>
		<table width="100%" cellpadding="2" cellspacing="2" border="2">
		    <tr>
			<td height="10px;">
			</td>
		    </tr>
		    <tr>
			<td>
			    <ul class="register-form">
				<li>
				    <label>Client Name</label>
				    <p>
					<span class="input-field">
					    <?php //echo $form->input('Search.cname', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?>
					</span>
				    </p>
				    <label>Client Address</label>
				    <p>
					<span class="input-field">
					    <?php //echo $form->input('Search.caddress', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?>
					</span>
				    </p>
				</li>
				<li>
				    <label>Client Phone</label>
				    <p><span class="input-field"><?php //echo $form->input('Search.cphone', array('maxLength'=>'50',"div"=>false,"label"=>false,"tabindex"=>"2")); ?></span></p>
				    <label>Company Name</label>
				    <p><span class="input-field"><?php //echo $form->input('Search.ccname', array('maxLength'=>'50',"div"=>false,"label"=>false,"tabindex"=>"2")); ?></span></p>
				</li>
				<!--<li>
				    <label>Client Inspector</label>
				    <p>
					<span class="input-field">
					 <?php //$option = $this->Common->getServicesProviderInspector($sp_id);
					       //echo $form->select('Search.inspector',$option,'',array('id'=>'selectinspector','legend'=>false,'label'=>false,'empty'=>'--Inspector--',"class"=>"wdth-140 validate[required]",'style'=>'width:244px;'));
					 ?>    
					</span>
				    </p>
				</li>-->
				<!--<li style="padding-left:30%;">
				    <label>&nbsp;</label>
				    <p class="bttm-btns"><span class="blue-btn"><span><?php //echo $form->submit('Search',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
				</li>
			    </ul>
			</td>
		    </tr>
		</table>
	    </div>-->    
	<?php //echo $form->end(); ?>			
	<br/>
	<div id="accordingView">	    
	    <div class="butn"><a href="javascript:void(0);" onclick="switchView('accordingView','cl');">Switch to Client View</a></div>
	    <br/>	    
            <table class="tbl">
		<tr>
		    <td width="70%" align="left">				
			<div id="accordion">
			    <?php if(!empty($data))
				  {
				    foreach($data as $key=>$res):
					$actualRowCounter = 0;
					$showCheck = $this->Common->checkCompleteServiceCall($res['ServiceCall']['id']);
					if($showCheck==true){
					    $actualRowCounter++;
				?>
					<div>
					    <!--<h3><a href="#"><?php //echo 'Name : '.$res['ServiceCall']['name'].'<span style="color:#EA7060"> [Sch. requested- '.date('m/d/Y H:i:s a',strtotime($res['ServiceCall']['created'])).']</span>' ?></a></h3>-->
					    <h3><a href="#" style="color: #000 !important;"><?php echo 'Client Name : '.$this->Common->getClientCompanyName($res['ServiceCall']['client_id']).'<span style="color:grey"> [Site Name- '.$this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']).']</span><span style="color:#000"> [Call Name- '.$res['ServiceCall']['name'].']</span><span style="color:grey"> [Sch. requested- '.date('m/d/Y',strtotime($res['ServiceCall']['created'])).']</span>' ?></a></h3>
					    <table class="inner" cellpadding="0" cellspacing="0">
						<tr>
						    <td colspan="6" style="background:url('../../img/newdesign_img/nav_active.png'); color:#FFF; font-size:14px;"><strong><?php echo 'Company : '.$this->Common->getClientCompanyName($res['ServiceCall']['client_id']);?></strong></td>
						</tr>
						<tr>
						    <td colspan="6" style="background-color:#fff;color:#817679;font-size:14px;"><strong><?php echo 'Site Address : '.$this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']);?></strong></td>
						</tr>
						<tr style="background:url('../../img/newdesign_img/nav_bg.png') bottom;">
						    <th width="15%" align="center" style="color:#FFF">Report</th>
						    <th width="10%" align="center"  style="color:#FFF">Lead Inspector</th>
						    <th width="10%" align="center"  style="color:#FFF">Helper</th>
						    <th width="20%" align="center"  style="color:#FFF" align="left">Sch Date(M/D/Y)</th>
						    <th style="color:#FFF" align="center"  width="10%">Status</th>
						    <th style="color:#FFF" align="center"  width="35%">Action</th>
						    <!--<th style="background-color:#2B92DD;color:#FFF" width="10%">Report Log</th>
						    <th style="background-color:#2B92DD;color:#FFF" width="10%">Send quote</th>-->
						</tr>				
						<?php
						    foreach($res['Schedule'] as $key1=>$schedule):
							$getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							if($getCompleted != ""){
						?>
						<tr style="background-color:#eaeaea;">
						    <td align="center" style="font-size: 16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
						    <td align="center" ><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
						    <td align="center" >
							<?php
							    $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
							    echo $this->Common->getHelperName($helper_ins_arr);
							?>
						    </td>
						    <td align="center" >
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:i a', $schedule['schedule_from_1']).'-'.$time->format('g:i a', $schedule['schedule_to_1']); ?><br/>
							<?php if(!empty($schedule['schedule_date_2'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:i a', $schedule['schedule_from_2']).'-'.$time->format('g:i a', $schedule['schedule_to_2'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_3'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:i a', $schedule['schedule_from_3']).'-'.$time->format('g:i a', $schedule['schedule_to_3'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_4'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:i a', $schedule['schedule_from_4']).'-'.$time->format('g:i a', $schedule['schedule_to_4'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_5'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:i a', $schedule['schedule_from_5']).'-'.$time->format('g:i a', $schedule['schedule_to_5'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_6'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:i a', $schedule['schedule_from_6']).'-'.$time->format('g:i a', $schedule['schedule_to_6'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_7'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:i a', $schedule['schedule_from_7']).'-'.$time->format('g:i a', $schedule['schedule_to_7'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_8'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:i a', $schedule['schedule_from_8']).'-'.$time->format('g:i a', $schedule['schedule_to_8'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_9'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:i a', $schedule['schedule_from_9']).'-'.$time->format('g:i a', $schedule['schedule_to_9'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_10'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:i a', $schedule['schedule_from_10']).'-'.$time->format('g:i a', $schedule['schedule_to_10'])?>
							<?php } ?>
						    </td>
						    <td align="center"><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
						    <?php $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							      $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							     if($getCompleted != "" && $getstarted == 1){ ?>
								Completed on <?php echo date('m/d/Y',strtotime($getCompleted)); ?>
						    <?php	 }
						    ?>
						    </td>
						    <td align="center" class="middle_img">
							<!--<a class="ajax" title="Log History" href="/sp/sps/viewLog?reportID=<?php echo $schedule['report_id']; ?>&clientID=<?php echo $res['ServiceCall']['client_id']; ?>&spID=<?php echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php echo $res['ServiceCall']['id']; ?>"><?php echo $html->image('icon_log.png');?></a>-->
							<?php if($schedule['report_id']== 1	){
							    $popupAction = 'firealarmView';
							    echo $html->link($html->image("pdf.png"), '/reports/firealarmFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Fire Alarm Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 2	){
							    $popupAction = 'sprinklerView';
							    echo $html->link($html->image("pdf.png"), '/reportPrints/sprinklerFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Sprinkler Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 3	){
							    $popupAction = 'kitchenhoodView';
							    echo $html->link($html->image("pdf.png"), '/reportPrints/kitchenhoodFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Kitchenhood Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 4	){
							    $popupAction = 'emergencyexitView';
							    echo $html->link($html->image("pdf.png"), '/reports/emergencyexitFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Emergency Exit Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 5){
							    $popupAction = 'extinguisherView';
							    echo $html->link($html->image("pdf.png"), '/reports/extinguisherFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Extinguisher Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 6	){
							    $popupAction = 'specialhazardView';
							    echo $html->link($html->image("pdf.png"), '/reportPrints/specialhazardFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Specialhazard Report Preview'));
							}
							echo $html->link($html->image('fineimages/view_report.png'),'javascript:void(0)',array('title'=>'Review Report','escape'=>false,'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>$popupAction.'?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],'inspector'=>true))."','','1000','800');"));
							echo $html->link($html->image('fineimages/send_quote.png'),array('controller'=>'sps','action'=>'sendQuote?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id']),array('class'=>'ajax','title'=>'Send Quote','escape'=>false));?>
						    </td>
						</tr>						
						<tr>
						    <td colspan="6" align="left">
							<div class="service-hdline">Inspection Devices</div>
							<ul class="services-tkn">
							    <li>
							    <?php $i=0;$j=1;
							    for($i=0;$i<count($schedule['ScheduleService']);$i++){
								echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
								<?php if($j%4==0)
								{
									echo '</li></ul><ul class="services-tkn"><li>';
								}
								else{
									echo '</li><li>';
								}
								 $j++;
								}
							    ?>
						    </td>
						</tr>
						<?php
						    $certs = $this->Common->getAllSchCerts($schedule['id']);						
						    if(sizeof($certs)>0){
						?>
						<tr>
						    <td colspan="6">
							<div class="service-hdline">Inspection Certificates by the Inspector</div>
							<ul class="services-tkn"><li>
							<?php $cr=1;
							foreach($certs as $cert)
							{
							    echo $html->image('tick.png');?><?php echo $html->link($cr.'.'.$cert['ReportCert']['ins_cert_title'],array("controller"=>"messages","action"=>"download_self_form",$cert['ReportCert']['ins_cert_form'],'inspector'=>true),array('title'=>'DownLoad/View')); ?>
							    <?php if($cr%4==0){
								    echo '</li></ul><ul class="services-tkn"><li>';
							    }
							    else{
								    echo '</li><li>';
							    }
							    $cr++;
							}
							?>		
						    </td>
						</tr>
						<?php						
						    }		
						?>
						<?php
						    $quotedocs = $this->Common->getQuoteDoc($schedule['report_id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id'],$res['ServiceCall']['id']);						
						    if(isset($quotedocs) && !empty($quotedocs)){
						?>
						<tr>
						    <td colspan="6">
							<div class="service-hdline">Quote Response from client</div>
							<?php
							    if($quotedocs['Quote']['client_response']=='a'){
								$status = 'Accepted';
							    }else if($quotedocs['Quote']['client_response']=='d'){
								$status = 'Denied';
							    }else if($quotedocs['Quote']['client_response']=='n'){
								$status = 'No wish to fix the deficiencies';
							    }else{
								$status = 'Pending';
							    }
							?>
							<div class="service-hdline"><?php echo 'Status: '.$status;?></div>
							<?php if($quotedocs['Quote']['client_response']=='a'){?>
							<div class="service-hdline">
							    <?php echo $html->link('Signed Quote Form',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['signed_form'],'quotefile','sp'=>true),array('style'=>'color:white; background:red; padding:1px 4px; border-radius:4px;'))?>
							</div>							    							
							<?php }else if($quotedocs['Quote']['client_response']=='d'){?>
							<div class="service-hdline">
							    <?php echo $html->link('Work Orders',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['work_order_form'],'quotefile','sp'=>true),array('style'=>'color:red'))?>
							</div>
							<?php }?>
						    </td>
						</tr>
						<?php						
						    }		
						?>
						
						
						<?php } ?>						
						<?php endforeach;?>
					    </table>				
					</div>
					<?php }endforeach;
				}else {
				    echo "No Record Found";
				}if(isset($actualRowCounter) && $actualRowCounter==0){
				    echo "No Record Found";
				} ?>
			</div>
		    </td>
		</tr>
	    </table>
	</div>
	
	<div id="clientView" style="display: none">	    
	    <div class="butn"><a href="javascript:void(0);" onclick="switchView('clientView','a');">Switch to Accordian View</a></div>
	    <br/>
	    <table class="tbl">
		<tr>
		    <td width="70%" align="left">				
			<div id="accordionClient">
			    <?php if(!empty($Clientdata))
				  {
				    foreach($Clientdata as $key=>$resClient):
					foreach($resClient as $res):
					    $actualRowClientCounter = 0;
					    $showCheck = $this->Common->checkCompleteServiceCall($res['ServiceCall']['id']);
					    if($showCheck==true){
						$actualRowClientCounter++;
				?>
					<div>
					    <h3><a href="#" style="color: #000 !important;"><?php echo $this->Common->getClientName($key);?> &nbsp;<span style="color: grey"><i>Company:<?php echo $this->Common->getClientCompanyName($key);?></i></span></a></h3>
					    <table class="inner" cellpadding="0" cellspacing="0">
						<tr>
						    <td colspan="6" style="background:url('../../img/newdesign_img/nav_active.png');color:#FFF;font-size:14px;"><strong><?php echo 'Name : '.$res['ServiceCall']['name'].' [Sch. requested : '.date('m/d/Y',strtotime($res['ServiceCall']['created'])).']' ?></strong></td>
						</tr>
						<tr>
						    <td colspan="6" style="background-color:#fff;color:#817679;font-size:14px;"><strong><?php echo 'Site Address : '.$this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']);?></strong></td>
						</tr>
						<tr style="background:url('../../img/newdesign_img/nav_bg.png') bottom;">
						    <th width="15%" align="center" style="color:#FFF">Report</th>
						    <th width="10%" align="center" style="color:#FFF">Lead Inspector</th>
						    <th width="10%" align="center" style="color:#FFF">Helper</th>
						    <th width="20%" align="center" style="color:#FFF" align="left">Sch Date(M/D/Y)</th>
						    <th style="color:#FFF" align="center" width="10%">Status</th>
						    <th style="color:#FFF" align="center" width="35%">Action</th>
						    <!--<th style="background-color:#2B92DD;color:#FFF" width="10%">Report Log</th>
						    <th style="background-color:#2B92DD;color:#FFF" width="10%">Send quote</th>-->
						</tr>				
						<?php
						    foreach($res['Schedule'] as $key1=>$schedule):
							$getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							if($getCompleted != ""){
						?>
						<tr style="background-color:#eaeaea;">
						    <td align="center" style="font-size: 16px;"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
						    <td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
						    <td align="center">
							<?php
							    $helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
							    echo $this->Common->getHelperName($helper_ins_arr);
							?>
						    </td>
						    <td align="center">
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:i a', $schedule['schedule_from_1']).'-'.$time->format('g:i a', $schedule['schedule_to_1']); ?><br/>
							<?php if(!empty($schedule['schedule_date_2'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:i a', $schedule['schedule_from_2']).'-'.$time->format('g:i a', $schedule['schedule_to_2'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_3'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:i a', $schedule['schedule_from_3']).'-'.$time->format('g:i a', $schedule['schedule_to_3'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_4'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:i a', $schedule['schedule_from_4']).'-'.$time->format('g:i a', $schedule['schedule_to_4'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_5'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:i a', $schedule['schedule_from_5']).'-'.$time->format('g:i a', $schedule['schedule_to_5'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_6'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:i a', $schedule['schedule_from_6']).'-'.$time->format('g:i a', $schedule['schedule_to_6'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_7'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:i a', $schedule['schedule_from_7']).'-'.$time->format('g:i a', $schedule['schedule_to_7'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_8'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:i a', $schedule['schedule_from_8']).'-'.$time->format('g:i a', $schedule['schedule_to_8'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_9'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:i a', $schedule['schedule_from_9']).'-'.$time->format('g:i a', $schedule['schedule_to_9'])?><br/>
							<?php } ?>
							<?php if(!empty($schedule['schedule_date_10'])) { ?>
							<?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:i a', $schedule['schedule_from_10']).'-'.$time->format('g:i a', $schedule['schedule_to_10'])?>
							<?php } ?>
						    </td>
						    <td align="center"><?php $arr=explode(' ',$schedule['schedule_from_1']);?>
						    <?php $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							      $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							     if($getCompleted != "" && $getstarted == 1){ ?>
								Completed on <?php echo date('m/d/Y',strtotime($getCompleted)); ?>
						    <?php	 }
						    ?>
						    </td>
						    <td class="middle_img" align="center">
							<!--<a class="ajax" title="Log History" href="/sp/sps/viewLog?reportID=<?php echo $schedule['report_id']; ?>&clientID=<?php echo $res['ServiceCall']['client_id']; ?>&spID=<?php echo $res['ServiceCall']['sp_id']; ?>&serviceCallID=<?php echo $res['ServiceCall']['id']; ?>"><?php echo $html->image('icon_log.png');?></a>-->							    
							<?php if($schedule['report_id']== 1	){
							    $popupAction = 'firealarmView';
							    echo $html->link($html->image("pdf.png"), '/reports/firealarmFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Fire Alarm Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 2	){
							    $popupAction = 'sprinklerView';
							    echo $html->link($html->image("pdf.png"), '/reportPrints/sprinklerFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Sprinkler Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 3	){
							    $popupAction = 'kitchenhoodView';
							    echo $html->link($html->image("pdf.png"), '/reportPrints/kitchenhoodFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Inspector Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 4	){
							    $popupAction = 'emergencyexitView';
							    echo $html->link($html->image("pdf.png"), '/reports/emergencyexitFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Emergency/Exit Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 5){
							    $popupAction = 'extinguisherView';
							    echo $html->link($html->image("pdf.png"), '/reports/extinguisherFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Extinguisher Report Preview'));
							} ?>
							<?php if($schedule['report_id']== 6	){
							    $popupAction = 'specialhazardView';
							    echo $html->link($html->image("pdf.png"), '/reportPrints/specialhazardFullReport?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'], array('escape' => false,'title'=>'Specialhazard Report Preview'));
							}
							echo $html->link($html->image('fineimages/view_report.png'),'javascript:void(0)',array('title'=>'Review Report','escape'=>false,'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>$popupAction.'?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id'],'inspector'=>true))."','','1000','800');"));
							echo $html->link($html->image('fineimages/send_quote.png'),array('controller'=>'sps','action'=>'sendQuote?reportID='.$schedule['report_id'].'&clientID='.$res['ServiceCall']['client_id'].'&spID='.$res['ServiceCall']['sp_id'].'&serviceCallID='.$res['ServiceCall']['id']),array('class'=>'ajax','title'=>'Send Quote','escape'=>false));?>
						    </td>
						</tr>						
						<tr>
						    <td colspan="6" align="left">
							<div class="service-hdline">Inspection Devices</div>
							<ul class="services-tkn">
							    <li>
							    <?php $i=0;$j=1;
							    for($i=0;$i<count($schedule['ScheduleService']);$i++){
								echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$schedule['ScheduleService'][$i]['service_id']);?>(<?php echo @$schedule['ScheduleService'][$i]['amount'] ?>,<?php echo $frequency[@$schedule['ScheduleService'][$i]['frequency']]; ?>)
								<?php if($j%4==0)
								{
									echo '</li></ul><ul class="services-tkn"><li>';
								}
								else{
									echo '</li><li>';
								}
								 $j++;
								}
							    ?>
						    </td>
						</tr>
						<?php
						    $certs = $this->Common->getAllSchCerts($schedule['id']);						
						    if(sizeof($certs)>0){
						?>
						<tr>
						    <td colspan="6">
							<div class="service-hdline">Inspection Certificates by the Inspector</div>
							<ul class="services-tkn"><li>
							<?php $cr=1;
							foreach($certs as $cert)
							{
							    echo $html->image('tick.png');?><?php echo $html->link($cr.'.'.$cert['ReportCert']['ins_cert_title'],array("controller"=>"messages","action"=>"download_self_form",$cert['ReportCert']['ins_cert_form'],'inspector'=>true),array('title'=>'DownLoad/View')); ?>
							    <?php if($cr%4==0){
								    echo '</li></ul><ul class="services-tkn"><li>';
							    }
							    else{
								    echo '</li><li>';
							    }
							    $cr++;
							}
							?>		
						    </td>
						</tr>
						<?php						
						    }		
						?>
						
						<?php
						    $quotedocs = $this->Common->getQuoteDoc($schedule['report_id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id'],$res['ServiceCall']['id']);						
						    if(isset($quotedocs) && !empty($quotedocs)){
						?>
						<tr>
						    <td colspan="6">
							<div class="service-hdline">Quote Response from client</div>
							<?php
							    if($quotedocs['Quote']['client_response']=='a'){
								$status = 'Accepted';
							    }else if($quotedocs['Quote']['client_response']=='d'){
								$status = 'Denied';
							    }else if($quotedocs['Quote']['client_response']=='n'){
								$status = 'No wish to fix the deficiencies';
							    }
							?>
							<div class="service-hdline"><?php echo 'Status: '.$status;?></div>
							<?php if($quotedocs['Quote']['client_response']=='a'){?>
							<div class="service-hdline">
							    <?php echo $html->link('Signed Quote Form',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['signed_form'],'quotefile','sp'=>true),array('style'=>'color:white; background:red; padding:1px 4px; border-radius:4px;'))?>
							</div>							    							
							<?php }else if($quotedocs['Quote']['client_response']=='d'){?>
							<div class="service-hdline">
							    <?php echo $html->link('Work Orders',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['work_order_form'],'quotefile','sp'=>true),array('style'=>'color:red'))?>
							</div>
							<?php }?>
						    </td>
						</tr>
						<?php						
						    }		
						?>					
						<?php } ?>						
						<?php endforeach;?>
					    </table>				
					</div>
					<?php }endforeach;
					endforeach;
				}else {
				    echo "No Record Found";
				}if(isset($actualRowCounter) && $actualRowCounter==0){
				    echo "No Record Found";
				} ?>
			</div>
		    </td>
		</tr>
	    </table>
	</div>	    
    </div>
</div>    