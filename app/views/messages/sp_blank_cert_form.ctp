<?php
    echo $this->Html->script('jquery.1.6.1.min');
  // echo $this->Html->script('jquery-ui.min.js'); 
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('validation/only_num');
      #FOR COLOR PICKER
    echo $this->Html->css('colorpicker/colorpicker.css');
    echo $this->Html->css('colorpicker/layout.css');
    echo $this->Html->script(array('colorpicker/colorpicker.js','colorpicker/eye.js','colorpicker/utils.js','colorpicker/layout.js?ver=1.0.2'));
    
?>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#CertForm").validationEngine();
});
function chkConfirm(certId, obj){
    var parentTr = jQuery(obj).parent('td').parent('tr');
    var id = certId;
    jQuery.ajax({
	url:'/sp/messages/chkUsed/'+certId,
	success:function(msg){
	    if(msg=='OK'){
		if(!confirm("This certificate is already used in reports. Are you sure you want to delete this?")){
		    return false;
		}		
	    }else if(msg=='NO'){	
		if(!confirm("Are you sure you want to delete this?")){
		    return false;
		}
	    }else{
		alert(msg);
	    }
	    window.location.href = '/sp/messages/deleteCert/'+id;
	}
    });
    return false;
}
</script>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div id="box">
        
    <h3 id="adduser">Blank Cert Form List</h3>
    <?php echo $this->Form->create('', array('controller'=>'messages','action' => 'blank_cert_form','name'=>'webpages','id'=>'WebsitepagesShowForm') );?>
                <?php if(count($resCertForm)) { ?>
                <table class="tbl">
                <thead>
                <tr>
                <th width="5%"><input type="checkbox"  name='checkall' onclick='checkedAllpage();' id='checkall'></th>
                <th width="30%"><?php echo $paginator->sort('Cert Name','title');?></th>
			    <th width="15%"><?php echo $paginator->sort('For Report','report_id');?></th>			    
			    <th width="30%">Cert Form</th>
                <th width="10%">Status</th>
		<th width="10%">Delete</th>
                
                </tr>
                </thead>
                <?php $i=1;
                
                foreach($resCertForm  as $res){			    
                ?>
                <tr>                   
                    <td align="center"><input type="checkbox" name="box[]" value="<?php echo $res['SpBlankCertForm']['id']; ?>" onclick='uncheck(this);'></td>
                    <td align="center"><?php echo $res['SpBlankCertForm']['title']?></td>
                    <td align="center"><?php echo $res['Report']['name']?></td>
                    <td align="center"><?php echo $html->link($res['SpBlankCertForm']['cert_form'],array("controller"=>"messages","action"=>"download_cert_form",$res['SpBlankCertForm']['cert_form']),array('title'=>'DownLoad/View'));?></td>
                    <td align="center"><?php echo $res['SpBlankCertForm']['status']?></td>
		    <td align="center"><?php echo $html->link($html->image('icons/mszdelete.gif'),'javascript:void(0);',array('title'=>'DELETE','escape'=>false,'onclick'=>'chkConfirm('.$res['SpBlankCertForm']['id'].',this)'));?></td>
					
                </tr>
                <?php
                $i++;
                } ?>
                 <tr>
                    <td colspan="6"><?php echo $this->Form->submit("Active",array('id'=>'active','div'=>false,'class'=>'iconlink','name' => 'active',"onClick"=>'return prompt_activepage();')); ?>&nbsp;<?php echo $this->Form->submit("Inactive",array('id'=>'Inactive','div'=>false,'class'=>'iconlink','name' => 'inactive',"onClick"=>'return prompt_inactivepage();')); ?></td>
                    
                    
                </tr>    
                <table>
                    <?php }
                    else
                    {
                        echo "No Record Found";
                    }
                    
                    ?>
            <?php echo $this->Form->end(); ?>    
            </div>
            <?php echo $this->Form->create('', array('controller'=>'messages','action' => 'blank_cert_form','id'=>'CertForm','name'=>'CertForm','enctype'=>'multipart/form-data') );?>
            <section class="form-box" style="width:100%;">
                <h2><strong>Add New Cert Form</strong></h2>
                <ul class="register-form">
                    <li>
                        <label>Cert Title(Appropriate Name)</label>
                        <p><span class="input-field"><?php echo $form->input('SpBlankCertForm.title', array('maxLength'=>'100','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?></span></p>
                    </li>
                    <li>
                        <label>Upload Form</label>
                        <p><span class=""><?php echo $form->file('SpBlankCertForm.cert_form', array("div"=>false,"label"=>false,"class"=>"validate[required]"));  ?></span></p>
                        <span>(Only pdf,xls,doc,docx,xlsx file extension)</span>
                    </li>
                    <li>
                        <label>For Report</label>
                        <p><span class=""><?php echo $form->select('SpBlankCertForm.report_id',$reports,'',array('div'=>false,'legend'=>false,'label'=>false,"class"=>"validate[required]",'style'=>'width:250px;')); ?></span></p>
                    </li>
                    <li>
                        <label>&nbsp;</label>
                        <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
                       
                    </li> 
                             
                  
                </ul>
            </section>
            <?php echo $form->end(); ?>
             <br/>   
</div>



<script type="text/javascript">
function prompt_activepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	

	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
	
      }
      
      if(flag==1)
      {
	      if(confirm('Are you sure you want to make this Cert Form active ?')){
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one record to active");
      return false;
      }
}

function prompt_inactivepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	
	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
      }
      
      if(flag==1)
      {
      
	      if(confirm('Are you sure you want to make this Cert Form inactive ?')){
		
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one record to inactive");
      return false;
      }
}

function checkedAllpage ()
      {        
        if (document.getElementById('checkall').checked == false)
        {
        checked = false
        }
        else
        {
        checked = true
        }
        for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++)
        {
          document.getElementById('WebsitepagesShowForm').elements[i].checked = checked;
        }
      }
</script> 
