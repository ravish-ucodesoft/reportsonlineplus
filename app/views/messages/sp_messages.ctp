<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:10px;"><b><?php echo $html->link('Compose',array('controller'=>'messages','action'=>'create_message'),array("style"=>"padding-right:20px;")); ?></b></div>
	      
	      <?php //echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addclients'),'id'=>'addclient')); ?>
              <div id="box" >
                	<h3 id="adduser">Inbox</h3>
    <table width="100%" cellpadding="3" cellspacing="3" border="0" class="tbl">

						<thead>

							<tr>							             					
								<th width="20%">From</th>
								<th width="30%">Subject</th>
							  <th width="30%">Message</th>
							  <th width="20%">Action</th>
							</tr>
						</thead>
						<?php  if(count($msg_data)) { ?>
						<tbody>
						<?php $i=isset($_GET['page'])?(($_GET['page']-1)*$pagelimit):0;?>
						<?php foreach($msg_data as $data):  ?>
							<tr>             	
                <td align="center"><?php echo ucWords($data['User']['fname']); ?></td>
                <td align="center"><?php echo $data['Message']['message_subject']; ?></td>
                <td align="center"><?php echo substr($data['Message']['message_body'],0,40)."..."; ?></td>
                <td align="center"><?php echo  $this->Html->link($this->Html->image('icons/view.png', array('alt'=>"View Detail", 'title'=>"View Detail", 'class' =>'iconlink2') ),array('controller'=>'messages','action'=>'show',$data['Message']['id']),array('escape'=>false), false); ?>
                <?php echo  $this->Html->link($this->Html->image('icons/mszreply.png', array('alt'=>"Reply", 'title'=>"Reply", 'class' =>'iconlink2') ),array('controller'=>'messages','action'=>'reply',$data['Message']['id']),array('escape'=>false), false); ?>
                <?php echo  $this->Html->link($this->Html->image('icons/mszdelete.gif', array('alt'=>"Delete", 'title'=>"Delete", 'class' =>'iconlink2') ),array('controller'=>'messages','action'=>'delrecievedmsg',$data['Message']['id']),array('escape'=>false, "onClick"=>"javascript:return confirm('Are you sure you want to delete this message ?');"), false); ?></td>
              </tr>
							<?php 
              $i++;
              endforeach; ?>
              
						</tbody>
						
						<?php } else { ?>
          	<div>No Message in inbox</div>
          <?php } ?>
					</table>
			  </div>
					<div class="pagination" >
					<span class="page_no">Page <?php echo $paginator->counter(); ?></span>
      <?php
          echo $paginator->prev('<< Previous ', null, null, array('class' => 'disabled'));
          echo $paginator->next(' Next >>', null, null, array('class' => 'disabled'));
          ?> 
										
					</div>
    
  </div>
  
  
