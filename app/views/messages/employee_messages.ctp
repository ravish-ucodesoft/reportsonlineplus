<div id="content" >	
				<div id="box">
				<?php echo $form->create('Messages',array('controller'=>'Messages','action' => 'employee_messages','method'=>'post','id'=>'ListForm','name'=>'ListForm'));?>
				<?php //echo $form->create(array('id'=>'ListForm','controller'=>'Employees','action' => 'list'));?>
				<input type="hidden" name="action" value="" id="action">
        <h3>New Messages (<?php echo $count_new_message;?>)&nbsp;&nbsp;&nbsp;<?php echo $html->link('Write a Message',array('controller'=>'messages','action'=>'write_message')); ?></h3>
        
        <table width="100%">
						<thead>

							<tr>
								<th>No.</th>
                <th><input type="checkbox"  name='checkall' onclick='checkedAllpage();' id='checkall'></th>					
								<th>From</th>
								<th>Subject</th>
							  <th>Date</th>
							</tr>
						</thead>
						<?php  if(count($msg_data)) { ?>
						<tbody>
						<?php $i=isset($_GET['page'])?(($_GET['page']-1)*$pagelimit):0;?>
						<?php foreach($msg_data as $msg_data): ?>
							<tr>
              	<td align="center"><?php echo ($i+1); ?></td>
              	<td align="center"><input type="checkbox" name="box[]" value="<?php echo $msg_data['Message']['id']; ?>" onclick='uncheck(this);' ></td>
                <td align="center"><?php echo $common->getSenderName($msg_data['Message']['sender_type'],$msg_data['Message']['sender_id']); ?></td>
                <td align="center"><?php echo $html->link($msg_data['Message']['message_subject'],array('controller'=>'messages','action'=>'view_message',$msg_data['Message']['id'])); ?></td>
                <td align="center"><?php echo $msg_data['Message']['created']; ?></td>
                
              </tr>
							<?php 
              $i++;
              endforeach; ?>
              <tr><td colspan="5"><?php echo  $form->submit(__('Delete',true),array('div'=>false,'name'=>'delete','onClick'=>'return prompt_delete()')); ?></td></tr>
						</tbody>
						
						<?php } else { ?>
          	<div>There is no incoming message</div>
          <?php } ?>
					</table>
					<?php echo $form->end();?>
        </div>	
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->					
					<div class="pagination" >
					<span class="page_no">Page <?php echo $paginator->counter(); ?></span>
    			<?php
          echo $paginator->prev('<< Previous ', null, null, array('class' => 'disabled'));
          echo $paginator->next(' Next >>', null, null, array('class' => 'disabled'));
          ?> 
										
					</div>
					<!--[if !IE]>end pagination<![endif]-->
						
</div>
<script>			
	  function uncheck(checkbox)
    {

        if(document.getElementById('checkall').checked==true && checkbox.checked==false)
        {
            document.getElementById('checkall').checked=false;
        }
    }

    function checkedAllpage ()
    {
        if (document.getElementById('checkall').checked == false)
        {
            checked = false
        }
        else
        {
            checked = true
        }
        for (var i = 0; i < document.getElementById('ListForm').elements.length; i++)
        {
            document.getElementById('ListForm').elements[i].checked = checked;
        }
    }
    function prompt_delete()
    {
        var flag=0;
        for (var i = 0; i < document.getElementById('ListForm').elements.length; i++)
        {

            if(document.getElementById('ListForm').elements[i].checked == true)
            {
                flag=1;
            }


        }

        if(flag==1)
        {

            if(confirm('Are you Sure To Delete !')){
                return true;
            }else{
                return false;
            }
        }
        else
        {
            alert("Please select atleast one message to delete");
            return false;
        }
    }
</script>