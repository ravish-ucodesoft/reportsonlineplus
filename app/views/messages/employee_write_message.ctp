<div id="content" >
              <div id="box" >
                	<h3 id="adduser">Write a New Message</h3>                	
                  <?php echo $form->create('Messages',array('controller'=>'Messages','action' => 'employee_write_message','onsubmit'=>'return formvalidate();','method'=>'post','id'=>'ListForm','name'=>'ListForm'));?>
                  <?php echo $form->input('Message.sender_id',array('type' => 'hidden','value'=>$session->read('EmployeeId'))); ?>
	                <?php //echo $form->input('Message.receiver_id',array('type' => 'hidden','value'=>$msg_data['Message']['sender_id'])); ?>
	                <?php //echo $form->input('Message.receiver_type',array('type' => 'hidden','value'=>$msg_data['Message']['sender_type'])); ?>
                  <table width="100%" cellspacing="2" cellpadding="2">
                  
                  <tr><td width="30%" class="td_label">To:</td>
                      <td class="td_label1"><?php echo $form->select('Message.receiver_id',$To,null,array('empty'=>'please select')); ?></td>
                  </tr>
                  <tr><td width="30%" class="td_label">Subject:</td>
                      <td class="td_label1"><?php echo $form->input('Message.message_subject',array('size'=>'45','maxlength'=>'100','div'=>false,'label'=>false,'value'=>'','style'=>'border:1px solid #c3c3c3;color:#999999')); ?></td>
                  </tr>
                  
                  <tr><td width="30%" class="td_label" valign="top">Message:</td>
                      <td class="td_label1"><?php echo $form->input('Message.message_body',array('type'=>'textarea','rows'=>7,'cols'=>35,'label'=>false,'div'=>false,'style'=>'border:1px solid #c3c3c3;color:#999999')); ?></td>
                  </tr>
                  <tr><td width="30%" class="td_label">&nbsp;</td>
                      <td class="td_label1">
                  <?php echo  $form->submit('Send',array('class'=>'submitbutton','div'=>false,'label'=>false)); ?><?php echo  $form->submit('Back',array('class'=>'submitbutton','type'=>'button','div'=>false,'label'=>false,'onClick'=>'history.go(-1);')); ?> 
                 </td>                                
                </table>
                  <?php echo $form->end(); ?> 
                </div>
</div>

<?php echo $html->script('jquery/jquery.1.4.2'); ?>            
<script type="text/javascript">

function formvalidate()
{
  
  if($.trim($('#MessageReceiverId').val())=='')
  {
    alert('please select the recipient');
    $('#MessageReceiverId').focus();
    return false;
  }
  
  if($.trim($('#MessageMessageSubject').val())=='')
  {
    alert('please enter the subject');
    $('#MessageMessageSubject').focus();
    return false;
  }
  if($.trim($('#MessageMessageBody').val())=='')
  {
    alert('please enter the message');
    $('#MessageMessageBody').focus();
    return false;
  }
  return true;
}
</script>