<?php echo $this->Html->script('fckeditor'); ?>

<section class="register-wrap">
	<h1 class="main-hdng">General Company Announcement</h1>
    <!--one complete form-->
    <div class='message' style="text-align:center;font-weight:bold;"><?php echo $this->Session->flash();?></div>
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'general_company_announcement'),'id'=>'general_company_announcement')); ?>
          <?php echo $form->input('SpGeneralCompanyAnnouncement.id',array('type'=>'hidden','value'=>$res['SpGeneralCompanyAnnouncement']['id'])); ?>     
    <section class="form-box" style="width:90%;">
        <h2><strong>For Inspectors</strong></h2>
        <ul class="register-form">
            <li>
                
                <?php echo $form->input('SpGeneralCompanyAnnouncement.for_inspectors', array('type'=>'textarea','rows' => 10,'cols' =>18,'div'=>false,'label'=>false,'value'=>$res['SpGeneralCompanyAnnouncement']['for_inspectors']));  
					echo $fck->load('SpGeneralCompanyAnnouncementForInspectors');
					?>
            </li>
            	     
	      
        </ul>
    </section>
    
    <section class="form-box" style="width:90%;">
        <h2><strong>For Clients</strong></h2>
        <ul class="register-form">
            <li>
                
                <?php echo $form->input('SpGeneralCompanyAnnouncement.for_clients', array('type'=>'textarea','rows' => 10,'cols' =>18,'div'=>false,'label'=>false,'value'=>$res['SpGeneralCompanyAnnouncement']['for_clients']));  
					echo $fck->load('SpGeneralCompanyAnnouncementForClients');
					?>
            </li>
             <li>
                <label>&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
               
            </li>   	     
	      
        </ul>
    </section>
	
	
   
    <?php echo $form->end(); ?>
</section>