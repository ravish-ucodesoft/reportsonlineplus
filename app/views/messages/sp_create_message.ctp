<?php 
    echo $this->Html->script('jquery.1.6.1.min.js');
    echo $this->Html->script('jquery.autocomplete.js');  
?>
<?php 
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>

<script type="text/javascript">
//	$.noConflict();
	jQuery(document).ready(function()
	{
		
	jQuery("#composeMszForm").validationEngine();
		jQuery('#creditsearchresult').click(function() {
			options  = {
				serviceUrl:'/messages/autoComplete',
				width:jQuery("#creditsearchresult").css("width"),			
				onSelect:function(value,data){		// on select put the username in the textarea				
					jQuery("#creditsearchresult").val(value);
                    jQuery("#creditsearchresult_hidden").val(data);		
				}
			}
			var a = jQuery("#creditsearchresult").autocomplete(options);
		});

		
	});
</script>
<style>
.usr-nam { display:inline-block; position:relative;}
.auto-cmplt { display:block; padding-top:0px;}
.autocmplt-cont span.ValidationErrors { margin-left:-226px;}
.autocomplete { background:#000; border:1px solid #ccc;padding:3px; font-weight:normal;}
.autocomplete{margin:2px 0px 0px 0px;padding:5px 5px;border-bottom:1px solid #A8A6A6;color:#FFFFFF;font-weight:bold;font-size:13px;width:px;background-color:#3688B8;list-style: none;cursor:pointer;}
.auto_complete{top:22px!important;left:0px!important;}
</style>
<div class="register-wrap">
	      <div class='message'><?php echo $this->Session->flash();?></div>
              <div id="box" >
          <?php echo $form->create('Messages',array('controller'=>'Messages','action' => 'create_message','onsubmit'=>'return formvalidate();','method'=>'post','id'=>'composeMszForm','name'=>'ListForm'));?>
                  <?php echo $form->input('Message.sender_id',array('type' => 'hidden','value'=>$session->read('Spid'))); ?>
		<div class="form-box" style="width:90%;">
		    <br/>
		    <h2>Compose Message</h2>
		    <ul class="register-form">
			<li>
			    <label>To:<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('Message.receiver_name',array('type'=>'text',"id" => "creditsearchresult",'div'=>false,'label'=>false,'class'=>'validate[required]', "autocomplete" => "off")); ?></span></p>
			    <p>Type slowly to use autocomplete</p>
			    <?php echo $form->hidden('Message.receiver_id',array('id'=>'creditsearchresult_hidden','class'=>'text-field','label'=>false,'div'=>false)); ?>
			</li>
			<li>
			    <label>Subject:<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('Message.message_subject',array('size'=>'45','maxlength'=>'100','div'=>false,'label'=>false,'value'=>'','class'=>'validate[required]')); ?></span></p>
			</li>
			<li>
			    <label>Message:<span>*</span></label>
			    <p><span class=""><?php echo $form->input('Message.message_body',array('type'=>'textarea','rows'=>7,'cols'=>35,'label'=>false,'div'=>false,'class'=>'validate[required]')); ?></span></p>
			</li>		
			<li>
			<label>&nbsp;</label>
			    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Send',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>   
			</li>
			</ul>
		    </div>
	      <?php echo $form->end(); ?>
                </div>
</div>


