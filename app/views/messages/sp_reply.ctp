<?php 
    echo $this->Html->script('jquery.1.6.1.min.js');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
//	$.noConflict();
	jQuery(document).ready(function(){		
	 jQuery("#msgsend").validationEngine();
	});
</script>
<div class="register-wrap">
	      <div class='message'><?php echo $this->Session->flash();?></div>
              <div id="box" >
          <?php //pr($result);
				   echo $this->Form->create('Message', array('controller' => 'messages','action' => 'reply','name'=>'msgsend','id'=>'msgsend','enctype' => 'multipart/form-data'));
				    echo $this->Form->input('Message.sender_id',array("type" => "hidden" ,'readonly'=>'','div'=>false,'label'=>false,"value"=>@$result['Message']['receiver_id']));
				    echo $this->Form->input('Message.receiver_id',array("type" => "hidden" ,'readonly'=>'','div'=>false,'label'=>false,"value"=>@$result['Message']['sender_id']));
				?>
		<div class="form-box" style="width:90%;">
		    <br/>
		    <h2>Reply</h2>
		    <ul class="register-form">			
			<li>
			    <label>Subject:<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('Message.message_subject',array('size'=>'45','maxlength'=>'100','div'=>false,'label'=>false,'class'=>'validate[required]',"value"=>"RE:".$result['Message']['message_subject'])); ?></span></p>
			</li>
			<li>
			    <label>Message:<span>*</span></label>
			    <p><span class=""><?php echo $form->input('Message.message_body',array('type'=>'textarea','rows'=>5,'cols'=>29,'label'=>false,'div'=>false,'class'=>'validate[required]','style'=>'')); ?></span></p>
			</li>		
			<li>
			<label>&nbsp;</label>
			    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Reply',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>   
			</li>
			</ul>
		    </div>
	      <?php echo $form->end(); ?>
                </div>
</div>


