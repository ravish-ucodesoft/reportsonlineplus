<?php
    echo $this->Html->script('jquery.1.6.1.min');
  // echo $this->Html->script('jquery-ui.min.js'); 
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('validation/only_num');
      #FOR COLOR PICKER
    echo $this->Html->css('colorpicker/colorpicker.css');
    echo $this->Html->css('colorpicker/layout.css');
    echo $this->Html->script(array('colorpicker/colorpicker.js','colorpicker/eye.js','colorpicker/utils.js','colorpicker/layout.js?ver=1.0.2'));
    
?>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#CertForm").validationEngine();   
});

function deleteRecord(fileName){
    jQuery.ajax({
        'url': '/inspector/messages/deleteFile/'+fileName,
        'success': function(msg){
            if(msg=='OK'){
                jQuery("#showFile").remove();
            }    
        }
    });
}
</script>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
            <?php echo $this->Form->create('', array('url'=>array('controller'=>'messages','action' => 'uploadReportCert',$resCertForm['ReportCert']['id']),'id'=>'CertForm','name'=>'CertForm','enctype'=>'multipart/form-data') );?>
            <section class="form-box" style="width:100%; margin: 0">
                <h2><strong>This certificate has to be attached along with the report submission <?php //echo $resCertForm['SpBlankCertForm']['title'];?></strong></h2>
                <ul class="register-form">
                    <?php if($resCertForm['ReportCert']['ins_cert_form']!="" && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'ins_report_cert'.DS.$resCertForm['ReportCert']['ins_cert_form'])){?>
                    <li id="showFile">
                        <label>&nbsp;</label>
                        <p>
                           Already Submitted. (<?php echo $html->link($resCertForm['ReportCert']['ins_cert_title'],array('controller'=>'messages','action'=>'download_self_form',$resCertForm['ReportCert']['ins_cert_form']));?>)<?php echo $html->link('DELETE','javascript:void(0)',array('id'=>'delBtn','style'=>'padding-left:20px; color:red', 'onclick'=>'deleteRecord("'.$resCertForm['ReportCert']['ins_cert_form'].'");'));?> 
                        </p>
                    </li>
                    <?php }?>
                    <li>
                        <label>Cert Title(Appropriate Name)</label>
                        <p><span class="input-field"><?php echo $form->input('ReportCert.ins_cert_title', array('maxLength'=>'100','class'=>'validate[required]',"div"=>false,"label"=>false,"tabindex"=>"1"));  ?></span></p>
                    </li>
                    <li>
                        <label>Upload Form</label>
                        <p><span class=""><?php echo $form->file('ReportCert.ins_cert_form', array("div"=>false,"label"=>false,"class"=>"validate[required]"));  ?></span></p>
                        <span>(Only pdf,xls,doc,docx,xlsx file extension)</span>
                    </li>                    
                    <li>
                        <label>&nbsp;</label>
                        <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>
                       
                    </li> 
                             
                  
                </ul>
            </section>
            <?php echo $form->end(); ?>
             <br/>   
</div>