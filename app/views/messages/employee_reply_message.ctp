<div id="content" >
              <div id="box" >
                	<h3 id="adduser">Reply/View Message</h3>                	
                  <?php echo $form->create('Messages',array('controller'=>'Messages','action' => 'employee_reply_message/'.$msg_data['Message']['id'],'onsubmit'=>'return formvalidate();','method'=>'post','id'=>'ListForm','name'=>'ListForm'));?>
                  <?php echo $form->input('Message.sender_id',array('type' => 'hidden','value'=>$session->read('EmployeeId'))); ?>
	                <?php echo $form->input('Message.receiver_id',array('type' => 'hidden','value'=>$msg_data['Message']['sender_id'])); ?>
	                <?php echo $form->input('Message.receiver_type',array('type' => 'hidden','value'=>$msg_data['Message']['sender_type'])); ?>
                  <table width="100%" cellspacing="2" cellpadding="2">
                  
                  <tr><td width="30%" class="td_label">To:</td>
                      <td class="td_label1"><?php echo $common->getSenderName($msg_data['Message']['sender_type'],$msg_data['Message']['sender_id']); ?></td>
                  </tr>
                  <tr><td width="30%" class="td_label">Subject:</td>
                      <td class="td_label1"><?php echo $form->input('Message.message_subject',array('size'=>'45','maxlength'=>'100','div'=>false,'label'=>false,'value'=>'Re: '.$msg_data['Message']['message_subject'],'style'=>'border:1px solid #c3c3c3;color:#999999')); ?></td>
                  </tr>
                  <tr><td width="30%" class="td_label">Original Message:</td>
                      <td class="td_label1"><?php echo nl2br($msg_data['Message']['message_body']);?></td>
                  </tr>
                  <tr><td width="30%" class="td_label" valign="top">Reply:</td>
                      <td class="td_label1"><?php 
                      $msg="\n\n\n\n\n\nQuoting: ".$common->getSenderName($msg_data['Message']['sender_type'],$msg_data['Message']['sender_id'])."\n\n".$msg_data['Message']['message_body'];
                      echo $form->input('Message.message_body',array('type'=>'textarea','value'=>$msg,'rows'=>7,'cols'=>35,'label'=>false,'div'=>false,'style'=>'border:1px solid #c3c3c3;color:#999999')); ?></td>
                  </tr>
                  <tr><td width="30%" class="td_label">&nbsp;</td>
                      <td class="td_label1">
                  <?php echo  $form->submit('Reply',array('class'=>'submitbutton','div'=>false,'label'=>false)); ?><?php echo  $form->submit('Back',array('class'=>'submitbutton','type'=>'button','div'=>false,'label'=>false,'onClick'=>'history.go(-1);')); ?> 
                 </td>                                
                </table>
                  <?php echo $form->end(); ?> 
                </div>
</div>

<?php echo $html->script('jquery/jquery.1.4.2'); ?>            
<script type="text/javascript">

function formvalidate()
{
  
  if($.trim($('#MessageMessageSubject').val())=='')
  {
    alert('please enter the subject');
    $('#MessageMessageSubject').focus();
    return false;
  }
  if($.trim($('#MessageMessageBody').val())=='')
  {
    alert('please enter the message');
    $('#MessageMessageBody').focus();
    return false;
  }
  return true;
}
</script>