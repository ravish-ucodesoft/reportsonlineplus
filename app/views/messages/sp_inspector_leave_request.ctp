<?php
    echo $this->Html->script('jquery.1.6.1.min');
    //echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core','ui/ui.datepicker','jquery-ui-1.8.20.custom.min'));
    echo $this->Html->script(array('timepicker/jquery-1.7.1.min'));
    echo $this->Html->script(array('timepicker/jquery-ui-1.8.16.custom.min','timepicker/jquery-ui-timepicker-addon','timepicker/jquery-ui-sliderAccess'));
    //echo $this->Html->script('languages/jquery.validationEngine-en.js');
    //echo $this->Html->script('jquery.validationEngine.js');
    
     /*** alert box**/
    
    echo $this->Html->css('alertbox/jquery.alerts.css');
    echo $this->Html->css('jquery/jquery.ui.draggable.js');
    echo $this->Html->script('alertbox/jquery.alert.js'); 
?>
<script type="text/javascript">

function loadDatePicker()
{
   jQuery(".calender").datepicker({
	dateFormat: 'mm/dd/yy',
	showOn: 'button',
	minDate:1 ,
	buttonImage: '/app/webroot/img/calendar.gif',
	buttonImageOnly: true,
	onSelect: function(){
	    //setMultiDate(jQuery(this).attr('name'),jQuery(this).val());    	    
	}
    });
}
jQuery(document).ready(function(){
loadDatePicker();
jQuery("#inspector_vacation").submit(function(){	
	if(jQuery("#InspectorVacationStartDate").val()==""){	    
	    jAlert('Please select the start date');
	    jQuery("#InspectorVacationStartDate").focus();
	    return false;
	}
	
	if(jQuery("#InspectorVacationEndDate").val()==""){	    
	    jAlert('Please select the end date');
	    jQuery("#InspectorVacationEndDate").focus();
	    return false;
	}
    
    if(jQuery("#InspectorVacationEndDate").val() < jQuery("#InspectorVacationStartDate").val()){	    
	    jAlert('End date cannot be less than start date');
	    jQuery("#InspectorVacationEndDate").focus();
	    return false;
	}
    
    
 });
 });
</script>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div id="box">     
    <h3 id="adduser">Inspectors Vacation List (Newest to Oldest)</h3>
                <table class="tbl">
                <thead>
                <tr>
                <th width="5%">S.No</th>
                <th width="15%" align="left">Inspector</th>
			    <th width="8%">Start Date</th>			    
			    <th width="8%">End Date</th>
                <th width="10%">Effect on Previous Service calls</th>
                <th width="18%">Change Inspector/View Effect on Previous Service calls</th>
                </tr>
                </thead>
                <?php $i=1;
                foreach($vacation  as $res){			    
                ?>
                <tr>                   
                    <td align="center"><?php echo $i;?></td>
                    <td align="left"><?php echo ucwords($res['User']['fname'].' '.$res['User']['lname']);?></td>
                    <td align="center"><?php echo date('m/d/Y',strtotime($res['InspectorVacation']['start_date'])); ?></td>  
                    <td align="center"><?php echo date('m/d/Y',strtotime($res['InspectorVacation']['end_date'])); ?></td>
                    <td align="center">
					<?php $arr= $this->Common->vacation_effect_previous_servicecall($res['InspectorVacation']['user_id'],$res['InspectorVacation']['start_date'],$res['InspectorVacation']['end_date']);
					if(!empty($arr))
					{
						echo "Yes";
					}
					else
					{
						echo "No";
					}
					?>
					</td>
					<td align="center"><?php echo $i;?></td>
                </tr>
                <?php
                $i++;
                } ?>
                </table>          
                
            </div>
                
                
</div>          
