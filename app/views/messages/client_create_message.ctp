<?php 
    echo $this->Html->script('jquery.1.6.1.min.js');     
?>
<?php 
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>

<script type="text/javascript">
//	$.noConflict();
	jQuery(document).ready(function()
	{
		
	jQuery("#composeMszForm").validationEngine();
	});
</script>
<div class="register-wrap">
	      <div class='message'><?php echo $this->Session->flash();?></div>
              <div id="box" >
          <?php echo $form->create('Messages',array('controller'=>'Messages','action' => 'create_message','onsubmit'=>'return formvalidate();','method'=>'post','id'=>'composeMszForm','name'=>'ListForm'));
                echo $form->input('Message.sender_id',array('type' => 'hidden','value'=>$session->read('client_id'))); 
	              //$spid = $spdata['User']['sp_id'];
                //$sp_name = $this->Common->getServiceProviderName($spid);
                //echo $form->input('Message.receiver_id',array('type' => 'hidden','value'=>$spid));
		$listData = array();
		foreach($spAllData as $datas){
		    $listData[$datas['User']['id']]=$datas['User']['fname'].' '.$datas['User']['lname'].'('.$datas['User']['email'].')';
		}
          ?>
		<div class="form-box" style="width:90%;">
		    <br/>
		    <h2>Compose Message</h2>
		    <ul class="register-form">
			<li>
			    <label>To:<span>*</span></label>
			    <!--<p><span class="input-field"><?php //echo $form->input('Message.receiver_name',array('type'=>'text',"id" => "creditsearchresult",'value'=>$sp_name,'div'=>false,'label'=>false,'class'=>'validate[required]', "autocomplete" => "off")); ?></span></p>-->
			    <p><?php echo $form->select('Message.receiver_id',$listData,'',array('class'=>'validate[required]','style'=>'width:250px;','empty'=>'Please Select')); ?></p>
			</li>
			<li>
			    <label>Subject:<span>*</span></label>
			    <p><span class="input-field"><?php echo $form->input('Message.message_subject',array('size'=>'45','maxlength'=>'100','div'=>false,'label'=>false,'value'=>'','class'=>'validate[required]')); ?></span></p>
			</li>
			<li>
			    <label>Message:<span>*</span></label>
			    <p><span class=""><?php echo $form->input('Message.message_body',array('type'=>'textarea','rows'=>7,'cols'=>35,'label'=>false,'div'=>false,'class'=>'validate[required]')); ?></span></p>
			</li>		
			<li>
			<label>&nbsp;</label>
			    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Send',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>   
			</li>
			</ul>
		    </div>
	      <?php echo $form->end(); ?>
                </div>
</div>


