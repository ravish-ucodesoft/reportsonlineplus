<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$commonPart = $this->element('reports/report_page_common_view');
$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center">Report Page</h3></td>          
        </tr>      
    </table>';
$html.=$commonPart;
$html.='<h3 class="main-heading" align="center" style="width:100%">SPECIALHAZARD REPORT RECORD</h3>
<table width="100%" class="qtytable tablesorter" id="sortTable" border="1" style="font-size:20px;">
    <thead>
        <tr style="background-color:#9FBFDF; color:#000000;">
            <th width="5%" style="text-align:center;">Sr. No.</th>
            <th width="25%" style="text-align:center;">Location</th>
            <th width="25%" style="text-align:center;">Type</th>	    
            <th width="25%" style="text-align:center;">Born Date</th>
            <th width="20%" style="text-align:center;">Deficient</th>            
        </tr>                
    </thead>
    <tbody>';
    
    if(!empty($record['SpecialhazardReportRecord'])){
	$reportCounter=0;
        foreach($record['SpecialhazardReportRecord'] as $recorddata){
		$reportCounter++;
		$tdColor = (($recorddata['deficiency']=='Yes')?'red':'#3D3D3D');
$html.='<tr style="font-size:20px; background-color:#FFFFFF; color:'.$tdColor.';">
            <td width="5%" style="text-align:center;">'.$reportCounter.'.</td>
	    <td width="25%" style="text-align:center;">'.$recorddata['location'].'</td>
            <td width="25%" style="text-align:center;">'.$this->Common->getServiceName($recorddata['type']).'</td>
            <td width="25%" style="text-align:center;">'.$recorddata['born_date'].'</td>
            <td width="20%" style="text-align:center;">'.$recorddata['deficiency'].'</td>                        
        </tr>';
    } } 
$html.='</tbody>
</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Specialhazard_Report_ReportPage.pdf', 'I');die;
?>