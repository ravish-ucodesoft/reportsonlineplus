<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$commonPart = $this->element('reports/report_page_common_view');
$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Report Page</h3></td>          
        </tr>      
    </table>';
    $html.='<table cellpadding="0" cellspacing="0" border="0" class="table_new" width="100%">
		<tr>		   
			<td align="right"><b>Inspection Type: '.ucfirst($record['SprinklerReport']['inspection_type']).'</b></td>
		</tr>
	    </table>';
    $html.=$commonPart;
$html.='<table cellpadding="1" cellspacing="1" border="1" class="table_new" width="100%">			    			    
		<tr>
		  <td valign="top">
			 <table width="100%" cellpadding="1" cellspacing="0" border="0">
			 <tr>
			    <td class="table_label"><strong>Date of Work</strong></td>
			    <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['date_of_work']).'</td>
			  </tr>				      
			  <tr>
			    <td class="table_label"><strong>Start Date of the Service</strong></td>
			    <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['start_date_service']).'</td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>End Date of the Service</strong></td>
			    <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['end_date_service']).'</td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Date of Performed Work </strong></td>
			    <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['date_performed_work']).'</td>
			  </tr>
			 </table>
		  </td>
		  <td valign="top">
		      <table width="100%" cellpadding="1" cellspacing="0" border="0">
			 <tr>
			    <td class="table_label"><strong>Deficiency yes or no </strong></td>
			    <td>'.$record['SprinklerReport']['deficiency'].'</td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Recommendations yes or no</strong></td>
			    <td>'.$record['SprinklerReport']['recommendations'].'</td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Inspection 100% complete and Passed</strong></td>
			    <td>'.$record['SprinklerReport']['inspection'].'</td>
			  </tr>				      
			 </table>
		  </td>
		</tr>    
	    </table>';    


$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Sprinkler_Report_ReportPage.pdf', 'I');die;
?>