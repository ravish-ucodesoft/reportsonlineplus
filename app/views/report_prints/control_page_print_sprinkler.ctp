<?php
	echo $this->Html->script('accordian.pack');	
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$element=$this->element('reports/report_common_view');
$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Control Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
            <tr>
                <td colspan="3"><h3>2. CONTROL VALVES</h3></td>
            </tr>	   
            <tr>
                <td colspan="2">a. Are all sprinkler system main control valves and all other valves in the appropriate open or closed position?</td>
                <td>'.$record['SprinklerReportControlvalve']['cvalve_a'].'</td>
            </tr>
            <tr>
                <td colspan="2">b. Are all control valves sealed or supervised in the appropriate position?</td>
                <td>'.$record['SprinklerReportControlvalve']['cvalve_b'].'</td>
            </tr>
        </table>
        <table class="tbl">
            <tr>
              <td  width="10%">Control Valves</td>
              <td  width="10%"># of Valves</td>
              <td  width="10%">Type</td>
              <td  width="10%">Easily Accessible</td>
              <td  width="10%">Signs</td>
              <td  width="10%">Valve Open</td>
              <td  width="10%">Secured? </td>
              <td  width="10%"> (Seal.?)/(Lock.?)/(Supvd.?)</td>
              <td  width="10%">Supervision Operational</td>
            </tr>
            <tr>
              <td>CITY CONN.</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_cconnection_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_supervision'].'</td>
            </tr>
            <tr>
              <td>TANK</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_tank_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_supervision'].'</td>
            </tr>
            <tr>
              <td>PUMP</td>              
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_pump_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_supervision'].'</td>
            </tr>
            <tr>
              <td>SECTIONAL</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_sectional_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_supervision'].'</td>
            </tr>
            <tr>
              <td>SYSTEM</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_system_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_supervision'].'</td>
            </tr>
            
            <tr>
              <td>ALARM LINE</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_alarm_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_supervision'].'</td>
            </tr>
        </table>';
$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Sprinkler_Report_ControlPage.pdf', 'I');die;
?>