<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<section class="register-wrap">
    <h1 class="main-hdng">Report Text</h1>    
    <div class='message' style="text-align:center;font-weight:bold;"><?php echo $this->Session->flash();?></div>
    <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'reportText'),'id'=>'report_text')); ?>
    <?php echo $form->input('SpReportText.id',array('type'=>'hidden','value'=>$res['SpReportText']['id'])); ?>     
    <section class="form-box" style="width:90%;">        
        <ul class="register-form" style="border: none;">
            <li>                
                <?php echo $form->input('SpReportText.report_text', array('class'=>'validate[required]','type'=>'textarea','rows' => 7,'cols' =>60,'div'=>false,'label'=>false,'value'=>$res['SpReportText']['report_text']));  		
		?>
            </li>
            <li>
                <label style="width: 340px;">&nbsp;</label>
                <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>               
            </li> 
        </ul>
    </section>   
    <?php echo $form->end(); ?>
</section>
<script type="text/javascript">
    jQuery(document).ready(function(){    
        jQuery("#report_text").validationEngine();
    });
</script>