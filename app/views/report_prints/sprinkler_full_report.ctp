<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');        
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
$element=$this->element('reports/report_common_view');
$element_cover=$this->element('reports/report_common_cover',array('report_name'=>'Sprinkler Report','finish_date'=>$record['SprinklerReport']['finish_date'],'freq'=>$schFreq));
$commonPart = $this->element('reports/report_page_common_view');
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setHeaderData('../../../webroot/img/company_logo/'.$spResult['Company']['company_logo'],21,'','');
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->setPrintHeader(true);
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];

//Cover page
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>';
$html.=$element_cover;

//Report Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Report Page</h3></td>          
        </tr>      
    </table>';
    $html.='<table cellpadding="0" cellspacing="0" border="0" class="table_new" width="100%">
		<tr>		   
			<td align="right"><b>Inspection Type: '.ucfirst($record['SprinklerReport']['inspection_type']).'</b></td>
		</tr>
	    </table>';
    $html.=$commonPart;
$html.='<table cellpadding="1" cellspacing="1" border="1" class="table_new" width="100%">			    			    
            <tr>
              <td valign="top">
                     <table width="100%" cellpadding="1" cellspacing="0" border="0">
                     <tr>
                        <td class="table_label"><strong>Date of Work</strong></td>
                        <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['date_of_work']).'</td>
                      </tr>				      
                      <tr>
                        <td class="table_label"><strong>Start Date of the Service</strong></td>
                        <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['start_date_service']).'</td>
                      </tr>
                      <tr>
                        <td class="table_label"><strong>End Date of the Service</strong></td>
                        <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['end_date_service']).'</td>
                      </tr>
                      <tr>
                        <td class="table_label"><strong>Date of Performed Work </strong></td>
                        <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['date_performed_work']).'</td>
                      </tr>
                     </table>
              </td>
              <td valign="top">
                  <table width="100%" cellpadding="1" cellspacing="0" border="0">
                     <tr>
                        <td class="table_label"><strong>Deficiency yes or no </strong></td>
                        <td>'.$record['SprinklerReport']['deficiency'].'</td>
                      </tr>
                      <tr>
                        <td class="table_label"><strong>Recommendations yes or no</strong></td>
                        <td>'.$record['SprinklerReport']['recommendations'].'</td>
                      </tr>
                      <tr>
                        <td class="table_label"><strong>Inspection 100% complete and Passed</strong></td>
                        <td>'.$record['SprinklerReport']['inspection'].'</td>
                      </tr>				      
                     </table>
              </td>
            </tr>    
        </table>';
        
//General Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >General Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
    <tr>
        <td colspan="2"><h3>1. GENERAL(Answered by Customer.)</h3></td>
    </tr>
    <tr>
        <td width="85%">a. Have there been any changes in the occupancy classification, machinery or operations since the last inspection?</td>
        <td>'.$record['SprinklerReportGeneral']['general_a'].'</td>
    </tr>
        <tr>
        <td width="85%">b. Have there been any changes or repairs to the fire protection systems since the last inspection?</td>
        <td>'.$record['SprinklerReportGeneral']['general_b'].'</td>
    </tr>
    <tr>
        <td width="85%">c. If a fire has occurred since the last inspection, have all damaged sprinkler system components been replaced?</td>
        <td>'.$record['SprinklerReportGeneral']['general_c'].'</td>
    </tr>    
    <tr>
        <td width="85%">d. Has the piping in all dry systems been checked for proper pitch within the past five years?</td>
        <td>'.$record['SprinklerReportGeneral']['general_d'].'</td>
    </tr>
    <tr>
        <td width="85%">Date last checked:&nbsp;&nbsp;'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_d_date']).'&nbsp;&nbsp;(check recommended at least every 5 years)</td>
        <td>'.$record['SprinklerReportGeneral']['general_d_1'].'</td>
    </tr>
    <tr>
        <td width="85%">e. Has the piping in all systems been checked for obstructive materials?</td>
        <td>'.$record['SprinklerReportGeneral']['general_e'].'</td>
    </tr>
    <tr>
        <td width="85%">Date last checked:&nbsp;&nbsp;'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_e_date']).'&nbsp;&nbsp;(check recommended at least every 5 years)</td>
        <td>'.$record['SprinklerReportGeneral']['general_e_1'].'</td>
    </tr>
    </table>
    <table>
    <tr>
        <td width="85%">f. Have all fire pumps been tested to full capacity using hose streams or flow meters within the past 12 months?</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_f_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_f'].'</td>
    </tr>
    <tr>
        <td colspan="2">g. Are gravity, surface or pressure tanks protected from freezing?</td>
        <td>'.$record['SprinklerReportGeneral']['general_g'].'</td>
    </tr>
    <tr>
        <td colspan="2">h. Are sprinklers newer than 50 years old? QR (20yr) Dry (10 yr) >325F/163C (5yr) Corrosive env\'t. (5yr.)</td>
        <td>'.$record['SprinklerReportGeneral']['general_h'].'</td>
    </tr>            
    <tr>
        <td colspan="2">(Testing or replacement required for sprinklers past these age limits.)
        i. Are extra high temperature solder sprinklers free from regular exposure to temperatures near 300F/149C?</td>
        <td>'.$record['SprinklerReportGeneral']['general_i'].'</td>
    </tr>
    <tr>
        <td width="85%">j. Have gauges been tested, calibrated or replaced in the last 5 years?</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_j_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_j'].'</td>
    </tr>
    <tr>
        <td width="85%">k. Alarm valves and associated trim been internally inspected past 5 years?</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_k_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_k'].'</td>
    </tr>
    <tr>
        <td width="85%">l. Check valves internally inspected in the last 5 years?</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_l_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_l'].'</td>
    </tr>
    <tr>
        <td width="85%">m. Has the private fire main been flow tested in last 5 years? </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_m_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_m'].'</td>
    </tr>
    <tr>
        <td width="85%">n. Standpipe 5 and 3 year requirements.</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n'].'</td>
    </tr>
    <tr>
        <td width="85%">1. Dry standpipe hydrostatic test  </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_1_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n_1'].'</td>
    </tr>
    <tr>
        <td width="85%">2. Flow test </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_2_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n_2'].'</td>
    </tr>            
    <tr>
        <td width="85%">3. Hose hydrostatic test (5 years from new, every 3 years after)</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_3_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n_3'].'</td>
    </tr>
    <tr>
        <td width="85%">4. Pressure reducing/control valve test </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_4_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n_4'].'</td>
    </tr>
    <tr>
        <td width="85%">o. Have pressure reducing/control valves been tested at full flow within the past 5 years? </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_o_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_o'].'</td>
    </tr>
    <tr>
        <td colspan="2">p. Have master pressure reducing/control valves been tested at full flow within the past 1 year?</td>
        <td>'.$record['SprinklerReportGeneral']['general_p'].'</td>
    </tr>
    <tr>
        <td colspan="2">q. Have the sprinkler systems been extended to all areas of the building?</td>
        <td>'.$record['SprinklerReportGeneral']['general_q'].'</td>
    </tr>
    <tr>
        <td colspan="2">r. Are the building areas protected by a wet system heated, including its blind attics and perimeter areas?</td>
        <td>'.$record['SprinklerReportGeneral']['general_r'].'</td>
    </tr>
    <tr>
        <td colspan="2">s. Are all exterior openings protected against the entrance of cold air?</td>
        <td>'.$record['SprinklerReportGeneral']['general_s'].'</td>
    </tr>            			    
</table>';

//Control Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Control Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
            <tr>
                <td colspan="3"><h3>2. CONTROL VALVES</h3></td>
            </tr>	   
            <tr>
                <td colspan="2">a. Are all sprinkler system main control valves and all other valves in the appropriate open or closed position?</td>
                <td>'.$record['SprinklerReportControlvalve']['cvalve_a'].'</td>
            </tr>
            <tr>
                <td colspan="2">b. Are all control valves sealed or supervised in the appropriate position?</td>
                <td>'.$record['SprinklerReportControlvalve']['cvalve_b'].'</td>
            </tr>
        </table>
        <table class="tbl">
            <tr>
              <td  width="10%">Control Valves</td>
              <td  width="10%"># of Valves</td>
              <td  width="10%">Type</td>
              <td  width="10%">Easily Accessible</td>
              <td  width="10%">Signs</td>
              <td  width="10%">Valve Open</td>
              <td  width="10%">Secured? </td>
              <td  width="10%"> (Seal.?)/(Lock.?)/(Supvd.?)</td>
              <td  width="10%">Supervision Operational</td>
            </tr>
            <tr>
              <td>CITY CONN.</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_cconnection_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_supervision'].'</td>
            </tr>
            <tr>
              <td>TANK</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_tank_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_supervision'].'</td>
            </tr>
            <tr>
              <td>PUMP</td>              
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_pump_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_supervision'].'</td>
            </tr>
            <tr>
              <td>SECTIONAL</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_sectional_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_supervision'].'</td>
            </tr>
            <tr>
              <td>SYSTEM</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_system_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_system_supervision'].'</td>
            </tr>
            
            <tr>
              <td>ALARM LINE</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_valves'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_type'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_access'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_signs'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_vopen'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_secured'].'</td>
              <td> '.$record['SprinklerReportControlvalve']['cvalve_alarm_option'].'</td>
              <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_supervision'].'</td>
            </tr>
        </table>';
        
//Water Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Water Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
            <tr>
                <td colspan="3"><h3>3. WATER SUPPLIES</h3></td>
            </tr>
        </table>
        <table class="tbl" width="100%" cellpadding="1" cellspacing="1">
            <tr>
              <td width="20%"> a. Water supply sources? City:</td>
              <td width="20%">'.$record['SprinklerReportWatersupply']['wsupply_city'].'</td>
              <td width="20%">Gravity Tank:</td>
              <td width="20%">'.$record['SprinklerReportWatersupply']['wsupply_tank'].'</td>
              <td width="20%"></td>
            </tr>
            <tr>
              <td colspan="5">Water Flow Test Results Made During This Inspection</td>
            </tr>				
            <tr>
              <td>Test Pipe Located</td>
              <td>Size Test Pipe</td>
              <td>Static Pressure Before</td>
              <td>Flow Pressure</td>
              <td>Static Pressure After</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_1'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_1'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_1'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_1'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_1'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_2'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_2'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_2'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_2'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_2'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_3'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_3'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_3'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_3'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_3'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_4'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_4'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_4'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_4'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_4'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_5'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_5'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_5'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_5'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_5'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_6'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_6'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_6'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_6'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_6'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_7'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_7'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_7'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_7'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_7'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_8'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_8'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_8'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_8'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_8'].'</td>
            </tr>
        </table>';

//Connection Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Connection Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl">		
            <tr>
                <td colspan="3"><h3>4. TANKS, PUMPS, FIRE DEPT. CONNECTION</h3></td>
            </tr>
        </table>	
        <table class="tbl">
            <tr>
                <td width="95%" colspan="2">a. Do fire pumps, gravity, surface or pressure tanks appear to be in good external conditions?</td>
                <td>';
                if(!empty($record['SprinklerReportConnection']['connection_a'])){$html.=$record['SprinklerReportConnection']['connection_a'];}
                $html.='</td>
            </tr>
            <tr>
                <td width="95%" colspan="2">b. Are gravity, surface and pressure tanks at the proper pressure and/or water levels?</td>
                <td>';if(!empty($record['SprinklerReportConnection']['connection_b'])){$html.=$record['SprinklerReportConnection']['connection_b'];}
                $html.='</td>
            </tr>
            <tr>
                <td width="85%">c. Has the storage tank been internally inspected in the last 3 yrs. (unlined) or 5 yrs. (lined)? </td>
                <td width="10%">';if(!empty($record['SprinklerReportConnection']['connection_c_date'])){$html.=$time->Format('m-d-Y',$record['SprinklerReportConnection']['connection_c_date']);} 
                $html.='</td>
                <td>';if(!empty($record['SprinklerReportConnection']['connection_c'])){$html.=$record['SprinklerReportConnection']['connection_c'];}
                $html.='</td>
            </tr>
            <tr>
                <td colspan="2" width="95%">d. Are fire dept. connections in satisfactory condition, couplings free, caps or plugs in place and check valves tight?</td>
                <td>';if(!empty($record['SprinklerReportConnection']['connection_d'])){$html.=$record['SprinklerReportConnection']['connection_d'];}
                $html.='</td>
            </tr>
            <tr>
                <td width="95%" colspan="2">e. Are fire dept. connections visible and accessible?</td>
                <td>';if(!empty($record['SprinklerReportConnection']['connection_e'])){$html.=$record['SprinklerReportConnection']['connection_e'];}
                $html.='</td>
            </tr>
        </table>';

//Wet Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Wet System Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl">		
            <tr>
                <td colspan="3"><h3> 5. WET SYSTEMS</h3></td>
            </tr>
        </table>
        <table class="tbl" width="100%">
            <tr>
                <td width="25%">a. No. of systems:</td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_a'].'</td>
                <td width="25%">Make & Model : </td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_a_makemodel'].'</td>
            </tr>
            <tr>
                <td colspan="3" width="75%">b. Are cold weather valves in the appropriate open or closed position?</td>
                <td>'.$record['SprinklerReportWetsystem']['wetsystem_b'].'</td>
            </tr>
            <tr>
                <td colspan="3" width="75%">If closed, has piping been drained ?</td>
                <td>'.$record['SprinklerReportWetsystem']['wetsystem_b_1'].'</td>
            </tr>
            <tr>
                <td colspan="3" width="75%">c. Has the Customer been advised that cold weather valves are not recommended?</td>
                <td>'.$record['SprinklerReportWetsystem']['wetsystem_c'].'</td>
            </tr>
            <tr>
                <td colspan="2" width="50%">d. Have all the antifreeze systems been tested in the past year?</td>
                <td width="25%">'.$time->Format('m-d-Y',$record['SprinklerReportWetsystem']['wetsystem_d_date']).'</td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_d'].'</td>
            </tr>
            <tr>
                <td colspan="4">The antifreeze tests indicated protection to:</td>
            </tr>
            <tr>
                <td width="25%">system 1 </td>
                <td width="25%" style="text-align:right"><b>Temperature</b></td>
                <td width="25%" style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_1_temp'].'</td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_1'].'</td>
            </tr>
            <tr>
                <td width="25%">system 2 </td>
                <td width="25%" style="text-align:right"><b>Temperature</b></td>
                <td width="25%" style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_2_temp'].'</td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_2_temp'].'</td>
            </tr>
            <tr>
                <td width="25%">system 3 </td>
                <td width="25%" style="text-align:right"><b>Temperature</b></td>
                <td width="25%" style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_3_temp'].'</td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_3_temp'].'</td>
            </tr>
            <tr>
                <td width="25%">system 4 </td>
                <td width="25%" style="text-align:right"><b>Temperature</b></td>
                <td width="25%" style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_4_temp'].'</td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_4_temp'].'</td>
            </tr>
            <tr>
                <td width="25%">system 5 </td>
                <td width="25%" style="text-align:right"><b>Temperature</b></td>
                <td width="25%" style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_5_temp'].'</td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_5_temp'].'</td>
            </tr>
            <tr>
                <td width="25%">system 6 </td>
                <td width="25%" style="text-align:right"><b>Temperature</b></td>
                <td width="25%" style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_6_temp'].'</td>
                <td width="25%">'.$record['SprinklerReportWetsystem']['wetsystem_6_temp'].'</td>
            </tr>
            <tr>
                <td colspan="3" width="75%">e. Did alarm valves, water flow alarm devices and retards test satisfactorily?</td>
                <td>'.$record['SprinklerReportWetsystem']['wetsystem_e'].'</td>
            </tr>
        </table>';

//Dry Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Dry System Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
            <tr>
                <td colspan="3"><h3>6. DRY SYSTEM</h3></td>
            </tr>
        </table>
        <table class="tbl">
          <tr>
            <td width="20%">a. No. of systems:	</td>
            <td width="10%">'.$record['SprinklerReportDrysystem']['drysystem_a'].'</td>
            <td width="20%">Make & Model :	Partial</td>
            <td width="10%">'.$record['SprinklerReportDrysystem']['drysystem_a_makemodel'].'</td>
            <td width="10%">Full</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_a_full'].'</td>            
          </tr>
          <tr>
            <td width="40%"> Date last trip tested:</td>
            <td width="60%">'.$record['SprinklerReportDrysystem']['drysystem_a_date'].'</td>
          </tr>
          <tr>
            <td colspan="5" width="70%">b. Are the air pressure(s) and priming water level(s) normal?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_b'].'</td>
          </tr>
          <tr>
            <td colspan="5" width="70%">c. Did the air compressor(s) operate satisfactorily?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_c'].'</td>
          </tr>
          <tr>
            <td colspan="5" width="70%">d. Air compressor(s) oil checked?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_d'].'</td>
          </tr>
          <tr>
            <td width="70%" colspan="5">Belt(s)?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_d_belt'].'</td>
          </tr>
          <tr>
            <td colspan="5" width="70%">e. Were Low Point drains drained during this inspection?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_e'].'</td>
          </tr>
          <tr>
            <td colspan="5" width="70%">No. of Drains:</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_e_drains'].'</td>
          </tr>
          <tr>
            <td colspan="6">Locations:</td>
          </tr>
          <tr>
            <td width="20%">1.</td>
            <td width="10%">'.$record['SprinklerReportDrysystem']['drysystem_e_locations_1'].'</td>
            <td width="20%">2.</td>
            <td width="10%">'.$record['SprinklerReportDrysystem']['drysystem_e_locations_2'].'</td>
            <td colspan="2"></td>
          </tr>
          <tr>
            <td width="20%">3.</td>
            <td width="10%">'.$record['SprinklerReportDrysystem']['drysystem_e_locations_3'].'</td>
            <td width="20%">4.</td>
            <td width="10%">'.$record['SprinklerReportDrysystem']['drysystem_e_locations_4'].'</td>
            <td colspan="2"></td>
          </tr>
          <tr>
            <td width="70%"colspan="5">f. Did all quick opening devices operate satisfactorily?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_f'].'</td>
          </tr>
          <tr>
            <td colspan="5" width="70%">Make & Model:?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_f_makemodel'].'</td>
          </tr>
          <tr>
            <td  colspan="5" width="70%">g. Did all the dry valves operate satisfactorily during this inspection?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_g'].'</td>
          </tr>
          <tr>
            <td  colspan="5" width="70%">h. Is the dry valve house heated?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_h'].'</td>
          </tr>
          <tr>
            <td colspan="5" width="70%">i. Do the dry valves appear to be protected from freezing?</td>
            <td width="30%">'.$record['SprinklerReportDrysystem']['drysystem_i'].'</td>
          </tr>
        </table>';

//Special Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Special System Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
            <tr>
                <td colspan="3"><h3>7. SPECIAL SYSTEMS</h3></td>
            </tr>
        </table>
        <table class="tbl" width="100%">		    
            <tr>  
              <td width="25%">  a. No. of systems:</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_a'].'</td>
              <td width="25%">Make & Model :</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_a_makemodel'].'</td>
            </tr>
            <tr>
              <td width="75%" colspan="3"> Type:</td>
              <td width="75%">'.$record['SprinklerReportSpecialsystem']['specialsystem_a_type'].'</td>
            </tr>           
            <tr>
              <td width="75%" colspan="3">b. Were valves tested as required?</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_b'].'</td>
            </tr>
            <tr>
              <td width="75%" colspan="3">c. Did all heat responsive systems operate satisfactorily?</td>
              <td width="25%">'. $record['SprinklerReportSpecialsystem']['specialsystem_c'].'</td>
            </tr>
            <tr>
              <td width="75%" colspan="3">d. Did the supervisory features operate during testing?</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_d'].'</td>
            </tr>          
            <tr>
              <td width="25%">Heat Responsive Devices:</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_heat'].'</td>
              <td width="25%">Type	</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_type'].'</td>
            <tr>
              <td width="75%" colspan="3">Type of test</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_type_test'].'</td>
            </tr>
            <tr>
              <td width="50%" colspan="2">Valve No.</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_valve_1'].'</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_valve_1'].'</td>              
            </tr>
            <tr>
              <td width="50%" colspan="2">Seconds</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_second_1'].'</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_second_2'].'</td>
            </tr>
            <tr>
              <td colspan="4"> e. Has a supplemental test form for this system been completed and provided to the customer?     (Please attach)</td>
            </tr>          
            <tr>
              <td colspan="4">Auxiliary equipment: </td>
            </tr>
            <tr>
              <td width="25%">No.</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_e_number'].'</td>
              <td width="25%">Type</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_e_type'].'</td>
            </tr>
            <tr>
              <td width="25%">Location</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_e_location'].'</td>
              <td width="25%">Test results</td>
              <td width="25%">'.$record['SprinklerReportSpecialsystem']['specialsystem_e_test_result'].'</td>
            </tr>          
          </table>';

//Alarms Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Alarm Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
            <tr>
                <td colspan="3"><h3>8. ALARMS</h3></td>
            </tr>
        </table>
        <table class="tbl" width="100%">
            <tr>
                <td>a. Did the water motor(s) and gong(s) operate during testing?</td>
                <td>'.$record['SprinklerReportAlarm']['alarm_a'].'</td>				    
            </tr>
            <tr>
                <td>b. Did the electric alarm(s) operate during testing? </td>
                <td>'.$record['SprinklerReportAlarm']['alarm_b'].'</td>
            </tr>
            <tr>
                <td>c. Did the supervisory alarm(s) operate during testing?</td>
                <td>'.$record['SprinklerReportAlarm']['alarm_c'].'</td>				    
            </tr>
        </table>';

//Sprinklers Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Sprinkler-Piping Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
            <tr>
                <td colspan="3"><h3>9. SPRINKLERS - PIPING</h3></td>
            </tr>
        </table>
        <table class="tbl" width="100%">
        <tr>
            <td width="70%">a. Do sprinklers generally appear to be in good external condition?</td>
            <td>'.$record['SprinklerReportPiping']['piping_a'].'</td>				    
        </tr>
        <tr>
            <td width="70%">b. Do sprinklers generally appear to be free of corrosion, paint, or loading and visible obstructions? </td>
            <td>'.$record['SprinklerReportPiping']['piping_b'].'</td>
        </tr>
        <tr>
            <td width="70%">c. Are extra sprinklers and sprinkler wrench available on the premises? </td>
            <td>'.$record['SprinklerReportPiping']['piping_c'].'</td>				    
        </tr>
        <tr>
            <td width="70%">d. Does the exposed exterior condition of piping, drain valves, check valves, hangers, pressure gauges, open sprinklers and strainers appear to be satisfactory? </td>
            <td>'.$record['SprinklerReportPiping']['piping_d'].'</td>				    
        </tr>
        <tr>
            <td width="70%">e. Does the hand hose on the sprinkler system appear to be in satisfactory condition? </td>
            <td>'.$record['SprinklerReportPiping']['piping_e'].'</td>				    
        </tr>
        <tr>
            <td width="70%">f. Does there appear to be proper clearance between the top of all storage and the sprinkler deflector?</td>
            <td>'.$record['SprinklerReportPiping']['piping_f'].'</td>				    
        </tr>
        </table>';

//Deficiency page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%">    	
    <tr>
        <td align="center" colspan="2" style="font-size: 36px; color:red"><b>Deficiencies</b></td>
    </tr>';
        $html.=$element;
$html.='</table>
<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
    <tr>
        <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
	<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
        <th width="60%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
	<th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Attachment</b></th>           
    </tr>';    
    if(!empty($Def_record))
    {       
        $defCount = 0;
	foreach($Def_record as $Def_record) {
		$defCount++;
    $html.='<tr>
	<td valign="top">'.$defCount.'.</td>
        <td valign="top">'.$Def_record['Code']['code'].'</td>
        <td valign="top">'.$Def_record['Code']['description'].'</td>
	<td valign="top">';
		if(!empty($Def_record['Deficiency']['attachment']) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$Def_record['Deficiency']['attachment'])){
		    //$html.=$html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$Def_record['Deficiency']['attachment'],'sp'=>false),array('title'=>'Click to Download'));
		    $html.='Attachment'; 
		}else{
		    $html.='No Attachment';
		}
	$html.='</td>
    </tr>';
        }
    }
    else
    {
        $html.='<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';   
    }    
$html.='</table>';

//Recommendation Page
$reportName='Sprinkler';
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px; color:orange"><b>Recommendation</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: '.$reportName.'</b></div>
<br/>
<table width="100%">
    <tr>
        <td align="right" colspan="2"></td>
    </tr>';
$html.=$element;		
$html.='</table>    
<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
    <tr>        
        <th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
        <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
        <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>        
    </tr>';    
    if(!empty($Def_record_recomm))
    {
        foreach($Def_record_recomm as $Def_record_recomm) {    
        $html.='<tr>        
            <td valign="top">'.$Def_record_recomm['Code']['code'].'</td>
            <td valign="top">'.$Def_record_recomm['Code']['description'].'</td>
            <td valign="top">'.$Def_record_recomm['Deficiency']['recommendation'].'</td>        
        </tr>';
        }
    }
    else
    {
        $html.='<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
    }    
$html.='</table>';

//Signature page
$modelName = 'SprinklerReport';
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
                <td colspan="2" align="center" style="border:none;"><span style="font-size:36px;"><b>Signature Page</b></span></td>                
        </tr>	   
</table>

<table width="100%" cellpadding="0" cellspacing="0">';
$html.=$element;
$html.='</table>

<table width="100%">
        <tr>
                <td style="border:none;"><b>Inspector Signature</b></td>
                <td style="border:none; vertical-align:top;" align="right"><b>Client Signature</b></td>	  
        </tr>
        <tr>
            <td style="border:none">';
                $name=$this->Common->getInspectorSignature($record[$modelName]['report_id'],$record[$modelName]['servicecall_id'],$record[$modelName]['inspector_id']);
                $html.='<img src="/app/webroot/img/signature_images/'.$name.'">';                
                $html.='</td>
            <td style="border:none;" align="right">&nbsp;</td>    
        </tr>
        <tr>
                <td style="border:none;">';
                $dateCreated=$this->Common->getSignatureCreatedDate($record[$modelName]['report_id'],$record[$modelName]['servicecall_id'],$record[$modelName]['inspector_id']);		  
                if($dateCreated!='empty'){
                        $html.=$this->Common->getClientName($record[$modelName]['inspector_id'])."<br/><br/>";
                        $html.=$time->Format('m-d-Y',$dateCreated);
                }                
                $html.='</td>
                <td align="right" style="border:none;">';
                //$dateCreated=$this->Common->getSignatureCreatedDate($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id']);		  
                //if($dateCreated!='empty'){
                        $html.=$this->Common->getClientName($record[$modelName]['client_id'])."<br/><br/>";
                        //$html.=$time->Format('m-d-Y',$dateCreated);
                //}                
                $html.='</td>
        </tr>
</table>';

//Certification Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px;"><b>Certificates</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: '.$reportName.'</b></div>
<br/>
<table width="100%">';
$html.=$element;
$html.='</table>

<table class="tbl input-chngs1" width="100%" >                
        <tr>
                <td>&nbsp;</td>
        </tr>
	<tr>
                <td align="left" style="text-align:left">';                
                $certs = $this->Common->getAllSchCerts($schId);						
                if(sizeof($certs)>0){                
                    $cr=1;
                    foreach($certs as $cert)
                    {
                        $html.='<b>'.$cr.'.'.$cert['ReportCert']['ins_cert_title'].'</b>';
                        $html.='<br/>';                    
                        $cr++;
                    }               
                }                
                $html.='</td>
        </tr>	
</table>';

//Quotes Page
$html.='<hr style="color:#fff"/>';
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px;"><b>Quotes</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: '.$reportName.'</b></div>
<br/>
<table width="100%">';
$html.=$element;
$html.='</table>

<table class="tbl input-chngs1" width="100%" >                
        <tr>
                <td align="left" style="text-align:left">
                        <div><b>Quote Form from SP</b></div>';                        
                        if(isset($chkRecord) && !empty($chkRecord)){
                                $html.=$chkRecord['Quote']['title'];
                        }else{
                                $html.='No Quote have been submitted by Service Provider.';
                        }                        
                $html.='</td>
        </tr>
        <tr>
                <td>&nbsp;</td>
        </tr>
        <tr>
                <td align="left" style="text-align:left">
                <div><b>Client\'s Response to Quote Form</b></div>';                
                $quotedocs = $this->Common->getQuoteDoc($_REQUEST['reportID'],$_REQUEST['clientID'],$_REQUEST['spID'],$_REQUEST['serviceCallID']);                
                if(isset($quotedocs) && !empty($quotedocs)){
                        if($quotedocs['Quote']['client_response']=='a'){
                                $status = 'Accepted';
                        }else if($quotedocs['Quote']['client_response']=='d'){
                                $status = 'Denied';
                        }else if($quotedocs['Quote']['client_response']=='n'){
                                $status = 'No wish to fix the deficiencies';
                        }else{
                                $status = 'Pending';
                        }                        
                        $html.='<div>Status: '.$status.'</div>';
                        if($quotedocs['Quote']['client_response']=='a'){
                        $html.='<div class="certificates-hdline" style="text-align: left;">';
                            $html.='Signed Quote Form';
                        $html.='</div>';
                        }else if($quotedocs['Quote']['client_response']=='d'){
                        $html.='<div class="certificates-hdline" style="text-align: left;">';
                            $html.='Work Orders';
                        $html.='</div>';
                        }
                        
                }else{
			$html.='NA';
		}
                $html.='</td>
        </tr>
</table>';


$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
//$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;


$delimiter = '<hr style="color:#fff"/>';
$chunks    = explode($delimiter, $html);
$cnt       = count($chunks);

for ($i = 0; $i < $cnt; $i++) {
    $tcpdf->writeHTML($chunks[$i], true, 0, true, 0);

    if ($i < $cnt - 1) {
        $tcpdf->AddPage();
    }
}

// Reset pointer to the last page
$tcpdf->lastPage();


// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Sprinkler_Report.pdf', 'I');die;
?>