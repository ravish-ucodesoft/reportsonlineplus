<?php
	echo $this->Html->script('accordian.pack');	
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$element=$this->element('reports/report_common_view');
$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >General Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
    <tr>
        <td colspan="2"><h3>1. GENERAL(Answered by Customer.)</h3></td>
    </tr>
    <tr>
        <td width="85%">a. Have there been any changes in the occupancy classification, machinery or operations since the last inspection?</td>
        <td>'.$record['SprinklerReportGeneral']['general_a'].'</td>
    </tr>
        <tr>
        <td width="85%">b. Have there been any changes or repairs to the fire protection systems since the last inspection?</td>
        <td>'.$record['SprinklerReportGeneral']['general_b'].'</td>
    </tr>
    <tr>
        <td width="85%">c. If a fire has occurred since the last inspection, have all damaged sprinkler system components been replaced?</td>
        <td>'.$record['SprinklerReportGeneral']['general_c'].'</td>
    </tr>    
    <tr>
        <td width="85%">d. Has the piping in all dry systems been checked for proper pitch within the past five years?</td>
        <td>'.$record['SprinklerReportGeneral']['general_d'].'</td>
    </tr>
    <tr>
        <td width="85%">Date last checked:&nbsp;&nbsp;'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_d_date']).'&nbsp;&nbsp;(check recommended at least every 5 years)</td>
        <td>'.$record['SprinklerReportGeneral']['general_d_1'].'</td>
    </tr>
    <tr>
        <td width="85%">e. Has the piping in all systems been checked for obstructive materials?</td>
        <td>'.$record['SprinklerReportGeneral']['general_e'].'</td>
    </tr>
    <tr>
        <td width="85%">Date last checked:&nbsp;&nbsp;'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_e_date']).'&nbsp;&nbsp;(check recommended at least every 5 years)</td>
        <td>'.$record['SprinklerReportGeneral']['general_e_1'].'</td>
    </tr>
    </table>
    <table>
    <tr>
        <td width="85%">f. Have all fire pumps been tested to full capacity using hose streams or flow meters within the past 12 months?</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_f_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_f'].'</td>
    </tr>
    <tr>
        <td colspan="2">g. Are gravity, surface or pressure tanks protected from freezing?</td>
        <td>'.$record['SprinklerReportGeneral']['general_g'].'</td>
    </tr>
    <tr>
        <td colspan="2">h. Are sprinklers newer than 50 years old? QR (20yr) Dry (10 yr) >325F/163C (5yr) Corrosive env\'t. (5yr.)</td>
        <td>'.$record['SprinklerReportGeneral']['general_h'].'</td>
    </tr>            
    <tr>
        <td colspan="2">(Testing or replacement required for sprinklers past these age limits.)
        i. Are extra high temperature solder sprinklers free from regular exposure to temperatures near 300F/149C?</td>
        <td>'.$record['SprinklerReportGeneral']['general_i'].'</td>
    </tr>
    <tr>
        <td width="85%">j. Have gauges been tested, calibrated or replaced in the last 5 years?</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_j_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_j'].'</td>
    </tr>
    <tr>
        <td width="85%">k. Alarm valves and associated trim been internally inspected past 5 years?</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_k_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_k'].'</td>
    </tr>
    <tr>
        <td width="85%">l. Check valves internally inspected in the last 5 years?</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_l_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_l'].'</td>
    </tr>
    <tr>
        <td width="85%">m. Has the private fire main been flow tested in last 5 years? </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_m_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_m'].'</td>
    </tr>
    <tr>
        <td width="85%">n. Standpipe 5 and 3 year requirements.</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n'].'</td>
    </tr>
    <tr>
        <td width="85%">1. Dry standpipe hydrostatic test  </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_1_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n_1'].'</td>
    </tr>
    <tr>
        <td width="85%">2. Flow test </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_2_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n_2'].'</td>
    </tr>            
    <tr>
        <td width="85%">3. Hose hydrostatic test (5 years from new, every 3 years after)</td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_3_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n_3'].'</td>
    </tr>
    <tr>
        <td width="85%">4. Pressure reducing/control valve test </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_4_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_n_4'].'</td>
    </tr>
    <tr>
        <td width="85%">o. Have pressure reducing/control valves been tested at full flow within the past 5 years? </td>
        <td width="10%">'.$time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_o_date']).'</td>
        <td>'.$record['SprinklerReportGeneral']['general_o'].'</td>
    </tr>
    <tr>
        <td colspan="2">p. Have master pressure reducing/control valves been tested at full flow within the past 1 year?</td>
        <td>'.$record['SprinklerReportGeneral']['general_p'].'</td>
    </tr>
    <tr>
        <td colspan="2">q. Have the sprinkler systems been extended to all areas of the building?</td>
        <td>'.$record['SprinklerReportGeneral']['general_q'].'</td>
    </tr>
    <tr>
        <td colspan="2">r. Are the building areas protected by a wet system heated, including its blind attics and perimeter areas?</td>
        <td>'.$record['SprinklerReportGeneral']['general_r'].'</td>
    </tr>
    <tr>
        <td colspan="2">s. Are all exterior openings protected against the entrance of cold air?</td>
        <td>'.$record['SprinklerReportGeneral']['general_s'].'</td>
    </tr>            			    
</table>';
$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Sprinkler_Report_GeneralPage.pdf', 'I');die;
?>