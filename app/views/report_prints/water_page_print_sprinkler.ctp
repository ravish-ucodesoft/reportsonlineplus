<?php
	echo $this->Html->script('accordian.pack');	
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$element=$this->element('reports/report_common_view');
$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Water Page</h3></td>          
        </tr>';
        $html.=$element;		
        $html.='<tr>
               <td colspan="2">&nbsp;</td>
       </tr>      
    </table>';
$html.='<table class="tbl" width="100%">		
            <tr>
                <td colspan="3"><h3>3. WATER SUPPLIES</h3></td>
            </tr>
        </table>
        <table class="tbl" width="100%" cellpadding="1" cellspacing="1">
            <tr>
              <td width="20%"> a. Water supply sources? City:</td>
              <td width="20%">'.$record['SprinklerReportWatersupply']['wsupply_city'].'</td>
              <td width="20%">Gravity Tank:</td>
              <td width="20%">'.$record['SprinklerReportWatersupply']['wsupply_tank'].'</td>
              <td width="20%"></td>
            </tr>
            <tr>
              <td colspan="5">Water Flow Test Results Made During This Inspection</td>
            </tr>				
            <tr>
              <td>Test Pipe Located</td>
              <td>Size Test Pipe</td>
              <td>Static Pressure Before</td>
              <td>Flow Pressure</td>
              <td>Static Pressure After</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_1'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_1'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_1'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_1'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_1'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_2'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_2'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_2'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_2'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_2'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_3'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_3'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_3'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_3'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_3'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_4'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_4'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_4'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_4'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_4'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_5'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_5'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_5'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_5'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_5'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_6'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_6'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_6'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_6'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_6'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_7'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_7'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_7'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_7'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_7'].'</td>
            </tr>
            <tr>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_8'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_8'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_8'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_8'].'</td>
              <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_8'].'</td>
            </tr>
        </table>';
$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Sprinkler_Report_WaterPage.pdf', 'I');die;
?>