<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');	
?>
<?php echo $this->Html->css('reports'); ?>
<style>
body { font-family:arial}
.summary-tbl2 p {
    float: left;
    margin-bottom: 0;
    margin-left: 5px;
    width: 10%;
    font-size: 10px;
    padding-left: 7px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
    text-align:center;
}
.lb1txt{text-align:right;width:20%}
.input-chngs1 input {
    margin-right: 3px;
    width: 136px;
}
.input-chngs2 input{
	margin:0px;
	color: #666666;
	font-size: 12px;
}
.red{
color: red; 
}
td{ vertical-align:top; }
#basic-accordian{
	border:5px solid #EEE;
	padding:5px;
	width:97.5%;
	position:absolute;
	top:10%;
	margin-left:0px;
	z-index:2;
	/*margin-top:-150px;*/
}

.accordion_headings{
	padding:5px;
	background:url('../../img/newdesign_img/nav_bg.png') bottom;
	color:#FFFFFF;
	border:1px solid #FFF;
	cursor:pointer;
	font-weight:bold;
	font-size:12px;
	border-radius:6px 6px 0 0 ;
}

.accordion_headings:hover{
	background:url('../../img/newdesign_img/nav_bg.png') top;
}

.accordion_child{
	padding:15px;
	background:#EEE;
}

.header_highlight{
	background:url('../../img/newdesign_img/nav_active.png') !important;
}

.tab_container *{
	float:left;
	width:145px;
}

.tdLabel {font-size:14px;font-weight:bold;text-align:center;}
.brdr {border:2px solid #666666;}
.input-chngs1 input {
    margin-right: 3px;
    width: 296px;
}
.lbl{color:#666666;font-size:11px;text-align:left;}
.firealramreport td{ border: 1px solid #000000;}
td.alramreporttest{text-align:left; background-color:#2b92dd; color:#000; font-weight:bold}
#accordionlist { text-align:left; }
.qtytable { background:#fff}
.qtytable td{ border:1px solid black;}
.qtytable{ border-collapse: collapse;}


.blue_bg {background:url("../../img/newdesign_img/nav_active.png") !important; color:#fff !important; font-weight:bold}
</style>

<body onload="new Accordian('basic-accordian',5,'header_highlight');">
	<div id="content">     
		<div id="box">
			<h3  align="center">Fire Alarm Report</h3>	
			<br/>	
		</div>	
		<!--Tab index start here---->
		<div id="basic-accordian" ><!--Parent of the Accordion-->
			<div class="tab_container">
				<div id="test1-header" class="accordion_headings header_highlight" >Cover Page</div>
				<div id="test2-header" class="accordion_headings" >Summary Page</div>
				<div id="test3-header" class="accordion_headings" >Report</div>
				<div id="test6-header" class="accordion_headings" >Deficiency</div>
				<div id="test7-header" class="accordion_headings" >Recommendation</div>				
				<div id="test9-header" class="accordion_headings" >Missed items</div>
				<div id="test10-header" class="accordion_headings" >Additional items</div>
				<div id="test11-header" class="accordion_headings" >Signature Page</div>
				<div id="test8-header" class="accordion_headings" >Certification</div>
				<div id="test12-header" class="accordion_headings" >Quotes</div>
			</div>
			<div style="float:left;width:100%">
				<!---Content div one start--->
				<div id="test1-content">
					<div class="accordion_child">
						<table width="100%" class="tbl input-chngs1">
							<tr>
								<td colspan="5" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
							</tr>
							<tr>
								<td colspan="3" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder; color: #000;">Cover Page</span></td>
								<td colspan="2" align="right"><?php echo $html->link($html->image("pdf.png"), '/reports/coverPagePrint?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Cover Page Preview'));?></td>
							</tr>
							<tr>
								<td colspan="5">
									<table width="100%">
										<tr>
											<td align="center"><?php echo $this->Html->image('company_logo/'.$spResult['Company']['company_logo']); ?></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" style="font-size:15px;letter-spacing: 4px;"><b><?php echo $spResult['Company']['name'];  ?></b></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" style="font-size:13px">
											<?php $arr=explode(',',$spResult['Company']['interest_service']);
											foreach($arr as $key=>$val):
												foreach($reportType as $key1=>$val1):
													if($val== $key1)
													{
														if($key1 == 9){ echo $spResult['Company']['interest_service_other']; }else{ echo $val1;} ;?><span class="red"> * </span>
												<?php  }
												endforeach;
											endforeach;
											?>
											</td>
										</tr>
										<tr>
											<td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
										</tr>
										<tr>
											<td align="center" class="Cpage" style="font-size:36px">Firealarm Report</td>
										</tr>
										<tr>
											<td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
										</tr>
										<tr>
											<td align="center" class="Cpage" style="font-size:36px">Schedule Frequency: <?php echo $schFreq;?></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
										</tr>
										<tr>
											<td align="right" class="Cpage" >Date:<?php if($record['FirealarmReport']['finish_date']!= ''){
												echo $time->Format('m/d/Y',$record['FirealarmReport']['finish_date']); } ?>
											</td>
										</tr>
										<tr>
											<td align="center" class="Cpage" ><i>Prepared for:</i></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" >&nbsp;</td>
										</tr>
										<tr>
											<td align="center" class="Cpage" ><?php echo $clientResult['User']['client_company_name'] ?></td>
										</tr>	  
										<tr>
											<td align="center" class="Cpage" ><?php echo $clientResult['User']['address'] ?></td>
										</tr>
										<tr>
											<?php $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];?>
											<td align="center" class="Cpage" ><?php echo $cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']); ?></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" ><?php echo $clientResult['User']['zip']; ?></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" >&nbsp;</td>
										</tr>
										<tr>
											<td align="center" class="Cpage" ><b>Site Address Info:</b></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact']; ?></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?></td>
										</tr>
										<tr>
											<?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
											<td align="center" class="Cpage" ><?php echo $siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']); ?></td>
										</tr>
										<tr>
											<td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_zip']; ?></td>
										</tr>
										<tr>
											<td align="left" class="Cpage" >Prepared By:</td>
										</tr>
										<tr>
											<td align="left" class="Cpage">&nbsp;</td>
										</tr>  
										<tr>
											<td align="left" class="Cpage"><?php echo $spResult['Company']['name'];  ?></td>
										</tr>
										<tr>
											<td align="left" class="Cpage"><?php echo $spResult['Company']['address'];  ?></td>
										</tr>
										<tr>
											<?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
											<td align="left" class="Cpage"><?php echo $spResult['Country']['name'].','.$spResult['State']['name'].','.$spCity;  ?></td>
										</tr>		      
										<tr>
											<td align="left" class="Cpage">&nbsp;</td>
										</tr>  		      
										<tr>
											<td align="left" class="Cpage" style="font-size:11px">&nbsp;</td>
										</tr>          
									</table>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<!---Content div one end--->
				<div id="test2-content">
					<div class="accordion_child">	    
						<table width="100%" class="tbl input-chngs1">
							<tr>
								<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
							</tr>
							<tr>
								<td colspan="2" align=right width="57%" style="border:none;"><span style="font-size:15px; font-weight:bolder; color: #000;">Summary Page</span></td>
								<td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reports/summaryPagePrint?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Summary Page Preview'));?></td>
							</tr>      
						</table>
						<table width="100%">
							<tr>
								<td width="50%" align="left"><i><b>Prepared For:</b></i><br/><?php echo ucwords($clientResult['User']['client_company_name']); ?>            
									<br/>
									<?php if(!empty($clientResult['User']['site_country_id'])) { echo $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
										if(!empty($clientResult['User']['site_state_id'])) { echo $this->Common->getStateName($clientResult['User']['site_state_id']).', '; }  echo $cityName;  ?>
									<br/>
									<b>Site Information:</b><br/>
									<?php echo $siteaddrResult['SpSiteAddress']['site_name'];?><br/>
									<?php echo $siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact']; ?><br/>
									<?php echo $siteaddrResult['SpSiteAddress']['site_address'];?><br/>
									<?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
									<?php echo $siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']); ?>-<?php echo $siteaddrResult['SpSiteAddress']['site_zip']; ?><br/></td>
								
								<td width="50%" align="right" style="padding-right:30px;">
									<i><b>Prepared By:</b></i>
									<br/>
									<?php echo $spResult['Company']['name']; ?>
									<br/>
									<?php echo $spResult['User']['address']; ?>
									<br/>
									<?php 	if(!empty($spResult['Country']['name'])){
										    echo $spResult['Country']['name'].', ';
										}
										if(!empty($spResult['State']['name'])) {
										    echo $spResult['State']['name'].', ';
										}
										echo $spResult['User']['city'];
									?>
									<br/>
									Phone <?php echo ' '.$spResult['User']['phone']; ?>
								</td>
							</tr>           							
						</table>
						<table width="100%" class="qtytable">
							<tr style="background-color: #3688B7; color: #fff;">
								<td rowspan="2">Sr. No.</td>
								<td rowspan="2">Device Name</td>
								<td>Surveyed</td>
								<td>Serviced</td>
								<td>Passed</td>
								<td>Deficient</td>
								<td>Recommended</td>
							</tr>
							<tr style="background-color:#3688B7; color:#fff;">
								<td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td>
							</tr>
							<?php
							$summCounter=0;
							foreach($servicedata as $serviceData){
								$summCounter++;
							$surveydata[]= $serviceData['ScheduleService']['amount'];
							$seviceddata[]=  $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
							$passdata[]= $this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
							$deficientdata[]=$this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
							$recommenddata[]=$this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
							?>
							<tr>
								<td><?php echo $summCounter.'.'; ?></td>
								<td><?php echo $serviceData['Service']['name']; ?></td>
								<td><?php echo $serviceData['ScheduleService']['amount']; ?></td>
								<td><?php echo $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
							</tr>
							<?php } ?>
							<tr style="background-color: #3688B7;">
								<td style="color:#fff">Total</td>
								<td style="color:#fff"></td>
								<td style="color:#fff"><?php echo array_sum($surveydata); ?></td>
								<td style="color:#fff"><?php echo array_sum($seviceddata); ?></td>
								<td style="color:#fff"><?php echo array_sum($passdata); ?></td>
								<td style="color:#fff"><?php echo array_sum($deficientdata); ?></td>
								<td style="color:#fff"><?php echo array_sum($recommenddata); ?></td>
							</tr>
						</table>								
					</div>
				</div>
				<!--Second tab end here---->
				<div id="test3-content">
					<div class="accordion_child">	    
						<table width="100%" class="tbl input-chngs1">
							<tr>
								<td colspan="2" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
							</tr>
							<tr>
								<td align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bold; color: #000;">Report Page</span></td>
								<td align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reports/reportPagePrint?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Report Page Preview'));?></td>
							</tr>      
						</table>	    
						<table class="tbl input-chngs1" width="100%" style="margin-left:0px;">               								    
							<tr><th colspan="2" align="left" style="background: #C6C6C6; padding:5px; text-align: left; font-size: 16px;">Client information</th></tr>
							<tr>
								<td valign="top">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td class="table_label" width="20%"><strong>Building Information</strong></td>
											<td width="60%"><?php echo $clientResult['User']['client_company_name'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>Address</strong></td>
											<td><?php echo $clientResult['User']['address'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>City/State/Zip:</strong></td>
											<td> <?php echo $cityName.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>Country: </strong></td>
											<td><?php echo $clientResult['Country']['name'];?></td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td class="table_label" width="20%"><strong>Contact: </strong></td>
											<td width="60%"><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>Phone:</strong></td>
											<td><?php echo $clientResult['User']['phone'];?></td>
										</tr>                               
										<tr>
											<td class="table_label"><strong>Email: </strong></td>
											<td> <?php echo $clientResult['User']['email'];?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<th colspan="2">&nbsp;</th>
							</tr>
							<tr><th colspan="2" align="left" style="background: #C6C6C6; text-align: left; font-size: 16px;">Site Information</th></tr>
							<tr>
								<td valign="top">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td class="table_label" width="20%"><strong>Site Name:</strong></td>
											<td width="60%"><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>Address</strong></td>
											<td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>City/State/Zip:</strong></td>
											<td> <?php echo $siteCity.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>Country: </strong></td>
											<td><?php echo $siteaddrResult['Country']['name'];?></td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td class="table_label" width="20%"><strong>Contact: </strong></td>
											<td width="60%"> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
										 </tr>
										 <tr>
											<td class="table_label"><strong>Phone:</strong></td>
											<td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
										 </tr>                               
										 <tr>
											<td class="table_label"><strong>Email: </strong></td>
											<td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
										 </tr>
									</table>
								</td>
							</tr>
							<tr>
								<th colspan="2">&nbsp;</th>
							</tr>
							<!--<tr><th colspan="2" align="left" style="background: #C6C6C6; text-align: left; font-size: 16px;">Service Provider Info</th></tr>
							<tr>
								<td valign="top">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td class="table_label" width="20%"><strong>Name:</strong></td>
											<td width="60%"><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>Address</strong></td>
											<td><?php echo $spResult['User']['address'];?></td>
										</tr>
										<tr>
											<?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
											<td class="table_label"><strong>City/State/Zip:</strong></td>
											<td><?php echo $spCity.', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
										</tr>
										<tr>
											<td class="table_label"><strong>Country: </strong></td>
											<td><?php echo $spResult['Country']['name'];?></td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td class="table_label" width="20%"><strong>Contact: </strong></td>
											<td width="60%"> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
										 </tr>
										 <tr>
											<td class="table_label"><strong>Phone:</strong></td>
											<td><?php echo $spResult['User']['phone'];?></td>
										 </tr>                               
										 <tr>
											<td class="table_label"><strong>Email: </strong></td>
											<td><?php echo $spResult['User']['email'];?></td>
										 </tr>
									</table>
								</td>
							</tr>-->           
						</table>							
						<div  style='width:90%;margin-left:50px;'>
							<?php foreach($device_place as $key=>$deviceplace): ?>
								<script type="text/javascript">
									jQuery.noConflict();
									jQuery(function(){
										jQuery('#alarm_device_list<?php echo $key; ?>').tablesorter();
									});
								</script>
								<div>
									<h3><?php echo ucwords($deviceplace) .$form->hidden('FaLocationDescription.'.$key.'.1.location_id',array('value'=>$key)); ?></h3>		
									<table style='width:100%;margin-bottom:10px; border-collapse: collapse;' class='tbl input-chngs1 firealramreport tablesorter' id='alarm_device_list<?php echo $key; ?>' >
										<thead>
											<tr>
												<td colspan='10' class='alramreporttest'><b>Alarm Device List</b></td>
											</tr>
											<tr>
												<th style="background-image: none;">Sr. No.</th>
												<th>Comment</th>
												<th>Device Type</th>
												<th>Family</th>
												<th>Zone/ Address</th>
												<th>Location/Description</th>
												<th>Manufacturer Model</th>
												<th>Physical Condition</th>
												<th>Service</th>
												<th>Functional Test</th>		
											</tr>
										</thead>
										<tbody>
										<?php 
										if(!empty($record['FaLocationDescription'])){
											$reportUpCounter=0;
											foreach($record['FaLocationDescription'] as $data){
												$reportUpCounter++;
												if($data['fa_alarm_device_place_id'] == $key ){ ?>			
											<tr id="trloc_<?php echo $data['id']; ?>">
												<td><?php echo $reportUpCounter.'.';?></td>
												<td><?php echo $data['comment'];?></td>
												<td><?php foreach($device_type as $keydata => $valdata){
												if($keydata == $data['fa_alarm_device_type_id']){ 
													echo $valdata;
												} } ?>
												</td>
												<td><?php echo $data['family'];?></td>
												<td><?php echo $data['zone_address'];?></td>
												<td><?php echo $data['location'];?></td>
												<td><?php echo $data['model'];?></td>
												<td><?php echo $data['physical_condition'];?></td>
												<td><?php echo $data['service'];?></td>
												<td <?php if($data['functional_test']=='Deficient'){?> style="color: red"<?php }?>><?php echo $data['functional_test'];?></td>	    
											</tr>
										<?php } } } else{ ?> 
											<tr><td colspan="10">NO RECORD FOUND</td></tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							<?php endforeach; ?>
						</div>
						
						<table style='margin-left:50px; width: 90%; border-collapse:collapse;'>
							<tr>
								<td class="lb1txt" >&nbsp;</td>  
							</tr>          
							<tr> 
								<td class="lb1txt" style='text-align:left;background-color:#2b92dd; color:#fff'>System Function Summary & Test</td>
							</tr>           
						</table>    	 
						<table style='margin-left:50px;width: 90%; border-collapse: collapse; font-size: 14px;'   class='tbl input-chngs1 firealramreport' >
							<tr>
								<td><b>Functions</b></td>
								<td><b>Description</b></td>
								<td><b>Test Result</b></td>
							</tr>
							<tr>
								<td colspan='3' class='alramreporttest'>Alarm Functions</td>
							</tr>    	    
							<tr>
								<td>Alarm Bells/Horns/Strobes</td>
								<td><?php echo $record['FaAlarmFunction']['description']; //$form->input('FaAlarmFunction.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
								<td><?php
								echo $record['FaAlarmFunction']['result']; //$form->input('FaAlarmFunction.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
							</tr>						
							 <tr>
								<td colspan='3' class='alramreporttest'>Alarm Panel Supervisory Functions</td>
							</tr>
							<?php foreach($record['FaAlarmPanelSupervisoryFunction'] as $data1){ ?>
							<tr>
								<td><?php echo $data1['function_name']; ?></td>
								<td><?php echo $data1['description']; ?></td>
								<td><?php echo $data1['result']; ?></td>
							</tr>
							<?php }?>				 
							<tr>
								<td colspan='3' class='alramreporttest'>Auxiliary Functions</td>
							</tr>    	    
							<?php foreach($record['FaAuxiliaryFunction'] as $data1){ ?>
							<tr>
								<td><?php echo $data1['function_name']; ?></td>
								<td><?php echo $data1['description']; ?></td>
								<td><?php echo $data1['result']; ?></td>
							</tr>
							<?php }?>
							<tr>
								<td colspan='3' class='alramreporttest'>Fire Pump Supervisory Functions</td>
							</tr>
							<?php foreach($record['FaPumpSupervisoryFunction'] as $data1){ ?>
							<tr>
								<td><?php echo $data1['function_name']; ?></td>
								<td><?php echo $data1['description']; ?></td>
								<td><?php echo $data1['result']; ?></td>
							</tr>
							<?php }?>    	    
							<tr>
								<td colspan='3' class='alramreporttest'>Generator Supervisory Functions</td>
							</tr>
							<?php foreach($record['FaGeneratorSupervisoryFunction'] as $data1){ ?>
							<tr>
								<td><?php echo $data1['function_name']; ?></td>
								<td><?php echo $data1['description']; ?></td>
								<td><?php echo $data1['result']; ?></td>
							</tr>
							<?php } ?>
						</table>
					</div>
				</div>
				<div id="test6-content">
					<div class="accordion_child">
						
						<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
						<div style="text-align:center;width:100%;font-size:15px; color: red;"><b>Deficiency</b></div>        
						<div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/deficiencyPagePrint/Firealarm?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Deficiency Page Preview'));?></div>
						<div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Firealarm</b></div>
						<br/>
						<table width="100%">
							<tr>
								<td align="right" colspan="2"></td>
							</tr>
							<tr>
								<td width="50%" align="left"><i><b>Prepared For:</b></i>
								<br/>
								<?php echo ucwords($clientResult['User']['client_company_name']); ?>            
								<br/>
								<?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
								if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
								<br/>
								<b>Site Information:</b><br/>
								<?php echo $siteaddrResult['SpSiteAddress']['site_name'];?><br/>
								<?php echo $siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact']; ?><br/>
								<?php echo $siteaddrResult['SpSiteAddress']['site_address'];?><br/>
								<?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
								<?php echo $siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']); ?>-<?php echo $siteaddrResult['SpSiteAddress']['site_zip']; ?><br/>
								</td>								
								<td width="50%" align="right" style="padding-right:30px;">
									<i><b>Prepared By:</b></i>
									<br/>
									<?php echo $spResult['Company']['name']; ?>
									<br/>
									<?php echo $spResult['User']['address']; ?>
									<br/>
									<?php if(!empty($spResult['Country']['name'])){
										    echo $spResult['Country']['name'].', ';
										}
										if(!empty($spResult['State']['name'])) {
										    echo $spResult['State']['name'].', ';
										}
										echo $spResult['User']['city'];
									?>
									<br/>
									Phone <?php echo ' '.$spResult['User']['phone']; ?>
								</td>            
							</tr>
						</table>        
						<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
							<tr>
								<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
								<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
								<th width="60%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
								<th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Attachment</b></th>
							</tr>
							<?php
							if(!empty($Def_record))
							{
						       // pr($codeData);
						       $defCounter=0;
							foreach($Def_record as $Def_record) {
								$defCounter++;
							?>
							<tr>            
								<td valign="top"><?php echo $defCounter.'.'; ?></td>
								<td valign="top"><?php echo $Def_record['Code']['code']; ?></td>
								<td valign="top"><?php echo $Def_record['Code']['description']; ?></td>
								<td valign="top">
									<?php if(!empty($Def_record['Deficiency']['attachment']) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$Def_record['Deficiency']['attachment'])){?>
									    <?php echo $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$Def_record['Deficiency']['attachment'],'sp'=>false)); ?>
									<?php }else{?>
									    <?php echo 'No Attachment';?>
									<?php }?>
								</td>
							</tr>
							<?php }
							} else
							{
								echo '<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';   
							}
							?>
						</table>
					</div>
				</div>
				<div id="test7-content">
					<div class="accordion_child">
						<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
						<div style="text-align:center;width:100%;font-size:15px; color: orange"><b>Recommendation</b></div>
						<div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/recommendationPagePrint/Firealarm?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Recommendation Page Preview'));?></div>        
						<div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Firealarm</b></div>
						<br/>
						<table width="100%">
							<tr>
								<td align="right" colspan="2"></td>
							</tr>
							<tr>
								<td width="50%" align="left"><i><b>Prepared For:</b></i>
								<br/>
								<?php echo ucwords($clientResult['User']['client_company_name']); ?>								
								<br/>
							       <?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
								if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
								<br/>
								</td>
								<td width="50%" align="right" style="padding-right:30px;">
									<i><b>Prepared By:</b></i>
									<br/>
									<?php echo $spResult['Company']['name']; ?>
									<br/>
									<?php echo $spResult['User']['address']; ?>
									<br/>
									<?php 	if(!empty($spResult['Country']['name'])){
										    echo $spResult['Country']['name'].', ';
										}
										if(!empty($spResult['State']['name'])) {
										    echo $spResult['State']['name'].', ';
										}
										echo $spResult['User']['city'];
									?>
									<br/>
									Phone <?php echo ' '.$spResult['User']['phone']; ?>
								</td>
							</tr>
						</table>	        
						<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
							<tr>            
								<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
								<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
								<th width="30%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
								<th width="30%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>	            
							</tr>
							<?php
							if(!empty($Def_record_recomm))
							{
							$recommCounter=0;
							foreach($Def_record_recomm as $Def_record_recomm) {
								$recommCounter++;
							?>
							<tr>            
								<td valign="top"><?php echo $recommCounter.'.'; ?></td>
								<td valign="top"><?php echo $Def_record_recomm['Code']['code']; ?></td>
								<td valign="top"><?php echo $Def_record_recomm['Code']['description']; ?></td>
							</tr>
							<?php }
							} else
							{
								echo '<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
							}
							?>
						</table>       
					</div>
				</div>										

				<div id="test9-content">
					<div class="accordion_child">
						<table width="100%" class="tbl">
							<tr>
								<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder;"><?php echo ucwords($spResult['Company']['name']);?></span></td>
							</tr>
							<tr>
								<td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder; color: green">Missed Items</span></td>
								<td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reports/missingItemsPagePrint?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Missed Items Page Preview'));?></td>
							</tr>      
						</table>
						<table width="100%">
							<tr>
								<td style="text-align:center;">Missed Device Page Prepared For: <?php echo $this->Common->get_comission_type($clientResult['User']['code_type_id']); ?></td>        
							</tr>           
							<tr>
								<td align="center" class="Cpage" ><?php echo ucwords($clientResult['User']['client_company_name']); ?></td>
							</tr>
							<tr>
								<td align="center" class="Cpage" ><?php echo $clientResult['User']['address'] ?></td>
							</tr>
							<tr>			
								<td align="center" class="Cpage" ><?php echo $cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']); ?></td>
							</tr>
							<tr>
								<td align="center" class="Cpage" ><?php echo $clientResult['User']['zip']; ?></td>
							</tr>      
						</table>
						<table width="100%" class="qtytable">
							<tr>
								<td rowspan="2" style="background:#2b92dd; color:#fff; font-weight:bold">Sr. No.</td>
								<td rowspan="2" style="background:#2b92dd; color:#fff; font-weight:bold">Device Name</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Surveyed</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Serviced</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Passed</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Deficient</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Recommended</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Missed</td>
							</tr>
							<tr style="background:#666; color:#fff; font-weight:bold">
								<td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td>
							</tr>
							<?php
							$missCounter=0;
							foreach($servicedata as $serviceData){
								$missCounter++;
							?>
							
							<?php //if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id'])< $serviceData['ScheduleService']['amount']){ ?>
							<tr>
								<td><?php echo $missCounter.'.'; ?></td>
								<td><?php echo $serviceData['Service']['name']; ?></td>
								<td><?php echo $serviceData['ScheduleService']['amount']; ?></td>
								<td><?php echo $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<?php
								if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id'])<$serviceData['ScheduleService']['amount']){
								     $missing = $serviceData['ScheduleService']['amount'] - $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
								}else{
								     $missing = "0";
								}
							       ?>	  
								<td><?php echo $missing; ?></td>
							</tr>
							<?php //} 
							}  ?>
						</table>
						<table style="color: green">
							<?php 
								$missingData = $this->Common->missingTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
								$missingText=$missingData['ExplanationText']['missing_reason'];
								$missingfile=$missingData['ExplanationText']['missing_file'];
								$missingText = (($missingText!="")?$missingText:'No explanation added');
								if(!empty($missingfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$missingfile)){
									$showMissfile = $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$missingfile,'sp'=>false));
								}else{
									$showMissfile='No Attachment';
								}
							?>
							<tr>								
								<td colspan="6">Missed Items Explanation: <?php echo $missingText;?></td>
							</tr>
							<tr>								
								<td colspan="6">Missed Items Attachment: <?php echo $showMissfile;?></td>
							</tr>
						</table>
						<!--<table>
							<tr><td colspan="6">&nbsp;</td></tr>
							<tr>
								<td colspan="6"><?php echo $spResult['Company']['name'];  ?></td>
							</tr>
							<tr>
							       <td colspan="6"><?php echo $spResult['Company']['address'];  ?></td>
							</tr>
							<tr>
							       <td colspan="6"><?php echo $spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'];  ?></td>
							</tr>		 
						</table>-->
					</div>
				</div>
				<div id="test10-content">
					<div class="accordion_child">						
						<table width="100%" class="tbl">
							<tr>
								<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder;"><?php echo ucwords($spResult['Company']['name']);?></span></td>
							</tr>
							<tr>
								<td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder; color: purple">Additional Items</span></td>
								<td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reports/additionalItemsPagePrint?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Additional Items Page Preview'));?></td>
							</tr>      
						</table>
						<table width="100%">
							<tr>
								<td style="text-align:center;">Additional Device Page Prepared For: <?php echo $this->Common->get_comission_type($clientResult['User']['code_type_id']); ?></td>        
							</tr>           
							<tr>
								<td align="center" class="Cpage" ><?php echo ucwords($clientResult['User']['client_company_name']); ?></td>
							</tr>
							<tr>
								<td align="center" class="Cpage" ><?php echo $clientResult['User']['address'] ?></td>
							</tr>
							<tr>			
								<td align="center" class="Cpage" ><?php echo $cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']); ?></td>
							</tr>
							<tr>
								<td align="center" class="Cpage" ><?php echo $clientResult['User']['zip']; ?></td>
							</tr>      
						</table>
						<table width="100%" class="qtytable">
							<tr>
								<td rowspan="2" style="background:#2b92dd; color:#fff; font-weight:bold">Sr. No.</td>
								<td rowspan="2" style="background:#2b92dd; color:#fff; font-weight:bold">Device Name</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Surveyed</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Serviced</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Passed</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Deficient</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Recommended</td>
								<td style="background:#2b92dd; color:#fff; font-weight:bold">Additional</td>
							</tr>
							<tr style="background:#666; color:#fff; font-weight:bold">
								<td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td>
							</tr>
							<?php
							$addCounter=0;
							foreach($servicedata as $serviceData){
								$addCounter++;
							?>
							<?php //if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']) > $serviceData['ScheduleService']['amount']){ ?>
							<tr>
								<td><?php echo $addCounter.'.'; ?></td>
								<td><?php echo $serviceData['Service']['name']; ?></td>
								<td><?php echo $serviceData['ScheduleService']['amount']; ?></td>
								<td><?php echo $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<td><?php echo $this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']); ?></td>
								<?php if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']) > 0) {
								      $additional = $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']) -  $serviceData['ScheduleService']['amount'] ;
								      }else{
									  $additional = "0";
								      }
								 ?>
								<td><?php echo $additional; ?></td>
							</tr>
							<?php //}
							} ?>
						</table>
						<table style="color: purple">
							<?php 
								$additionalData = $this->Common->additionalTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
								$additionalText=$additionalData['ExplanationText']['additional_reason'];
								$additionalfile=$additionalData['ExplanationText']['additional_file'];
								$additionalText = (($additionalText!="")?$additionalText:'No explanation added');
								if(!empty($additionalfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$additionalfile)){
									$showAdditionalfile = $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$additionalfile,'sp'=>false));
								}else{
									$showAdditionalfile='No Attachment';
								}
							?>
							<tr>								
								<td colspan="6">Additional Items Explanation: <?php echo $additionalText;?></td>
							</tr>
							<tr>								
								<td colspan="6">Additional Items Attachment: <?php echo $showAdditionalfile;?></td>
							</tr>
						</table>
						<!--<table>
							<tr><td colspan="6">&nbsp;</td></tr>
							<tr>
								<td colspan="6"><?php echo $spResult['Company']['name'];  ?></td>
							</tr>
							<tr>
							       <td colspan="6"><?php echo $spResult['Company']['address'];  ?></td>
							</tr>
							<tr>
							       <td colspan="6"><?php echo $spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'];  ?></td>
							</tr>		 
						</table>-->
					</div>
				</div>
				<div id="test11-content">
					<div class="accordion_child">
						<table width="100%" class="tbl">
							<tr>
								<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>
							</tr>
							<tr>
								<td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Signature Page</span></td>
								<td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reports/signaturePagePrint/FirealarmReport?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Signature Page Preview'));?></td>
							</tr>	   
						</table>
						<table width="100%">
							<tr>
								<td style="border:none;"><b>Inspector Signature</b></br></br>
								<?php  $name=$this->Common->getInspectorSignature($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id'])?><?php echo $this->Html->image('signature_images/'.$name);  ?>
								
								</td>
								<td style="border:none; vertical-align:top;" align=right><b>Client Signature</b></td>	  
							</tr>
							<tr>
								<td style="border:none;">		
								<?php $dateCreated=$this->Common->getSignatureCreatedDate($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id']);		  
								if($dateCreated!='empty'){
									echo $this->Common->getClientName($record['FirealarmReport']['inspector_id'])."<br/><br/>";
									echo $time->Format('m-d-Y',$dateCreated);
								}
								?>
								</td>
								<td align="right" style="border:none;">
								<?php //$dateCreated=$this->Common->getSignatureCreatedDate($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id']);		  
								//if($dateCreated!='empty'){
									echo $this->Common->getClientName($record['FirealarmReport']['client_id'])."<br/><br/>";
									//echo $time->Format('m-d-Y',$dateCreated);
								//}
								?>	
								</td>
							</tr>
						</table>
						<!--<table>
							<tr><td colspan="6">&nbsp;</td></tr>
							<tr>
								<td colspan="6"><?php echo $spResult['Company']['name'];?></td>
							</tr>
							<tr>
							       <td colspan="6"><?php echo $spResult['Company']['address']?></td>
							</tr>
							<tr>
							       <td colspan="6"><?php echo $spResult['Country']['name'].','.$spResult['State']['name'].','.$spCity;?></td>
							</tr>		 
						</table>-->
					</div>
				</div>
				<div id="test8-content">
					<div class="accordion_child">
						<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
						<div style="text-align:center;width:100%;font-size:15px;"><b>Certification</b></div>
						<div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/certAttachedPagePrint/Firealarm?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'CERT Attached Page Preview'));?></div>        
						<div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Firealarm</b></div>
						<br/>
						<table width="100%">
							<tr>
								<td align="right" colspan="2"></td>
							</tr>
							<tr>
								<td width="50%" align="left"><i><b>Prepared For:</b></i>
								<br/>
								<?php echo ucwords($clientResult['User']['client_company_name']); ?>		    
								<br/>
								<?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
								if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $clientResult['User']['city'];  ?>
								<br/>
								</td>
								<td width="50%" align="right" style="padding-right:30px;">
									<i><b>Prepared By:</b></i>
									<br/>
									<?php echo $spResult['Company']['name']; ?>
									<br/>
									<?php echo $spResult['User']['address']; ?>
									<br/>
									<?php 	if(!empty($spResult['Country']['name'])){
											echo $spResult['Country']['name'].', ';
										    }
										    if(!empty($spResult['State']['name'])) {
											echo $spResult['State']['name'].', ';
										    }
										    echo $spResult['User']['city'];
									?>
									<br/>
									Phone <?php echo ' '.$spResult['User']['phone']; ?>
								</td>		    
							</tr>
						</table>
						<table class="tbl input-chngs1" width="100%" >                
							<tr>
								<td align='left' style="text-align:left">
								<?php
									$certs = $this->Common->getAllSchCerts($scheduledata['Schedule']['id']);						
									if(sizeof($certs)>0){
								?>					
								<?php $cr=1;
								foreach($certs as $cert)
								{
								    echo $html->link($cr.'.'.$cert['ReportCert']['ins_cert_title'],array("controller"=>"messages","action"=>"download_self_form",$cert['ReportCert']['ins_cert_form'],'inspector'=>true),array('title'=>'DownLoad/View')); 
								    
									    echo '<br/>';
								    
								    $cr++;
								}
								?>
								<?php
								}
								?>
								</td>
							</tr>	
						</table>
					</div>
				</div>
				<div id="test12-content">
					<div class="accordion_child">
						<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
						<div style="text-align:center;width:100%;font-size:15px;"><b>Quotes</b></div>
						<div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/quotesPagePrint/Firealarm?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Quotes Page Preview'));?></div>        
						<div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Firealarm</b></div>
						<br/>
						<table width="100%">
							<tr>
								<td align="right" colspan="2"></td>
							</tr>
							<tr>
								<td width="50%" align="left"><i><b>Prepared For:</b></i>
								<br/>
								<?php echo ucwords($clientResult['User']['client_company_name']); ?>		    
								<br/>
								<?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
								if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
								<br/>
								</td>
								<td width="50%" align="right" style="padding-right:30px;">
									<i><b>Prepared By:</b></i>
									<br/>
									<?php echo $spResult['Company']['name']; ?>
									<br/>
									<?php echo $spResult['User']['address']; ?>
									<br/>
									<?php 	if(!empty($spResult['Country']['name'])){
										    echo $spResult['Country']['name'].', ';
										}
										if(!empty($spResult['State']['name'])) {
										    echo $spResult['State']['name'].', ';
										}
										echo $spResult['User']['city'];
									?>
									<br/>
									Phone <?php echo ' '.$spResult['User']['phone']; ?>
								</td>			    
							</tr>
						</table>
						<table class="tbl" width="100%" >                
							<tr>
								<td align='left' style="text-align:left">
									<div><b>Quote Form from SP</b></div>
									<?php										
									if(isset($chkRecord) && !empty($chkRecord)){
										echo $html->link($chkRecord['Quote']['title'],array('controller'=>'sps','action'=>'download_file',$chkRecord['Quote']['attachment'],'quotefile'));
									}else{
										echo 'No Quote have been submitted by Service Provider.';
									}
									?>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align='left' style="text-align:left">
								<div><b>Client's Response to Quote Form</b></div>
								<?php										
								$quotedocs = $this->Common->getQuoteDoc($_REQUEST['reportID'],$_REQUEST['clientID'],$_REQUEST['spID'],$_REQUEST['serviceCallID']);
								
								if(isset($quotedocs) && !empty($quotedocs)){
									if($quotedocs['Quote']['client_response']=='a'){
										$status = 'Accepted';
									}else if($quotedocs['Quote']['client_response']=='d'){
										$status = 'Denied';
									}else if($quotedocs['Quote']['client_response']=='n'){
										$status = 'No wish to fix the deficiencies';
									}else{
										$status = 'Pending';
									}
									?>
									<div>Status: <?php echo $status;?></div>
									<?php if($quotedocs['Quote']['client_response']=='a'){?>
									<div class="certificates-hdline" style="text-align: left;">
									    <?php echo $html->link('Signed Quote Form',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['signed_form'],'quotefile','sp'=>true),array('style'=>'color:red'))?>
									</div>							    							
									<?php }else if($quotedocs['Quote']['client_response']=='d'){?>
									<div class="certificates-hdline" style="text-align: left;">
									    <?php echo $html->link('Work Orders',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['work_order_form'],'quotefile','sp'=>true),array('style'=>'color:red'))?>
									</div>
									<?php }?>
									<?php					
								}
								?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div><!--End of accordion parent-->
	</div>
</body>