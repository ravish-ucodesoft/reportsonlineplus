<?php
	echo $this->Html->script('jquery.1.6.1.min');   
	echo $this->Html->css('validationEngine.jquery');
	echo $this->Html->css(array('ajaxuploader/fileuploader'));
	echo $this->Html->script('languages/jquery.validationEngine-en.js');
	echo $this->Html->script('jquery.validationEngine.js');
	echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
	echo $this->Html->script(array('ajaxuploader/fileuploader'));
	echo $this->Html->script(array('ui/ui.core.js','ui/ui.datepicker.js'));
	echo $this->Html->css('reports');
	echo $this->Html->css(array('gallery/colorbox'));
	echo $this->Html->script(array('colorbox/jquery.colorbox'));	
?>
<script type="text/javascript">
	$(document).ready(function(){
		jQuery(".ajax").colorbox();
		jQuery(".inline").colorbox({inline:true, width:"50%"});
		loadDatePicker();
		jQuery("#kitchenreport").validationEngine();
		<?php
		if(isset($this->data['KitchenhoodReport']['finish_date']) && ($this->data['KitchenhoodReport']['finish_date'] != "")){ ?>	   
			for(i = 0; i < document.getElementById('kitchenreport').length; i++){
				var formElement = document.getElementById('kitchenreport').elements[i];	
				formElement.disabled = true;
			}
		<?php } ?>
	});
	function limit(field, chars) {	
		if (document.getElementById(field).value.length > chars) {
		    document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
		    var errMsg="You are only allowed to enter "+chars+" characters.";
		    document.getElementById('essay_Error').innerHTML= errMsg;
		}
		var len=document.getElementById(field).value.length;
		if(len < chars){document.getElementById('essay_Error').innerHTML='';}
		document.getElementById('limitCounter').innerHTML=len;
		document.getElementById('limitCounterLeft').innerHTML=chars-len;
	}
</script>
<script type="text/javascript">

function AddRow(divid){    
    var counter = jQuery('#optioncount').val();
    // var type= new array('2.5 lb Dry Chemical'=>'2.5 lb Dry Chemical','5 lb Dry Chemical'=>'5 lb Dry Chemical','10 lb Dry Chemical'=>'10 lb Dry Chemical','20 lb Dry Chemical'=>'20 lb Dry Chemical','5 lb CO2'=>'5 lb CO2','10 lb CO2'=>'10 lb CO2','15 lb CO2'=>'15 lb CO2','20 lb CO2'=>'20 lb CO2','5 lb FE36 Clean Agent'=>'5 lb FE36 Clean Agent','10 lb FE36 Clean Agent'=>'10 lb FE36 Clean Agent','13 lb FE36 Clean Agent'=>'13 lb FE36 Clean Agent','2.5 Gal Pressurized Water'=>'2.5 Gal Pressurized Water','K Class'=>'K Class');
    //alert(type);
    counter = parseInt(counter) + 1;
    jQuery('#optioncount').val(counter);
    var OptionHtml = '<tr id="option_'+counter+'">';
    OptionHtml = OptionHtml+'<td width="2%" style="padding: 4px 2px; text-align: center">'+counter+'</td>';
    OptionHtml = OptionHtml+'<td width="25%" style="padding: 4px 2px; text-align: center"><input type="text" name="data[KitchenhoodReportRecord]['+counter+'][location]" class="validate[required]" id="" ></td>';
    OptionHtml = OptionHtml+'<td width="25%" style="padding: 4px 2px; text-align: center"><input type="text" name="data[KitchenhoodReportRecord]['+counter+'][born_date]"  id="" class="calender validate[required]" style="margin:0;" readonly = "readonly"></td>';
    OptionHtml = OptionHtml+'<td width="25%" style="padding: 4px 2px; text-align: center"><select id="status_'+counter+'" name="data[KitchenhoodReportRecord]['+counter+'][type]" class="validate[required]"><option value="" selected="selected">--Select-</option>';
    <?php foreach($options as $key=>$val){ ?>
    OptionHtml = OptionHtml+'<option value="<?php echo $key; ?>"><?php echo $val; ?></option>';    
    <?php } ?>
    OptionHtml = OptionHtml+'</select></td>';   
    OptionHtml = OptionHtml+'<td width="20%" style="padding: 4px 2px; text-align: center"><select onchange="chkStatus(this);" id="status_'+counter+'" name="data[KitchenhoodReportRecord]['+counter+'][deficiency]"><option value="No" selected="selected">No</option><option value="Yes">Yes</option></select></td>';        
    OptionHtml = OptionHtml+'<td width="3%" style="padding: 4px 2px; text-align: center"><a href="javascript:void(0)" onclick="RemoveRow('+counter+',this)"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></td>';    
    OptionHtml = OptionHtml+'</tr>';
    jQuery('#DivOption').append(OptionHtml);
     loadDatePicker();
}

function RemoveRow(divid,obj){    
    selectBox  = jQuery(obj).parent('td').parent('tr').find('select:first');
    deficienVal = selectBox.parent('td').next('td').children('select').val();
    rowId = jQuery(obj).parent('td').parent('tr').children('td:first').html();    
    locationData = jQuery(obj).parent('td').parent('tr').find('input:first').val();
    modelData = '';
    serviceId = selectBox.val();
    if(deficienVal=='No'){
	jQuery('#option_'+divid).remove();
	return false;
    }
    if(deficienVal=='Yes'){
	if(!confirm("It will also delete the added deficiency. Do you want to continue?")){	
		return false;
	}	
    }
    reportId = '<?php echo $_REQUEST['reportID'];?>';
    clientId = '<?php echo $_REQUEST['clientID'];?>';
    spId = '<?php echo $_REQUEST['spID'];?>';
    servicecallID = '<?php echo $_REQUEST['serviceCallID'];?>';
    
    data = 'reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId;
    jQuery.ajax({
	'url':'/inspector/reports/deleteDeficiency/',
	'data':data,
	'type':'post',
	'success':function(msg){
		if(msg == 'Success'){
			jQuery('#option_'+divid).remove();
		}else{
			alert('Problem in deleting the record. Please try again');
		}
	}
    });    
}
function RemoveRecord(rId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delrecord/" + rId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#tr_'+rId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmbox(rId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveRecord(rId);    
  }
  else{
    return false;
  }
 }
function createUploader(){	    
	var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
		}
        });           
}        
window.onload = createUploader;
/*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
function loadDatePicker()
{
   jQuery(".calender").datepicker({ dateFormat: 'mm/dd/yy',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });
}

function RemoveAttachment(attachId){	
	jQuery.ajax({
                type : "GET",
                url  : "/reports/delattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
} 

function checkUploadedAttachment(){
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
function checkNotification(){
	var notifyVal = jQuery("#ExtinguisherReportNotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
}

function confirmattach(attachId){
	var r = confirm("Are you sure you want to delete this record");
	if (r==true){
		RemoveAttachment(attachId);    
	}else{
		return false;
	}
}
function RemoveNotifier(notifierId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delnotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#pnotify_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
	var r = confirm("Are you sure you want to delete this record");
	if (r==true){
		RemoveNotifier(notifierId);    
	}else{
		return false;
	}
}

function chkStatus(obj){
	response = jQuery(obj).val();
	currentObj = jQuery(obj);
	selectBox  = jQuery(obj).parent('td').parent('tr').find('select:first');	
	rowId = jQuery(obj).parent('td').parent('tr').children('td:first').html();
	locationData = jQuery(obj).parent('td').parent('tr').find('input:first').val();
	modelData = '';
	mfdDate = selectBox.parent('td').prev('td').children('input').val();
	serviceId = selectBox.val();
	reportId = '<?php echo $_REQUEST['reportID'];?>';
	clientId = '<?php echo $_REQUEST['clientID'];?>';
	spId = '<?php echo $_REQUEST['spID'];?>';
	servicecallID = '<?php echo $_REQUEST['serviceCallID'];?>';
	if(response=='No'){		
		if(!confirm("It will delete the added deficiency. Do you want to continue?")){
			jQuery(obj).val("Yes");
			return false;
		}else{
			data = 'reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId;
			jQuery.ajax({
				'url':'/inspector/reports/deleteDeficiency/',
				'data':data,
				'type':'post',
				'success':function(msg){
					currentObj.css('color','#666666');
					return false;
				}
			});
		}
		return false;
	}
	if(serviceId=="" || locationData=="" || mfdDate==""){
		alert('Please fill location, type and Born date first');
		jQuery(obj).val("No");
		return false;
	}
	currentObj.css('color','red');
	PopupCenter('/inspector/reports/fillDeficiency?reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId+'&locationData='+locationData+'&modelData='+modelData+'&mfdDate='+mfdDate,'','600','400');
}
</script>

<style>
.message{text-align:center;color:#54A41A;font-weight:bold;}
.headingtxt {
    background-color: #2B92DD;
    color: #FFFFFF;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    margin-bottom: 10px;
    padding: 5px;
    width: 100%;
}
/*----------- 30052012---------------------*/
.input-chngs input{ background: #ebebeb; /* Old browsers */
background: -moz-linear-gradient(top, #ebebeb 0%, #ffffff 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ebebeb), color-stop(100%,#ffffff)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* IE10+ */
background: linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebebeb', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
border:1px solid #bbb; width:66%; margin:0 30% 0 0;
}
.input-chngs td { color:#666;}

table td { border:none;}
#box .blue-hdng { background: none repeat scroll 0 0 #F4FAFF; border:none;
    border-bottom: 1px solid #AECEEF;
    color: #2281D4;
    font-size: 16px; font-weight:normal;
}
</style>


<div id="content" >
	<div class='message'><?php echo $this->Session->flash();?></div>
	<div id="box">
		<div style="width: 100%">				
			<?php echo $html->link($html->image("fineimages/pdf_preview.png",array('title'=>'Print Preview')), 'reportPreview/Kitchenhood?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
		</div>
		<h3 align="center" >Kitchenhood Report</h3>
		<h4 class="table_title">Agreement and Report of Inspection</h4>
		<?php
			$actionUrl = 'kitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'];
		?>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>$actionUrl,'name'=>'kitchenreport','id'=>'kitchenreport')); ?>
			<table cellpadding="0" cellspacing="0" border="0" class="table_new">
				<tr>
					<td>
						<b>Days Left for report Submission: <?php echo $daysRemaining;?></b>
					</td>					
				</tr>
				<tr>
					<th colspan="2" align="left">Client information</th>
				</tr>
				<tr>
					<td valign="top">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="table_label"><strong>Building Information</strong></td>
								<td><?php echo $clientRes['User']['client_company_name'];?></td>
							</tr>
							<tr>
							    <td class="table_label"><strong>Address</strong></td>
							    <td><?php echo $clientRes['User']['address'];?></td>
							</tr>
							<tr>
							    <td class="table_label"><strong>City/State/Zip:</strong></td>
							    <td> <?php echo $clientRes['User']['city'].', '.$clientRes['State']['name'].' '.$clientRes['User']['zip'];?></td>
							</tr>
							<tr>
							    <td class="table_label"><strong>Country: </strong></td>
							    <td><?php echo $clientRes['Country']['name'];?></td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="table_label"><strong>Contact: </strong></td>
								<td><?php echo $clientRes['User']['fname'].' '.$clientResult['User']['lname'];?></td>
							</tr>
							<tr>
								<td class="table_label"><strong>Phone:</strong></td>
								<td><?php echo $clientRes['User']['phone'];?></td>
							</tr>                               
							<tr>
								<td class="table_label"><strong>Email: </strong></td>
								<td> <?php echo $clientRes['User']['email'];?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th colspan="2" align="left">Site Information</th>
				</tr>
				<tr>
					<td valign="top">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="table_label"><strong>Site Name:</strong></td>
								<td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
							</tr>
							<tr>
							    <td class="table_label"><strong>Address</strong></td>
							    <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
							</tr>
							<tr>
							    <td class="table_label"><strong>City/State/Zip:</strong></td>
							    <td> <?php echo $siteaddrResult['SpSiteAddress']['site_city'].', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
							</tr>
							<tr>
							    <td class="table_label"><strong>Country: </strong></td>
							    <td><?php echo $siteaddrResult['Country']['name'];?></td>
							</tr>
						</table>
					</td>
					<td valign="top">
						<table width="100%" cellpadding="0" cellspacing="0">
						    <tr>
							<td class="table_label"><strong>Contact: </strong></td>
							<td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
						    </tr>
						    <tr>
							<td class="table_label"><strong>Phone:</strong></td>
							<td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
						    </tr>                               
						    <tr>
							<td class="table_label"><strong>Email: </strong></td>
							<td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
						    </tr>
						</table>
					</td>
				</tr>
				<tr><th colspan="2" align="left">Service Provider Info</th></tr>
				<tr>
					<td valign="top">
					    <table width="100%" cellpadding="0" cellspacing="0">
						<tr>
						    <td class="table_label"><strong>Name:</strong></td>
						    <td><?php echo $spRes['User']['fname'].' '.$spRes['User']['lname'];?></td>
						</tr>
						<tr>
						    <td class="table_label"><strong>Address</strong></td>
						    <td><?php echo $spRes['User']['address'];?></td>
						</tr>
						<tr>
						    <td class="table_label"><strong>City/State/Zip:</strong></td>
						    <td><?php echo $spRes['User']['city'].', '.$spRes['State']['name'].' '.$spRes['User']['zip'];?></td>
						</tr>
						<tr>
						    <td class="table_label"><strong>Country: </strong></td>
						    <td><?php echo $spRes['Country']['name'];?></td>
						</tr>
					    </table>
					</td>
					<td valign="top">
						<table width="100%" cellpadding="0" cellspacing="0">
						    <tr>
							<td class="table_label"><strong>Contact: </strong></td>
							<td> <?php echo $spRes['User']['fname'].' '.$spResult['User']['lname'];?></td>
						    </tr>
						    <tr>
							<td class="table_label"><strong>Phone:</strong></td>
							<td><?php echo $spRes['User']['phone'];?></td>
						    </tr>                               
						    <tr>
							<td class="table_label"><strong>Email: </strong></td>
							<td><?php echo $spRes['User']['email'];?></td>
						    </tr>
						</table>
					</td>
				</tr>
			</table>
			<div style="border:4px solid #000; padding:10px; margin:10px">
				<?php echo $texts;?>
			</div>			
			<?php  if(!(isset($this->data['KitchenhoodReport']['finish_date']) && ($this->data['KitchenhoodReport']['finish_date'] != ""))){ ?>
			<div style="float:right;padding-right:10px;">
				<?php echo $html->link('Add Explanation',array('controller'=>'inspectors','action'=>'addExplanation?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID']),array('class'=>'ajax'));?>
			</div>
			<?php }?>
			<!--<div style="float:right;padding-right:10px;">-->  
				  <?php
                                //$permissionfill=$this->Common->checkPermission(1,$session_inspector_id);    
                                //if($session_inspector_id!=$schedule['lead_inspector_id'])
                                //if($permissionfill!=1)
                                //{
                                ?>
                                    <!--<a href="javascript:void(0)" title="Add Attachment"  class="linktoreport" onclick="alert('<?php //echo $permissionfill;?>')">Add Attachment</a><span style='vertical-align:bottom'><?php //echo $html->image('icon_attachment.gif');?></span>-->
                                        
                                <?php //} else {								
                                ?>
                                <?php //echo $html->link('Add Attachment','#inline_content_attachment',array('class' => 'inline'));?><?php //echo $html->image('icon_attachment.gif');?>&nbsp;&nbsp;&nbsp;
                            <?php
                            //} 
                            ?>
                           <?php
                                //$permissionfillnotify=$this->Common->checkPermission(2,$session_inspector_id);    
                                //if($session_inspector_id!=$schedule['lead_inspector_id'])
                                //if($permissionfillnotify!=1)
                                //{
                                ?>
                                    <!--<a href="javascript:void(0)" title="Add Notification"  class="linktoreport" onclick="alert('<?php //echo $permissionfillnotify;?>')">Add Notification</a><span style='vertical-align:bottom'><?php //echo $html->image('icon_notify.gif');?></span>-->
                                        
                                <?php //} else {								
                                ?>
                                <?php //echo $html->link('Add Notifier',"#inline_content_notification",array('class' => 'inline'));?><?php //echo $html->image('icon_notify.gif');?>
                            <?php
                            //} 
                         ?>
			<!--</div>-->
			
			<?php		    
				echo $form->input('KitchenhoodReport.servicecall_id',array('type'=>'hidden','value'=>@$servicecallId));
				echo $form->input('KitchenhoodReport.sp_id',array('type'=>'hidden','value'=>@$spId));
				echo $form->input('KitchenhoodReport.inspector_id',array('type'=>'hidden','value'=>@$inspector_id));
				echo $form->input('KitchenhoodReport.client_id',array('type'=>'hidden','value'=>@$clientId));
				echo $form->input('KitchenhoodReport.report_id',array('type'=>'hidden','value'=>@$reportId));
				echo $form->input('KitchenhoodReport.id',array('type'=>'hidden','value'=>@$result['id'])); ?>
				<?php
				if(sizeof($data)>0){
					$countVal = count($data);
				}else{
					$countVal = count($options);
				}
				echo $this->Form->hidden('KitchenhoodReport.optioncount',array('value'=>$countVal,'id'=>'optioncount'));?>
				<?php echo $this->element('reports/kitchenhood_form'); ?>				
				<!--<tr>
					<td align="center">-->
					<?php
						//$permissionfilldef=$this->Common->checkPermission(6,$session_inspector_id);    
						//if($session_inspector_id!=$schedule['lead_inspector_id'])
						//if($permissionfilldef!=1)
						//{
						?>
						    <!--<a href="javascript:void(0)" title="Add Deficiency"  class="linktoreport" onclick="alert('<?php //echo $permissionfilldef;?>')"><span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span></a>-->
							
						<?php //} else {								
						?>
						<?php //echo $html->link('<span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span>',array('controller'=>'reports','action'=>'defAndRec?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),array('class' => 'ajax','escape'=>false),false); ?>
						<?php
						//} 
					?>				
					<!--</td>
				</tr>-->
				<?php if(!(isset($result['finish_date']) && ($result['finish_date'] != ""))){ ?>
				<tr>      
					<td>
					<?php if(isset($this->data['KitchenhoodReport']['finish_date']) && ($this->data['KitchenhoodReport']['finish_date'] != "")){
						echo $html->link('<span>FINISH</span>','javascript:void(0);',array('escape'=>false,'class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'kitchenhoodView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');"));
						} else { ?>
						<span class="blue-btn" style="float:right;">
							<input type="submit" name="finish" value="FINISH" id="linkFinish" onclick="return confirm('Are you sure that you want to finish this report? after clicking ok you will not be able to further edit this report.')" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/>
						</span>		
					<?php
						}
						echo $html->link('<span>REVIEW</span>','javascript:void(0);',array('escape'=>false,'id'=>'reviewanswer','class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'kitchenhoodView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');")); ?>
						<span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" name="save" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span>
					</td>
				</tr>
				<?php }?>
				<?php  if(isset($this->data['KitchenhoodReport']['finish_date']) && ($this->data['KitchenhoodReport']['finish_date'] != "")){ ?>
				<tr><td align="center"><font color="red" size="3px">The form is disabled, as you have finished the report. </font></td></tr>
				<?php } ?>
			</table>
		</div>
                <?php echo $form->end(); ?>		
        </div>
</div>
<!-- This contains the hidden content for inline calls -->
<div style='display:none'>		
	<div id='inline_content_attachment' style='padding:10px; background:#fff;'>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'attachment','name'=>'attachment','id'=>'attachment',"onsubmit"=>"return checkUploadedAttachment();")); ?>
		<?php echo $form->input('Attachment.attach_file', array('id'=>'uploadattachment','type'=>'hidden','value'=>'')); ?>
		<?php echo $form->input('Attachment.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
		<?php echo $form->input('Attachment.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Attachment.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Attachment.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Attachment.client_id',array('type'=>'hidden','value'=>$clientId)); ?>		
		<p><strong>Upload Attachment</strong></p>
		<p>
		    <div id="demo"></div>
		    <ul id="separate-list"></ul>
		</p>
		<p><div id="uploaded_picture" style="text-align:center;padding-left:90px;"></div></p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($attachmentdata)){?>
			<p><strong>Attachments</strong></p>
		<?php } ?>		
		<?php foreach($attachmentdata as $attachdata){ ?>
			<p id="p_<?php echo $attachdata['Attachment']['id'] ?>"><?php echo $html->link($attachdata['Attachment']['attach_file'],array("controller"=>"reports","action"=>"download",$attachdata['Attachment']['attach_file']),array()).'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmattach('<?php echo $attachdata['Attachment']['id'] ?>')">&nbsp;<?php echo $html->image('closeNew.png',array('alt'=>'Delete','title'=>'delete','style'=>'vertical-align: middle'));?></a></p>
		<?php } ?>
		<?php echo $form->end(); ?>
	</div>	
</div>
<!-- This contains the hidden content for inline calls -->
<div style='display:none'>		
	<div id='inline_content_notification' style='padding:10px; background:#fff;'>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'notification','name'=>'notification','id'=>'notification',"onsubmit"=>"return checkNotification();")); ?>
		<?php echo $form->input('Notification.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
		<?php echo $form->input('Notification.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Notification.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Notification.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Notification.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		<p><strong>Notification</strong></p>
		<p>
		    <?php echo $form->input('Notification.notification',array('type'=>'textarea','rows'=>5,'cols'=>40,'label'=>false,'id'=>'ContentMessage','onKeyUp' =>"return limit('ContentMessage',500);")); ?>
		</p>
		<p>
		    <div id="essay_Error" class='error'></div>
			<div>
			<small>&nbsp;<label id='limitCounter'>0</label>&nbsp;characters entered&nbsp;|&nbsp;<label id='limitCounterLeft'>500</label>&nbsp;characters remaining</small>
			</div>   
		</p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($notifierdata)){?>
			<p><strong>Notifiers</strong></p>
		<?php } ?>		
		<?php foreach($notifierdata as $notifydata){ ?>
			<p id="pnotify_<?php echo $notifydata['Notification']['id'] ?>"><?php echo $notifydata['Notification']['notification'].'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmnotifier('<?php echo $notifydata['Notification']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>
		<?php echo $form->end(); ?>
	</div>		
</div>