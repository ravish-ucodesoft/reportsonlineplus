<?php echo $html->css('manager/style.css');?>
<?php echo $html->css('manager/theme.css');?>
<div id="wrapper">
    <div id="content">
        <div id="box">            
            <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td colspan="2" align="center"><strong>Fire Alarm Report</strong> </td>
                </tr>    
                <tr>
                    <td colspan="2">
                        <table width="100%">
                            <!--<tr>
                                <td align="center"><img src="/app/webroot/img/company_logo/<?php //echo $spResult['Company']['company_logo']?>"></td>
                            </tr>-->
                            <tr>
                                <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b><?php echo $spResult['Company']['name']?></b></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:21px">
                                <?php 
                                    $arr=explode(',',$spResult['Company']['interest_service']);	    
                                    foreach($arr as $key=>$val):
                                        foreach($reportType as $key1=>$val1):
                                            if($val== $key1)
                                            {
                                                
                                                 if($key1 == 9){
                                                    echo  $spResult['Company']['interest_service_other'];
                                                    }else{
                                                        echo $val1;
                                                        }
                                               echo '<span class="red"> * </span>';
                                            }		    
                                        endforeach;		    
                                    endforeach;
                                ?>    
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:24px">Fire Alarm Report</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>          
                            <tr>
                                <td align="right" class="Cpage">
                                    Date:
                                    <?php if($fireData['FirealarmReport']['finish_date'] != ''){
                                        echo  date('m/d/Y',strtotime($fireData['FirealarmReport']['finish_date']));
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage"><b><i>Prepared for:</i></b></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage">
                                    <?php
                                        $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
                                        echo ucwords($clientResult['User']['client_company_name']).'<br/>';
                                        echo ucwords($clientResult['User']['address']).', '.$cityName.', '.$clientResult['State']['name'].'<br/>';
                                        echo $clientResult['Country']['name'];
                                    ?>
                                </td>
                            </tr>
                            <!--<tr>
                                <td align="center" class="Cpage"><?php //echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage">
                                <?php
                                    /*if(!empty($siteaddrResult['SpSiteAddress']['country_id'])){
                                        echo $this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']).', ';
                                    }
                                    if(!empty($siteaddrResult['SpSiteAddress']['state_id'])) { echo $this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '; }
                                    $upSiteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
                                    echo $upSiteCityName;*/
                                ?>
                                </td>
                            </tr>-->
                            <tr>
                                <td align="center" class="Cpage" ><?php echo $clientResult['User']['site_phone'];?></td>
                            </tr>   
                            <tr>
                                <td align="left" class="Cpage" ><b>Prepared By:</b></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['Company']['name'];?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['address']?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php
                                    if(!empty($spResult['Country']['name'])){
                                        echo $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) { echo $spResult['State']['name'].', '; }
                                    $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
                                    echo $spCity;?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['phone'];?></td>
                            </tr> 
                            <!--<tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="center"><h2><?php //echo ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']);?></h2></td>
                            </tr>-->          
                        </table>
                    </td>
                </tr>
            </table>
            <h3 align="center">Fire Alarm Report</h3>
            <h4 class="table_title" style="text-align: center; font-size: 18px; padding:14px 0;">Agreement and Report of Inspection</h4>
            <table cellpadding="0" cellspacing="0" border="0" class="table_new" style="border:1px solid #C6C6C6">
                <!--<tr>
                    <td colspan="2">
                        <b>Days Left for report Submission: <?php //echo $daysRemaining;?></b>
                    </td>                    
                </tr>-->
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"> <b>Client information</b></th>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Building Information</strong></td>
                                <td><?php echo $clientResult['User']['client_company_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $clientResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $clientCity = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];?>
                                <td><?php echo $clientCity.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $clientResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $clientResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $clientResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"> <b>Site Information</b></th>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Site Name:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
                                <td><?php echo $siteCity.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $siteaddrResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr><th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"> <b>Service Provider Info</b></th></tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Name:</strong></td>
                                <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $spResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
                                <td><?php echo $spCity.', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $spResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $spResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $spResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
            </table>
            <div style="padding: 5px">
                <div style="border:2px solid #000; padding:10px; margin:10px">
                    <?php echo $texts;?>
                </div>
            </div>
            
            
            <table style='margin-left:50px;width: 90%;border:1px solid #C6C6C6'  class='tbl input-chngs1 firealramreport'>
                <tr>
                    <td><b>Functions</b></td>
                    <td><b>Description</b></td>
                    <td><b>Test Result</b></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" class='alramreporttest' style="background-color: #CCCCCC">Alarm Functions</td>
                </tr>
                
                <tr>
                    <td>Alarm Bells/Horns/Strobes</td>
                    <td><?php echo $fireData['FaAlarmFunction']['description']; ?></td>
                    <td><?php echo $fireData['FaAlarmFunction']['result']; ?></td>
                </tr>
                
                 <tr>
                    <td colspan="3" class='alramreporttest' style="background-color: #CCCCCC">Alarm Panel Supervisory Functions</td>
                </tr>
                
                <tr>
                    <td>Panel AC Power Loss</td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][0]['description']; ?></td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][0]['result']; ?></td>
                </tr>
                
                
                <tr>
                    <td>Panel Sec Power Loss</td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][1]['description']; ?></td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][0]['result']; ?></td>
                </tr>
                
                
                <tr>
                    <td>Open Alarm Circuits</td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][2]['description']; ?></td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][2]['result']; ?></td>
                </tr>
                
                <tr>
                    <td>Short Alarm Circuits</td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][3]['description']; ?></td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][3]['result']; ?></td>
                </tr>
                <tr>
                    <td>Panel to Panel Circuits</td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][4]['description']; ?></td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][4]['result']; ?></td>
                </tr>
                
                <tr>
                    <td>Ground Faults Detected</td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][5]['description']; ?></td>
                    <td><?php echo $fireData['FaAlarmPanelSupervisoryFunction'][5]['result']; ?></td>
                </tr>
                
                 <tr>
                    <td colspan="3" class='alramreporttest' style="background-color: #CCCCCC">Auxiliary Functions</td>
                </tr>
                
                <tr>
                    <td>Remote Annunciator</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][0]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][0]['result']; ?></td>
                </tr>
                 <tr>
                    <td>Stair Pressurization</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][1]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][1]['result']; ?></td>
                </tr>
                 <tr>
                    <td>Elevator Recall</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][2]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][2]['result']; ?></td>
                </tr>
                 <tr>
                    <td>Elevator Recall Primary Fl</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][3]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][3]['result']; ?></td>
                </tr>
                
                <tr>
                    <td>HVAC Shutdown</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][4]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][4]['result']; ?></td>
                </tr>
                <tr>
                    <td>High/Low Air</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][5]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][5]['result']; ?></td>
                </tr>
                
                <tr>
                    <td>Building Temp</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][6]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][6]['result']; ?></td>
                </tr>
                <tr>
                    <td>Site Water Temp</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][7]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][7]['result']; ?></td>
                </tr>
                
                <tr>
                    <td>Site Water Level</td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][8]['description']; ?></td>
                    <td><?php echo $fireData['FaAuxiliaryFunction'][8]['result']; ?></td>
                </tr>
                
                 <tr>
                    <td colspan="3" class='alramreporttest' style="background-color: #CCCCCC">Fire Pump Supervisory Functions</td>
                </tr>
                
                 <tr>                    
                    <td>Fire Pump Power</td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][0]['description']; ?></td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][0]['result']; ?></td>
                </tr>
                
                <tr>
                    <td>Fire Pump Running</td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][1]['description']; ?></td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][1]['result']; ?></td>
                </tr>
                  <tr>
                    <td>Fire Pump Phase Reversal</td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][2]['description']; ?></td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][2]['result']; ?></td>
                </tr>          
                 
                  <tr>
                    <td>Fire Pump Auto Position</td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][3]['description']; ?></td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][3]['result']; ?></td>
                </tr>  
                
                <tr>
                    <td>Fire Pump or Pump Controller Trouble</td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][4]['description']; ?></td>
                    <td><?php echo $fireData['FaPumpSupervisoryFunction'][4]['result']; ?></td>
                </tr>   
                 <tr>
                    <td colspan="3" class='alramreporttest' style="background-color: #CCCCCC">Generator Supervisory Functions</td>
                </tr>
                 
                  <tr>
                    <td>Generator In Auto Position</td>
                    <td><?php echo $fireData['FaGeneratorSupervisoryFunction'][0]['description']; ?></td>
                    <td><?php echo $fireData['FaGeneratorSupervisoryFunction'][0]['result']; ?></td>                    
                </tr>  
                
                <tr>
                    <td>Generator or Controller Trouble</td>
                    <td><?php echo $fireData['FaGeneratorSupervisoryFunction'][1]['description']; ?></td>
                    <td><?php echo $fireData['FaGeneratorSupervisoryFunction'][1]['result']; ?></td>
                </tr>   
                <tr>
                    <td>Switch Transfer</td>
                    <td><?php echo $fireData['FaGeneratorSupervisoryFunction'][2]['description']; ?></td>
                    <td><?php echo $fireData['FaGeneratorSupervisoryFunction'][2]['result']; ?></td>
                </tr>  
                
                <tr>
                    <td>Generator Engine Running</td>
                    <td><?php echo $fireData['FaGeneratorSupervisoryFunction'][3]['description']; ?></td>
                    <td><?php echo $fireData['FaGeneratorSupervisoryFunction'][3]['result']; ?></td>
                </tr>    
            
             
            </table>
            
            <table cellpadding=0 cellspacing=0 class="table_new" border=0 style="border: 1px solid #C6C6C6">
                <tr>
                    <td>                        
                        <img src="/app/webroot/img/signature_images/<?php echo $chkSign['Signature']['signature_image'];?>"><?php //echo $html->image('signatureImage');?>
                    </td>                    
                </tr>
                <tr>
                    <td>
                    Inspector's Signature
                    </td>
                </tr>
                <tr>
                    <td>
                    &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    Thanks & Regards
                    </td>
                </tr>
                <tr>
                    <td>
                    ReportsOnlineplus.com
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>