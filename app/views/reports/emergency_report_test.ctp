<?php echo $html->css('manager/style.css');?>
<?php echo $html->css('manager/theme.css');?>
<div id="wrapper">
    <div id="content">
        <div id="box">            
            <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td colspan="2" align="center"><strong>Emergency Light/Exit Inspection Report</strong> </td>
                </tr>    
                <tr>
                    <td colspan="2">
                        <table width="100%">                            
                            <tr>
                                <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b><?php echo $spResult['Company']['name']?></b></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:21px">
                                <?php 
                                    $arr=explode(',',$spResult['Company']['interest_service']);	    
                                    foreach($arr as $key=>$val):
                                        foreach($reportType as $key1=>$val1):
                                            if($val== $key1)
                                            {
                                                
                                                 if($key1 == 9){
                                                    echo  $spResult['Company']['interest_service_other'];
                                                    }else{
                                                        echo $val1;
                                                        }
                                               echo '<span class="red"> * </span>';
                                            }		    
                                        endforeach;		    
                                    endforeach;
                                ?>    
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:24px">Emergency Light/Exit Inspection Report</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>          
                            <tr>
                                <td align="right" class="Cpage">
                                    Date:
                                    <?php if($record['EmergencyexitReport']['finish_date'] != ''){
                                        echo  date('m/d/Y',strtotime($record['EmergencyexitReport']['finish_date']));
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><i><b>Prepared for:</b></i></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><?php                                        
                                         $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
                                        echo ucwords($clientResult['User']['client_company_name']).'<br/>';
                                        echo ucwords($clientResult['User']['address']).', '.$cityName.', '.$clientResult['State']['name'].'<br/>';
                                        echo $clientResult['Country']['name'];?></td>
                            </tr>                            
                            <tr>
                                <td align="center" class="Cpage" ><?php echo $clientResult['User']['site_phone'];?></td>
                            </tr>   
                            <tr>
                                <td align="left" class="Cpage"><b>Prepared By:</b></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['Company']['name'];?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['address']?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php
                                    if(!empty($spResult['Country']['name'])){
                                        echo $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) { echo $spResult['State']['name'].', '; }
                                    $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
                                    echo $spCity;?></td>                                
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['phone'];?></td>
                            </tr> 
                            <!--<tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="center"><h2><?php //echo ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']);?></h2></td>
                            </tr>-->          
                        </table>
                    </td>
                </tr>
            </table>
            <h3 align="center" >Emergency Light/Exit Inspection Report</h3>
            <h4 class="table_title" style="text-align: center; padding:14px 0;">Agreement and Report of Inspection</h4>
            <table cellpadding="0" cellspacing="0" border="0" class="table_new" style="border:1px solid #C6C6C6">                
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"> <b>Client information</b></th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Building Information</strong></td>
                                <td><?php echo $clientResult['User']['client_company_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $clientResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $clientCity = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];?>
                                <td><?php echo $clientCity.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $clientResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $clientResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td> <?php echo $clientResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"><b>Site Information</b></th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Site Name:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
                                <td> <?php echo $siteCity.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $siteaddrResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"> <b>Service Provider Info</b></th></tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Name:</strong></td>
                                <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $spResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
                                <td><?php echo $spCity.', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $spResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $spResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $spResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="padding: 5px">
                <div style="border:2px solid #000; padding:10px; margin:10px">
                    <?php echo $texts;?>
                </div>
            </div>
            
            
            
            <table cellpadding="0" cellspacing="0" border="1" class="table_new">               
                <tr>
                    <td colspan="7" style="text-align: center;"><b>Summary</b></td>                    
                </tr>
                <tr style="background-color:#C5E5FF;">
                    <td>&nbsp;</td>
                    <td>Exit Signs</td>
                    <td>Emergency Lights</td>
                    <td>Exit Pass</td>
                    <td>Exit Fail</td>
                    <td>Emergency Pass</td>
                    <td>Emergency Fail</td>
                </tr>
                <tr>
                    <td>Surveyed</td>
                    <td><?php echo @$exit_amount;?></td>
                    <td><?php echo @$emergency_amount;?></td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td><?php echo @$exit_amount;?></td>
                    <td><?php echo @$emergency_amount;?></td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Difference</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>                               
            </table>
            <div style="width: 100%; min-height:10px;">&nbsp;</div>
            <table cellpadding=0 cellspacing=0 class="table_new" border="1">
                <tr style="background-color:#C5E5FF;">
                    <td width="18%">Location</td>
                    <td width="18%">MFD Date</td>
                    <td width="18%">Type</td>
                    <td width="18%">Status</td>
                    <td width="28%">Recommendation/Comment</td>
                </tr>
                <?php
                if(isset($data) && !empty($data)){
                    foreach($data as $key=>$val){ 
                ?>
                <tr>
                    <td><?php echo $val['location']; ?></td>
                    <td><?php $mfd = explode(" ",$val['mfd_date']); echo $mfd[0]; ?></td>
                    <td><?php echo $this->Common->getServiceName($val['type']); ?></td>
                    <td><?php echo $val['status']; ?></td>
                    <td><?php echo $val['comment']; ?></td>
                </tr>
                <?php
                    }
                }else{    
                ?>        
                <tr>
                    <td colspan="5">No Record Found</td>
                </tr>    
                <?php                    
                }    
                ?>
            </table>
            <table cellpadding=0 cellspacing=0 class="table_new" border=0 >
                <tr>
                    <td>
                        <?php if(isset($chkSign) && $chkSign['Signature']['signature_image']!=""){?>
                        <img src="/app/webroot/img/signature_images/<?php echo $chkSign['Signature']['signature_image'];?>">
                        <?php }?>
                    </td>                    
                </tr>
                <tr>
                    <td>
                    Inspector's Signature
                    </td>
                </tr>
                <tr>
                    <td>
                    &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    Thanks & Regards
                    </td>
                </tr>
                <tr>
                    <td>
                    ReportsOnlineplus.com
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>