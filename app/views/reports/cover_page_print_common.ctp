<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');
        switch($modelName){
            case 'FirealarmReport':
                $reportName = 'Firealarm Report';
                $reportTitle='Firealarm';
                break;
            case 'SprinklerReport':
                $reportName = 'Sprinkler/Suppresion Report';
                $reportTitle='Sprinkler';
                break;
            case 'KitchenhoodReport':
                $reportName = 'Kitchenhood Report';
                $reportTitle='Kitchenhood';
                break;
            case 'EmergencyexitReport':
                $reportName = 'Emergency Exit Report';
                $reportTitle='EmergencyExit';
                break;
            case 'ExtinguisherReport':
                $reportName = 'Extinguisher Report';
                $reportTitle='Extinguisher';
                break;
            case 'SpecialhazardReport':
                $reportName = 'Specialhazard Report';
                $reportTitle='Specialhazard';
                break;
            default:
                $reportName = 'Firealarm Report';
                $reportTitle='Firealarm';
                break;
        }
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$element_cover=$this->element('reports/report_common_cover',array('report_name'=>$reportName,'finish_date'=>$record[$modelName]['finish_date'],'freq'=>$schFreq));
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>';
$html.=$element_cover;

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_'.$reportTitle.'_Report_CoverPage.pdf', 'I');die;
?>