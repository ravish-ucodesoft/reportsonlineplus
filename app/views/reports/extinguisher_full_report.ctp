<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
$element=$this->element('reports/report_common_view');
$element_cover=$this->element('reports/report_common_cover',array('report_name'=>'Extinguisher Report','finish_date'=>$record['ExtinguisherReport']['finish_date'],'freq'=>$schFreq));
$commonReportPagePart = $this->element('reports/report_page_common_view');
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setHeaderData('../../../webroot/img/company_logo/'.$spResult['Company']['company_logo'],21,'','');
//$tcpdf->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
$tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->setPrintHeader(true);

$tcpdf->SetAutoPageBreak( true );
//$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>';
$html.=$element_cover;
//Summary Page        
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%">	
        <tr>
            <td style="text-align:center; font-size:36px;"><b>Summary Page</b></td>        
        </tr>';
        $html.=$element;
		
		 $html.='<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	    </table>
      <table width="100%" class="qtytable" border="1" cellpadding="2">
        <tr style="background-color: #3688B7;">
          <td rowspan="2" style="color:#fff; text-align:center;">Sr. No.</td>
	  <td rowspan="2" style="color:#fff; text-align:center;">Device</td>
          <td style="color:#fff; text-align:center;">Surveyed</td>
          <td style="color:#fff; text-align:center;">Inspected</td>
          <td style="color:#fff; text-align:center;">Pass</td>          
          <td style="color:#fff; text-align:center;">Fail</td>
        </tr>
        <tr style="background-color: #666666;">
          <td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td>
        </tr>';
	$summCounter = 0;
        foreach($options as $serviceData){
	    $amount[]= $serviceData['amount'];
	    $served[] = $serviceData['served'];
	    $pass[] = $serviceData['pass'];
	    $fail[] = $serviceData['fail'];  
            $summCounter++;    
        $html.='<tr>
          <td style="text-align:center">'.$summCounter.'.</td>
	  <td style="text-align:center">'.$serviceData['name'].'</td>
          <td style="text-align:center">'.$serviceData['amount'].'</td>
          <td style="text-align:center">'.$serviceData['served'].'</td>
          <td style="text-align:center">'.$serviceData['pass'].'</td>
          <td style="text-align:center">'.$serviceData['fail'].'</td>
        </tr>';
        }
        $html.='<tr style="background-color: #3688B7;">
          <td style="color:#fff; text-align:center">Total</td>
	  <td style="color:#fff; text-align:center"></td>
          <td style="color:#fff; text-align:center">'.array_sum($amount).'</td>
          <td style="color:#fff; text-align:center">'.array_sum($served).'</td>
          <td style="color:#fff; text-align:center">'.array_sum($pass).'</td>
          <td style="color:#fff; text-align:center">'.array_sum($fail).'</td>
        </tr>
        </table>';
//Report Page        
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
	<tr>
	  <td align="center" style="border:none;"><h3 style="margin-bottom:10px;font-size:36px;" align="center" ><b>Report Page</b></h3></td>          
	</tr>      
</table>';
$html.=$commonReportPagePart;
$html.='<table width="100%">
    <tr>
            <td colspan="4"><h3 class="main-heading" align="center" style="font-size:36px;"><b>Inspection Report</b></h3></td>
    </tr>
    <tr>
            <td class="lbtxt" width="30%"><b>Inspection Report:</b></td>
            <td width="20%">'.$record['ExtinguisherReport']['inspection_report'].'</td>
            <td class="lbtxt" width="30%"><b>Inspection Contract Number:</b></td>
            <td width="20%">'.$record['ExtinguisherReport']['inspection_contract_number'].'</td>
    </tr>
    <tr>
            <td class="lbtxt"><b>Inspection Date:</b></td>
            <td>'.$record['ExtinguisherReport']['inspection_date'].'</td>
            <td class="lbtxt"><b>Inspection Time:</b></td>
            <td>'.$record['ExtinguisherReport']['inspection_time'].'</td>
    </tr>    	
</table>
<h3 class="main-heading" align="center" style="width:100%">PORTABLE FIRE EXTINGUISHER REPORT RECORD</h3>
<table width="100%" class="qtytable tablesorter" id="sortTable" border="1" style="font-size:20px;">
    <thead>
        <tr style="background-color:#9FBFDF; color:#000000;">
            <th rowspan="2" width="10%" style="text-align:center;">Sr. No.</th>
	    <th rowspan="2" width="15%" style="text-align:center;">Location</th>
            <th rowspan="2" width="5%" style="text-align:center;">Type</th>
            <th rowspan="2" width="6%" style="text-align:center;">Size</th>
            <th rowspan="2" width="10%" style="text-align:center;">Born Date</th>
            <th width="8%" style="text-align:center;">Deficient</th>
            <th colspan="2" width="13%" style="text-align:center;">Last Maintenace</th>
            <th colspan="2" width="13%" style="text-align:center;">Last Recharge</th>
            <th colspan="2" width="13%" style="text-align:center;">Last Hydro test</th>            
        </tr>
        <tr style="background-color:#9FBFDF; color:#000000;">        
            <th style="text-align:center;" width="8%">Yes/No</th>
            
            <th width="13%" colspan="2" style="text-align:center;">Date</th>
           
            <th width="13%" colspan="2" style="text-align:center;">Date</th>
            
            <th width="13%" colspan="2" style="text-align:center;">Date</th>
                    
        </tr>        
    </thead>
    <tbody>';
    if(!empty($record['ExtinguisherReportRecord'])){
	$reportCounter = 0;
        foreach($record['ExtinguisherReportRecord'] as $recorddata){
		$tdColor = (($recorddata['deficiency']=='Yes')?'red':'#3D3D3D');
		$reportCounter++;
$html.='<tr style="font-size:20px; background-color:#FFFFFF; color:'.$tdColor.';">
            <td width="10%" style="text-align:left;">'.$reportCounter.'.</td>
	    <td width="15%" style="text-align:left;">'.$recorddata['location'].'</td>
            <td width="5%" style="text-align:center;">'.$this->Common->getServiceName($recorddata['ext_type']).'</td>
            <td width="6%" style="text-align:center;">'.$recorddata['model'].'</td>
            <td width="10%" style="text-align:center;">'.$recorddata['born_date'].'</td>
            <td width="8%" style="text-align:center;">'.$recorddata['deficiency'].'</td>
            
            <td width="13%" colspan="2" style="text-align:center;">'.$recorddata['last_maintenace_date'].'</td>    
            
            <td width="13%" colspan="2" style="text-align:center;">'.$recorddata['last_recharge_date'].'</td>
            
            <td width="13%" colspan="2" style="text-align:center;">'.$recorddata['last_hydrotest_date'].'</td>
            
        </tr>';
    } } 
$html.='</tbody>        
        <tr>
            <td colspan="14" align="left" style="text-align:left">Comments:</td>
        </tr>
        <tr>
            <td colspan="14" align="left" style="text-align:left">'.$record['ExtinguisherReport']['comments'].'</td>
        </tr>
</table>';


//Deficiency Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%">    	
    <tr>
        <td align="center" colspan="2" style="font-size: 36px; color:red"><b>Deficiencies</b></td>
    </tr>';
        $html.=$element;
$html.='</table>


<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
    <tr>
        <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
	<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
        <th width="60%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
	<th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Attachment</b></th>
    </tr>';    
    if(!empty($Def_record))
    {
	$defCounter=0;
        foreach($Def_record as $Def_record) {
		$defCounter++;
    $html.='<tr>        
        <td valign="top">'.$defCounter.'.</td>
	<td valign="top">'.$Def_record['Code']['code'].'</td>
        <td valign="top">'.$Def_record['Code']['description'].'</td>
	<td valign="top">';
		if(!empty($Def_record['Deficiency']['attachment']) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$Def_record['Deficiency']['attachment'])){
		    //$html.=$html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$Def_record['Deficiency']['attachment'],'sp'=>false),array('title'=>'Click to Download'));
		    $html.='Attachment'; 
		}else{
		    $html.='No Attachment';
		}
	$html.='</td>
    </tr>';
        }
    }
    else
    {
        $html.='<tr><td colspan="4" align="center" style="font-size:36px;">No Deficiency Found.</td></tr>';   
    }    
$html.='</table>';


//Recommendation Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px; color:orange"><b>Recommendation</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: Extinguisher</b></div>
<br/>
<table width="100%">
    <tr>
        <td align="right" colspan="2"></td>
    </tr>';
        $html.=$element;
		
		 $html.='</table>

    
<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
    <tr>        
        <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
	<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
        <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
        <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>        
    </tr>';    
    if(!empty($Def_record_recomm))
    {
	$recommCounter = 0;
        foreach($Def_record_recomm as $Def_record_recomm) {
		$recommCounter++;
        $html.='<tr>        
            <td valign="top">'.$recommCounter.'.</td>
	    <td valign="top">'.$Def_record_recomm['Code']['code'].'</td>
            <td valign="top">'.$Def_record_recomm['Code']['description'].'</td>
            <td valign="top">'.$Def_record_recomm['Deficiency']['recommendation'].'</td>        
        </tr>';
        }
    }
    else
    {
        $html.='<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
    }    
$html.='</table>';


//Missed Items Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
                <td colspan="2" align="center" style="border:none;"><span style="font-size:36px; color:green;"><b>Missed</b></span></td>                
        </tr>      
</table>

<table width="100%" cellpadding="0" cellspacing="0">';
        $html.=$element;
		
		 $html.='</table>

<table width="100%" class="qtytable" border="1">
        <tr>
                <td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Sr. No.</td>
		<td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Device</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Surveyed</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Inspected</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Pass</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Fail</td>                
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Missed item</td>
        </tr>
        <tr style="background-color:#666; color:#fff; font-weight:bold">
                <td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td>
        </tr>';
	$missingCounter=0;
        foreach($options as $serviceData){
	    $amount[]= $serviceData['amount'];
	    $served[] = $serviceData['served'];
	    $pass[] = $serviceData['pass'];
	    $fail[] = $serviceData['fail'];
	    $missingCounter++;
        $html.='<tr>
                <td style="text-align:center;">'.$missingCounter.'.</td>
		<td style="text-align:center;">'.$serviceData['name'].'</td>
                <td style="text-align:center;">'.$serviceData['amount'].'</td>
                <td style="text-align:center;">'.$serviceData['served'].'</td>
                <td style="text-align:center;">'.$serviceData['pass'].'</td>
                <td style="text-align:center;">'.$serviceData['fail'].'</td>';                
                if($serviceData['amount']>$serviceData['served']){
		    $missed = $serviceData['amount']-$serviceData['served'];
		}else{
		    $missed = 0;
		}               
                $html.='<td style="text-align:center;">'.$missed.'</td>
        </tr>';        
        }  
$html.='</table>';
$html.='<table style="color: green">';	
	$missingData = $this->Common->missingTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
	$missingText=$missingData['ExplanationText']['missing_reason'];
	$missingfile=$missingData['ExplanationText']['missing_file'];
	$missingText = (($missingText!="")?$missingText:'No explanation added');
	if(!empty($missingfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$missingfile)){
		$showMissfile = 'Attachment';
	}else{
		$showMissfile='No Attachment';
	}
	$html.='<tr>								
			<td colspan="6">Missed Items Explanation: '.$missingText.'</td>
		</tr>
		<tr>								
			<td colspan="6">Missed Items Attachment: '.$showMissfile.'</td>
		</tr>
</table>';

//Additional Items Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
                <td colspan="2" align="center" style="border:none;"><span style="font-size:36px; color:purple;"><b>Additional</b></span></td>                
        </tr>      
</table>

<table width="100%" cellpadding="0" cellspacing="0">';
        $html.=$element;
		
		 $html.='</table>

<table width="100%" class="qtytable" border="1">
        <tr>
                <td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Sr. No.</td>
		<td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Device</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Surveyed</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Inspected</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Pass</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Fail</td>                
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Additional Item</td>
        </tr>
        <tr style="background-color:#666; color:#fff; font-weight:bold">
                <td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td>
        </tr>';
	$addCounter=0;
        foreach($options as $serviceData){
	    $amount[]= $serviceData['amount'];
	    $served[] = $serviceData['served'];
	    $pass[] = $serviceData['pass'];
	    $fail[] = $serviceData['fail'];
	    $addCounter++;
        $html.='<tr>
                <td style="text-align:center;">'.$addCounter.'.</td>
		<td style="text-align:center;">'.$serviceData['name'].'</td>
                <td style="text-align:center;">'.$serviceData['amount'].'</td>
                <td style="text-align:center;">'.$serviceData['served'].'</td>
                <td style="text-align:center;">'.$serviceData['pass'].'</td>
                <td style="text-align:center;">'.$serviceData['fail'].'</td>';
                if($serviceData['served']>$serviceData['amount']){
                    $additional = $serviceData['served']-$serviceData['amount'];
		}else{
                    $additional = 0;
		}                
                $html.='<td style="text-align:center;">'.$additional.'</td>
        </tr>';        
        } 
$html.='</table>';
$html.='<table style="color: purple">';	
	$additionalData = $this->Common->additionalTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
	$additionalText=$additionalData['ExplanationText']['additional_reason'];
	$additionalfile=$additionalData['ExplanationText']['additional_file'];
	$additionalText = (($additionalText!="")?$additionalText:'No explanation added');
	if(!empty($additionalfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$additionalfile)){
		$showAdditionalfile = 'Attachment';
	}else{
		$showAdditionalfile='No Attachment';
	}
	$html.='<tr>								
			<td colspan="6">Additional Items Explanation: '.$additionalText.'</td>
		</tr>
		<tr>								
			<td colspan="6">Additional Items Attachment: '.$showAdditionalfile.'</td>
		</tr>
</table>';

//Signature Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
                <td colspan="2" align="center" style="border:none;"><span style="font-size:36px;"><b>Signature Page</b></span></td>                
        </tr>	   
</table>

<table width="100%" cellpadding="0" cellspacing="0">';
        $html.=$element;
		
		 $html.='</table>

<table width="100%">
        <tr>
                <td style="border:none;"><b>Inspector Signature</b></td>
                <td style="border:none; vertical-align:top;" align="right"><b>Client Signature</b></td>	  
        </tr>
        <tr>
            <td style="border:none">';
                $name=$name=$this->Common->getInspectorSignature($record['ExtinguisherReport']['report_id'],$record['ExtinguisherReport']['servicecall_id'],$record['ExtinguisherReport']['inspector_id']);
                $html.='<img src="/app/webroot/img/signature_images/'.$name.'">';                
                $html.='</td>
            <td style="border:none;" align="right">&nbsp;</td>    
        </tr>		
	<tr>
		<td align="left" class="Cpage">'.$inspector['User']['email'].'</td>
		<td style="border:none;" align="right">'.$clientResult['User']['email'].'</td>    
	</tr>
	<tr>
		<td align="left" class="Cpage">'.$inspector['User']['phone'].'</td>
		<td style="border:none;" align="right">'.$clientResult['User']['phone'].'</td>    
	</tr>
        <tr>
                <td style="border:none;">';
                $dateCreated=$this->Common->getSignatureCreatedDate($record['ExtinguisherReport']['report_id'],$record['ExtinguisherReport']['servicecall_id'],$record['ExtinguisherReport']['inspector_id']);
                if($dateCreated!='empty'){
                        $html.=$this->Common->getClientName($record['ExtinguisherReport']['inspector_id'])."<br/><br/>";
                        $html.=$time->Format('m-d-Y',$dateCreated);
                }                
                $html.='</td>
                <td align="right" style="border:none;">';
                //$dateCreated=$this->Common->getSignatureCreatedDate($record['ExtinguisherReport']['report_id'],$record['ExtinguisherReport']['servicecall_id'],$record['ExtinguisherReport']['inspector_id']);
                //if($dateCreated!='empty'){
                        $html.=$this->Common->getClientName($record['ExtinguisherReport']['client_id'])."<br/><br/>";
                       //$html.=$time->Format('m-d-Y',$dateCreated);
                //}                
                $html.='</td>
        </tr>
</table>';

//Cert Attached Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px;"><b>Certification</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: Extinguisher</b></div>
<br/>
<table width="100%">';
        $html.=$element;
		
		 $html.='</table>



<table class="tbl input-chngs1" width="100%" >                
        <tr>
                <td align="left" style="text-align:left">';
                
                $certs = $this->Common->getAllSchCerts($schId);
		$html.='<div style="font-size:30px;"><b>Attached Certificates</b></div>';
		$html.='<br/>';
                if(sizeof($certs)>0){                
                    $cr=1;
                    foreach($certs as $cert)
                    {
                        $html.=$cr.'.'.$cert['ReportCert']['ins_cert_title'];
                        $html.='<br/>';                    
                        $cr++;
                    }               
                }else{
			$html.='No Certificate has been attached for this report.';
		}
                $html.='</td>
        </tr>	
</table>';

//Quotes Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px;"><b>Quotes</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: Extinguisher</b></div>
<br/>
<table width="100%">';
        $html.=$element;
		
		 $html.='</table>


<table class="tbl input-chngs1" width="100%" >                
        <tr>
                <td align="left" style="text-align:left">
                        <div style="font-size:30px;"><b>Quote Form from SP</b></div>';                        
                        if(isset($chkRecord) && !empty($chkRecord)){
                                $html.=$chkRecord['Quote']['title'];
                        }else{
                                $html.='No Quote have been submitted by Service Provider.';
                        }                        
                $html.='</td>
        </tr>
        <tr>
                <td>&nbsp;</td>
        </tr>
        <tr>
                <td align="left" style="text-align:left">
                <div style="font-size:30px;"><b>Client\'s Response to Quote Form</b></div>';                
                $quotedocs = $this->Common->getQuoteDoc($_REQUEST['reportID'],$_REQUEST['clientID'],$_REQUEST['spID'],$_REQUEST['serviceCallID']);                
                if(isset($quotedocs) && !empty($quotedocs)){
                        if($quotedocs['Quote']['client_response']=='a'){
                                $status = 'Accepted';
                        }else if($quotedocs['Quote']['client_response']=='d'){
                                $status = 'Denied';
                        }else if($quotedocs['Quote']['client_response']=='n'){
                                $status = 'No wish to fix the deficiencies';
                        }else{
                                $status = 'Pending';
                        }                        
                        $html.='<div>Status: '.$status.'</div>';
                        if($quotedocs['Quote']['client_response']=='a'){
                        $html.='<div class="certificates-hdline" style="text-align: left;">';
                            $html.='Signed Quote Form';
                        $html.='</div>';
                        }else if($quotedocs['Quote']['client_response']=='d'){
                        $html.='<div class="certificates-hdline" style="text-align: left;">';
                            $html.='Work Orders';
                        $html.='</div>';
                        }
                        
                }else{
                    $html.='NA';
                }
                $html.='</td>
        </tr>
</table>';		

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
//$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;


$delimiter = '<hr style="color:#fff"/>';
$chunks    = explode($delimiter, $html);
$cnt       = count($chunks);

for ($i = 0; $i < $cnt; $i++) {
    $tcpdf->writeHTML($chunks[$i], true, 0, true, 0);

    if ($i < $cnt - 1) {
        $tcpdf->AddPage();
    }
}

// Reset pointer to the last page
$tcpdf->lastPage();


// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Extinguisher_Report.pdf', 'I');die;
?>