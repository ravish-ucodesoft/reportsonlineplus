<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;     
}
#box {
    border: none;
}

</style>
<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'extinguishers','name'=>'extinguisher','id'=>'extinguisher')); ?> 
<div id="content">
     <div id="box" style="width:700px;padding-left:10px;">
     <div class="form-box" style="width:90%">
        <h2 style="font-weight:bold;">Notification</h2>
        <ul class="register-form">
            <li>
                <label>&nbsp;</label>
                <p><?php echo $form->input('ExtinguisherReport.description',array('type'=>'textarea','rows'=>5,'cols'=>40,'label'=>false)); ?></p>
            </li>
	    <li>
                <label>&nbsp;</label>
                <p><span class="blue-btn" style="float:right;"><input type="submit" value="Save" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></td></p>
            </li>
            
        </ul>
    </div>
    <!--one complete form ends-->
      </div>
</div>
<?php echo $form->end(); ?>