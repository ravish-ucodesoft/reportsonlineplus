<?php 
    echo $this->Html->script('jquery.1.6.1.min');
?>
<?php echo $html->css('manager/style'); ?>
<style>
th,td{
    border:none;    
}
.tbl input-chngs1 th
{
    color:#fff;
    background-color:#3688B7;
}
</style>
<div style="width:950px;height:500px">
    <h2 style="text-align:center"><?php echo @$codeData[0]['Report']['name']. " Deficiency Codes" ?></h2>
    <div style="float:left;font-size:11px;padding-left:30px;margin-top:5px;"><b>Reference: <?php echo @$codeData[0]['Report']['name']; ?></b></div>
    <div style="float:left;font-size:11px;padding-left:30px;margin-top:5px;"><i>Use the following deficiency codes for <?php echo @$codeData[0]['Report']['name']; ?>&nbsp;.Please check the respective checkbox for any deficiency found and enter your recommendation for the same</i></div>
    <table>
        <tr>
            <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
            <br/>
            <?php echo ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']); ?>
            <br/>
            <?php echo $clientResult['User']['site_name']; ?>
            <br/>
            <?php echo $clientResult['User']['site_city_state_zip']; ?>
            <br/>
            <?php echo $clientResult['User']['phone']; ?>
            </td>
            <td width="50%" align="right" style="padding-right:30px;">
                <i><b>Prepared By:</b></i>
                <br/>
                Wayman Fire Protection, Inc.
                <br/>
                403 Meco Drive
                <br/>
                Wilmington, DE 19804
                <br/>
                Phone 302-994-5757 Fax 302-994-5750
            </td>
            
            </tr>
    </table>
    <?php echo $form->create('Reports', array('action'=>'defAndRec','name'=>'specialhazardDeficiency','id'=>'specialhazardDeficiency','inputDefaults' => array('label' => false,'div' => false)));
    echo $form->input('Deficiency.servicecall_id',array('type'=>'hidden','value'=>@$servicecallId));
    echo $form->input('Deficiency.sp_id',array('type'=>'hidden','value'=>@$spId));
    echo $form->input('Deficiency.inspector_id',array('type'=>'hidden','value'=>@$inspector_id));
    echo $form->input('Deficiency.client_id',array('type'=>'hidden','value'=>@$clientId));
    echo $form->input('Deficiency.report_id',array('type'=>'hidden','value'=>@$reportId));
    //echo $form->input('Deficiency.id',array('type'=>'hidden','value'=>@$result['id'])); ?>
    <table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
        <tr>
            <th width="5%" style="background-color:#3688B7;color:#fff;">&nbsp;</th>
            <th width="5%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
            <th width="30%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
            <th width="60%" style="text-align:center;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>
        </tr>
        <?php
        if(!empty($codeData))
        {
       // pr($codeData);
        foreach($codeData as $cData) {
            ?>
        <tr>
            <td align="center" valign="top">                
                <?php
                if(!empty($cData['Deficiency']))
                {
                    echo $form->input('Deficiency.check.'.$cData['Code']['id'],array('id'=>'pq_'.$cData['Code']['id'],'type'=>'checkbox','label'=>false,'div'=>false,'hidden'=>false,'style'=>'width:10px;','onClick'=> 'showTextarea('.$cData['Code']['id'].')','checked'=>true));
                    $recommendation=$cData['Deficiency'][0]['recommendation'];
                    $style='style="display:block"';
                }
                else
                {
                   echo $form->input('Deficiency.check.'.$cData['Code']['id'],array('id'=>'pq_'.$cData['Code']['id'],'type'=>'checkbox','label'=>false,'div'=>false,'hidden'=>false,'style'=>'width:10px;','onClick'=> 'showTextarea('.$cData['Code']['id'].')'));
                    $recommendation='';
                    $style='style="display:none"';
                }
                ?>
                     
            </td>
            <td valign="top"><?php echo $cData['Code']['code']; ?></td>
            <td valign="top"><?php echo $cData['Code']['description']; ?></td>
            <td align="right" valign="top"> <p id="pnew_<?php echo $cData['Code']['id']; ?>" <?php echo $style ?>><?php echo $form->input('Deficiency.recommendation.'.$cData['Code']['id'],array('type'=>'textarea','rows'=>4,'cols'=>40,'label'=>false,'div'=>false,'value'=>$recommendation)); ?></p></td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="4">
                <p><span class="blue-btn" style="float:left;"><input type="submit" value="Submit" style="margin-right:0; border:0; color:#fff; width:50px; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
            </td>
        </tr>
        <?php } else
        {
         echo '<tr><td colspan="4" align="center">No Record Found</td></tr>';   
        }
        ?>
    </table>
    <?php echo $form->end(); ?>
</div>
<script>
function showTextarea(rid)
{
   var oid= "pq_"+rid;   
if(document.getElementById(oid).checked){
  // Insert code here.

jQuery("#pnew_"+rid).fadeIn(500);

    }else{
     
jQuery("#pnew_"+rid).fadeOut(500);  
    } 

}
</script>