<?php echo $html->css('manager/style.css');?>
<?php echo $html->css('manager/theme.css');?>
<div id="wrapper">
    <div id="content">
        <div id="box">            
            <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td colspan="2" align="center"><strong>Kitchenhood Report</strong></td>
                </tr>    
                <tr>
                    <td colspan="2">
                        <table width="100%">                            
                            <tr>
                                <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b><?php echo $spResult['Company']['name']?></b></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:21px">
                                <?php 
                                    $arr=explode(',',$spResult['Company']['interest_service']);	    
                                    foreach($arr as $key=>$val):
                                        foreach($reportType as $key1=>$val1):
                                            if($val== $key1)
                                            {
                                                
                                                 if($key1 == 9){
                                                    echo  $spResult['Company']['interest_service_other'];
                                                    }else{
                                                        echo $val1;
                                                        }
                                               echo '<span class="red"> * </span>';
                                            }		    
                                        endforeach;		    
                                    endforeach;
                                ?>    
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:24px">Kitchenhood Report</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>          
                            <tr>
                                <td align="right" class="Cpage">
                                    Date:
                                    <?php if($this->data['KitchenhoodReport']['finish_date'] != ''){
                                        echo  date('m/d/Y',strtotime($this->data['KitchenhoodReport']['finish_date']));
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><i>Prepared for:</i></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><?php echo ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname'])?></td>
                            </tr>                            
                            <tr>
                                <td align="center" class="Cpage"><?php
                                    if(!empty($clientResult['User']['country_id'])){
                                        echo $this->Common->getCountryName($clientResult['User']['country_id']).', ';
                                }
                                if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; } 
                                $cityName = ((isset($clientResult['City']['name']) && $clientResult['City']['name']!='')?$clientResult['City']['name']:$clientResult['User']['city']);
                                echo $cityName;
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><?php echo $clientResult['User']['phone'];?></td>
                            </tr>   
                            <tr>
                                <td align="left" class="Cpage" >Prepared By:</td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['Company']['name'];?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['address']?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php
                                    if(!empty($spResult['Country']['name'])){
                                        echo $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) { echo $spResult['State']['name'].', '; }
                                    $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
                                    echo $spCity; 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['phone'];?></td>
                            </tr> 
                            <!--<tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="center"><h2><?php //echo ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']);?></h2></td>
                            </tr>-->          
                        </table>
                    </td>
                </tr>
            </table>
            <h3 align="center">Kitchenhood Report</h3>
            <h4 class="table_title" style="text-align: center; font-size: 18px; padding:14px 0;">Agreement and Report of Inspection</h4>
            <table cellpadding="0" cellspacing="0" border="0" class="table_new" style="border:1px solid #C6C6C6">
                <tr>
                    <td><b>Days Left for report Submission: <?php echo $daysRemaining;?></b></td>                    
                </tr>
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6;"><b>Client information</b></th>
                </tr>
                <tr>
                    <td valign="top" width="50%">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Building Information</strong></td>
                                <td><?php echo $clientResult['User']['client_company_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $clientResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $clientCity = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];?>
                                <td> <?php echo $clientCity.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $clientResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="50%">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $clientResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td> <?php echo $clientResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6;"><b>Site Information</b></th>
                </tr>
                <tr>
                    <td valign="top" width="50%">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Site Name:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
                                <td> <?php echo $siteCity.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $siteaddrResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="50%">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><th colspan="2" align="left" style="background-color: #C6C6C6;"><b>Service Provider Info</b></th></tr>
                <tr>
                    <td valign="top" width="50%">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Name:</strong></td>
                                <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $spResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
                                <td><?php echo $spCity.', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $spResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="50%">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $spResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $spResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="padding: 5px">
                <div style="border:2px solid #000; padding:10px; margin:10px">
                    <?php echo $texts;?>
                </div>
            </div>            
            <table cellpadding="0" cellspacing="0" border="0" class="table_new" style="border: 1px solid #C6C6C6">
                <tr>
                    <th width="5%" style="text-align: center"><p class="lbl">#</p></th>
                    <th width="25%" style="text-align: center"><p class="lbl">Location</p></th>
                    <th width="25%" style="text-align: center"><p class="lbl">Born Date</p></th>
                    <th width="25%" style="text-align: center"><p class="lbl">Type</p></th>       
                    <th width="20%" style="text-align: center"><p class="lbl">Deficient</p></th>                    
                </tr>
                <?php
                if(isset($data) && !empty($data)){
                     $k = 0;
                    foreach($data as $key=>$val){
                    $k++;
                ?>
                <tr id="tr_<?php echo $val['id']; ?>">
                    <td style="text-align: center" width="5%"><?php echo $k; ?></td>
                    <td style="text-align: center" width="25%"><?php echo $val['location']; ?></td>
                    <td style="text-align: center" width="25%"><?php echo $val['born_date']; ?></td>
                    <td style="text-align: center" width="25%"><?php echo $this->Common->getServiceName($val['type']); ?></td>	    	    
                    <td style="text-align: center" width="20%" <?php if($val['deficiency']=='Yes'){?> style="color: red"<?php }?>><?php echo $val['deficiency']; ?></td>
                </tr>
                <?php
                    }}
                ?>
            </table>
            <table cellpadding=0 cellspacing=0 class="table_new" border=0 style="border: 1px solid #C6C6C6">
                <tr>
                    <td>
                        <img src="/app/webroot/img/signature_images/<?php echo $chkSign['Signature']['signature_image'];?>">
                    </td>                    
                </tr>
                <tr>
                    <td>
                    Inspector's Signature
                    </td>
                </tr>
                <tr>
                    <td>
                    &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    Thanks & Regards
                    </td>
                </tr>
                <tr>
                    <td>
                    ReportsOnlineplus.com
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>