<?php
echo $this->Html->script('jquery.1.6.1.min');   
echo $this->Html->css('validationEngine.jquery');
echo $this->Html->css(array('ajaxuploader/fileuploader'));

echo $this->Html->script('languages/jquery.validationEngine-en.js');
echo $this->Html->script('jquery.validationEngine.js');

echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
echo $this->Html->script(array('ajaxuploader/fileuploader'));
echo $this->Html->script(array('ui/ui.core.js','ui/ui.datepicker.js'));



?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript">
$(document).ready(function(){
	//jQuery(".ajax").colorbox();
	jQuery(".inline").colorbox({inline:true, width:"50%"});
	loadDatePicker();
	jQuery("#extinguisher").validationEngine();
});
 function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
/*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
	    }
            });           
    }        
window.onload = createUploader;
	
function loadDatePicker()
{
   jQuery(".calender").datepicker({ dateFormat: 'mm/dd/yy',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });
}
 function checkUploadedAttachment(){
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
  function checkNotification(){
	var notifyVal = jQuery("#NotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }
</script>
<?php echo $this->Html->css('reports'); ?>
<script type="text/javascript">
function AddRow(divid){
    var counter = jQuery('#optioncount').val();
    // var type= new array('2.5 lb Dry Chemical'=>'2.5 lb Dry Chemical','5 lb Dry Chemical'=>'5 lb Dry Chemical','10 lb Dry Chemical'=>'10 lb Dry Chemical','20 lb Dry Chemical'=>'20 lb Dry Chemical','5 lb CO2'=>'5 lb CO2','10 lb CO2'=>'10 lb CO2','15 lb CO2'=>'15 lb CO2','20 lb CO2'=>'20 lb CO2','5 lb FE36 Clean Agent'=>'5 lb FE36 Clean Agent','10 lb FE36 Clean Agent'=>'10 lb FE36 Clean Agent','13 lb FE36 Clean Agent'=>'13 lb FE36 Clean Agent','2.5 Gal Pressurized Water'=>'2.5 Gal Pressurized Water','K Class'=>'K Class');
    //alert(type);
    counter = parseInt(counter) + 1;
    jQuery('#optioncount').val(counter);
    var OptionHtml = '<tr id="option_'+counter+'">';
    OptionHtml = OptionHtml+'<td width="2%">'+counter+'</td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][location]" class="validate[required]" id="" ></td>';
    OptionHtml = OptionHtml+'<td><select id="status_'+counter+'" name="data[ExtinguisherReportRecord]['+counter+'][ext_type]" class="validate[required]" style="width:80px"><option value="" selected="selected">--Select-</option><option value="2.5 lb Dry Chemical" >2.5 lb Dry Chemical</option><option value="5 lb Dry Chemical">5 lb Dry Chemical</option>';
    OptionHtml = OptionHtml+'<option value="10 lb Dry Chemical">10 lb Dry Chemical</option>';    
    OptionHtml = OptionHtml+'<option value="20 lb Dry Chemical">20 lb Dry Chemical</option>';
    OptionHtml = OptionHtml+'<option value="5 lb CO2">5 lb CO2</option>';
    OptionHtml = OptionHtml+'<option value="10 lb CO2">10 lb CO2</option>';
    OptionHtml = OptionHtml+'<option value="15 lb CO2">15 lb CO2</option>';    
    OptionHtml = OptionHtml+'<option value="20 lb CO2">20 lb CO2</option>';
    OptionHtml = OptionHtml+'<option value="5 lb FE36 Clean Agent">5 lb FE36 Clean Agent</option>';
    OptionHtml = OptionHtml+'<option value="10 lb FE36 Clean Agent">10 lb FE36 Clean Agent</option>';
    OptionHtml = OptionHtml+'<option value="13 lb FE36 Clean Agent">13 lb FE36 Clean Agent</option>';
    OptionHtml = OptionHtml+'<option value="2.5 Gal Pressurized Water">2.5 Gal Pressurized Water</option>';
    OptionHtml = OptionHtml+'<option value="K Class">K Class</option>';
    OptionHtml = OptionHtml+'</select></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][model]"  id="" class="validate[required]" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][born_date]"  id="" class="calender validate[required]" style="width:42px;margin:0;" readonly = "readonly"></td>';
    OptionHtml = OptionHtml+'<td><select id="status_'+counter+'" name="data[ExtinguisherReportRecord]['+counter+'][deficiency]" style="width:60px"><option value="No" selected="selected">No</option><option value="Yes">Yes</option></select></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][deficiency_reason]"  id="" style="" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_maintenace_date]"  id="" class="calender" style="width:50px;margin:0;" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_maintenace_performed_by]"  id="" style="" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_recharge_date]"  id="" class="calender" style="width:50px;margin:0;" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_recharge_performed_by]"  id="" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_hydrotest_date]"  id="" class="calender" style="width:50px;margin:0;" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_hydrotest_performed_by]"  id="" style="" ></td>';
    OptionHtml = OptionHtml+'<td><a href="javascript:void(0)" onclick="RemoveRow('+counter+')"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></td>';    
    OptionHtml = OptionHtml+'</tr>';
    jQuery('#DivOption').append(OptionHtml);
     loadDatePicker();
}
function RemoveRow(divid){
    jQuery('#option_'+divid).remove();
}
 
function RemoveRecord(rId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delrecord/" + rId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#tr_'+rId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmbox(rId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveRecord(rId);    
  }
  else{
    return false;
  }
 }
function RemoveAttachment(attachId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmattach(attachId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId);    
  }
  else{
    return false;
  }
 }
function RemoveNotifier(notifierId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delnotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#tr_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId);    
  }
  else{
    return false;
  }
 }  
</script>
<script type="text/javascript" >
jQuery.noConflict();
jQuery(function() {
	// setup ul.tabs to work as tabs for each div directly under div.panes
	//jQuery("ul.tabs").tabs("div.panes > div");	
});
</script>
<style>
.lbtxt{text-align:right;width:20%}
.input-chngs1 input {
    margin-right: 3px;
    width: 180px;
}
.input-chngs2 input {
	margin:0px;
	color: #666666;
	font-size: 12px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
</style>

<!--<ul class="tabs">
	<li><a href="#">Building Info</a></li>
	<li><a href="#">ExtinguisherReports</a></li>
	<li><a href="#">Report</a></li> 
</ul>-->
 <?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'extinguishers','name'=>'extinguisher','id'=>'extinguisher')); ?>
 <?php echo $form->input('ExtinguisherReport.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
 <?php echo $form->input('ExtinguisherReport.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
 <?php echo $form->input('ExtinguisherReport.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
 <?php echo $form->input('ExtinguisherReport.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
 <?php echo $form->input('ExtinguisherReport.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
 <?php echo $form->input('ExtinguisherReport.id',array('type'=>'hidden','value'=>@$result['id'])); ?> 
<div id="">
	      <div class='message'><?php echo $this->Session->flash();?></div>
	      <div id="deleteFlashmessage" style="display:none;">
	      <div class="notification msginfo">
		     <a class="close"></a>
		     <p style="text-align:center;color:red">Record has been deleted successfully</p>
	      </div>
	      </div>
              <div id="">
<div class="panes">
<div class="headingtxt">Extinguisher Report</div>	
<!-- Building Info tab Start-->
	<div>
   <div style="float:right;padding-right:10px;"><?php echo $html->link('Add Attachment','#inline_content_attachment',array('class' => 'inline'));?><?php echo $html->image('icon_attachment.gif');?>&nbsp;&nbsp;&nbsp;<?php echo $html->link('Add Notifier',"#inline_content_notification",array('class' => 'inline'));?><?php echo $html->image('icon_notify.gif');?></div>
    <h3 class="main-heading" align="center" ><?php echo ucwords($spResult['User']['fname'].' '.$spResult['User']['lname']);?></h3>
    <?php echo $this->Form->hidden('ExtinguisherReport.optioncount',array('value'=>'1','id'=>'optioncount')); ?>
    <table class="tbl input-chngs1" >
      
      <tr><td colspan="4"><h3 class="" align="center" >&nbsp;</h3></td></tr>
      
   
    <?php echo $this->element('reports/extinguisher_form'); ?>
    <tr>      
      <td>
		
	    
		<?php echo $html->link('<span>REVIEW</span>','javascript:void(0);',array('escape'=>false,'id'=>'reviewanswer','class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'extinguisherView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');")); ?>
		<span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" name="save" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></td>
    </tr>
  </table> 
  </div>
<!-- ExtinguisherReport Info tab end-->
</div>
              </div>
    <!--box end -->
</div>
<?php echo $form->end(); ?>
    <!--content end -->
    <!-- This contains the hidden content for inline calls -->
	<div style='display:none'>		
		<div id='inline_content_attachment' style='padding:10px; background:#fff;'>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'attachment','name'=>'attachment','id'=>'attachment',"onsubmit"=>"return checkUploadedAttachment();")); ?>
		<?php echo $form->input('Attachment.attach_file', array('id'=>'uploadattachment','type'=>'hidden','value'=>'')); ?>
		<?php echo $form->input('Attachment.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
		<?php echo $form->input('Attachment.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Attachment.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Attachment.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Attachment.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		<p><strong>Upload Attachment</strong></p>
		<p>
		    <div id="demo"></div>
		    <ul id="separate-list"></ul>
		</p>
		<p><div id="uploaded_picture" style="text-align:center;padding-left:90px;"></div></p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($attachmentdata)){ ?>
			<p><strong>Attachments</strong></p>
		<?php } ?>
		<?php foreach($attachmentdata as $attachdata){ ?>
			<p id="p_<?php echo $attachdata['Attachment']['id'] ?>"><?php echo $attachdata['Attachment']['attach_file'].'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmattach('<?php echo $attachdata['Attachment']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>
		<p>

		</p>
		</div>
		<?php echo $form->end(); ?>
	</div>
<!-- This contains the hidden content for inline calls -->
	<div style='display:none'>		
		<div id='inline_content_notification' style='padding:10px; background:#fff;'>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'notification','name'=>'notification','id'=>'notification',"onsubmit"=>"return checkNotification();")); ?>
		<?php echo $form->input('Notification.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
		<?php echo $form->input('Notification.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Notification.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Notification.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Notification.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		<p><strong>Notification</strong></p>
		<p>
		    <?php echo $form->input('Notification.notification',array('type'=>'textarea','rows'=>5,'cols'=>40,'label'=>false,'id'=>'ContentMessage','onKeyUp' =>"return limit('ContentMessage',500);")); ?>
		</p>
		<p>
                     <div id="essay_Error" class='error'></div>
		    <div>
		    <small>&nbsp;<label id='limitCounter'>0</label>&nbsp;characters entered&nbsp;|&nbsp;<label id='limitCounterLeft'>500</label>&nbsp;characters remaining</small>
		    </div>   
                </p>
		
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($notifierdata)){ ?>
			<p><strong>Notifiers</strong></p>
		<?php } ?>				
		<?php foreach($notifierdata as $notifydata){ ?>
			<p id="tr_<?php echo $notifydata['Notification']['id'] ?>"><?php echo $notifydata['Notification']['notification'].'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmnotifier('<?php echo $notifydata['Notification']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>
		
		</div>
		<?php echo $form->end(); ?>
	</div>