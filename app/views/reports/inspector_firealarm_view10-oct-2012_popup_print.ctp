<?php
	echo $this->Html->css('style_tablesort');
	
	echo $this->Html->css('css-rep/reset');
	echo $this->Html->css(array('css-rep/print'), 'stylesheet', array('media' => 'print'));
	echo $this->Html->css('css-rep/page-print');
	echo $this->Html->css('css-rep/styles');
	echo $this->Html->css('css-rep/pstyles');
	
	echo $this->Html->script('js-rep/jquery-1.3.2');
	echo $this->Html->script('js-rep/jquery.quickflip');
	echo $this->Html->script('js-rep/page-print');
?>
<script type="text/javascript" >
	$('document').ready(function(){	
		setCss();
		$('#flip-container').quickFlip();		
		$('#flip-navigation li a').each(function(){
			$(this).click(function(){
				$('#flip-navigation li').each(function(){
					$(this).removeClass('selected');
				});
				$(this).parent().addClass('selected');
				var flipid=$(this).attr('id').substr(4);			
				//alert(flipid);				
				$('#flip-container').quickFlipper({ }, flipid, 1);			
				return false;
			});
		});
		$('.close').live('click',function(){
			location.reload(); 
		});
		$('.noprint').click(function(){	
});
});
function setCss(){
		$('#setContent').css('height',$(window).height());
		$('#setContent').css('width',$(window).width());
		//$('.canvas').css('width','52%');
	}
</script> 
<div id="mainbody" class="canvas">	
	<ul id="flip-navigation">
				<li class="selected"><a href="#" id="tab-0">Recent</a></li>
				<li><a href="#" id="tab-1" >Popular</a></li>
				<li><a href="#" id="tab-2" >Comments</a></li>
				<li><a href="#" id="tab-3" >Comments</a></li>
				<li><a href="#" id="tab-4"  >Recent</a></li>
				<li><a href="#" id="tab-5" >Popular</a></li>
				<li><a href="#" id="tab-6" >Summary</a></li>
				<li><a href="#" id="tab-7" >Calls</a></li>
				<li><a href="#" id="tab-8" >Data</a></li>		
	</ul>
		
		<div id="flip-container">
			<div>
			

<img src="/home/rahuld/Desktop/map.jpg"/>

	
			A Time-Saving Tip for Creating Print Styles

To save time while you're creating the print-only styles, you may want to choose to add the @media print { ... } declaration as the last step, instead previewing the CSS along the way directly on the screen. This avoids having to launch the print preview to try out every change. Once you've perfected the CSS, you can add the declaration and confirm that everything appears as expected in the print preview.
Helpful Print-Related CSS
Hiding Content

The most common CSS action to perform when printing is to hide irrelevant page elements. As shown in the example above, the display property is used to hide content:
When certain elements are hidden, note that you may need to adjust margins on other page sections to avoid empty space.
Tweaking the Layout

When you're printing, the primary content should take absolute center-stage. Because of the limited available width and need for readable text sizing, the paper environment is rather similar to the tablet viewing environment. This means that content should almost always be arranged into separate, vertically-stacked sections rather than horizontally-adjacent columns. In addition to hiding various elements, creating this type of layout often requires removing element floats:

				
			</div>

			<div>
				<ul>
					<li>1Phasellus ut erat sapien. Suspendisse ultrices vestibulum</li>
					<li>Donec nec erat neque. Curabitur orci risus,</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
					<li>Cras congue venenatis interdum</li>
					<li>Nunc quis tellus at eros aliquam hendrerit nec sed mauris</li>	
					<img src="/home/rahuld/Desktop/map.jpg"/>				
				</ul>
			</div>

			<div>
				<ul>
					<li>2Duis laoreet elementum imperdiet. Fusce iaculis,</li>
					<li>erat rutrum mollis sodales, quam arcu vehicula ipsum,</li>
					<li>Curabitur porttitor nisl at mi sagittis eleifend.</li>
					<li>Nam tempus pretium nisl quis pharetra. Maecenas</li>
					<li>at feugiat nibh sem ut urna.  Curabitur turpis nisl.</li>					
				</ul>
			</div>

			<div>
				<ul>
					<li>311 laoreet elementum imperdiet. Fusce iaculis,</li>
					<li>erat rutrum mollis sodales, quam arcu vehicula ipsum,</li>
					<li>Curabitur porttitor nisl at mi sagittis eleifend.</li>
					<li>Nam tempus pretium nisl quis pharetra. Maecenas</li>
					<li>at feugiat nibh sem ut urna.  Curabitur turpis nisl.</li>					
				</ul>
			</div>

			<div>
				<ul>
					<li>411 laoreet elementum imperdiet. Fusce iaculis,</li>
					<li>erat rutrum mollis sodales, quam arcu vehicula ipsum,</li>
					<li>Curabitur porttitor nisl at mi sagittis eleifend.</li>
					<li>Nam tempus pretium nisl quis pharetra. Maecenas</li>
					<li>at feugiat nibh sem ut urna.  Curabitur turpis nisl.</li>					
				</ul>
			</div>

			<div>
				<ul>
					<li>511 laoreet elementum imperdiet. Fusce iaculis,</li>
					<li>erat rutrum mollis sodales, quam arcu vehicula ipsum,</li>
					<li>Curabitur porttitor nisl at mi sagittis eleifend.</li>
					<li>Nam tempus pretium nisl quis pharetra. Maecenas</li>
					<li>at feugiat nibh sem ut urna.  Curabitur turpis nisl.</li>					
				</ul>
			</div>

			<div>
				<ul>
					<li>611 laoreet elementum imperdiet. Fusce iaculis,</li>
					<li>erat rutrum mollis sodales, quam arcu vehicula ipsum,</li>
					<li>Curabitur porttitor nisl at mi sagittis eleifend.</li>
					<li>Nam tempus pretium nisl quis pharetra. Maecenas</li>
					<li>at feugiat nibh sem ut urna.  Curabitur turpis nisl.</li>					
				</ul>
			</div>

			<div>
				<ul>
					<li>711 laoreet elementum imperdiet. Fusce iaculis,</li>
					<li>erat rutrum mollis sodales, quam arcu vehicula ipsum,</li>
					<li>Curabitur porttitor nisl at mi sagittis eleifend.</li>
					<li>Nam tempus pretium nisl quis pharetra. Maecenas</li>
					<li>at feugiat nibh sem ut urna.  Curabitur turpis nisl.</li>					
				</ul>
			</div>

			<div>
				<ul>
					<li>811 laoreet elementum imperdiet. Fusce iaculis,</li>
					<li>erat rutrum mollis sodales, quam arcu vehicula ipsum,</li>
					<li>Curabitur porttitor nisl at mi sagittis eleifend.</li>
					<li>Nam tempus pretium nisl quis pharetra. Maecenas</li>
					<li>at feugiat nibh sem ut urna.  Curabitur turpis nisl.</li>					
				</ul>
			</div>

			<div>
				<ul>
					<li>11 laoreet elementum imperdiet. Fusce iaculis,</li>
					<li>erat rutrum mollis sodales, quam arcu vehicula ipsum,</li>
					<li>Curabitur porttitor nisl at mi sagittis eleifend.</li>
					<li>Nam tempus pretium nisl quis pharetra. Maecenas</li>
					<li>at feugiat nibh sem ut urna.  Curabitur turpis nisl.</li>					
				</ul>
			</div>
		</div>
	</div>
		
<div id="setContent" style="font-family:Arial, sans-serif; font-size:12px; color:#666; line-height:30px;">
&nbsp;
</div>


<script type="text/javascript">
$('<a href="#">Print this page</a>').prependTo('.canvas').css({position: 'absolute', top: 20, right: 75,}).addClass('noprint').pagePrint();
</script>
