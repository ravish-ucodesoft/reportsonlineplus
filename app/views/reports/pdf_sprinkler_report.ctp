<?php

App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();


$html = '
<table border="0" cellspacing="0" cellpadding="2">
              	<tr>
              	   <td colspan="2" align="center"><strong>Sprinkler Inspection Report</strong> </td>
              	</tr>
                <tr>
                <tr><td colspan="2">
        <table width="100%">
          <tr>
            <td align="center"><img src="/app/webroot/img/company_logo/'.$spResult['Company']['company_logo'].'"></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b>'.$spResult['Company']['name'].'</b></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:21px">';		
		$arr=explode(',',$spResult['Company']['interest_service']);
	    
		    foreach($arr as $key=>$val):
		    foreach($reportType as $key1=>$val1):

		    if($val== $key1)
		    {
			
			 if($key1 == 9){
                            $html.=  $spResult['Company']['interest_service_other'];
                            }else{
                                $html.= $val1;
                                }
                       $html.=' <span class="red"> * </span>';
		     }
		    
		    endforeach;
		    
		    endforeach;	

		 $html.=   '</td></tr>
            
            
          
          
          
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:24px">Sprinkler Inspection Report</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>          
          <tr>
            <td align="right" class="Cpage" >Date:';if($record['SprinklerReport']['finish_date'] != ''){
                $html.= date('m/d/Y',strtotime($record['SprinklerReport']['finish_date'])); } $html.='</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" ><i>Prepared for:</i></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_name'].'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'; if(!empty($clientResult['User']['site_country_id'])){
                                        $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                        if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                        $html.= $clientResult['User']['site_city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_phone'].'</td>
          </tr>   
          <tr>
            <td align="left" class="Cpage" >Prepared By:</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
          <tr>
            <td align="left" class="Cpage">'.$spResult['Company']['name'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['address'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'; if(!empty($spResult['Country']['name'])){
                                        $html.= $spResult['Country']['name'].', '; }
                                        if(!empty($spResult['State']['name'])) { $html.= $spResult['State']['name'].', '; } 
                                        $html.= $clientResult['User']['city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['phone'].'</td>
          </tr> 
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
         <tr>
            <td align="center"><h2>'. ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']) .'</h2></td>
          </tr>
          
        </table>
        </td>
      </tr>
      </table>
      
      
      <table cellpadding="0" cellspacing="0" border="0" class="table_new">
	  <tr>
	     <td colspan="2" align="right">
		<table cellspacing="0" cellpadding="0" border="0">
		   <tr>
		      <td width="32%" align="right"> Monthly</td>			
		   <tr>
		</table>
	     </td>
	  </tr>
	  <tr><th colspan="2" align="left">Client information</th></tr>
	  <tr>
	    <td valign="top">
		   <table width="100%" cellpadding="0" cellspacing="0">
		   <tr>
		      <td class="table_label"><strong>Building Information</strong></td>
		      <td>'.$clientResult['User']['client_company_name'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Address</strong></td>
		      <td>'.$clientResult['User']['address'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>City/State/Zip:</strong></td>
		      <td> '.$clientResult['User']['city'].', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Country: </strong></td>
		      <td>'.$clientResult['Country']['name'].'</td>
		    </tr>
		   </table>
	    </td>
	    <td valign="top">
		<table width="100%" cellpadding="0" cellspacing="0">
		   <tr>
		      <td class="table_label"><strong>Contact: </strong></td>
		      <td>'.$clientResult['User']['fname'].' '.$clientResult['User']['lname'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Phone:</strong></td>
		      <td>'.$clientResult['User']['phone'].'</td>
		    </tr>                               
		    <tr>
		      <td class="table_label"><strong>Email: </strong></td>
		      <td> '.$clientResult['User']['email'].'</td>
		    </tr>
		   </table>
	    </td>
	  </tr>
	  
	  <tr><th colspan="2" align="left">Site Information</th></tr>
	  <tr>
	    <td valign="top">
		   <table width="100%" cellpadding="0" cellspacing="0">
		   <tr>
		      <td class="table_label"><strong>Site Name:</strong></td>
		      <td>'.$siteaddrResult['SpSiteAddress']['site_name'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Address</strong></td>
		      <td>'.$siteaddrResult['SpSiteAddress']['site_address'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>City/State/Zip:</strong></td>
		      <td> '.$siteaddrResult['SpSiteAddress']['site_city'].', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Country: </strong></td>
		      <td>'.$siteaddrResult['Country']['name'].'</td>
		    </tr>
		   </table>
	    </td>
	    <td valign="top">
		<table width="100%" cellpadding="0" cellspacing="0">
		   <tr>
		      <td class="table_label"><strong>Contact: </strong></td>
		      <td> '.$siteaddrResult['SpSiteAddress']['site_contact_name'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Phone:</strong></td>
		      <td>'.$siteaddrResult['SpSiteAddress']['site_phone'].'</td>
		    </tr>                               
		    <tr>
		      <td class="table_label"><strong>Email: </strong></td>
		      <td>'.$siteaddrResult['SpSiteAddress']['site_email'].'</td>
		    </tr>
		   </table>
	    </td>
	  </tr>
	   <tr><th colspan="2" align="left">Service Provider Info</th></tr>
	  <tr>
	    <td valign="top">
		   <table width="100%" cellpadding="0" cellspacing="0">
		   <tr>
		      <td class="table_label"><strong>Name:</strong></td>
		      <td>'.$spResult['User']['fname'].' '.$spResult['User']['lname'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Address</strong></td>
		      <td>'.$spResult['User']['address'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>City/State/Zip:</strong></td>
		      <td>'.$spResult['User']['city'].', '.$spResult['State']['name'].' '.$spResult['User']['zip'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Country: </strong></td>
		      <td>'.$spResult['Country']['name'].'</td>
		    </tr>
		   </table>
	    </td>
	    <td valign="top">
		<table width="100%" cellpadding="0" cellspacing="0">
		   <tr>
		      <td class="table_label"><strong>Contact: </strong></td>
		      <td> '.$spResult['User']['fname'].' '.$spResult['User']['lname'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Phone:</strong></td>
		      <td>'.$spResult['User']['phone'].'</td>
		    </tr>                               
		    <tr>
		      <td class="table_label"><strong>Email: </strong></td>
		      <td>'.$spResult['User']['email'].'</td>
		    </tr>
		   </table>
	    </td>
	  </tr>
       </table>
      
      
      <table cellpadding="0" cellspacing="0" border="0" class="table_new">			    			    
	  <tr>
	    <td valign="top">
		   <table width="100%" cellpadding="0" cellspacing="0">
		   <tr>
		      <td class="table_label"><strong>Date of Work</strong></td>
		      <td>'.$record['SprinklerReport']['date_of_work'].'</td>
		    </tr>				      
		    <tr>
		      <td class="table_label"><strong>Start Date of the Service</strong></td>
		      <td>'.$time->Format('m-d-Y',$record['SprinklerReport']['start_date_service']).'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>End Date of the Service</strong></td>
		      <td> '.$time->Format('m-d-Y',$record['SprinklerReport']['end_date_service']).'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Date of Performed Work </strong></td>
		      <td>'.$record['SprinklerReport']['date_performed_work'].'</td>
		    </tr>
		   </table>
	    </td>
	    <td valign="top">
		<table width="100%" cellpadding="0" cellspacing="0">
		   <tr>
		      <td class="table_label"><strong>Deficiency yes or no </strong></td>
		      <td>'.$record['SprinklerReport']['deficiency'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Recommendations yes or no</strong></td>
		      <td>'.$record['SprinklerReport']['recommendations'].'</td>
		    </tr>
		    <tr>
		      <td class="table_label"><strong>Inspection 100% complete and Passed</strong></td>
		      <td>'.$record['SprinklerReport']['inspection'].'</td>
		    </tr>				      
		   </table>
	    </td>
	  </tr>    
      </table>
      
              <table cellpadding="2">
                <tr>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2"><b><h3>1. GENERAL(To be answered by Customer.)</h3></b></td>
                </tr>
      				  <tr>
      				    <td width="90%">a. Have there been any changes in the occupancy classification, machinery or operations since the last inspection?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_a'].'</td>
                </tr>
                <tr>
      				    <td>b. Have there been any changes or repairs to the fire protection systems since the last inspection?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_b'].'</td>
                </tr>
                <tr>
      				    <td>c. If a fire has occurred since the last inspection, have all damaged sprinkler system components been replaced?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_c'].'</td>
                </tr>
                <tr>
      				    <td colspan="2">If answered "yes" to a, b or c, list changes in Section 13</td>
                </tr>
                <tr>
      				    <td>d. Has the piping in all dry systems been checked for proper pitch within the past five years?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_d'].'</td>
                </tr>
                <tr>
      				    <td>Date last checked:&nbsp;&nbsp;'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_d_date']).'&nbsp;&nbsp;(check recommended at least every 5 years)</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_d_1'].'</td>
                </tr>
                <tr>
      				    <td>e. Has the piping in all systems been checked for obstructive materials?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_e'].'</td>
                </tr>
                <tr>
      				    <td>Date last checked:&nbsp;&nbsp;'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_e_date']).'&nbsp;&nbsp;(check recommended at least every 5 years)</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_e_1'].'</td>
                </tr>
                </table>
              <table cellpadding="2">
                <tr>
      				    <td width="80%">f. Have all fire pumps been tested to full capacity using hose streams or flow meters within the past 12 months?</td>
      				    <td width="10%">'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_f_date']).'</td>
      				    <td width="10%">'.$record['SprinklerReportGeneral']['general_f'].'</td>
                </tr>
                <tr>
      				    <td colspan="2">g. Are gravity, surface or pressure tanks protected from freezing?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_g'].'</td>
                </tr>
                <tr>
      				    <td colspan="2">h. Are sprinklers newer than 50 years old? QR (20yr) Dry (10 yr) >325F/163C (5yr) Corrosive env'."'".'t. (5yr.)</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_h'].'</td>
                </tr>            
                <tr>
      				    <td colspan="3">(Testing or replacement required for sprinklers past these age limits.)</td>
                </tr>
                <tr>
                  <td colspan="2">i. Are extra high temperature solder sprinklers free from regular exposure to temperatures near 300F/149C?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_i'].'</td>
                </tr>
                <tr>
      				    <td>j. Have gauges been tested, calibrated or replaced in the last 5 years?</td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_j_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_j'].'</td>
                </tr>
                <tr>
      				    <td>k. Alarm valves and associated trim been internally inspected past 5 years?</td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_k_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_k'].'</td>
                </tr>
                <tr>
      				    <td>l. Check valves internally inspected in the last 5 years?</td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_l_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_l'].'</td>
                </tr>
                <tr>
      				    <td>m. Has the private fire main been flow tested in last 5 years? </td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_m_date']).'</td>
      				    <td>'. $record['SprinklerReportGeneral']['general_m'].'</td>
                </tr>
                <tr>
      				    <td>n. Standpipe 5 and 3 year requirements.</td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_n_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_n'].'</td>
                </tr>
                <tr>
      				    <td>1. Dry standpipe hydrostatic test  </td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_n_1_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_n_1'].'</td>
                </tr>
                <tr>
      				    <td>2. Flow test </td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_n_2_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_n_2'].'</td>
                </tr>            
                <tr>
      				    <td>3. Hose hydrostatic test (5 years from new, every 3 years after)</td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_n_3_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_n_3'].'</td>
                </tr>
                <tr>
      				    <td>4. Pressure reducing/control valve test </td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_n_4_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_n_4'].'</td>
                </tr>
                <tr>
      				    <td>o. Have pressure reducing/control valves been tested at full flow within the past 5 years? </td>
      				    <td>'.$time->Format('m/d/y',$record['SprinklerReportGeneral']['general_o_date']).'</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_o'].'</td>
                </tr>
                <tr>
      				    <td colspan="2">p. Have master pressure reducing/control valves been tested at full flow within the past 1 year?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_p'].'</td>
                </tr>
                <tr>
      				    <td colspan="2">q. Have the sprinkler systems been extended to all areas of the building?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_q'].'</td>
                </tr>
                <tr>
      				    <td colspan="2">r. Are the building areas protected by a wet system heated, including its blind attics and perimeter areas?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_r'].'</td>
                </tr>
                <tr>
      				    <td colspan="2">s. Are all exterior openings protected against the entrance of cold air?</td>
      				    <td>'.$record['SprinklerReportGeneral']['general_s'].'</td>
                </tr>            			    
    				  </table>              
              <table cellpadding="2">
                <tr>
      				    <td colspan="3"><h3>&nbsp;</h3></td>
                </tr>
                <tr>
      				    <td colspan="3"><h3>2. CONTROL VALVES</h3></td>
                </tr>
                <tr>
      				    <td colspan="3"><h3>&nbsp;</h3></td>
                </tr>                
    				    <tr>
      				    <td colspan="2" width="90%">a. Are all sprinkler system main control valves and all other valves in the appropriate open or closed position?</td>
      				    <td>'.$record['SprinklerReportControlvalve']['cvalve_a'].'</td>
                </tr>
                <tr>
      				    <td colspan="2">b. Are all control valves sealed or supervised in the appropriate position?</td>
      				    <td>'.$record['SprinklerReportControlvalve']['cvalve_b'].'</td>
                </tr>
                <tr>
      				    <td colspan="3"><h3>&nbsp;</h3></td>
                </tr>
    				  </table>
              <table cellpadding="2">			    
                <tr>
                  <td  width="15%">Control Valves</td>
                  <td  width="10%"># of Valves</td>
                  <td  width="10%">Type</td>
                  <td  width="10%">Easily Accessible</td>
                  <td  width="10%">Signs</td>
                  <td  width="10%">Valve Open</td>
                  <td  width="10%">Secured ? IF YES, HOW? </td>
                  <td  width="15%"> (Sealed?) (Locked?) (Supvd.?)</td>
                  <td  width="10%">Supervision Operational</td>
                </tr>
                <tr>
                  <td>CITY CONNECTION</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_valves'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_type'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_access'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_signs'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_vopen'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_secured'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_option'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_cconnection_supervision'].'</td>
                </tr>
                <tr>
                  <td>TANK</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_valves'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_type'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_access'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_signs'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_vopen'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_secured'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_option'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_tank_supervision'].'</td>
                </tr>
                <tr>
                  <td>PUMP</td>              
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_valves'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_type'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_access'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_signs'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_vopen'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_secured'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_option'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_pump_supervision'].'</td>
                </tr>
                <tr>
                  <td>SECTIONAL</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_valves'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_type'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_access'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_signs'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_vopen'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_secured'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_option'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_sectional_supervision'].'</td>
                </tr>
                <tr>
                  <td>SYSTEM</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_system_valves'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_system_type'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_system_access'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_system_signs'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_system_vopen'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_system_secured'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_system_option'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_system_supervision'].'</td>
                </tr>
                
                <tr>
                  <td>ALARM LINE</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_valves'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_type'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_access'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_signs'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_vopen'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_secured'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_option'].'</td>
                  <td>'.$record['SprinklerReportControlvalve']['cvalve_alarm_supervision'].'</td>
                </tr>
              </table>  
              <table cellpadding="2">
              <tr>
                  <td  colspan="5">&nbsp;</td>
                </tr>
                <tr>
                  <td  colspan="5">3. WATER SUPPLIES</td>
                </tr>
                <tr>
                  <td  colspan="5">&nbsp;</td>
                </tr>		    
                <tr>
                  <td colspan="2"> a. Water supply sources? City: &nbsp;&nbsp;'.$record['SprinklerReportWatersupply']['wsupply_city'].'</td>
                  <td colspan="2">Gravity Tank:&nbsp;&nbsp;'.$record['SprinklerReportWatersupply']['wsupply_tank'].'</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td  colspan="5">Water Flow Test Results Made During This Inspection</td>
                </tr>				
                <tr>
                  <td>Test Pipe Located</td>
                  <td>Size Test Pipe</td>
                  <td>Static Pressure Before</td>
                  <td>Flow Pressure</td>
                  <td>Static Pressure After</td>
                </tr>
                <tr>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_1'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_1'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_1'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_1'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_1'].'</td>
                </tr>
                <tr>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_2'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_2'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_2'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_2'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_2'].'</td>
                </tr>
                <tr>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_3'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_3'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_3'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_3'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_3'].'</td>
                </tr>
                <tr>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_4'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_4'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_4'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_4'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_4'].'</td>
                </tr>
                <tr>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_5'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_5'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_5'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_5'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_5'].'</td>
                </tr>
                <tr>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_6'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_6'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_6'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_6'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_6'].'</td>
                </tr>
                <tr>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_7'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_7'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_7'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_7'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_7'].'</td>
                </tr>
                <tr>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_located_8'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_pipe_size_8'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_before_8'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_flow_pressure_8'].'</td>
                  <td>'.$record['SprinklerReportWatersupply']['wsupply_static_pressure_after_8'].'</td>
                </tr>
                </table>
                <table cellpadding="2">
                  <tr>
        				    <td colspan="3">&nbsp;</td>        				    
                  </tr>
                  <tr>
        				    <td colspan="3"><h3>4. TANKS, PUMPS, FIRE DEPT. CONNECTION</h3></td>        				    
                  </tr>
                  <tr>
        				    <td colspan="3">&nbsp;</td>        				    
                  </tr>
      				    <tr>
        				    <td colspan="2" width="90%">a. Do fire pumps, gravity, surface or pressure tanks appear to be in good external conditions?</td>
        				    <td>'.$record['SprinklerReportConnection']['connection_a'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="2">b. Are gravity, surface and pressure tanks at the proper pressure and/or water levels?</td>
        				    <td>'.$record['SprinklerReportConnection']['connection_b'].'</td>
                  </tr>
                  <tr>
        				    <td  width="80%">c. Has the storage tank been internally inspected in the last 3 yrs. (unlined) or 5 yrs. (lined)? </td>
        				    <td  width="10%">'.$time->Format('m/d/y',$record['SprinklerReportConnection']['connection_c_date']).'</td>
        				    <td  width="10%">'.$record['SprinklerReportConnection']['connection_c'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="2">d. Are fire dept. connections in satisfactory condition, couplings free, caps or plugs in place and check valves tight?</td>
        				    <td>'.$record['SprinklerReportConnection']['connection_d'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="2">e. Are fire dept. connections visible and accessible?</td>
        				    <td>'.$record['SprinklerReportConnection']['connection_e'].'</td>
                  </tr>
                </table>
                <table cellpadding="2">
                  <tr>
        				    <td colspan="4">&nbsp;</td>        				    
                  </tr>
      				    <tr>
        				    <td colspan="4"><h3>5. WET SYSTEMS</h3></td>        				    
                  </tr>
                  <tr>
        				    <td colspan="4">&nbsp;</td>        				    
                  </tr>
                  <tr>
        				    <td>a. No. of systems:</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_a'].'</td>
        				    <td>Make & Model : </td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_a_makemodel'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="3">b. Are cold weather valves in the appropriate open or closed position?</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_b'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="3">If closed, has piping been drained ?</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_b_1'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="3">c. Has the Customer been advised that cold weather valves are not recommended?</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_c'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="2">d. Have all the antifreeze systems been tested in the past year?</td>
        				    <td>'.$time->Format('m/d/y',$record['SprinklerReportWetsystem']['wetsystem_d_date']).'</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_d'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="4">The antifreeze tests indicated protection to:</td>
                  </tr>
                  <tr>
        				    <td>system 1 </td>
        				    <td style="text-align:right"><b>Temperature</b></td>
        				    <td style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_1_temp'].'</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_1'].'</td>
                  </tr>
                  <tr>
        				    <td>system 2 </td>
        				    <td style="text-align:right"><b>Temperature</b></td>
        				    <td style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_2_temp'].'</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_2_temp'].'</td>
                  </tr>
                  <tr>
        				    <td>system 3 </td>
        				    <td style="text-align:right"><b>Temperature</b></td>
        				    <td style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_3_temp'].'</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_3_temp'].'</td>
                  </tr>
                  <tr>
        				    <td>system 4 </td>
        				    <td style="text-align:right"><b>Temperature</b></td>
        				    <td style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_4_temp'].'</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_4_temp'].'</td>
                  </tr>
                  <tr>
        				    <td>system 5 </td>
        				    <td style="text-align:right"><b>Temperature</b></td>
        				    <td style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_5_temp'].'</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_5_temp'].'</td>
                  </tr>
                  <tr>
        				    <td>system 6 </td>
        				    <td style="text-align:right"><b>Temperature</b></td>
        				    <td style="text-align:left">'.$record['SprinklerReportWetsystem']['wetsystem_6_temp'].'</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_6_temp'].'</td>
                  </tr>
                  <tr>
        				    <td colspan="3">e. Did alarm valves, water flow alarm devices and retards test satisfactorily?</td>
        				    <td>'.$record['SprinklerReportWetsystem']['wetsystem_e'].'</td>
                  </tr>
                </table>
                <table cellpadding="2">
                  <tr>
                    <td colspan="6">&nbsp;</td>                                
                  </tr>
                  <tr>
                    <td colspan="6"><h3>6. DRY SYSTEM</h3></td>                                
                  </tr>
                  <tr>
                    <td colspan="6"><h3>&nbsp;</h3></td>                                
                  </tr>
                  <tr>
                    <td  width="20%">a. No. of systems:	</td>
                    <td >'.$record['SprinklerReportDrysystem']['drysystem_a'].'</td>
                    <td >Make & Model :	Partial</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_a_makemodel'].'</td>
                    <td>	Full</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_a_full'].'</td>            
                  </tr>
                  <tr>
                    <td> Date last trip tested:</td>
                    <td >'.$record['SprinklerReportDrysystem']['drysystem_a_date'].'</td>
                  </tr>
                  <tr>
                    <td colspan="5">b. Are the air pressure(s) and priming water level(s) normal?</td>
                    <td >'.$record['SprinklerReportDrysystem']['drysystem_b'].'</td>
                  </tr>
                  <tr>
                    <td colspan="5">c. Did the air compressor(s) operate satisfactorily?</td>
                    <td >'.$record['SprinklerReportDrysystem']['drysystem_c'].'</td>
                  </tr>
                  <tr>
                    <td colspan="5">d. Air compressor(s) oil checked?</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_d'].'</td>
                  </tr>
                  <tr>
                    <td colspan="5">Belt(s)?</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_d_belt'].'</td>
                  </tr>
                  <tr>
                    <td colspan="5">e. Were Low Point drains drained during this inspection?</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_e'].'</td>
                  </tr>
                  <tr>
                    <td>No. of Drains:</td>
                    <td colspan="5">'.$record['SprinklerReportDrysystem']['drysystem_e_drains'].'</td>
                  </tr>
                  <tr>
                    <td colspan="6">Locations:</td>
                  </tr>
                  <tr>
                    <td>1.</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_e_locations_1'].'</td>
                    <td>2.</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_e_locations_2'].'</td>
                    <td colspan="2"></td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_e_locations_3'].'</td>
                    <td>4.</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_e_locations_4'].'</td>
                    <td colspan="2"></td>
                  </tr>
                  <tr>
                    <td  colspan="5">f. Did all quick opening devices operate satisfactorily?</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_f'].'</td>
                  </tr>
                  <tr>
                    <td>Make & Model:?</td>
                    <td colspan="6">'.$record['SprinklerReportDrysystem']['drysystem_f_makemodel'].'</td>
                  </tr>
                  <tr>
                    <td  colspan="5">g. Did all the dry valves operate satisfactorily during this inspection?</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_g'].'</td>
                  </tr>
                  <tr>
                    <td  colspan="5">h. Is the dry valve house heated?</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_h'].'</td>
                  </tr>
                  <tr>
                    <td  colspan="5">i. Do the dry valves appear to be protected from freezing?</td>
                    <td>'.$record['SprinklerReportDrysystem']['drysystem_i'].'</td>
                  </tr>
                </table>  
                <table cellpadding="2">
                  <tr>  
                    <td colspan="4">&nbsp;</td>
                  </tr>
                  <tr>  
                    <td colspan="4"><h3>7. SPECIAL SYSTEMS</h3></td>
                  </tr>
                  <tr>  
                    <td colspan="4">&nbsp;</td>
                  </tr>		    
                  <tr>  
                    <td>  a. No. of systems:</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_a'].'</td>
                    <td>Make & Model :</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_a_makemodel'].'</td>
                  </tr>
                  <tr>
                    <td> Type:</td>
                    <td colspan="3">'.$record['SprinklerReportSpecialsystem']['specialsystem_a_type'].'</td>
                  </tr>           
                  <tr>
                    <td colspan="3">b. Were valves tested as required?</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_b'].'</td>
                  </tr>
                  <tr>
                    <td colspan="3">c. Did all heat responsive systems operate satisfactorily?</td>
                    <td>'. $record['SprinklerReportSpecialsystem']['specialsystem_c'].'</td>
                  </tr>
                  <tr>
                    <td colspan="3">d. Did the supervisory features operate during testing?</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_d'].'</td>
                  </tr>          
                  <tr>
                    <td>Heat Responsive Devices:</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_d_heat'].'</td>
                    <td>Type	</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_d_type'].'</td>
                  </tr>
                  <tr>
                    <td>Type of test</td>
                    <td colspan="3">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_type_test'].'</td>
                  </tr>
                  <tr>
                    <td>Valve No.</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_d_valve_1'].'</td>
                    <td colspan="2">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_valve_1'].'</td>              
                  </tr>
                  <tr>
                    <td>Seconds</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_d_second_1'].'</td>
                    <td colspan="2">'.$record['SprinklerReportSpecialsystem']['specialsystem_d_second_2'].'</td>
                  </tr>
                  <tr>
                    <td colspan="4"> e. Has a supplemental test form for this system been completed and provided to the customer?     (Please attach)</td>
                  </tr>          
                  <tr>
                    <td colspan="4" >  Auxiliary equipment: </td>
                  </tr>
                  <tr>
                    <td>No.</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_e_number'].'</td>
                    <td>Type</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_e_type'].'</td>
                  </tr>
                  <tr>
                    <td>Location</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_e_location'].'</td>
                    <td>Test results</td>
                    <td>'.$record['SprinklerReportSpecialsystem']['specialsystem_e_test_result'].'</td>
                  </tr> 
                </table>
                <table cellpadding="2">
                    <tr>
        				    <td colspan="2">&nbsp;</td>        				    				    
                    </tr>
                    <tr>
        				    <td colspan="2">8. ALARMS</td>        				    				    
                    </tr>
                    <tr>
        				    <td colspan="2">&nbsp;</td>        				    				    
                    </tr>
        				    <tr>
        				    <td width="90%">a. Did the water motor(s) and gong(s) operate during testing?</td>
        				    <td>'.$record['SprinklerReportAlarm']['alarm_a'].' </td>				    
                    </tr>
                    <tr>
        				    <td>b. Did the electric alarm(s) operate during testing? </td>
        				    <td>'.$record['SprinklerReportAlarm']['alarm_b'].' </td>
                    </tr>
                    <tr>
        				    <td>c. Did the supervisory alarm(s) operate during testing?</td>
        				    <td>'.$record['SprinklerReportAlarm']['alarm_c'].' </td>				    
                    </tr>
                  </table>
                  <table cellpadding="2">
                    <tr>
        				    <td colspan="2">&nbsp;</td>
        				    </tr>
        				    <tr>
        				    <td colspan="2">9. SPRINKLERS - PIPING</td>
        				    </tr>
        				    <tr>
        				    <td colspan="2">&nbsp;</td>
        				    </tr>                    
        				    <tr>
        				    <td  width="90%">a. Do sprinklers generally appear to be in good external condition?</td>
        				    <td>'.$record['SprinklerReportPiping']['piping_a'].' </td>				    
                    </tr>
                    <tr>
        				    <td>b. Do sprinklers generally appear to be free of corrosion, paint, or loading and visible obstructions? </td>
        				    <td>'. $record['SprinklerReportPiping']['piping_b'].' </td>
                    </tr>
                    <tr>
        				    <td>c. Are extra sprinklers and sprinkler wrench available on the premises? </td>
        				    <td>'. $record['SprinklerReportPiping']['piping_c'].' </td>				    
                    </tr>
                    <tr>
        				    <td>d. Does the exposed exterior condition of piping, drain valves, check valves, hangers, pressure gauges, open sprinklers and strainers appear to be satisfactory? </td>
        				    <td>'. $record['SprinklerReportPiping']['piping_d'].' </td>				    
                    </tr>
                    <tr>
        				    <td>e. Does the hand hose on the sprinkler system appear to be in satisfactory condition? </td>
        				    <td>'. $record['SprinklerReportPiping']['piping_e'].' </td>				    
                    </tr>
                    <tr>
        				    <td>f. Does there appear to be proper clearance between the top of all storage and the sprinkler deflector?</td>
        				    <td>'. $record['SprinklerReportPiping']['piping_f'].' </td>				    
                    </tr>
                  </table>
                  <table>
                  	<tr>
                  	    <td colspan="6">&nbsp;</td>	    
                  	</tr>
                  	<tr>
                  	    <td colspan="6">&nbsp;</td>	    
                  	</tr>
                    <tr>
                  	    <td colspan="6"><u>Attachments</u>:</td>	    
                  	</tr>	'; 
                    foreach($attachmentdata as $attachdata){
                   	$html .='<tr>
                  	    <td colspan="2">'.$attachdata['Attachment']['attach_file'].'</td>	
                        <td>'.date('m/d/Y',strtotime($attachdata['Attachment']['created'])).'</td>	
                        <td colspan="2">'.$this->Common->getClientName($attachdata['Attachment']['user_id']).'('.$attachdata['Attachment']['added_by'].')</td>	 
                        <td>&nbsp;</td>	         
                  	</tr>'; }
                    	$html .='<tr>
                  	    <td colspan="6">&nbsp;</td>	    
                  	</tr>
                    <tr>
                  	    <td colspan="6"><u>Notifications</u>:</td>	    
                  	</tr>';
                    foreach($notifierdata as $notifydata){
                    $html .='<tr>
                  	    <td colspan="2">'.$notifydata['Notification']['notification'].'</td>	
                        <td>'.date('m/d/Y',strtotime($notifydata['Notification']['created'])).'</td>	
                        <td colspan="2">'.$this->Common->getClientName($notifydata['Notification']['user_id']).'('.$notifydata['Notification']['added_by'].')</td>	 
                        <td>&nbsp;</td>	         
                  	</tr>'; }
                        $html .= '</table>
                        
                        
                       <table width="100%">
                            <tr><td colspan="2" align="center"><h3>Deficiency</h3></td></tr>
                            <tr>
                                <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                                <br/>
                                '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                                <br/>
                                '.$clientResult['User']['site_name'].'
                                <br/>';
                                if(!empty($clientResult['User']['site_country_id'])){
                                                $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                                if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                                $html.= $clientResult['User']['site_city'].' <br/>
                                '.$clientResult['User']['site_phone'].'
                                </td>
                                <td width="50%" align="right" style="padding-right:30px;">
                                    <i><b>Prepared By:</b></i>
                                    <br/>'.
                                    $spResult['Company']['name'].'
                                    <br/>
                                    '. $spResult['User']['address'].'
                                    <br/>
                                    ';	if(!empty($spResult['Country']['name'])){
                                              $html.= $spResult['Country']['name'].', ';
                                            }
                                            if(!empty($spResult['State']['name'])) {
                                               $html.=  $spResult['State']['name'].', ';
                                            }
                                           $html.=  $spResult['User']['city'];
                                    
                                   $html.= ' <br/>
                                    Phone  '.$spResult['User']['phone'].'
                                </td>
                            </tr>
                        </table>
                        <table width="100%" class="tbl input-chngs1">
                            <tr>
                                
                                <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                                <th width="90%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
                                
                            </tr>';
                            
                            if(!empty($Def_record))
                            {
                           // pr($codeData);
                            foreach($Def_record as $Def_record) {
                           
                            $html.='<tr>
                                
                                <td valign="top">'. $Def_record['Code']['code'].'</td>
                                <td valign="top">'. $Def_record['Code']['description'].'</td>
                                
                            </tr>';
                            }
                            } else
                            {
                             $html.= '<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';  
                            }
                            $html.='</table>
                            <table width="100%">
                            <tr><td colspan="2" align="center"><h3>&nbsp;</h3></td></tr>
                                <tr>
                                    <td align="center" colspan="2"><h3>Recommendation</h3></td>
                                </tr>
                                <tr>
                                    <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                                    <br/>
                                    '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                                    <br/>
                                    '.$clientResult['User']['site_name'].'
                                    <br/>
                                    ';
                                    if(!empty($clientResult['User']['site_country_id'])){
                                                    $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                                    if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                                    $html.= $clientResult['User']['site_city'].'
                                    <br/>
                                    '.$clientResult['User']['site_phone'].'
                                    </td>
                                    <td width="50%" align="right" style="padding-right:30px;">
                                        <i><b>Prepared By:</b></i>
                                        <br/>
                                        '.$spResult['Company']['name'].'
                                        <br/>
                                        '.$spResult['User']['address'].'
                                        <br/>';
                                            if(!empty($spResult['Country']['name'])){
                                                    $html.= $spResult['Country']['name'].', ';
                                                }
                                                if(!empty($spResult['State']['name'])) {
                                                    $html.= $spResult['State']['name'].', ';
                                                }
                                               $html.=  $spResult['User']['city'];
                                       
                                      $html.=   '<br/>
                                        Phone  '.$spResult['User']['phone'].'
                                    </td>
                                    
                                    </tr>
                            </table>
                                
                            <table width="100%" class="tbl input-chngs1" >
                             <tr width="100%">
                                 
                                 <th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                                 <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
                                  <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>
                                 
                             </tr>';
                             if(!empty($Def_record_recomm))
                             {
                             
                             foreach($Def_record_recomm as $Def_record_recomm) {
                           
                             $html.='<tr width="100%">
                                 
                                 <td valign="top">'.$Def_record_recomm['Code']['code'].'</td>
                                 <td valign="top">'.$Def_record_recomm['Code']['description'].'</td>
                                  <td valign="top">'.$Def_record_recomm['Deficiency']['recommendation'].'</td>
                                 
                             </tr>';
                              }
                             } else
                             {
                              $html.='<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
                             }
                             $html.='</table>
                             
                <table width="100%" class="tbl">
                    <tr width="100%">
                        <td  align=center  style="border:none;"><span style="font-size:15px; font-weight:bolder;"><h3>Signature Page</h3></span></td>                        
                    </tr>                   
                </table>
                <table width="100%">
                    <tr>
                        <td style="border:none;"><b>Inspector Signature</b><br/><br/>';
                        $name=$this->Common->getInspectorSignature($record['SprinklerReport']['report_id'],$record['SprinklerReport']['servicecall_id'],$record['SprinklerReport']['inspector_id']);
                        if($name!='empty'){
                            $html.= '<img src="/app/webroot/img/signature_images/'.$name.'">';
                        }
                        $html.='                        
                        </td>
                        <td style="border:none; vertical-align:top;" align=right><b>Client Signature</b></td>
                  
                    </tr>
                    <tr>
                        <td style="border:none;">';
                        $dateCreated=$this->Common->getSignatureCreatedDate($record['SprinklerReport']['report_id'],$record['SprinklerReport']['servicecall_id'],$record['SprinklerReport']['inspector_id']);
                          
                        if($dateCreated!='empty'){
                        $html.= $this->Common->getClientName($record['SprinklerReport']['inspector_id'])."<br/><br/>".
                          $time->Format('m-d-Y',$dateCreated);
                        }
                       $html.='</td>
                        <td align=left style="border:none;"></td>
                  
                    </tr>'
                    
                
                            
                    
                    ;

	
	 
	 $html .= '</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
 
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Sprinkler_Inspection.pdf', 'D');die;