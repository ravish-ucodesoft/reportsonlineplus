<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<?php
    echo $this->Html->script('accordian.pack');
    echo $this->Html->css('style_tablesort');
?>
<script type="text/javascript">
    jQuery(function(){
	jQuery('#sortTable').tablesorter();	
    });
</script>
<style>
.lbtxt{text-align:right;width:20%}
.input-chngs1 input{
    margin-right: 3px;
    width: 180px;
}
.input-chngs2 input{
	margin:0px;
	color: #666666;
	font-size: 12px;
}
#basic-accordian{
	border:5px solid #EEE;
	padding:5px;
	width:950px;
	position:absolute;
	top:10%;
	margin-left:0px;
	z-index:2;
	/*margin-top:-200px;*/
}
.accordion_headings{
	padding:5px;
	background:url('../../img/newdesign_img/nav_bg.png') bottom;
	color:#FFFFFF;
	border:1px solid #FFF;
	cursor:pointer;
	font-weight:bold;
	font-size:12px;
	border-radius:6px 6px 0 0 ;
}
.accordion_headings:hover{
	background:url('../../img/newdesign_img/nav_bg.png') top;
}
.accordion_child{
	padding:15px;
	background:#EEE;
}
.header_highlight{
	background:url('../../img/newdesign_img/nav_active.png') !important;
}
.tab_container *{
	float:left;
	width:145px;
}
.message{text-align:center;color:#54A41A;font-weight:bold;}
/*----------- 30052012---------------------*/
.input-chngs input{ background: #ebebeb; /* Old browsers */
background: -moz-linear-gradient(top, #ebebeb 0%, #ffffff 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ebebeb), color-stop(100%,#ffffff)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* IE10+ */
background: linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebebeb', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
border:1px solid #bbb; width:66%; margin:0 30% 0 0;
}
.input-chngs td { color:#666;}

table td { border:none; vertical-align:top;}
#box .blue-hdng { background: none repeat scroll 0 0 #F4FAFF; border:none;
    border-bottom: 1px solid #AECEEF;
    color: #2281D4;
    font-size: 16px; font-weight:normal;
}
.red{
color: red; 
}
.qtytable td{ border:1px solid black;}
.qtytable{ border-collapse: collapse;}
.blue_bg {background:url("../../img/newdesign_img/nav_active.png") !important; color:#fff !important; font-weight:bold}
</style>
<body onload="new Accordian('basic-accordian',5,'header_highlight');">
<div id="content">
    <div id="box">                       
	<h3  align="center">Kitchenhood Report</h3>
	<br/>	
    </div>
</div>


<div id="basic-accordian" ><!--Parent of the Accordion-->
<div class="tab_container">
  <div id="test1-header" class="accordion_headings header_highlight" >Cover Page</div>  
  <div id="test2-header" class="accordion_headings" >Summary Page</div>
  <div id="test3-header" class="accordion_headings" >Report</div>
  <div id="test4-header" class="accordion_headings" >Deficiencies</div>
  <div id="test5-header" class="accordion_headings" >Recommendation</div>  
  <div id="test6-header" class="accordion_headings" >Missed</div>
  <div id="test7-header" class="accordion_headings" >Additional</div>  
  <div id="test8-header" class="accordion_headings" >Signature Page</div>
  <div id="test9-header" class="accordion_headings" >Certification</div>
  <div id="test10-header" class="accordion_headings" >Quotes</div>
</div>
<div style="float:left;width:100%;">
  <div id="test1-content">
	<div class="accordion_child">
	<table class="tbl input-chngs1" width="100%"> 
	    <tr>
		<td colspan="4" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>
            </tr>         
	    <tr>
		<td colspan="3" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder; color: #000;">Cover Page</span></td>
		<td align="right" ><?php echo $html->link($html->image("pdf.png"), '/reports/coverPagePrintCommon/KitchenhoodReport?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Cover Page Preview'));?></td>
	    </tr> 
	    <tr>
		<td colspan="4">
		    <table width="100%">
			<tr>
			    <td align="center"><?php echo $this->Html->image('company_logo/'.$spResult['Company']['company_logo']); ?></td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" style="font-size:15px;letter-spacing: 4px;"><b><?php echo $spResult['Company']['name'];  ?></b></td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" style="font-size:13px">
			<?php $arr=explode(',',$spResult['Company']['interest_service']);
				foreach($arr as $key=>$val):
				foreach($reportType as $key1=>$val1):
				if($val== $key1)
				{
			?>	
			<?php  if($key1 == 9){ echo $spResult['Company']['interest_service_other']; }else{ echo $val1;} ;?><span class="red"> * </span>     
			<?php    }
				endforeach;
				endforeach;
			?>
			    </td>
			</tr>		     
			<tr>
			    <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" style="font-size:36px">Kitchenhood Report</td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" style="font-size:36px">Schedule Frequency: <?php echo $schFreq;?></td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
			</tr>
			<tr>
			    <td align="right" class="Cpage" >Date:<?php if($record['KitchenhoodReport']['finish_date']!= ''){
			    echo $time->Format('m/d/Y',$record['KitchenhoodReport']['finish_date']); } ?></td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" ><i>Prepared for:</i></td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" >&nbsp;</td>
			</tr>
			<tr>
			    <td align="center" class="Cpage" ><?php echo $clientResult['User']['client_company_name'] ?></td>
			</tr>	  
			<tr>
				<td align="center" class="Cpage" ><?php echo $clientResult['User']['address'] ?></td>
			</tr>
			<tr>
				<?php $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];?>
				<td align="center" class="Cpage" ><?php echo $cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']); ?></td>
			</tr>
			<tr>
				<td align="center" class="Cpage" ><?php echo $clientResult['User']['zip']; ?></td>
			</tr>
		    
			<tr>
				<td align="center" class="Cpage" >&nbsp;</td>
			</tr>
			<tr>
				<td align="center" class="Cpage" ><b>Site Address Info:</b></td>
			</tr>
			<tr>
				<td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
			</tr>
			<tr>
				<td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact']; ?></td>
			</tr>
			<tr>
				<td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?></td>
			</tr>
			<tr>
				<?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
				<td align="center" class="Cpage" ><?php echo $siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']); ?></td>
			</tr>
			<tr>
				<td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_zip']; ?></td>
			</tr>
			
			<tr>
			    <td align="left" class="Cpage" >Prepared By:</td>
			</tr>
			<tr>
			    <td align="left" class="Cpage"></td>
			</tr>  
			<tr>
			    <td align="left" class="Cpage"><?php echo $spResult['Company']['name'];  ?></td>
			</tr>
			<tr>
			    <td align="left" class="Cpage"><?php echo $spResult['User']['address'];  ?></td>
			</tr>
			<tr>
			    <?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
			    <td align="left" class="Cpage"><?php echo $spResult['Country']['name'].','.$spResult['State']['name'].','.$spCity;  ?></td>
			</tr>		      
			<tr>
			    <td align="left" class="Cpage">&nbsp;</td>
			</tr>  
			<tr>
			    <td align="left" class="Cpage" style="font-size:11px">&nbsp;</td>
			</tr>
		    </table>
		</td></tr>
	</table>	
    </div>
  </div>
  
  <div id="test2-content">
	<div class="accordion_child">
	    <table width="100%" class="tbl">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
			<td colspan="2" align=right width="57%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Summary Page</span></td>
			<td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/summaryPagePrintKitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Summary Page Preview'));?></td>
		</tr>      
	    </table>
	    <table width="100%">
		<tr>
		    <td width="50%" align="left"><i><b>Prepared For:</b></i>
		    <br/>
		    <?php echo ucwords($clientResult['User']['client_company_name']); ?>            
		    <br/>
		    <?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
			if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
		    <br/>
		    <b>Site Information:</b><br/>
		    <?php echo $siteaddrResult['SpSiteAddress']['site_name'];?><br/>
		    <?php echo $siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact']; ?><br/>
		    <?php echo $siteaddrResult['SpSiteAddress']['site_address'];?><br/>
		    <?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
		    <?php echo $siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']); ?>-<?php echo $siteaddrResult['SpSiteAddress']['site_zip']; ?><br/>
		    </td>
		    <td width="50%" align="right" style="padding-right:30px;"><i><b>Prepared By:</b></i>
		    <br/>
		    <?php echo $spResult['Company']['name']; ?>
		    <br/>
		    <?php echo $spResult['User']['address']; ?>
		    <br/>
		    <?php 	if(!empty($spResult['Country']['name'])){
			echo $spResult['Country']['name'].', ';
		    }
		    if(!empty($spResult['State']['name'])) {
			echo $spResult['State']['name'].', ';
		    }
		    echo $spResult['User']['city'];
		    ?>
		    <br/>
		    Phone <?php echo ' '.$spResult['User']['phone']; ?>
		    </td>            
		</tr>     
	    </table>
	    <table width="100%" class="qtytable">
		<tr style="background-color: #3688B7; color: #fff;">
		    <td rowspan="2">Sr. No.</td>
		    <td rowspan="2">Device</td>
		    <td>Surveyed</td>
		    <td>Inspected</td>
		    <td>Pass</td>
		    <td>Fail</td>
		</tr>
		<tr style="background-color:#3688B7; color:#fff;">
		    <td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td>
		</tr>
		<?php
		    $summCounter=0;
		    foreach($options as $serviceData){
			$summCounter++;
			$amount[]= $serviceData['amount'];
			$served[] = $serviceData['served'];
			$pass[] = $serviceData['pass'];
			$fail[] = $serviceData['fail'];
		?>
		<tr>
		  <td><?php echo $summCounter.'.'; ?></td>
		  <td><?php echo $serviceData['name']; ?></td>
		  <td><?php echo $serviceData['amount']; ?></td>
		  <td><?php echo $serviceData['served']; ?></td>
		  <td><?php echo $serviceData['pass']; ?></td>
		  <td><?php echo $serviceData['fail']; ?></td>
		</tr>
		<?php } ?>
		<tr style="background-color: #3688B7;">
		  <td style="color:#fff">Total</td>
		  <td style="color:#fff"></td>
		  <td style="color:#fff"><?php echo array_sum($amount); ?></td>
		  <td style="color:#fff"><?php echo array_sum($served); ?></td>
		  <td style="color:#fff"><?php echo array_sum($pass); ?></td>
		  <td style="color:#fff"><?php echo array_sum($fail); ?></td>
		</tr>
	    </table>
	</div>
  </div>
  
  <div id="test3-content">
	<div class="accordion_child">	    
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
			<td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bold; color: #000;">Report</span></td>
			<td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/reportPagePrintKitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Report Page Preview'));?></td>
		</tr> 
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
	    </table>	
	    <table class="tbl input-chngs1" width="100%" style="margin-left:0px;">	
		<tr><th colspan="2" align="left" style="background: #C6C6C6; padding:5px; text-align: left; font-size: 16px;">Client information</th></tr>
		<tr>
		    <td valign="top">
			<table width="100%" cellpadding="0" cellspacing="0">
			   <tr>
			      <td class="table_label"><strong>Building Information</strong></td>
			      <td><?php echo $clientResult['User']['client_company_name'];?></td>
			    </tr>
			    <tr>
			      <td class="table_label"><strong>Address</strong></td>
			      <td><?php echo $clientResult['User']['address'];?></td>
			    </tr>
			    <tr>
			      <td class="table_label"><strong>City/State/Zip:</strong></td>
			      <td> <?php echo $cityName.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
			    </tr>
			    <tr>
			      <td class="table_label"><strong>Country: </strong></td>
			      <td><?php echo $clientResult['Country']['name'];?></td>
			    </tr>
			</table>
		    </td>
		    <td valign="top">
			<table width="100%" cellpadding="0" cellspacing="0">
			   <tr>
			      <td class="table_label"><strong>Contact: </strong></td>
			      <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
			    </tr>
			    <tr>
			      <td class="table_label"><strong>Phone:</strong></td>
			      <td><?php echo $clientResult['User']['phone'];?></td>
			    </tr>                               
			    <tr>
			      <td class="table_label"><strong>Email: </strong></td>
			      <td> <?php echo $clientResult['User']['email'];?></td>
			    </tr>
			   </table>
		    </td>
		</tr>
		<tr>
			<th colspan="2">&nbsp;</th>
		</tr>
		<tr><th colspan="2" align="left" style="background: #C6C6C6; text-align: left; font-size: 16px;">Site Information</th></tr>
		<tr>
		    <td valign="top">
			   <table width="100%" cellpadding="0" cellspacing="0">
			   <tr>
			      <td class="table_label"><strong>Site Name:</strong></td>
			      <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
			    </tr>
			    <tr>
			      <td class="table_label"><strong>Address</strong></td>
			      <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?></td>
			    </tr>
			    <tr>
			      <td class="table_label"><strong>City/State/Zip:</strong></td>
			      <td> <?php echo $siteCity.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
			    </tr>
			    <tr>
			      <td class="table_label"><strong>Country: </strong></td>
			      <td><?php echo $siteaddrResult['Country']['name'];?></td>
			    </tr>
			   </table>
		    </td>
		    <td valign="top">
			<table width="100%" cellpadding="0" cellspacing="0">
			   <tr>
			      <td class="table_label"><strong>Contact: </strong></td>
			      <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
			    </tr>
			    <tr>
			      <td class="table_label"><strong>Phone:</strong></td>
			      <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
			    </tr>                               
			    <tr>
			      <td class="table_label"><strong>Email: </strong></td>
			      <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
			    </tr>
			   </table>
		    </td>
		</tr>
		<tr>
			<th colspan="2">&nbsp;</th>
		</tr>
		<!--<tr><th colspan="2" align="left">Service Provider Info</th></tr>
		<tr>
		  <td valign="top">
			 <table width="100%" cellpadding="0" cellspacing="0">
			 <tr>
			    <td class="table_label"><strong>Name:</strong></td>
			    <td><?php //echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Address</strong></td>
			    <td><?php //echo $spResult['User']['address'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <td><?php //echo $spResult['User']['city'].', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Country: </strong></td>
			    <td><?php //echo $spResult['Country']['name'];?></td>
			  </tr>
			 </table>
		  </td>
		  <td valign="top">
		      <table width="100%" cellpadding="0" cellspacing="0">
			 <tr>
			    <td class="table_label"><strong>Contact: </strong></td>
			    <td> <?php //echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
			    <td><?php //echo $spResult['User']['phone'];?></td>
			  </tr>                               
			  <tr>
			    <td class="table_label"><strong>Email: </strong></td>
			    <td><?php //echo $spResult['User']['email'];?></td>
			  </tr>
			 </table>
		  </td>
		</tr>
		<tr><th colspan="2" align="left">Other Info</th></tr>-->
		<!--<tr>           
		    <td width="21%"><b>Date of Service:</b><br/><?php //echo $time->Format('m-d-Y',$kitchenData['KitchenhoodReport']['service_date']); ?></td>
		    <td width="20%"><b>Time:</b><?php //echo $kitchenData['KitchenhoodReport']['totaltime'].' '.$kitchenData['KitchenhoodReport']['totaltime_format']; ?></td>
		    <td colspan="4"></td>
		</tr>   
		<tr>		    
		    <td width="21%"><b>Location of System Cylinders:</b><?php //echo $kitchenData['KitchenhoodReport']['locationsystem']; ?></td>
		    <td colspan="3">&nbsp;</td>
		</tr>
		<tr>
		    <td colspan="6">&nbsp;</td>
		</tr>
		<tr>		    
		    <td width="20%"><b>Manufacturer:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['manufacturer']; ?></td>
		    <td width="20%"><b>Model:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['model']; ?></td>
		    <td width="20%"><b>Wet:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['wet']; ?></td>
		    <td width="20%"><b>Dry Chemical:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['chemical']; ?></td>
		    <td colspan="2"></td>
		</tr> 
		<tr><td colspan="6">&nbsp;</td></tr>                        
		<tr>		    
		    <td width="20%"><b>Cylinder Size (Master):</b><br/><?php //echo $kitchenData['KitchenhoodReport']['mastersize']; ?></td>
		    <td width="20%"><b>Cylinder Size (Slave):</b><br/><?php //echo $kitchenData['KitchenhoodReport']['slavesize']; ?></td>
		    <td width="20%"><b>Cylinder Size (Slave):</b><br/><?php //echo $kitchenData['KitchenhoodReport']['slavesize1']; ?></td>
		    <td colspan="2"></td>
		</tr>
		<tr><td colspan="6">&nbsp;</td></tr>                         
		<tr>		    
		    <td width="20%"><b>Fuse Links 360 F:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['fuselink1']; ?></td>
		    <td width="20%"><b>Fuse Links 450 F:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['fuselink2']; ?></td>
		    <td width="20%"><b>Fuse Links 500 F:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['fuselink3']; ?></td>
		    <td width="20%"><b>Other:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['fuselink_other']; ?></td>
		    <td colspan="1"></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>		    
		    <td width="20%"><b>Fuel Shut Off:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['shutoff']; ?></td>
		    <td width="20%"><b>Electric:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['electric']; ?></td>
		    <td width="20%"><b>Gas:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['gas']; ?></td>
		    <td width="20%"><b>Size:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['size']; ?></td>
		    <td></td>
		</tr>
		<tr><td>&nbsp;</td></tr>  
				
		<tr>		    
		    <td width="20%"><b>Serial Number:</b><br/><?php //echo $kitchenData['KitchenhoodReport']['serialnumber']; ?></td>
		    <td width="20%"><b>Last Hydro Test Date:</b><br/><?php //echo $time->Format('m-d-Y',$kitchenData['KitchenhoodReport']['hydro_date']);  ?></td>
		    <td width="20%"><b>Last Recharge Date:</b><br/><?php //echo $time->Format('m-d-Y',$kitchenData['KitchenhoodReport']['recharge_date']); ?></td>
		    <td colspan="2"></td>
		</tr>
		   </table>
		    <table class="emrgncy-tbl2" style="width:100%">
		    <tr>
			<td align="right" colspan="6"></td>
		    </tr>                     
		<tr><td colspan="6"><b><i>COOKING APPLIANCE SIZES (NoTE: List appliances from left to right and indicate Nozzles used for each)</i></b></td></tr>
		<tr><td colspan="6"><b><i>(Please Note the Plenum and Duct Size(s) in appropriate boxes below)</i></b></td></tr>
			    
		<tr>
		    <td><b>Plenum Size(s):</b></td>
		    <td><?php //echo $kitchenData['KitchenhoodReport']['plenumsize1']; ?></td>
		    <td width="20%"><?php //echo $kitchenData['KitchenhoodReport']['plenumsize2']; ?></td>
		    <td width="20%"><?php //echo $kitchenData['KitchenhoodReport']['plenumsize3']; ?></td>
		    <td colspan="2"></td>
		</tr>
		<tr><td colspan="6">&nbsp;</td></tr> 
		<tr>
		    <td><b>Duct Size(s):</b></td>
		    <td><?php //echo $kitchenData['KitchenhoodReport']['ductsize1']; ?></td>
		    <td width="20%"><?php //echo $kitchenData['KitchenhoodReport']['ductsize2']; ?></td>
		    <td width="20%"><?php //echo $kitchenData['KitchenhoodReport']['ductsize3']; ?></td>
		    <td colspan="2"></td>
		</tr>
		<tr><td colspan="6">&nbsp;</td></tr>
			    
		<tr>
		    <td colspan="2">1:All appliances properly covered w/ correct Nozzles</td>
		    <td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_1']; ?></td>
		    <td colspan="2">19. Check travel of able nuts/S-hooks</td>
		    <td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_19']; ?></td>
		</tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
				
		    <tr>
			<td colspan="2" >2:Duct and plenum covered w/ correct Nozzles</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_2']; ?></td>
			<td colspan="2" >20. Piping and conduit securely bracketed</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_20']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">3:Check positioning of all Nozzles</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_3']; ?></td>
			<td colspan="2">21:Proper separation between fryers & flame</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_21']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">4:System installed in accordance w/ Mfg UL listing</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_4']; ?></td>
			<td colspan="2">22:Proper clearance-flame to filters</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_22']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
		    <tr>
			<td colspan="2">5:Hood/duct penetrations sealed w/weld or UL device</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_5']; ?></td>
			<td colspan="2">23:Exhaust fan operating properly</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_23']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">6: Check if seals intact, evidence of tampering</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_6']; ?></td>
			<td colspan="2">24: All filters reinstalled</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_24']; ?></td>
		    </tr>
		     </table>
		    
		    <table class="emrgncy-tbl2" style="width:100%">
		    <tr>
			<td align="right" colspan="6"></td>
		    </tr> 
		    <tr><td colspan="6">&nbsp;</td></tr>
			 
		    <tr>
			<td colspan="2">7: If system has been discharged, report same</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_7']; ?></td>
			<td colspan="2">25: Fuel shut-off in ON position</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_25']; ?></td>
		    </tr>
		   
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">8: Pressure gauge in proper range (if gauged)</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_8']; ?></td>
			<td colspan="2">26:Manual & Remove set/seals in place</td>
			<td colspan="2" style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_26']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">9: Check cartridge weight (Replace, if needed)</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_9']; ?></td>
			<td colspan="2">27:Reinstall systems covers</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_27']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">10:Hydrostatic/6 year maintenance date</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_10']; ?></td>
			<td colspan="2">28:System operational and seals in place</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_28']; ?></td>
		    </tr>
		    
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">11: Inspect cylinder and mount</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_11']; ?></td>
			<td colspan="2">29:Slave system operational</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_29']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">12: Operate system from terminal link</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_12']; ?></td>
			<td colspan="2">30:Clean cylinder and mount</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_30']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
			     
		    <tr>
			<td colspan="2">13:Test for proper operation from remote</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_13']; ?></td>
			<td colspan="2" >31:Fan warning sign on hood</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_31']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
		    <tr>
			<td colspan="2">14:  Check operation of micro switch</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_14']; ?></td>
			<td colspan="2">32:  Personnel instructed in manual operation of system</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_32']; ?></td>
		    </tr>
		    
		    <tr><td colspan="6">&nbsp;</td></tr>
		    <tr>
			<td colspan="2">15:Check operation of gas valve</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_15']; ?></td>
			<td colspan="2">33:Proper hand portable extinguishers(K Class and ABC)</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_33']; ?></td>
		    </tr>
		    <tr><td colspan="6">&nbsp;</td></tr>
		    <tr>
			<td colspan="2">16:Proper Nozzle covers in place/clean Nozzles</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_16']; ?></td>
			<td colspan="2">34:Portable extinguishers properly serviced</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_34']; ?></td>
		    </tr>
		    
		    <tr><td colspan="6">&nbsp;</td></tr>
		    <tr>
			<td colspan="2">17:Check fuse links and clean</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_17']; ?></td>
			<td colspan="2">35:Service & certification tag on system</td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_35']; ?></td>
		    </tr>
		    
		    <tr><td colspan="6">&nbsp;</td></tr>
		    <tr>
			<td colspan="2">18: Replaced fuse links (Record date here) </td>
			<td style="padding-left:50px"><?php //echo $kitchenData['KitchenhoodReport']['question_18']; ?></td>
			<td colspan="3"></td>
		    </tr>-->
	     </table>
	     
	     <table width="100%"><tr> <td align="right" colspan="4"><!--<a href="#" class="quickFlipCta"><?php echo $html->image('next-page-icon.jpg');?></a>--></td></tr></table>
	    <h3 class="main-heading" align="center" >KITCHENHOOD REPORT RECORD</h3>
	
	    <table width="100%" class="qtytable tablesorter" id="sortTable">
		<thead>
		  <tr>
		    <th width="5%">Sr. No.</th>
		    <th width="25%">Location</th>
		    <th width="25%">Type</th>	    
		    <th width="25%">Born Date</th>
		    <th width="20%">Deficient</th>	    		    
		  </tr>	  
		  <tr>
		    <td colspan="5"><strong></strong></td>
		  </tr>
		  </thead>
		<tbody>
	       <?php
		if(!empty($record['KitchenhoodReportRecord'])){
		    $reportCounter=0;
	       foreach($record['KitchenhoodReportRecord'] as $recorddata){ $reportCounter++;?>   
		  <tr style="font-size:14px">
		    <td><?php echo $reportCounter.'.'; ?></td>
		    <td><?php echo $recorddata['location']; ?></td>
		    <td><?php echo $this->Common->getServiceName($recorddata['type']); ?></td>	    
		    <td><?php echo $recorddata['born_date']; ?></td>
		    <td <?php if($recorddata['deficiency']=='Yes'){?> style="color: red"<?php }?>><?php echo $recorddata['deficiency']; ?></td>	    		    
		  </tr>	   
		  <?php } } ?>
		  </tbody>	    
		  <tr>
		    <td colspan="5">&nbsp;</td>
		  </tr>	    
	    </table>
    </div>
  </div>  
  
  <div id="test4-content">
	<div class="accordion_child">
	<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>    
	<div style="text-align:center;width:100%;font-size:15px; color: red;"><b>Deficiencies</b></div>
        <div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/deficiencyPagePrint/Kitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Deficiency Page Preview'));?></div>        
        <div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Kitchenhood</b></div>
        <br/>
	<table width="100%">
	    <tr>
		<td align="right" colspan="2"></td>
	    </tr>
	    <tr>
		<td width="50%" align="left"><i><b>Prepared For:</b></i>
		<br/>
		<?php echo ucwords($clientResult['User']['client_company_name']); ?>            
		<br/>
	       <?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
		    if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
		<br/>
		<b>Site Information:</b><br/>
		<?php echo $siteaddrResult['SpSiteAddress']['site_name'];?><br/>
		<?php echo $siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact']; ?><br/>
		<?php echo $siteaddrResult['SpSiteAddress']['site_address'];?><br/>
		<?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
		<?php echo $siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']); ?>-<?php echo $siteaddrResult['SpSiteAddress']['site_zip']; ?><br/>
		</td>
		<td width="50%" align="right" style="padding-right:30px;">
		    <i><b>Prepared By:</b></i>
		    <br/>
		    <?php echo $spResult['Company']['name']; ?>
		    <br/>
		    <?php echo $spResult['User']['address']; ?>
		    <br/>
		    <?php 	if(!empty($spResult['Country']['name'])){
				echo $spResult['Country']['name'].', ';
			    }
			    if(!empty($spResult['State']['name'])) {
				echo $spResult['State']['name'].', ';
			    }
			    echo $spCity;
		    ?>
		    <br/>
		    Phone <?php echo ' '.$spResult['User']['phone']; ?>
		</td>
		
	    </tr>
	</table>
        
       <table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
        <tr>            
            <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
	    <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
            <th width="60%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
	    <th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Attachment</b></th>
        </tr>
        <?php
        if(!empty($Def_record))
        {
		$defCounter=0;
       // pr($codeData);
        foreach($Def_record as $Def_record) {
		$defCounter++;
        ?>
        <tr>            
            <td valign="top"><?php echo $defCounter.'.'; ?></td>
	    <td valign="top"><?php echo $Def_record['Code']['code']; ?></td>	    
            <td valign="top"><?php echo $Def_record['Code']['description']; ?></td>
	    <td valign="top">
	    <?php if(!empty($Def_record['Deficiency']['attachment']) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$Def_record['Deficiency']['attachment'])){?>
		<?php echo $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$Def_record['Deficiency']['attachment'],'sp'=>false)); ?>
	    <?php }else{?>
		<?php echo 'No Attachment';?>
	    <?php }?>
	    </td>
        </tr>
        <?php }
        } else
        {
         echo '<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';   
        }
        ?>
    </table>       
    </div>
  </div>	
  <div id="test5-content">
    <div class="accordion_child">	
	<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
	<div style="text-align:center;width:100%;font-size:15px; color:orange;"><b>Recommendation</b></div>
        <div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/recommendationPagePrint/Kitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Recommendation Page Preview'));?></div>        
        <div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Kitchenhood</b></div>
        <br/>
	<table width="100%">
	    <tr>
		<td align="right" colspan="2"></td>
	    </tr>
	    <tr>
		<td width="50%" align="left"><i><b>Prepared For:</b></i>
		<br/>
		<?php echo ucwords($clientResult['User']['client_company_name']); ?>            
		<br/>
		<?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
		    if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
		<br/>            
		</td>
		<td width="50%" align="right" style="padding-right:30px;">
		    <i><b>Prepared By:</b></i>
		    <br/>
		    <?php echo $spResult['Company']['name']; ?>
		    <br/>
		    <?php echo $spResult['User']['address']; ?>
		    <br/>
		    <?php 	if(!empty($spResult['Country']['name'])){
				echo $spResult['Country']['name'].', ';
			    }
			    if(!empty($spResult['State']['name'])) {
				echo $spResult['State']['name'].', ';
			    }
			    echo $spCity;
		    ?>
		    <br/>
		    Phone <?php echo ' '.$spResult['User']['phone']; ?>
		</td>		
	    </tr>
	</table>
	<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
        <tr>            
            <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
	    <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
            <th width="30%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
            <th width="30%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>            
        </tr>
        <?php
        if(!empty($Def_record_recomm))
        {
        $recommCounter=0;
        foreach($Def_record_recomm as $Def_record_recomm) {
		$recommCounter++;
        ?>
        <tr>            
            <td valign="top"><?php echo $recommCounter.'.'; ?></td>
	    <td valign="top"><?php echo $Def_record_recomm['Code']['code']; ?></td>
            <td valign="top"><?php echo $Def_record_recomm['Code']['description']; ?></td>
            <td valign="top"><?php echo $Def_record_recomm['Deficiency']['recommendation']; ?></td>            
        </tr>
        <?php }
        } else
        {
	    echo '<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
        }
        ?>
    </table>
    </div>
  </div>
    
    <div id="test6-content">
	<div class="accordion_child">	    
	<table width="100%" class="tbl">
	    <tr>
		<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>
	    </tr>
	    <tr>
		<td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder; color:green;">Missed</span></td>
		<td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/missingItemsPagePrintKitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Missed Items Page Preview'));?></td>
	    </tr>      
	</table>
	<table width="100%">
	    <tr>
		    <td style="text-align:center;">Missed Device Page Prepared For: <?php echo $this->Common->get_comission_type($clientResult['User']['code_type_id']); ?></td>        
	    </tr>           
	    <tr>
		    <td align="center" class="Cpage" ><?php echo ucwords($clientResult['User']['client_company_name']); ?></td>
	    </tr>
	    <tr>
		    <td align="center" class="Cpage" ><?php echo $clientResult['User']['address'] ?></td>
	    </tr>
	    <tr>			
		    <td align="center" class="Cpage" ><?php echo $cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']); ?></td>
	    </tr>
	    <tr>
		    <td align="center" class="Cpage" ><?php echo $clientResult['User']['zip']; ?></td>
	    </tr>      
	</table>
	<table width="100%" class="qtytable">
	    <tr>
		<td rowspan="2" style="background:#2b92dd; color:#fff; font-weight:bold">Sr. No.</td>
		<td rowspan="2" style="background:#2b92dd; color:#fff; font-weight:bold">Device</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Surveyed</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Inspected</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Pass</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Fail</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Missed item</td>
	    </tr>
	    <tr style="background:#666; color:#fff; font-weight:bold">
		<td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td>
	    </tr>
	    <?php
	    $missCounter=0;
	    foreach($options as $serviceData){
		$amount[]= $serviceData['amount'];
		$served[] = $serviceData['served'];
		$pass[] = $serviceData['pass'];
		$fail[] = $serviceData['fail'];
		$missCounter++;
	    ?>
	    <tr>
		<td><?php echo $missCounter.'.'; ?></td>
		<td><?php echo $serviceData['name']; ?></td>
		<td><?php echo $serviceData['amount']; ?></td>
		<td><?php echo $serviceData['served']; ?></td>
		<td><?php echo $serviceData['pass']; ?></td>
		<td><?php echo $serviceData['fail']; ?></td>
		<?php if($serviceData['amount']>$serviceData['served']){
			  $missed = $serviceData['amount']-$serviceData['served'];
		      }else{
			  $missed = 0;
		      }
		 ?>
		<td><?php echo $missed; ?></td>
	    </tr>
	    <?php } ?>        
        </table>
	
	<table style="color: green">
		<?php 
			$missingData = $this->Common->missingTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
			$missingText=$missingData['ExplanationText']['missing_reason'];
			$missingfile=$missingData['ExplanationText']['missing_file'];
			$missingText = (($missingText!="")?$missingText:'No explanation added');
			if(!empty($missingfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$missingfile)){
				$showMissfile = $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$missingfile,'sp'=>false));
			}else{
				$showMissfile='No Attachment';
			}
		?>
		<tr>								
			<td colspan="6">Missed Items Explanation: <?php echo $missingText;?></td>
		</tr>
		<tr>								
			<td colspan="6">Missed Items Attachment: <?php echo $showMissfile;?></td>
		</tr>
	</table>
	
    </div>
  </div>
  <div id="test7-content">
	<div class="accordion_child">
	<table width="100%" class="tbl">
	    <tr>
		<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>
	    </tr>
	    <tr>
	      <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; color:purple;"><b>Additional</b></span></td>
	      <td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/additionalItemsPagePrintKitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Additional Items Page Preview'));?></td>
	    </tr>      
	</table>
	<table width="100%">
	    <tr>
		<td style="text-align:center;">Additional Device Page Prepared For: <?php echo $this->Common->get_comission_type($clientResult['User']['code_type_id']); ?></td>        
	    </tr>           
	    <tr>
		<td align="center" class="Cpage" ><?php echo ucwords($clientResult['User']['client_company_name']); ?></td>
	    </tr>
	    <tr>
		<td align="center" class="Cpage" ><?php echo $clientResult['User']['address'] ?></td>
	    </tr>
	    <tr>			
		<td align="center" class="Cpage" ><?php echo $cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']); ?></td>
	    </tr>
	    <tr>
		<td align="center" class="Cpage" ><?php echo $clientResult['User']['zip']; ?></td>
	    </tr>
	</table>
	<table width="100%" class="qtytable">
	    <tr>
		<td rowspan="2" style="background:#2b92dd; color:#fff; font-weight:bold">Sr. No.</td>
		<td rowspan="2" style="background:#2b92dd; color:#fff; font-weight:bold">Device</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Surveyed</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Inspected</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Pass</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Fail</td>
		<td style="background:#2b92dd; color:#fff; font-weight:bold">Additional Item</td>
	    </tr>
	    <tr style="background:#666; color:#fff; font-weight:bold">
		<td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td><td style="color:#fff;">Qty</td>
	    </tr>
	    <?php
	    $addCounter=0;
	    foreach($options as $serviceData){
		$amount[]= $serviceData['amount'];
		$served[] = $serviceData['served'];
		$pass[] = $serviceData['pass'];
		$fail[] = $serviceData['fail'];
		$addCounter++;
	    ?>
	    <tr>
		<td><?php echo $addCounter.'.'; ?></td>
		<td><?php echo $serviceData['name']; ?></td>
		<td><?php echo $serviceData['amount']; ?></td>
		<td><?php echo $serviceData['served']; ?></td>
		<td><?php echo $serviceData['pass']; ?></td>
		<td><?php echo $serviceData['fail']; ?></td>
	    <?php if($serviceData['served']>$serviceData['amount']){
		 $additional = $serviceData['served']-$serviceData['amount'];
		 }else{
		 $additional = 0;
		 }
	    ?>
		<td><?php echo $additional; ?></td>
	    </tr>
        <?php } ?>
        </table>
	
	<table style="color: purple">
		<?php 
			$additionalData = $this->Common->additionalTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
			$additionalText=$additionalData['ExplanationText']['additional_reason'];
			$additionalfile=$additionalData['ExplanationText']['additional_file'];
			$additionalText = (($additionalText!="")?$additionalText:'No explanation added');
			if(!empty($additionalfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$additionalfile)){
				$showAdditionalfile = $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$additionalfile,'sp'=>false));
			}else{
				$showAdditionalfile='No Attachment';
			}
		?>
		<tr>								
			<td colspan="6">Additional Items Explanation: <?php echo $additionalText;?></td>
		</tr>
		<tr>								
			<td colspan="6">Additional Items Attachment: <?php echo $showAdditionalfile;?></td>
		</tr>
	</table>
	
	</div>
  </div>
  
  <div id="test8-content">
	<div class="accordion_child">	
	<table width="100%" class="tbl">
	    <tr>
		<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey"><?php echo ucwords($spResult['Company']['name']);?></span></td>
	</tr>
	<tr>
		<td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Signature Page</span></td>
		<td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reports/signaturePagePrint/KitchenhoodReport?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Signature Page Preview'));?></td>
	</tr>	   
	</table>
	<table width="100%">
	    <tr>
		<td style="border:none;"><b>Inspector Signature</b></br></br>
		<?php  $name=$this->Common->getInspectorSignature($record['KitchenhoodReport']['report_id'],$record['KitchenhoodReport']['servicecall_id'],$record['KitchenhoodReport']['inspector_id'])?><?php echo $this->Html->image('signature_images/'.$name);  ?>
		
		</td>
		<td style="border:none; vertical-align:top;" align=right><b>Client Signature</b></td>
	  
	    </tr>
	    <tr>
		<td style="border:none;">		
		<?php $dateCreated=$this->Common->getSignatureCreatedDate($record['KitchenhoodReport']['report_id'],$record['KitchenhoodReport']['servicecall_id'],$record['KitchenhoodReport']['inspector_id']);
		  
		if($dateCreated!='empty'){
			echo $this->Common->getClientName($record['KitchenhoodReport']['inspector_id'])."<br/><br/>";
			echo $time->Format('m-d-Y',$dateCreated);
		}
		?>
		</td>
		<td align="right" style="border:none;">
		<?php //$dateCreated=$this->Common->getSignatureCreatedDate($record['ExtinguisherReport']['report_id'],$record['ExtinguisherReport']['servicecall_id'],$record['ExtinguisherReport']['inspector_id']);
		  
		//if($dateCreated!='empty'){
			echo $this->Common->getClientName($record['KitchenhoodReport']['client_id'])."<br/><br/>";
			//echo $time->Format('m-d-Y',$dateCreated);
		//}
		?>	
		</td>
	  
	    </tr>
	</table>	
    </div>
  </div>
  
  <div id="test9-content">
	<div class="accordion_child">
		<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
		<div style="text-align:center;width:100%;font-size:15px;"><b>Certification</b></div>
		<div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/certAttachedPagePrint/Kitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'CERT Attached Page Preview'));?></div>        
		<div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Kitchenhood Report</b></div>
		<br/>
		<table width="100%">
			<tr>
			    <td align="right" colspan="2"></td>
			</tr>
			<tr>
			    <td width="50%" align="left"><i><b>Prepared For:</b></i>
			    <br/>
			    <?php echo ucwords($clientResult['User']['client_company_name']); ?>		    
			    <br/>
			    <?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
			    if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $clientResult['User']['city'];  ?>
			    <br/>
			    </td>
			    <td width="50%" align="right" style="padding-right:30px;">
				    <i><b>Prepared By:</b></i>
				    <br/>
				    <?php echo $spResult['Company']['name']; ?>
				    <br/>
				    <?php echo $spResult['User']['address']; ?>
				    <br/>
				    <?php 	if(!empty($spResult['Country']['name'])){
						    echo $spResult['Country']['name'].', ';
						}
						if(!empty($spResult['State']['name'])) {
						    echo $spResult['State']['name'].', ';
						}
						echo $spCity;
				    ?>
				    <br/>
				    Phone <?php echo ' '.$spResult['User']['phone']; ?>
			    </td>		    
			</tr>
		</table>
		<table class="tbl input-chngs1" width="100%" >                
			<tr>
				<td align='left' style="text-align:left">
				<?php
					$certs = $this->Common->getAllSchCerts($schId);						
					if(sizeof($certs)>0){
				?>					
				<?php $cr=1;
				foreach($certs as $cert)
				{
				    echo $html->link($cr.'.'.$cert['ReportCert']['ins_cert_title'],array("controller"=>"messages","action"=>"download_self_form",$cert['ReportCert']['ins_cert_form'],'inspector'=>true),array('title'=>'DownLoad/View')); 
				    
					    echo '<br/>';
				    
				    $cr++;
				}
				?>
				<?php
				}
				?>
				</td>
			</tr>	
		</table>
	    </div>
	</div>
	<div id="test10-content">
	    <div class="accordion_child">
		<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
		<div style="text-align:center;width:100%;font-size:15px;"><b>Quotes</b></div>
		<div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/quotesPagePrint/Kitchenhood?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Quotes Page Preview'));?></div>        
		<div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Kitchenhood</b></div>
		<br/>
		<table width="100%">
			<tr>
				<td align="right" colspan="2"></td>
			</tr>
			<tr>
				<td width="50%" align="left"><i><b>Prepared For:</b></i>
				<br/>
				<?php echo ucwords($clientResult['User']['client_company_name']); ?>		    
				<br/>
				<?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
				if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
				<br/>
				</td>
				<td width="50%" align="right" style="padding-right:30px;">
					<i><b>Prepared By:</b></i>
					<br/>
					<?php echo $spResult['Company']['name']; ?>
					<br/>
					<?php echo $spResult['User']['address']; ?>
					<br/>
					<?php 	if(!empty($spResult['Country']['name'])){
						    echo $spResult['Country']['name'].', ';
						}
						if(!empty($spResult['State']['name'])) {
						    echo $spResult['State']['name'].', ';
						}
						echo $spCity;
					?>
					<br/>
					Phone <?php echo ' '.$spResult['User']['phone']; ?>
				</td>			    
			</tr>
		</table>
		<table class="tbl" width="100%" >                
			<tr>
				<td align='left' style="text-align:left">
					<div><b>Quote Form from SP</b></div>
					<?php										
					if(isset($chkRecord) && !empty($chkRecord)){
						echo $html->link($chkRecord['Quote']['title'],array('controller'=>'sps','action'=>'download_file',$chkRecord['Quote']['attachment'],'quotefile'));
					}else{
						echo 'No Quote have been submitted by Service Provider.';
					}
					?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align='left' style="text-align:left">
				<div><b>Client's Response to Quote Form</b></div>
				<?php										
				$quotedocs = $this->Common->getQuoteDoc($_REQUEST['reportID'],$_REQUEST['clientID'],$_REQUEST['spID'],$_REQUEST['serviceCallID']);
				
				if(isset($quotedocs) && !empty($quotedocs)){
					if($quotedocs['Quote']['client_response']=='a'){
						$status = 'Accepted';
					}else if($quotedocs['Quote']['client_response']=='d'){
						$status = 'Denied';
					}else if($quotedocs['Quote']['client_response']=='n'){
						$status = 'No wish to fix the deficiencies';
					}else{
						$status = 'Pending';
					}
					?>
					<div>Status: <?php echo $status;?></div>
					<?php if($quotedocs['Quote']['client_response']=='a'){?>
					<div class="certificates-hdline" style="text-align: left;">
					    <?php echo $html->link('Signed Quote Form',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['signed_form'],'quotefile','sp'=>true),array('style'=>'color:red'))?>
					</div>							    							
					<?php }else if($quotedocs['Quote']['client_response']=='d'){?>
					<div class="certificates-hdline" style="text-align: left;">
					    <?php echo $html->link('Work Orders',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['work_order_form'],'quotefile','sp'=>true),array('style'=>'color:red'))?>
					</div>
					<?php }?>
					<?php					
				}else{
				    echo 'NA';
				}
				?>
				</td>
			</tr>
		</table>
	</div>
    </div>
  
</div>
</div><!--End of accordion parent-->
</body>