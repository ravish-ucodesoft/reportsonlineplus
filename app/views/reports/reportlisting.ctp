<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery(".ajax").colorbox();
});
</script>

<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:16px;"><?php echo $html->link('Add New Inspector',array('controller'=>'sps','action'=>'addInspector'),array("style"=>"padding-right:20px;")); ?></div>
	      
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addclients'),'id'=>'addclient')); ?>
              <div id="box" >
                	<h3 id="adduser">Report Listing</h3>
                        <br/>
                        <table class="tbl">
                        <tr>
			    
			    <th width="20%"> Report Name</th>
			    <th width="15%">Created</th>
			    		    
			    <th width="20%">Action</th>
                        </tr>
			<?php foreach($result  as $res) {
			    $cid = base64_encode($res['Report']['id']);
			?>
			<tr>
			    
			    <td width="20%" align="center"><?php  echo $res['Report']['name']; ?> </td>
			    <td width="15%" align="center"><?php echo $res['Report']['created']; ?></td>
			   
			    <!--<td width="20%" align="center"><?php echo $res['Service']['name']; ?></td>-->
			    <td width="20%" align="center">
				<?php echo $html->link('Edit '.$html->image('user_edit.png',array('alt'=>'Report Edit','title'=>'Report Edit')),array('controller'=>'reports','action'=>'editreport',$cid),array('escape'=>false)); ?>
				<?php //echo $html->link('View Detail',array('controller'=>'reports','action'=>'reportdetail',$cid),array('')); ?>
			    </td>
                        </tr>
			<?php } ?>
                      </table>
                </div>
		<?php echo $form->end(); ?>
            </div>