<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
$element=$this->element('reports/report_common_view');
$element_cover=$this->element('reports/report_common_cover',array('report_name'=>'Firealarm Report','finish_date'=>$record['FirealarmReport']['finish_date'],'freq'=>$schFreq));
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setHeaderData('../../../webroot/img/company_logo/'.$spResult['Company']['company_logo'],21,'','');
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->setPrintHeader(true);
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];

//Cover page
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>';
$html.=$element_cover;
        
//Summary Page        
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%">	
        <tr>
            <td style="text-align:center; font-size:36px;"><b>Summary Page</b></td>        
        </tr>';
        $html.=$element;
		
		 $html.='<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	    </table>
      <table width="100%" class="qtytable" border="1" cellpadding="2">
        <tr style="background-color: #3688B7;">
          <td rowspan="2" style="color:#fff;text-align:center;">Sr. No.</td>
	  <td rowspan="2" style="color:#fff;text-align:center;">Device Name</td>
          <td style="color:#fff;text-align:center;">Surveyed</td>
          <td style="color:#fff;text-align:center;">Serviced</td>
          <td style="color:#fff;text-align:center;">Passed</td>
          <td style="color:#fff;text-align:center;">Deficient</td>
          <td style="color:#fff;text-align:center;">Recommended</td>
        </tr>
        <tr style="background-color: #666666;">
          <td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td>
        </tr>';
	$summCounter=0;
        foreach($servicedata as $serviceData){
		$summCounter++;
        $surveydata[]= $serviceData['ScheduleService']['amount'];
        $seviceddata[]=  $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
        $passdata[]= $this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
        $deficientdata[]=$this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
        $recommenddata[]=$this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);        
        $html.='<tr>
          <td style="text-align:center">'.$summCounter.'.</td>
	  <td style="text-align:center">'.$serviceData['Service']['name'].'</td>
          <td style="text-align:center">'.$serviceData['ScheduleService']['amount'].'</td>
          <td style="text-align:center">'.$this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td style="text-align:center">'.$this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td style="text-align:center">'.$this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td style="text-align:center">'.$this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
        </tr>';
        }
        $html.='<tr style="background-color: #3688B7;">
          <td style="color:#fff; text-align:center">Total</td>
	  <td style="color:#fff; text-align:center"></td>
          <td style="color:#fff; text-align:center">'.array_sum($surveydata).'</td>
          <td style="color:#fff; text-align:center">'.array_sum($seviceddata).'</td>
          <td style="color:#fff; text-align:center">'.array_sum($passdata).'</td>
          <td style="color:#fff; text-align:center">'.array_sum($deficientdata).'</td>
          <td style="color:#fff; text-align:center">'.array_sum($recommenddata).'</td>
        </tr>
        </table>';
        /*<table>
        <tr><td colspan="7">&nbsp;</td></tr>
        <tr>
          <td colspan="6">'.$spResult['Company']['name'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">'.$spResult['Company']['address'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">'.$spResult['Country']['name'].','.$spResult['State']['name'].','.$spCityName.'</td>
		    </tr>		 
      </table>';*/
      
//Report Page        
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
    <table width="100%" class="tbl">	
        <tr>
          <td align="center" style="border:none;"><h3 style="margin-bottom:10px;" align="center" >Report Page</h3></td>          
        </tr>      
    </table>';
$html.=$commonReportPagePart;
foreach($device_place as $key=>$deviceplace):
$html.='<h3>'.ucwords($deviceplace).'</h3>		
<table style="width:100%;margin-bottom:10px; border-collapse: collapse; border:1px solid #CDCDCD; background-color:#CDCDCD;" class="tbl input-chngs1 firealramreport tablesorter" id="alarm_device_list'.$key.'">
    <thead>
        <tr>
            <td colspan="10" class="alramreporttest" style="background-color:#2B92DD; color:#fff; font-weight:bold">Alarm Device List</td>
        </tr>
        <tr>
            <th style="background-color:#99BFE6; color: #000;">Sr. No.</th>
	    <th style="background-color:#99BFE6; color: #000;">Comment</th>
            <th style="background-color:#99BFE6; color: #000;">Device Type</th>
            <th style="background-color:#99BFE6; color: #000;">Family</th>
            <th style="background-color:#99BFE6; color: #000;">Zone/ Address</th>
            <th style="background-color:#99BFE6; color: #000;">Location/Description</th>
            <th style="background-color:#99BFE6; color: #000;">Manufacturer Model</th>
            <th style="background-color:#99BFE6; color: #000;">Physical Condition</th>
            <th style="background-color:#99BFE6; color: #000;">Service</th>
            <th style="background-color:#99BFE6; color: #000;">Functional Test</th>	    
        </tr>
    </thead>
    <tbody>';        
        if(!empty($record['FaLocationDescription'])){
		$reportCounter=0;
        foreach($record['FaLocationDescription'] as $data){
		$reportCounter++;
		$tdColor = (($data['functional_test']=='Deficient')?'red':'#3D3D3D');
        if($data['fa_alarm_device_place_id'] == $key ){        
        $html.='<tr id="trloc_'.$data['id'].'" style="font-size:20px; background-color:#FFFFFF; color:'.$tdColor.';">
            <td>'.$reportCounter.'.</td>
	    <td>'.$data['comment'].'</td>
            <td>';
            foreach($device_type as $keydata => $valdata){
                if($keydata == $data['fa_alarm_device_type_id']){ 
                    $html.=$valdata;
                } } 
            $html.='</td>
            <td>'.$data['family'].'</td>
            <td>'.$data['zone_address'].'</td>
            <td>'.$data['location'].'</td>
            <td>'.$data['model'].'</td>
            <td>'.$data['physical_condition'].'</td>
            <td>'.$data['service'].'</td>
            <td>'.$data['functional_test'].'</td>	    
        </tr>';
        } } } else{ 
        $html.='<tr><td colspan="10">NO RECORD FOUND</td></tr>';
        }
    $html.='</tbody>    
</table>';
endforeach;
$html.='<table style="margin-left:50px; width: 90%; border-collapse:collapse;">
    <tr>
        <td class="lb1txt" >&nbsp;</td>  
    </tr>          
    <tr> 
        <td class="lb1txt" style="text-align:left;background-color:#2b92dd; color:#fff; border:1px solid #000;">System Function Summary & Test</td>
    </tr>           
</table>
<table style="margin-left:50px;width: 90%; border-collapse: collapse;" border="1"   class="tbl input-chngs1 firealramreport">
    <tr>
        <td><b>Functions</b></td>
        <td><b>Description</b></td>
        <td><b>Test Result</b></td>
    </tr>
    <tr>
        <td colspan="3" class="alramreporttest" style="background-color:#2B92DD; color:#fff;">Alarm Functions</td>
    </tr>
    <tr>
        <td>Alarm Bells/Horns/Strobes</td>
        <td>'.$record['FaAlarmFunction']['description'].'</td>
        <td>'.$record['FaAlarmFunction']['result'].'</td>
    </tr>
    <tr>
        <td colspan="3" class="alramreporttest" style="background-color:#2B92DD; color:#fff;">Alarm Panel Supervisory Functions</td>
    </tr>';
    foreach($record['FaAlarmPanelSupervisoryFunction'] as $data1){ 
    $html.='<tr>
        <td>'.$data1['function_name'].'</td>
        <td>'.$data1['description'].'</td>
        <td>'.$data1['result'].'</td>
    </tr>';
    }		
    $html.='<tr>
        <td colspan="3" class="alramreporttest" style="background-color:#2B92DD; color:#fff;">Auxiliary Functions</td>
    </tr>';
    foreach($record['FaAuxiliaryFunction'] as $data1){ 
    $html.='<tr>
        <td>'.$data1['function_name'].'</td>
        <td>'.$data1['description'].'</td>
        <td>'.$data1['result'].'</td>
    </tr>';
    }
    $html.='<tr>
        <td colspan="3" class="alramreporttest" style="background-color:#2B92DD; color:#fff;">Fire Pump Supervisory Functions</td>
    </tr>';
    foreach($record['FaPumpSupervisoryFunction'] as $data1){ 
    $html.='<tr>
        <td>'.$data1['function_name'].'</td>
        <td>'.$data1['description'].'</td>
        <td>'.$data1['result'].'</td>
    </tr>';
    }
    $html.='<tr>
        <td colspan="3" class="alramreporttest" style="background-color:#2B92DD; color:#fff;">Generator Supervisory Functions</td>
    </tr>';
    foreach($record['FaGeneratorSupervisoryFunction'] as $data1){ 
    $html.='<tr>
        <td>'.$data1['function_name'].'</td>
        <td>'.$data1['description'].'</td>
        <td>'.$data1['result'].'</td>
    </tr>';
    }
$html.='</table>';



//Deficiency Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%">    	
    <tr>
        <td align="center" colspan="2" style="font-size: 36px; color:red"><b>Deficiencies</b></td>
    </tr>';
    $html.=$element;
$html.='</table>
<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
    <tr>
        <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
	<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
        <th width="60%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
	<th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Attachment</b></th> 
    </tr>';    
    if(!empty($Def_record))
    {
	$defCounter=0;
        foreach($Def_record as $Def_record) {
		$defCounter++;
    $html.='<tr>        
        <td valign="top">'.$defCounter.'.</td>
	<td valign="top">'.$Def_record['Code']['code'].'</td>
        <td valign="top">'.$Def_record['Code']['description'].'</td>
	<td valign="top">';
		if(!empty($Def_record['Deficiency']['attachment']) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$Def_record['Deficiency']['attachment'])){
		    //$html.=$html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$Def_record['Deficiency']['attachment'],'sp'=>false),array('title'=>'Click to Download'));
		    $html.='Attachment'; 
		}else{
		    $html.='No Attachment';
		}
	$html.='</td>
    </tr>';
        }
    }
    else
    {
        $html.='<tr><td colspan="3" align="center">No Deficiency Found.</td></tr>';   
    }    
$html.='</table>';

//Recommendation Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px; color:orange"><b>Recommendation</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: Firealarm</b></div>
<br/>
<table width="100%">
    <tr>
        <td align="right" colspan="2"></td>
    </tr>';
$html.=$element;		
$html.='</table>    
<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
    <tr>        
        <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
	<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
        <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
        <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>        
    </tr>';    
    if(!empty($Def_record_recomm))
    {
	$recommCounter=0;
        foreach($Def_record_recomm as $Def_record_recomm) {
		$recommCounter++;
        $html.='<tr>        
            <td valign="top">'.$recommCounter.'.</td>
	    <td valign="top">'.$Def_record_recomm['Code']['code'].'</td>
            <td valign="top">'.$Def_record_recomm['Code']['description'].'</td>
            <td valign="top">'.$Def_record_recomm['Deficiency']['recommendation'].'</td>        
        </tr>';
        }
    }
    else
    {
        $html.='<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
    }    
$html.='</table>';


//Missed Items Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
                <td colspan="2" align="center" style="border:none;"><span style="font-size:36px; color:green;"><b>Missed Items</b></span></td>                
        </tr>      
</table>

<table width="100%" cellpadding="0" cellspacing="0">';
$html.=$element;
$html.='</table>
<table width="100%" class="qtytable" border="1">
        <tr>
                <td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Sr. No.</td>
		<td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Device Name</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Surveyed</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Serviced</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Passed</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Deficient</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Recommended</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Missed</td>
        </tr>
        <tr style="background-color:#666; color:#fff; font-weight:bold">
                <td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td>
        </tr>';
	$missCounter=0;
        foreach($servicedata as $serviceData){
		$missCounter++;
        $html.='<tr>
                <td style="text-align:center;">'.$missCounter.'.</td>
		<td style="text-align:center;">'.$serviceData['Service']['name'].'</td>
                <td style="text-align:center;">'.$serviceData['ScheduleService']['amount'].'</td>
                <td style="text-align:center;">'.$this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>';                
                if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id'])<$serviceData['ScheduleService']['amount']){
                     $missing = $serviceData['ScheduleService']['amount'] - $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
                }else{
                     $missing = "0";
                }                
                $html.='<td style="text-align:center;">'.$missing.'</td>
        </tr>';        
        }  
$html.='</table>';
$html.='<table style="color: green">';	
	$missingData = $this->Common->missingTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
	$missingText=$missingData['ExplanationText']['missing_reason'];
	$missingfile=$missingData['ExplanationText']['missing_file'];
	$missingText = (($missingText!="")?$missingText:'No explanation added');
	if(!empty($missingfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$missingfile)){
		$showMissfile = 'Attachment';
	}else{
		$showMissfile='No Attachment';
	}
	$html.='<tr>								
			<td colspan="6">Missed Items Explanation: '.$missingText.'</td>
		</tr>
		<tr>								
			<td colspan="6">Missed Items Attachment: '.$showMissfile.'</td>
		</tr>
</table>';
/*<table>
        <tr><td colspan="6">&nbsp;</td></tr>
        <tr>
                <td colspan="6">'.$spResult['Company']['name'].'</td>
        </tr>
        <tr>
               <td colspan="6">'.$spResult['Company']['address'].'</td>
        </tr>
        <tr>
               <td colspan="6">'.$spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'].'</td>
        </tr>		 
</table>';*/

//Additional Items Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
                <td colspan="2" align="center" style="border:none;"><span style="font-size:36px; color:purple;"><b>Additional Items</b></span></td>                
        </tr>      
</table>

<table width="100%" cellpadding="0" cellspacing="0">';
$html.=$element;
$html.='</table>

<table width="100%" class="qtytable" border="1">
        <tr>
                <td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Sr. No.</td>
		<td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Device Name</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Surveyed</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Serviced</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Passed</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Deficient</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Recommended</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Additional</td>
        </tr>
        <tr style="background-color:#666; color:#fff; font-weight:bold">
                <td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td>
        </tr>';
	$addCounter=0;
        foreach($servicedata as $serviceData){
		$addCounter++;
        $html.='<tr>
                <td style="text-align:center;">'.$addCounter.'.</td>
		<td style="text-align:center;">'.$serviceData['Service']['name'].'</td>
                <td style="text-align:center;">'.$serviceData['ScheduleService']['amount'].'</td>
                <td style="text-align:center;">'.$this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>';
                if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']) > 0) {
                    $additional = $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']) -  $serviceData['ScheduleService']['amount'] ;
                }else{
                    $additional = "0";
                }                
                $html.='<td style="text-align:center;">'.$additional.'</td>
        </tr>';        
        } 
$html.='</table>';
$html.='<table style="color: purple">';	
	$additionalData = $this->Common->additionalTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
	$additionalText=$additionalData['ExplanationText']['additional_reason'];
	$additionalfile=$additionalData['ExplanationText']['additional_file'];
	$additionalText = (($additionalText!="")?$additionalText:'No explanation added');
	if(!empty($additionalfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$additionalfile)){
		$showAdditionalfile = 'Attachment';
	}else{
		$showAdditionalfile='No Attachment';
	}
	$html.='<tr>								
			<td colspan="6">Additional Items Explanation: '.$additionalText.'</td>
		</tr>
		<tr>								
			<td colspan="6">Additional Items Attachment: '.$showAdditionalfile.'</td>
		</tr>
</table>';
/*<table>
        <tr><td colspan="6">&nbsp;</td></tr>
        <tr>
                <td colspan="6">'.$spResult['Company']['name'].'</td>
        </tr>
        <tr>
               <td colspan="6">'.$spResult['Company']['address'].'</td>
        </tr>
        <tr>
               <td colspan="6">'.$spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'].'</td>
        </tr>		 
</table>';*/

//Signature Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
                <td colspan="2" align="center" style="border:none;"><span style="font-size:36px;"><b>Signature Page</b></span></td>                
        </tr>	   
</table>

<table width="100%" cellpadding="0" cellspacing="0">';
$html.=$element;
$html.='</table>

<table width="100%">
        <tr>
                <td style="border:none;"><b>Inspector Signature</b></td>
                <td style="border:none; vertical-align:top;" align="right"><b>Client Signature</b></td>	  
        </tr>
        <tr>
            <td style="border:none">';
                $name=$this->Common->getInspectorSignature($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id']);
                $html.='<img src="/app/webroot/img/signature_images/'.$name.'">';                
                $html.='</td>
            <td style="border:none;" align="right">&nbsp;</td>    
        </tr>
        <tr>
                <td style="border:none;">';
                $dateCreated=$this->Common->getSignatureCreatedDate($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id']);		  
                if($dateCreated!='empty'){
                        $html.=$this->Common->getClientName($record['FirealarmReport']['inspector_id'])."<br/><br/>";
                        $html.=$time->Format('m-d-Y',$dateCreated);
                }                
                $html.='</td>
                <td align="right" style="border:none;">';
		//$dateCreated=$this->Common->getSignatureCreatedDate($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id']);		  
                //if($dateCreated!='empty'){
                        $html.=$this->Common->getClientName($record['FirealarmReport']['client_id'])."<br/><br/>";
                        //$html.=$time->Format('m-d-Y',$dateCreated);
                //}                
                $html.='</td>
        </tr>
</table>';
/*<table>
	<tr><td colspan="6">&nbsp;</td></tr>
	<tr>
	  <td colspan="6">'.$spResult['Company']['name'].'</td>
	</tr>
	<tr>
	       <td colspan="6">'.$spResult['Company']['address'].'</td>
	</tr>
	<tr>
	       <td colspan="6">'.$spResult['Country']['name'].','.$spResult['State']['name'].','.$spCityName.'</td>
	</tr>		 
</table>';*/

//Cert Attached Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px;"><b>Certification</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: Firealarm</b></div>
<br/>
<table width="100%">';
$html.=$element;
$html.='</table>
<table class="tbl input-chngs1" width="100%" >
	<tr>
                <td>&nbsp;</td>
        </tr>
        <tr>
                <td align="left" style="text-align:left">';
                
                $certs = $this->Common->getAllSchCerts($scheduledata['Schedule']['id']);						
                if(sizeof($certs)>0){                
                    $cr=1;
                    foreach($certs as $cert)
                    {
                        $html.='<b>'.$cr.'.'.$cert['ReportCert']['ins_cert_title'].'</b>';
                        $html.='<br/>';                    
                        $cr++;
                    }               
                }                
                $html.='</td>
        </tr>	
</table>';

//Quotes Page
$html.='<hr style="color:#fff"/>';        
$html.='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<div style="text-align:center;width:100%;font-size:36px;"><b>Quotes</b></div>
<div style="float:left;font-size:16px;padding-left:30px;margin-top:5px;"><b>Reference: Firealarm</b></div>
<br/>
<table width="100%">';
$html.=$element;
$html.='</table>
<table class="tbl input-chngs1" width="100%" >                
        <tr>
                <td align="left" style="text-align:left">
                        <div><b>Quote Form from SP</b></div>';                        
                        if(isset($chkRecord) && !empty($chkRecord)){
                                $html.=$chkRecord['Quote']['title'];
                        }else{
                                $html.='No Quote have been submitted by Service Provider.';
                        }                        
                $html.='</td>
        </tr>
        <tr>
                <td>&nbsp;</td>
        </tr>
        <tr>
                <td align="left" style="text-align:left">
                <div><b>Client\'s Response to Quote Form</b></div>';                
                $quotedocs = $this->Common->getQuoteDoc($_REQUEST['reportID'],$_REQUEST['clientID'],$_REQUEST['spID'],$_REQUEST['serviceCallID']);                
                if(isset($quotedocs) && !empty($quotedocs)){
                        if($quotedocs['Quote']['client_response']=='a'){
                                $status = 'Accepted';
                        }else if($quotedocs['Quote']['client_response']=='d'){
                                $status = 'Denied';
                        }else if($quotedocs['Quote']['client_response']=='n'){
                                $status = 'No wish to fix the deficiencies';
                        }else{
                                $status = 'Pending';
                        }                        
                        $html.='<div>Status: '.$status.'</div>';
                        if($quotedocs['Quote']['client_response']=='a'){
                        $html.='<div class="certificates-hdline" style="text-align: left;">';
                            $html.='Signed Quote Form';
                        $html.='</div>';
                        }else if($quotedocs['Quote']['client_response']=='d'){
                        $html.='<div class="certificates-hdline" style="text-align: left;">';
                            $html.='Work Orders';
                        $html.='</div>';
                        }
                        
                }else{
			$html.='NA';
		}
                $html.='</td>
        </tr>
</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
//$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;


$delimiter = '<hr style="color:#fff"/>';
$chunks    = explode($delimiter, $html);
$cnt       = count($chunks);

for ($i = 0; $i < $cnt; $i++) {
    $tcpdf->writeHTML($chunks[$i], true, 0, true, 0);

    if ($i < $cnt - 1) {
        $tcpdf->AddPage();
    }
}

// Reset pointer to the last page
$tcpdf->lastPage();


// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Firealarm_Report.pdf', 'I');die;
?>