<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));    
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    //echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('languages/jquery.validationEngine-en.js','jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js')); 
    //echo $this->Html->script(array('ajaxuploader/fileuploader'));
?>

<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript">
 jQuery(function(){
    jQuery(".calender").datepicker({ dateFormat: 'mm/dd/yy',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });
    jQuery("#SprinklerReportreport").validationEngine();
 });
 function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
</script>
<script type="text/javascript">
$(document).ready(function(){
    jQuery(".ajax").colorbox();
    jQuery(".inline").colorbox({inline:true, width:"50%"});	

	
    <?php	
        if(isset($this->data['SprinklerReport']['finish_date']) && ($this->data['SprinklerReport']['finish_date'] != "")){ ?>
	    for(i = 0; i < document.getElementById('SprinklerReportreport').length; i++){
		var formElement = document.getElementById('SprinklerReportreport').elements[i];	
		formElement.disabled = true;
	    }
    <?php }?>

});
$(function(){
	// Accordion
	$("#accordion").accordion({ header: "h3" });
	//hover states on the static widgets
	$('#dialog_link, ul#icons li').hover(
		function() { $(this).addClass('ui-state-hover'); },
		function() { $(this).removeClass('ui-state-hover'); }
	);
});
		
    
function createUploader(){	    
    var uploader = new qq.FileUploader({
    element: document.getElementById('demo'),
    listElement: document.getElementById('separate-list'),
    action: '/reports/ajaxuploadAttachment',		
    debug: true,
    onComplete: function(id, fileName, responseJSON){
        jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
        jQuery("#uploaded_picture").html(responseJSON.attachment);
    }
    });           
}        
window.onload = createUploader;
		
/*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
function checkUploadedAttachment(){
    var uploadVal = jQuery("#uploadattachment").val();
    if(uploadVal != ""){
        return true;
    }else{
        alert("Please upload a file");
        return false;
    }
}
function checkNotification(){
    var notifyVal = jQuery("#NotificationNotification").val();
    if(notifyVal != ""){
        return true;
    }else{
        alert("Please add notification");
        return false;
    }
} 
 
function RemoveAttachment(attachId){
    jQuery.ajax({
	     type : "GET",
	     url  : "/reports/delattachment/" + attachId,                
	     success : function(opt){
		 if(opt == 1){
		    jQuery('#p_'+attachId).remove();
		    jQuery("#deleteFlashmessage").show();		       
		 }
	     }
     });
}

function confirmattach(attachId){
    var r = confirm("Are you sure you want to delete this record");
    if (r==true){
        RemoveAttachment(attachId);    
    }
    else{
        return false;
    }
}
function RemoveNotifier(notifierId){
    jQuery.ajax({
        type : "GET",
        url  : "/reports/delnotifier/" + notifierId,                
        success : function(opt){
	    if(opt == 1){
		jQuery('#pnotify_'+notifierId).remove();
		jQuery("#deleteFlashmessage").show();		       
	    }
        }
    });
}

function confirmnotifier(notifierId){
    var r = confirm("Are you sure you want to delete this record");
    if (r==true){
        RemoveNotifier(notifierId);    
    }
    else{
        return false;
    }
}

function chkStatus(obj){
	response = jQuery(obj).val();
	currentObj = jQuery(obj);
	//selectBox  = jQuery(obj).parent('td').parent('tr').find('select:first');	
	//rowId = jQuery(obj).parent('td').parent('tr').children('td:first').html();
	//locationData = selectBox.parent('td').prev('td').children('input').val();
	//modelData = selectBox.parent('td').next('td').children('input').val();
	//serviceId = selectBox.val();
	rowId = '0';
	locationData = '';
	modelData = '';
	serviceId = '0';
	mfdDate = '';
	reportId = '<?php echo $_REQUEST['reportID'];?>';
	clientId = '<?php echo $_REQUEST['clientID'];?>';
	spId = '<?php echo $_REQUEST['spID'];?>';
	servicecallID = '<?php echo $_REQUEST['serviceCallID'];?>';
	if(response=='No'){		
		if(!confirm("It will delete the added deficiency. Do you want to continue?")){
			jQuery(obj).val("Yes");
			return false;
		}else{
			data = 'reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId;
			jQuery.ajax({
				'url':'/inspector/reports/deleteDeficiency/',
				'data':data,
				'type':'post',
				'success':function(msg){
					currentObj.css('color','#666666');
					return false;
				}
			});
		}
		return false;
	}	
	currentObj.css('color','red');
	PopupCenter('/inspector/reports/fillDeficiency?reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId+'&locationData='+locationData+'&modelData='+modelData+'&mfdDate='+mfdDate,'','600','400');
}
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}
    .ui-accordion-content{}
    .message{text-align:center;color:#54A41A;font-weight:bold;}
</style>
<div id="content" >
    <div class='message'><?php echo $this->Session->flash();?></div>
    <div id="box">
	<div style="width: 100%">
	    <?php //echo $html->link($html->image("pdf.png",array('title'=>'Print Preview')), 'reportPreview?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
	    <?php echo $html->link($html->image("fineimages/pdf_preview.png",array('title'=>'Print Preview')), 'reportPreview/Sprinkler?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
	</div>    
        <h3 align="center" >Sprinkler Inspection Report</h3>
        <h4 class="table_title">Agreement and Report of Inspection</h4>
	<?php $action='sprinklerinspection?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'];?>
	<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>$action,'name'=>'SprinklerReportreport','id'=>'SprinklerReportreport')); ?>
        <table cellpadding="0" cellspacing="0" border="0" class="table_new">
	    <tr>
		<td>
		    <b>Days Left for report Submission: <?php echo $daysRemaining;?></b>
		</td>
                <td align="right">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
			    <td width="100%" align="right">
				<?php
				    $options=array("monthly"=>"Monthly","Quarterly"=>"Quarterly","semi_annual"=>"Semi-Annual","Annual"=>"Annual");
				    $attributes=array('legend'=>false,'separator'=>'&nbsp;&nbsp;','class'=>'validate[required]');
				    echo $this->Form->radio('SprinklerReport.inspection_type',$options,$attributes);
				?>				    
			    </td>                                
                        <tr>
                    </table>
                </td>
            </tr>
            <tr>
	        <th colspan="2" align="left">Client information</th>
	    </tr>
            <tr>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="table_label"><strong>Building Information</strong></td>
                            <td><?php echo $clientResult['User']['client_company_name'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $clientResult['User']['address'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>City/State/Zip:</strong></td>
                            <td> <?php echo $clientResult['User']['city'].', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Country: </strong></td>
                            <td><?php echo $clientResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="table_label"><strong>Contact: </strong></td>
                            <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Phone:</strong></td>
                            <td><?php echo $clientResult['User']['phone'];?></td>
                        </tr>                               
                        <tr>
                            <td class="table_label"><strong>Email: </strong></td>
                            <td> <?php echo $clientResult['User']['email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
	    <tr>
		<th colspan="2" align="left">Site Information</th>
	    </tr>
            <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Site Name:</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <td> <?php echo $siteaddrResult['SpSiteAddress']['site_city'].', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Country: </strong></td>
                            <td><?php echo $siteaddrResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Contact: </strong></td>
                            <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                        </tr>                               
                        <tr>
			    <td class="table_label"><strong>Email: </strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2" align="left">Service Provider Info</th></tr>
            <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Name:</strong></td>
                            <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $spResult['User']['address'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <td><?php echo $spResult['User']['city'].', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Country: </strong></td>
			    <td><?php echo $spResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Contact: </strong></td>
                            <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
			    <td><?php echo $spResult['User']['phone'];?></td>
                        </tr>                               
                        <tr>
			    <td class="table_label"><strong>Email: </strong></td>
			    <td><?php echo $spResult['User']['email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div style="border:4px solid #000; padding:10px; margin:10px">
	    <?php echo $texts;?>
        </div>
        <div style="float:right;padding-right:10px;">
	    <?php
		//$permissionfill=$this->Common->checkPermission(1,$session_inspector_id);    
		//if($session_inspector_id!=$schedule['lead_inspector_id'])
		//if($permissionfill!=1)
		//{
            ?>
		    <!--<a href="javascript:void(0)" title="Add Attachment"  class="linktoreport" onclick="alert('<?php //echo $permissionfill;?>')">Add Attachment</a><span style='vertical-align:bottom'><?php echo $html->image('icon_attachment.gif');?></span>-->
                                        
            <?php //} else {
		    //echo $html->link('Add Attachment','#inline_content_attachment',array('class' => 'inline'));?><?php //echo $html->image('icon_attachment.gif');?>&nbsp;&nbsp;&nbsp;
            <?php //}?>
            <?php
		//$permissionfillnotify=$this->Common->checkPermission(2,$session_inspector_id);    
		//if($session_inspector_id!=$schedule['lead_inspector_id'])
		//if($permissionfillnotify!=1)
		//{
            ?>
                    <!--<a href="javascript:void(0)" title="Add Notification"  class="linktoreport" onclick="alert('<?php //echo $permissionfillnotify;?>')">Add Notification</a><span style='vertical-align:bottom'><?php echo $html->image('icon_notify.gif');?></span>-->
            <?php //} else {
		    //echo $html->link('Add Notifier',"#inline_content_notification",array('class' => 'inline'));?><?php //echo $html->image('icon_notify.gif');?>
            <?php //}?>                         
        </div>
	<?php echo $form->input('SprinklerReport.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
	<?php echo $form->input('SprinklerReport.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
	<?php echo $form->input('SprinklerReport.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
	<?php echo $form->input('SprinklerReport.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
	<?php echo $form->input('SprinklerReport.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
	<?php echo $form->input('SprinklerReport.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportAlarm.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportConnection.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportControlvalve.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportDrysystem.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportGeneral.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportPiping.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportSpecialsystem.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportWatersupply.id',array('type'=>'hidden')); ?>
	<?php echo $form->input('SprinklerReportWetsystem.id',array('type'=>'hidden')); ?> 
        <br/>
	<table cellpadding="0" cellspacing="0" border="0" class="table_new">			    			    
	    <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Date of Work</strong></td>
			    <td><?php echo $form->input('SprinklerReport.date_of_work',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender validate[required]','readonly'=>'readonly')); ?></td>
			</tr>				      
			<tr>
			    <td class="table_label"><strong>Start Date of the Service</strong></td>
			    <td><?php echo $form->input('SprinklerReport.start_date_service',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender validate[required]','readonly'=>'readonly')); ?></td>
			</tr>
			<tr>
			    <td class="table_label"><strong>End Date of the Service</strong></td>
			    <td> <?php echo $form->input('SprinklerReport.end_date_service',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender validate[required]','readonly'=>'readonly')); ?></td>
			</tr>
			<tr>
			    <td class="table_label"><strong>Date of Performed Work </strong></td>
			    <td><?php echo $form->input('SprinklerReport.date_performed_work',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender validate[required]','readonly'=>'readonly')); ?></td>
			</tr>
		    </table>
		</td>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Deficiency yes or no </strong></td>
			    <td>
				<?php
				    $data1=array("Yes"=>"Yes","No"=>"No");
                                    echo $form->select('SprinklerReport.deficiency',$data1,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:190px;','onchange'=>'chkStatus(this);'));
				?>
			    </td>
			</tr>
			<tr>
			    <td class="table_label"><strong>Recommendations yes or no</strong></td>
			    <td>
				<?php
				    echo $form->select('SprinklerReport.recommendations',$data1,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:190px;'));
				?>
			    </td>
			</tr>
			<tr>
			    <td class="table_label"><strong>Inspection 100% complete and Passed</strong></td>
			    <td>
				<?php
				    echo $form->select('SprinklerReport.inspection',$data1,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:190px;'));
				?>
			    </td>
			</tr>				      
		    </table>
		</td>
	    </tr>    
	</table>
	<div id="accordion">
	    <div>
		    <h3><a href="#" id=""> 1. GENERAL(To be answered by Customer.)</a></h3>
		    <div >
		    <?php     echo $this->element("sprinklerinspection/sp_general");    ?>
		    </div>
	    </div>
		   
	    <div>
		    <h3><a href="#" id="65">2. CONTROL VALVES</a></h3>
		    <div>
		     <?php echo $this->element("sprinklerinspection/sp_control"); ?>
	    </div>
		    
	    <div>
		    <h3><a href="#">3. WATER SUPPLIES</a></h3>
		    <div>
			     <?php  echo $this->element("sprinklerinspection/sp_watersupplies");   ?>	</div>
	    </div>
	    <div>
		    <h3><a href="#"> 4. TANKS, PUMPS, FIRE DEPT. CONNECTION</a></h3>
		    <div>
		      <?php echo $this->element("sprinklerinspection/sp_tankconnection");   ?>  
	    </div>
		    
	    <div>
		    <h3><a href="#" id="42">5. WET SYSTEMS</a></h3>
		    <div>
		     <?php  echo $this->element("sprinklerinspection/sp_wet");   ?>
		    </div>
	    </div>
		    
		
	    <div>
		    <h3><a href="#" id="43">6. DRY SYSTEM</a></h3>
		    <div>
		      <?php  echo $this->element("sprinklerinspection/sp_dry");   ?>
		    </div>
	    </div>
		  
	    <div>
		    <h3><a href="#">7. SPECIAL SYSTEMS</a></h3>
		    <div>
		     <?php  echo $this->element("sprinklerinspection/sp_system");   ?>
		    </div>
	    <div>
		    <h3><a href="#">8. ALARMS</a></h3>
		    <div>
		     <?php  echo $this->element("sprinklerinspection/sp_alarm");   ?>
	    </div>
	    <div>
		    <h3><a href="#">9. SPRINKLERS - PIPING</a></h3>
		    <div>
		     <?php  echo $this->element("sprinklerinspection/sp_piping");   ?>
	    </div>
    </div>             
	    <table>
		<!--<tr><td >&nbsp;</td></tr>
		<tr>
		    <td align="center">
			<?php
			    //$permissionfilldef=$this->Common->checkPermission(6,$session_inspector_id);    
			    //if($session_inspector_id!=$schedule['lead_inspector_id'])
			    //if($permissionfilldef!=1)
			    //{
			?>
				<a href="javascript:void(0)" title="Add Deficiency"  class="linktoreport" onclick="alert('<?php echo $permissionfilldef;?>')"><span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span></a>		
			<?php //} else {
				//echo $html->link('<span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span>',array('controller'=>'reports','action'=>'defAndRec?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),array('class' => 'ajax','escape'=>false),false); ?> 
			<?php //} ?>
		    </td>
	    </tr>-->
		<tr><td>&nbsp;</td></tr>
		<tr>      
		    <td>
			<?php
			    if(isset($result['finish_date']) && ($result['finish_date'] != "")){
				echo $html->link('<span>FINISH</span>','javascript:void(0)',array('escape'=>false,'class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'sprinklerView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');"));
			    } else { ?>
				<span class="blue-btn" style="float:right;">
				    <input type="submit" id="linkFinish", name="finish" value="FINISH" onclick="return confirm('Are you sure that you want to finish this report? after clicking ok you will not be able to further edit this report.')" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/>
				</span>		
			<?php } ?>
			<?php echo $html->link('<span>REVIEW</span>','javascript:void(0);',array('escape'=>false,'id'=>'reviewanswer','class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'sprinklerView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');")); ?>
			<span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" name="save" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span>
		    </td>
		</tr>
		<?php  if(isset($this->data['SprinklerReport']['finish_date']) && ($this->data['SprinklerReport']['finish_date'] != "")){ ?>
		<tr>
		    <td align="center"><font color="red" size="3px">The form is disabled, as you have finished the report. </font></td>
		</tr>
		<?php } ?>
	    </table>
	    <?php echo $form->end(); ?>
	</div>
    </div>
    <!-- MAIN CONTENT ENDS AND ATTACHMENT NOTIFICATION DIV -->

    <!-- This contains the hidden content for inline calls -->
    <div style='display:none'>	
	<div id='inline_content_attachment' style='padding:10px; background:#fff;'>
	    <?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'attachment','name'=>'attachment','id'=>'attachment',"onsubmit"=>"return checkUploadedAttachment();")); ?>
	    <?php echo $form->input('Attachment.attach_file', array('id'=>'uploadattachment','type'=>'hidden','value'=>'')); ?>
	    <?php echo $form->input('Attachment.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
	    <?php echo $form->input('Attachment.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
	    <?php echo $form->input('Attachment.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
	    <?php echo $form->input('Attachment.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
	    <?php echo $form->input('Attachment.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
	    <p><strong>Upload Attachment</strong></p>
	    <p>
		<div id="demo"></div>
		<ul id="separate-list"></ul>
	    </p>
	    <p><div id="uploaded_picture" style="text-align:center;padding-left:90px;"></div></p>
	    <p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
	    <?php if(!empty($attachmentdata)){ ?>
	    <p><strong>Attachments</strong></p>
	    <?php } ?>
	    <?php foreach($attachmentdata as $attachdata){ ?>
	    <p id="p_<?php echo $attachdata['Attachment']['id'] ?>"><?php echo $html->link($attachdata['Attachment']['attach_file'],array("controller"=>"reports","action"=>"download",$attachdata['Attachment']['attach_file']),array()).'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmattach('<?php echo $attachdata['Attachment']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
	    <?php } ?>
	    <?php echo $form->end(); ?>
	</div>	
    </div>
    <!-- This contains the hidden content for inline calls -->
    <div style='display:none'>		
	<div id='inline_content_notification' style='padding:10px; background:#fff;'>
	    <?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'notification','name'=>'notification','id'=>'notification',"onsubmit"=>"return checkNotification();")); ?>
	    <?php echo $form->input('Notification.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
	    <?php echo $form->input('Notification.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
	    <?php echo $form->input('Notification.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
	    <?php echo $form->input('Notification.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
	    <?php echo $form->input('Notification.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
	    <p><strong>Notification</strong></p>
	    <p>
		<?php echo $form->input('Notification.notification',array('type'=>'textarea','id'=>'ContentMessage','rows'=>5,'cols'=>40,'label'=>false,'onKeyUp' =>"return limit('ContentMessage',500);")); ?>
	    </p>
	    <p>
		<div id="essay_Error" class='error'></div>
		<div>
		    <small>&nbsp;<label id='limitCounter'>0</label>&nbsp;characters entered&nbsp;|&nbsp;<label id='limitCounterLeft'>500</label>&nbsp;characters remaining</small>
		</div>   
            </p>
	    <p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
	    <?php if(!empty($notifierdata)){ ?>
		<p><strong>Notifiers</strong></p>
	    <?php } ?>
	    <?php foreach($notifierdata as $notifydata){ ?>
		<p id="pnotify_<?php echo $notifydata['Notification']['id'] ?>"><?php echo $notifydata['Notification']['notification'].'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmnotifier('<?php echo $notifydata['Notification']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
	    <?php } ?>
	    <?php echo $form->end(); ?>
	</div>		
    </div>
</div>    