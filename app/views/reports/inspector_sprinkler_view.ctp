    <?php
	echo $this->Html->script('accordian.pack');
    ?>
    <style type="text/css">
    /*Date Jan 18, 2013*/
    .summary-tbl2 p {
	float: left;
	margin-bottom: 0;
	margin-left: 5px;
	width: 10%;
	font-size: 10px;
	padding-left: 7px;
    }
    .headingtxt{
	background-color: #2B92DD;
	color: #fff;
	float: left;
	font-size: 14px;
	font-weight: bold;
	height: 14px;
	padding: 5px;
	width: 100%;
	margin-bottom:10px;
	text-align:center;
    }
    .lbtxt{text-align:right;width:20%;}    
    .input-chngs2 input{
	    margin:0px;
	    color: #666666;
	    font-size: 12px;
    }
    .txt{
	font-size:12px;
    }
    /*Date Jan 18, 2013*/
    
    #basic-accordian{
	    border:1px solid #EEE;
	    padding:0px;
	    width:920px;
	    position:absolute;
	    top:10%;
	    margin-left:0px;
	    z-index:2;	
    }
    .accordion_headings{
	    padding:5px;
	    background:url('../../img/newdesign_img/nav_bg.png') bottom;
	    color:#FFFFFF;
	    border:1px solid #FFF;
	    cursor:pointer;
	    font-weight:bold;
	    font-size:12px;
	    border-radius:6px 6px 0 0 ;
    }
    .accordion_headings:hover{
	    background:url('../../img/newdesign_img/nav_bg.png') top;
    }
    .accordion_child{
	    padding:15px;
	    background:#EEE;
    }
    .header_highlight{
	    background:url('../../img/newdesign_img/nav_active.png') !important;
    }
    .tab_container *{
	    float:left;
	    width:118px;
    }
    .demoHeaders { margin-top: 2em; }
    td{ font-size: 12px;}
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}
    .ui-accordion-content{}
    
    .message{text-align:center;color:#54A41A;font-weight:bold;}    
    .red{ color: red;  }
    .cpage td{ border:none !important; }
    
    /*Date Jan 18, 2013*/
    .tdLabel {font-size:16px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
	margin-right: 3px;
	width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#CCCCCC;}
    #accordionlist { text-align:left; }
    .qtytable td{ border:1px solid black;}
    .qtytable{ border-collapse: collapse;}
    .blue_bg {background:url("../../img/newdesign_img/nav_active.png") !important; color:#fff !important; font-weight:bold}    
    
    /*Date Jan 18, 2013*/
    
    </style>    
    <body onload="new Accordian('basic-accordian',5,'header_highlight');">
	<div id="content">     
	    <div id="box">	      
		<h3  align="center">Sprinkler Inspection Report</h3>
		<br/>		
	    </div>
	    <div id="basic-accordian" ><!--Parent of the Accordion-->
		<div class="tab_container">
		    <div id="test1-header" class="accordion_headings header_highlight" >Cover Page</div>
		    <div id="test2-header" class="accordion_headings" >Report</div>
		    <div id="test3-header" class="accordion_headings" >General</div>
		    <div id="test4-header" class="accordion_headings" >Control</div>
		    <div id="test5-header" class="accordion_headings" >Water</div>
		    <div id="test6-header" class="accordion_headings" >Connection</div>
		    <div id="test7-header" class="accordion_headings" >Wet</div>
		    <div id="test8-header" class="accordion_headings" >Dry</div>
		    <div id="test9-header" class="accordion_headings" >Special</div>
		    <div id="test10-header" class="accordion_headings" >Alarms</div>
		    <div id="test11-header" class="accordion_headings" >Sprinklers</div>
		    <!--<div id="test12-header" class="accordion_headings" >Attachments</div>-->
		    <div id="test13-header" class="accordion_headings" >Deficiencies</div>
		    <div id="test14-header" class="accordion_headings" >Recommendation</div>
		    <div id="test15-header" class="accordion_headings" >Signature Page</div>
		    <div id="test16-header" class="accordion_headings" >Certification</div>
		    <div id="test17-header" class="accordion_headings" >Quotes</div>
		</div>
		<div style="float:left;width:100%;">
		    <div id="test1-content">
			<div class="accordion_child">
			    <table class="tbl input-chngs1" width="100%" >
				<tr>
					<td colspan="2" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
				</tr>
				<tr>
				    <td align="right" width="55%" style="border:none;"><span style="font-size:15px; font-weight:bold;">Cover Page</span></td>
				    <td align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reports/coverPagePrintCommon/SprinklerReport?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Cover Page Preview'));?></td>
				</tr>
				<tr>
				    <td colspan="2">
					<table width="100%" class="cpage">
					    <tr>
						<td align="center"><?php echo $this->Html->image('company_logo/'.$spResult['Company']['company_logo']); ?></td>
					    </tr>
					    <tr>
						<td align="center" class="Cpage" style="font-size:15px;letter-spacing: 4px;"><b><?php echo $spResult['Company']['name'];  ?></b></td>
					    </tr>
					    <tr>
						<td align="center" class="Cpage" style="font-size:13px">
						<?php $arr=explode(',',$spResult['Company']['interest_service']);		
						    foreach($arr as $key=>$val):
							foreach($reportType as $key1=>$val1):
							    if($val== $key1)
							    { 
								if($key1 == 9){ echo $spResult['Company']['interest_service_other']; }else{ echo $val1;} ;?><span class="red"> * </span>			 
						    <?php    }			
							endforeach;			
						    endforeach;
						    ?>
						</td>
					    </tr>		     
					    <tr>
						<td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
					    </tr>
					    <tr>
						<td align="center" class="Cpage" style="font-size:18px">Sprinkler Inspection Report</td>
					    </tr>
					    <tr>
						<td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
					    </tr>
					    <tr>
						<td align="center" class="Cpage" style="font-size:36px">Schedule Frequency: <?php echo $schFreq;?></td>
					    </tr>
					    <tr>
						<td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
					    </tr>
			  <tr>
			    <td align="right" class="Cpage" >Date:<?php if($record['SprinklerReport']['finish_date']!= ''){
				echo $time->Format('m/d/Y',$record['SprinklerReport']['finish_date']); } ?></td>
			  </tr>
			  <tr>
			    <td align="center" class="Cpage" ><i>Prepared for:</i></td>
			  </tr>
			  <tr>
			    <td align="center" class="Cpage" >&nbsp;</td>
			  </tr>
			  
			    <tr>
				<td align="center" class="Cpage" ><?php echo $clientResult['User']['client_company_name'] ?></td>
			</tr>	  
			<tr>
				<td align="center" class="Cpage" ><?php echo $clientResult['User']['address'] ?></td>
			</tr>
			<tr>
				<?php $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];?>
				<td align="center" class="Cpage" ><?php echo $cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']); ?></td>
			</tr>
			<tr>
				<td align="center" class="Cpage" ><?php echo $clientResult['User']['zip']; ?></td>
			</tr>
		    
			<tr>
				<td align="center" class="Cpage" >&nbsp;</td>
			</tr>
			<tr>
				<td align="center" class="Cpage" ><b>Site Address Info:</b></td>
			</tr>
			    <tr>
					 <td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_name'] ?></td>
			    </tr>
			    <tr>
					 <td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['site_phone']; ?></td>
			    </tr>
			    <tr>
					 <td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_address'] ?></td>
			    </tr>
			    <tr>
					 <?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
					<td align="center" class="Cpage" ><?php echo $siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']); ?></td>
			    </tr>
			    <tr>
					 <td align="center" class="Cpage" ><?php echo $siteaddrResult['SpSiteAddress']['site_zip']; ?></td>
			    </tr>
	      
			  
			  <tr>
			    <td align="left" class="Cpage" >Prepared By:</td>
			  </tr>
			  <tr>
			    <td align="left" class="Cpage">&nbsp;</td>
			  </tr>  
			  <tr>
			    <td align="left" class="Cpage"><?php echo $spResult['Company']['name'];  ?></td>
			  </tr>
			  <tr>
			    <td align="left" class="Cpage"><?php echo $spResult['Company']['address'];  ?></td>
			  </tr>
			  <tr>
			    <?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
			    <td align="left" class="Cpage"><?php echo $spResult['Country']['name'].','.$spResult['State']['name'].','.$spCity;  ?></td>
			  </tr>		      
			  <tr>
			    <td align="left" class="Cpage">&nbsp;</td>
			  </tr>  			 
			  <tr>
			    <td align="left" class="Cpage" style="font-size:11px">&nbsp;</td>
			  </tr>
			  
			</table>
		    </td>
		</tr>
	    </table>    
	</div>
      </div>
      
      <div id="test2-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Report</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/reportPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Report Page Preview'));?></td>
		</tr> 
	    </table>
	    <table cellpadding="0" cellspacing="0" border="0" class="table_new" width="100%">
		<tr>
		   <td colspan="2" align="right">
		      <table cellspacing="0" cellpadding="0" border="0">
			 <tr>
			    <td width="32%" align="right"><b>Inspection Type: <?php echo ucfirst($record['SprinklerReport']['inspection_type']);?></b></td>			
			 </tr>
		      </table>
		   </td>
		</tr>
	    </table>
	    <?php //$commonReportPagePart = $this->element('reports/report_page_common_view');?>
	    <?php //echo $commonReportPagePart;?>
	    <table class="tbl input-chngs1" width="100%" style="margin-left:0px;">	
		<tr><th colspan="2" align="left" style="background: #C6C6C6; padding:5px; text-align: left; font-size: 16px;">Client information</th></tr>
		    <tr>
		      <td valign="top">
			     <table width="100%" cellpadding="0" cellspacing="0">
			     <tr>
					<td class="table_label" width="20%"><strong>Building Information</strong></td>
					<td width="60%"><?php echo $clientResult['User']['client_company_name'];?></td>
				</tr>
			      <tr>
				<td class="table_label"><strong>Address</strong></td>
				<td><?php echo $clientResult['User']['address'];?></td>
			      </tr>
			      <tr>
				<td class="table_label"><strong>City/State/Zip:</strong></td>
				<td><?php echo $cityName.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
			      </tr>
			      <tr>
				<td class="table_label"><strong>Country: </strong></td>
				<td><?php echo $clientResult['Country']['name'];?></td>
			      </tr>
			     </table>
		      </td>
		      <td valign="top">
			  <table width="100%" cellpadding="0" cellspacing="0">
			     <tr>
					<td class="table_label" width="20%"><strong>Contact: </strong></td>
					<td width="60%"><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
				</tr>
			      <tr>
				<td class="table_label"><strong>Phone:</strong></td>
				<td><?php echo $clientResult['User']['phone'];?></td>
			      </tr>                               
			      <tr>
				<td class="table_label"><strong>Email: </strong></td>
				<td> <?php echo $clientResult['User']['email'];?></td>
			      </tr>
			     </table>
		      </td>
		    </tr>
		    
		    <tr>
			<th colspan="2">&nbsp;</th>
		    </tr>
		    <tr><th colspan="2" align="left" style="background: #C6C6C6; text-align: left; font-size: 16px;">Site Information</th></tr>
		    <tr>
		      <td valign="top">
			     <table width="100%" cellpadding="0" cellspacing="0">
			     <tr>
					<td class="table_label" width="20%"><strong>Site Name:</strong></td>
					<td width="60%"><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
				</tr>
			      <tr>
				<td class="table_label"><strong>Address</strong></td>
				<td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?></td>
			      </tr>
			      <tr>
				<td class="table_label"><strong>City/State/Zip:</strong></td>
				<td> <?php echo $siteaddrResult['SpSiteAddress']['site_city'].', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
			      </tr>
			      <tr>
				<td class="table_label"><strong>Country: </strong></td>
				<td><?php echo $siteaddrResult['Country']['name'];?></td>
			      </tr>
			     </table>
		      </td>
		      <td valign="top">
			  <table width="100%" cellpadding="0" cellspacing="0">
			     <tr>
				<td class="table_label" width="20%"><strong>Contact: </strong></td>
				<td width="60%"> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
			 </tr>
			      <tr>
				<td class="table_label"><strong>Phone:</strong></td>
				<td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
			      </tr>                               
			      <tr>
				<td class="table_label"><strong>Email: </strong></td>
				<td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
			      </tr>
			     </table>
		      </td>
		    </tr>
		     <tr>
			<th colspan="2">&nbsp;</th>
		</tr>
		 <!--<tr><th colspan="2" align="left">Service Provider Info</th></tr>
		<tr>
		  <td valign="top">
			 <table width="100%" cellpadding="0" cellspacing="0">
			 <tr>
			    <td class="table_label"><strong>Name:</strong></td>
			    <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Address</strong></td>
			    <td><?php echo $spResult['User']['address'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <td><?php echo $spResult['User']['city'].', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Country: </strong></td>
			    <td><?php echo $spResult['Country']['name'];?></td>
			  </tr>
			 </table>
		  </td>
		  <td valign="top">
		      <table width="100%" cellpadding="0" cellspacing="0">
			 <tr>
			    <td class="table_label"><strong>Contact: </strong></td>
			    <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
			    <td><?php echo $spResult['User']['phone'];?></td>
			  </tr>                               
			  <tr>
			    <td class="table_label"><strong>Email: </strong></td>
			    <td><?php echo $spResult['User']['email'];?></td>
			  </tr>
			 </table>
		  </td>
		</tr>-->
	     </table>
	    
	    
	    <table cellpadding="1" cellspacing="1" border="1" class="table_new" width="100%">			    			    
		<tr>
		  <td valign="top">
			 <table width="100%" cellpadding="1" cellspacing="0" border="0">
			 <tr>
			    <td class="table_label"><strong>Date of Work</strong></td>
			    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReport']['date_of_work']); ?></td>
			  </tr>				      
			  <tr>
			    <td class="table_label"><strong>Start Date of the Service</strong></td>
			    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReport']['start_date_service']); ?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>End Date of the Service</strong></td>
			    <td> <?php echo $time->Format('m-d-Y',$record['SprinklerReport']['end_date_service']); ?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Date of Performed Work </strong></td>
			    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReport']['date_performed_work']); ?></td>
			  </tr>
			 </table>
		  </td>
		  <td valign="top">
		      <table width="100%" cellpadding="1" cellspacing="0" border="0">
			 <tr>
			    <td class="table_label"><strong>Deficiency yes or no </strong></td>
			    <td><?php echo $record['SprinklerReport']['deficiency'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Recommendations yes or no</strong></td>
			    <td><?php echo $record['SprinklerReport']['recommendations'];?></td>
			  </tr>
			  <tr>
			    <td class="table_label"><strong>Inspection 100% complete and Passed</strong></td>
			    <td><?php echo $record['SprinklerReport']['inspection'];?></td>
			  </tr>				      
			 </table>
		  </td>
		</tr>    
	    </table>
	    
	    
	    <!--<table class="tbl">-->
		<!--<tr>
		    <td align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Report</span></td>
		    <td colspan="" align="right" style="border:none;"><!--<a href="#" class="quickFlipCta"><?php //echo $html->image('next-page-icon.jpg');?></a>--><!--</td>
		</tr>-->	    
		<!--<tr>
		  <td width="20%"><b>Date of Work</b></td>
		  <td><?php //echo $record['SprinklerReport']['date_of_work']; ?></td>
		</tr>-->
		<!--<tr>
		  <td  width="20%"><b>This inspection is </b></td>
		  <td><?php //echo $record['SprinklerReport']['inspection_type']; ?></td>
		</tr>
		<tr>
		  <td  colspan="2"><b>Note: A) All questions are to be answered Yes, No or NA. All answers are to be explained in Def/Rec section.</b></td>
		</tr>
		<tr>
		  <td colspan="2"><b>Note: B) Inspection,Testing and Maintenance</b></td>
		</tr>-->	    
		<!--<tr>
		  <td width="20%"><b >Start Date of the Service</b></td>
		  <td><?php //echo $time->Format('m-d-Y',$record['SprinklerReport']['start_date_service']); ?></td>
		</tr>
		<tr>
		  <td width="20%"><b >End Date of the Service</b></td>
		  <td><?php //echo $time->Format('m-d-Y',$record['SprinklerReport']['end_date_service']); ?></td></tr>
		<tr>
		  <td width="20%"><b>Date of Performed Work</b></td>
		  <td><?php //echo $record['SprinklerReport']['date_performed_work']; ?></td>
		</tr>
		<tr>
		  <td  width="20%"><b>Deficiency yes or no</b></td>
		  <td><?php //echo $record['SprinklerReport']['deficiency']?> </td></tr>
		<tr>
		  <td  width="20%"><b>Recommendations yes or no  </b></td>
		  <td><?php //echo $record['SprinklerReport']['recommendations']; ?></td>
		</tr>
		<tr>
		  <td  width="23%"><b>Inspection 100% complete and Passed</b></td>
		  <td><?php //echo $record['SprinklerReport']['inspection']; ?></td>
		</tr>
	    </table>-->
	</div>
      </div>
      
      <div id="test3-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">General</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/generalPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'General Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl" width="100%">		
		<tr>
		    <td colspan="2"><h3><a href="#" id=""> 1. GENERAL(Answered by Customer.)</a></h3></td>
		</tr>
		<tr>
		    <td>a. Have there been any changes in the occupancy classification, machinery or operations since the last inspection?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_a']; ?></td>
		</tr>
		    <tr>
		    <td>b. Have there been any changes or repairs to the fire protection systems since the last inspection?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_b']; ?></td>
		</tr>
		<tr>
		    <td>c. If a fire has occurred since the last inspection, have all damaged sprinkler system components been replaced?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_c']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">If answered "yes" to a, b or c, list changes in Section 13</td>
		</tr>
		<tr>
		    <td>d. Has the piping in all dry systems been checked for proper pitch within the past five years?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_d'] ?></td>
		</tr>
		<tr>
		    <td>Date last checked:&nbsp;&nbsp;<?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_d_date']); ?>&nbsp;&nbsp;(check recommended at least every 5 years)</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_d_1']; ?></td>
		</tr>
		<tr>
		    <td>e. Has the piping in all systems been checked for obstructive materials?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_e'] ?></td>
		</tr>
		<tr>
		    <td>Date last checked:&nbsp;&nbsp;<?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_e_date']); ?>&nbsp;&nbsp;(check recommended at least every 5 years)</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_e_1'] ; ?></td>
		</tr>
		</table>
		<table>
		<tr>
		    <td>f. Have all fire pumps been tested to full capacity using hose streams or flow meters within the past 12 months?</td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_f_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_f']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">g. Are gravity, surface or pressure tanks protected from freezing?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_g']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">h. Are sprinklers newer than 50 years old? QR (20yr) Dry (10 yr) >325F/163C (5yr) Corrosive env't. (5yr.)</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_h']; ?></td>
		</tr>            
		<tr>
		    <td colspan="2">(Testing or replacement required for sprinklers past these age limits.)
		    i. Are extra high temperature solder sprinklers free from regular exposure to temperatures near 300F/149C?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_i']; ?></td>
		</tr>
		<tr>
		    <td>j. Have gauges been tested, calibrated or replaced in the last 5 years?</td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_j_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_j']; ?></td>
		</tr>
		<tr>
		    <td>k. Alarm valves and associated trim been internally inspected past 5 years?</td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_k_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_k']; ?></td>
		</tr>
		<tr>
		    <td>l. Check valves internally inspected in the last 5 years?</td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_l_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_l']; ?></td>
		</tr>
		<tr>
		    <td>m. Has the private fire main been flow tested in last 5 years? </td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_m_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_m']; ?></td>
		</tr>
		<tr>
		    <td>n. Standpipe 5 and 3 year requirements.</td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_n']; ?></td>
		</tr>
		<tr>
		    <td>1. Dry standpipe hydrostatic test  </td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_1_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_n_1']; ?></td>
		</tr>
		<tr>
		    <td>2. Flow test </td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_2_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_n_2']; ?></td>
		</tr>            
		<tr>
		    <td>3. Hose hydrostatic test (5 years from new, every 3 years after)</td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_3_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_n_3']; ?></td>
		</tr>
		<tr>
		    <td>4. Pressure reducing/control valve test </td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_n_4_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_n_4']; ?></td>
		</tr>
		<tr>
		    <td>o. Have pressure reducing/control valves been tested at full flow within the past 5 years? </td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportGeneral']['general_o_date']); ?></td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_o']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">p. Have master pressure reducing/control valves been tested at full flow within the past 1 year?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_p']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">q. Have the sprinkler systems been extended to all areas of the building?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_q']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">r. Are the building areas protected by a wet system heated, including its blind attics and perimeter areas?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_r']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">s. Are all exterior openings protected against the entrance of cold air?</td>
		    <td><?php echo $record['SprinklerReportGeneral']['general_s']; ?></td>
		</tr>            			    
	    </table>
	</div>
      </div>
      <div id="test4-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Control</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/controlPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Control Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl" width="100%">		
		<tr>
		    <td colspan="3"><h3><a href="#" id="">2. CONTROL VALVES</a></h3></td>
		</tr>	   
		<tr>
		    <td colspan="2">a. Are all sprinkler system main control valves and all other valves in the appropriate open or closed position?</td>
		    <td><?php echo $record['SprinklerReportControlvalve']['cvalve_a']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">b. Are all control valves sealed or supervised in the appropriate position?</td>
		    <td><?php echo $record['SprinklerReportControlvalve']['cvalve_b']; ?></td>
		</tr>
	    </table>
	    <table class="tbl">
		<tr>
		  <td  width="10%">Control Valves</td>
		  <td  width="10%"># of Valves</td>
		  <td  width="10%">Type</td>
		  <td  width="10%">Easily Accessible</td>
		  <td  width="10%">Signs</td>
		  <td  width="10%">Valve Open</td>
		  <td  width="10%">Secured? </td>
		  <td  width="10%"> (Seal.?)/(Lock.?)/(Supvd.?)</td>
		  <td  width="10%">Supervision Operational</td>
		</tr>
		<tr>
		  <td>CITY CONNECTION</td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_valves']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_type']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_access']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_signs']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_vopen']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_secured']; ?></td>
		  <td> <?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_option']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_supervision']; ?></td>
		</tr>
		<tr>
		  <td>TANK</td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_valves']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_type']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_access']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_signs']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_vopen']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_secured']; ?></td>
		  <td> <?php echo $record['SprinklerReportControlvalve']['cvalve_tank_option']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_supervision']; ?></td>
		</tr>
		<tr>
		  <td>PUMP</td>              
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_valves']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_type']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_access']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_signs']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_vopen']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_secured']; ?></td>
		  <td> <?php echo $record['SprinklerReportControlvalve']['cvalve_pump_option']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_supervision']; ?></td>
		</tr>
		<tr>
		  <td>SECTIONAL</td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_valves']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_type']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_access']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_signs']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_vopen']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_secured']; ?></td>
		  <td> <?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_option']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_supervision']; ?></td>
		</tr>
		<tr>
		  <td>SYSTEM</td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_valves']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_type']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_access']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_signs']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_vopen']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_secured']; ?></td>
		  <td> <?php echo $record['SprinklerReportControlvalve']['cvalve_system_option']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_supervision']; ?></td>
		</tr>
		
		<tr>
		  <td>ALARM LINE</td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_valves']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_type']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_access']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_signs']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_vopen']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_secured']; ?></td>
		  <td> <?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_option']; ?></td>
		  <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_supervision']; ?></td>
		</tr>
	    </table>
	</div>
      </div>
      <div id="test5-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Water</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/waterPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Water Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl" width="100%">		
		<tr>
		    <td colspan="3"><h3><a href="#" id="">3. WATER SUPPLIES</a></h3></td>
		</tr>
	    </table>
	    <table class="tbl" width="100%" cellpadding="1" cellspacing="1">
		<tr>
		  <td width="20%"> a. Water supply sources? City:</td>
		  <td width="20%"><?php echo $record['SprinklerReportWatersupply']['wsupply_city']; ?></td>
		  <td width="20%">Gravity Tank:</td>
		  <td width="20%"><?php echo $record['SprinklerReportWatersupply']['wsupply_tank']; ?></td>
		  <td width="20%"></td>
		</tr>
		<tr>
		  <td colspan="5">Water Flow Test Results Made During This Inspection</td>
		</tr>				
		<tr>
		  <td>Test Pipe Located</td>
		  <td>Size Test Pipe</td>
		  <td>Static Pressure Before</td>
		  <td>Flow Pressure</td>
		  <td>Static Pressure After</td>
		</tr>
		<tr>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_1']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_1']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_1']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_1']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_1']; ?></td>
		</tr>
		<tr>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_2']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_2']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_2']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_2']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_2']; ?></td>
		</tr>
		<tr>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_3']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_3']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_3']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_3']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_3']; ?></td>
		</tr>
		<tr>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_4']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_4']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_4']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_4']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_4']; ?></td>
		</tr>
		<tr>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_5']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_5']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_5']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_5']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_5']; ?></td>
		</tr>
		<tr>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_6']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_6']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_6']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_6']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_6']; ?></td>
		</tr>
		<tr>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_7']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_7']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_7']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_7']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_7']; ?></td>
		</tr>
		<tr>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_8']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_8']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_8']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_8']; ?></td>
		  <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_8']; ?></td>
		</tr>
	    </table>
	</div>
      </div>
      <div id="test6-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Connection</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/connectionPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Connection Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl">		
		<tr>
		    <td colspan="3"><h3><a href="#" id=""> 4. TANKS, PUMPS, FIRE DEPT. CONNECTION</a></h3></td>
		</tr>
	    </table>	
	    <table class="tbl">
		<tr>
		    <td colspan="2">a. Do fire pumps, gravity, surface or pressure tanks appear to be in good external conditions?</td>
		    <td><?php if(!empty($record['SprinklerReportConnection']['connection_a'])){echo $record['SprinklerReportConnection']['connection_a'];} ?></td>
		</tr>
		<tr>
		    <td colspan="2">b. Are gravity, surface and pressure tanks at the proper pressure and/or water levels?</td>
		    <td><?php if(!empty($record['SprinklerReportConnection']['connection_b'])){echo $record['SprinklerReportConnection']['connection_b'];} ?></td>
		</tr>
		<tr>
		    <td>c. Has the storage tank been internally inspected in the last 3 yrs. (unlined) or 5 yrs. (lined)? </td>
		    <td><?php if(!empty($record['SprinklerReportConnection']['connection_c_date'])){echo $time->Format('m-d-Y',$record['SprinklerReportConnection']['connection_c_date']);} ?></td>
		    <td><?php if(!empty($record['SprinklerReportConnection']['connection_c'])){echo $record['SprinklerReportConnection']['connection_c'];} ?></td>
		</tr>
		<tr>
		    <td colspan="2">d. Are fire dept. connections in satisfactory condition, couplings free, caps or plugs in place and check valves tight?</td>
		    <td><?php if(!empty($record['SprinklerReportConnection']['connection_d'])){echo $record['SprinklerReportConnection']['connection_d'];} ?></td>
		</tr>
		<tr>
		    <td colspan="2">e. Are fire dept. connections visible and accessible?</td>
		    <td><?php if(!empty($record['SprinklerReportConnection']['connection_e'])){echo $record['SprinklerReportConnection']['connection_e'];} ?></td>
		</tr>
	    </table>
	</div>
      </div>
	<div id="test7-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Wet System</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/wetPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Wet Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl">		
		<tr>
		    <td colspan="3"><h3><a href="#" id=""> 5. WET SYSTEMS</a></h3></td>
		</tr>
	    </table>
	    <table class="tbl" width="100%">
		<tr>
		    <td>a. No. of systems:</td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_a'] ?></td>
		    <td>Make & Model : </td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_a_makemodel']; ?></td>
		</tr>
		<tr>
		    <td colspan="3">b. Are cold weather valves in the appropriate open or closed position?</td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_b']; ?></td>
		</tr>
		<tr>
		    <td colspan="3">If closed, has piping been drained ?</td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_b_1']; ?></td>
		</tr>
		<tr>
		    <td colspan="3">c. Has the Customer been advised that cold weather valves are not recommended?</td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_c']; ?></td>
		</tr>
		<tr>
		    <td colspan="2">d. Have all the antifreeze systems been tested in the past year?</td>
		    <td><?php echo $time->Format('m-d-Y',$record['SprinklerReportWetsystem']['wetsystem_d_date']); ?></td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_d']; ?></td>
		</tr>
		<tr>
		    <td colspan="4">The antifreeze tests indicated protection to:</td>
		</tr>
		<tr>
		    <td>system 1 </td>
		    <td style="text-align:right"><b>Temperature</b></td>
		    <td style="text-align:left"><?php echo $record['SprinklerReportWetsystem']['wetsystem_1_temp']; ?></td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_1']; ?></td>
		</tr>
		<tr>
		    <td>system 2 </td>
		    <td style="text-align:right"><b>Temperature</b></td>
		    <td style="text-align:left"><?php echo $record['SprinklerReportWetsystem']['wetsystem_2_temp']; ?></td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_2_temp']; ?></td>
		</tr>
		<tr>
		    <td>system 3 </td>
		    <td style="text-align:right"><b>Temperature</b></td>
		    <td style="text-align:left"><?php echo $record['SprinklerReportWetsystem']['wetsystem_3_temp']; ?></td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_3_temp']; ?></td>
		</tr>
		<tr>
		    <td>system 4 </td>
		    <td style="text-align:right"><b>Temperature</b></td>
		    <td style="text-align:left"><?php echo $record['SprinklerReportWetsystem']['wetsystem_4_temp']; ?></td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_4_temp']; ?></td>
		</tr>
		<tr>
		    <td>system 5 </td>
		    <td style="text-align:right"><b>Temperature</b></td>
		    <td style="text-align:left"><?php echo $record['SprinklerReportWetsystem']['wetsystem_5_temp']; ?></td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_5_temp']; ?></td>
		</tr>
		<tr>
		    <td>system 6 </td>
		    <td style="text-align:right"><b>Temperature</b></td>
		    <td style="text-align:left"><?php echo $record['SprinklerReportWetsystem']['wetsystem_6_temp']; ?></td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_6_temp']; ?></td>
		</tr>
		<tr>
		    <td colspan="3">e. Did alarm valves, water flow alarm devices and retards test satisfactorily?</td>
		    <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_e']; ?></td>
		</tr>
	    </table>
	</div>
      </div>
	
	<div id="test8-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Dry System</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/dryPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Dry Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl" width="100%">		
		<tr>
		    <td colspan="3"><h3><a href="#" id="">6. DRY SYSTEM</a></h3></td>
		</tr>
	    </table>
	    <table class="tbl">
	      <tr>
		<td  width="20%">a. No. of systems:	</td>
		<td ><?php echo $record['SprinklerReportDrysystem']['drysystem_a']; ?></td>
		<td >Make & Model :	Partial</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_a_makemodel']; ?></td>
		<td>	Full</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_a_full']; ?></td>            
	      </tr>
	      <tr>
		<td> Date last trip tested:</td>
		<td ><?php echo $record['SprinklerReportDrysystem']['drysystem_a_date']; ?></td>
	      </tr>
	      <tr>
		<td colspan="5">b. Are the air pressure(s) and priming water level(s) normal?</td>
		<td ><?php echo $record['SprinklerReportDrysystem']['drysystem_b']; ?></td>
	      </tr>
	      <tr>
		<td colspan="5">c. Did the air compressor(s) operate satisfactorily?</td>
		<td ><?php echo $record['SprinklerReportDrysystem']['drysystem_c']; ?></td>
	      </tr>
	      <tr>
		<td colspan="5">d. Air compressor(s) oil checked?</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_d']; ?></td>
	      </tr>
	      <tr>
		<td colspan="5">Belt(s)?</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_d_belt']; ?></td>
	      </tr>
	      <tr>
		<td colspan="5">e. Were Low Point drains drained during this inspection?</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_e']; ?></td>
	      </tr>
	      <tr>
		<td>No. of Drains:</td>
		<td colspan="5"><?php echo $record['SprinklerReportDrysystem']['drysystem_e_drains']; ?></td>
	      </tr>
	      <tr>
		<td colspan="6">Locations:</td>
	      </tr>
	      <tr>
		<td>1.</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_e_locations_1']; ?></td>
		<td>2.</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_e_locations_2']; ?></td>
		<td colspan="2"></td>
	      </tr>
	      <tr>
		<td>3.</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_e_locations_3']; ?></td>
		<td>4.</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_e_locations_4']; ?></td>
		<td colspan="2"></td>
	      </tr>
	      <tr>
		<td  colspan="5">f. Did all quick opening devices operate satisfactorily?</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_f']; ?></td>
	      </tr>
	      <tr>
		<td>Make & Model:?</td>
		<td colspan="5"><?php echo $record['SprinklerReportDrysystem']['drysystem_f_makemodel']; ?></td>
	      </tr>
	      <tr>
		<td  colspan="5">g. Did all the dry valves operate satisfactorily during this inspection?</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_g']; ?></td>
	      </tr>
	      <tr>
		<td  colspan="5">h. Is the dry valve house heated?</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_h']; ?></td>
	      </tr>
	      <tr>
		<td  colspan="5">i. Do the dry valves appear to be protected from freezing?</td>
		<td><?php echo $record['SprinklerReportDrysystem']['drysystem_i']; ?></td>
	      </tr>
	    </table>
	</div>
      </div>
	<div id="test9-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Special System</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/specialPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Special Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl" width="100%">		
		<tr>
		    <td colspan="3"><h3><a href="#" id="">7. SPECIAL SYSTEMS</a></h3></td>
		</tr>
	    </table>
	    <table class="tbl" width="100%">		    
		<tr>  
		  <td  width="20%">  a. No. of systems:</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_a']; ?></td>
		  <td>Make & Model :</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_a_makemodel']; ?></td>
		</tr>
		<tr>
		  <td> Type:</td>
		  <td  colspan="3"><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_a_type']; ?></td>
		</tr>           
		<tr>
		  <td colspan="3">b. Were valves tested as required?</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_b']; ?></td>
		</tr>
		<tr>
		  <td  colspan="3">c. Did all heat responsive systems operate satisfactorily?</td>
		  <td><?php echo  $record['SprinklerReportSpecialsystem']['specialsystem_c']; ?></td>
		</tr>
		<tr>
		  <td  colspan="3">d. Did the supervisory features operate during testing?</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d']; ?></td>
		</tr>          
		<tr>
		  <td>Heat Responsive Devices:</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_heat']; ?></td>
		  <td>Type	</td>
		  <td ><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_type']; ?></td>
		<tr>
		  <td>Type of test</td>
		  <td colspan="3"><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_type_test']; ?></td>
		</tr>
		<tr>
		  <td>Valve No.</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_valve_1']; ?></td>
		  <td colspan="2"><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_valve_1']; ?></td>              
		</tr>
		<tr>
		  <td>Seconds</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_second_1']; ?></td>
		  <td colspan="2"><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_second_2']; ?></td>
		</tr>
		<tr>
		  <td colspan="4"> e. Has a supplemental test form for this system been completed and provided to the customer?     (Please attach)</td>
		</tr>          
		<tr>
		  <td colspan="4">Auxiliary equipment: </td>
		</tr>
		<tr>
		  <td>No.</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_e_number']; ?></td>
		  <td>Type</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_e_type']; ?></td>
		</tr>
		<tr>
		  <td width="10%">Location</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_e_location']; ?></td>
		  <td>Test results</td>
		  <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_e_test_result']; ?></td>
		</tr>          
	      </table>
	</div>
      </div>
	<div id="test10-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Alarms</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/alarmPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Alarms Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl" width="100%">		
		<tr>
		    <td colspan="3"><h3><a href="#" id="">8. ALARMS</a></h3></td>
		</tr>
	    </table>
	    <table class="tbl" width="100%">
		<tr>
		    <td>a. Did the water motor(s) and gong(s) operate during testing?</td>
		    <td><?php echo $record['SprinklerReportAlarm']['alarm_a']; ?> </td>				    
		</tr>
		<tr>
		    <td>b. Did the electric alarm(s) operate during testing? </td>
		    <td><?php echo $record['SprinklerReportAlarm']['alarm_b']; ?> </td>
		</tr>
		<tr>
		    <td>c. Did the supervisory alarm(s) operate during testing?</td>
		    <td><?php echo $record['SprinklerReportAlarm']['alarm_c']; ?> </td>				    
		</tr>
	    </table>
	</div>
      </div>
    <div id="test11-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Sprinklers</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reportPrints/sprinklerpipingPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Sprinklers Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl" width="100%">		
		<tr>
		    <td colspan="3"><h3><a href="#" id="">9. SPRINKLERS - PIPING</a></h3></td>
		</tr>
	    </table>
	    <table class="tbl" width="100%">
	    <tr>
		<td>a. Do sprinklers generally appear to be in good external condition?</td>
		<td><?php echo $record['SprinklerReportPiping']['piping_a']; ?> </td>				    
	    </tr>
	    <tr>
		<td>b. Do sprinklers generally appear to be free of corrosion, paint, or loading and visible obstructions? </td>
		<td><?php echo $record['SprinklerReportPiping']['piping_b']; ?> </td>
	    </tr>
	    <tr>
		<td>c. Are extra sprinklers and sprinkler wrench available on the premises? </td>
		<td><?php echo $record['SprinklerReportPiping']['piping_c']; ?> </td>				    
	    </tr>
	    <tr>
		<td>d. Does the exposed exterior condition of piping, drain valves, check valves, hangers, pressure gauges, open sprinklers and strainers appear to be satisfactory? </td>
		<td><?php echo $record['SprinklerReportPiping']['piping_d']; ?> </td>				    
	    </tr>
	    <tr>
		<td>e. Does the hand hose on the sprinkler system appear to be in satisfactory condition? </td>
		<td><?php echo $record['SprinklerReportPiping']['piping_e']; ?> </td>				    
	    </tr>
	    <tr>
		<td>f. Does there appear to be proper clearance between the top of all storage and the sprinkler deflector?</td>
		<td><?php echo $record['SprinklerReportPiping']['piping_f']; ?> </td>				    
	    </tr>
	    </table>
	</div>
      </div>
    
    <!--<div id="test12-content">
	    <div class="accordion_child">
	    <table class="tbl input-chngs1" width="100%">
		<tr>
			<td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey;"><?php echo ucwords($spResult['Company']['name']);?></span></td>								
		</tr>
		<tr>
		    <td colspan="2" align=right width="55%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Wet System</span></td>
		    <td align="right"><?php echo $html->link($html->image("pdf.png"), '/reports/wetPagePrintSprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Wet Page Preview'));?></td>
		</tr> 
	    </table>
	    <table class="tbl" width="100%">		
		<tr>
		    <td colspan="3"><h3><a href="#" id="">Attachments And Notifications</a></h3></td>
		</tr>
	    </table>
	    
		<table class="tbl" width="100%">
		    <tr>
	    <td  colspan="14" align='left' style="text-align:left"><p class="main-heading">Attachments:</p></td>
	  </tr>
	  <tr>
	    <td colspan="14" align='left' style="text-align:left">
		<table border="0" cellpadding="1" cellspacing="1" width="100%">
		    <?php foreach($attachmentdata as $attachdata){ ?>
		    <tr>
		      <td width="45%"><?php echo $html->link($attachdata['Attachment']['attach_file'],array("controller"=>"reports","action"=>"download",$attachdata['Attachment']['attach_file']),array()); ?></td>
		      <td><?php echo date('m/d/Y',strtotime($attachdata['Attachment']['created'])); ?></td>
		      <td><?php echo $this->Common->getClientName($attachdata['Attachment']['user_id'])."(".$attachdata['Attachment']['added_by'].")"; ?></td>
		    </tr>  
		    <?php } ?>
		</table>
	    </td>
	  </tr>
	  
	  <tr>
	    <td colspan="14" align='left' style="text-align:left"><p class="main-heading">Notifications:</p></td>
	  </tr>
	  <tr>
	    <td colspan="14" align='left' style="text-align:left">
		<table border="0" cellpadding="1" cellspacing="1" width="100%">
		    <?php foreach($notifierdata as $notifydata){ ?>
		    <tr>
		      <td width="45%"><?php echo $notifydata['Notification']['notification']; ?></td>
		      <td><?php echo date('m/d/Y',strtotime($notifydata['Notification']['created'])); ?></td>
		      <td><?php echo $this->Common->getClientName($notifydata['Notification']['user_id'])."(". $notifydata['Notification']['added_by'].")"; ?></td>
		    </tr>  
		    <?php } ?>
		</table>
	    </table>
	</div>
      </div>-->
    
    <div id="test13-content">
	    <div class="accordion_child">
	    <div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>    
	    <div style="text-align:center;width:100%;font-size:15px; color: red;"><b>Deficiencies</b></div>
	    <div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/deficiencyPagePrint/Sprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Deficiency Page Preview'));?></div>
	    <div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Sprinkler</b></div>    
	    <br/>
	<table width="100%">
	    <tr>
		<td align="right" colspan="2"></td>
	    </tr>
	    <tr>
		<td width="50%" align="left"><i><b>Prepared For:</b></i>
		<br/>
		<?php echo ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']); ?>		
		<br/>
		<?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
		    if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $clientResult['User']['site_city'];  ?>
		<br/>
		<b>Site Information:</b><br/>
		    <?php echo $siteaddrResult['SpSiteAddress']['site_name'];?><br/>
		    <?php echo $siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact']; ?><br/>
		    <?php echo $siteaddrResult['SpSiteAddress']['site_address'];?><br/>
		    <?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
		    <?php echo $siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']); ?>-<?php echo $siteaddrResult['SpSiteAddress']['site_zip']; ?><br/>
		</td>
		<td width="50%" align="right" style="padding-right:30px;">
		    <i><b>Prepared By:</b></i>
		    <br/>
		    <?php echo $spResult['Company']['name']; ?>
		    <br/>
		    <?php echo $spResult['User']['address']; ?>
		    <br/>
		    <?php 	if(!empty($spResult['Country']['name'])){
				echo $spResult['Country']['name'].', ';
			    }
			    if(!empty($spResult['State']['name'])) {
				echo $spResult['State']['name'].', ';
			    }
			    echo $spCity;
		    ?>
		    <br/>
		    Phone <?php echo ' '.$spResult['User']['phone']; ?>
		</td>
		
		</tr>
	</table>
	    
	   <table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
	    <tr>
		
		<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
		<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
		<th width="60%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
		<th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Attachment</b></th>
		
	    </tr>
	    <?php
	    if(!empty($Def_record))
	    {
		$defCounter = 0;
	   // pr($codeData);
	    foreach($Def_record as $Def_record) {
		$defCounter++;
	    ?>
	    <tr>
		
		<td valign="top"><?php echo $defCounter.'.'; ?></td>
		<td valign="top"><?php echo $Def_record['Code']['code']; ?></td>
		<td valign="top"><?php echo $Def_record['Code']['description']; ?></td>
		<td valign="top">
		<?php if(!empty($Def_record['Deficiency']['attachment']) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$Def_record['Deficiency']['attachment'])){?>
		    <?php echo $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$Def_record['Deficiency']['attachment'],'sp'=>false)); ?>
		<?php }else{?>
		    <?php echo 'No Attachment';?>
		<?php }?>
		</td>
		
	    </tr>
	    <?php }
	    } else
	    {
	     echo '<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';   
	    }
	    ?>
	</table>       
	</div>
      </div>
    <div id="test14-content">
	    <div class="accordion_child">	    
	    <div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
	    <div style="text-align:center;width:100%;font-size:15px; color:orange;"><b>Recommendation</b></div>
	    <div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/recommendationPagePrint/Sprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Recommendation Page Preview'));?></div>        
	    <div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Sprinkler</b></div>	    
	    <br/>
	<table width="100%">
	    <tr>
		<td align="right" colspan="2"></td>
	    </tr>
	    <tr>
		<td width="50%" align="left"><i><b>Prepared For:</b></i>
		<br/>
		<?php echo ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']); ?>		
		<br/>
	       <?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
		    if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
		<br/>            
		</td>
		<td width="50%" align="right" style="padding-right:30px;">
		    <i><b>Prepared By:</b></i>
		    <br/>
		    <?php echo $spResult['Company']['name']; ?>
		    <br/>
		    <?php echo $spResult['User']['address']; ?>
		    <br/>
		    <?php 	if(!empty($spResult['Country']['name'])){
				echo $spResult['Country']['name'].', ';
			    }
			    if(!empty($spResult['State']['name'])) {
				echo $spResult['State']['name'].', ';
			    }
			    echo $spCity;
		    ?>
		    <br/>
		    Phone <?php echo ' '.$spResult['User']['phone']; ?>
		</td>
		
		</tr>
	</table>
	    
	   <table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
	    <tr>
		
		<th width="5%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
		<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
		<th width="30%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
		<th width="25%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>
		
	    </tr>
	    <?php
	    if(!empty($Def_record_recomm))
	    {
		$recomCounter = 0;
	    foreach($Def_record_recomm as $Def_record_recomm) {
		$recomCounter++;
	    ?>
	    <tr>
		
		<td valign="top"><?php echo $recomCounter.'.'; ?></td>
		<td valign="top"><?php echo $Def_record_recomm['Code']['code']; ?></td>
		<td valign="top"><?php echo $Def_record_recomm['Code']['description']; ?></td>
		<td valign="top"><?php echo $Def_record_recomm['Deficiency']['recommendation']; ?></td>
		
	    </tr>
	    <?php }
	    } else
	    {
	     echo '<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
	    }
	    ?>
	</table>
	</div>
      </div>
    <div id="test15-content">
	    <div class="accordion_child">
	    <table width="100%" class="tbl">
		<tr>
		    <td colspan="3" align="center" style="border:none;"><span style="font-size:35px; font-weight:bolder; color: grey"><?php echo ucwords($spResult['Company']['name']);?></span></td>
		</tr>	
		<tr>
		    <td colspan="2" align=right width="57%" style="border:none;"><span style="font-size:15px; font-weight:bolder;">Signature Page</span></td>
		    <td colspan="" align="right" style="border:none;"><?php echo $html->link($html->image("pdf.png"), '/reports/signaturePagePrint/SprinklerReport?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Signature Page Preview'));?></td>
		</tr>	       
	    </table>
	    <table width="100%">
		<tr>
		    <td style="border:none;"><b>Inspector Signature</b></br></br>
		    <?php  $name=$this->Common->getInspectorSignature($record['SprinklerReport']['report_id'],$record['SprinklerReport']['servicecall_id'],$record['SprinklerReport']['inspector_id'])?><?php echo $this->Html->image('signature_images/'.$name);  ?>
		    
		    </td>
		    <td style="border:none; vertical-align:top;" align="right"><b>Client Signature</b></td>
	      
		</tr>
		<tr>
		    <td style="border:none;">		
		    <?php $dateCreated=$this->Common->getSignatureCreatedDate($record['SprinklerReport']['report_id'],$record['SprinklerReport']['servicecall_id'],$record['SprinklerReport']['inspector_id']);
		      
		    if($dateCreated!='empty'){
		    echo $this->Common->getClientName($record['SprinklerReport']['inspector_id'])."<br/><br/>";
		      echo $time->Format('m-d-Y',$dateCreated);
		    }
		    ?>
		    </td>
		    <td align="right" style="border:none;">
		    <?php //$dateCreated=$this->Common->getSignatureCreatedDate($record['EmergencyexitReport']['report_id'],$record['EmergencyexitReport']['servicecall_id'],$record['EmergencyexitReport']['inspector_id']);
		      
		    //if($dateCreated!='empty'){
		    echo $this->Common->getClientName($record['SprinklerReport']['client_id'])."<br/><br/>";
		      //echo $time->Format('m-d-Y',$dateCreated);
		    //}
		    ?>	
		    </td>
	      
		</tr>
		
	    </table>
	</div>
      </div>
      
      <div id="test16-content">
	<div class="accordion_child">
		<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
		<div style="text-align:center;width:100%;font-size:15px;"><b>Certification</b></div>
		<div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/certAttachedPagePrint/Sprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'CERT Attached Page Preview'));?></div>
		<div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Sprinkler</b></div>
		<br/>
		<table width="100%">
			<tr>
				<td align="right" colspan="2"></td>
			</tr>
			<tr>
				<td width="50%" align="left"><i><b>Prepared For:</b></i>
				<br/>
				<?php echo ucwords($clientResult['User']['client_company_name']); ?>		    
				<br/>
				<?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
				if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $clientResult['User']['city'];  ?>
				<br/>
				</td>
				<td width="50%" align="right" style="padding-right:30px;">
					<i><b>Prepared By:</b></i>
					<br/>
					<?php echo $spResult['Company']['name']; ?>
					<br/>
					<?php echo $spResult['User']['address']; ?>
					<br/>
					<?php 	if(!empty($spResult['Country']['name'])){
							echo $spResult['Country']['name'].', ';
						    }
						    if(!empty($spResult['State']['name'])) {
							echo $spResult['State']['name'].', ';
						    }
						    echo $spCity;
					?>
					<br/>
					Phone <?php echo ' '.$spResult['User']['phone']; ?>
				</td>		    
			</tr>
		</table>
		<table class="tbl input-chngs1" width="100%" >                
			<tr>
				<td align='left' style="text-align:left">
				<?php
					$certs = $this->Common->getAllSchCerts($schId);						
					if(sizeof($certs)>0){
				?>					
				<?php $cr=1;
				foreach($certs as $cert)
				{
				    echo $html->link($cr.'.'.$cert['ReportCert']['ins_cert_title'],array("controller"=>"messages","action"=>"download_self_form",$cert['ReportCert']['ins_cert_form'],'inspector'=>true),array('title'=>'DownLoad/View')); 
				    
					    echo '<br/>';
				    
				    $cr++;
				}
				?>
				<?php
				}
				?>
				</td>
			</tr>	
		</table>
	    </div>
	</div>

	<div id="test17-content">
	    <div class="accordion_child">
		<div style="text-align:center;width:100%;font-size:35px; color: grey; padding-bottom: 10px;"><b><?php echo ucwords($spResult['Company']['name']);?></b></div>
		<div style="text-align:center;width:100%;font-size:15px;"><b>Quotes</b></div>
		<div style="float:right"><?php echo $html->link($html->image("pdf.png"), '/reports/quotesPagePrint/Sprinkler?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'], array('escape' => false,'title'=>'Quotes Page Preview'));?></div>        
		<div style="float:left;font-size:11px;margin-top:5px;"><b>Reference: Sprinkler</b></div>
		<br/>
		<table width="100%">
			<tr>
				<td align="right" colspan="2"></td>
			</tr>
			<tr>
				<td width="50%" align="left"><i><b>Prepared For:</b></i>
				<br/>
				<?php echo ucwords($clientResult['User']['client_company_name']); ?>		    
				<br/>
				<?php if(!empty($clientResult['User']['country_id'])) { echo $this->Common->getCountryName($clientResult['User']['country_id']).', '; }
				if(!empty($clientResult['User']['state_id'])) { echo $this->Common->getStateName($clientResult['User']['state_id']).', '; }  echo $cityName;  ?>
				<br/>
				</td>
				<td width="50%" align="right" style="padding-right:30px;">
					<i><b>Prepared By:</b></i>
					<br/>
					<?php echo $spResult['Company']['name']; ?>
					<br/>
					<?php echo $spResult['User']['address']; ?>
					<br/>
					<?php 	if(!empty($spResult['Country']['name'])){
						    echo $spResult['Country']['name'].', ';
						}
						if(!empty($spResult['State']['name'])) {
						    echo $spResult['State']['name'].', ';
						}
						echo $spCity;
					?>
					<br/>
					Phone <?php echo ' '.$spResult['User']['phone']; ?>
				</td>			    
			</tr>
		</table>
		<table class="tbl input-chngs1" width="100%" >                
			<tr>
				<td align='left' style="text-align:left">
					<div><b>Quote Form from SP</b></div>
					<?php										
					if(isset($chkRecord) && !empty($chkRecord)){
						echo $html->link($chkRecord['Quote']['title'],array('controller'=>'sps','action'=>'download_file',$chkRecord['Quote']['attachment'],'quotefile'));
					}else{
						echo 'No Quote have been submitted by Service Provider.';
					}
					?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align='left' style="text-align:left">
				<div><b>Client's Response to Quote Form</b></div>
				<?php										
				$quotedocs = $this->Common->getQuoteDoc($_REQUEST['reportID'],$_REQUEST['clientID'],$_REQUEST['spID'],$_REQUEST['serviceCallID']);
				
				if(isset($quotedocs) && !empty($quotedocs)){
					if($quotedocs['Quote']['client_response']=='a'){
						$status = 'Accepted';
					}else if($quotedocs['Quote']['client_response']=='d'){
						$status = 'Denied';
					}else if($quotedocs['Quote']['client_response']=='n'){
						$status = 'No wish to fix the deficiencies';
					}else{
						$status = 'Pending';
					}
					?>
					<div>Status: <?php echo $status;?></div>
					<?php if($quotedocs['Quote']['client_response']=='a'){?>
					<div class="certificates-hdline" style="text-align: left;">
					    <?php echo $html->link('Signed Quote Form',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['signed_form'],'quotefile','sp'=>true),array('style'=>'color:red'))?>
					</div>							    							
					<?php }else if($quotedocs['Quote']['client_response']=='d'){?>
					<div class="certificates-hdline" style="text-align: left;">
					    <?php echo $html->link('Work Orders',array('controller'=>'sps','action'=>'download_file',$quotedocs['Quote']['work_order_form'],'quotefile','sp'=>true),array('style'=>'color:red'))?>
					</div>
					<?php }?>
					<?php					
				}else{
					echo 'NA';
				}
				?>
				</td>
			</tr>
		</table>
	    </div>
	</div>
      
    </div>
    </div><!--End of accordion parent-->
    </body>
	     