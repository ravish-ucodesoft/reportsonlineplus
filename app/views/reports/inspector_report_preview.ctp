<?php
App::import('Vendor','tcpdf');

// Extend the TCPDF class to create custom Header and Footer
/*class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = 'http://172.24.0.9:9807/app/webroot/img/company_logo/company50765a866042c_315x134.jpg';
        $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
    }    
}
$tcpdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);*/

$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
//$tcpdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);
$tcpdf->setHeaderData('../../../webroot/img/company_logo/'.$spResult['Company']['company_logo'],20);
$tcpdf->setPrintHeader(true);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();
// set default header data

//$tcpdf->setHeaderData('http://172.24.0.9:9807/app/webroot/img/company_logo/company50765a866042c_315x134.jpg');
//$tcpdf->setPrintHeader(true);
$fileName = (($reportName=='Sprinkler')?'':$reportName);
//$html = file_get_contents($html->url(array('controller'=>'reports','action'=>'reportTest?reportID=3&clientID=86&spID=10&serviceCallID=8','inspector'=>false)));
$html = file_get_contents(BASE_URL.'reports/reportTest'.$fileName.'?reportID='.$reportId.'&clientID='.$clientID.'&spID='.$spId.'&serviceCallID='.$serviceCallID);
//$html = 'hello';
//echo $html;die;
$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
 
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_'.$reportName.'_Inspection.pdf', 'I');die;
?>