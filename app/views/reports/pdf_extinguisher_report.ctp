<?php

App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();


$html = '
<table border="" cellspacing="0" cellpadding="2">

	      <tr><td colspan="4">
        <table width="100%">
          <tr>
            <td align="center"><img src="/app/webroot/img/company_logo/'.$spResult['Company']['company_logo'].'"></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b>'.$spResult['Company']['name'].'</b></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:21px">';		
		$arr=explode(',',$spResult['Company']['interest_service']);
	    
		    foreach($arr as $key=>$val):
		    foreach($reportType as $key1=>$val1):

		    if($val== $key1)
		    {
			
			 if($key1 == 9){
                            $html.=  $spResult['Company']['interest_service_other'];
                            }else{
                                $html.= $val1;
                                }
                       $html.=' <span class="red"> * </span>';
		     }
		    
		    endforeach;
		    
		    endforeach;	

		 $html.=   '</td></tr>
            
            
          
          
          
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:24px">Extinguisher Report</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>          
          <tr>
            <td align="right" class="Cpage" >Date:';if($record['ExtinguisherReport']['finish_date'] != ''){
                $html.= date('m/d/Y',strtotime($record['ExtinguisherReport']['finish_date'])); } $html.='</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" ><i>Prepared for:</i></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_name'].'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'; if(!empty($clientResult['User']['site_country_id'])){
                                        $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                        if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                        $html.= $clientResult['User']['site_city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_phone'].'</td>
          </tr>   
          <tr>
            <td align="left" class="Cpage" >Prepared By:</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
          <tr>
            <td align="left" class="Cpage">'.$spResult['Company']['name'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['address'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'; if(!empty($spResult['Country']['name'])){
                                        $html.= $spResult['Country']['name'].', '; }
                                        if(!empty($spResult['State']['name'])) { $html.= $spResult['State']['name'].', '; } 
                                        $html.= $clientResult['User']['city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['phone'].'</td>
          </tr> 
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
         <tr>
            <td align="center"><h2>'. ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']) .'</h2></td>
          </tr>
          
        </table>
      </td>
      </tr>
      <tr><td colspan"4" align="center">&nbsp;</td></tr>
  <tr>
	   <td width="30%" align="right"><strong>Inspection Report:</strong></td>
	   <td width="20%" align="left">'.$record['ExtinguisherReport']['inspection_report'].'</td>
	   <td width="30%" align="right"><strong>Inspection Contract Number:</strong></td>
	   <td width="20%" align="left">'.$record['ExtinguisherReport']['inspection_contract_number'].'</td>
	</tr>
	<tr>
	   <td width="30%" align="right"><strong>Inspection Date:</strong></td>
	   <td width="20%" align="left">'.$record['ExtinguisherReport']['inspection_date'].'</td>
	   <td width="30%" align="right"><strong>Inspection Time:</strong></td>
	   <td width="20%" align="left">'.$record['ExtinguisherReport']['inspection_time'].'</td>
	</tr>
	<tr>
	   <td colspan="4" align="center">&nbsp;</td>
	</tr>
	<tr>
	   <td colspan="2" align="center"><strong>Client Info </strong> </td>	   
	   <td colspan="2" align="center"><strong>Service Provider Info </strong> </td>	
	</tr>
	<tr>
	   <td width="20%" align="right"><strong>Company Name:</strong></td>
	   <td width="30%" align="left">'.$clientResult['User']['site_name'].'</td>
	   <td width="20%" align="right"><strong>Name of Service Provider:</strong></td>
	   <td width="30%" align="left">'.ucwords($spResult['User']['fname'].' '.$spResult['User']['lname']).'</td>
	</tr>
	<tr>
	   <td width="20%" align="right"><strong>Address:</strong></td>
	   <td width="30%" align="left">'.$clientResult['User']['address'].'</td>
	   <td width="20%" align="right"><strong>Address:</strong></td>
	   <td width="30%" align="left">'.$spResult['Company']['address'].'</td>
	</tr>
	<tr>
	   <td width="20%" align="right"><strong>City State & Zip:</strong></td>
	   <td width="30%" align="left">'.$clientResult['User']['site_city_state_zip'].'</td>
	   <td width="20%" align="right"><strong>City State & Zip:</strong></td>
	   <td width="30%" align="left">'.$spResult['User']['site_city_state_zip'].'</td>
	</tr>
	<tr>
	   <td width="20%" align="right"><strong>Owner Contact:</strong></td>
	   <td width="30%" align="left">'.$clientResult['User']['phone'].'</td>
	   <td width="20%" align="right"><strong>Office Phone:</strong></td>
	   <td width="30%" align="left">'.$spResult['Company']['phone'].'</td>
	</tr>
	<tr>
	   <td width="20%" align="right"><strong>Building: 	</strong></td>
	   <td width="30%" align="left">'.$clientResult['User']['site_name'].'</td>
	   <td width="20%" align="right"><strong>Office License #</strong></td>
	   <td width="30%" align="left">&nbsp;</td>
	</tr>
	<tr>
	   <td colspan="4" align="center">&nbsp;</td>
	</tr>
	<tr>
	   <td colspan="4" align="center">PORTABLE FIRE EXTINGUISHER REPORT RECORD</td>
	</tr>
	<tr>
	   <td colspan="4" align="center">&nbsp;</td>
	</tr>
	</table>
	<table style="border: 0.5px solid #E8E7E1;">
	<tr align="center" style="font-size:18px">    
    <th style="border: 0.5px solid #E8E7E1;" rowspan="2" width="8%"><strong>Location</strong></th>
    <th style="border: 0.5px solid #E8E7E1;" rowspan="2" width="5%"><strong>Type</strong></th>
    <th style="border: 0.5px solid #E8E7E1;" rowspan="2" width="7%"><strong>Model</strong></th>
    <th style="border: 0.5px solid #E8E7E1;" rowspan="2" width="9%"><strong>Born Date</strong></th>
    <th style="border: 0.5px solid #E8E7E1;" width="8%"><strong>Deficient</strong></th>
    <th style="border: 0.5px solid #E8E7E1;" width="8%"><strong>Deficient Reason</strong></th>
    <th style="border: 0.5px solid #E8E7E1;" colspan="2" width="18%"><strong>Last Maintenace</strong></th>
    <th style="border: 0.5px solid #E8E7E1;" colspan="2" width="18%"><strong>Last Recharge</strong></th>
    <th style="border: 0.5px solid #E8E7E1;" colspan="2" width="19%"><strong>Last Hydro test</strong></th>
	  
  </tr>
  <tr align="center" style="font-size:18px">        
    <th style="border: 0.5px solid #E8E7E1;"><strong>Yes/No</strong></th>
    <th style="border: 0.5px solid #E8E7E1;"><strong>&nbsp;</strong></th>
    <th style="border: 0.5px solid #E8E7E1;"><strong>Date</strong></th>
    <th style="border: 0.5px solid #E8E7E1;"><strong>Performed By</strong></th>
    <th style="border: 0.5px solid #E8E7E1;"><strong>Date</strong></th>
    <th style="border: 0.5px solid #E8E7E1;"><strong>Performed By</strong></th>
    <th style="border: 0.5px solid #E8E7E1;"><strong>Date</strong></th>
    <th style="border: 0.5px solid #E8E7E1;"><strong>Performed By</strong></th>        
  </tr>';
  foreach($record['ExtinguisherReportRecord'] as $recorddata){
  	$html .='<tr style="font-size:18px; vertical-align:middle" align="center">
  	<td>'. $recorddata['location'].'</td>
    <td>'. $recorddata['ext_type'].'</td>
    <td>'. $recorddata['model'].'</td>
    <td>'. $recorddata['born_date'].'</td>
    <td>'. $recorddata['deficiency'].'</td>
    <td>'. $recorddata['deficiency_reason'].'</td>
    <td>'. $recorddata['last_maintenace_date'].'</td>    
    <td>'. $recorddata['last_maintenace_performed_by'].'</td>
    <td>'. $recorddata['last_recharge_date'].'</td>
    <td>'. $recorddata['last_recharge_performed_by'].'</td>
    <td>'. $recorddata['last_hydrotest_date'].'</td>
    <td>'. $recorddata['last_hydrotest_performed_by'].'</td>        
  </tr>';
  }
  	$html .='</table>
  	<table>
    <tr>
	    <td colspan="6">&nbsp;</td>	    
	  </tr>
	  <tr>
	    <td colspan="6"><u>Attachments</u>:</td>	    
	  </tr>	';
	    foreach($attachmentdata as $attachdata){
   	  $html .='<tr>
  	    <td colspan="2">'.$attachdata['Attachment']['attach_file'].'</td>	
        <td>'.date('m/d/Y',strtotime($attachdata['Attachment']['created'])).'</td>	
        <td colspan="2">'.$this->Common->getClientName($attachdata['Attachment']['user_id']).'('.$attachdata['Attachment']['added_by'].')</td>	 
        <td>&nbsp;</td>	         
  	</tr>'; }
    	$html .='<tr>
  	    <td colspan="6">&nbsp;</td>	    
  	</tr>
    <tr>
  	    <td colspan="6"><u>Notifications</u>:</td>	    
  	</tr>';
    foreach($notifierdata as $notifydata){
    $html .='<tr>
  	    <td colspan="2">'.$notifydata['Notification']['notification'].'</td>	
        <td>'.date('m/d/Y',strtotime($notifydata['Notification']['created'])).'</td>	
        <td colspan="2">'.$this->Common->getClientName($notifydata['Notification']['user_id']).'('.$notifydata['Notification']['added_by'].')</td>	 
        <td>&nbsp;</td>	         
  	</tr>'; }
        $html.= '</table>
        <table width="100%">
                        <tr><td height="70px"></td></tr>
                        </table>
        
                                <table width="100%" >
                            <tr><td colspan="2" align="center"><h3>Deficiency</h3></td></tr>
                            <tr>
                                <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                                <br/>
                                '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                                <br/>
                                '.$clientResult['User']['site_name'].'
                                <br/>';
                                if(!empty($clientResult['User']['site_country_id'])){
                                                $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                                if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                                $html.= $clientResult['User']['site_city'].' <br/>
                                '.$clientResult['User']['site_phone'].'
                                </td>
                                <td width="50%" align="right" style="padding-right:30px;">
                                    <i><b>Prepared By:</b></i>
                                    <br/>'.
                                    $spResult['Company']['name'].'
                                    <br/>
                                    '. $spResult['User']['address'].'
                                    <br/>
                                    ';	if(!empty($spResult['Country']['name'])){
                                              $html.= $spResult['Country']['name'].', ';
                                            }
                                            if(!empty($spResult['State']['name'])) {
                                               $html.=  $spResult['State']['name'].', ';
                                            }
                                           $html.=  $spResult['User']['city'];
                                    
                                   $html.= ' <br/>
                                    Phone  '.$spResult['User']['phone'].'
                                </td>
                            </tr>
                        </table>
                        <table width="100%" class="tbl input-chngs1">
                            <tr>
                                
                                <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                                <th width="90%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
                                
                            </tr>';
                            
                            if(!empty($Def_record))
                            {
                           // pr($codeData);
                            foreach($Def_record as $Def_record) {
                           
                            $html.='<tr>
                                
                                <td valign="top">'. $Def_record['Code']['code'].'</td>
                                <td valign="top">'. $Def_record['Code']['description'].'</td>
                                
                            </tr>';
                            }
                            } else
                            {
                             $html.= '<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';  
                            }
                            $html.='</table>
                            <table width="100%">
                                <tr>
                                    <td align="center" colspan="2"><h3>Recommendation</h3></td>
                                </tr>
                                <tr>
                                    <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                                    <br/>
                                    '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                                    <br/>
                                    '.$clientResult['User']['site_name'].'
                                    <br/>
                                    ';
                                    if(!empty($clientResult['User']['site_country_id'])){
                                                    $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                                    if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                                    $html.= $clientResult['User']['site_city'].'
                                    <br/>
                                    '.$clientResult['User']['site_phone'].'
                                    </td>
                                    <td width="50%" align="right" style="padding-right:30px;">
                                        <i><b>Prepared By:</b></i>
                                        <br/>
                                        '.$spResult['Company']['name'].'
                                        <br/>
                                        '.$spResult['User']['address'].'
                                        <br/>';
                                            if(!empty($spResult['Country']['name'])){
                                                    $html.= $spResult['Country']['name'].', ';
                                                }
                                                if(!empty($spResult['State']['name'])) {
                                                    $html.= $spResult['State']['name'].', ';
                                                }
                                               $html.=  $spResult['User']['city'];
                                       
                                      $html.=   '<br/>
                                        Phone  '.$spResult['User']['phone'].'
                                    </td>
                                    
                                    </tr>
                            </table>
                                
                            <table width="100%" class="tbl input-chngs1" >
                             <tr width="100%">
                                 
                                 <th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                                 <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
                                  <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>
                                 
                             </tr>';
                             if(!empty($Def_record_recomm))
                             {
                             
                             foreach($Def_record_recomm as $Def_record_recomm) {
                           
                             $html.='<tr width="100%">
                                 
                                 <td valign="top">'.$Def_record_recomm['Code']['code'].'</td>
                                 <td valign="top">'.$Def_record_recomm['Code']['description'].'</td>
                                  <td valign="top">'.$Def_record_recomm['Deficiency']['recommendation'].'</td>
                                 
                             </tr>';
                              }
                             } else
                             {
                              $html.='<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
                             }
                             $html.='</table>
                             
                <table width="100%" class="tbl">
                    <tr width="100%">
                        <td  align=center  style="border:none;"><span style="font-size:15px; font-weight:bolder;"><h3>Signature Page</h3></span></td>                        
                    </tr>                   
                </table>
                <table width="100%">
                    <tr>
                        <td style="border:none;"><b>Inspector Signature</b><br/><br/>';
                        $name=$this->Common->getInspectorSignature($record['ExtinguisherReport']['report_id'],$record['ExtinguisherReport']['servicecall_id'],$record['ExtinguisherReport']['inspector_id']);
                        if($name!='empty'){
                            $html.= '<img src="/app/webroot/img/signature_images/'.$name.'">';
                        }
                        $html.='                        
                        </td>
                        <td style="border:none; vertical-align:top;" align=right><b>Client Signature</b></td>
                  
                    </tr>
                    <tr>
                        <td style="border:none;">';
                        $dateCreated=$this->Common->getSignatureCreatedDate($record['ExtinguisherReport']['report_id'],$record['ExtinguisherReport']['servicecall_id'],$record['ExtinguisherReport']['inspector_id']);
                          
                        if($dateCreated!='empty'){
                        $html.= $this->Common->getClientName($record['ExtinguisherReport']['inspector_id'])."<br/><br/>".
                          $time->Format('m-d-Y',$dateCreated);
                        }
                       $html.='</td>
                        <td align=left style="border:none;"></td>
                  
                    </tr>'
    
    ;
   
	 $html .= '</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
 
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Extinguisher.pdf', 'D');die;