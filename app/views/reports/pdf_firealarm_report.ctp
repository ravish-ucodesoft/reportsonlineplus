 <?php

App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();
$html=       '<table cellpadding="2" width="100%">
      <tr><td colspan="4">
        <table width="100%">
          <tr>
            <td align="center"><img src="/app/webroot/img/company_logo/'.$spResult['Company']['company_logo'].'"></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b>'.$spResult['Company']['name'].'</b></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:21px">';		
		$arr=explode(',',$spResult['Company']['interest_service']);
	    
		    foreach($arr as $key=>$val):
		    foreach($reportType as $key1=>$val1):

		    if($val== $key1)
		    {
			
			 if($key1 == 9){
                            $html.=  $spResult['Company']['interest_service_other'];
                            }else{
                                $html.= $val1;
                                }
                       $html.=' <span class="red"> * </span>';
		     }
		    
		    endforeach;
		    
		    endforeach;	

		 $html.=   '</td></tr>
            
            
          
          
          
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:24px">Firealarm Report</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>          
          <tr>
            <td align="right" class="Cpage" >Date:';if($record['FirealarmReport']['finish_date'] != ''){
                $html.= date('m/d/Y',strtotime($record['FirealarmReport']['finish_date'])); } $html.='</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" ><i>Prepared for:</i></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_name'].'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'; if(!empty($clientResult['User']['site_country_id'])){
                                        $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                        if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                        $html.= $clientResult['User']['site_city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_phone'].'</td>
          </tr>   
          <tr>
            <td align="left" class="Cpage" >Prepared By:</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
          <tr>
            <td align="left" class="Cpage">'.$spResult['Company']['name'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['address'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'; if(!empty($spResult['Country']['name'])){
                                        $html.= $spResult['Country']['name'].', '; }
                                        if(!empty($spResult['State']['name'])) { $html.= $spResult['State']['name'].', '; } 
                                        $html.= $clientResult['User']['city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['phone'].'</td>
          </tr> 
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
         
          
        </table>
      </td>
      </tr>
      <tr><td colspan="4"><h2 align="center">'. ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']) .'</h2></td></tr>
        <tr>
          <td colspan="2" style="text-align:center"><b>Client Info auto-populated</b></td>
          <td colspan="2" style="text-align:center"><b>Service Provider Info auto-populated</b></td>
        </tr>     
        <tr>
          <td style="text-align:right"><strong>Company Name:</strong></td>
          <td>'.$clientResult['User']['site_name'].'</td>
          <td style="text-align:right"><strong>Name of Service Provider:</strong></td>
          <td>'.ucwords($spResult['User']['fname'].' '.$spResult['User']['lname']).'</td>
        </tr>    
        <tr>
          <td style="text-align:right"><strong>Address:</strong></td>
          <td>'.$clientResult['User']['site_address'].'</td>
          <td style="text-align:right"><strong>Address:</strong></td>
          <td>'.$spResult['Company']['address'].'</td>
        </tr>    
        <tr>
          <td style="text-align:right"><strong>City State & Zip:</strong></td>
          <td>'.$clientResult['User']['site_city_state_zip'].'</td>
          <td style="text-align:right"><strong>City State & Zip:</strong></td>
          <td>'.$spResult['User']['site_city_state_zip'].'</td>
        </tr>    
        <tr>
          <td style="text-align:right"><strong>Owner Contact:</strong></td>
          <td>'.$clientResult['User']['responsible_contact'].'</td>
          <td style="text-align:right"><strong>Office Phone:</td>
          <td>'.$spResult['Company']['phone'].'</td>
        </tr>     
        <tr>
          <td style="text-align:right"><strong>Building:</strong></td>
          <td>'.$clientResult['User']['site_name'].'</td>
          <td style="text-align:right"><strong>Office License #</strong></td>
          <td>&nbsp;</td>
        </tr>
      </table>
      </td></tr>
      </table>
      <table>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
      
      
      <table width="100%">
        <tr>
         <td style="text-align:center;color:red;">Device Summary Page Prepared For:</td>        
        </tr>           
  		  <tr>
  			 <td align="center" class="Cpage" >'.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >'.$clientResult['User']['site_name'].'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >';
			 if(!empty($clientResult['User']['site_country_id'])) { ;
			      $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', ';
			      };
  			if(!empty($clientResult['User']['site_state_id'])) { ;
			 $html.=  $this->Common->getStateName($clientResult['User']['site_state_id']).', ';
			  } ;
			$html.=  $clientResult['User']['site_city'] .'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >'.$clientResult['User']['site_phone'].'</td>
  		  </tr>      
      </table>
      <table width="100%" border="1" cellpadding="2">
        <tr>
          <td width="20%" rowspan="2">Device Name</td>
          <td>Surveyed</td>
          <td>Serviced</td>
          <td>Passed</td>
          <td>Deficient</td>
          <td>Recommended</td>
        </tr>
        <tr>
          <td>Qty</td><td>Qty</td><td>Qty</td><td>Qty</td><td>Qty</td>
        </tr>';
        foreach($servicedata as $serviceData){ 
        $surveydata[]= $serviceData['ScheduleService']['amount'];
        $seviceddata[]=  $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
        $passdata[]= $this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
        $deficientdata[]=$this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
        $recommenddata[]=$this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
   
       $html.=' <tr>
          <td>'.$serviceData['Service']['name'].'</td>
          <td>'.$serviceData['ScheduleService']['amount'].'</td>
          <td>'.$this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'.$this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'.$this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'.$this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
        </tr>';
	  } 
       $html.=' <tr>
          <td>Total</td>
          <td>'.array_sum($surveydata).'</td>
          <td>'.array_sum($seviceddata).'</td>
          <td>'.array_sum($passdata).'</td>
          <td>'.array_sum($deficientdata).'</td>
          <td>'.array_sum($recommenddata).'</td>
        </tr>
        </table>	
        <table>
        <tr><td colspan="6">&nbsp;</td></tr>
        <tr>
          <td colspan="6">'.$spResult['Company']['name'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">'.$spResult['Company']['address'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">'.$spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">&nbsp;</td>
		    </tr>
		    
      </table>
      
      
      
      <table border="1" cellpadding="2" width="100%">
    	    <tr width="100%">
        		<td>Functions</td>
        		<td>Description</td>
        		<td>Test Result</td>
    	    </tr>
    	    <tr>
    		<td colspan="3" style="text-align:left;background-color:#3688B7;color:#fff;" >Alarm Functions</td>
    	    </tr>
    	    
    	    <tr>
    		<td>Alarm Bells/Horns/Strobes</td>
    		<td>'. $record['FaAlarmFunction']['description'].'</td>
    		<td>'. $record['FaAlarmFunction']['result'].'</td>
    	    </tr>
    	    
    	     <tr>
    		<td colspan="3" style="text-align:left;background-color:#3688B7;color:#fff;">Alarm Panel Supervisory Functions</td>
    	    </tr>';
    	    foreach($record['FaAlarmPanelSupervisoryFunction'] as $data1){ ;
    	    $html.='<tr>
    	    <td>'. $data1['function_name'].'</td>
    	    <td>'. $data1['description'].'</td>
    	    <td>'. $data1['result'].'</td>
	       </tr>';
    	    };
 	    
    	     $html.=' <tr>
    		<td colspan="3" style="text-align:left; background-color:#3688B7;color:#fff;">Auxiliary Functions</td>
    	    </tr>';
    	    
    	    foreach($record['FaAuxiliaryFunction'] as $data1){ ;
    	     $html.='<tr><td>'. $data1['function_name'].'</td>
    	    <td>'. $data1['description'].'</td>
    	    <td>'. $data1['result'].'</td>
          </tr>';
    	 };
    	    $html.=' <tr>
    		<td colspan="3" style="text-align:left;background-color:#3688B7;color:#fff;">Fire Pump Supervisory Functions</td>
    	    </tr>';
    	    foreach($record['FaPumpSupervisoryFunction'] as $data1){ ;
    	    $html.=' <tr>
    	    <td>'. $data1['function_name'].'</td>
    	    <td>'. $data1['description'].'</td>
    	    <td>'. $data1['result'].'</td>
          </tr>';
    	    } ;
    	    
    	   $html.='  <tr>
    		<td colspan="3" style="text-align:left;background-color:#3688B7;color:#fff;">Generator Supervisory Functions</td>
    	    </tr>';
	   foreach($record['FaGeneratorSupervisoryFunction'] as $data1){ ;
    	    $html.=' <tr>
    	    <td>'. $data1['function_name'].'</td>
    	    <td>'. $data1['description'].'</td>
    	    <td>'. $data1['result'].'</td>
          </tr>';
    	   } ;
    	      
    	$html.=' </table>';
	
	foreach($device_place as $key=>$deviceplace){ ;
      
      $html.='<h3><a href="#">'.$deviceplace .'</a></h3>
      <table border="1" style="width:100%;margin-bottom:10px; border-collapse: collapse;"  >
		<tr>
		<td colspan="9" >Alarm Device List</td>
	    </tr>
	 
	    <tr>
		<td><b>Comment</b></td>
		<td><b>Device Type</b></td>
		<td><b>Family</b></td>
		<td><b>Zone/ Address</b></td>
		<td><b>Location/Description</b></td>
		<td><b>Manufacturer Model</b></td>
		<td><b>Physical Condition</b></td>
		<td><b>Service</b></td>
		<td><b>Functional Test</b></td>		
	    </tr>';

	    if(!empty($record['FaLocationDescription'])){ 
	    foreach($record['FaLocationDescription'] as $data){
	    if($data['fa_alarm_device_place_id'] == $key ){ ;
	  $html.='  <tr>
	     <td>'.$data['comment'].'</td>
	      <td>';
	      
	      foreach($device_type as $keydata => $valdata){
	     if($keydata == $data['fa_alarm_device_type_id']){ ;
       $html.= $valdata;
        } } ;
	$html.=' </td>
        <td>'.$data['family'].'</td>
		    <td>'. $data['zone_address'].'</td>
		    <td>'. $data['location'].'</td>
		    <td>'. $data['model'].'</td>
		    <td>'. $data['physical_condition'].'</td>
		    <td>'. $data['service'].'</td>
		    <td>'. $data['functional_test'].'</td>
	    
      </tr>';
    } } }else{ ;
  $html.='  <tr><td colspan="10">NO RECORD FOUND</td></tr>';
   } ;
		
	   $html.='  </table>';
      } ;
      
      
      
      
      $html.='  <table>
                  	<tr>
                  	    <td colspan="6">&nbsp;</td>	    
                  	</tr>
                  	<tr>
                  	    <td colspan="6">&nbsp;</td>	    
                  	</tr>
                    <tr>
                  	    <td colspan="6"><u>Attachments</u>:</td>	    
                  	</tr>	'; 
                    foreach($attachmentdata as $attachdata){
                   	$html .='<tr>
                  	    <td colspan="2">'.$attachdata['Attachment']['attach_file'].'</td>	
                        <td>'.date('m/d/Y',strtotime($attachdata['Attachment']['created'])).'</td>	
                        <td colspan="2">'.$this->Common->getClientName($attachdata['Attachment']['user_id']).'('.$attachdata['Attachment']['added_by'].')</td>	 
                        <td>&nbsp;</td>	         
                  	</tr>'; }
                    	$html .='<tr>
                  	    <td colspan="6">&nbsp;</td>	    
                  	</tr>
                    <tr>
                  	    <td colspan="6"><u>Notifications</u>:</td>	    
                  	</tr>';
                    foreach($notifierdata as $notifydata){
                    $html .='<tr>
                  	    <td colspan="2">'.$notifydata['Notification']['notification'].'</td>	
                        <td>'.date('m/d/Y',strtotime($notifydata['Notification']['created'])).'</td>	
                        <td colspan="2">'.$this->Common->getClientName($notifydata['Notification']['user_id']).'('.$notifydata['Notification']['added_by'].')</td>	 
                        <td>&nbsp;</td>	         
                  	</tr>'; }
                        $html .= '</table>
      
      
      
      <table width="100%" cellpadding="0" cellspacing="0">        
        <tr>
	    <td width="100%" class="brdr" valign="top" colspan="3" style="border:1px solid black">
                <table width="100%" >
                <tr><td colspan="2" align="center"><h3>Deficiency</h3></td></tr>
                    <tr>
                        <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                        <br/>
                        '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                        <br/>
                        '.$clientResult['User']['site_name'];
                       
                        if(!empty($clientResult['User']['site_country_id'])){ ;
			 
                         $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', ';
					
			 } ;
                         if(!empty($clientResult['User']['site_state_id'])) { ;
			 
			 $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', ';
			 }; 
                         $html.= $clientResult['User']['site_city'].' <br/>
                        '.$clientResult['User']['site_phone'].'
                        </td>
                        <td width="50%" align="right" style="padding-right:30px;">
                            <i><b>Prepared By:</b></i>
                            <br/>'.
                            $spResult['Company']['name'].'
                            <br/>
                            '. $spResult['User']['address'].'
                            <br/>
                            ';	if(!empty($spResult['Country']['name'])){
                                      $html.= $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) {
                                       $html.=  $spResult['State']['name'].', ';
                                    }
                                   $html.=  $spResult['User']['city'];
                            
                           $html.= ' <br/>
                            Phone  '.$spResult['User']['phone'].'
                        </td>
                    </tr>
                </table>
		
                <table width="100%" class="tbl input-chngs1">
                    <tr>
                        
                        <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                        <th width="90%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
                        
                    </tr>';
                    
                    if(!empty($Def_record))
                    {
                   // pr($codeData);
                    foreach($Def_record as $Def_record) {
                   
                    $html.='<tr>
                        
                        <td valign="top">'. $Def_record['Code']['code'].'</td>
                        <td valign="top">'. $Def_record['Code']['description'].'</td>
                        
                    </tr>';
                    }
                    } else
                    {
                     $html.= '<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';  
                    }
                   
                $html.= '</table>       
                
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
	  <tr>
	    <td width="100%" class="brdr" valign="top" colspan="3" style="border:1px solid black">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="2"><h3>Missing Device Page</h3></td>
                    </tr>
        
                </table>
		<table width="100%">
        <tr>
         <td style="text-align:center;color:red;">Missing Device Page Prepared For:</td>        
        </tr>           
  		  <tr>
  			 <td align="center" class="Cpage" >'. ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >'. $clientResult['User']['site_name'].'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >';
			 if(!empty($clientResult['User']['site_country_id'])) { ;
			 $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', ';
			 }
  			if(!empty($clientResult['User']['site_state_id'])) { ;
			  $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', ';
			 } ;
			  $html.=  $clientResult['User']['site_city'].'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >'. $clientResult['User']['site_phone'].'</td>
  		  </tr>      
      </table>
      <table width="100%" class="qtytable" border="1">
        <tr>
          <td rowspan="2">Device Name</td>
          <td>Surveyed</td>
          <td>Serviced</td>
          <td>Passed</td>
          <td>Deficient</td>
          <td>Recommended</td>
          <td>Missing</td>
        </tr>
        <tr>
          <td>Qty</td><td>Qty</td><td>Qty</td><td>Qty</td><td>Qty</td><td>Qty</td>
        </tr>';
       foreach($servicedata as $serviceData){ 
        if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id'])< $serviceData['ScheduleService']['amount']){ ;
        $html.='<tr>
          <td>'. $serviceData['Service']['name'].'</td>
          <td>'. $serviceData['ScheduleService']['amount'].'</td>
          <td>'. $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'. $this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'. $this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'. $this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>';
        $missing = $serviceData['ScheduleService']['amount'] - $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']) ;
         $html.=' <td>'. $missing.'</td>
        </tr> ' ;
      } ;
        }  ;
          $html.='</table>
        <table>
        <tr><td colspan="6">&nbsp;</td></tr>
        <tr>
          <td colspan="6">'. $spResult['Company']['name'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">'. $spResult['Company']['address'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">'. $spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'].'</td>
		    </tr>		 
      </table>
            </td>
        </tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	        <tr><td colspan="3">&nbsp;</td></tr>
	  <tr>
	    <td width="100%" class="brdr" valign="top" colspan="3" style="border:1px solid black">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="2"><h3>Additional Device Page</h3></td>
                    </tr>
        
                </table>
		<table width="100%">
        <tr>
         <td style="text-align:center;color:red;">Additional Device Page Prepared For:</td>        
        </tr>           
  		  <tr>
  			 <td align="center" class="Cpage" >'. ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >'. $clientResult['User']['site_name'].'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >';
			 if(!empty($clientResult['User']['site_country_id'])) { ;
			 $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', ';
			 }
  			if(!empty($clientResult['User']['site_state_id'])) { ;
			  $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', ';
			 } ;
			  $html.=  $clientResult['User']['site_city'].'</td>
  		  </tr>
  		  <tr>
  			 <td align="center" class="Cpage" >'. $clientResult['User']['site_phone'].'</td>
  		  </tr>      
      </table>
      <table width="100%" class="qtytable" border="1">
        <tr>
          <td rowspan="2">Device Name</td>
          <td>Surveyed</td>
          <td>Serviced</td>
          <td>Passed</td>
          <td>Deficient</td>
          <td>Recommended</td>
          <td>Additional</td>
        </tr>
        <tr>
          <td>Qty</td><td>Qty</td><td>Qty</td><td>Qty</td><td>Qty</td><td>Qty</td>
        </tr>';
       foreach($servicedata as $serviceData){ 
        if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']) > $serviceData['ScheduleService']['amount']){ ;
        $html.='<tr>
          <td>'. $serviceData['Service']['name'].'</td>
          <td>'. $serviceData['ScheduleService']['amount'].'</td>
          <td>'. $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'. $this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'. $this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
          <td>'. $this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>';
        $additional = $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']) -  $serviceData['ScheduleService']['amount'] ;
         $html.=' <td>'. $additional.'</td>
        </tr> ' ;
      } ;
        }  ;
          $html.='</table>
        <table>
        <tr><td colspan="6">&nbsp;</td></tr>
        <tr>
          <td colspan="6">'. $spResult['Company']['name'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">'. $spResult['Company']['address'].'</td>
		    </tr>
		    <tr>
			   <td colspan="6">'. $spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'].'</td>
		    </tr>		 
      </table>
            </td>
        </tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3" style="border:1px solid black">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="2"><h3>Recommendation</h3></td>
                    </tr>
                    <tr>
                        <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                        <br/>
                        '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                        <br/>
                        '.$clientResult['User']['site_name'].'
                        <br/>
                        ';
                        if(!empty($clientResult['User']['site_country_id'])){
                                        $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                        if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                        $html.= $clientResult['User']['site_city'].'
                        <br/>
                        '.$clientResult['User']['site_phone'].'
                        </td>
                        <td width="50%" align="right" style="padding-right:30px;">
                            <i><b>Prepared By:</b></i>
                            <br/>
                            '.$spResult['Company']['name'].'
                            <br/>
                            '.$spResult['User']['address'].'
                            <br/>';
                             	if(!empty($spResult['Country']['name'])){
                                        $html.= $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) {
                                        $html.= $spResult['State']['name'].', ';
                                    }
                                   $html.=  $spResult['User']['city'];
                           
                          $html.=   '<br/>
                            Phone  '.$spResult['User']['phone'].'
                        </td>
                        
                        </tr>
                </table>
                    
                   <table width="100%" class="tbl input-chngs1" >
                    <tr width="100%">
                        
                        <th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                        <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
                         <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>
                        
                    </tr>';
                    if(!empty($Def_record_recomm))
                    {
                    
                    foreach($Def_record_recomm as $Def_record_recomm) {
                  
                    $html.='<tr width="100%">
                        
                        <td valign="top">'.$Def_record_recomm['Code']['code'].'</td>
                        <td valign="top">'.$Def_record_recomm['Code']['description'].'</td>
                         <td valign="top">'.$Def_record_recomm['Deficiency']['recommendation'].'</td>
                        
                    </tr>';
                     }
                    } else
                    {
                     $html.='<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';   
                    }
                   
                 $html.='</table>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3" style="border:1px solid black">
                <table width="100%" class="tbl">
                    <tr>
                        <td width="100%" align=center  style="border:none;"><span style="font-size:15px; font-weight:bolder;"><h3>Signature Page</h3></span></td>                        
                    </tr>                   
                </table>
                <table width="100%">
                    <tr>
                        <td style="border:none;"><b>Inspector Signature</b><br/><br/>';
                        $name=$this->Common->getInspectorSignature($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id']);
                        if($name!='empty'){
                            $html.= '<img src="/app/webroot/img/signature_images/'.$name.'">';
                        }
                        $html.='                        
                        </td>
                        <td style="border:none; vertical-align:top;" align=right><b>Client Signature</b></td>
                  
                    </tr>
                    <tr>
                        <td style="border:none;">';
                        $dateCreated=$this->Common->getSignatureCreatedDate($record['FirealarmReport']['report_id'],$record['FirealarmReport']['servicecall_id'],$record['FirealarmReport']['inspector_id']);
                          
                        if($dateCreated!='empty'){
                        $html.= $this->Common->getClientName($record['FirealarmReport']['inspector_id'])."<br/><br/>".
                          $time->Format('m-d-Y',$dateCreated);
                        }
                       $html.='</td>
                        <td align=left style="border:none;"></td>
                  
                    </tr>
                    
                </table>
            </td>
        </tr>
 ';

 $html .= '</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
 
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Firealarm_Report.pdf', 'D');die;