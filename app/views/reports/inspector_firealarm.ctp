<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    echo $this->Html->css('reports');
    echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox'));
    $FaAlarmDeviceType = '';
    foreach($device_type as $skey=>$delist):
    	$FaAlarmDeviceType .= '<option value="'.$skey.'">'.$delist."</option>"; 
    endforeach;
?>
<style>
.summary-tbl2 p {
    float: left;
    margin-bottom: 0;
    margin-left: 5px;
    width: 10%;
    font-size: 10px;
    padding-left: 7px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
.lbtxt{text-align:left; font-weight:bold; width:auto !important; }
.lb1txt{text-align:right;width:20%}
.input-chngs1 input {
    margin-right: 3px;
    width: 136px;
}
</style>
<style>
.tdLabel {font-size:16px;font-weight:bold;text-align:center;}
.brdr {border:2px solid #666666;}
.input-chngs1 input {
    margin-right: 3px;
    width: 296px;
}
.lbl{color:#666666;font-size:11px;text-align:left;}
td{
	padding:4px;
}
.firealramreport td{ border: 1px solid #000000;}
.alramreporttest{text-align:left;background-color:#CCCCCC;}
#accordionlist input {   
    width: 120px;
}

table td label{
    float: none;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	 jQuery(".ajax").colorbox();
	 jQuery(".inline").colorbox({inline:true, width:"50%"});
	 <?php
	   if(isset($this->data['FirealarmReport']['finish_date']) && ($this->data['FirealarmReport']['finish_date'] != "")){ ?>
	   
	    for(i = 0; i < document.getElementById('fareportid').length; i++){
		var formElement = document.getElementById('fareportid').elements[i];	
		    formElement.disabled = true;
	    }
	    
  <?php } ?>
	 
});
$(function(){
    // Accordion
    $("#accordionlist").accordion({ header: "h3"});
   
       
});
    function showbox(divid,id)
    { 
	var checkid= "service_"+divid+"_"+id;
    if(document.getElementById(checkid).checked){
	$("#others_"+divid+"_"+id).fadeIn(500);
    }else{
	$("#others_"+divid+"_"+id).fadeOut(500);  
    }
    }
 function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
</script>
<script type="text/javascript">

function createUploader(){	    
         var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
	    }
            });           
    }        
window.onload = createUploader;

/*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
function confirmbox(rId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveRecord(rId);    
  }
  else{
    return false;
  }
 }
function RemoveRecord(rId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delFaLocDescription/" + rId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#trloc_'+rId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}
function AddMore(divid){	
   var counter = jQuery('#optioncount').val();
    
    counter = parseInt(counter) + 1;
    jQuery('#optioncount').val(counter);
    OptionHtml ='<tr id="option_'+counter+'">';
    OptionHtml = OptionHtml+'<input type="hidden" name="data[FaLocationDescription]['+divid+']['+counter+'][location_id]"  id="test1" style="" value="'+divid+'" >';
    OptionHtml = OptionHtml+'<td><input name="data[FaLocationDescription]['+divid+']['+counter+'][check]" id="service_'+divid+'_'+counter+'" type="checkbox" value="" class="checkbox" style="width:20px;" onclick = "showbox('+divid+','+counter+')" /></td>';
    OptionHtml = OptionHtml+'<td><select name="data[FaLocationDescription]['+divid+']['+counter+'][fa_alarm_device_type_id]" id="FaAlarmDeviceType" style="width:80px"><option value="0">Please Select</option><?php echo $FaAlarmDeviceType ; ?></select></td>';
    OptionHtml = OptionHtml+'<td><select name="data[FaLocationDescription]['+divid+']['+counter+'][family]" id="functional_test" style="width:80px"><option value="Control">Control</option><option value="Notification">Notification</option><option value="Initiating">Initiating</option><option value="Monitor">Monitor</option><option value="Supervisory">Supervisory</option><option value="Auxiliary">Auxiliary</option><option value="Supplemental">Supplemental</option></select></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[FaLocationDescription]['+divid+']['+counter+'][zone_address]"  id="test3" style="" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[FaLocationDescription]['+divid+']['+counter+'][location]"  id="test2" style="" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[FaLocationDescription]['+divid+']['+counter+'][model]"  id="test1" style="" ></td>';
    OptionHtml = OptionHtml+'<td><select name="data[FaLocationDescription]['+divid+']['+counter+'][physical_condition]" id="physical_condition" style="width:80px" ><option value="Excellent">Excellent</option><option value="Good">Good</option><option value="Poor">Poor</option></select></td>';
    OptionHtml = OptionHtml+'<td><select name="data[FaLocationDescription]['+divid+']['+counter+'][service]" id="physical_condition" style="width:80px"><option value="Tested">Tested</option><option value="Tested &amp; Cleaned">Tested &amp; Cleaned</option><option value="Visually Inspected">Visually Inspected</option><option value="Not Tested">Not Tested</option><option value="Repaired">Repaired</option></select></td>';
    OptionHtml = OptionHtml+'<td><select name="data[FaLocationDescription]['+divid+']['+counter+'][functional_test]" id="functional_test" style="width:80px"><option value="Pass">Pass</option><option value="Deficient">Deficient</option><option value="Recommend">Recommend</option></select></td>';
    OptionHtml = OptionHtml+'<td><a href="javascript:void(0)" onclick="RemoveRow('+divid+','+counter+')"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></td>';    
    OptionHtml = OptionHtml+'</tr>';
    OptionHtml = OptionHtml+'<tr style="display:none;" id="others_'+divid+'_'+counter+'">';
    OptionHtml = OptionHtml+'<td colspan="10"><input type="text" name="data[FaLocationDescription]['+divid+']['+counter+'][comment]"  id="test3" style="width:200px" ></td>';
    OptionHtml = OptionHtml+'</tr>';
    jQuery('#alarm_device_list'+divid).append(OptionHtml);
     
    
}
function RemoveRow(divid,counter){
    jQuery('#option_'+counter).remove();
    jQuery('#others_'+divid+'_'+counter).remove();
}

function RemoveAttachment(attachId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}
 

 function checkUploadedAttachment(){
  
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
  function checkNotification(){
	var notifyVal = jQuery("#ExtinguisherReportNotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }

function confirmattach(attachId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId);    
  }
  else{
    return false;
  }
 }
function RemoveNotifier(notifierId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delnotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#pnotify_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId);    
  }
  else{
    return false;
  }
 } 

</script>

<div id="content" >
    <div class='message'><?php echo $this->Session->flash();?></div>
    <div id="deleteFlashmessage" style="display:none;">
	<div class="notification msginfo">
	    <a class="close"></a>
	    <p style="text-align:center;color:red">Record has been deleted successfully</p>
	</div>
    </div>
    <div id="box">
	<div style="width: 100%">
	    <?php //echo $html->link($html->image("pdf.png",array('title'=>'Print Preview')), 'reportPreview?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
	    <?php echo $html->link($html->image("fineimages/pdf_preview.png",array('title'=>'Print Preview')), 'firealarmReportPreview?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
	</div>
        <h3 align="center" >Fire Alarm Report</h3>
        <h4 class="table_title">Agreement and Report of Inspection</h4>
	<?php
	    $actionUrl = 'firealarm?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'];
	?>
	<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>$actionUrl,'name'=>'specialhazardReport','id'=>'fareportid','inputDefaults' => array('label' => false,'div' => false))); ?>
        <table cellpadding="0" cellspacing="0" border="0" class="table_new">
	    <tr>
		<td>
		    <b>Days Left for report Submission: <?php echo $daysRemaining;?></b>
		</td>
                <!--<td align="right">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
			    <td width="100%" align="right">
				<?php
				    $options=array("monthly"=>"Monthly","Quarterly"=>"Quarterly","semi_annual"=>"Semi-Annual","Annual"=>"Annual");
				    $attributes=array('legend'=>false,'separator'=>'&nbsp;&nbsp;','class'=>'validate[required]');
				    //echo $this->Form->radio('FirealarmReport.inspection_type',$options,$attributes);
				?>				    
			    </td>                                
                        <tr>
                    </table>
                </td>-->
            </tr>
	    <?php
		$certs = $this->Common->getAllSchCerts($scheduleId);//echo '<pre>';print_r($certs);die;
		if(sizeof($certs)>0){
	    ?>
	    <tr><td colspan="2">
	    <div class="certificates-hdline"><?php echo $html->image('fineimages/cert_attached.png');?></div>
	    <ul class="services-tkn"><li>
	    <?php $cr=1;
	    foreach($certs as $cert)
	    {
		    echo $html->link($cert['SpBlankCertForm']['title'],array("controller"=>"messages","action"=>"download_cert_form",$cert['SpBlankCertForm']['cert_form']),array('title'=>'DownLoad/View'));
		    if(!(isset($this->data['FirealarmReport']['finish_date']) && ($this->data['FirealarmReport']['finish_date'] != ""))){
			echo '<div style="padding-left:20px">'.$html->link($html->image('submit.gif',array('style'=>'float:none')),array('controller'=>'messages','action'=>'uploadReportCert',$cert['ReportCert']['id']),array('escape'=>false,'class'=>'ajax')).'</div>';
		    }	
	    ?>
	    <?php if($cr%4==0){
		    echo '</li></ul><ul class="services-tkn"><li>';
	    }
	    else{
		    echo '</li><li>';
	    }
	     $cr++;
	    }
	    ?>		
	    </td></tr>
	    <?php						
	    }		
	    ?>
            <tr>
	        <th colspan="2" align="left">Client information</th>
	    </tr>
            <tr>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="table_label"><strong>Building Information</strong></td>
                            <td><?php echo $clientResult['User']['client_company_name'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $clientResult['User']['address'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <?php
				$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
			    ?>
                            <td> <?php echo $cityName.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Country: </strong></td>
                            <td><?php echo $clientResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="table_label"><strong>Contact: </strong></td>
                            <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Phone:</strong></td>
                            <td><?php echo $clientResult['User']['phone'];?></td>
                        </tr>                               
                        <tr>
                            <td class="table_label"><strong>Email: </strong></td>
                            <td> <?php echo $clientResult['User']['email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
	    <tr>
		<th colspan="2" align="left">Site Information</th>
	    </tr>
            <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Site Name:</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <?php
				$siteAddrCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
			    ?>
			    <td> <?php echo $siteAddrCityName.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Country: </strong></td>
                            <td><?php echo $siteaddrResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Contact: </strong></td>
                            <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                        </tr>                               
                        <tr>
			    <td class="table_label"><strong>Email: </strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2" align="left">Service Provider Info</th></tr>
            <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Name:</strong></td>
                            <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $spResult['User']['address'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <?php
				$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
			    ?>
			    <td><?php echo $spCityName.', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Country: </strong></td>
			    <td><?php echo $spResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Contact: </strong></td>
                            <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
			    <td><?php echo $spResult['User']['phone'];?></td>
                        </tr>                               
                        <tr>
			    <td class="table_label"><strong>Email: </strong></td>
			    <td><?php echo $spResult['User']['email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div style="border:4px solid #000; padding:10px; margin:10px">
	    <?php echo $texts;?>
        </div>
	<div style="float:right;padding-right:10px;">
	    <?php echo $html->link('Add Explanation',array('controller'=>'inspectors','action'=>'addExplanation?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID']),array('class'=>'ajax'));?>
	</div>
	<!--<div style="float:right;padding-right:10px;">				  
	<?php
	    $permissionfill=$this->Common->checkPermission(1,$sessionInspectorId);    
	    //if($session_inspector_id!=$schedule['lead_inspector_id'])
	    if($permissionfill!=1)
	    {
        ?>
                <a href="javascript:void(0)" title="Add Attachment"  class="linktoreport" onclick="alert('<?php echo $permissionfill;?>')">Add Attachment</a><span style='vertical-align:bottom'><?php echo $html->image('icon_attachment.gif');?></span>                                        
        <?php } else {	?>
                <?php //echo $html->link('Add Attachment','#inline_content_attachment',array('class' => 'inline'));?><?php echo $html->image('icon_attachment.gif');?>&nbsp;&nbsp;&nbsp;
        <?php } ?>
        <?php
	    $permissionfillnotify=$this->Common->checkPermission(2,$sessionInspectorId);    
	    //if($session_inspector_id!=$schedule['lead_inspector_id'])
	    if($permissionfillnotify!=1)
	    {
        ?>
		<a href="javascript:void(0)" title="Add Notification"  class="linktoreport" onclick="alert('<?php echo $permissionfillnotify;?>')">Add Notification</a><span style='vertical-align:bottom'><?php echo $html->image('icon_notify.gif');?></span>
                                        
        <?php } else {	?>
                <?php //echo $html->link('Add Notifier',"#inline_content_notification",array('class' => 'inline'));?><?php echo $html->image('icon_notify.gif');?>
        <?php }?>
	</div>-->			
	<?php echo $form->input('FirealarmReport.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
	<?php echo $form->input('FirealarmReport.inspector_id',array('type'=>'hidden','value'=>$sessionInspectorId)); ?>
	<?php echo $form->input('FirealarmReport.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
	<?php echo $form->input('FirealarmReport.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
	<?php echo $form->input('FirealarmReport.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
	<?php echo $form->input('FirealarmReport.id',array('type'=>'hidden','value'=>$this->data['FirealarmReport']['id'])); ?>
	<?php echo $this->Form->hidden('FirealarmReport.optioncount',array('value'=>'1','id'=>'optioncount')); ?>
	
	<table style='margin-left:50px;width: 90%;'  class='tbl input-chngs1 firealramreport' >
	    <tr>
		<td>Functions</td>
		<td>Description</td>
		<td>Test Result</td>
	    </tr>
	    <tr>
		<td colspan='3' class='alramreporttest'>Alarm Functions</td>
	    </tr>
	    
	    <tr>
		<td>Alarm Bells/Horns/Strobes<?php echo $form->hidden('FaAlarmFunction.function_name',array('value'=>'Alarm Bells/Horns/Strobes')); ?></td>
		<td><?php echo $form->input('FaAlarmFunction.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmFunction.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	     <tr>
		<td colspan='3' class='alramreporttest'>Alarm Panel Supervisory Functions</td>
	    </tr>
	    
	    <tr>
		<td>Panel AC Power Loss<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.0.function_name',array('value'=>'Panel AC Power Loss')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.0.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.0.result',array('readonly'=>false,'type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	    
	    <tr>
		<td>Panel Sec Power Loss<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.1.function_name',array('value'=>'Panel Sec Power Loss')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.1.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.1.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	    
	    <tr>
		<td>Open Alarm Circuits<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.2.function_name',array('value'=>'Open Alarm Circuits')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.2.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.2.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Short Alarm Circuits<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.3.function_name',array('value'=>'Short Alarm Circuits')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.3.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.3.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    <tr>
		<td>Panel to Panel Circuits<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.4.function_name',array('value'=>'Panel to Panel Circuits')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.4.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.4.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Ground Faults Detected<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.5.function_name',array('value'=>'Ground Faults Detected')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.5.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.5.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	     <tr>
		<td colspan='3' class='alramreporttest'>Auxiliary Functions</td>
	    </tr>
	    
	    <tr>
		<td>Remote Annunciator<?php echo $form->hidden('FaAuxiliaryFunction.0.function_name',array('value'=>'Remote Annunciator')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.0.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.0.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	     <tr>
		<td>Stair Pressurization<?php echo $form->hidden('FaAuxiliaryFunction.1.function_name',array('value'=>'Stair Pressurization')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.1.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.1.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	     <tr>
		<td>Elevator Recall<?php echo $form->hidden('FaAuxiliaryFunction.2.function_name',array('value'=>'Elevator Recall')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.2.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.2.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	     <tr>
		<td>Elevator Recall Primary Fl<?php echo $form->hidden('FaAuxiliaryFunction.3.function_name',array('value'=>'Elevator Recall Primary Fl')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.3.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.3.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>HVAC Shutdown<?php echo $form->hidden('FaAuxiliaryFunction.4.function_name',array('value'=>'HVAC Shutdown')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.4.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.4.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    <tr>
		<td>High/Low Air<?php echo $form->hidden('FaAuxiliaryFunction.5.function_name',array('value'=>'High/Low Air')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.5.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.5.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Building Temp<?php echo $form->hidden('FaAuxiliaryFunction.6.function_name',array('value'=>'Building Temp')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.6.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.6.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    <tr>
		<td>Site Water Temp<?php echo $form->hidden('FaAuxiliaryFunction.7.function_name',array('value'=>'Site Water Temp')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.7.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.7.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Site Water Level<?php echo $form->hidden('FaAuxiliaryFunction.8.function_name',array('value'=>'Site Water Level')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.8.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.8.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	     <tr>
		<td colspan='3' class='alramreporttest'>Fire Pump Supervisory Functions</td>
	    </tr>
	    
	     <tr>
		<td>Fire Pump Power<?php echo $form->hidden('FaPumpSupervisoryFunction.0.function_name',array('value'=>'Fire Pump Power')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.0.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.0.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Fire Pump Running<?php echo $form->hidden('FaPumpSupervisoryFunction.1.function_name',array('value'=>'Fire Pump Running')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.1.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.1.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	      <tr>
		<td>Fire Pump Phase Reversal<?php echo $form->hidden('FaPumpSupervisoryFunction.2.function_name',array('value'=>'Fire Pump Phase Reversal')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.2.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.2.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>
	      
	     </tr>
	      <tr>
		<td>Fire Pump Auto Position<?php echo $form->hidden('FaPumpSupervisoryFunction.3.function_name',array('value'=>'Fire Pump Auto Position')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.3.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.3.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>  
	    
	    <tr>
		<td>Fire Pump or Pump Controller Trouble<?php echo $form->hidden('FaPumpSupervisoryFunction.4.function_name',array('value'=>'Fire Pump or Pump Controller Trouble')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.4.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.4.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>   
	     <tr>
		<td colspan='3' class='alramreporttest'>Generator Supervisory Functions</td>
	    </tr>
	     
	      <tr>
		<td>Generator In Auto Position<?php echo $form->hidden('FaGeneratorSupervisoryFunction.0.function_name',array('value'=>'Generator In Auto Position')); ?></td>
		<td><?php echo $form->input('FaGeneratorSupervisoryFunction.0.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaGeneratorSupervisoryFunction.0.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>  
	    
	    <tr>
		<td>Generator or Controller Trouble<?php echo $form->hidden('FaGeneratorSupervisoryFunction.1.function_name',array('value'=>'Generator or Controller Trouble')); ?></td>
		<td><?php echo $form->input('FaGeneratorSupervisoryFunction.1.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaGeneratorSupervisoryFunction.1.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>   
	    <tr>
		<td>Switch Transfer<?php echo $form->hidden('FaGeneratorSupervisoryFunction.2.function_name',array('value'=>'Switch Transfer')); ?></td>
		<td><?php echo $form->input('FaGeneratorSupervisoryFunction.2.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaGeneratorSupervisoryFunction.2.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>  
	    
	    <tr>
		<td>Generator Engine Running<?php echo $form->hidden('FaGeneratorSupervisoryFunction.3.function_name',array('value'=>'Generator Engine Running')); ?></td>
		<td><?php echo $form->input('FaGeneratorSupervisoryFunction.3.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaGeneratorSupervisoryFunction.3.result',array('type'=>'select','label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false)); ?></td>
	    </tr>    
	
	 
	</table>
	 <?php if(!(isset($this->data['FirealarmReport']['finish_date']) && ($this->data['FirealarmReport']['finish_date'] != ""))){?>
	 <table style='width:90%;margin-left:50px;'>
	    <tr>
		<td><?php echo $html->link('ADD NEW DEVICE LOCATION',array('controller'=>'reports','action'=>'addFireAlarmLoc?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),array('escape'=>false,'class'=>'ajax'));?></td>	
	    </tr>    
	 </table>
	 <?php }?>
	    <div id="accordionlist" style='width:90%;margin-left:50px;'>
		 
	    <?php
		if(!empty($device_place))
		{
		foreach($device_place as $key=>$deviceplace): ?>
		<div>
		<h3><a href="#"><?php echo $deviceplace .$form->hidden('FaLocationDescription.'.$key.'.1.location_id',array('value'=>$key)); ?></a></h3>
		
		<table style='width:100%;margin-bottom:10px;' class='tbl input-chngs1 firealramreport' id='alarm_device_list<?php echo $key; ?>' >
		<tr>
		<td colspan='10' class='alramreporttest'>Alarm Device List</td>
	    </tr>
	 
	    <tr>
	  <td><b>Comment</b></td>
		<td><b>Device Type</b></td>
		<td><b>Family</b></td>
		<td><b>Zone/ Address</b></td>
		<td><b>Location/Description</b></td>
		<td><b>Manufacturer Model</b></td>
		<td><b>Physical Condition</b></td>
		<td><b>Service</b></td>
		<td><b>Functional Test</b></td>
		<td>&nbsp;</td>
	    </tr>
	    <?php if(!empty($this->data['FirealarmReport']['id'])){ 
	    foreach($this->data['FaLocationDescription'] as $data){
	    if($data['fa_alarm_device_place_id'] == $key ){ ?>
	    <tr id="trloc_<?php echo $data['id']; ?>">
	      <td><?php echo $data['comment'];?></td>
	      <td><?php foreach($device_type as $keydata => $valdata){
	     if($keydata == $data['fa_alarm_device_type_id']){ 
         echo $valdata;
        } } ?></td>
        <td><?php echo $data['family'];?></td>
		    <td><?php echo $data['zone_address'];?></td>
		    <td><?php echo $data['location'];?></td>
		    <td><?php echo $data['model'];?></td>
		    <td><?php echo $data['physical_condition'];?></td>
		    <td><?php echo $data['service'];?></td>
		    <td><?php echo $data['functional_test'];?></td>
		    <td><a href="javascript:void(0)" onclick="confirmbox('<?php echo $data['id']; ?>')"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></td>
	    
      </tr>
    <?php } }} ?>
		<tr>
		    <td><input name="data[check]" id="service_<?php echo $key; ?>_1" type="checkbox" value="" class="checkbox" style="width:20px;" onclick = "showbox(<?php echo $key; ?>,1)" /></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.fa_alarm_device_type_id',array('type'=>'select','div'=>false,'label'=>false,'options'=>$device_type,'empty'=>'Please Select','style'=>'width:80px'));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.family',array('type'=>'select','div'=>false,'label'=>false,'options'=>array('Control'=>'Control','NotificationN'=>'Notification','Initiating'=>'Initiating','Monitor'=>'Monitor','Supervisory'=>'Supervisory','Auxiliary'=>'Auxiliary','Supplemental'=>'Supplemental'),'empty'=>false,'style'=>'width:80px'));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.zone_address',array('type'=>'text','div'=>false,'label'=>false));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.location',array('type'=>'text','div'=>false,'label'=>false));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.model',array('type'=>'text','div'=>false,'label'=>false));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.physical_condition',array('type'=>'select','div'=>false,'label'=>false,'options'=>array('Excellent'=>'Excellent','Good'=>'Good','Poor'=>'Poor'),'empty'=>false,'style'=>'width:80px'));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.service',array('type'=>'select','div'=>false,'label'=>false,'options'=>array('Tested'=>'Tested','Tested & Cleaned'=>'Tested & Cleaned','Visually Inspected'=>'Visually Inspected','Not Tested'=>'Not Tested','Repaired'=>'Repaired'),'empty'=>false,'style'=>'width:80px'));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.functional_test',array('type'=>'select','div'=>false,'label'=>false,'options'=>array('Pass'=>'Pass','Deficient'=>'Deficient','Recommend'=>'Recommend'),'empty'=>false,'style'=>'width:80px'));?></td>
		    <td>&nbsp;</td>
		</tr>
		<tr style= "display:none" id="others_<?php echo $key; ?>_1">
		  <td colspan='10' ><?php echo $form->input('FaLocationDescription.'.$key.'.1.comment',array('type'=>'text','div'=>false,'label'=>false,'style'=>'width:200px'));?></td>
    </tr>
		<?php if(!(isset($this->data['FirealarmReport']['finish_date']) && ($this->data['FirealarmReport']['finish_date'] != ""))){?>
		<tr>
		    <td colspan='10' style='text-align:right;'><?php echo $html->link('ADD MORE','javascript:void(0);',array('escape'=>false,'onclick'=>"AddMore('".$key."');"));?></td>
		</tr>
		<?php }?>
	     </table>
	    </div>
	    <?php endforeach; } ?>
	    <table>
  	    <tr><td>&nbsp;</td></tr>
        <tr><td align="center">
        <?php
      	$permissionfilldef=$this->Common->checkPermission(6,$sessionInspectorId);    
      	//if($session_inspector_id!=$schedule['lead_inspector_id'])
      	if($permissionfilldef!=1)
      	{
      	?>
      	    <a href="javascript:void(0)" title="Add Deficiency"  class="linktoreport" onclick="alert('<?php echo $permissionfilldef;?>')"><span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span></a>
      		
      	<?php } else {								
      	?>
      	 <?php //echo $html->link('<span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span>',array('controller'=>'reports','action'=>'defAndRec?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),array('class' => 'ajax','escape'=>false),false); ?> 
          <?php
          } 
          ?>
       </td></tr>
        <tr><td>&nbsp;</td></tr>
	    </table>
		<table>
		
			<tr>      
				  <td>
					<?php if(isset($this->data['FirealarmReport']['finish_date']) && ($this->data['FirealarmReport']['finish_date'] != "")){
						echo $html->link('<span>FINISH</span>','javascript:void(0);',array('escape'=>false,'class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'firealarmView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');"));
						} else { ?>
						<span class="blue-btn" style="float:right;">
							<input type="submit" name="finish" value="FINISH" id="linkFinish" onclick="return confirm('Are you sure that you want to finish this report? after clicking ok you will not be able to further edit this report.')" style="margin-right:0;width:82px; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/>
						</span>		
					<?php
						}
						echo $html->link('<span>REVIEW</span>','javascript:void(0);',array('escape'=>false,'id'=>'reviewanswer','class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'firealarmView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');")); ?>
						<span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" name="save" style="margin-right:0; border:0; color:#fff; width:82px;height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span>
				</td>
				</tr>
		</table>		
	    </div>
	    <?php echo $form->end(); ?>
    </div>
    
    <!-- This contains the hidden content for inline calls -->
	<div style='display:none'>		
		<div id='inline_content_attachment' style='padding:10px; background:#fff;'>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'attachment','name'=>'attachment','id'=>'attachment',"onsubmit"=>"return checkUploadedAttachment();")); ?>
		<?php echo $form->input('Attachment.attach_file', array('id'=>'uploadattachment','type'=>'hidden','value'=>'')); ?>
		<?php echo $form->input('Attachment.inspector_id',array('type'=>'hidden','value'=>$sessionInspectorId)); ?>
		<?php echo $form->input('Attachment.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Attachment.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Attachment.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Attachment.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		
			<p><strong>Upload Attachment</strong></p>
		<p>
		    <div id="demo"></div>
		    <ul id="separate-list"></ul>
		</p>
		<p><div id="uploaded_picture" style="text-align:center;padding-left:90px;"></div></p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($attachmentdata)){?>
			<p><strong>Attachments</strong></p>
		<?php } ?>		
		<?php foreach($attachmentdata as $attachdata){ ?>
			<p id="p_<?php echo $attachdata['Attachment']['id'] ?>"><?php echo $html->link($attachdata['Attachment']['attach_file'],array("controller"=>"reports","action"=>"download",$attachdata['Attachment']['attach_file']),array()).'('.$this->Common->getClientName($sessionInspectorId).')'; ?><a href="javascript:void(0)" onclick="confirmattach('<?php echo $attachdata['Attachment']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>		
		</div>
		
		<?php echo $form->end(); ?>
	</div>
<!-- This contains the hidden content for inline calls -->
	<div style='display:none'>		
		<div id='inline_content_notification' style='padding:10px; background:#fff;'>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'notification','name'=>'notification','id'=>'notification',"onsubmit"=>"return checkNotification();")); ?>
		<?php echo $form->input('Notification.inspector_id',array('type'=>'hidden','value'=>$sessionInspectorId)); ?>
		<?php echo $form->input('Notification.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Notification.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Notification.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Notification.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		<p><strong>Notification</strong></p>
		<p>
		    <?php echo $form->input('Notification.notification',array('type'=>'textarea','rows'=>5,'cols'=>40,'label'=>false,'id'=>'ContentMessage','onKeyUp' =>"return limit('ContentMessage',500);")); ?>
		</p>
		<p>
                    <div id="essay_Error" class='error'></div>
			<div>
			<small>&nbsp;<label id='limitCounter'>0</label>&nbsp;characters entered&nbsp;|&nbsp;<label id='limitCounterLeft'>500</label>&nbsp;characters remaining</small>
			</div>   
		</p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($notifierdata)){?>
			<p><strong>Notifiers</strong></p>
		<?php } ?>		
		<?php foreach($notifierdata as $notifydata){ ?>
			<p id="pnotify_<?php echo $notifydata['Notification']['id'] ?>"><?php echo $notifydata['Notification']['notification'].'('.$this->Common->getClientName($sessionInspectorId).')'; ?><a href="javascript:void(0)" onclick="confirmnotifier('<?php echo $notifydata['Notification']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>
		</div>
		
		<?php echo $form->end(); ?>
	</div>
</div>	