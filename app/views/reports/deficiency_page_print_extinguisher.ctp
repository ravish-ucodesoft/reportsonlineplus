<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
$element=$this->element('reports/report_common_view');
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%">    	
    <tr>
        <td align="center" colspan="2" style="font-size:30px; color:red"><b>Deficiencies</b></td>
    </tr>';
        $html.=$element;
	/*<table width="100%" cellpadding="0" cellspacing="0">
    <tr><th colspan="2" align="left" style="background-color: #C6C6C6"> <b>Client information</b></th></tr>
    <tr>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="table_label"><strong>Building Information</strong></td>
                    <td>'.$clientResult['User']['client_company_name'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Address</strong></td>
                    <td>'.$clientResult['User']['address'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>City/State/Zip:</strong></td>
                    <td>'.$cityName.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Country: </strong></td>
                    <td>'.$clientResult['Country']['name'].'</td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
		    <td class="table_label"><strong>Contact: </strong></td>
		    <td>'.$clientResult['User']['fname'].' '.$clientResult['User']['lname'].'</td>
		</tr>
		<tr>
                    <td class="table_label"><strong>Phone:</strong></td>
                    <td>'.$clientResult['User']['phone'].'</td>
		</tr>                               
		<tr>
                    <td class="table_label"><strong>Email: </strong></td>
                    <td>'.$clientResult['User']['email'].'</td>
		</tr>
            </table>
	</td>
    </tr>
    <tr>
        <th colspan="2">&nbsp;</th>
    </tr>
    <tr><th colspan="2" align="left" style="background-color: #C6C6C6"> <b>Site Information</b></th></tr>
    <tr>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
		<tr>
                    <td class="table_label"><strong>Site Name:</strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_name'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Address</strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_address'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>City/State/Zip:</strong></td>
                    <td>'.$siteCityName.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Country: </strong></td>
                    <td>'.$siteaddrResult['Country']['name'].'</td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="table_label"><strong>Contact: </strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_contact_name'].'</td>
                 </tr>
                <tr>
                    <td class="table_label"><strong>Phone:</strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_phone'].'</td>
                </tr>                               
                <tr>
                    <td class="table_label"><strong>Email: </strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_email'].'</td>
                </tr>
            </table>
	</td>
    </tr>
    <tr>
	<th colspan="2">&nbsp;</th>
    </tr>
    <tr><th colspan="2" align="left" style="background-color: #C6C6C6"> <b>Service Provider Info</b></th></tr>
    <tr>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="table_label"><strong>Name:</strong></td>
                    <td>'.$spResult['User']['fname'].' '.$spResult['User']['lname'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Address</strong></td>
                    <td>'.$spResult['User']['address'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>City/State/Zip:</strong></td>
                    <td>'.$spCityName.', '.$spResult['State']['name'].' '.$spResult['User']['zip'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Country: </strong></td>
                    <td>'.$spResult['Country']['name'].'</td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="table_label"><strong>Contact: </strong></td>
                    <td>'.$spResult['User']['fname'].' '.$spResult['User']['lname'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Phone:</strong></td>
                    <td>'.$spResult['User']['phone'].'</td>
                </tr>                               
                <tr>
                    <td class="table_label"><strong>Email: </strong></td>
                    <td>'.$spResult['User']['email'].'</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
	<th colspan="2">&nbsp;</th>
    </tr>
</table>*/

$html.='<table width="100%" class="tbl input-chngs1" style="border:1px solid grey;">
    <tr>
        <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Sr. No.</b></th>
	<th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
        <th width="60%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
	<th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Attachment</b></th>
    </tr>';    
    if(!empty($Def_record))
    {
	$defCounter=0;
        foreach($Def_record as $Def_record) {
		$defCounter++;
    $html.='<tr>        
        <td valign="top">'.$defCounter.'.</td>
	<td valign="top">'.$Def_record['Code']['code'].'</td>
        <td valign="top">'.$Def_record['Code']['description'].'</td>
	<td valign="top">';
		if(!empty($Def_record['Deficiency']['attachment']) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$Def_record['Deficiency']['attachment'])){
		    //$html.=$html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$Def_record['Deficiency']['attachment'],'sp'=>false),array('title'=>'Click to Download'));
		    $html.='Attachment'; 
		}else{
		    $html.='No Attachment';
		}
	$html.='</td>
    </tr>';
        }
    }
    else
    {
        $html.='<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';   
    }    
$html.='</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Extinguisher_Report_DeficiencyPage.pdf', 'I');die;
?>