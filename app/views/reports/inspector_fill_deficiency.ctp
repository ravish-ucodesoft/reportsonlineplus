<?php
    echo $this->Html->css('manager/style.css');
    echo $this->Html->css('manager/theme.css');
    echo $this->Html->script('jquery.1.6.1.min');  
    echo $this->Html->css('validationEngine.jquery');    
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');    
?>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#DefForm").validationEngine();   
});
function openForm(){
    jQuery("#DefForm").slideDown(300);
}
</script>
<div id="content">
    <h2 style="font-size: 24px; text-align: center;"><strong>Deficiency</strong></h2>
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
    <?php if(is_array($chkExistance) && !empty($chkExistance)){?>
    <div style="float: left; width: 100%; text-align: center; padding-top: 10px;">
        Want to update, <?php echo $html->link('Click Here','javascript:void(0)',array('color:#ff0000','onclick'=>'openForm();'));?>
    </div>
    <?php echo $this->Form->create('', array('url'=>array('controller'=>'reports','action' => 'fillDeficiency?reportID='.$_REQUEST['reportID'].'&clientId='.$_REQUEST['clientId'].'&spId='.$_REQUEST['spId'].'&serviceCallId='.$_REQUEST['serviceCallId'].'&serviceId='.$_REQUEST['serviceId'].'&rowId='.$_REQUEST['rowId'].'&locationData='.$_REQUEST['locationData'].'&modelData='.$_REQUEST['modelData'].'&mfdDate='.$_REQUEST['mfdDate']),'id'=>'DefForm','name'=>'DefForm','style'=>'display: none;','enctype'=>'multipart/form-data') );?>
        <section class="form-box" style="width:100%; margin: 0;">                
            <ul class="register-form">
                <li>                    
                    <div style="width: 100%; float: left;">
                    <?php foreach($codes as $codeId=>$codeDesc){?>
                        <div style="width: 100%; float: left;">
                            <input <?php if($chkExistance['Deficiency']['code_id']==$codeId){?>checked="checked"<?php }?> style="float: left;" type="radio" class="validate[required]" name="data[Deficiency][code_id]" value="<?php echo $codeId;?>"/>&nbsp;<?php echo $codeDesc;?>
                        </div>    
                    <?php }?>
                    </div>                    
                </li>
                <li>
                    <label>Comment/Recommendation</label>
                    <?php echo $form->hidden('Deficiency.id',array('value'=>$chkExistance['Deficiency']['id']));?>
                    <p><span class=""><?php echo $form->textarea('Deficiency.recommendation', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','rows'=>'3','cols'=>'25','value'=>$chkExistance['Deficiency']['recommendation']));  ?></span></p>                       
                </li>
                <li>
                    <label>Attachment(if any)</label>                    
                    <p>
                        <span class=""><?php echo $form->input('Deficiency.attachdata', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','type'=>'file'));?></span>
                        <?php if($chkExistance['Deficiency']['attachment']!="" && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$chkExistance['Deficiency']['attachment'])){?>
                        <span style="padding-left: 5px;"><?php echo $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$chkExistance['Deficiency']['attachment'],'inspector'=>false))?></span><span style="padding-left: 5px"><?php echo $html->link('X',array('controller'=>'reports','action'=>'deleteRecord',$chkExistance['Deficiency']['attachment'],'inspector'=>false),array('style'=>'color:red','title'=>'Delete','onclick'=>'if(!confirm("Are you sure, you want to delete this attachment?")){ return false;}'));?></span>
                        <?php }?>
                    </p>                       
                </li>
                <li>
                    <label>&nbsp;</label>
                    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>                   
                </li>              
            </ul>
        </section>
    <?php echo $form->end(); ?>
    <?php }else{?>
    <?php echo $this->Form->create('', array('url'=>array('controller'=>'reports','action' => 'fillDeficiency?reportID='.$_REQUEST['reportID'].'&clientId='.$_REQUEST['clientId'].'&spId='.$_REQUEST['spId'].'&serviceCallId='.$_REQUEST['serviceCallId'].'&serviceId='.$_REQUEST['serviceId'].'&rowId='.$_REQUEST['rowId'].'&locationData='.$_REQUEST['locationData'].'&modelData='.$_REQUEST['modelData'].'&mfdDate='.$_REQUEST['mfdDate']),'id'=>'DefForm','name'=>'DefForm','enctype'=>'multipart/form-data') );?>
        <section class="form-box" style="width:100%; margin: 0">                
            <ul class="register-form">
                <li>                    
                    <div style="width: 100%; float: left;">
                    <?php foreach($codes as $codeId=>$codeDesc){?>
                        <div style="width: 100%; float: left;">
                            <input style="float: left;" type="radio" class="validate[required]" name="data[Deficiency][code_id]" value="<?php echo $codeId;?>"/>&nbsp;<?php echo $codeDesc;?>
                        </div>    
                    <?php }?>
                    </div>                    
                </li>
                <li>
                    <label>Comment/Recommendation</label>
                    <p><span class=""><?php echo $form->textarea('Deficiency.recommendation', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','rows'=>'3','cols'=>'25'));  ?></span></p>                       
                </li>
                <li>
                    <label>Attachment(if any)</label>                    
                    <p><span class=""><?php echo $form->input('Deficiency.attachdata', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','type'=>'file'));?></span></p>                       
                </li>
                <li>
                    <label>&nbsp;</label>
                    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>                   
                </li>              
            </ul>
        </section>
    <?php echo $form->end(); ?>
    <?php }?>
     <br/>   
</div>