 <?php

App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();
$html=       '<table cellpadding="2" width="100%">
      <tr><td colspan="4">
        <table width="100%">
          <tr>
            <td align="center"><img src="/app/webroot/img/company_logo/'.$spResult['Company']['company_logo'].'"></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b>'.$spResult['Company']['name'].'</b></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:21px">';		
		$arr=explode(',',$spResult['Company']['interest_service']);
	    
		    foreach($arr as $key=>$val):
		    foreach($reportType as $key1=>$val1):

		    if($val== $key1)
		    {
			
			 if($key1 == 9){
                            $html.=  $spResult['Company']['interest_service_other'];
                            }else{
                                $html.= $val1;
                                }
                       $html.=' <span class="red"> * </span>';
		     }
		    
		    endforeach;
		    
		    endforeach;	

		 $html.=   '</td></tr>
            
            
          
          
          
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:24px">Special Hazard Report</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>          
          <tr>
            <td align="right" class="Cpage" >Date:';if($record['SpecialhazardReport']['finish_date'] != ''){
                $html.= date('m/d/Y',strtotime($record['SpecialhazardReport']['finish_date'])); } $html.='</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" ><i>Prepared for:</i></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_name'].'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'; if(!empty($clientResult['User']['site_country_id'])){
                                        $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                        if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                        $html.= $clientResult['User']['site_city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_phone'].'</td>
          </tr>   
          <tr>
            <td align="left" class="Cpage" >Prepared By:</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
          <tr>
            <td align="left" class="Cpage">'.$spResult['Company']['name'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['address'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'; if(!empty($spResult['Country']['name'])){
                                        $html.= $spResult['Country']['name'].', '; }
                                        if(!empty($spResult['State']['name'])) { $html.= $spResult['State']['name'].', '; } 
                                        $html.= $clientResult['User']['city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['phone'].'</td>
          </tr> 
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
         
          
        </table>
      </td>
      </tr>
      <tr><td colspan="4"><h2 align="center">'. ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']) .'</h2></td></tr>
        <tr>
          <td colspan="2" style="text-align:center"><b>Client Info auto-populated</b></td>
          <td colspan="2" style="text-align:center"><b>Service Provider Info auto-populated</b></td>
        </tr>     
        <tr>
          <td style="text-align:right"><strong>Company Name:</strong></td>
          <td>'.$clientResult['User']['site_name'].'</td>
          <td style="text-align:right"><strong>Name of Service Provider:</strong></td>
          <td>'.ucwords($spResult['User']['fname'].' '.$spResult['User']['lname']).'</td>
        </tr>    
        <tr>
          <td style="text-align:right"><strong>Address:</strong></td>
          <td>'.$clientResult['User']['site_address'].'</td>
          <td style="text-align:right"><strong>Address:</strong></td>
          <td>'.$spResult['Company']['address'].'</td>
        </tr>    
        <tr>
          <td style="text-align:right"><strong>City State & Zip:</strong></td>
          <td>'.$clientResult['User']['site_city_state_zip'].'</td>
          <td style="text-align:right"><strong>City State & Zip:</strong></td>
          <td>'.$spResult['User']['site_city_state_zip'].'</td>
        </tr>    
        <tr>
          <td style="text-align:right"><strong>Owner Contact:</strong></td>
          <td>'.$clientResult['User']['responsible_contact'].'</td>
          <td style="text-align:right"><strong>Office Phone:</td>
          <td>'.$spResult['Company']['phone'].'</td>
        </tr>     
        <tr>
          <td style="text-align:right"><strong>Building:</strong></td>
          <td>'.$clientResult['User']['site_name'].'</td>
          <td style="text-align:right"><strong>Office License #</strong></td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td width="48%" border="" valign="top">
            <table style="border:1px solid black" cellpadding="2" width="100%">
              <tr>
                <td colspan="2" style="text-align:center"><b><font size="9px">Protected Property</font></b></td>
              </tr>    		
              <tr>
                <td >Name:</td>
                <td>'.$record['SpecialhazardReport']['protectedprop_name'].'</td>
              </tr>
              <tr>
                <td>Physical Address:</td>
                <td>'.$record['SpecialhazardReport']['protectedprop_physicaladdress'].'</td>
              </tr>
              <tr>
                <td >Contact:</td>
                <td>'.$record['SpecialhazardReport']['protectedprop_contact'].'</td>
              </tr>
              <tr>
                <td >Phone:</td>
                <td>'.$record['SpecialhazardReport']['protectedprop_phone'].'</td>
              </tr>
            </table>	    
          </td>
          <td width="4%"></td>
          <td width="48%" valign="top" >
            <table width="100%" style="border:1px solid black" cellpadding="2">
              <tr><td colspan="8" style="text-align:center"><b><font size="9px">For SFMO Use Only</font></b></td></tr> 	
              <tr>
                <td >Date:</td>
                <td>'.$record['SpecialhazardReport']['sfmo_date'].'</td>
                <td >Ck$:</td>
                <td>'.$record['SpecialhazardReport']['sfmo_ck$'].'</td>
                <td >Ck#:</td>
                <td>'.$record['SpecialhazardReport']['sfmo_ck#'].'</td>
                <td >By:</td>
                <td>'.$record['SpecialhazardReport']['sfmo_by'].'</td>      
              </tr>  		    
            </table>
            <br/>  	    
            <table width="100%" style="border:1px solid black" cellpadding="2">
              <tr><td colspan="8" style="text-align:center"><b><font size="9px">Original System Installer</font></b></td></tr>  
              <tr>
                <td>Name:</td>
                <td>'.$record['SpecialhazardReport']['originalsysteminstaller_name'].'</td>
              </tr>    
            </table>    
          </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>      
        <tr>
          <td width="48%" valign="top">
            <table width="100%" style="border:1px solid black" cellpadding="2">
              <tr><td colspan="2"><b><font size="9px">Inspection Company</font></b></td></tr>  
              <tr>
                <td>Name:</td>
                <td>'. $record['SpecialhazardReport']['inspectioncompany_name'].'</td>
              </tr>
              <tr>
                <td>Address:</td>
                <td>'. $record['SpecialhazardReport']['inspectioncompany_physicaladdress'].'</td>
              </tr>
              <tr>
                <td>Contact:</td>
                <td>'. $record['SpecialhazardReport']['inspectioncompany_contact'].'</td>
              </tr>
              <tr>
                <td>Phone:</td>
                <td>'. $record['SpecialhazardReport']['inspectioncompany_phone'].'</td>
              </tr>
            </table>	    
          </td>
          <td width="4%">&nbsp;</td>      
          <td width="48%" valign="top">
            <table width="100%" style="border:1px solid black" cellpadding="2">
              <tr><td colspan="2"><b><font size="9px">System Owner</font></b></td></tr>      
              <tr>
                <td>Name:</td>
                <td>'. $record['SpecialhazardReport']['systemowner_name'].'</td>
              </tr>
              <tr>
                <td>Address:</td>
                <td>'. $record['SpecialhazardReport']['systemowner_physicaladdress'].'</td>
              </tr>
              <tr>
                <td>Contact:</td>
                <td>'. $record['SpecialhazardReport']['systemowner_contact'].'</td>
              </tr>
              <tr>
                <td>Phone:</td>
                <td>'. $record['SpecialhazardReport']['systemowner_phone'].'</td>
              </tr>
            </table>      
          </td>
         </tr>
         <tr><td colspan="3">&nbsp;</td></tr>
         	<tr>
            <td width="100%" valign="top" colspan="3">
              <table width="100%" style="border:1px solid black" cellpadding="2">
                <tr><td colspan="4" style="text-align:center"><strong>Special Hazard Fire Suppression System Company'."'".'s License</strong></td></tr>
            
                <tr>
                  <td width="10%" >Company Name:</td>
                  <td width="60%">'. $record['SpecialhazardReport']['companylicense_cname'].'</td>
                  <td width="10%" >License#:</td>
                  <td width="20%">'. $record['SpecialhazardReport']['companylicense_license'].'</td>
                </tr>
                <tr>
                  <td colspan="4">The special hazard fire suppression system company certifies that the features of the system,as indicated herein,were tested,inspected,and/or maintained in accordance with the Delaware State Fire Prevention Regulations :</td>
                </tr>
                <tr>
                  <td width="10%" >Print Name:</td>
                  <td width="60%">'. $record['SpecialhazardReport']['companylicense_pname'].'</td>
                  <td width="10%" >Title:</td>
                  <td width="20%">'. $record['SpecialhazardReport']['companylicense_title'].'</td>
                </tr>
                <tr>
                  <td width="10%" >Signature:</td>
                  <td width="60%">'. $record['SpecialhazardReport']['companylicense_signature'].'</td>
                  <td width="10%" >Date:</td>
                  <td width="20%">'. $record['SpecialhazardReport']['companylicense_date'].'</td>
                </tr>
              </table>	    
            </td>
          </tr>
          <tr><td colspan="3">&nbsp;</td></tr>
          <tr>
            <td width="100%" valign="top" colspan="3">
              <table width="100%" style="border:1px solid black" cellpadding="2">
                <tr>
                  <td colspan="4" style="text-align:center"><strong>Special Hazard Fire Suppression System Owner'."'".'s Acknowledgement</strong></td></tr>                <tr>
                  <td colspan="4">The special hazard fire suppression system owner acknowledges having reviewed this certificate and confirms that any deficiencies and/or failures noted will be corrected forthwith:</td>              
                </tr>
                <tr>
                  <td width="10%" >Print Name:</td>
                  <td width="60%">'.$record['SpecialhazardReport']['ownerackldge_pname'].'</td>
                  <td width="10%" >Title:</td>
                  <td width="20%">'. $record['SpecialhazardReport']['ownerackldge_title'].'</td>
                </tr>
                <tr>
                  <td width="10%" >Signature:</td>
                  <td width="60%">'. $record['SpecialhazardReport']['ownerackldge_signature'].'</td>
                  <td width="10%" >Date:</td>
                  <td width="20%">'. $record['SpecialhazardReport']['ownerackldge_date'].'</td>
                </tr>
              </table>	    
            </td>
          </tr>
          <tr><td colspan="3">&nbsp;</td></tr>          
          <tr>
            <td width="100%" valign="top" colspan="3">
            <table  width="100%" style="border:1px solid black" cellpadding="2">
            <tr><td colspan="4" style="text-align:center"><strong>Special Hazard Fire Suppression SystemType</strong></td></tr>          
            <tr>
                <td width="10%" >System ID#:</td>
                <td width="90%" colspan="3">'.$record['SpecialhazardReport']['systemtype_systemID'].'</td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>          
            <tr><td colspan="4">';        
          
            foreach($arr as $key=>$val){
            foreach($specialHazardarr as $key1=>$val1){
            
            
            if($val == $key1)
            {
              $html.= $val1.', ';
            }
            
            }
            
            }     
          
            $html.= '</td></tr>
            </table>
            </td></tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td width="100%" valign="top" colspan="3">
                <table width="100%" style="border:1px solid black" cellpadding="2">
                    <tr><td colspan="4" style="text-align:center"><strong>Special Hazard Fire Suppression System Monitoring</strong></td></tr>
            
                    
                    <tr>
                        <td width="40%" >Is this system monitored off-site?:</td>
                        <td width="60%" colspan="3">'. $record['SpecialhazardReport']['system_monitoring_offsite'].'</td>
                    </tr>
                    <tr>
                        <td width="40%" >If yes, type of dialer installed:</td>
                        <td width="60%" colspan="3">'. $record['SpecialhazardReport']['system_monitoring_dialer_installed'].'</td>
                    </tr>
                    <tr>
                        <td width="40%" >If yes, provide name, location, and phone # of monitoring station:</td>
                        <td width="60%" colspan="3">'. $record['SpecialhazardReport']['system_monitoring_namelocation'].'</td>
                    </tr>
                </table>	    
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" valign="top" colspan="3">
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td colspan="4" class="tdLabel">Special Hazard Fire Suppression System Deficiencies</td></tr>
	
		
		<tr>
		    <td width="40%" >Any deficiencies from DSFPR and/or adopted NFPA Standards?:</td>
		    <td width="60%" colspan="3">'. $record['SpecialhazardReport']['system_deficiency'].'</td>
		</tr>		
	    </table>	    
	   </td>
	</tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr><td colspan="3">&nbsp;</td></tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr><td colspan="4" class="tdLabel" align="center"><strong>~ A COPY OF THIS FROM MUST BE SUBMITTED ANNUALLY TO THE STATE FIRE MARSHAL ~</strong></td></tr>
	<tr><td colspan="4" class="tdLabel" align="center"><strong>~ ALL DEFICIENCIES MUST BE FULLY EXPLAINED, SEE COMMENTS PAGE ~</strong></td></tr>
	<tr><td colspan="4" class="tdLabel" align="center"><strong>~ A COPY OF THIS FORM MUST BE MAINTAINED ON THE SITE ~</strong></td></tr>
	
	<!-- PAGE 2 START-->
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr><td colspan="3">&nbsp;</td></tr>
        
	<tr><td colspan="4" class="tdLabel" align="center"><strong>SPECIAL HAZARD FIRE SUPPRESSION SYSTEM ANNUAL CERTIFICATE OF INSPECTION</strong></td></tr>
	<tr><td colspan="3">&nbsp;</td></tr>
        
	<tr><td colspan="3">&nbsp;</td></tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
	    <td width="100%" valign="top" colspan="3">
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td colspan="5"  align="center"><strong>Alarm Initating Devices</strong></td></tr>
		<tr>
		    <th width="20%"><strong>Device/Equipment</strong></th>
		    <th width="20%"><strong>System Total</strong></th>
		    <th width="20%"><strong>Number Tested</strong></th>
		    <th width="20%"><strong>Deficiencies*</strong></th>
		    <th width="20%"><strong>Remarks</strong></th>		
		</tr>
		<tr>
		    <td style="padding-left:30px;">Manual Pull Station</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_MPS_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_MPS_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_MPS_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_MPS_remark'].'</td>
		</tr>
		
		
		
		<tr>
		    <td style="padding-left:30px;">Heat Detectors</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_HD_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_HD_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_HD_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_HD_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Smoke Detectors</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_SD_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_SD_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_SD_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_SD_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Water Flow Devices</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_WFD_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_WFD_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_WFD_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_WFD_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Pneumatic Devices</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_PD_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_PD_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_PD_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_PD_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Releasing Devices</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_RD_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_RD_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_RD_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_RD_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Other (Specify):</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_OTHER_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_OTHER_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_OTHER_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIniDevice']['alarm_ini_device_OTHER_remark'].'</td>
		</tr> 
		 
	    </table>	    
	   </td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" valign="top" colspan="3">
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td colspan="5" align="center"><strong>Alarm Indicating Devices</strong></td></tr>
		<tr>
		    <th width="20%"><strong>Device/Equipment</strong></th>
		    <th width="20%"><strong>System Total</strong></th>
		    <th width="20%"><strong>Number Tested</strong></th>
		    <th width="20%"><strong>Deficiencies*</strong></th>
		    <th width="20%"><strong>Remarks</strong></th>		
		</tr>
		<tr>
		    <td style="padding-left:30px;">Bells</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_bells_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_bells_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_bells_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_bells_remark'].'</td>
		</tr>
		
		
		
		<tr>
		    <td style="padding-left:30px;">Horns</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_horns_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_horns_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_horns_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_horns_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Chimes</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_chimes_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_chimes_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_chimes_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_chimes_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Speakers</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_speakers_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_speakers_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_speakers_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_speakers_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Remote Annunciators</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_RA_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_RA_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_RA_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_RA_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Visual Signals</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_VS_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_VS_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_VS_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_VS_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Other (Specify):</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_OTHER_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_OTHER_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_OTHER_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardAlarmIndDevice']['alarm_ind_device_OTHER_remark'].'</td>
		</tr> 
		 
	    </table>	    
	   </td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr><td colspan="3" >
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td colspan="5" align="center"><strong>Power Supply Systems</strong></td></tr>
		<tr><td colspan="5">&nbsp;</td></tr>
		
		<tr>
		    <td width="10%"><strong>Primary:</strong></td>
		    <td width="10%">Dedicated?</td>
		    <td width="30%">'.$record['SpecialhazardPowerSystem']['powersupply_dedicated'].'</td>
		    <td width="10%">Locked Out?</td>
		    <td width="40%">'.$record['SpecialhazardPowerSystem']['powersupply_locked_out'].'</td>
		</tr>
		<tr>
		    <td width="14%"><strong>Secondary:</strong></td>
		    <td width="36%" colspan="2">Battery Amp Power Rating:
		    &nbsp;'.$record['SpecialhazardPowerSystem']['powersupply_power_rating'].'
		    </td>		    
		    <td width="10%">Tested OK?</td>
		    <td width="40%">'.$record['SpecialhazardPowerSystem']['powersupply_power_rating_ok'].'</td>
		</tr>
		<tr>
		    <td width="10%">&nbsp;</td>
		    <td width="40%"  colspan="2">Engine Driven Generator Rating:
		    &nbsp;'.$record['SpecialhazardPowerSystem']['powersupply_generator_rating'].'
		    </td>		    
		    <td width="10%">Tested OK?</td>
		    <td width="40%">'.$record['SpecialhazardPowerSystem']['powersupply_generator_rating_ok'].'</td>
		</tr>
		<tr>
		    <td width="10%">&nbsp;</td>
		    <td width="40%" >Fueled by: &nbsp;'.$record['SpecialhazardPowerSystem']['powersupply_fueled_by'].' </td>		    
		    <td width="50%">Storage Capacity:&nbsp;'.$record['SpecialhazardPowerSystem']['powersupply_storage_capacity'].' </td>
		</tr>  
				
	    </table>
	    </td>
	</tr>
        
        	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%"  valign="top" colspan="3">
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td colspan="5" style="text-align:center"><strong>Supervisory Initiating Devices</strong></td></tr>
		<tr>
		    <th width="20%"><strong>Device/Equipment</strong></th>
		    <th width="20%"><strong>System Total</strong></th>
		    <th width="20%"><strong>Number Tested</strong></th>
		    <th width="20%"><strong>Deficiencies*</strong></th>
		    <th width="20%"><strong>Remarks</strong></th>		
		</tr>
		<tr>
		    <td style="padding-left:30px;">Sprinkler Control Valve</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_SCV_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_SCV_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_SCV_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_SCV_remark'].'</td>
		</tr>
		
		
		
		<tr>
		    <td style="padding-left:30px;">Pressure Switches</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_PS_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_PS_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_PS_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_PS_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Building/Water Temp.</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_WT_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_WT_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_WT_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_WT_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Water Supply Level</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_WSL_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_WSL_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_WSL_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_WSL_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Extinguishing Agent Level</td>
		    <td style="text-align:center">&nbsp;</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_EAL_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_EAL_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_EAL_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Expallant Gas Level</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_EGL_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_EGL_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_EGL_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_EGL_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Other (Specify):</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_OTHER_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_OTHER_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_OTHER_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardSupIniDevice']['supervisory_ini_device_OTHER_remark'].'</td>
		</tr> 
		 
	    </table>	    
	   </td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr><td colspan="4" class="tdLabel" align="center"><strong>* ALL DEFICIENCIES MUST BE FULLY EXPLAINED, SEE COMMENTS PAGE *</strong></td></tr>
	<tr><td colspan="4" class="tdLabel" align="center"><strong>* A COPY OF THIS FORM MUST BE MAINTAINED ON THE SITE *</strong></td></tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr><td colspan="4" class="tdLabel" align="center"><strong>SPECIAL HAZARD FIRE SUPPRESSION SYSTEM ANNUAL CERTIFICATE OF INSPECTION</strong></td></tr>
	<tr><td colspan="3">&nbsp;</td></tr>	
		<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3">
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td colspan="5" align="center"><strong>Control Unit</strong></td></tr>
		
		<tr>
		    <td width="30%" colspan="2">System #1 Manufacturer:&nbsp;'. $record['SpecialhazardControlUnit']['controlunit_syst_manufacturer'].'</td>
		    <td width="30%" colspan="2">Model #:&nbsp;'.$record['SpecialhazardControlUnit']['controlunit_model'].'</td>
		    <td width="40%">System ID #:&nbsp;'.$record['SpecialhazardControlUnit']['controlunit_systemID'].'</td>
		    		
		</tr>
		<tr><td colspan="5">&nbsp;</td></tr>
		<tr><td width="10%" align="center">Type:</td>
		    <td colspan="4" width="90%">';		
			
			foreach($arr1 as $key=>$val):
			foreach($controlUnit as $key1=>$val1):
			
			
			if($val== $key1)
			{
			$html.= $val1." , ";
			}
			
			endforeach;
			
			endforeach;
					

		 $html.=   '</td>
		</tr>
		<tr>
		    <th width="20%"><strong>Device/Equipment</strong></th>
		    <th width="20%"><strong>System Total</strong></th>
		    <th width="20%"><strong>Number Tested</strong></th>
		    <th width="20%"><strong>Deficiencies*</strong></th>
		    <th width="20%"><strong>Remarks</strong></th>		
		</tr>
		<tr>
		    <td style="">Lamp & LEDs</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_LED_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_LED_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_LED_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_LED_remark'].'</td>
		</tr>
		
		
		
		<tr>
		    <td style="padding-left:30px;">Fuses</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_FUSES_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_FUSES_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_FUSES_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_FUSES_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Trouble Signals</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_TS_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_TS_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_TS_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_TS_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Zone Disable</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_ZD_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_ZD_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_ZD_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_ZD_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Supervisory Signals</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_SS_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_SS_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_SS_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_SS_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Ground Fault</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_GF_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_GF_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_GF_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_GF_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Transmission Off Premises:</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_TOP_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_TOP_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_TOP_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_TOP_remark'].'</td>
		</tr>
		<tr>
		    <td style="padding-left:30px;">Other (Specify):</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_OTHER_total'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_OTHER_tested'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_OTHER_deficiency'].'</td>
		    <td style="text-align:center">'.$record['SpecialhazardControlUnit']['controlunit_device_OTHER_remark'].'</td>
		</tr>		
	    </table>	    
	   </td>
	</tr>
	
		<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3">
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td colspan="5" align="center"><strong>Extinguishing Agents and Expellant Gas</strong></td></tr>

		
		<tr>
		    <th width="20%"><strong>Device/Equipment</strong></th>
		    <th width="20%"><strong>System Total</strong></th>
		    <th width="20%"><strong>Number Tested</strong></th>
		    <th width="20%"><strong>Deficiencies*</strong></th>
		    <th width="20%"><strong>Remarks</strong></th>		
		</tr>
		<tr>
		    <td style="padding-left:30px;">Agent Storage Container</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_ASC_total'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_ASC_tested'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_ASC_deficiency'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_ASC_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Releasing Valves</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_RV_total'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_RV_tested'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_RV_deficiency'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_RV_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Water Supply</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_WS_total'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_WS_tested'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_WS_deficiency'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_WS_remark'].'</td>
		</tr>
		
		<tr>
		    <td style="padding-left:30px;">Other (Specify):</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_OTHER_total'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_OTHER_tested'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_OTHER_deficiency'].'</td>
		    <td style="text-align:center">'. $record['SpecialhazardGasDevice']['expellantgas_device_OTHER_remark'].'</td>
		</tr>		 
	    </table>	    
	   </td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3">
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td>Are All devices uniquely identified in accordance with the Delaware State Fire Prevention Regulations, Part lll,Section 1-4.8.2?&nbsp;&nbsp;&nbsp;'.$record['SpecialhazardReport']['all_device_check'].'</td></tr>
				 
	    </table>	    
	   </td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3">
	    <table width="100%" style="border:1px solid black" cellpadding="2">
		<tr><td colspan="2" align="center"><strong>Special Hazard Fire Suppression System Inspection</strong></td></tr>
		
		<tr>		    
		    <td width="73%">1.&nbsp;Is the Hazard the same as the last inspection?</td>
		    <td width="27%">'.$record['SpecialhazardReport']['inspection_result1'].'</td>
		</tr>
		
		<tr>		    
		    <td>2.&nbsp;Does the inspection of the enclosure indicate NO new penetrations?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result2'].'</td>
		</tr>
		
		<tr>		    
		    <td>3.&nbsp;If NO, are the new penetrations properly sealed?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result3'].'</td>
		</tr>
		
		<tr>		    
		    <td>4.&nbsp;Are all cylinders free of damage or defects?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result4'].'</td>
		</tr>
		
		<tr>		    
		    <td>5.&nbsp;Are all system components securely fastened?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result5'].'</td>
		</tr>
		
		<tr>		    
		    <td>6.&nbsp;Are all discharge nozzles clean and properly positioned?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result6'].'</td>
		</tr>
		
		<tr>		    
		    <td>7.&nbsp;Are all proper warning signs in place and visible?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result7'].'</td>
		</tr>
		
		<tr>		    
		    <td>8.&nbsp;Are the system manual pull stations visibly different from the fire alarm pull stations?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result8'].'</td>
		</tr>
		
		<tr>		    
		    <td>9.&nbsp;Did all auxiliary functions test O.K.?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result9'].'</td>
		</tr>
		
		<tr>		    
		    <td>10.&nbsp;What is the date of last hydrostatic test/date of cylinder manufacture?</td>
		    <td>'.$record['SpecialhazardReport']['inspection_result10'].'</td>
		</tr>
		
	    </table>	    
	   </td>
	</tr>
		
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3">
	    <table width="100%" class="tbl input-chngs1" style="border:1px solid black">
		<tr><td class="tdLabel">Comments/Deficiencies</td></tr>		
		<tr>		    
		    <td width="100%" align="center">'. $record['SpecialhazardReport']['inspection_result_comments'].'</td>		    
		</tr>
	    </table>	    
	   </td>
	</tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
	    <td width="100%" class="brdr" valign="top" colspan="3" style="border:1px solid black">
                <table width="100%" >
                <tr><td colspan="2" align="center"><h3>Deficiency</h3></td></tr>
                    <tr>
                        <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                        <br/>
                        '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                        <br/>
                        '.$clientResult['User']['site_name'].'
                        <br/>';
                        if(!empty($clientResult['User']['site_country_id'])){
                                        $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                        if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                        $html.= $clientResult['User']['site_city'].' <br/>
                        '.$clientResult['User']['site_phone'].'
                        </td>
                        <td width="50%" align="right" style="padding-right:30px;">
                            <i><b>Prepared By:</b></i>
                            <br/>'.
                            $spResult['Company']['name'].'
                            <br/>
                            '. $spResult['User']['address'].'
                            <br/>
                            ';	if(!empty($spResult['Country']['name'])){
                                      $html.= $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) {
                                       $html.=  $spResult['State']['name'].', ';
                                    }
                                   $html.=  $spResult['User']['city'];
                            
                           $html.= ' <br/>
                            Phone  '.$spResult['User']['phone'].'
                        </td>
                    </tr>
                </table>
                <table width="100%" class="tbl input-chngs1">
                    <tr>
                        
                        <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                        <th width="90%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
                        
                    </tr>';
                    
                    if(!empty($Def_record))
                    {
                   // pr($codeData);
                    foreach($Def_record as $Def_record) {
                   
                    $html.='<tr>
                        
                        <td valign="top">'. $Def_record['Code']['code'].'</td>
                        <td valign="top">'. $Def_record['Code']['description'].'</td>
                        
                    </tr>';
                    }
                    } else
                    {
                     $html.= '<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';  
                    }
                   
                $html.= '</table>       
                
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3" style="border:1px solid black">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="2"><h3>Recommendation</h3></td>
                    </tr>
                    <tr>
                        <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                        <br/>
                        '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                        <br/>
                        '.$clientResult['User']['site_name'].'
                        <br/>
                        ';
                        if(!empty($clientResult['User']['site_country_id'])){
                                        $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                        if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                        $html.= $clientResult['User']['site_city'].'
                        <br/>
                        '.$clientResult['User']['site_phone'].'
                        </td>
                        <td width="50%" align="right" style="padding-right:30px;">
                            <i><b>Prepared By:</b></i>
                            <br/>
                            '.$spResult['Company']['name'].'
                            <br/>
                            '.$spResult['User']['address'].'
                            <br/>';
                             	if(!empty($spResult['Country']['name'])){
                                        $html.= $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) {
                                        $html.= $spResult['State']['name'].', ';
                                    }
                                   $html.=  $spResult['User']['city'];
                           
                          $html.=   '<br/>
                            Phone  '.$spResult['User']['phone'].'
                        </td>
                        
                        </tr>
                </table>
                    
                   <table width="100%" class="tbl input-chngs1" >
                    <tr width="100%">
                        
                        <th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                        <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
                         <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>
                        
                    </tr>';
                    if(!empty($Def_record_recomm))
                    {
                    
                    foreach($Def_record_recomm as $Def_record_recomm) {
                  
                    $html.='<tr width="100%">
                        
                        <td valign="top">'.$Def_record_recomm['Code']['code'].'</td>
                        <td valign="top">'.$Def_record_recomm['Code']['description'].'</td>
                         <td valign="top">'.$Def_record_recomm['Deficiency']['recommendation'].'</td>
                        
                    </tr>';
                     }
                    } else
                    {
                     $html.='<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';   
                    }
                   
                 $html.='</table>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td width="100%" class="brdr" valign="top" colspan="3" style="border:1px solid black">
                <table width="100%" class="tbl">
                    <tr>
                        <td width="100%" align=center  style="border:none;"><span style="font-size:15px; font-weight:bolder;"><h3>Signature Page</h3></span></td>                        
                    </tr>                   
                </table>
                <table width="100%">
                    <tr>
                        <td style="border:none;"><b>Inspector Signature</b><br/><br/>';
                        $name=$this->Common->getInspectorSignature($record['SpecialhazardReport']['report_id'],$record['SpecialhazardReport']['servicecall_id'],$record['SpecialhazardReport']['inspector_id']);
                        if($name!='empty'){
                            $html.= '<img src="/app/webroot/img/signature_images/'.$name.'">';
                        }
                        $html.='                        
                        </td>
                        <td style="border:none; vertical-align:top;" align=right><b>Client Signature</b></td>
                  
                    </tr>
                    <tr>
                        <td style="border:none;">';
                        $dateCreated=$this->Common->getSignatureCreatedDate($record['SpecialhazardReport']['report_id'],$record['SpecialhazardReport']['servicecall_id'],$record['SpecialhazardReport']['inspector_id']);
                          
                        if($dateCreated!='empty'){
                        $html.= $this->Common->getClientName($record['SpecialhazardReport']['inspector_id'])."<br/><br/>".
                          $time->Format('m-d-Y',$dateCreated);
                        }
                       $html.='</td>
                        <td align=left style="border:none;"></td>
                  
                    </tr>
                    
                </table>
            </td>
        </tr>
 ';

 $html .= '</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
 
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Specialhazard_Report.pdf', 'D');die;