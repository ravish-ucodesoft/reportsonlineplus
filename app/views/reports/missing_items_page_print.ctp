<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
$element=$this->element('reports/report_common_view');
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%" class="tbl">	
        <tr>
                <td colspan="2" align="center" style="border:none;"><span style="font-size:36px; color:green;"><b>Missed Items</b></span></td>                
        </tr>      
</table>

<table width="100%" cellpadding="0" cellspacing="0">';
$html.=$element;
$html.='</table>

<table width="100%" class="qtytable" border="1">
        <tr>
                <td rowspan="2" style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Device Name</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Surveyed</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Serviced</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Passed</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Deficient</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Recommended</td>
                <td style="background-color:#2b92dd; color:#fff; font-weight:bold; text-align:center;">Missed</td>
        </tr>
        <tr style="background-color:#666; color:#fff; font-weight:bold">
                <td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td><td style="color:#fff; text-align:center;">Qty</td>
        </tr>';
        foreach($servicedata as $serviceData){         
        $html.='<tr>
                <td style="text-align:center;">'.$serviceData['Service']['name'].'</td>
                <td style="text-align:center;">'.$serviceData['ScheduleService']['amount'].'</td>
                <td style="text-align:center;">'.$this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_pass_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_deficient_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>
                <td style="text-align:center;">'.$this->Common->get_recomend_device($record['FirealarmReport']['id'],$serviceData['Service']['id']).'</td>';                
                if($this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id'])<$serviceData['ScheduleService']['amount']){
                     $missing = $serviceData['ScheduleService']['amount'] - $this->Common->get_total_device($record['FirealarmReport']['id'],$serviceData['Service']['id']);
                }else{
                     $missing = "0";
                }                
                $html.='<td style="text-align:center;">'.$missing.'</td>
        </tr>';        
        }  
$html.='</table>';
$html.='<table style="color: green">';	
	$missingData = $this->Common->missingTextExplanation($_REQUEST['serviceCallID'],$_REQUEST['reportID']);
	$missingText=$missingData['ExplanationText']['missing_reason'];
	$missingfile=$missingData['ExplanationText']['missing_file'];
	$missingText = (($missingText!="")?$missingText:'No explanation added');
	if(!empty($missingfile) && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$missingfile)){
		$showMissfile = 'Attachment';
	}else{
		$showMissfile='No Attachment';
	}
	$html.='<tr>								
			<td colspan="6">Missed Items Explanation: '.$missingText.'</td>
		</tr>
		<tr>								
			<td colspan="6">Missed Items Attachment: '.$showMissfile.'</td>
		</tr>
</table>';
/*<table>
        <tr><td colspan="6">&nbsp;</td></tr>
        <tr>
                <td colspan="6">'.$spResult['Company']['name'].'</td>
        </tr>
        <tr>
               <td colspan="6">'.$spResult['Company']['address'].'</td>
        </tr>
        <tr>
               <td colspan="6">'.$spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'].'</td>
        </tr>		 
</table>';*/
$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Firealarm_Report_MissedItemsPage.pdf', 'I');die;
?>