<?php
	echo $this->Html->script('accordian.pack');
	echo $this->Html->css('style_tablesort');
?>
<style type="text/css">
    body { font-family:arial}
    .summary-tbl2 p {
        float: left;
        margin-bottom: 0;
        margin-left: 5px;
        width: 10%;
        font-size: 10px;
        padding-left: 7px;
    }
    .headingtxt{
        background-color: #2B92DD;
        color: #fff;
        float: left;
        font-size: 14px;
        font-weight: bold;
        height: 14px;
        padding: 5px;
        width: 100%;
        margin-bottom:10px;
        text-align:center;
    }
    .lbtxt{text-align:left; font-weight:bold; width:auto !important; }
    .lb1txt{text-align:right;width:20%}
    .input-chngs1 input {
        margin-right: 3px;
        width: 136px;
    }
    #basic-accordian{
            border:5px solid #EEE;
            padding:5px;
            width:100%;
            position:absolute;
            top:10%;
            margin-left:0px;
            z-index:2;
            /*margin-top:-150px;*/
    }
    
    .accordion_headings{
            padding:5px;
            background:#00CCFF;
            color:#FFFFFF;
            border:1px solid #FFF;
            cursor:pointer;
            font-weight:bold;
            font-size:12px;
    }
    
    .accordion_headings:hover{
            background:#00CCFF;
    }
    
    .accordion_child{
            padding:15px;
            background:#EEE;
    }
    
    .header_highlight{
            background:#2B92DD;
    }
    
    .tab_container *{
            float:left;
            width:107px;
    }
    
    .tdLabel {font-size:14px;font-weight:bold;text-align:center;}
    .brdr {border:2px solid #666666;}
    .input-chngs1 input {
        margin-right: 3px;
        width: 296px;
    }
    .lbl{color:#666666;font-size:11px;text-align:left;}
    .firealramreport td{ border: 1px solid #000000;}
    .alramreporttest{text-align:left;background-color:#2b92dd; color:#fff; font-size:13px; font-weight:bold}
    #accordionlist { text-align:left; }
    .qtytable { background:#fff}
    .qtytable td{ border:1px solid black;font-size:14px;color:grey;}
    td{font-size:14px;color:grey;}
    .qtytable{ border-collapse: collapse;}
</style>
<?php
App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();

$element_cover=$this->element('reports/report_common_cover',array('report_name'=>'Extinguisher Report','finish_date'=>$record['ExtinguisherReport']['finish_date'],'freq'=>$schFreq));
$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>';
$html.=$element_cover;
/*$html='<div style="text-align:center;width:100%;font-size:55px; color: grey; padding-bottom: 10px;"><b><i>'.ucwords($spResult['Company']['name']).'</i></b></div>
<table width="100%">                            
            <tr>
                <td colspan="5">
                    <table width="100%">			
                        <tr>
                            <td align="center" style="font-size:30px;"><b>Cover Page</b></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="center"><img src="/app/webroot/img/company_logo/'.$spResult['Company']['company_logo'].'"></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b>'.$spResult['Company']['name'].'</b></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:21px">';
                                $arr=explode(',',$spResult['Company']['interest_service']);
                                    foreach($arr as $key=>$val):
                                    foreach($reportType as $key1=>$val1):
                                    if($val== $key1)
                                    {
                                        if($key1 == 9){ $html.=  $spResult['Company']['interest_service_other']; }else{ $html.= $val1;} 
                                        $html.= '<span class="red"> * </span>';
                                    }
                                    endforeach;
                                    endforeach;                                                
                        $html.=  '</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:24px">Extinguisher Report</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                        </tr>          
                        <tr>
                            <td align="right" class="Cpage" >Date:';
                                if($record['ExtinguisherReport']['finish_date']!= ''){
                                    $html.=  $time->Format('m/d/Y',$record['ExtinguisherReport']['finish_date']);
                                }
                        $html.='</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" ><i>Prepared for:</i></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$clientResult['User']['client_company_name'].'</td>
                        </tr>	  
                        <tr>
                            <td align="center" class="Cpage" >'.$clientResult['User']['address'].'</td>
                        </tr>
                        <tr>';
                            $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
                            $html.=  '<td align="center" class="Cpage" >'.$cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']).'</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$clientResult['User']['zip'].'</td>
                        </tr>                                        
                        <tr>
                            <td align="center" class="Cpage" >&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" ><b>Site Address Info:</b></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$siteaddrResult['SpSiteAddress']['site_name'].'</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact'].'</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$siteaddrResult['SpSiteAddress']['site_address'].'</td>
                        </tr>
                        <tr>';
                            $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
                            $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
                            $html.=  '<td align="center" class="Cpage" >'.$siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']).'</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$siteaddrResult['SpSiteAddress']['site_zip'].'</td>
                        </tr>                                        
                        <tr>
                            <td align="left" class="Cpage" >Prepared By:</td>
                        </tr>
                        <tr>
                            <td align="left" class="Cpage">&nbsp;</td>
                        </tr>  
                        <tr>
                            <td align="left" class="Cpage">'.$spResult['Company']['name'].'</td>
                        </tr>
                        <tr>
                            <td align="left" class="Cpage">'.$spResult['Company']['address'].'</td>
                        </tr>
                        <tr>
                            <td align="left" class="Cpage">'.$spResult['Country']['name'].','.$spResult['State']['name'].','.$spCity.'</td>
                        </tr>		      
                        <tr>
                            <td align="left" class="Cpage">&nbsp;</td>
                        </tr>  		      
                        <tr>
                            <td align="left" class="Cpage" style="font-size:11px">&nbsp;</td>
                        </tr>          
                    </table>
                </td>
            </tr>
        </table>';*/
$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
//echo $html;die;
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_Extinguisher_Report_CoverPage.pdf', 'I');die;
?>