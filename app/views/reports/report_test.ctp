<?php echo $html->css('manager/style.css');?>
<?php echo $html->css('manager/theme.css');?>
<div id="wrapper">
    <div id="content">
        <div id="box">            
            <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td colspan="2" align="center"><strong>Sprinkler Inspection Report</strong> </td>
                </tr>    
                <tr>
                    <td colspan="2">
                        <table width="100%">
                            <!--<tr>
                                <td align="center"><img src="/app/webroot/img/company_logo/<?php //echo $spResult['Company']['company_logo']?>"></td>
                            </tr>-->
                            <tr>
                                <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b><?php echo $spResult['Company']['name']?></b></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:21px">
                                <?php 
                                    $arr=explode(',',$spResult['Company']['interest_service']);	    
                                    foreach($arr as $key=>$val):
                                        foreach($reportType as $key1=>$val1):
                                            if($val== $key1)
                                            {
                                                
                                                 if($key1 == 9){
                                                    echo  $spResult['Company']['interest_service_other'];
                                                    }else{
                                                        echo $val1;
                                                        }
                                               echo '<span class="red"> * </span>';
                                            }		    
                                        endforeach;		    
                                    endforeach;
                                ?>    
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:24px">Sprinkler Inspection Report</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>          
                            <tr>
                                <td align="right" class="Cpage">
                                    Date:
                                    <?php if($record['SprinklerReport']['finish_date'] != ''){
                                        echo  date('m/d/Y',strtotime($record['SprinklerReport']['finish_date']));
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><i>Prepared for:</i></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><?php echo ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname'])?></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><?php echo $clientResult['User']['site_name'];?></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage">
                                <?php
                                    if(!empty($clientResult['User']['site_country_id'])){
                                        echo $this->Common->getCountryName($clientResult['User']['site_country_id']).', ';
                                }
                                if(!empty($clientResult['User']['site_state_id'])) { echo $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                echo $clientResult['User']['site_city'];
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><?php echo $clientResult['User']['site_phone'];?></td>
                            </tr>   
                            <tr>
                                <td align="left" class="Cpage" >Prepared By:</td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['Company']['name'];?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['address']?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage">
                                <?php
                                    if(!empty($spResult['Country']['name'])){
                                        echo $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) { echo $spResult['State']['name'].', '; }
                                    $clientCity = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
                                    echo $clientCity; 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['phone'];?></td>
                            </tr> 
                            <!--<tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="center"><h2><?php //echo ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']);?></h2></td>
                            </tr>-->          
                        </table>
                    </td>
                </tr>
            </table>
            <h3 align="center">Sprinkler Inspection Report</h3>
            <h4 class="table_title" style="text-align: center; font-size: 18px; padding:14px 0;">Agreement and Report of Inspection</h4>
            <table cellpadding="0" cellspacing="0" border="0" class="table_new" style="border:1px solid #C6C6C6">
                <tr>
                    <td>
                        <b>Days Left for report Submission: <?php echo $daysRemaining;?></b>
                    </td>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td width="100%" align="right">
                                    <?php
                                        $options=array("monthly"=>"Monthly","Quarterly"=>"Quarterly","semi_annual"=>"Semi-Annual","Annual"=>"Annual");
                                        $attributes=array('legend'=>false,'separator'=>'&nbsp;&nbsp;','class'=>'validate[required]');
                                        echo $this->Form->radio('SprinklerReport.inspection_type',$options,$attributes);
                                    ?>				    
                                </td>                                
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6;"><b>Client information</b></th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Building Information</strong></td>
                                <td><?php echo $clientResult['User']['client_company_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $clientResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $clientCity = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];?>
                                <td> <?php echo $clientCity.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $clientResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $clientResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td> <?php echo $clientResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6;"><b>Site Information</b></th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Site Name:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
                                <td> <?php echo $siteCity.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $siteaddrResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><th colspan="2" align="left" style="background-color: #C6C6C6;"><b>Service Provider Info</b></th></tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Name:</strong></td>
                                <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $spResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
                                <td><?php echo $spCity.', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $spResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $spResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $spResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="padding: 5px">
                <div style="border:2px solid #000; padding:10px; margin:10px">
                    <?php echo $texts;?>
                </div>
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="table_new" style="border: 1px solid #C6C6C6">			    			    
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label" style="width: 50%"><strong>Date of Work</strong></td>
                                <td style="width: 50%">
                                <?php
                                    if($record['SprinklerReport']['date_of_work']!=""){
                                        echo  date('m/d/Y',strtotime($record['SprinklerReport']['date_of_work']));
                                    }
                                ?>
                                </td>                                
                            </tr>				      
                            <tr>
                                <td class="table_label"><strong>Start Date of the Service</strong></td>
                                <td>
                                    <?php
                                        if($record['SprinklerReport']['start_date_service']!=""){
                                            echo  date('m/d/Y',strtotime($record['SprinklerReport']['start_date_service']));
                                        }
                                    ?>
                                </td>                                
                            </tr>
                            <tr>
                                <td class="table_label"><strong>End Date of the Service</strong></td>
                                <td>
                                    <?php
                                        if($record['SprinklerReport']['end_date_service']!=""){
                                            echo  date('m/d/Y',strtotime($record['SprinklerReport']['end_date_service']));
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Date of Performed Work </strong></td>
                                <td>
                                    <?php
                                        if($record['SprinklerReport']['date_performed_work']!=""){
                                            echo  date('m/d/Y',strtotime($record['SprinklerReport']['date_performed_work']));
                                        }
                                    ?>
                                </td>                                
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Deficiency</strong></td>
                                <td><?php echo $record['SprinklerReport']['deficiency']; ?></td>                                
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Recommendations</strong></td>
                                <td><?php echo $record['SprinklerReport']['recommendations']; ?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Inspection 100% complete and Passed</strong></td>
                                <td><?php echo $record['SprinklerReport']['inspection']; ?></td>                                
                            </tr>				      
                        </table>
                    </td>
                </tr>    
            </table>
            
            <table cellpadding="0" cellspacing="0" border="0" class="table_new" style="border: 1px solid #C6C6C6">
                <tr>
                    <td colspan="2"  style="width: 70%">1. GENERAL</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%">Yes</td>
                    <td style="width: 5%">No</td>
                    <td style="width: 5%">Na</td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">a.</b> Have there been any changes in the occupancy classification, machinery or operations since the last inspection?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">b.</b> Have there been any changes or repairs to the fire protection systems since the last inspection?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">c.</b> If a fire has occurred since the last inspection, have all damaged sprinkler system components been replaced?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">d.</b> Has the piping in all dry systems been checked for proper pitch within the past five years?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">
                        <b class="alpha">
                            Date last checked:
                            <?php
                                if($record['SprinklerReportGeneral']['general_d_date']!=""){
                                    echo  date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_d_date']));
                                }
                            ?>
                        </b>
                    </td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">e.</b> Has the piping in all systems been checked for obstructive materials?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">
                        <b class="alpha">
                            Date last checked:
                            <?php
                                if($record['SprinklerReportGeneral']['general_e_date']!=""){
                                    echo  date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_e_date']));
                                }
                            ?>
                        </b>    
                    </td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">f.</b> Have all fire pumps been tested to full capacity using hose streams or flow meters within the past 12 months?</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_f_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_f_date'])).'</b>';
                            }
                        ?>                        
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">g.</b> Are gravity, surface or pressure tanks protected from freezing?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">h.</b> Are sprinklers newer than 50 years old?           QR (20yr)        Dry (10 yr)         >325F/163C (5yr)           Corrosive env't.  (5yr.)</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">i.</b> Are extra high temperature solder sprinklers free from regular exposure to temperatures near 300F/149C?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">j. </b> Have gauges been tested, calibrated or replaced in the last 5 years?</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_j_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_j_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">k. </b>Alarm valves and associated trim been internally inspected past 5 years?</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_k_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_k_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">l.</b> Check valves internally inspected in the last 5 years?</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_l_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_l_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">m.</b> Has the private fire main been flow tested in last 5 years?</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_m_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_m_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">n.</b> Standpipe 5 and 3 year requirements.</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_n_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_n_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">1. Dry standpipe hydrostatic test</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_n_1_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_n_1_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">2. Flow test</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_n_2_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_n_2_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">3. Hose hydrostatic test (5 years from new, every 3 years after)</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_n_3_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_n_3_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">4. Pressure reducing/control valve test</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_n_4_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_n_4_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha"> o.</b> Have pressure reducing/control valves been tested at full flow within the past 5 years?</td>
                    <td style="width: 15%">
                        <?php
                            if($record['SprinklerReportGeneral']['general_o_date']!=""){
                                echo  '<b class="alpha">'.date('m/d/Y',strtotime($record['SprinklerReportGeneral']['general_o_date'])).'</b>';
                            }
                        ?>
                    </td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">p.</b> Have master pressure reducing/control valves been tested at full flow within the past  1 year?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">q.</b> Have the sprinkler systems been extended to all areas of the building?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha"> r.</b>  Are the building areas protected by a wet system heated, including its blind attics and perimeter areas?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">s. </b> Are all exterior openings protected against the entrance of cold air?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td colspan="2"  style="width: 70%">2. CONTROL VALVES</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"></td>
                    <td style="width: 5%"></td>
                    <td style="width: 5%"></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">a. </b> Are all sprinkler system main control valves and all other valves in the appropriate open or closed position?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">b. </b> Are all control valves sealed or supervised in the appropriate position?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0 class="table_new" style="border: 1px solid #C6C6C6">
                            <tr>
                                <td>Control Valves</td>
                                <td># of Valves</td>
                                <td>Type</td>
                                <td>Easily Accessible</td>
                                <td>Signs</td>
                                <td>Valve Open</td>
                                <td>Secured</td>
                                <td>Sealed?Locked?Supvd.?</td>
                                <td>Supervision Operational</td>
                            </tr>
                            <tr>
                                <td>CITY CONNECTION</td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_valves'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_type'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_access'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_signs'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_vopen'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_secured'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_option'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_cconnection_signs'];?></td>
                            </tr>
                            <tr>
                                <td>TANK</td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_valves'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_type'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_access'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_signs'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_vopen'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_secured'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_option'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_tank_signs'];?></td>
                            </tr>
                            <tr>
                                <td>PUMP</td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_valves'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_type'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_access'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_signs'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_vopen'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_secured'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_option'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_pump_signs'];?></td>
                            </tr>
                            <tr>
                                <td>SECTIONAL</td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_valves'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_type'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_access'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_signs'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_vopen'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_secured'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_option'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_sectional_signs'];?></td>
                            </tr>
                            <tr>
                                <td>SYSTEM</td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_valves'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_type'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_access'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_signs'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_vopen'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_secured'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_option'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_system_signs'];?></td>
                            </tr>
                            <tr>
                                <td>ALARM LINE</td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_valves'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_type'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_access'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_signs'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_vopen'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_secured'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_option'];?></td>
                                <td><?php echo $record['SprinklerReportControlvalve']['cvalve_alarm_signs'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">3. WATER SUPPLIES</td>                    
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0 class="table_new" style="border: 1px solid #C6C6C6">
                            <tr>
                                <td>
                                   <b class="alpha">a.</b> Water Supply sources? City:
                                </td>
                                <td>
                                    <?php echo $record['SprinklerReportWatersupply']['wsupply_city'];?>
                                </td>
                                <td>
                                    Gravity Tank:
                                </td>
                                <td>
                                    <?php echo $record['SprinklerReportWatersupply']['wsupply_tank'];?>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            
                            <tr>
                                <td colspan="6">
                                    Water Flow Test Results Made During This Inspection
                                </td>
                            </tr>
                            <tr>
                                <td>Test Pipe Located</td>
                                <td>Size Test Pipe</td>
                                <td>Static Pressure Before</td>
                                <td>Flow Pressure</td>
                                <td colspan="2">Static Pressure After</td>
                            </tr>
                            <tr>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_1'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_1'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_1'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_1'];?></td>
                                <td colspan="2"><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_1'];?></td>
                            </tr>
                            <tr>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_2'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_2'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_2'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_2'];?></td>
                                <td colspan="2"><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_2'];?></td>
                            </tr>
                            <tr>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_3'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_3'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_3'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_3'];?></td>
                                <td colspan="2"><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_3'];?></td>
                            </tr>
                            <tr>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_4'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_4'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_4'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_4'];?></td>
                                <td colspan="2"><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_4'];?></td>
                            </tr>
                            <tr>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_5'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_5'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_5'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_5'];?></td>
                                <td colspan="2"><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_5'];?></td>
                            </tr>
                            <tr>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_6'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_6'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_6'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_6'];?></td>
                                <td colspan="2"><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_6'];?></td>
                            </tr>
                            <tr>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_7'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_7'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_7'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_7'];?></td>
                                <td colspan="2"><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_7'];?></td>
                            </tr>
                            <tr>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_located_8'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_pipe_size_8'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_before_8'];?></td>
                                <td><?php echo $record['SprinklerReportWatersupply']['wsupply_flow_pressure_8'];?></td>
                                <td colspan="2"><?php echo $record['SprinklerReportWatersupply']['wsupply_static_pressure_after_8'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"  style="width: 70%">4. TANKS, PUMPS, FIRE DEPT. CONNECTION</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%">Yes</td>
                    <td style="width: 5%">No</td>
                    <td style="width: 5%">Na</td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">a.</b> Do fire pumps, gravity, surface or pressure tanks appear to be in good external conditions?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">b.</b> Are gravity, surface and pressure tanks at the proper pressure and/or water levels?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">c.</b> Has the storage tank been internally inspected in the last 3 yrs. (unlined) or 5 yrs. (lined)?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">d.</b> Are fire dept. connections in satisfactory condition, couplings free, caps or plugs in place and check valves tight?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">e.</b> Are fire dept. connections visible and accessible?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td colspan="2"  style="width: 70%">5. WET SYSTEMS</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%">Yes</td>
                    <td style="width: 5%">No</td>
                    <td style="width: 5%">Na</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0 class="table_new" style="border: 1px solid #C6C6C6">
                            <tr>
                                <td>a. No. of systems:</td>
                                <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_a'];?></td>
                                <td>Make & Model:</td>
                                <td><?php echo $record['SprinklerReportWetsystem']['wetsystem_a_makemodel'];?></td>
                            </tr>
                        </table>
                    </td>                    
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">b.</b> Are cold weather valves in the appropriate open or closed position?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">If closed, has piping been drained ?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">c.</b> Has the Customer been advised that cold weather valves are not recommended?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">d.</b> Have all the antifreeze systems been tested in the past year?</td>
                    <td style="width: 15%"><?php echo $record['SprinklerReportWetsystem']['wetsystem_d_date'];?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="5">
                        The antifreeze tests indicated protection to:
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">System 1</td>
                    <td style="width: 15%">Temperature<?php echo $record['SprinklerReportWetsystem']['wetsystem_1_temp'];?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">System 2</td>
                    <td style="width: 15%">Temperature<?php echo $record['SprinklerReportWetsystem']['wetsystem_2_temp'];?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">System 3</td>
                    <td style="width: 15%">Temperature<?php echo $record['SprinklerReportWetsystem']['wetsystem_3_temp'];?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">System 4</td>
                    <td style="width: 15%">Temperature<?php echo $record['SprinklerReportWetsystem']['wetsystem_4_temp'];?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">System 5</td>
                    <td style="width: 15%">Temperature<?php echo $record['SprinklerReportWetsystem']['wetsystem_5_temp'];?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">System 6</td>
                    <td style="width: 15%">Temperature<?php echo $record['SprinklerReportWetsystem']['wetsystem_6_temp'];?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                
                <tr>
                    <td colspan="2"  style="width: 70%">6. DRY SYSTEM</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%">Yes</td>
                    <td style="width: 5%">No</td>
                    <td style="width: 5%">Na</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0 class="table_new" style="border: 1px solid #C6C6C6">
                            <tr>
                                <td>a. No. of systems:</td>
                                <td><?php echo $record['SprinklerReportDrysystem']['drysystem_a'];?></td>
                                <td>Make & Model: Partial</td>
                                <td><?php echo $record['SprinklerReportDrysystem']['drysystem_a_makemodel'];?></td>
                                <td>Full</td>
                                <td><?php echo $record['SprinklerReportDrysystem']['drysystem_a_full'];?></td>
                            </tr>
                            <tr>
                                <td>Date last trip tested:</td>
                                <td><?php echo $record['SprinklerReportDrysystem']['drysystem_a_date'];?></td>
                            </tr>
                        </table>    
                    </td>                    
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">b.</b>Are the air pressure(s) and priming water level(s) normal?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">c.</b>Did the air compressor(s) operate satisfactorily?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">d.</b>Air compressor(s) oil checked?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%">Belt(s)?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">e.</b>Were Low Point drains drained during this inspection?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0 class="table_new" width="60%" style="border: 1px solid #C6C6C6">
                            <tr>
                                <td>No. of Drains:</td>
                                <td><?php echo $record['SprinklerReportDrysystem']['drysystem_e_drains'];?></td>                                                                
                            </tr>
                            <tr>
                                <td>Locations:</td>                                
                            </tr>
                            <tr>
                                <td>1.<?php echo $record['SprinklerReportDrysystem']['drysystem_e_locations_1'];?> </td>
                                <td>2.<?php echo $record['SprinklerReportDrysystem']['drysystem_e_locations_2'];?> </td>
                            </tr>
                            <tr>
                                <td>3.<?php echo $record['SprinklerReportDrysystem']['drysystem_e_locations_3'];?> </td>
                                <td>4.<?php echo $record['SprinklerReportDrysystem']['drysystem_e_locations_4'];?> </td>
                            </tr>
                        </table>    
                    </td>                    
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">f.</b>Did all quick opening devices operate satisfactorily?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0 class="table_new">
                            <tr>
                                <td style="width: 40%">Make & Model:</td>
                                <td style="width: 60%"><?php echo $record['SprinklerReportDrysystem']['drysystem_f_makemodel'];?></td>                                                                
                            </tr>                            
                        </table>    
                    </td>                    
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">g.</b>Did all the dry valves operate satisfactorily during this inspection?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">h.</b>Is the dry valve house heated?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">i.</b>Do the dry valves appear to be protected from freezing?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td colspan="2"  style="width: 70%">7. SPECIAL SYSTEMS</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%">Yes</td>
                    <td style="width: 5%">No</td>
                    <td style="width: 5%">Na</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0>
                            <tr>
                                <td>a. No. of systems:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_a'];?></td>
                                <td>Make & Model :</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_a_makemodel'];?></td>
                            </tr>
                            <tr>
                                <td>Type:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_a_type'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">b.</b>Were valves tested as required?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">c.</b>Did all heat responsive systems operate satisfactorily?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">d.</b>Did the supervisory features operate during testing?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0>
                            <tr>
                                <td>Heat Responsive Devices:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_heat'];?></td>
                                <td>Type:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_type'];?></td>
                            </tr>
                            <tr>
                                <td>Type of test:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_type_test'];?></td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td>Valve No.:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_valve_1'];?></td>                                
                                <td colspan="2"><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_valve_2'];?></td>
                            </tr>
                            <tr>
                                <td>Seconds:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_second_1'];?></td>                                
                                <td colspan="2"><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_d_second_2'];?></td>
                            </tr>                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td colspan="5"><b class="alpha">e.</b>Has a supplemental test form for this system been completed and provided to the customer?</td>                    
                </tr>
                <tr>
                    <td colspan="6">
                        <table cellpadding=0 cellspacing=0 border=0>
                            <tr>
                                <td colspan="6">Auxiliary equipment:</td>                                
                            </tr>
                            <tr>
                                <td>No.</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_e_number'];?></td>
                                <td>Type:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_e_type'];?></td>
                            </tr>
                            <tr>
                                <td>Location:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_e_location'];?></td>
                                <td>Test results:</td>
                                <td><?php echo $record['SprinklerReportSpecialsystem']['specialsystem_e_test_result'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"  style="width: 70%">8. ALARMS</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%">Yes</td>
                    <td style="width: 5%">No</td>
                    <td style="width: 5%">Na</td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">a.</b>Did the water motor(s) and gong(s) operate during testing?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">b.</b>Did the electric alarm(s) operate during testing?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">c.</b>Did the supervisory alarm(s) operate during testing?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td colspan="2"  style="width: 70%">9. SPRINKLERS - PIPING</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%">Yes</td>
                    <td style="width: 5%">No</td>
                    <td style="width: 5%">Na</td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">a.</b>Do sprinklers generally appear to be in good external condition?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">b.</b>Do sprinklers generally appear to be free of corrosion, paint, or loading and visible obstructions?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">c.</b>Are extra sprinklers and sprinkler wrench available on the premises?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">d.</b>Does the exposed exterior condition of piping, drain valves, check valves, hangers, pressure gauges, open sprinklers and strainers appear to be satisfactory?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">e.</b>Does the hand hose on the sprinkler system appear to be in satisfactory condition?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
                <tr>
                    <td style="width: 2%"></td>
                    <td style="width: 68%"><b class="alpha">f.</b>Does there appear to be proper clearance between the top of all storage and the sprinkler deflector?</td>
                    <td style="width: 15%"></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                    <td style="width: 5%"><?php echo $form->input('',array('type'=>'checkbox','label'=>false,'div'=>false));?></td>
                </tr>
            </table>
            <table cellpadding=0 cellspacing=0 class="table_new" border=0 style="border: 1px solid #C6C6C6">
                <tr>
                    <td>
                        <img src="/app/webroot/img/signature_images/<?php echo $chkSign['Signature']['signature_image'];?>">
                    </td>                    
                </tr>
                <tr>
                    <td>
                    Inspector's Signature
                    </td>
                </tr>
                <tr>
                    <td>
                    &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    Thanks & Regards
                    </td>
                </tr>
                <tr>
                    <td>
                    ReportsOnlineplus.com
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>