<?php

App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();


$html = '<table  cellspacing="0" cellpadding="2" width="100%">
	<tr>
	   <td colspan="2" align="center"><strong>Emergency Light / Exit Sign</strong> </td>
	</tr>
        <tr>
            <td colspan="2">
                <table width="100%">
          <tr>
            <td align="center"><img src="/app/webroot/img/company_logo/'.$spResult['Company']['company_logo'].'"></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b>'.$spResult['Company']['name'].'</b></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:21px">';		
		$arr=explode(',',$spResult['Company']['interest_service']);
	    
		    foreach($arr as $key=>$val):
		    foreach($reportType as $key1=>$val1):

		    if($val== $key1)
		    {
			
			 if($key1 == 9){
                            $html.=  $spResult['Company']['interest_service_other'];
                            }else{
                                $html.= $val1;
                                }
                       $html.=' <span class="red"> * </span>';
		     }
		    
		    endforeach;
		    
		    endforeach;	

		 $html.=   '</td></tr>
            
            
          
          
          
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:24px">Emergency Light / Exit Sign Report</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
          </tr>          
          <tr>
            <td align="right" class="Cpage" >Date:';if($record['EmergencyexitReport']['finish_date'] != ''){
                $html.= date('m/d/Y',strtotime($record['EmergencyexitReport']['finish_date'])); } $html.='</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" ><i>Prepared for:</i></td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_name'].'</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'; if(!empty($clientResult['User']['site_country_id'])){
                                        $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                        if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                        $html.= $clientResult['User']['site_city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="center" class="Cpage" >'. $clientResult['User']['site_phone'].'</td>
          </tr>   
          <tr>
            <td align="left" class="Cpage" >Prepared By:</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
          <tr>
            <td align="left" class="Cpage">'.$spResult['Company']['name'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['address'].'</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'; if(!empty($spResult['Country']['name'])){
                                        $html.= $spResult['Country']['name'].', '; }
                                        if(!empty($spResult['State']['name'])) { $html.= $spResult['State']['name'].', '; } 
                                        $html.= $clientResult['User']['city']; 
               $html.= '</td>
          </tr>
          <tr>
            <td align="left" class="Cpage">'.$spResult['User']['phone'].'</td>
          </tr> 
          <tr>
            <td align="left" class="Cpage">&nbsp;</td>
          </tr>  
         <tr>
            <td align="center"><h2>'. ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']) .'</h2></td>
          </tr>
          
        </table>
            </td>
        </tr>
  <tr>
    <td width="60%" border="1">
      <table style="font-size:20px" >
        	<tr>
        	   <td colspan="2" align="center"><strong>Client Info </strong> </td>	   
        	   <td colspan="2" align="center"><strong>Service Provider Info</strong> </td>	
        	</tr>
        	<tr>
        	   <td width="20%" align="right"><strong>Name:</strong></td>
        	   <td width="30%" align="left">'.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'</td>
        	   <td width="20%" align="right"><strong>Name:</strong></td>
        	   <td width="30%" align="left">'.ucwords($spResult['User']['fname'].' '.$spResult['User']['lname']).'</td>
        	</tr>
        	<tr>
        	   <td width="20%" align="right"><strong>Address:</strong></td>
        	   <td width="30%" align="left">'.$clientResult['User']['address'].'</td>
        	   <td width="20%" align="right"><strong>Address:</strong></td>
        	   <td width="30%" align="left">'.$spResult['Company']['address'].'</td>
        	</tr>
        	<tr>
        	   <td width="20%" align="right"><strong>City,State,Zip:</strong></td>
        	   <td width="30%" align="left">'.$clientResult['User']['site_city_state_zip'].'</td>
        	   <td width="20%" align="right"><strong>City,State,Zip:</strong></td>
        	   <td width="30%" align="left">'.$spResult['User']['site_city_state_zip'].'</td>
        	</tr>
        	<tr>
        	   <td width="20%" align="right"><strong>Phone:</strong></td>
        	   <td width="30%" align="left">'.$clientResult['User']['phone'].'</td>
        	   <td width="20%" align="right"><strong>Phone:</strong></td>
        	   <td width="30%" align="left">'.$spResult['Company']['phone'].'</td>
        	</tr>
        	<tr>
        	   <td width="20%" align="right"><strong>Building: 	</strong></td>
        	   <td width="30%" align="left">'.$clientResult['User']['site_name'].'</td>
        	   <td width="20%" align="right"><strong>Office License</strong></td>
        	   <td width="30%" align="left">&nbsp;</td>
        	</tr>
      </table>
    </td>
    <td width="40%" border="1">
      <table>
        <tr>  
          <td colspan="7" align="center"><strong>Summary</strong></td>
        </tr>
        <tr style="font-size:14px">
          <td>&nbsp;</td>
          <td align="center">Exit Signs</td>
          <td align="center">>Emergency Lights</td>
          <td align="center">Exit Pass</td>
          <td align="center">Exit Fail</td>
          <td align="center">Emergency Pass </td>
          <td align="center">Emergency Fail</td>
        </tr>
        <tr style="font-size:15px">
          <td><strong>Surveyed</strong></td>
          <td align="center">'.$record['EmergencyexitReport']['survey_exit'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['survey_emergency'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['survey_exit_pass'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['survey_exit_fail'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['survey_emergency_pass'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['survey_emergency_fail'].'</td>
        </tr>
        <tr style="font-size:15px">
          <td><strong>Total</strong></td>
          <td align="center">'.$record['EmergencyexitReport']['total_exit'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['total_emergency'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['total_exit_pass'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['total_exit_fail'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['total_emergency_pass'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['total_emergency_fail'].'</td>          
        </tr>
        <tr style="font-size:15px">
          <td><strong>Difference</strong></td>          
          <td align="center">'.$record['EmergencyexitReport']['diff_exit'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['diff_emergency'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['diff_exit_pass'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['diff_exit_fail'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['diff_emergency_pass'].'</td>
          <td align="center">'.$record['EmergencyexitReport']['diff_emergency_fail'].'</td>
        </tr>        
      </table>
    </td>
  </tr>
  
  </table>
  <table>
  <tr><td>&nbsp;</td></tr>
  </table>
  
  <table style="text-align:left">
  <tr>
    <td><strong>Location</strong></td>
    <td><strong>MFD Date</strong></td>
    <td><strong>Type</strong></td>
    <td><strong>Status</strong></td>
    <td><strong>Recommendation / Comment</strong></td>
  </tr>';
  foreach($record['EmergencyexitReportRecord'] as $value){
  $html .='<tr>
    <td>'.$value['location'].'</td>
    <td>'.$value['mfd_date'].'</td>
    <td>'.$value['type'].'</td>
    <td>'.$value['status'].'</td>
    <td>'.$value['comment'].'</td>
  </tr>';
  }
  $html .='</table>
  	<table>
    <tr>
	    <td colspan="6">&nbsp;</td>	    
	  </tr>
	  <tr>
	    <td colspan="6"><u>Attachments</u>:</td>	    
	  </tr>	';
	    foreach($attachmentdata as $attachdata){
   	  $html .='<tr>
  	    <td colspan="2">'.$attachdata['Attachment']['attach_file'].'</td>	
        <td>'.date('m/d/Y',strtotime($attachdata['Attachment']['created'])).'</td>	
        <td colspan="2">'.$this->Common->getClientName($attachdata['Attachment']['user_id']).'('.$attachdata['Attachment']['added_by'].')</td>	 
        <td>&nbsp;</td>	         
  	</tr>'; }
    	$html .='<tr>
  	    <td colspan="6">&nbsp;</td>	    
  	</tr>
    <tr>
  	    <td colspan="6"><u>Notifications</u>:</td>	    
  	</tr>';
    foreach($notifierdata as $notifydata){
    $html .='<tr>
  	    <td colspan="2">'.$notifydata['Notification']['notification'].'</td>	
        <td>'.date('m/d/Y',strtotime($notifydata['Notification']['created'])).'</td>	
        <td colspan="2">'.$this->Common->getClientName($notifydata['Notification']['user_id']).'('.$notifydata['Notification']['added_by'].')</td>	 
        <td>&nbsp;</td>	         
  	</tr>'; }
        $html.='</table>

                <table width="100%">
                 <tr><td colspan="2" align="center"><h3>&nbsp;</h3></td></tr>
             <tr><td colspan="2" align="center"><h3>Deficiency</h3></td></tr>
             <tr>
                 <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                 <br/>
                 '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                 <br/>
                 '.$clientResult['User']['site_name'].'
                 <br/>';
                 if(!empty($clientResult['User']['site_country_id'])){
                                 $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                 if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                 $html.= $clientResult['User']['site_city'].' <br/>
                 '.$clientResult['User']['site_phone'].'
                 </td>
                 <td width="50%" align="right" style="padding-right:30px;">
                     <i><b>Prepared By:</b></i>
                     <br/>'.
                     $spResult['Company']['name'].'
                     <br/>
                     '. $spResult['User']['address'].'
                     <br/>
                     ';	if(!empty($spResult['Country']['name'])){
                               $html.= $spResult['Country']['name'].', ';
                             }
                             if(!empty($spResult['State']['name'])) {
                                $html.=  $spResult['State']['name'].', ';
                             }
                            $html.=  $spResult['User']['city'];
                     
                    $html.= ' <br/>
                     Phone  '.$spResult['User']['phone'].'
                 </td>
             </tr>
         </table>
         <table width="100%" class="tbl input-chngs1">
             <tr>
                 
                 <th width="10%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                 <th width="90%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Description</b></th>
                 
             </tr>';
             
             if(!empty($Def_record))
             {
            // pr($codeData);
             foreach($Def_record as $Def_record) {
            
             $html.='<tr>
                 
                 <td valign="top">'. $Def_record['Code']['code'].'</td>
                 <td valign="top">'. $Def_record['Code']['description'].'</td>
                 
             </tr>';
             }
             } else
             {
              $html.= '<tr><td colspan="4" align="center">No Deficiency Found.</td></tr>';  
             }
             $html.='</table>
             <table width="100%">
             <tr><td colspan="2" align="center"><h3>&nbsp;</h3></td></tr>
                 <tr>
                     <td align="center" colspan="2"><h3>Recommendation</h3></td>
                 </tr>
                 <tr>
                     <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
                     <br/>
                     '.ucwords($clientResult['User']['fname'].' '.$clientResult['User']['lname']).'
                     <br/>
                     '.$clientResult['User']['site_name'].'
                     <br/>
                     ';
                     if(!empty($clientResult['User']['site_country_id'])){
                                     $html.= $this->Common->getCountryName($clientResult['User']['site_country_id']).', '; }
                                     if(!empty($clientResult['User']['site_state_id'])) { $html.= $this->Common->getStateName($clientResult['User']['site_state_id']).', '; } 
                                     $html.= $clientResult['User']['site_city'].'
                     <br/>
                     '.$clientResult['User']['site_phone'].'
                     </td>
                     <td width="50%" align="right" style="padding-right:30px;">
                         <i><b>Prepared By:</b></i>
                         <br/>
                         '.$spResult['Company']['name'].'
                         <br/>
                         '.$spResult['User']['address'].'
                         <br/>';
                             if(!empty($spResult['Country']['name'])){
                                     $html.= $spResult['Country']['name'].', ';
                                 }
                                 if(!empty($spResult['State']['name'])) {
                                     $html.= $spResult['State']['name'].', ';
                                 }
                                $html.=  $spResult['User']['city'];
                        
                       $html.=   '<br/>
                         Phone  '.$spResult['User']['phone'].'
                     </td>
                     
                     </tr>
             </table>
                 
             <table width="100%" class="tbl input-chngs1" >
              <tr width="100%">
                  
                  <th width="20%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code</b></th>
                  <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Code Description</b></th>
                   <th width="40%" style="text-align:left;background-color:#3688B7;color:#fff;"><b>Recommendation</b></th>
                  
              </tr>';
              if(!empty($Def_record_recomm))
              {
              
              foreach($Def_record_recomm as $Def_record_recomm) {
            
              $html.='<tr width="100%">
                  
                  <td valign="top">'.$Def_record_recomm['Code']['code'].'</td>
                  <td valign="top">'.$Def_record_recomm['Code']['description'].'</td>
                   <td valign="top">'.$Def_record_recomm['Deficiency']['recommendation'].'</td>
                  
              </tr>';
               }
              } else
              {
               $html.='<tr><td colspan="4" align="center">No Recommendation Found.</td></tr>';   
              }
              $html.='</table>
              
 <table width="100%" class="tbl">
     <tr width="100%">
         <td  align=center  style="border:none;"><span style="font-size:15px; font-weight:bolder;"><h3>Signature Page</h3></span></td>                        
     </tr>                   
 </table>
 <table width="100%">
     <tr>
         <td style="border:none;"><b>Inspector Signature</b><br/><br/>';
         $name=$this->Common->getInspectorSignature($record['EmergencyexitReport']['report_id'],$record['EmergencyexitReport']['servicecall_id'],$record['EmergencyexitReport']['inspector_id']);
         if($name!='empty'){
             $html.= '<img src="/app/webroot/img/signature_images/'.$name.'">';
         }
         $html.='                        
         </td>
         <td style="border:none; vertical-align:top;" align=right><b>Client Signature</b></td>
   
     </tr>
     <tr>
         <td style="border:none;">';
         $dateCreated=$this->Common->getSignatureCreatedDate($record['EmergencyexitReport']['report_id'],$record['EmergencyexitReport']['servicecall_id'],$record['EmergencyexitReport']['inspector_id']);
           
         if($dateCreated!='empty'){
         $html.= $this->Common->getClientName($record['EmergencyexitReport']['inspector_id'])."<br/><br/>".
           $time->Format('m-d-Y',$dateCreated);
         }
        $html.='</td>
         <td align=left style="border:none;"></td>
   
     </tr>'
        
    ;
  	
	 $html .= '</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
 
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus_EmergencyExit.pdf', 'D');die;