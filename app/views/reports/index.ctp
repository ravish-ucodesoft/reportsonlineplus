<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<section class="register-wrap">
	<h1 class="main-hdng2"> Emergency Light / Exit Signs</h1>
    <section class="form-box2-wrapper2">
        <!--one complete form-->
        <section class="form-box2">
            
            <ul class="register-form2">
            	<h2>Customer</h2>
                <li>
                    <label>Name<span>*</span></label>
                    <p><span class="input-field2"><input type="text"/></span></p>
                </li>                
                <li>
                    <label>Phone<span>*</span></label>
                    <p><span class="input-field2"><input type="text"/></span></p>
                </li>
                <li>
                    <label>Street</label>
                    <p><span class="input-field2"><input type="text"/></span></p>
                </li> 
                <li>
                    <label>City</label>
                    <p><span class="input-field2"><input type="text"/></span></p>
                </li>                 
                <li>
                    <label>State<span>*</span></label>
                    <p><select class="wdth-250"><option>India</option><option>Canada</option></select></p>
                </li>                            
            </ul>
        </section>
        <!--one complete form ends-->
        <!--one complete form-->
        <section class="form-box2">
            
            <ul class="register-form2">
            	<h2>Site Location</h2>
                 <li>
                    <label>Name<span>*</span></label>
                    <p><span class="input-field2"><input type="text"/></span></p>
                </li>                
                <li>
                    <label>Phone<span>*</span></label>
                    <p><span class="input-field2"><input type="text"/></span></p>
                </li>
                <li>
                    <label>Street</label>
                    <p><span class="input-field2"><input type="text"/></span></p>
                </li> 
                <li>
                    <label>City</label>
                    <p><span class="input-field2"><input type="text"/></span></p>
                </li>                 
                <li>
                    <label>State<span>*</span></label>
                    <p><select class="wdth-250"><option>India</option><option>Canada</option></select></p>
                </li>
            </ul>
        </section>
        <!--one complete form ends-->
        <!--one complete form-->
        <section class="form-box2 wdth-46">
        	<ul class="summary-tbl2">
            <h2>Summary</h2>
            	<li class="summry-hdr">
                	<p>&nbsp;</p>
                    <p>Exit Signs</p>
                    <p>Emergency Lights</p>
                    <p>Status</p>
                </li>
                <li>
                	<p class="summry-lbl2">Surveyed</p>
                    <p><span class="input-field2"><input type="text" readonly="readonly"/></span></p>
                    <p><span class="input-field2"><input type="text" readonly="readonly"/></span></p>
                    <p><select class="wdth-250"><option>Pass</option><option>Fail</option></select></p>
                </li>
                <li>
                	<p class="summry-lbl2">Surveyed</p>
                    <p><span class="input-field2"><input type="text" readonly="readonly"/></span></p>
                    <p><span class="input-field2"><input type="text" readonly="readonly"/></span></p>
                    <p><select class="wdth-250"><option>Pass</option><option>Fail</option></select></p>
                </li>
                <li>
                	<p class="summry-lbl2">Surveyed</p>
                    <p><span class="input-field2"><input type="text" readonly="readonly"/></span></p>
                    <p><span class="input-field2"><input type="text" readonly="readonly"/></span></p>
                    <p><select class="wdth-250"><option>Pass</option><option>Fail</option></select></p>
                </li>
                <li>
                	<p class="summry-lbl2">Surveyed</p>
                    <p><span class="input-field2"><input type="text" readonly="readonly"/></span></p>
                    <p><span class="input-field2"><input type="text" readonly="readonly"/></span></p>
                    <p><select class="wdth-250"><option>Pass</option><option>Fail</option></select></p>
                </li>
            </ul>     
        </section>
        <!--one complete form ends-->   
    </section>

   	<ul class="emrgncy-tbl2" style="width:100%;">
    		<li class="emrgncy-hdr2">
            <section style="width:54%">
            	<p style="width:10%">#</p>
                <p style="width:20%">Location</p>
                <p>MFD Date.</p>
                <p>Type</p>
                <p>Status</p>
            </section>
            <section style="width:46%; float:left;">      
            	<p style="width:100%;">Recommendation / Comment</p>      	
            </section>
            </li>
        	<li>
            <section style="width:54%">
            	<p style="width:10%">1</p>
                <p style="width:20%">&nbsp;</p>
                <p><span class="input-field2"><input type="text"/></span></p>
                <p><span class="input-field2"><input type="text"/></span></p>
                <p><span class="input-field2"><input type="text"/></span></p>
            </section>
            <section style="width:46%; float:left;">      
            	<p style="width:100%;"><span class="input-field2"><input type="text" style="width:70%"/></span></p>      	
            </section>
            </li>
            <li>
            <section style="width:54%">
            	<p style="width:10%">1</p>
                <p style="width:20%">&nbsp;</p>
                <p><span class="input-field2"><input type="text"/></span></p>
                <p><span class="input-field2"><input type="text"/></span></p>
                <p><span class="input-field2"><input type="text"/></span></p>
            </section>
            <section style="width:46%; float:left;">      
            	<p style="width:100%;"><span class="input-field2"><input type="text" style="width:70%"/></span></p>      	
            </section>
            </li>
            <li>
            <section style="width:54%">
            	<p style="width:10%">1</p>
                <p style="width:20%">&nbsp;</p>
                <p><span class="input-field2"><input type="text"/></span></p>
                <p><span class="input-field2"><input type="text"/></span></p>
                <p><span class="input-field2"><input type="text"/></span></p>
            </section>
            <section style="width:46%; float:left;">      
            	<p style="width:100%;"><span class="input-field2"><input type="text" style="width:70%"/></span></p>      	
            </section>
            </li>
            <li>
            <section style="width:54%">
            	<p style="width:10%">1</p>
                <p style="width:20%">&nbsp;</p>
                <p><span class="input-field2"><input type="text"/></span></p>
                <p><span class="input-field2"><input type="text"/></span></p>
                <p><span class="input-field2"><input type="text"/></span></p>
            </section>
            <section style="width:46%; float:left;">      
            	<p style="width:100%;"><span class="input-field2"><input type="text" style="width:70%"/></span></p>      	
            </section>
            </li>
            
      </ul>	

    
</section>