<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<?php echo $this->Html->css('reports'); ?>
<style>
.summary-tbl2 p {
    float: left;
    margin-bottom: 0;
    margin-left: 5px;
    width: 10%;
    font-size: 10px;
    padding-left: 7px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
.lbtxt{text-align:left;width:40%}
.input-chngs1 input {
    margin-right: 3px;
    width: 136px;
}
.lbl{
vertical-align:top;
}

</style>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#addform").validationEngine();
});

function confirmbox1(rId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveLocation(rId);    
  }
  else{
    return false;
  }
 }
function RemoveLocation(rId){
var servicecallid = jQuery('#servicelink').val();
 var reportid = jQuery('#reportlink').val();
 var spid = jQuery('#splink').val();
 var clientid = jQuery('#clientlink').val();
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delFaLoc/" + rId,                
                success : function(opt){
		    if(opt == 1){			  
		       window.location='/inspector/reports/firealarm?reportID='+reportid+'&clientID='+clientid+'&spID='+spid+'&serviceCallID='+servicecallid;
		       
		    }
                }
        });
}
function showform(id){
var divid = "editdiv_"+id;
document.getElementById(divid).style.display = "block";

  }
</script>

<section style="min-width:600px">
    <section class="register-wrap">
    <div class="headingtxt">Add New Location</div>
        <div style="width:100%;float:left;padding-right:10px;">
	    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
            <?php echo $form->create('', array('type'=>'POST','controller'=>'reports','action'=>'addFireAlarmLoc','name'=>'specialhazardReport','id'=>'addform','inputDefaults' => array('label' => false,'div' => false))); ?>
            <?php echo $form->input('FaAlarmDevicePlace.inspector_id',array('type'=>'hidden','value'=>$session_inspector_id)); ?>
        		<?php echo $form->input('FaAlarmDevicePlace.servicecall_id',array('type'=>'hidden','id'=>'servicelink','value'=>$servicecallId)); ?>
        		<?php echo $form->input('FaAlarmDevicePlace.report_id',array('type'=>'hidden','id'=>'reportlink','value'=>$reportId)); ?>
        		<?php echo $form->input('FaAlarmDevicePlace.sp_id',array('type'=>'hidden','id'=>'splink','value'=>$spId)); ?>
        		<?php echo $form->input('FaAlarmDevicePlace.client_id',array('type'=>'hidden','id'=>'clientlink','value'=>$clientId)); ?>
            <table class="tbl input-chngs1" width="100%">
		
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
		    <td  class="lbl">Location Name:</td>
		    <td><?php echo $form->input('FaAlarmDevicePlace.name',array('type'=>'text','style'=>'width:150px;','class'=>'validate[required]')); ?></td>
		</tr>
		
    <tr>
      <td colspan="2"><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:80px;float:right;')); ?></td>
    </tr>
	    </table>
            <?php echo $form->end(); ?>
         <table class="tbl input-chngs1" width="100%">
         
         <?php  foreach($locData as $data){ ?>
        <tr id="place_<?php echo $data['FaAlarmDevicePlace']['id'] ?>"> <td><?php echo $data['FaAlarmDevicePlace']['name']; ?></td><td><?php echo $html->link('Edit','javascript:void(0)',array('id'=>'editlink_'.$data['FaAlarmDevicePlace']['id'],'onclick'=>"showform('".$data['FaAlarmDevicePlace']['id']."')")); ?>
               &nbsp;<?php echo $html->link('Delete','javascript:void(0)',array('onclick'=>"confirmbox1('".$data['FaAlarmDevicePlace']['id']."')",'escape'=>false)); ?>
        </td></tr>
        <tr><td colspan="2"><div id="editdiv_<?php echo $data['FaAlarmDevicePlace']['id']; ?>" style="display:none;" >
        <!-----------------------------------Edit form------------------------------------------------>
        
        <?php echo $form->create('', array('type'=>'POST','controller'=>'reports','action'=>'editFireAlarmLoc','name'=>'specialhazardReport','id'=>'edit','inputDefaults' => array('label' => false,'div' => false))); ?>
            <?php echo $form->input('FaAlarmDevicePlace.id',array('type'=>'hidden','value'=>$data['FaAlarmDevicePlace']['id'])); ?>
            <?php echo $form->input('FaAlarmDevicePlace.inspector_id',array('type'=>'hidden','value'=>$session_inspector_id)); ?>
        		<?php echo $form->input('FaAlarmDevicePlace.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
        		<?php echo $form->input('FaAlarmDevicePlace.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
        		<?php echo $form->input('FaAlarmDevicePlace.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
        		<?php echo $form->input('FaAlarmDevicePlace.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
        <table class="tbl input-chngs1" width="100%" border="1">
		
      		<tr><td colspan="2">&nbsp;</td></tr>
      		<tr>
      		    <td  class="lbl">Location Name:</td>
      		    <td><?php echo $form->input('FaAlarmDevicePlace.name',array('type'=>'text','style'=>'width:150px;','value'=>$data['FaAlarmDevicePlace']['name'])); ?></td>
      		</tr>
      		
          <tr>
            <td colspan="2"><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:80px;float:right;')); ?></td>
          </tr>
      	</table>
            <?php echo $form->end(); ?>
        
        <!-----------------------------------Edit form------------------------------------------------>
        
        
        </div></td></tr>
         <?php } ?>
         </table>    
        </div>
    </section>
</section>