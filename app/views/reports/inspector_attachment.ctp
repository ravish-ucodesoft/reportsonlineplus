<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
?>
<script type="text/javascript">
function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxupload',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#CompanyImage").val(responseJSON.company_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/company_logo_temp/'+responseJSON.company_image+'" height="100px" width="200px">');
	    }
            });           
    }        
	window.onload = createUploader;
</script>
<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;     
}
#box {
    border: none;
}
.qq-uploader {
    margin-left: 6px;
    position: relative;
    width: 200px;
}

</style>
<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'extinguishers','name'=>'extinguisher','id'=>'extinguisher')); ?> 
<div id="content">
     <div id="box" style="width:700px;padding-left:10px;">
     <div class="form-box" style="width:90%">
        <h2 style="font-weight:bold;">Upload Attachment</h2>
        <ul class="register-form">
            <li>
                <label>&nbsp;</label>
                <p>
		    <div id="demo"></div>
		    <ul id="separate-list"></ul>
		<?php //echo $form->input('ExtinguisherReport.file',array('type'=>'file','label'=>false)); ?></p>
            </li>
	    
	     <li>
                <label>&nbsp;</label>
                <p>
		     <div id="uploaded_picture"  style="text-align:center;padding-left:90px;"></div>
		</p>
            </li>
	     
	    <li>
                <label>&nbsp;</label>
                <p><span class="blue-btn" style="float:right;"><input type="submit" value="Upload" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></td></p>
            </li>
            
        </ul>
    </div>
    <!--one complete form ends-->
      </div>
</div>
<?php echo $form->end(); ?>