<div id="content">
<h1>Standard Reports & Views</h1>
       			<table width="100%" cellspacing="3" cellpadding="3">
       			<tr>
       			    <td><?php echo $html->link('Weekly Schedule Statistics & Summary(Statistics Schedule View)',array('controller'=>'reports','action'=>'weekly_statistics'),array('alt'=>'Weekly Schedule Statistics & Summary(Statistics Schedule View)','title'=>'Weekly Schedule Statistics & Summary(Statistics Schedule View)'));?></td>
           	</tr>
       			<tr>
       			    <td><?php echo $html->link('Estimate your Weekly Payroll',array('controller'=>'reports','action'=>'weekly_statistics'),array('alt'=>'Estimate your Weekly Payroll','title'=>'Estimate your Weekly Payroll'));?></a></td>
       			</tr>
       			
       			<tr>
       			    <td><?php echo $html->link('Analyze shifts(Sortable List Schedule View)',array('controller'=>'onnows','action'=>'all_shift'),array('alt'=>'Analyze shifts(Sortable List Schedule View)','title'=>'Analyze shifts(Sortable List Schedule View)'));?></a></td>
       			</tr>
       			
       			<tr>
       			    <td><?php echo $html->link('List of Employees',array('controller'=>'reports','action'=>'emp_export'),array('alt'=>'List of Employees','title'=>'List of Employees'));?></td>
       		  </tr>
       			
       			<tr>
       			    <td><?php echo $html->link('Monthly Schedule View(Calendar-Monthly Schedule View)',array('controller'=>'reports','action'=>'monthly_statistics'),array('alt'=>'Monthly Schedule View(Calendar-Monthly Schedule View)','title'=>'Monthly Schedule View(Calendar-Monthly Schedule View)'));?></td>
       		  </tr>
       			
       			</table>
</div>