<?php echo $html->css('manager/style.css');?>
<?php echo $html->css('manager/theme.css');?>
<div id="wrapper">
    <div id="content">
        <div id="box">            
            <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td colspan="2" align="center"><strong>Extinguisher Inspection Report</strong> </td>
                </tr>    
                <tr>
                    <td colspan="2">
                        <table width="100%">
                            <!--<tr>
                                <td align="center"><img src="/app/webroot/img/company_logo/<?php //echo $spResult['Company']['company_logo']?>"></td>
                            </tr>-->
                            <tr>
                                <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b><?php echo $spResult['Company']['name']?></b></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:21px">
                                <?php 
                                    $arr=explode(',',$spResult['Company']['interest_service']);	    
                                    foreach($arr as $key=>$val):
                                        foreach($reportType as $key1=>$val1):
                                            if($val== $key1)
                                            {
                                                
                                                 if($key1 == 9){
                                                    echo  $spResult['Company']['interest_service_other'];
                                                    }else{
                                                        echo $val1;
                                                        }
                                               echo '<span class="red"> * </span>';
                                            }		    
                                        endforeach;		    
                                    endforeach;
                                ?>    
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:24px">Extinguisher Inspection Report</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                            </tr>          
                            <tr>
                                <td align="right" class="Cpage">
                                    Date:
                                    <?php if($record['ExtinguisherReport']['finish_date'] != ''){
                                        echo  date('m/d/Y',strtotime($record['ExtinguisherReport']['finish_date']));
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" ><i><b>Prepared for:</b></i></td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" class="Cpage" >
                                    <?php                                        
                                         $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
                                        echo ucwords($clientResult['User']['client_company_name']).'<br/>';
                                        echo ucwords($clientResult['User']['address']).', '.$cityName.', '.$clientResult['State']['name'].'<br/>';
                                        echo $clientResult['Country']['name'];
                                    ?>
                                </td>
                            </tr>                            
                            <tr>
                                <td align="center" class="Cpage" ><?php echo $clientResult['User']['site_phone'];?></td>
                            </tr>   
                            <tr>
                                <td align="left" class="Cpage" ><b>Prepared By:</b></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['Company']['name'];?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['address']?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php
                                    if(!empty($spResult['Country']['name'])){
                                        echo $spResult['Country']['name'].', ';
                                    }
                                    if(!empty($spResult['State']['name'])) { echo $spResult['State']['name'].', '; }
                                    $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
                                    echo $spCity;?></td>
                            </tr>
                            <tr>
                                <td align="left" class="Cpage"><?php echo $spResult['User']['phone'];?></td>
                            </tr> 
                            <!--<tr>
                                <td align="left" class="Cpage">&nbsp;</td>
                            </tr>  
                            <tr>
                                <td align="center"><h2><?php //echo ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']);?></h2></td>
                            </tr>-->          
                        </table>
                    </td>
                </tr>
            </table>
            <h3 align="center">Extinguisher Inspection Report</h3><br/>
            <h4 class="table_title" style="text-align: center; padding:14px 0;">Agreement and Report of Inspection</h4>
            <table cellpadding="0" cellspacing="0" border="0" class="table_new" style="border:1px solid #C6C6C6">                
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"> <b>Client information</b></th>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Building Information</strong></td>
                                <td><?php echo $clientResult['User']['client_company_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $clientResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $clientCity = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];?>
                                <td><?php echo $clientCity.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $clientResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $clientResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td> <?php echo $clientResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr>
                    <th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"><b>Site Information</b></th>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Site Name:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];?>
                                <td> <?php echo $siteCity.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $siteaddrResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr><th colspan="2" align="left" style="background-color: #C6C6C6; padding-left: 5px;"> <b>Service Provider Info</b></th></tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Name:</strong></td>
                                <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Address</strong></td>
                                <td><?php echo $spResult['User']['address'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>City/State/Zip:</strong></td>
                                <?php $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];?>
                                <td><?php echo $spCity.', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Country: </strong></td>
                                <td><?php echo $spResult['Country']['name'];?></td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Contact: </strong></td>
                                <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Phone:</strong></td>
                                <td><?php echo $spResult['User']['phone'];?></td>
                            </tr>                               
                            <tr>
                                <td class="table_label"><strong>Email: </strong></td>
                                <td><?php echo $spResult['User']['email'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">&nbsp;</th>
                </tr>
            </table>
            <div style="padding: 5px">
                <div style="border:2px solid #000; padding:10px; margin:10px">
                    <?php echo $texts;?>
                </div>
            </div>
            
            <table style="border:1px solid #C6C6C6;" cellpadding="0" cellspacing="0" border="0" class="table_new">			    			    
                <tr>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label" style="width: 50%"><strong>Inspection Date</strong></td>
                                <td style="width: 50%">
                                <?php
                                    if(@$result['inspection_date']!=""){
                                        echo  date('m/d/Y',strtotime(@$result['inspection_date']));
                                    }
                                ?>
                                </td>                                
                            </tr>				      
                            <tr>
                                <td class="table_label"><strong>Inspection Contract Number</strong></td>
                                <td>
                                    <?php
                                        if($result['inspection_contract_number']!=""){
                                            echo  date('m/d/Y',strtotime($result['inspection_contract_number']));
                                        }
                                    ?>
                                </td>                                
                            </tr>                            
                        </table>
                    </td>
                    <td valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="table_label"><strong>Inspection Time</strong></td>
                                <td><?php echo @$result['inspection_time']; ?></td>                                
                            </tr>
                            <tr>
                                <td class="table_label"><strong>Comments</strong></td>
                                <td><?php echo @$result['comments']; ?></td>
                            </tr>                            				      
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
            <h3 align="center">PORTABLE FIRE ExtinguisherReport RECORD</h3>
            <table style="border:1px solid #C6C6C6" cellpadding="0" cellspacing="0" border="0" class="table_new">               
                <tr>
                    <td colspan="13">&nbsp;</td>                    
                </tr>
                <tr>
                    <td rowspan="2"><b>#</b></td>
                    <td rowspan="2"><b>Location</b></td>
                    <td rowspan="2"><b>Type</b></td>
                    <td rowspan="2"><b>Model</b></td>
                    <td rowspan="2"><b>Born Date</b></td>
                    <td><b>Deficient</b></td>                    
                    <td colspan="2"><b>Last Maintenace</b></td>
                    <td colspan="2"><b>Last Recharge</b></td>
                    <td colspan="2"><b>Last Hydro test</b></td>
                </tr>
                <tr>
                    <td><b>Yes/No</b></td>                    
                    <td><b>Date</b></td>
                    <td><b>Performed By</b></td>
                    <td><b>Date</b></td>
                    <td><b>Performed By</b></td>
                    <td><b>Date</b></td>
                    <td><b>Performed By</b></td>
                </tr>
                <?php
                if(!empty($data) && isset($data))
                {
                        foreach($data as $key=>$val){
                            $tdColor = (($val['deficiency']=='Yes')?'red':'');
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td><?php echo $val['location']; ?></td>
                    <td><?php echo $this->Common->getServiceName($val['ext_type']); ?></td>
                    <td><?php echo $val['model']; ?></td>
                    <td><?php echo $val['born_date']; ?></td>
                    <td style="color:<?php echo $tdColor;?>"><?php echo $val['deficiency']; ?></td>                    
                    <td><?php echo $val['last_maintenace_date']; ?></td>
                    <td><?php echo $val['last_maintenace_performed_by']; ?></td>
                    <td><?php echo $val['last_recharge_date']; ?></td>
                    <td><?php echo $val['last_recharge_performed_by']; ?></td>
                    <td><?php echo $val['last_hydrotest_date']; ?></td>
                    <td><?php echo $val['last_hydrotest_performed_by']; ?></td>                    
                </tr>
                <?php
                        }
                }else{
                ?>
                <tr>
                    <td colspan="13">No Record found</td>
                </tr>
                <?php
                }    
                ?>
            </table>            
            <table cellpadding=0 cellspacing=0 class="table_new" border=0 style="border: 1px solid #C6C6C6">
                <tr>
                    <td>
                        <img src="/app/webroot/img/signature_images/<?php echo $chkSign['Signature']['signature_image'];?>">
                    </td>                    
                </tr>
                <tr>
                    <td>
                    Inspector's Signature
                    </td>
                </tr>
                <tr>
                    <td>
                    &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    Thanks & Regards
                    </td>
                </tr>
                <tr>
                    <td>
                    ReportsOnlineplus.com
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>