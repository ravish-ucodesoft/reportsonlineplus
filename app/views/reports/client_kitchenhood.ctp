<?php

    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js')); 
?>

<script type="text/javascript">
 jQuery(function() {
        jQuery(".calender").datepicker({ dateFormat: 'yy-mm-dd',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });});
</script> 
<style>
.message{text-align:center;color:#54A41A;font-weight:bold;}

/*----------- 30052012---------------------*/
.input-chngs input{ background: #ebebeb; /* Old browsers */
background: -moz-linear-gradient(top, #ebebeb 0%, #ffffff 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ebebeb), color-stop(100%,#ffffff)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* IE10+ */
background: linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebebeb', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
border:1px solid #bbb; width:66%; margin:0 30% 0 0;
}
.input-chngs td { color:#666;}

table td { border:none;}
#box .blue-hdng { background: none repeat scroll 0 0 #F4FAFF; border:none;
    border-bottom: 1px solid #AECEEF;
    color: #2281D4;
    font-size: 16px; font-weight:normal;
}
</style><?php //pr($clientResult); ?>
<div id="content" >
	      <div class='message'><?php echo $this->Session->flash();?></div>
              <div id="box">
              
		 <?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'kitchenhood','name'=>'kitchenreport','id'=>'kitchenreport')); ?>
                	<h3 class="blue-hdng" align="center" ><?php echo ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']); ?></h3>
                        <br/>
                        <table class="tbl input-chngs">
                        <tr><td><?php echo $html->link($html->image("printer-icon.png"), 'javascript:void()', array('escape' => false,'onClick'=>"window.print()"));?></td></tr>
			    <tr height="38px"><td  colspan="2"><td align="center" width="21%" style="color:#3D94DC"><b>RANGE HOOD SYSTEMS REPORT</b></td><td></td><td colspan="1"></td><td colspan="2"></td></tr>
                        <tr>           
                        </td><td width="21%"><b>Date of Service:</b></br><?php echo $kitchenData['KitchenhoodReport']['service_date']; ?></td>
                       	<td width="20%"><b>Time:</b><?php echo $kitchenData['KitchenhoodReport']['totaltime'].' '.$kitchenData['KitchenhoodReport']['totaltime_format']; ?></td><td  colspan="2">
                        </tr>   
			    <tr>
				<td width="22%"><b>Service Provider Info.</b></td>
				<td><?php echo ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']); ?>			    </td><td width="21%"><b>Location of System Cylinders:</b><?php echo $kitchenData['KitchenhoodReport']['locationsystem']; ?></td><td colspan="3"></td></tr><tr><td>&nbsp;</td></tr>
                        <tr><td width="20%"><b>Customer:</b></br><?php echo ucWords($clientResult['User']['fname'].' '.$clientResult['User']['lname']); ?></td>
                          <td width="20%"><b>Manufacturer:</b></br><?php echo $kitchenData['KitchenhoodReport']['manufacturer']; ?></td><td width="20%"><b>Model:</b></br><?php echo $kitchenData['KitchenhoodReport']['model']; ?></td><td width="20%"><b>Wet:</b></br><?php echo $kitchenData['KitchenhoodReport']['wet']; ?></td><td width="20%"><b>Dry Chemical:</b></br><?php echo $kitchenData['KitchenhoodReport']['chemical']; ?></td><td colspan="2"></td></tr> 
                          <tr><td>&nbsp;</td></tr>  
                        
                        <tr><td><b>Address:</b></br><?php echo $clientResult['User']['address']; ?></td>
                       <td width="20%"><b>Cylinder Size (Master):</b></br><?php echo $kitchenData['KitchenhoodReport']['mastersize']; ?></td><td width="20%"><b>Cylinder Size (Slave):</b></br><?php echo $kitchenData['KitchenhoodReport']['slavesize']; ?></td><td width="20%"><b>Cylinder Size (Slave):</b></br><?php echo $kitchenData['KitchenhoodReport']['slavesize1']; ?></td><td colspan="1"></td>
		                    </tr><tr><td>&nbsp;</td></tr>                         
                        <tr><td width="20%">Location:
                       	<?php echo $clientResult['User']['site_address']; ?>            
                       <td width="20%"><b>Fuse Links 360 F:</b></br><?php echo $kitchenData['KitchenhoodReport']['fuselink1']; ?></td><td width="20%"><b>Fuse Links 450 F:</b></br><?php echo $kitchenData['KitchenhoodReport']['fuselink2']; ?></td><td width="20%"><b>Fuse Links 500 F:</b></br><?php echo $kitchenData['KitchenhoodReport']['fuselink3']; ?></td><td width="20%"><b>Other:</b></br><?php echo $kitchenData['KitchenhoodReport']['fuselink_other']; ?></td><td colspan="2"></td>
		        </td>
                        </tr><tr><td>&nbsp;</td></tr>
			<tr><td width="20%"><b>Attention:</b></br>
                       	<?php echo $kitchenData['KitchenhoodReport']['attention']; ?>            
                        <td width="20%"><b>Fuel Shut Off:</b></br><?php echo $kitchenData['KitchenhoodReport']['shutoff']; ?></td><td width="20%"><b>Electric:</b></br><?php echo $kitchenData['KitchenhoodReport']['electric']; ?></td><td width="20%"><b>Gas:</b></br><?php echo $kitchenData['KitchenhoodReport']['gas']; ?></td><td width="20%"><b>Size:</b></br><?php echo $kitchenData['KitchenhoodReport']['size']; ?></td><td colspan="2"></td>
			</td>
                        </tr> <tr><td>&nbsp;</td></tr>  
                        
                        <tr><td><b>Phone:</b></br>
                        <?php echo "757685464"; ?></td>
                        <td width="20%"><b>Serial Number:</b></br><?php echo $kitchenData['KitchenhoodReport']['serialnumber']; ?></td><td width="20%"><b>Last Hydro Test Date:</b></br><?php echo $kitchenData['KitchenhoodReport']['hydro_date']; ?></td><td width="20%"><b>Last Recharge Date:</b></br><?php echo $kitchenData['KitchenhoodReport']['recharge_date']; ?></td><td colspan="3"></td>
		        </tr>  <tr><td>&nbsp;</td></tr>                       
			
                                        
                     <tr><td colspan="5"><b><i>COOKING APPLIANCE SIZES (NoTE: List appliances from left to right and indicate Nozzles used for each)</i></b></td><td colspan="2"></td>
			 <tr><td colspan="5"><b><i>(Please Note the Plenum and Duct Size(s) in appropriate boxes below)</i></b></td><td colspan="3"></td>
                    
		     <tr><td><b>Plenum Size(s):</b></td>
                        <td><?php echo $kitchenData['KitchenhoodReport']['plenumsize1']; ?></td>
                        <td width="20%"><?php echo $kitchenData['KitchenhoodReport']['plenumsize2']; ?></td><td width="20%"><?php echo $kitchenData['KitchenhoodReport']['plenumsize3']; ?></td></td><td colspan="2"></td>
		        </tr> <tr><td>&nbsp;</td></tr> 
		    <tr><td><b>Duct Size(s):</b></td>
                        <td><?php echo $kitchenData['KitchenhoodReport']['ductsize1']; ?></td>
                        <td width="20%"><?php echo $kitchenData['KitchenhoodReport']['ductsize2']; ?></td><td width="20%"><?php echo $kitchenData['KitchenhoodReport']['ductsize3']; ?></td></td><td colspan="2"></td>
		        </tr>
		        <tr><td>&nbsp;</td></tr>
		    
		    <tr><td>1:All appliances properly covered w/ correct Nozzles</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_1']; ?><td>19. Check travel of able nuts/S-hooks</td>
			<td colspan="2" style="padding-left:50px"<?php echo $kitchenData['KitchenhoodReport']['question_19']; ?></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
		     <tr><td>2:Duct and plenum covered w/ correct Nozzles</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_2']; ?></td>
			<td >20. Piping and conduit securely bracketed</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_20']; ?></td>
		     </tr>
		     <tr><td>&nbsp;</td></tr>
		     <tr><td>3:Check positioning of all Nozzles</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_3']; ?></td>
			
		     <td >21:Proper separation between fryers & flame</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_21']; ?></td>
		     </tr>
		     <tr><td>&nbsp;</td></tr>
		     <tr><td >4:System installed in accordance w/ Mfg UL listing</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_4']; ?></td>
			<td >22:Proper clearance-flame to filters</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_22']; ?></td>
			</tr><tr><td>&nbsp;</td></tr>
		     <tr><td >5:Hood/duct penetrations sealed w/weld or UL device</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_5']; ?></td>
			
		     <td >23:Exhaust fan operating properly</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_23']; ?></td>
		     </tr><tr><td>&nbsp;</td></tr>
		     <tr><td >6: Check if seals intact, evidence of tampering</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_6']; ?></td>
			<td >24: All filters reinstalled</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_24']; ?></td>
		     
		     </tr><tr><td>&nbsp;</td></tr>
		     <tr><td >7: If system has been discharged, report same</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_7']; ?></td>
			<td >25: Fuel shut-off in ON position</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_25']; ?></td>
		     </tr><tr><td>&nbsp;</td></tr>
		     <tr><td >8: Pressure gauge in proper range (if gauged)</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_8']; ?></td>
			<td >26:Manual & Remove set/seals in place</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_26']; ?></td>
		     </tr><tr><td>&nbsp;</td></tr>
		     
		     <tr><td >9: Check cartridge weight (Replace, if needed)</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_9']; ?></td>
			<td >27:Reinstall systems covers</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_27']; ?></td>
			</tr><tr><td>&nbsp;</td></tr>
		     <tr><td>10:Hydrostatic/6 year maintenance date</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_10']; ?></td>
			<td >28:System operational and seals in place</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_28']; ?></td>
		     </tr><tr><td>&nbsp;</td></tr>
		     <tr><td >11: Inspect cylinder and mount</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_11']; ?></td>
			<td >29:Slave system operational</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_29']; ?></td>
		     </tr><tr><td>&nbsp;</td></tr>
		     <tr><td >12: Operate system from terminal link</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_12']; ?></td>
			<td >30:Clean cylinder and mount</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_30']; ?></td>
		     </tr><tr><td>&nbsp;</td></tr>
		      <tr><td >13:Test for proper operation from remote</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_13']; ?></td>
			<td >31:Fan warning sign on hood</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_31']; ?></td>
			</tr><tr><td>&nbsp;</td></tr>
		     <tr><td >14:  Check operation of micro switch</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_14']; ?></td>
			<td >32:  Personnel instructed in manual operation of system</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_32']; ?></td>
			</tr><tr><td>&nbsp;</td></tr>
		     <tr><td >15:Check operation of gas valve</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_15']; ?></td>
			
			<td >33:Proper hand portable extinguishers(K Class and ABC)</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_33']; ?></td>
			</tr><tr><td>&nbsp;</td></tr>
		     <tr><td >16:Proper Nozzle covers in place/clean Nozzles</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_16']; ?></td>
			<td >34:Portable extinguishers properly serviced</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_34']; ?></td>
			</tr><tr><td>&nbsp;</td></tr>
		     <tr><td >17:Check fuse links and clean</td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_17']; ?></td>
			<td >35:Service & certification tag on system</td>
			<td colspan="2" style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_35']; ?></td>
			</tr><tr><td>&nbsp;</td></tr>
			<tr><td>18: Replaced fuse links (Record date here) </td>
			<td style="padding-left:50px"><?php echo $kitchenData['KitchenhoodReport']['question_18']; ?></td>
			</tr><tr><td>&nbsp;</td></tr>
			<tr><td > <b>NoTE DISCREPANCIES OR DEFICIENCIES:</b></td>
                        <td colspan="2"><?php echo $kitchenData['KitchenhoodReport']['discrepancies']; ?></td><td colspan="3"></td>
		        </tr>  
			<tr><td width="20%"><b>Service Technician:</b>
                       	<?php echo $kitchenData['KitchenhoodReport']['service_technician']; ?>             
                        </td><td width="20%"><b>Date:</b><?php echo $kitchenData['KitchenhoodReport']['k_date']; ?></td><td width="20%"><b>Time:</b><?php echo $kitchenData['KitchenhoodReport']['k_time'].' '.$kitchenData['KitchenhoodReport']['k_timeformat']; ?></td><td width="20%"><b>Customer Authorized Agent:</b><?php echo $kitchenData['KitchenhoodReport']['authorizedagent']; ?></td>
                        </tr>
			<tr><td></td><td></td><td>     
			<div align="left" style="text-align:center;">
			<?php //echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
			</div>
			</td>
			</tr>
			</table>
                    	<?php echo $form->end(); ?>
                </div>
</div> 