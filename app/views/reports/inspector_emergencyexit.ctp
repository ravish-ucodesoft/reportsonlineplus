<?php 
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    echo $this->Html->css('reports');
    echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox'));
?>
<style>
.summary-tbl2 p {
    float: left;
    margin-bottom: 0;
    margin-left: 5px;
    width: 10%;
    font-size: 10px;
    padding-left: 7px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
.lbtxt{text-align:right;width:20%}
.input-chngs1 input {
    margin-right: 3px;
    width: 136px;
}

table td label{
    float: none;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	jQuery(".ajax").colorbox();
	jQuery(".inline").colorbox({inline:true, width:"50%"});
	loadDatePicker();
	jQuery("#EmergencyexitReportId").validationEngine();
	<?php
	   if(isset($result['EmergencyexitReport']['finish_date']) && ($result['EmergencyexitReport']['finish_date'] != "")){ ?>
	   
	    for(i = 0; i < document.getElementById('EmergencyexitReportId').length; i++){
		var formElement = document.getElementById('EmergencyexitReportId').elements[i];	
		    formElement.disabled = true;
	    }
	    
           <?php } ?>      

});
 function limit(field, chars){	
        if (document.getElementById(field).value.length > chars){
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
 function changevalExit(obj){
    var passExit = 0;
    var failExit = 0;
    $(".dropdownexit").each(function(){
	switch(this.value){
	    case "Pass":
		    passExit = passExit+1;
		    break;
	    case "Fail":
		    failExit = failExit+1;
		    break;
	}
    });
	
    $("#EmergencyexitReportSurveyExitPass").val(passExit);
    $("#EmergencyexitReportSurveyExitFail").val(failExit);
    $("#EmergencyexitReportTotalExitPass").val(passExit);
    $("#EmergencyexitReportTotalExitFail").val(failExit);
    chkStatus(obj);
 }
  function changevalEmergency(obj){
	var passEmergency = 0;
	var failEmergency = 0;
	$(".dropdownemergency").each(function(){
	  switch(this.value){
		case "Pass":
			passEmergency = passEmergency+1;
			break;
		case "Fail":
			failEmergency = failEmergency+1;
			break;
	  }
      });
	
	$("#EmergencyexitReportSurveyEmergencyPass").val(passEmergency);
	$("#EmergencyexitReportSurveyEmergencyFail").val(failEmergency);
	$("#EmergencyexitReportTotalEmergencyPass").val(passEmergency);
	$("#EmergencyexitReportTotalEmergencyFail").val(failEmergency);
	chkStatus(obj);
  }

function loadDatePicker(){
        jQuery(".calender").datepicker({dateFormat: 'mm/dd/yy',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });
}

function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
	    }
            });           
}        
window.onload = createUploader;
function AppendExit(divid){
    var counter = jQuery('#optioncountExit').val();
    counter = parseInt(counter) + 1;
    jQuery('#optioncountExit').val(counter);
    OptionHtml ='<div id="Newdiv_'+counter+'" style="float: left; width: 100%; padding-bottom:10px;">';
    OptionHtml = OptionHtml+'<section style="width:54%">';
    OptionHtml = OptionHtml+'<p style="width:5%">&nbsp;</p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="location_'+counter+'" class="validate[required]" name="data[EmergencyexitReportsRecord]['+counter+'][location]" id=""></span></p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="mfd_'+counter+'" class="calender validate[required]" name="data[EmergencyexitReportsRecord]['+counter+'][mfd_date]" id="" readonly="readonly" style="width:78%"></span></p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><select id="status_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][type]"  class="dropdownexit wdth-250 validate[required]" style="width:78%"><option value="" selected="selected">Select</option>';
    <?php if(sizeof($option1)>0){?>
    <?php foreach($option1 as $key=>$opt){ ?>
    OptionHtml = OptionHtml+'<option value="<?php echo $key; ?>"><?php echo $opt; ?></option>';
    <?php }} ?>
    OptionHtml = OptionHtml+ '</select></p>';
    OptionHtml = OptionHtml+'<p style="width:20%"><select id="status_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][status]"  class="dropdownexit wdth-250 validate[required]" style="width:78%" onchange="changevalExit(this)"><option value="" selected="selected">Select</option><option value="Pass">PASS</option><option value="Fail">FAIL</option></select></p>';    
    OptionHtml = OptionHtml+'</section>';
    OptionHtml = OptionHtml+'<section style="width:46%; float:left;">';
    OptionHtml = OptionHtml+'<p style="width:100%;"><span class="input-field2"><input type="text" id="comment_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][comment]" style="width:70%" class="validate[required]" value=""><a href="javascript:void(0)" id="'+counter+'" onclick="RemoveExit(this.id,this);"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></span></p>';
    OptionHtml = OptionHtml+'</section><br/>';    
    OptionHtml = OptionHtml+'</div>';
    jQuery('#Exitoption').append(OptionHtml);
    loadDatePicker();
    var incrementExit = $("#EmergencyexitReportTotalExit").val();
    incrementExit = parseInt(incrementExit)+1;
    $("#EmergencyexitReportTotalExit").val(incrementExit);
    var diffTotal = incrementExit - parseInt($("#EmergencyexitReportSurveyExit").val());
    $("#EmergencyexitReportDiffExit").val(diffTotal);
}

function RemoveExit(id,obj){
	rowId = "#Newdiv_"+id;	
	if($("#status_"+id).val() == "Pass"){
		var ExitPass = $("#EmergencyexitReportSurveyExitPass").val();
		ExitPass = parseInt(ExitPass)-1;
		$("#EmergencyexitReportSurveyExitPass").val(ExitPass);
		
		var ExitPassTotal = $("#EmergencyexitReportTotalExitPass").val();
		ExitPassTotal = parseInt(ExitPassTotal)-1;
		$("#EmergencyexitReportTotalExitPass").val(ExitPassTotal);
		
		var valTotalExit = $("#EmergencyexitReportTotalExit").val();
		    valTotalExit = parseInt(valTotalExit)-1;
		 $("#EmergencyexitReportTotalExit").val(valTotalExit);
		 
		var diff =  (parseInt($("#EmergencyexitReportTotalExit").val()))-(parseInt($("#EmergencyexitSurveyExit").val()))
		 $("#EmergencyexitReportDiffExit").val(diff);
		 
		 var valTotalExitHidden = $("#optioncountExit").val();
		valTotalExitHidden = parseInt(valTotalExitHidden)-1;
		$("#optioncountExit").val(valTotalExitHidden); 
		
	}
	if($("#status_"+id).val() == "Fail"){
		var ExitFail = $("#EmergencyexitReportSurveyExitFail").val();
		ExitFail = parseInt(ExitFail)-1;
		$("#EmergencyexitReportSurveyExitFail").val(ExitFail);
		
		var ExitFailTotal = $("#EmergencyexitReportTotalExitFail").val();
		ExitFailTotal = parseInt(ExitFailTotal)-1;
		$("#EmergencyexitReportTotalExitFail").val(ExitFailTotal);
		
		var valTotalExit = $("#EmergencyexitReportTotalExit").val();
		valTotalExit = parseInt(valTotalExit)-1;
		 $("#EmergencyexitReportTotalExit").val(valTotalExit);
		 
		 var diff =  (parseInt($("#EmergencyexitReportTotalExit").val()))-(parseInt($("#EmergencyexitSurveyExit").val()))
		 $("#EmergencyexitReportDiffExit").val(diff);
		 
		 var valTotalExitHidden = $("#optioncountExit").val();
		 valTotalExitHidden = parseInt(valTotalExitHidden)-1;
		 $("#optioncountExit").val(valTotalExitHidden);
		
	}
	if($("#status_"+id).val() == ""){
		var valTotalExit = $("#EmergencyexitReportTotalExit").val();
		    valTotalExit = parseInt(valTotalExit)-1;
		 $("#EmergencyexitReportTotalExit").val(valTotalExit);
		
		var valTotalExitHidden = $("#optioncountExit").val();
		valTotalExitHidden = parseInt(valTotalExitHidden)-1;
		$("#optioncountExit").val(valTotalExitHidden);
		
		var diff =  (parseInt($("#EmergencyexitReportTotalExit").val()))-(parseInt($("#EmergencyexitSurveyExit").val()))
		 $("#EmergencyexitReportDiffExit").val(diff);
	 }
        RemoveRow(rowId,obj);
}

function AppendEmergency(divid){
    var counter = jQuery('#optioncountEmergency').val();
    counter = parseInt(counter) + 1;
    jQuery('#optioncountEmergency').val(counter);
    OptionHtml ='<div id="NewdivEmergency_'+counter+'" style="float: left; width: 100%; padding-bottom:10px;">';
    OptionHtml = OptionHtml+'<section style="width:54%">';
    OptionHtml = OptionHtml+'<p style="width:5%">&nbsp;</p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="location_'+counter+'" class="validate[required]" name="data[EmergencyexitReportsRecord]['+counter+'][location]" id=""></span></p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="mfd_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][mfd_date]" id="" readonly="readonly" class="calender validate[required]" style="width:78%"></span></p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><select id="status_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][type]"  class="dropdownexit wdth-250 validate[required]" style="width:78%"><option value="" selected="selected">Select</option>';
    <?php foreach($option2 as $key=>$opt){ ?>
    OptionHtml = OptionHtml+'<option value="<?php echo $key; ?>"><?php echo $opt; ?></option>';
    <?php } ?>
    OptionHtml = OptionHtml+ '</select></p>';
    OptionHtml = OptionHtml+'<p style="width:20%"><select id="status_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][status]" class="dropdownemergency wdth-250 validate[required]" style="width:78%" onchange="changevalEmergency(this);"><option value="" selected="selected">Select</option><option value="Pass">PASS</option><option value="Fail">FAIL</option></select></p>';    
    OptionHtml = OptionHtml+'</section>';
    OptionHtml = OptionHtml+'<section style="width:46%; float:left;">';
    OptionHtml = OptionHtml+'<p style="width:100%;"><span class="input-field2"><input type="text" id="comment_'+counter+'" class="validate[required]" name="data[EmergencyexitReportsRecord]['+counter+'][comment]" style="width:70%" id="" value=""><a href="javascript:void(0)" id="'+counter+'" onclick="RemoveEmergency(this.id,this);"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></span></p>';
    OptionHtml = OptionHtml+'</section><br/>';    
    OptionHtml = OptionHtml+'</div>';
    jQuery('#Emergencyoption').append(OptionHtml);
    var incrementEmergency = $("#EmergencyexitReportTotalEmergency").val();
    incrementEmergency = parseInt(incrementEmergency)+1;
    $("#EmergencyexitReportTotalEmergency").val(incrementEmergency);
    
     var diffTotal = incrementEmergency - parseInt($("#EmergencyexitReportSurveyEmergency").val());
    $("#EmergencyexitReportDiffEmergency").val(diffTotal);
    
    loadDatePicker();
}
function RemoveEmergency(id,obj){
	rowId = "#NewdivEmergency_"+id;	
	if($("#status_"+id).val() == "Pass")
	{
		var EmergencyPass = $("#EmergencyexitReportSurveyEmergencyPass").val();
		EmergencyPass = parseInt(EmergencyPass)-1;
		$("#EmergencyexitReportSurveyEmergencyPass").val(EmergencyPass);
		
		var EmergencyPassTotal = $("#EmergencyexitReportTotalEmergencyPass").val();
		EmergencyPassTotal = parseInt(EmergencyPassTotal)-1;
		$("#EmergencyexitReportTotalEmergencyPass").val(EmergencyPassTotal);
		
		var valTotalEmergency = $("#EmergencyexitReportTotalEmergency").val();
		    valTotalEmergency = parseInt(valTotalEmergency)-1;
		    $("#EmergencyexitReportTotalEmergency").val(valTotalEmergency);
		 
		var valTotalEmergencyHidden = $("#optioncountEmergency").val();
		    valTotalEmergencyHidden = parseInt(valTotalEmergencyHidden)-1;
		    $("#optioncountEmergency").val(valTotalEmergencyHidden);
		    
		    
		var diff =  (parseInt($("#EmergencyexitReportTotalEmergency").val()))-(parseInt($("#EmergencyexitSurveyEmergency").val()))
		 $("#EmergencyexitReportDiffEmergency").val(diff);
		 
	}
	if($("#status_"+id).val() == "Fail")
	{
		var EmergencyFail = $("#EmergencyexitReportSurveyEmergencyFail").val();
		EmergencyFail = parseInt(EmergencyFail)-1;
		$("#EmergencyexitReportSurveyEmergencyFail").val(EmergencyFail);
		
		var EmergencyFailTotal = $("#EmergencyexitReportTotalEmergencyFail").val();
		EmergencyFailTotal = parseInt(EmergencyFailTotal)-1;
		$("#EmergencyexitReportTotalEmergencyFail").val(EmergencyFailTotal);
		
		var valTotalEmergency = $("#EmergencyexitReportTotalEmergency").val();
		valTotalEmergency = parseInt(valTotalEmergency)-1;
		$("#EmergencyexitReportTotalEmergency").val(valTotalEmergency);
		
		var valTotalEmergencyHidden = $("#optioncountEmergency").val();
		valTotalEmergencyHidden = parseInt(valTotalEmergencyHidden)-1;
		$("#optioncountEmergency").val(valTotalEmergencyHidden);
		
		var diff =  (parseInt($("#EmergencyexitReportTotalEmergency").val()))-(parseInt($("#EmergencyexitSurveyEmergency").val()))
		 $("#EmergencyexitReportDiffEmergency").val(diff);
	}
	 if($("#status_"+id).val() == "")
	 {
		var valTotalEmergency = $("#EmergencyexitReportTotalEmergency").val();
		valTotalEmergency = parseInt(valTotalEmergency)-1;
		$("#EmergencyexitReportTotalEmergency").val(valTotalEmergency);
		
		var valTotalEmergencyHidden = $("#optioncountEmergency").val();
		valTotalEmergencyHidden = parseInt(valTotalEmergencyHidden)-1;
		$("#optioncountEmergency").val(valTotalEmergencyHidden);
		
		var diff =  (parseInt($("#EmergencyexitReportTotalEmergency").val()))-(parseInt($("#EmergencyexitSurveyEmergency").val()))
		 $("#EmergencyexitReportDiffEmergency").val(diff);
	 }
    //jQuery('#NewdivEmergency_'+id).remove();
    RemoveRow(rowId,obj);
}

 function checkUploadedAttachment(){
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
  function checkNotification(){
	var notifyVal = jQuery("#ExtinguisherReportNotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }

function RemoveAttachment(attachId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmattach(attachId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId);    
  }
  else{
    return false;
  }
}

function RemoveNotifier(notifierId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delnotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#pnotify_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId);    
  }
  else{
    return false;
  }
 }
 function RemoveRecord(rId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delexitrecord/" + rId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#LI_'+rId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmbox(rId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveRecord(rId);    
  }
  else{
    return false;
  }
 }
 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

/*
 Function changeStatus
 To fill the deficiency of report
 created by manish kumar on Jan 11, 2013 
*/
function chkStatus(obj){
	response = jQuery(obj).val();
	currentObj = jQuery(obj);
	selectBox  = jQuery(obj).parent('p').parent('section').find('select:first');
	/*find row id*/
	topParentId = jQuery(obj).parent('p').parent('section').parent('div').attr('id');
	rowId = topParentId.slice(-1);	
	/*find row id*/
	mfdDate = selectBox.parent('p').prev('p').find('input').val();
	modelData = '';
	locationData = selectBox.parent('p').parent('section').find('input:first').val();
	serviceId = selectBox.val();
	reportId = '<?php echo $_REQUEST['reportID'];?>';
	clientId = '<?php echo $_REQUEST['clientID'];?>';
	spId = '<?php echo $_REQUEST['spID'];?>';
	servicecallID = '<?php echo $_REQUEST['serviceCallID'];?>';
	if(response=='Pass'){		
		if(!confirm("It will delete the added deficiency. Do you want to continue?")){
			jQuery(obj).val("Fail");
			return false;
		}else{
			data = 'reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId;
			jQuery.ajax({
				'url':'/inspector/reports/deleteDeficiency/',
				'data':data,
				'type':'post',
				'success':function(msg){
					currentObj.css('color','#666666');
					return false;
				}
			});
		}
		return false;
	}
	if(serviceId=="" || locationData=="" || mfdDate==""){
		alert('Please fill location, type and mfd date first');
		jQuery(obj).val("Pass");
		return false;
	}
	currentObj.css('color','red');
	PopupCenter('/inspector/reports/fillDeficiency?reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId+'&locationData='+locationData+'&modelData='+modelData+'&mfdDate='+mfdDate,'','600','400');
}
/*
 Function RemoveRow
 To remove the added row
 created by manish kumar on Jan 11, 2013 
*/
function RemoveRow(divid,obj){    
    selectBox  = jQuery(obj).parent('span').parent('p').parent('section').prev('section').find('select:first');
    deficienVal = selectBox.parent('p').next('p').children('select').val();
    /*find row id*/
    topParentId = selectBox.parent('p').parent('section').parent('div').attr('id');
    rowId = topParentId.slice(-1);	
    /*find row id*/    
    serviceId = selectBox.val();
    if(deficienVal==''){
	jQuery(divid).remove();
	return false;
    }
    if(deficienVal=='Pass'){
	jQuery(divid).remove();
	return false;
    }
    if(deficienVal=='Fail'){
	if(!confirm("It will also delete the added deficiency. Do you want to continue?")){	
		return false;
	}	
    }
    reportId = '<?php echo $_REQUEST['reportID'];?>';
    clientId = '<?php echo $_REQUEST['clientID'];?>';
    spId = '<?php echo $_REQUEST['spID'];?>';
    servicecallID = '<?php echo $_REQUEST['serviceCallID'];?>';
    
    data = 'reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId;
    jQuery.ajax({
	'url':'/inspector/reports/deleteDeficiency/',
	'data':data,
	'type':'post',
	'success':function(msg){
		if(msg == 'Success'){
			jQuery(divid).remove();
		}else{
			alert('Problem in deleting the record. Please try again');
		}
	}
    });    
}
</script>

<div id="content">
    <div class='message'><?php echo $this->Session->flash();?></div>
    <div id="box">
	<div style="width: 100%">
	    <?php //echo $html->link($html->image("pdf.png",array('title'=>'Print Preview')), 'reportPreview?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
	    <?php echo $html->link($html->image("fineimages/pdf_preview.png",array('title'=>'Print Preview')), 'emergencyReportPreview?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
	</div>
        <h3 align="center" >Emergency Light / Exit Sign Report</h3>
        <h4 class="table_title">Agreement and Report of Inspection</h4>
	<?php
		$actionUrl = 'emergencyexit?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'];
	?>
	<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>$actionUrl,'name'=>'EmergencyexitReport','id'=>'EmergencyexitReportId')); ?>
	<?php echo $form->input('EmergencyexitReport.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
	<?php echo $form->input('EmergencyexitReport.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
	<?php echo $form->input('EmergencyexitReport.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
	<?php echo $form->input('EmergencyexitReport.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
	<?php echo $form->input('EmergencyexitReport.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
	<?php echo $form->input('EmergencyexitReport.id',array('type'=>'hidden','value'=>@$result['EmergencyexitReport']['id'])); ?>
	<table cellpadding="0" cellspacing="0" border="0" class="table_new">
	    <tr>
		<td>
		    <b>Days Left for report Submission: <?php echo $daysRemaining;?></b>
		</td>
                <!--<td align="right">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
			    <td width="100%" align="right">
				<?php
				    $options=array("monthly"=>"Monthly","Quarterly"=>"Quarterly","semi_annual"=>"Semi-Annual","Annual"=>"Annual");
				    $attributes=array('legend'=>false,'separator'=>'&nbsp;&nbsp;','class'=>'validate[required]');
				    //echo $this->Form->radio('EmergencyexitReport.inspection_type',$options,$attributes);
				?>				    
			    </td>                                
                        <tr>
                    </table>
                </td>-->
            </tr>
            <tr>
	        <th colspan="2" align="left">Client information</th>
	    </tr>
            <tr>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="table_label"><strong>Building Information</strong></td>
                            <td><?php echo $clientResult['User']['client_company_name'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $clientResult['User']['address'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>City/State/Zip:</strong></td>
                            <td> <?php echo $clientResult['User']['city'].', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Country: </strong></td>
                            <td><?php echo $clientResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="table_label"><strong>Contact: </strong></td>
                            <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Phone:</strong></td>
                            <td><?php echo $clientResult['User']['phone'];?></td>
                        </tr>                               
                        <tr>
                            <td class="table_label"><strong>Email: </strong></td>
                            <td> <?php echo $clientResult['User']['email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
	    <tr>
		<th colspan="2" align="left">Site Information</th>
	    </tr>
            <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Site Name:</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <td> <?php echo $siteaddrResult['SpSiteAddress']['site_city'].', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Country: </strong></td>
                            <td><?php echo $siteaddrResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Contact: </strong></td>
                            <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                        </tr>                               
                        <tr>
			    <td class="table_label"><strong>Email: </strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2" align="left">Service Provider Info</th></tr>
            <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Name:</strong></td>
                            <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $spResult['User']['address'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <td><?php echo $spResult['User']['city'].', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Country: </strong></td>
			    <td><?php echo $spResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Contact: </strong></td>
                            <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
			    <td><?php echo $spResult['User']['phone'];?></td>
                        </tr>                               
                        <tr>
			    <td class="table_label"><strong>Email: </strong></td>
			    <td><?php echo $spResult['User']['email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div style="border:4px solid #000; padding:10px; margin:10px">
	    <?php echo $texts;?>
        </div>
	<div style="float:right;padding-right:10px;">
	    <?php echo $html->link('Add Explanation',array('controller'=>'inspectors','action'=>'addExplanation?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID']),array('class'=>'ajax'));?>
	</div>    
	<!--<div style="float:right;padding-right:10px;">
	    <?php
		//$permissionfill=$this->Common->checkPermission(1,$session_inspector_id);    
		//if($session_inspector_id!=$schedule['lead_inspector_id'])
		//if($permissionfill!=1)
		{
		?>
		    <a href="javascript:void(0)" title="Add Attachment"  class="linktoreport" onclick="alert('<?php //echo $permissionfill;?>')">Add Attachment</a><span style='vertical-align:bottom'><?php //echo $html->image('icon_attachment.gif');?></span>
			
		<?php //} else {								
		?>
		<?php //echo $html->link('Add Attachment','#inline_content_attachment',array('class' => 'inline'));?><?php //echo $html->image('icon_attachment.gif');?>&nbsp;&nbsp;&nbsp;
	    <?php
	    } 
	    ?>
	   <?php
		//$permissionfillnotify=$this->Common->checkPermission(2,$session_inspector_id);    
		//if($session_inspector_id!=$schedule['lead_inspector_id'])
		//if($permissionfillnotify!=1)
		{
		?>
		    <a href="javascript:void(0)" title="Add Notification"  class="linktoreport" onclick="alert('<?php //echo $permissionfillnotify;?>')">Add Notification</a><span style='vertical-align:bottom'><?php //echo $html->image('icon_notify.gif');?></span>
			
		<?php //} else {								
		?>
		<?php //echo $html->link('Add Notifier',"#inline_content_notification",array('class' => 'inline'));?><?php //echo $html->image('icon_notify.gif');?>
	    <?php
	    } 
	 ?> 
	</div>-->
	<!--<h3 style="margin-top:10px;" align="center" ><?php //echo ucwords($spResult['User']['fname'].' '.$spResult['User']['lname']);?></h3></br>-->
   <?php echo $this->element('reports/emergency_exit_form'); ?>
    
    <!--<tr><td align="center">
				<?php
	//$permissionfilldef=$this->Common->checkPermission(6,$session_inspector_id);    
	//if($session_inspector_id!=$schedule['lead_inspector_id'])
	//if($permissionfilldef!=1)
	{
	?>
	    <a href="javascript:void(0)" title="Add Deficiency"  class="linktoreport" onclick="alert('<?php //echo $permissionfilldef;?>')"><span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span></a>
		
	<?php //} else {								
	?>
	<?php //echo $html->link('<span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span>',array('controller'=>'reports','action'=>'defAndRec?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),array('class' => 'ajax','escape'=>false),false); ?> 
    <?php
    } 
    ?>
	
    </td></tr>-->
    <tr>      
      <td>
	<?php if(isset($result['finish_date']) && ($result['finish_date'] != "")){
		echo $html->link('<span>FINISH</span>','javascript:void(0);',array('escape'=>false,'class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'emergencyexitView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');"));
		} else { ?>
		<span class="blue-btn" style="float:right;">
			<input type="submit" name="finish" value="FINISH" id="linkFinish" onclick="return confirm('Are you sure that you want to finish this report? after clicking ok you will not be able to further edit this report.')" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/>
		</span>		
	<?php
		}
		echo $html->link('<span>REVIEW</span>','javascript:void(0);',array('escape'=>false,'id'=>'reviewanswer','class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'emergencyexitView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');")); ?>
		<span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" name="save" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></td>
    </tr>
    <?php  if(isset($this->data['EmergencyexitReport']['finish_date']) && ($this->data['EmergencyexitReport']['finish_date'] != "")){ ?>
				<tr><td align="center"><font color="red" size="3px">The form is disabled, as you have finished the report. </font></td></tr>
				<?php } ?>
  </table>  
<?php echo $form->end(); ?>
    </div>
    <!-- This contains the hidden content for inline calls -->
	<div style='display:none'>		
		<div id='inline_content_attachment' style='padding:10px; background:#fff;'>
		<?php echo $form->create('', array('type'=>'POST', 'action'=>'attachment','name'=>'attachment','id'=>'attachment',"onsubmit"=>"return checkUploadedAttachment();")); ?>
		<?php echo $form->input('Attachment.attach_file', array('id'=>'uploadattachment','type'=>'hidden','value'=>'')); ?>
		<?php echo $form->input('Attachment.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
		<?php echo $form->input('Attachment.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Attachment.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Attachment.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Attachment.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		<p><strong>Upload Attachment</strong></p>
		<p>
		    <div id="demo"></div>
		    <ul id="separate-list"></ul>
		</p>
		<p><div id="uploaded_picture" style="text-align:center;padding-left:90px;"></div></p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($attachmentdata)){?>
			<p><strong>Attachments</strong></p>
		<?php } ?>		
		<?php foreach($attachmentdata as $attachdata){ ?>
			<p id="p_<?php echo $attachdata['Attachment']['id'] ?>"><?php echo $html->link($attachdata['Attachment']['attach_file'],array("controller"=>"reports","action"=>"download",$attachdata['Attachment']['attach_file']),array()).'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmattach('<?php echo $attachdata['Attachment']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>		
		</div>
		
		<?php echo $form->end(); ?>
	</div>
<!-- This contains the hidden content for inline calls -->
	<div style='display:none'>		
		<div id='inline_content_notification' style='padding:10px; background:#fff;'>
		<?php echo $form->create('', array('type'=>'POST', 'action'=>'notification','name'=>'notification','id'=>'notification',"onsubmit"=>"return checkNotification();")); ?>
		<?php echo $form->input('Notification.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
		<?php echo $form->input('Notification.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Notification.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Notification.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Notification.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		<p><strong>Notification</strong></p>
		<p>
		    <?php echo $form->input('Notification.notification',array('type'=>'textarea','rows'=>5,'cols'=>40,'label'=>false,'id'=>'ContentMessage','onKeyUp' =>"return limit('ContentMessage',500);")); ?>
		    
		</p>
		<p>
                    <div id="essay_Error" class='error'></div>
			<div>
			<small>&nbsp;<label id='limitCounter'>0</label>&nbsp;characters entered&nbsp;|&nbsp;<label id='limitCounterLeft'>500</label>&nbsp;characters remaining</small>
			</div>   
		</p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($notifierdata)){?>
			<p><strong>Notifiers</strong></p>
		<?php } ?>		
		<?php foreach($notifierdata as $notifydata){ ?>
			<p id="pnotify_<?php echo $notifydata['Notification']['id'] ?>"><?php echo $notifydata['Notification']['notification'].'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmnotifier('<?php echo $notifydata['Notification']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>
		</div>
		
		<?php echo $form->end(); ?>
	</div>
</div>