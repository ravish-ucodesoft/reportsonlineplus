<?php
  $dashboard=$schedule=$chapass=$emp='';$msg='';$holiday='';$trade='';$employee='';$onnows='';$reports='';$trade_board_permission='';$schedule_template='';
	 $controller = $this->params['controller'];
	 $action = $this->params['action'];
      if(($controller == "clients") && ($action == "client_dashboard") || ($action == "inspector_editinfo")){
	    $home = "class='current'";
	    $dashboard='active';
      }
      else if(($controller == "clients") && ($action == "client_changepasswd")){
	    //$chapass = "class='current'";
	    $chapass = "active";
      }
      else if(($controller == "clients") && ($action == "client_schedule" || $action == "client_viewQuote")){
	    //$schedule = "class='current'";
	    $schedule = "active";
      }
      else if(($controller == "messages") && ($action == "client_messages") || ($action == "client_sentlist") || ($action == "client_create_message")){
	    //$msg = "class='current'";
	     $msg = "active";
      }
?>
<div id="header">
<section class="top_blue_bar"></section> 
     	<div style='padding:0 12px; margin-top:0px; height:100px'>
      <div style="float:left; margin-top:26px; color:#fff"><h1>Client Panel</h1></div>
      <div style="float:right;">
        <section class="top_menu">
                   <section class="top_menu_l">
                      <section class="top_menu_r">
                       <h2 style='color:#fff; margin-bottom:10px;'>Client Home Page</h2>
                    </section>
                   </section>
        </section>
        <div class="user_info">
         <div class="user_img">
         <h4 style='float:right;color:#fff;margin-top:5px; clear:both'><?php echo $html->image($this->Common->getUserProfilePhoto($clientsession['User']['profilepic']));?></h4></div>
        <div class="user_txt">
         <h4 style="float:left; color:#c6c6c6; clear:both; padding-bottom:5px;">Welcome <span class="user_name"><?php echo ucwords($clientname) ;?></span></h4>
         <h4 style="float:left; color:#c6c6c6; clear:both">Logged in as <span style='color:#3688B8'><?php echo $clientsession['User']['email'] ;?></span></h4>
        </div>
     </div>
     </div>
     </div>
		
		

    <div id="topmenu">
      <ul class="f-tabs sf-menu">
            <li class="<?php echo $dashboard; ?> ext-tabs">
              <a href="/client/clients/dashboard"><span><?php echo $html->image('gnumeric.png'); ?> Dashboard</span></a>               
            </li>
	    <li class="<?php echo $schedule; ?> ext-tabs">
              <a href="/client/clients/schedule"><span><?php echo $html->image('gnumeric.png'); ?> Schedules</span></a>               
            </li>
            <li class="<?php echo $msg; ?> ext-tabs"><a href="/client/messages/messages"><span><?php echo $html->image('mail.png'); ?> Messages</span></a>
            <div class="nav-span">                
                        	<div class="sub">
                                <ul> 
                                    <li><a href="/client/messages/create_message">Compose</a></li>
                                    <li><a href="/client/messages/sentlist">Sent Box</a></li>
                                    
                                </ul>                        
                        	</div>     
                        </div>
            </li>            
            <li class="<?php echo $chapass; ?> ext-tabs"><a href="/client/clients/changepasswd"><span><?php echo $html->image('def-report.png'); ?> Change Password</span></a></li>
            <li class=" ext-tabs"><a href="/client/clients/logout"><span><?php echo $html->image('def-quote.png'); ?> Logout</span></a></li>
      </ul>
            	<!--<ul style="margin: 15px 10px;">
            	    <li <?php echo $home; ?> ><?php echo $html->link('Dashboard',array('controller'=>'clients','action'=>'dashboard')); ?></li>
                    <li <?php echo $chapass; ?> ><?php echo $html->link('Change Password',array('controller'=>'clients','action'=>'changepasswd')); ?></li>
		    <li <?php echo $schedule; ?> ><?php echo $html->link('My schedule',array('controller'=>'clients','action'=>'myschedule')); ?></li>
                    <li <?php echo $msg; ?>><?php echo $html->link('Messages',array('controller'=>'messages','action'=>'messages')); ?></li>
                    <li><?php echo $html->link('Logout',array('controller'=>'clients','action'=>'logout')); ?></li>
		    </ul>-->
          </div>
   </div>
   <div id="top-panel">
	  <div id="panel">
	      <ul>
	      </ul>
	  </div>
   </div>