<!-- Begin: Header  -->
<section id="header_full">
      <section id="header_wrap">
            <section id="header_l">
                <section id="logo"><a href="/"><?php echo $html->image('frontend/logo.png',array('alt'=>'Report Online Plus','title'=>'Report Online Plus')); ?></a></section>
            </section>
            <?php $sessionuser=$this->Session->read('Log');
            if(!empty($sessionuser))
            {
               switch($sessionuser['User']['user_type_id'])
               {
                  case 1 :                     
                        $redirect='/sp/sps/dashboard';
                        $logout='/sp/sps/logout';
                        break;
                     
                  case 2 :
                        $redirect='/inspector/inspectors/dashboard';
                        $logout='/inspector/inspectors/logout';
                        break;
                  case 3 :
                        $redirect='/client/clients/dashboard';
                        $logout='/client/clients/logout';
                        break;
                     
               }
            ?>
            <ul class="top-menu sf-menu" style="margin-right: 5px;">      
           <li><?php echo $this->Html->link('<span style="color:#fff">Welcome '.$sessionuser["User"]["fname"].'<b>&nbsp;<img src="/img/dropdown.png"></b></span>', $redirect,array('escape'=>false,'title'=>'My Account','alt'=>'My Account','class'=>'black-btn'), false ); ?>
           <div class="sub">
             <ul class="menu-submenu">
                     <li><a class="" href="<?php echo $logout?>">Logout</a></li>                 
             </ul>
           </div>
           </li>     
         </ul>   
                  
            <?php
            }
            else
            {
            ?>
            <ul class="top-menu sf-menu" style="margin-right: 60px;">      
               <li><?php echo $this->Html->link('<span style="color:#fff">Login <b>&nbsp;<img src="/img/dropdown.png"></b></span>', 'javascript:void(0);',array('escape'=>false,'title'=>'My Account','alt'=>'My Account','class'=>'black-btn'), false ); ?>
               <div class="sub">
                 <ul class="menu-submenu">
                 <li><a href="/homes/clientlogin" class="client-lnk">Client Login</a></li>
                 <li><a href="/homes/splogin" class="server-lnk">Service Provider Login</a></li>
                 <li><a href="/homes/inspectorlogin" class="insp-lnk">Inspector Login</a></li>
                     
                 </ul>
               </div>
               </li>     
         </ul>
         <!-- 2 nd start --> 
         <ul class="top-menu sf-menu" style="margin-right: 5px;">      
           <li><?php echo $this->Html->link('<span style="color:#fff">Register <b>&nbsp;<img src="/img/dropdown.png"></b></span>', 'javascript:void(0);',array('escape'=>false,'title'=>'My Account','alt'=>'My Account','class'=>'black-btn'), false ); ?>
           <div class="sub">
             <ul class="menu-submenu">
                     <li><a class="" href="/users/clientsignup">Client Registration</a></li>
                 <li><a class="" href="/users/signup">Service Provider Registration</a></li>
                 
             </ul>
           </div>
           </li>     
         </ul>                       
            <?php
            }
            ?>      

</section>
</section>
<!-- End: Header  -->

<!-- Begin: Navbar -->
<nav id="nav_full">
       <ul id="nav_wrap">
    <?php
    $controller = $this->params['controller'];
    $action = $this->params['action'];
    $pass = @$this->params['pass'][0];
    if($controller == "homes" && $action == "pages" && ($pass == "66" || $pass == "67" || $pass == "76" || $pass == "77" || $pass == "68") || ($controller == "users" && $action == "clientsignup")){
            echo '<li><a href="/">Home</a></li>';    
            foreach ($propertyOwnerLink as $data):
            echo '<li>'.$this->Html->link($data['Websitepage']['page_name'],'/homes/pages/'.$data['Websitepage']['id'],array('title'=>$data['Websitepage']['page_name'],'escape'=>false));
            endforeach;
            if(($controller == "users" && $action == "clientsignup")){
                  $class="class='active'";
            }
            else{
                  $class="class=''";
            }
            echo '<li ' .$class.'><a href="/users/clientsignup">Service Provider Recommendation</a></li>';
    }
    else
    {
            echo '<li><a href="/">Home</a></li>'; 
            $counter=1;
            foreach ($headerlinks as $data):
            echo '<li>'.$this->Html->link($data['Websitepage']['page_name'],'/homes/pages/'.$data['Websitepage']['id'],array('title'=>$data['Websitepage']['page_name'],'escape'=>false));   
            $counter++;
            echo '</li>';
            endforeach;
//            echo '<li><a href="/homes/samplereport">Sample Reports</a></li>'; 
    }
   
    ?>
  </ul>
</nav>
<!-- End: Navbar -->