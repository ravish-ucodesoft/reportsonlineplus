<!-- Begin: Header  -->
<section id="header_full">
      <section id="header_wrap">
            <section id="header_l">
                <section id="logo"><a href="/"><?php echo $html->image('frontend/logo.png',array('alt'=>'Report Online Plus','title'=>'Report Online Plus')); ?></a></section>
            </section>
            
    <ul class="top-menu sf-menu" style="margin-right: 60px;">      
      <li><?php echo $this->Html->link('<span style="color:#fff">Login <b>&nbsp;<img src="/img/dropdown.png"></b></span>', 'javascript:void(0);',array('escape'=>false,'title'=>'My Account','alt'=>'My Account','class'=>'black-btn'), false ); ?>
      <div class="sub">
        <ul class="menu-submenu">
	<li class="signin"><a href="/homes/login">Login</a>
                                   <!--<div class="sub left108">
                                          <ul class="menu-submenu">
                                                   <li class="current"><a href="/homes/clientlogin">Client Login</a></li>
                                                   <li><a href="/homes/splogin">Service Provider Login</a></li>
                                                   <li><a href="/homes/inspectorlogin">Inspector Login</a></li>
										  </ul>
                                      </div>-->
                               </li>
        </ul>
      </div>
      </li>     
    </ul>
    <!-- 2 nd start --> 
    <ul class="top-menu sf-menu" style="margin-right: 5px;">      
      <li><?php echo $this->Html->link('<span style="color:#fff">Register <b>&nbsp;<img src="/img/dropdown.png"></b></span>', 'javascript:void(0);',array('escape'=>false,'title'=>'My Account','alt'=>'My Account','class'=>'black-btn'), false ); ?>
      <div class="sub">
        <ul class="menu-submenu">
				    <li><a class="" href="/users/clientsignup">Client Registration</a></li>
				<li><a class="" href="/users/signup">Service Provider Registration</a></li>
				
        </ul>
      </div>
      </li>     
    </ul>
      

</section>
</section>
<!-- End: Header  -->

<!-- Begin: Navbar -->
<nav id="nav_full">
       <ul id="nav_wrap">
	    <li><a href="/users/step1">Company Registration</a><li>
	    <?php
		  $counter=1;
		  foreach ($spDuringRegLink as $data):	  
		  echo '<li>'.$this->Html->link($data['Websitepage']['page_name'],'/homes/pages/'.$data['Websitepage']['id'],array('title'=>$data['Websitepage']['page_name'],'escape'=>false));
		  $counter++;
		  echo '</li>';
		  endforeach;
	    ?>
       </ul>
</nav>
<!-- End: Navbar -->