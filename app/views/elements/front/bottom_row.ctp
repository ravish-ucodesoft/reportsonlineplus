<!-- Bottom Container -->
<section class="btm_cont">
    <!-- Bottom Container LEFT -->
    <section class="btm_cont_l brdr_r">
        <section class="about_us">
            <section class="btm_title"><h4>About us</h4></section>
            <section class="h_line"></section>
            <section class="btm_txt">
                <?php echo nl2br($abouttext['AboutusText']['desc']);?>
                <ul class="foot_img">
                    <li><?php echo $html->image('newdesign_img/paypal.png');?></li>
                    <li><a target="_blank" href="http://www.nfpa.org/categoryList.asp?categoryID=124&URL=Codes%20&%20Standards
"><?php echo $html->image('newdesign_img/nfpa.png',array('class'=>'nfpa'));?></a></li>
                    <li><?php echo $html->image('newdesign_img/free_trial.png');?></li>
                </ul>                                 
            </section>
        </section>
    </section>
    <section class="v_line"></section>
    <!-- Bottom Container RIGHT -->
    <section class="btm_cont_r">
        <!-- Testimonials -->
        <section class="testimonials">
            <section class="btm_title"><h4>Testimonials</h4></section>
            <section class="h_line"></section>
            <section class="btm_txt">
                <ul class="testimonials_list">
                    <marquee scrollamount="2" direction="up" loop="true" onmouseover="this.stop();" onmouseout="this.start();">
                    <?php
                        if(count($testimonials)>0){
                            foreach($testimonials as $testimonial){
                    ?>
                    <li>
                        <section class="testi_wrap">
                            <section class="testi_profile_img">
                                <?php echo $html->image('testimonial/'.$testimonial['Testimonial']['image'],array('width'=>84,'height'=>84));?>
                            </section>
                            <section class="testi_txt">
                                <p><?php echo $testimonial['Testimonial']['body'];?></p>
                                <strong>- <?php echo $testimonial['Testimonial']['from'];?></strong>
                            </section>
                        </section>
                    </li>
                    <?php }}?>
                    </marquee>                                       
                </ul>
            </section>
        </section>
        <!-- Testimonials Ends -->
    </section>
</section>
<!-- Bottom Container Ends -->