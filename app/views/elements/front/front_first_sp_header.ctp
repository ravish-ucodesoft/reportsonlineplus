<!-- Header Starts-->
           <header class="header_wrap">
                <section class="logo">
                  <a href="/"><?php echo $html->image('newdesign_img/logo.png'); ?></a>
                </section>
                <section class="top_menu">
                   <section class="top_menu_l">
                      <section class="top_menu_r">
                        <?php $sessionuser=$this->Session->read('Log');
                        if(!empty($sessionuser))
                        {
                           switch($sessionuser['User']['user_type_id'])
                           {
                              case 1 :                     
                                    $redirect='/sp/sps/dashboard';
                                    $logout='/sp/sps/logout';
                                    break;
                                 
                              case 2 :
                                    $redirect='/inspector/inspectors/dashboard';
                                    $logout='/inspector/inspectors/logout';
                                    break;
                              case 3 :
                                    $redirect='/client/clients/dashboard';
                                    $logout='/client/clients/logout';
                                    break;
                                 
                           }
                           ?><ul class="sf-menu" id="topnav">
                               <li><a href="<?php echo $redirect ?>" title="My Account">Welcome <?php echo $sessionuser["User"]["fname"] ?></a>                                  
                               </li>
                               <li><a href="<?php echo $logout?>">Logout</a>                                   
                               </li>
                           </ul>
                              
                        <?php
                           
                           
                           
                        }
                        else{
                              ?><ul class="sf-menu" id="topnav">
                               <li class="member"><a href="#">Register</a>
                                  <div class="sub">
                                      <ul class="menu-submenu">
                                               <li class="current"><a href="/users/clientsignup">Client Registration</a></li>
                                               <li><a href="/users/signup">Service Provider Registration</a></li>
									   </ul>
                  				  </div>
                               </li>
                               <li class="signin"><a href="/homes/login">Login</a>
                                   <!--<div class="sub left108">
                                          <ul class="menu-submenu">
                                                   <li class="current"><a href="/homes/clientlogin">Client Login</a></li>
                                                   <li><a href="/homes/splogin">Service Provider Login</a></li>
                                                   <li><a href="/homes/inspectorlogin">Inspector Login</a></li>
										  </ul>
                                      </div>-->
                               </li>
                           </ul>
                              
                        <?php
                        }
                        ?>
                        
                        
                        
                        
                           
                      </section>
                   </section>
                </section>
           </header>
           <!-- Header Ends-->
      <nav class="nav">
               <section class="navbar">
                  <ul id="nav_wrap">
                  <?php echo '<li class="home"><a href="/"><span></span></a></li>'; ?>
		    <?php echo '<li class="active"><a href="/">Service Providers</a></li>'; ?>
                </ul>
                    
               </section>
          	   <section class="nav_shadow"></section>
           </nav>
           <!-- Navbar Ends-->  
           
           