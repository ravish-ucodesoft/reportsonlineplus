<!-- Footer -->
  <footer class="footer">
     <section class="footer_wrap">
          <section class="foot_l">
               <ul class="foot_links">
                   <?php $counter=1;
			      $count = count($footerlinks);
                            foreach ($footerlinks as $data):
			     if($counter == $count){
			       echo '<li style="border-right:0px;">'.$this->Html->link($data['Websitepage']['page_name'],'/homes/pages/'.$data['Websitepage']['id'],array('title'=>$data['Websitepage']['page_name'],'escape'=>false));
			     }else {
			       echo '<li>'.$this->Html->link($data['Websitepage']['page_name'],'/homes/pages/'.$data['Websitepage']['id'],array('title'=>$data['Websitepage']['page_name'],'escape'=>false));
			     }
			       $counter++;
			       echo '</li>';
			     
			     endforeach;
                           // echo '&nbsp &nbsp'.$this->Html->link('Contact Us','/homes/contactus/',array('title'=>'Contact Us','escape'=>false));
                        ?>
               </ul>
            </section>
          <section class="foot_r">&copy; 2012. All Rights Reserved for ReportsOnline Plus</section>
     </section>
  </footer>
<!-- Footer Ends -->