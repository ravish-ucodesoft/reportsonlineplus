<?php
 $client=$inspectors= $schedule_inspection=$pendiing_schedule_inspection=$complete_schedule_inspection=$dashboard=$messages=$active=$home=$schedule=$chapass=$our_inspectors='';$msg='';$our_client='';$changepassword='';$services='';$employee='';$onnows='';$reports='';$trade_board_permission='';$schedule_template='';
	  $controller = $this->params['controller'];
	  $action = $this->params['action'];
	  if(($controller == "sps") && ($action == "sp_dashboard") || ($action == "sp_editinfo") || ($action == "sp_editinfo")){
	    $home = "class='current'";
	    $dashboard='active';
	  }
	  else if(($controller == "schedules") && ($action == "sp_schedule" || $action == "sp_weekly_view" || $action == "sp_day_view" || $action="sp_not_started_schedule")){
	    $schedule = "class='current'";
	    $schedule_inspection='active';
	  }
	  else if(($controller == "schedules") && ($action == "sp_pendingSchedule")){
	    $schedule = "class='current'";
	    $pendiing_schedule_inspection='active';
	  }
	  else if(($controller == "schedules") && ($action == "sp_completedschedule")){
	    $completeschedule = "class='current'";
	    $complete_schedule_inspection='active';
	  }
	  else if(($controller == "sps") && ($action == "sp_changepasswd")){
	    $changepassword = "class='current'";
	    $dashboard='active';
	  }
	  else if(($controller == "sps") && ($action == "sp_inspectorlisting") || ($action == "sp_addInspector") || ($action == "sp_editInspector")){
	    $our_inspectors = "class='current'";
	    $inspectors='active';
	  }
	  else if(($controller == "sps") && ($action == "sp_clientadded") || ($action == "sp_clientlisting") || ($action == "sp_editclient") || ($action == "sp_addclients")){
	    $our_client = "class='current'";
	    $client='active';
	  }
	  else if(($controller == "sps") && ($action == "sp_serviceslisting") || ($action == "sp_editService") || ($action == "sp_addService")){
	    $services = "class='current'";
	    $active='active';
	  }
	  else if(($controller == "messages") && ($action == "sp_messages") || ($action == "sp_show") || ($action == "sp_reply") || ($action == "sp_create_message") || ($action == "sp_sentlist")){
	    $msg = "class='current'";
	    $messages='active';
	  }
	  else if(($controller == "sps") && ($action == "sp_tutorial")){
	    $tutorial= "class='current'";
	    $tutorial_1='active';
	  }
?>

<div id="header">
      <section class="top_blue_bar"></section> 
      <div style='padding:0 12px; margin-top:0px; height:100px'>
      <div style="float:left; margin-top:26px"><?php echo $html->image('newdesign_img/anim_logo.gif'.$spsession['Company']['company_logo'],array('height'=>72,'width'=>249)); ?></div>
      <div style="float:right;">
               <section class="top_menu">
                   <section class="top_menu_l">
                      <section class="top_menu_r">
                           <h2 style='color:#fff; margin-bottom:10px;'>Service Provider Landing Page</h2>
                      </section>
                   </section>
                </section>
      
      <div class="user_info">
         <div class="user_img"><h4 style='float:right;color:#fff;margin-top:5px; clear:both'><?php echo $html->image($this->Common->getUserProfilePhoto($spsession['User']['profilepic']));?></h4></div>
         <div class="user_txt">
           <h4 style="float:left; color:#c6c6c6; clear:both; padding-bottom:5px;">Welcome <span class="user_name"><?php echo ucwords($spname) ;?>,<?php echo $html->link('Logout',array('controller'=>'sps','action'=>'logout')); ?></span></h4>
           <h4 style="float:left; color:#c6c6c6; clear:both">Logged in as <span style='color:#3688B8'><?php echo $spsession['User']['email'] ;?></span></h4>
         </div>
      </div>
      
      
      
      </div>
     </div>
    <div id="topmenu">
      <ul class="f-tabs sf-menu">
            <li class="<?php echo $dashboard?> ext-tabs">
              <a href="/sp/sps/dashboard"><span><?php echo $html->image('gnumeric.png'); ?> Dashboard</span></a>               
            
			</li>
            <li class="<?php echo $inspectors?> ext-tabs"><a href="/sp/sps/inspectorlisting"><span><?php echo $html->image('fmam.png'); ?>Inspectors</span></a>
	    <div class="nav-span">                
                        	<div class="sub">
                                <ul> 
                                <?php 
                                 //$sData=$this->Session->read('Log');
//if($this->Common->get_max_clients($sData['User']['id'])== 'maxLimitReached'){ echo "hi"; }else{ "no";} ?>
                                    <li><a href="/sp/sps/addInspector">Add Inspector</a></li>
									<li><a href="/sp/messages/inspector_leave_request">Vacation Request</a></li>
                                   <?php // } ?> 
                                </ul>                        
                        	</div>     
                        </div>
	    
	    </li>
		<li class="<?php echo $client?> ext-tabs"><a href="/sp/sps/clientlisting"><span><?php echo $html->image('client-list.png'); ?>Clients</span></a>
	    <div class="nav-span">                
                        	<div class="sub">
                                <ul> 
                                    <li><a href="/sp/sps/addclients">Create Client</a></li>
                                    
                                </ul>                        
                        	</div>     
	    </div>
	    </li>
			<li class="<?php echo $pendiing_schedule_inspection?> ext-tabs"><a href="/sp/schedules/pendingSchedule"><span><?php echo $html->image('pending-icon.png'); ?>Pending Inspection</span></a></li>    
            <li class="<?php echo $schedule_inspection?> ext-tabs">
		<a href="/sp/schedules/schedule"><span><?php echo $html->image('sheduled_task.png'); ?>Inspection Calendar</span></a>
		<div class="nav-span">                
		    <div class="sub">
		        <ul> 
		            <li><a href="/sp/schedules/schedule">Monthly View</a></li>
			    <li><a href="/sp/schedules/weekly_view/">Weekly View</a></li>
			    <li><a href="/sp/schedules/day_view/">Day View</a></li>
			    <li><a href="/sp/schedules/inspector_schedule/view:all">Cal. by Inspector</a></li>
			    <li><a href="/sp/schedules/not_started_schedule">Not Started</a></li>
                        </ul>                        
                    </div>     
		</div>
	    </li>    
            
	    <!--<li class="<?php echo $msg; ?> ext-tabs"><a href="/sp/messages/messages"><span><?php echo $html->image('mail.png'); ?> Messages</span></a>
            <div class="nav-span">                
                        	<div class="sub">
                                <ul> 
                                    <li><a href="/sp/messages/create_message">Compose</a></li>
                                    <li><a href="/sp/messages/sentlist">Sent Box</a></li>
                                    
                                </ul>                        
                        	</div>     
                        </div>
            </li>-->
			
			
	    <li class="<?php echo $complete_schedule_inspection;?> ext-tabs"><a href="/sp/schedules/completedschedule"><span><?php echo $html->image('insp-complete.png'); ?> Completed Inspection</span></a></li>
            <li class="ext-tabs"><a href="/sp/sps/leadInfo"><span><?php echo $html->image('def-report.png'); ?>Lead Requests</span></a></li>
            <!--<li class="<?php echo $msg; ?> ext-tabs" ><a href="/sp/sps/tutorial"><span><?php echo $html->image('education.png'); ?> Tutorial</span></a></li>-->
      
          </div>
   </div>
   <div id="top-panel">
            <div id="panel">
                <ul>
                </ul>
            </div>
   </div>
