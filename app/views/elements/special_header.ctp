<?php $spname=$spsession['User']['fname'].' '.$spsession['User']['lname'];?>
<div id="header">
      <section class="top_blue_bar"></section> 
      <div style='padding:0 12px; margin-top:0px; height:100px'>
            <div style="float:left; margin-top:26px"><?php echo $html->image('/img/company_logo/'.$spsession['Company']['company_logo'],array('height'=>75,'width'=>200)); ?></div>
            <div style="float: left; margin-top: 50px; width: 920px;"><h1 style="font-size:26px; color:#fff; text-align:center"><?php echo $spsession['Company']['name'];?></h1></div>
            <div style="float:right;">
                  <section class="top_menu">
                        <section class="top_menu_l">
                              <section class="top_menu_r">
                                    <h2 style='color:#fff; margin-bottom:10px;'>Login Page</h2>
                              </section>
                        </section>
                  </section>      
                  <div class="user_info">
                        <div class="user_img"><h4 style='float:right;color:#fff;margin-top:5px; clear:both'><?php echo $html->image($this->Common->getUserProfilePhoto($spsession['User']['profilepic']));?></h4></div>                        
                        <div class="user_txt">
                              <h4 style="float:left; color:#c6c6c6; clear:both; padding-bottom:5px; padding-top: 20px;"><span class="user_name"><?php echo ucwords($spname) ;?></span></h4>           
                        </div>
                  </div>      
            </div>
      </div>    
      <div id="top-panel">
            <div id="panel">
                  <ul>
                  </ul>
            </div>
      </div>
</div>