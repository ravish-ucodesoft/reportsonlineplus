<div id="head">
			<div class="inner">
				
				<!--[if !IE]>start user details<![endif]-->
				
				<div><div style="float:left">
				<?php echo $html->image("newdesign_img/anim_logo.gif", array('escape' => false,"alt"=>"Report Online Plus", "title"=>"Report Online Plus","style"=>"margin-top:15px")); 	?>
				</div><div style="float:right"><div id="user_details">
				<?php
				 $action = $this->params['action'];
				 if($action!='admin_login') 
			  	{
				 ?>
				 
					<ul id="user_details_menu">
						<li class="first">Welcome Admin<strong><?php //echo $adminName;?></strong></li>
						<li><a href="/admin/admins/changepasswd">Change Password</a></li>
						<!--<li><a href="/admin/admins/account">My Account</a></li>-->
						<li class="last"><?php echo $html->link('Log out', '/admin/admins/logout'); ?></li>
					</ul>
					<div id="server_details">
						<dl>
							<dt>Server time:</dt>
							<dd><?php echo date('M d Y h:i:s A',$_SERVER['REQUEST_TIME']); ?></dd>
						</dl>
						<!-- <dl>
							<dt>Last login ip:</dt>
							<dd><?php //echo $_SERVER['REMOTE_ADDR']; ?></dd>
						</dl> -->
					</div>
				<?php } ?></div>
			</div></div>
			                      
				
				
				<!--[if !IE]>end user details<![endif]-->
				<!--[if !IE]>start main menu<![endif]-->
				<?php
				 if(($this->params['controller'] == "admins" && $this->params['action'] == "admin_list"))
				 {
					$select = "selected";	
				 }

				if($action!='admin_login') 
				{
				$dashboard=$adminsc=$users=$adv=$home=$client_list=$banner_list=$pay=$page=$subscriberslist=$durationscharge=$emailtemp=$webpage=$code='';$site_usage='';
				$transactions='';
				$indusblock = $pblock=$gblock='none';			
				$controller = $this->params['controller'];
				$action = $this->params['action'];
				
				
        switch($controller)
       {
          case 'admins':
						$adminsc="selected"; 
            break;
          case 'emailTemplates':
						$emailtemp="selected"; 
            break;
           case 'websitepages':
						$webpage="selected"; 
            break;
      		default:
						$home="selected";
				}
				
				// Switch case to show current menu item selected in the admin section
				
       switch($action)
       {
           case 'admin_list':
  			     $adminsc="selected";
  			     $serviceProvider=$dashboard=$subscriberslist=$client_list=""; 
           break;      
           case 'admin_subscriberslist':
           /*case 'admin_editsubscriber':*/
  			     $subscriberslist="selected";
  			     $serviceProvider=$dashboard=$adminsc=$client_list=""; 
           break; 
           case 'admin_durationscharge':
  			     $durationscharge="selected";
  			     $serviceProvider=$dashboard=$subscriberslist=$client_list=""; 
           break; 
          
           case 'admin_edit':
  			     $adminsc="selected";
  			     $serviceProvider=$dashboard=$subscriberslist=""; 
           break;
           case 'admin_emailtemplist':
  			     $emailtemp="selected";
  			     $serviceProvider=$dashboard=$subscriberslist=$client_list=""; 
           break;
           case 'admin_pagelisting':
  			     $webpage="selected";
  			     $serviceProvider=$dashboard=$subscriberslist=$client_list=""; 
           break;
           case 'admin_codeListing':
  			     $code="selected";
  			     $serviceProvider=$dashboard=$subscriberslist=$adminsc=$client_list=""; 
           break;
	   case 'admin_addCode':
  			     $code="selected";
			     $serviceProvider=$dashboard=$subscriberslist=$adminsc=$client_list=""; 
           break;
           case 'admin_editCode':
  			     $code="selected";
			     $serviceProvider=$dashboard=$subscriberslist=$adminsc=$client_list=""; 
           break;
					
					case 'admin_serviceProviderList':
  			     $serviceProvider="selected";
  			     $dashboard=$subscriberslist=$adminsc=$client_list=""; 
           break;
			case 'admin_clientList':
  			     $client_list="selected";
  			     $dashboard=$subscriberslist=$serviceProvider=$adminsc=""; 
           break;
			case 'admin_bannerlisting':
  			     $banner_list="selected";
  			     $dashboard=$subscriberslist=$serviceProvider=$adminsc=""; 
           break;
           default:
  			     $dashboard="selected";
  			     $client_list=$serviceProvider=$subscriberslist=$adminsc="";
       }
        ?>
				<div id="main_menu">
					<ul>
						<li>
			            <!--<a href="/admin/admins/" class="<?php echo $dashboard; ?>"><span><span>Dashboard</span></span></a>-->
				    <?php echo $html->link('<span><span>Dashboard</span></span>',array('controller'=>'admins','action'=>'index'),array('class'=>$dashboard,'escape'=>false)); ?>
						</li>
						<li>
				    <?php echo $html->link('<span><span>Admin Staff</span></span>',array('controller'=>'admins','action'=>'list'),array('class'=>$adminsc,'escape'=>false)); ?>
						</li>
						<li>
				     <?php echo $html->link('<span><span>SP\'s</span></span>',array('controller'=>'admins','action'=>'serviceProviderList'),array('class'=>$serviceProvider,'escape'=>false)); ?>
						</li>
						<li>
				      <?php echo $html->link('<span><span>Subscription Plan</span></span>',array('controller'=>'companies','action'=>'durationscharge'),array('class'=>$durationscharge,'escape'=>false)); ?>
						</li>
						<li>
				     <?php echo $html->link('<span><span>Email Template</span></span>',array('controller'=>'emailTemplates','action'=>'emailtemplist'),array('class'=>$emailtemp,'escape'=>false)); ?>
						</li>
						<li>
				     <?php echo $html->link('<span><span>Website Pages</span></span>',array('controller'=>'websitepages','action'=>'pagelisting'),array('class'=>$webpage,'escape'=>false)); ?>
						</li>
						<li>
				     <?php echo $html->link('<span><span>Code Chart</span></span>',array('controller'=>'admins','action'=>'codeListing'),array('class'=>$code,'escape'=>false)); ?>
						</li>
						<li>
				     <?php echo $html->link('<span><span>Clients</span></span>',array('controller'=>'admins','action'=>'clientList'),array('class'=>$client_list,'escape'=>false)); ?>
						</li>
						<li>
				     <?php echo $html->link('<span><span>Home Page Banner</span></span>',array('controller'=>'banners','action'=>'bannerlisting'),array('class'=>$banner_list,'escape'=>false)); ?>
						</li>
						
						
					</ul>
				</div>
				<!--[if !IE]>end main menu<![endif]-->
				<?php } ?>
			</div>
		</div>