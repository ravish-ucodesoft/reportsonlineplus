	<!--[if !IE]>start footer<![endif]-->
	<div id="footer">
		<div class="inner">
			<!--[if !IE]>start help & footer menu<![endif]-->
			<div id="help_footer_menu">
				
			<!--	<h4>HELP AND SUPPORT</h4> -->
			
				
				<!--<h4>QUICK LINKS</h4>
				<ul id="footer_menu">
					<li><a href="#">Help</a></li>
					<li><a href="#">FAQ</a></li>
					<li><a href="#">Feedback</a></li>
					<li><a href="#">Contact us</a></li>
					<li><a href="#">Live Help</a></li>
				</ul> -->
			</div>
			<!--[if !IE]>end help & footer menu<![endif]-->
			<!--[if !IE]>start admin details<![endif]-->
			<div id="admin_details">				
				<h4>Admin Panel</h4>
				<p>	
					Copyright <?php echo date('Y'); ?> Fire Inspection Report <br />
					All rights reserved.
				</p>	
				
			</div>
			<!--[if !IE]>end admin details<![endif]-->
		</div>	
	</div>
	<!--[if !IE]>end footer<![endif]-->