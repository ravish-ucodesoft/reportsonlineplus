<div style="margin-top:20px;">    
    <table class="tbl input-chngs2" style="border:1px solid #E8E7E1;">
	<tr>
	    <td colspan="6" style="text-align: center"><strong>You have to fill at least <?php echo count($options);?> records. (<?php echo $html->link('Inspection Device List',array('controller'=>'inspectors','action'=>'deviceList?reportID='.$_REQUEST['reportID'].'&serviceCallID='.$_REQUEST['serviceCallID']),array('class'=>'ajax'));?>) </strong></td>
	</tr> 	
	<tr>
	  <th width="2%"><p class="lbl">#</p></th>
	  <th width="25%"><p class="lbl">Location</p></th>
	  <th width="25%"><p class="lbl">Born Date</p></th>
	  <th width="25%"><p class="lbl">Type</p></th>       
	  <th width="20%"><p class="lbl">Deficient</p></th>
	  <th width="3%"><p class="lbl"></p></th>
	</tr>
	<?php
	if(isset($data) && !empty($data)){
	     $k = 0;
	    foreach($data as $key=>$val){
	    $k++;
	?>
    	<tr id="tr_<?php echo $val['id']; ?>">
	    <td style="text-align: center" width="2%"><?php echo $k; ?></td>
	    <td style="text-align: center" width="25%"><?php echo $val['location']; ?></td>
	    <td style="text-align: center" width="25%"><?php echo $val['born_date']; ?></td>
	    <td style="text-align: center" width="25%"><?php echo $this->Common->getServiceName($val['type']); ?></td>	    	    
	    <td style="text-align: center" width="20%" <?php if($val['deficiency']=='Yes'){?> style="color: red"<?php }?>><?php echo $val['deficiency']; ?></td>	    
	    <td style="text-align: center" width="3%"><?php if(!(isset($result['finish_date']) && ($result['finish_date'] != ""))){ ?><a href="javascript:void(0)" onclick="confirmbox('<?php echo $val['id']; ?>')"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a><?php }?></td>
	</tr>
	<?php } } ?>
	<tr>
	    <td colspan="6">&nbsp;</td>
	</tr>
	<tr><td colspan="6" style="padding: 0px;">
	<?php if(isset($result['id']) && !empty($result['id'])){ ?>
	<?php } else{
	    $kitchenRow=0;
	    foreach($options as $key=>$optData){$kitchenRow++;
	    ?>
	    <table style="margin:0px;">
		<tr>
		  <td width="2%" style="padding: 4px 2px; text-align: center"><?php echo $kitchenRow ?></td>
		  <td width="25%" style="padding: 4px 2px; text-align: center"><?php echo $form->input('KitchenhoodReportRecord.'.$kitchenRow.'.location',array('maxLength'=>'50','type'=>'text','div'=>false,"label"=>false,"class"=>"validate[required]")); ?></td>
		  <td width="25%" style="padding: 4px 2px; text-align: center"><?php echo $form->input('KitchenhoodReportRecord.'.$kitchenRow.'.born_date',array('readonly'=>true,'type'=>'text','div'=>false,"label"=>false,'class'=>'calender validate[required]','style'=>'margin:0;')); ?></td>
		  <td width="25%" style="padding: 4px 2px; text-align: center"><?php echo $form->input('KitchenhoodReportRecord.'.$kitchenRow.'.type',array('type'=>'select','options'=>$options,'div'=>false,"label"=>false,'default'=>$key,'empty'=>'-Select-',"class"=>"validate[required]")); ?></td>		  
		  <td width="20%" style="padding: 4px 2px; text-align: center"><?php $opts=array('No'=>'No','Yes'=>'Yes');
			  echo $form->input('KitchenhoodReportRecord.'.$kitchenRow.'.deficiency',array('type'=>'select','options'=>$opts,'div'=>false,"label"=>false,'empty'=>'-Select-','onchange'=>'chkStatus(this);'));?>
		  </td>		  
		  <td width="3%" style="padding: 4px 2px;">&nbsp;</td>
		</tr>
	    </table>
	<?php }}?>
	</td></tr>
	<tr><td colspan="6"><table id="DivOption"></table> </td></tr>
	<?php if(!(isset($result['finish_date']) && ($result['finish_date'] != ""))){ ?>
	<tr>
	    <td colspan="6" align="right"><a href="#"><strong><?php echo $this->Html->link('Add More','javascript:void(0)',array('escape'=>false,'id'=>'AddOption','onclick'=>'AddRow(1)','class'=>'up_arrow'));?></strong></a></td>
	</tr>
	<?php }?>
	<tr>
	    <td colspan="6">&nbsp;</td>
	</tr>
    </table>	
<table>
<tr><td >&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>