<?php
echo $this->Html->script('jquery.1.6.1.min');   
echo $this->Html->css('validationEngine.jquery');
echo $this->Html->css(array('ajaxuploader/fileuploader'));

echo $this->Html->script('languages/jquery.validationEngine-en.js');
echo $this->Html->script('jquery.validationEngine.js');

echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
echo $this->Html->script(array('ajaxuploader/fileuploader'));
echo $this->Html->script(array('ui/ui.core.js','ui/ui.datepicker.js'));
echo $this->Html->script(array('jquery-ui.min.js','jquery-easy-confirm-dialog'));


?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript">
$(document).ready(function(){
	jQuery(".ajax").colorbox();
	jQuery(".inline").colorbox({inline:true, width:"50%"});
	loadDatePicker();
	jQuery("#extinguisher").validationEngine();
	 <?php
	   if(isset($result['finish_date']) && ($result['finish_date'] != "")){ ?>
	   
	    for(i = 0; i < document.getElementById('extinguisher').length; i++){
		var formElement = document.getElementById('extinguisher').elements[i];	
		    formElement.disabled = true;
	    }
	    
           <?php } ?>

});
 function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }

/*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
	    }
            });           
    }        
window.onload = createUploader;
	
function loadDatePicker()
{
   jQuery(".calender").datepicker({ dateFormat: 'mm/dd/yy',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });
}
 function checkUploadedAttachment(){
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
  function checkNotification(){
	var notifyVal = jQuery("#NotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }
</script>
<?php echo $this->Html->css('reports'); ?>
<script type="text/javascript">
function AddRow(divid){    
    var counter = jQuery('#optioncount').val();
    // var type= new array('2.5 lb Dry Chemical'=>'2.5 lb Dry Chemical','5 lb Dry Chemical'=>'5 lb Dry Chemical','10 lb Dry Chemical'=>'10 lb Dry Chemical','20 lb Dry Chemical'=>'20 lb Dry Chemical','5 lb CO2'=>'5 lb CO2','10 lb CO2'=>'10 lb CO2','15 lb CO2'=>'15 lb CO2','20 lb CO2'=>'20 lb CO2','5 lb FE36 Clean Agent'=>'5 lb FE36 Clean Agent','10 lb FE36 Clean Agent'=>'10 lb FE36 Clean Agent','13 lb FE36 Clean Agent'=>'13 lb FE36 Clean Agent','2.5 Gal Pressurized Water'=>'2.5 Gal Pressurized Water','K Class'=>'K Class');
    //alert(type);
    counter = parseInt(counter) + 1;
    jQuery('#optioncount').val(counter);
    var OptionHtml = '<tr id="option_'+counter+'">';
    OptionHtml = OptionHtml+'<td width="2%" style="padding: 4px 2px;">'+counter+'</td>';
    OptionHtml = OptionHtml+'<td width="8%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][location]" class="validate[required]" id="" ></td>';
    OptionHtml = OptionHtml+'<td width="5%" style="padding: 4px 2px;"><select id="status_'+counter+'" name="data[ExtinguisherReportRecord]['+counter+'][ext_type]" class="validate[required]" style="width:80px"><option value="" selected="selected">--Select-</option>';
    <?php foreach($options as $key=>$val){ ?>
    OptionHtml = OptionHtml+'<option value="<?php echo $key; ?>"><?php echo $val; ?></option>';    
    <?php } ?>
    OptionHtml = OptionHtml+'</select></td>';
    OptionHtml = OptionHtml+'<td width="8%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][model]"  id="" class="validate[required]" ></td>';
    OptionHtml = OptionHtml+'<td width="8%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][born_date]"  id="" class="calender validate[required]" style="width:42px;margin:0;" readonly = "readonly"></td>';
    OptionHtml = OptionHtml+'<td width="5%" style="padding: 4px 2px;"><select onchange="chkStatus(this);" id="status_'+counter+'" name="data[ExtinguisherReportRecord]['+counter+'][deficiency]" style="width:60px"><option value="No" selected="selected">No</option><option value="Yes">Yes</option></select></td>';    
    OptionHtml = OptionHtml+'<td width="7%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_maintenace_date]"  id="" class="calender" style="width:50px;margin:0;" ></td>';
    OptionHtml = OptionHtml+'<td width="13%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_maintenace_performed_by]"  id="" style="" ></td>';
    OptionHtml = OptionHtml+'<td width="7%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_recharge_date]"  id="" class="calender" style="width:50px;margin:0;" ></td>';
    OptionHtml = OptionHtml+'<td width="13%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_recharge_performed_by]"  id="" ></td>';
    OptionHtml = OptionHtml+'<td width="7%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_hydrotest_date]"  id="" class="calender" style="width:50px;margin:0;" ></td>';
    OptionHtml = OptionHtml+'<td width="13%" style="padding: 4px 2px;"><input type="text" name="data[ExtinguisherReportRecord]['+counter+'][last_hydrotest_performed_by]"  id="" style="" ></td>';
    OptionHtml = OptionHtml+'<td width="2%" style="padding: 4px 2px;"><a href="javascript:void(0)" onclick="RemoveRow('+counter+',this)"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></td>';    
    OptionHtml = OptionHtml+'</tr>';
    jQuery('#DivOption').append(OptionHtml);
     loadDatePicker();
}

function RemoveRow(divid,obj){    
    selectBox  = jQuery(obj).parent('td').parent('tr').find('select:first');
    deficienVal = selectBox.parent('td').next('td').next('td').next('td').children('select').val();
    rowId = jQuery(obj).parent('td').parent('tr').children('td:first').html();
    locationData = selectBox.parent('td').prev('td').children('input').val();
    modelData = selectBox.parent('td').next('td').children('input').val();
    serviceId = selectBox.val();
    if(deficienVal=='No'){
	jQuery('#option_'+divid).remove();
	return false;
    }
    if(deficienVal=='Yes'){
	if(!confirm("It will also delete the added deficiency. Do you want to continue?")){	
		return false;
	}	
    }
    reportId = '<?php echo $_REQUEST['reportID'];?>';
    clientId = '<?php echo $_REQUEST['clientID'];?>';
    spId = '<?php echo $_REQUEST['spID'];?>';
    servicecallID = '<?php echo $_REQUEST['serviceCallID'];?>';
    
    data = 'reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId;
    jQuery.ajax({
	'url':'/inspector/reports/deleteDeficiency/',
	'data':data,
	'type':'post',
	'success':function(msg){
		if(msg == 'Success'){
			jQuery('#option_'+divid).remove();
		}else{
			alert('Problem in deleting the record. Please try again');
		}
	}
    });    
}
 
function RemoveRecord(rId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delrecord/" + rId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#tr_'+rId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmbox(rId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveRecord(rId);    
  }
  else{
    return false;
  }
 }
function RemoveAttachment(attachId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmattach(attachId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId);    
  }
  else{
    return false;
  }
 }
function RemoveNotifier(notifierId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delnotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#pnotify_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId);    
  }
  else{
    return false;
  }
 }  
</script>
<script type="text/javascript" >
jQuery.noConflict();
jQuery(function() {
	// setup ul.tabs to work as tabs for each div directly under div.panes
	//jQuery("ul.tabs").tabs("div.panes > div");	
});

function chkStatus(obj){
	response = jQuery(obj).val();
	currentObj = jQuery(obj);
	selectBox  = jQuery(obj).parent('td').parent('tr').find('select:first');	
	rowId = jQuery(obj).parent('td').parent('tr').children('td:first').html();
	locationData = selectBox.parent('td').prev('td').children('input').val();
	modelData = selectBox.parent('td').next('td').children('input').val();
	serviceId = selectBox.val();
	reportId = '<?php echo $_REQUEST['reportID'];?>';
	clientId = '<?php echo $_REQUEST['clientID'];?>';
	spId = '<?php echo $_REQUEST['spID'];?>';
	servicecallID = '<?php echo $_REQUEST['serviceCallID'];?>';
	if(response=='No'){		
		if(!confirm("It will delete the added deficiency. Do you want to continue?")){
			jQuery(obj).val("Yes");
			return false;
		}else{
			data = 'reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId;
			jQuery.ajax({
				'url':'/inspector/reports/deleteDeficiency/',
				'data':data,
				'type':'post',
				'success':function(msg){
					currentObj.css('color','#666666');
					return false;
				}
			});
		}
		return false;
	}
	if(serviceId=="" || locationData=="" || modelData==""){
		alert('Please fill location, type and model first');
		jQuery(obj).val("No");
		return false;
	}
	currentObj.css('color','red');
	PopupCenter('/inspector/reports/fillDeficiency?reportID='+reportId+'&clientId='+clientId+'&spId='+spId+'&serviceCallId='+servicecallID+'&serviceId='+serviceId+'&rowId='+rowId+'&locationData='+locationData+'&modelData='+modelData,'','600','400');
}
</script>
<style>
.lbtxt{text-align:right;width:20%}
.input-chngs1 input {
    margin-right: 3px;
    width: 180px;
}
.input-chngs2 input {
	margin:0px;
	color: #666666;
	font-size: 12px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
table td label {
	float: none;
}
</style>

<!--<ul class="tabs">
	<li><a href="#">Building Info</a></li>
	<li><a href="#">ExtinguisherReports</a></li>
	<li><a href="#">Report</a></li> 
</ul>-->
<?php
	$actionUrl = 'extinguishers?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID'];
?>
 <?php echo $form->create('Reports', array('type'=>'POST', 'action'=>$actionUrl,'name'=>'extinguisher','id'=>'extinguisher')); ?>
 <?php echo $form->input('ExtinguisherReport.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
 <?php echo $form->input('ExtinguisherReport.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
 <?php echo $form->input('ExtinguisherReport.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
 <?php echo $form->input('ExtinguisherReport.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
 <?php echo $form->input('ExtinguisherReport.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
 <?php echo $form->input('ExtinguisherReport.id',array('type'=>'hidden','value'=>@$result['id'])); ?>
<div id="content">
    <div class='message'><?php echo $this->Session->flash();?></div>
    <div id="deleteFlashmessage" style="display:none;">
        <div class="notification msginfo">
            <a class="close"></a>
            <p style="text-align:center;color:red">Record has been deleted successfully</p>
        </div>
    </div>
    <div id="box">
	<div style="width: 100%">
		<?php //echo $html->link($html->image("pdf.png",array('title'=>'Print Preview')), 'reportPreview?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
		<?php echo $html->link($html->image("fineimages/pdf_preview.png",array('title'=>'Print Preview')), 'extinguisherReportPreview?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId, array('escape' => false));?>
	</div>
    <h3 align="center" >Extinguisher Report</h3>
    <h4 class="table_title">Agreement and Report of Extinguisher</h4>
    <table cellpadding="0" cellspacing="0" border="0" class="table_new">
	    <tr>
		<td>
		    <b>Days Left for report Submission: <?php echo $daysRemaining;?></b>
		</td>
                <td align="right">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
			    <td width="100%" align="right">
				<?php
				    $options=array("monthly"=>"Monthly","Quarterly"=>"Quarterly","semi_annual"=>"Semi-Annual","Annual"=>"Annual");
				    $attributes=array('legend'=>false,'separator'=>'&nbsp;&nbsp;','class'=>'validate[required]');
				    echo $this->Form->radio('ExtinguisherReport.inspection_report',$options,$attributes);
				?>				    
			    </td>                                
                        <tr>
                    </table>
                </td>
            </tr>
            <tr>
	        <th colspan="2" align="left">Client information</th>
	    </tr>
            <tr>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="table_label"><strong>Building Information</strong></td>
                            <td><?php echo $clientResult['User']['client_company_name'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $clientResult['User']['address'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>City/State/Zip:</strong></td>
                            <td> <?php echo $clientResult['User']['city'].', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Country: </strong></td>
                            <td><?php echo $clientResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="table_label"><strong>Contact: </strong></td>
                            <td><?php echo $clientResult['User']['fname'].' '.$clientResult['User']['lname'];?></td>
                        </tr>
                        <tr>
                            <td class="table_label"><strong>Phone:</strong></td>
                            <td><?php echo $clientResult['User']['phone'];?></td>
                        </tr>                               
                        <tr>
                            <td class="table_label"><strong>Email: </strong></td>
                            <td> <?php echo $clientResult['User']['email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
	    <tr>
		<th colspan="2" align="left">Site Information</th>
	    </tr>
            <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Site Name:</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_name'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_address'];?>235 Main Street</td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <td> <?php echo $siteaddrResult['SpSiteAddress']['site_city'].', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Country: </strong></td>
                            <td><?php echo $siteaddrResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Contact: </strong></td>
                            <td> <?php echo $siteaddrResult['SpSiteAddress']['site_contact_name'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_phone'];?></td>
                        </tr>                               
                        <tr>
			    <td class="table_label"><strong>Email: </strong></td>
                            <td><?php echo $siteaddrResult['SpSiteAddress']['site_email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><th colspan="2" align="left">Service Provider Info</th></tr>
            <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Name:</strong></td>
                            <td><?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Address</strong></td>
                            <td><?php echo $spResult['User']['address'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>City/State/Zip:</strong></td>
			    <td><?php echo $spResult['User']['city'].', '.$spResult['State']['name'].' '.$spResult['User']['zip'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Country: </strong></td>
			    <td><?php echo $spResult['Country']['name'];?></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">
			<tr>
			    <td class="table_label"><strong>Contact: </strong></td>
                            <td> <?php echo $spResult['User']['fname'].' '.$spResult['User']['lname'];?></td>
                        </tr>
                        <tr>
			    <td class="table_label"><strong>Phone:</strong></td>
			    <td><?php echo $spResult['User']['phone'];?></td>
                        </tr>                               
                        <tr>
			    <td class="table_label"><strong>Email: </strong></td>
			    <td><?php echo $spResult['User']['email'];?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div style="border:4px solid #000; padding:10px; margin:10px">
	    Customer hires Reportsonlineplus.com to conduct an inspection of the fire suppression system at the above location or building and to prepare a written report of that inspection for the agreed upon fee. 
	    As a part of this agreement, customer agrees to Terms and Conditions found on Report of Inspection and will complete the Owner's Section of this document. 
	    Reportsonlineplus.com will not be held liable for incorrect or unknown aswers provided in the Owner's Section.
        </div>
	<div style="float:right;padding-right:10px;">
	    <?php
	        $permissionfill=$this->Common->checkPermission(1,$session_inspector_id);    
	        //if($session_inspector_id!=$schedule['lead_inspector_id'])
	        if($permissionfill!=1)
	        {
	    ?>
		    <a href="javascript:void(0)" title="Add Attachment"  class="linktoreport" onclick="alert('<?php echo $permissionfill;?>')">Add Attachment</a><span style='vertical-align:bottom'><?php echo $html->image('icon_attachment.gif');?></span>
		    
	    <?php } else { ?>
	    <?php echo $html->link('Add Attachment','#inline_content_attachment',array('class' => 'inline'));?><?php echo $html->image('icon_attachment.gif');?>&nbsp;&nbsp;&nbsp;
	    <?php } ?>
	    <?php
		$permissionfillnotify=$this->Common->checkPermission(2,$session_inspector_id);    
		//if($session_inspector_id!=$schedule['lead_inspector_id'])
		if($permissionfillnotify!=1)
		{
	    ?>
		    <a href="javascript:void(0)" title="Add Notification"  class="linktoreport" onclick="alert('<?php echo $permissionfillnotify;?>')">Add Notification</a><span style='vertical-align:bottom'><?php echo $html->image('icon_notify.gif');?></span>
	    <?php } else { ?>
		    <?php echo $html->link('Add Notifier',"#inline_content_notification",array('class' => 'inline'));?><?php echo $html->image('icon_notify.gif');?>
	    <?php } ?>
	    <?php
		if(sizeof($data)>0){
			$countVal = count($data);
		}else{
			$countVal = 1;
		}
		echo $this->Form->hidden('ExtinguisherReport.optioncount',array('value'=>$countVal,'id'=>'optioncount'));
	    ?>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="table_new">			    			    
	    <tr>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">			
			
		        <tr>
			    <td class="table_label"><strong>Inspection Date</strong></td>
			    <td><?php echo $form->input('ExtinguisherReport.inspection_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;',"class"=>"calender validate[required]","readonly"=>"readonly",'value'=>@$result['inspection_date'])); ?> </td>
		        </tr>			
			<tr>  
			    <td class="table_label">Inspection Contract Number</td>
			    <td><?php echo $form->input('ExtinguisherReport.inspection_contract_number',array('type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;',"class"=>"validate[required,custom[number]]",'value'=>$result['inspection_contract_number'])); ?></td>
			</tr>				      			
		    </table>
		</td>
		<td valign="top">
		    <table width="100%" cellpadding="0" cellspacing="0">			
			<tr>
			    <td class="table_label"><strong>Inspection Time</strong></td>
			    <td>		
			        <?php $opt=array('Normal'=>'Normal','After'=>'After','Weekend Hours'=>'Weekend Hours');
			        echo $form->input('ExtinguisherReport.inspection_time',array('type'=>'select','options'=>$opt,@$result['inspection_time'],'div'=>false,"label"=>false,'style'=>'width:180px;')); ?>
			    </td>
		        </tr>
			
			<tr>
			    <td class="table_label"><strong>Comments</strong></td>
			    <td>		
			        <?php echo $form->input('ExtinguisherReport.comments',array('type'=>'textarea','rows'=>5,'cols'=>55,'style'=>'border:1px solid #EBEBEB; background: none repeat scroll 0 0 #E2E3E3','div'=>false,"label"=>false,'value'=>@$result['comments'])); ?>
			    </td>
		        </tr>
		    </table>
		</td>
	    </tr>    
	</table>
	
	<?php echo $this->element('reports/extinguisher_form'); ?>
        <!--<tr>
	    <td align="center">
 			<?php
	/*$permissionfilldef=$this->Common->checkPermission(6,$session_inspector_id);    
	//if($session_inspector_id!=$schedule['lead_inspector_id'])
	if($permissionfilldef!=1)
	{
	?>
	    <a href="javascript:void(0)" title="Add Deficiency"  class="linktoreport" onclick="alert('<?php echo $permissionfilldef;?>')"><span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span></a>
		
	<?php } else {								
	?>
	<?php echo $html->link('<span style="color:#3688B7; font-size:17px; font-weight:bold; text-decoration:blink;"><i>Is there any deficiency in this report? If any please refer to this link.</i></span>',array('controller'=>'reports','action'=>'defAndRec?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),array('class' => 'ajax','escape'=>false),false); ?> 
    <?php
    }*/ 
    ?>
 </td></tr>-->
    <tr>      
      <td>
	 <?php
	   if(isset($result['finish_date']) && ($result['finish_date'] != "")){
	    echo $html->link('<span>FINISH</span>','javascript:void(0)',array('escape'=>false,'class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'extinguisherView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');"));
	    } else { ?>
		<span class="blue-btn" style="float:right;">
 			<input type="submit" id="linkFinish", name="finish" value="FINISH" onclick="return confirm('Are you sure that you want to finish this report? after clicking ok you will not be able to further edit this report.')" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/>
		</span>		
	    <?php } ?>
		<?php echo $html->link('<span>REVIEW</span>','javascript:void(0);',array('escape'=>false,'id'=>'reviewanswer','class'=>"blue-btn",'onclick'=>"PopupCenter('".$html->url(array('controller'=>'reports','action'=>'extinguisherView?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId))."','','1000','800');")); ?>
		<span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" name="save" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></td>
    </tr>
    <?php  if(isset($this->data['ExtinguisherReport']['finish_date']) && ($this->data['ExtinguisherReport']['finish_date'] != "")){ ?>
				<tr><td align="center"><font color="red" size="3px">The form is disabled, as you have finished the report. </font></td></tr>
				<?php } ?>
  </table> 
  </div>
<!-- ExtinguisherReport Info tab end-->
</div>
<!--box end -->
</div>
<?php echo $form->end(); ?>
    <!--content end -->
    
    <!-- This contains the hidden content for inline calls -->
	<div style='display:none'>		
		<div id='inline_content_attachment' style='padding:10px; background:#fff;'>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'attachment','name'=>'attachment','id'=>'attachment',"onsubmit"=>"return checkUploadedAttachment();")); ?>
		<?php echo $form->input('Attachment.attach_file', array('id'=>'uploadattachment','type'=>'hidden','value'=>'')); ?>
		<?php echo $form->input('Attachment.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
		<?php echo $form->input('Attachment.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Attachment.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Attachment.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Attachment.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		<p><strong>Upload Attachment</strong></p>
		<p>
		    <div id="demo"></div>
		    <ul id="separate-list"></ul>
		</p>
		<p><div id="uploaded_picture" style="text-align:center;padding-left:90px;"></div></p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		<?php if(!empty($attachmentdata)){ ?>
			<p><strong>Attachments</strong></p>
		<?php } ?>
		
			<?php foreach($attachmentdata as $attachdata){ ?>
			<p id="p_<?php echo $attachdata['Attachment']['id'] ?>"><?php echo $html->link($attachdata['Attachment']['attach_file'],array("controller"=>"reports","action"=>"download",$attachdata['Attachment']['attach_file']),array()).'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmattach('<?php echo $attachdata['Attachment']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>
		</div>
		<?php echo $form->end(); ?>
	</div>
       <!-- This contains the hidden content for inline calls -->
	<div style='display:none'>		
		<div id='inline_content_notification' style='padding:10px; background:#fff;'>
		<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'notification','name'=>'notification','id'=>'notification',"onsubmit"=>"return checkNotification();")); ?>
		<?php echo $form->input('Notification.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
		<?php echo $form->input('Notification.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
		<?php echo $form->input('Notification.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $form->input('Notification.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
		<?php echo $form->input('Notification.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
		<p><strong>Notification</strong></p>
		<p>
		    <?php echo $form->input('Notification.notification',array('type'=>'textarea','rows'=>5,'cols'=>40,'label'=>false,'id'=>'ContentMessage','onKeyUp' =>"return limit('ContentMessage',500);")); ?>
		</p>
		<p>
                    <div id="essay_Error" class='error'></div>
			<div>
			<small>&nbsp;<label id='limitCounter'>0</label>&nbsp;characters entered&nbsp;|&nbsp;<label id='limitCounterLeft'>500</label>&nbsp;characters remaining</small>
			</div>   
		</p>
		<p><span class="blue-btn" style="float:right;"><input type="submit" value="SAVE" style="margin-right:0; border:0; color:#fff; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/></span></p>
		
		<?php if(!empty($notifierdata)){ ?>
			<p><strong>Notifiers</strong></p>
		<?php } ?>
		
		
			<?php foreach($notifierdata as $notifydata){ ?>
			<p id="pnotify_<?php echo $notifydata['Notification']['id'] ?>"><?php echo $notifydata['Notification']['notification'].'('.$this->Common->getClientName($inspector_id).')'; ?><a href="javascript:void(0)" onclick="confirmnotifier('<?php echo $notifydata['Notification']['id'] ?>')">&nbsp;<img src="../../img/closeNew.png" alt="Delete" title="Delete" style="vertical-align: middle;" /></a></p>
		<?php } ?>
		
		</div>
		<?php echo $form->end(); ?>
	</div>