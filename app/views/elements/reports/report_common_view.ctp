<?php
$cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
$spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
$siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
$html='<tr>
        <td width="50%" align="left" style="padding-left:30px;"><i><b>Prepared For:</b></i>
             <br/>'.ucwords($clientResult['User']['client_company_name']).'<br/>'.$clientResult['User']['address'].'<br/>';
            if(!empty($clientResult['User']['country_id'])) {
                $html.=$this->Common->getCountryName($clientResult['User']['country_id']).', ';
            }
            if(!empty($clientResult['User']['state_id'])) {
                $html.=$this->Common->getStateName($clientResult['User']['state_id']).', ';
            }
            $html.=$clientResult['User']['city'];                
            
            $html.='<br/>';
			$html.='<br/>
			<i><b>Site information:</b></i><br/>';
			$html.=$siteaddrResult['SpSiteAddress']['site_name'];					
			$html.='<br/>'.$siteaddrResult['SpSiteAddress']['site_address'];
			$html.='<br/>'.$siteCityName.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'];
			$html.='<br/>'.$siteaddrResult['Country']['name'];
			
			
        $html.='</td>
        <td width="50%" align="right" style="padding-right:30px;">
            <i><b>Prepared By:</b></i>
            <br/>'.$spResult['Company']['name'].'<br/>
            '.$spResult['User']['address'].'
            <br/>';
            if(!empty($spResult['Country']['name'])){
                $html.=$spResult['Country']['name'].', ';
            }
            if(!empty($spResult['State']['name'])) {
                $html.=$spResult['State']['name'].', ';
            }
            $html.=$spResult['User']['city'];            
            $html.='<br/>
            Phone " "'.$spResult['User']['phone'];
        $html.='</td>            
    </tr>';
    echo $html;
?>