<!-- ExtinguisherReport tab Start-->
<div style="margin-top:20px;">
    <h3 class="main-heading" align="center" >PORTABLE FIRE ExtinguisherReport RECORD</h3>
    <table class="tbl input-chngs2" style="border:1px solid #E8E7E1;">
      <tr>
	<td colspan="14" style="text-align: center"><strong>You have to fill at least <?php echo count($options);?> records. (<?php echo $html->link('Inspection Device List',array('controller'=>'inspectors','action'=>'deviceList?reportID='.$_REQUEST['reportID'].'&serviceCallID='.$_REQUEST['serviceCallID']),array('class'=>'ajax'));?>) </strong></td>
      </tr> 	
      <tr>
        <th rowspan="2" width="2%"><p class="lbl">#</p></th>
        <th rowspan="2" width="8%"><p class="lbl">Location</p></th>
        <th rowspan="2" width="5%"><p class="lbl">Type</p></th>
        <th rowspan="2" width="8%"><p class="lbl">Model</p></th>
        <th rowspan="2" width="8%"><p class="lbl">Born Date</p></th>
        <th width="5%"><p class="lbl">Deficient</p></th>
        <!--<th width="8%"><p class="lbl">Deficient Reason</p></th>-->
        <th colspan="2" width="20%"><p class="lbl">Last Maintenace</p></th>
        <th colspan="2" width="20%"><p class="lbl">Last Recharge</p></th>
        <th colspan="2" width="20%"><p class="lbl">Last Hydro test</p></th>
	<th rowspan="2" width="2%"><p class="lbl"></p></th>
      </tr>
      
      <tr>        
        <th><p class="lbl">Yes/No</p></th>
        <!--<th><p class="lbl">&nbsp;</p></th>-->
        <th><p class="lbl">Date</p></th>
        <th><p class="lbl">Performed By</p></th>
        <th><p class="lbl">Date</p></th>
        <th><p class="lbl">Performed By</p></th>
        <th><p class="lbl">Date</p></th>
        <th><p class="lbl">Performed By</p></th>        
      </tr>
      
      <tr>
        <td colspan="14"><strong><?php echo "" ?></strong></td>
      </tr>      		
	<?php
	if(!empty($data) && isset($data))
	{       $k = 0;
		foreach($data as $key=>$val){$k++;
	?>
	<tr id="tr_<?php echo $val['id']; ?>">
	<td width="2%"><?php echo $k; ?></td>
	<td width="8%"><?php echo $val['location']; ?></td>
	<td width="5%"><?php echo $this->Common->getServiceName($val['ext_type']); ?></td>
	<td width="8%"><?php echo $val['model']; ?></td>
	<td width="8%"><?php echo $val['born_date']; ?></td>
	<td width="5%" <?php if($val['deficiency']=='Yes'){?> style="color: red"<?php }?>><?php echo $val['deficiency']; ?></td>
	<!--<td width="8%"><?php //echo $val['deficiency_reason']; ?></td>-->
	<td width="10%"><?php echo $val['last_maintenace_date']; ?></td>
	<td width="10%"><?php echo $val['last_maintenace_performed_by']; ?></td>
	<td width="10%"><?php echo $val['last_recharge_date']; ?></td>
	<td width="10%"><?php echo $val['last_recharge_performed_by']; ?></td>
	<td width="10%"><?php echo $val['last_hydrotest_date']; ?></td>
	<td width="10%"><?php echo $val['last_hydrotest_performed_by']; ?></td>	
	<td width="2%"><?php if(!(isset($result['finish_date']) && ($result['finish_date'] != ""))){ ?><a href="javascript:void(0)" onclick="confirmbox('<?php echo $val['id']; ?>')"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a><?php }?></td>
	</tr>
	<?php } } ?>
      
      <tr>
        <td colspan="14">&nbsp;</td>
      </tr>
      
   <tr><td colspan="14" style="padding: 0px;">
    <?php if(isset($result['id']) && !empty($result['id'])){ ?>
    <?php } else{ 
	$extinguisherRow=0;
	foreach($options as $key=>$optData){$extinguisherRow++;
	?>
   <table style="margin:0px;">
      <tr>
        <td width="2%" style="padding: 4px 2px;"><?php echo $extinguisherRow; ?></td>
	<td width="8%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.location',array('maxLength'=>'50','type'=>'text','div'=>false,"label"=>false,"class"=>"validate[required]")); ?></td>
        <td width="5%" style="padding: 4px 2px;">
	         <?php $opttypes=array('2.5 lb Dry Chemical'=>'2.5 lb Dry Chemical','5 lb Dry Chemical'=>'5 lb Dry Chemical','10 lb Dry Chemical'=>'10 lb Dry Chemical','20 lb Dry Chemical'=>'20 lb Dry Chemical','5 lb CO2'=>'5 lb CO2','10 lb CO2'=>'10 lb CO2','15 lb CO2'=>'15 lb CO2','20 lb CO2'=>'20 lb CO2','5 lb FE36 Clean Agent'=>'5 lb FE36 Clean Agent','10 lb FE36 Clean Agent'=>'10 lb FE36 Clean Agent','13 lb FE36 Clean Agent'=>'13 lb FE36 Clean Agent','2.5 Gal Pressurized Water'=>'2.5 Gal Pressurized Water','K Class'=>'K Class');
	               echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.ext_type',array('type'=>'select','options'=>$options,'default'=>$key,'div'=>false,"label"=>false,'empty'=>'-Select-','style'=>'width:80px;',"class"=>"validate[required]")); ?>
	</td>
        <td width="8%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.model',array('maxLength'=>'50','type'=>'text','div'=>false,"label"=>false,"class"=>"validate[required]")); ?></td>
        <td width="8%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.born_date',array('readonly'=>true,'type'=>'text','div'=>false,"label"=>false,'class'=>'calender validate[required]','style'=>'width:42px;margin:0;')); ?></td>
        <td width="5%" style="padding: 4px 2px;"><?php $opts=array('No'=>'No','Yes'=>'Yes');
		echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.deficiency',array('type'=>'select','options'=>$opts,'div'=>false,"label"=>false,'style'=>'width:60px;','onchange'=>'chkStatus(this);'));?>
	</td>
	<!--<td width="8%" style="padding: 4px 2px;"><?php //echo $form->input('ExtinguisherReportRecord.1.deficiency_reason',array('maxLength'=>'100','type'=>'text','div'=>false,"label"=>false)); ?></td>-->
	<td width="7%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.last_maintenace_date',array('readonly'=>true,'type'=>'text','div'=>false,"label"=>false,'class'=>'calender','style'=>'width:50px;margin:0;')); ?></td>
        <td width="13%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.last_maintenace_performed_by',array('maxLength'=>'100','type'=>'text','div'=>false,"label"=>false)); ?></td>
	<td width="7%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.last_recharge_date',array('readonly'=>true,'type'=>'text','div'=>false,"label"=>false,'class'=>'calender','style'=>'width:50px;margin:0;')); ?></td>
        <td width="13%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.last_recharge_performed_by',array('maxLength'=>'100','type'=>'text','div'=>false,"label"=>false)); ?></td>	
	<td width="7%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.last_hydrotest_date',array('readonly'=>true,'type'=>'text','div'=>false,"label"=>false,'class'=>'calender','style'=>'width:50px;margin:0;')); ?></td>
        <td width="13%" style="padding: 4px 2px;"><?php echo $form->input('ExtinguisherReportRecord.'.$extinguisherRow.'.last_hydrotest_performed_by',array('maxLength'=>'100','type'=>'text','div'=>false,"label"=>false)); ?></td>
        <td width="2%" style="padding: 4px 2px;">&nbsp;</td>
      </tr>
      </table>
   <?php }} ?>
   </td></tr>
        <tr><td colspan="14"><table id="DivOption"></table> </td></tr>
      <?php if(!(isset($result['finish_date']) && ($result['finish_date'] != ""))){ ?>
      <tr>
        <td colspan="14"><a href="#"><strong><?php echo $this->Html->link('Add More','javascript:void(0)',array('escape'=>false,'id'=>'AddOption','onclick'=>'AddRow(1)','class'=>'up_arrow'));?></strong></a></td>
      </tr>
      <?php }?>
      <tr>
        <td colspan="14">&nbsp;	</td>
      </tr>
      <!--<tr>
        <td colspan="14" align='left' style="text-align:left">Comments:</td>
      </tr>
      <tr>
        <td colspan="14" align='left' style="text-align:left"><?php //echo $form->input('ExtinguisherReport.comments',array('type'=>'textarea','rows'=>14,'cols'=>115,'style'=>'border:1px solid #EBEBEB;','div'=>false,"label"=>false,'value'=>@$result['comments'])); ?></td>
      </tr>-->
    </table>
<table>
  <tr><td >&nbsp;</td></tr>
  <tr><td>&nbsp;</td></tr>