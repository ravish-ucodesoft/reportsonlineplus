<?php
    $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
    $spCityName = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
    $siteCityName = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
    $html='<table width="100%" cellpadding="0" cellspacing="0">
    <tr><th colspan="2" align="left" style="background-color: #C6C6C6"> <b>Client information</b></th></tr>
    <tr>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="table_label"><strong>Building Information</strong></td>
                    <td>'.$clientResult['User']['client_company_name'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Address</strong></td>
                    <td>'.$clientResult['User']['address'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>City/State/Zip:</strong></td>
                    <td>'.$cityName.', '.$clientResult['State']['name'].' '.$clientResult['User']['zip'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Country: </strong></td>
                    <td>'.$clientResult['Country']['name'].'</td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
		    <td class="table_label"><strong>Contact: </strong></td>
		    <td>'.$clientResult['User']['fname'].' '.$clientResult['User']['lname'].'</td>
		</tr>
		<tr>
                    <td class="table_label"><strong>Phone:</strong></td>
                    <td>'.$clientResult['User']['phone'].'</td>
		</tr>                               
		<tr>
                    <td class="table_label"><strong>Email: </strong></td>
                    <td>'.$clientResult['User']['email'].'</td>
		</tr>
            </table>
	</td>
    </tr>
    <tr>
        <th colspan="2">&nbsp;</th>
    </tr>
    <tr><th colspan="2" align="left" style="background-color: #C6C6C6"> <b>Site Information</b></th></tr>
    <tr>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
		<tr>
                    <td class="table_label"><strong>Site Name:</strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_name'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Address</strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_address'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>City/State/Zip:</strong></td>
                    <td>'.$siteCityName.', '.$siteaddrResult['State']['name'].' '.$siteaddrResult['SpSiteAddress']['site_zip'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Country: </strong></td>
                    <td>'.$siteaddrResult['Country']['name'].'</td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="table_label"><strong>Contact: </strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_contact_name'].'</td>
                 </tr>
                <tr>
                    <td class="table_label"><strong>Phone:</strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_phone'].'</td>
                </tr>                               
                <tr>
                    <td class="table_label"><strong>Email: </strong></td>
                    <td>'.$siteaddrResult['SpSiteAddress']['site_email'].'</td>
                </tr>
            </table>
	</td>
    </tr>
    <tr>
	<th colspan="2">&nbsp;</th>
    </tr>';
    /*<tr><th colspan="2" align="left" style="background-color: #C6C6C6"> <b>Service Provider Info</b></th></tr>
    <tr>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="table_label"><strong>Name:</strong></td>
                    <td>'.$spResult['User']['fname'].' '.$spResult['User']['lname'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Address</strong></td>
                    <td>'.$spResult['User']['address'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>City/State/Zip:</strong></td>
                    <td>'.$spCityName.', '.$spResult['State']['name'].' '.$spResult['User']['zip'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Country: </strong></td>
                    <td>'.$spResult['Country']['name'].'</td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="table_label"><strong>Contact: </strong></td>
                    <td>'.$spResult['User']['fname'].' '.$spResult['User']['lname'].'</td>
                </tr>
                <tr>
                    <td class="table_label"><strong>Phone:</strong></td>
                    <td>'.$spResult['User']['phone'].'</td>
                </tr>                               
                <tr>
                    <td class="table_label"><strong>Email: </strong></td>
                    <td>'.$spResult['User']['email'].'</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
	<td colspan="2"></td>
    </tr>*/
$html.='</table>';
echo $html;
?>