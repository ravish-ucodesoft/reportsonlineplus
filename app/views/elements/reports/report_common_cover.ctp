<?php
$html='<table width="100%">                            
            <tr>
                <td colspan="5">
                    <table width="100%">			
                        
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center"><img src="/app/webroot/img/company_logo/'.$spResult['Company']['company_logo'].'"></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:25px;letter-spacing: 4px;"><b>'.$spResult['Company']['name'].'</b></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:21px">';
                                $arr=explode(',',$spResult['Company']['interest_service']);
                                    foreach($arr as $key=>$val):
                                    foreach($reportType as $key1=>$val1):
                                    if($val== $key1)
                                    {
                                        if($key1 == 9){ $html.=  $spResult['Company']['interest_service_other']; }else{ $html.= $val1;} 
                                        $html.= '<span class="red"> * </span>';
                                    }
                                    endforeach;
                                    endforeach;                                                
                        $html.=  '</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:48px;"><b>'.$report_name.'</b></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:48px;"><b>Schedule Frequency: '.$freq.'</b></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" style="font-size:18px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" class="Cpage" >Date:';
                                if($finish_date!= ''){
                                    $html.=  $time->Format('m/d/Y',$finish_date);
                                }
                        $html.='</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" ><i>Prepared for:</i></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$clientResult['User']['client_company_name'].'</td>
                        </tr>	  
                        <tr>
                            <td align="center" class="Cpage" >'.$clientResult['User']['address'].'</td>
                        </tr>
                        <tr>';
                            $cityName = ($clientResult['City']['name']!="")?$clientResult['City']['name']:$clientResult['User']['city'];
                            $html.=  '<td align="center" class="Cpage" >'.$cityName.', '.$this->Common->getStateName($clientResult['User']['state_id']).', '.$this->Common->getCountryName($clientResult['User']['country_id']).'</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$clientResult['User']['zip'].'</td>
                        </tr>                                        
                        <tr>
                            <td align="center" class="Cpage" >&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" ><b>Site Address Info:</b></td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$siteaddrResult['SpSiteAddress']['site_name'].'</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$siteaddrResult['SpSiteAddress']['site_email'].', '.$siteaddrResult['SpSiteAddress']['responsible_contact'].'</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$siteaddrResult['SpSiteAddress']['site_address'].'</td>
                        </tr>
                        <tr>';
                            $siteCity = ($siteaddrResult['City']['name']!="")?$siteaddrResult['City']['name']:$siteaddrResult['SpSiteAddress']['site_city'];
                            $spCity = ($spResult['City']['name']!="")?$spResult['City']['name']:$spResult['User']['city'];
                            $html.=  '<td align="center" class="Cpage" >'.$siteCity.', '.$this->Common->getStateName($siteaddrResult['SpSiteAddress']['state_id']).', '.$this->Common->getCountryName($siteaddrResult['SpSiteAddress']['country_id']).'</td>
                        </tr>
                        <tr>
                            <td align="center" class="Cpage" >'.$siteaddrResult['SpSiteAddress']['site_zip'].'</td>
                        </tr>                                        
                        <tr>
                            <td align="left" class="Cpage" >Prepared By:</td>
                        </tr>
                        <tr>
                            <td align="left" class="Cpage">&nbsp;</td>
                        </tr>  
                        <tr>
                            <td align="left" class="Cpage">'.$spResult['Company']['name'].'</td>
                        </tr>
                        <tr>
                            <td align="left" class="Cpage">'.$spResult['Company']['address'].'</td>
                        </tr>
                        <tr>
                            <td align="left" class="Cpage">'.$spResult['Country']['name'].','.$spResult['State']['name'].','.$spResult['User']['city'].'</td>
                        </tr>
						
						<tr>
                            <td align="left" class="Cpage"><b>Inspector : </b>'.$inspector['User']['fname'].' '.$inspector['User']['lname'].'</td>
                        </tr>
						<tr>
                            <td align="left" class="Cpage">'.$inspector['User']['email'].'</td>
                        </tr>
						<tr>
                            <td align="left" class="Cpage">'.$inspector['User']['phone'].'</td>
                        </tr>
						
						
                        <tr>
                            <td align="left" class="Cpage">&nbsp;</td>
                        </tr>  		      
                        <tr>
                            <td align="left" class="Cpage" style="font-size:11px">&nbsp;</td>
                        </tr>          
                    </table>
                </td>
            </tr>
        </table>';
        echo $html;
        