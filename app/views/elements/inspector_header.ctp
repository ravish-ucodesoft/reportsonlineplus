<?php
  $dashboard=$clientschedule=$vacation=$chapass='';
	 $controller = $this->params['controller'];
	 $action = $this->params['action'];
	 if(($controller == "inspectors") && ($action == "inspector_dashboard") || ($action == "inspector_editinfo")){
	    $dashboard='active';
	  }
	  else if(($controller == "inspectors") && ($action == "inspector_clientandschedule")){
	    //$clientschedule = "class='current'";
	    $clientschedule = "active";
	  }
	  else if(($controller == "inspectors") && ($action == "inspector_changepasswd")){
	    //$chapass = "class='current'";
	    $chapass = "active";
	  }
	  else if(($controller == "inspectors") && ($action == "inspector_vacation")){
	    //$chapass = "class='current'";
	    $vacation = "active";
	  }
	  
?>
<div id="header">
<section class="top_blue_bar"></section> 
     	<div style='padding:0 12px; margin-top:0px; height:100px'>
      <div style="float:left; margin-top:26px; color:#fff"><h1>Inspector Panel</h1></div>
      <div style="float:right;">
        <section class="top_menu">
                   <section class="top_menu_l">
                      <section class="top_menu_r">
                       <h2 style='color:#fff; margin-bottom:10px;'>Inspector Home Page</h2>
                    </section>
                   </section>
        </section>
        <div class="user_info">
         <div class="user_img">
         <h4 style='float:right;color:#fff;margin-top:5px; clear:both'><?php echo $html->image($this->Common->getUserProfilePhoto($inspectorses['User']['profilepic']));?></h4></div>
        <div class="user_txt">
         <h4 style="float:left; color:#c6c6c6; clear:both; padding-bottom:5px;">Welcome <span class="user_name"><?php echo ucwords($name) ;?><?php echo $html->link('Edit info',array('controller'=>'inspectors','action'=>'inspector_editinfo',base64_encode($inspectorses['User']['id'])))?></span></h4>
         <h4 style="float:left; color:#c6c6c6; clear:both">Logged in as <span style='color:#3688B8'><?php echo $inspectorses['User']['email'] ;?></span></h4>
        </div>
     </div>
     </div>
     </div>
    <div id="topmenu">
          <ul class="f-tabs sf-menu">
                <li class="<?php echo $dashboard; ?> ext-tabs">
                  <a href="/inspector/inspectors/dashboard"><span><?php echo $html->image('gnumeric.png'); ?>Dashboard</span></a>               
                </li>
                <li class="<?php echo $clientschedule; ?> ext-tabs"><a href="/inspector/inspectors/schedule"><span><?php echo $html->image('sheduled_task.png'); ?>Schedule Inspection</span></a></li>
                <li class="<?php echo $chapass; ?> ext-tabs"><a href="/inspector/inspectors/changepasswd"><span><?php echo $html->image('change_password.png'); ?>Change Password</span></a></li>
                <li class="<?php echo $vacation; ?> ext-tabs"><a href="/inspector/inspectors/vacation"><span><?php echo $html->image('vacation_icon.png'); ?>Vacation/Disabled</span></a></li>
				<li class="ext-tabs"><a href="/inspector/inspectors/codechart"><span><?php echo $html->image('code_chart.png'); ?>Code Chart</span></a></li>
				
				<li class="ext-tabs"><a href="/inspector/inspectors/logout"><span><?php echo $html->image('def-quote.png'); ?> Logout</span></a></li>
          
		  </ul>
          </div>
   </div>
   <div id="top-panel">
            <div id="panel">
                <ul>
                </ul>
            </div>
   </div>