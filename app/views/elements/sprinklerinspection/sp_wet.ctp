<table>
			    
			    
			 <tr><td  width="20%">  a. No. of systems:	
</td><td ><?php echo $form->input('SprinklerReportWetsystem.wetsystem_a',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td><td >Make & Model :					
</td><td>
         <?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
               echo $form->select('SprinklerReportWetsystem.wetsystem_a_makemodel',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; '));
         ?>
     <?php //echo $form->input('SprinklerReportWetsystem.wetsystem_a_makemodel',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?>
</td><td colspan="1"></td></tr>
				</table>
				<span style="display: block; width: 740px; float: left; margin-right: 80px;"> b. Are cold weather valves in the appropriate open or closed position?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportWetsystem.wetsystem_b',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
                        
                        <span style="display: block; width: 740px; float: left; margin-right: 80px;">If closed, has piping been drained ?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportWetsystem.wetsystem_b_1',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
				
				
				
				<span style="display: block; width: 740px; float: left; margin-right: 80px;"> c. Has the Customer been advised that cold weather valves are not recommended?</span><?php 
			echo $form->select('SprinklerReportWetsystem.wetsystem_c',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span>
			<div style="padding-top: 5px;">
			 <span style="display: block; width:674px; float: left; margin-right: -15px;">   d. Have all the antifreeze systems been tested in the past year?																			
</span> <?php echo $form->input('SprinklerReportWetsystem.wetsystem_d_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 21px;')); ?>
			<?php echo $form->select('SprinklerReportWetsystem.wetsystem_d',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
			</div>
			
					
				<span style="display: block; width: 740px; float: left; margin-right: 80px;">  The antifreeze tests indicated protection to:</span><div style="margin:10px 0px;"></div><br/>
				<span style="display: block; width:674px; float: left; margin-right: -15px;">    system 1																			
</span> <span style="margin-left: -72px;">Temperature</span><?php echo $form->input('SprinklerReportWetsystem.wetsystem_1_temp',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 24px;')); ?>
<?php echo $form->select('SprinklerReportWetsystem.wetsystem_1',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
<span style="display: block; width:674px; float: left; margin-right: -15px;">    system 2																			
</span> <span style="margin-left: -72px;">Temperature</span><?php echo $form->input('SprinklerReportWetsystem.wetsystem_2_temp',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 24px;')); ?>
<?php echo $form->select('SprinklerReportWetsystem.wetsystem_2',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
<span style="display: block; width:674px; float: left; margin-right: -15px;">    system 3																			
</span> <span style="margin-left: -72px;">Temperature</span><?php echo $form->input('SprinklerReportWetsystem.wetsystem_3_temp',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 24px;')); ?>
<?php echo $form->select('SprinklerReportWetsystem.wetsystem_3',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
<span style="display: block; width:674px; float: left; margin-right: -15px;">    system 4																			
</span> <span style="margin-left: -72px;">Temperature</span><?php echo $form->input('SprinklerReportWetsystem.wetsystem_4_temp',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 24px;')); ?>
<?php echo $form->select('SprinklerReportWetsystem.wetsystem_4',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
<span style="display: block; width:674px; float: left; margin-right: -15px;">    system 5																			
</span> <span style="margin-left: -72px;">Temperature</span><?php echo $form->input('SprinklerReportWetsystem.wetsystem_5_temp',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 24px;')); ?>
<?php echo $form->select('SprinklerReportWetsystem.wetsystem_5',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
<span style="display: block; width:674px; float: left; margin-right: -15px;">    system 6																			
</span> <span style="margin-left: -72px">Temperature</span><?php echo $form->input('SprinklerReportWetsystem.wetsystem_6_temp',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 24px;')); ?>
<?php echo $form->select('SprinklerReportWetsystem.wetsystem_6',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
<span style="display: block; width: 740px; float: left; margin-right: 80px;">    e. Did alarm valves, water flow alarm devices and retards test satisfactorily?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportWetsystem.wetsystem_e',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>