<span style="display: block; width: 740px; float: left; margin-right: 80px;"> a. Do sprinklers generally appear to be in good external condition?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportPiping.piping_a',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
				
				<span style="display: block; width: 740px; float: left; margin-right: 80px;">   b. Do sprinklers generally appear to be free of corrosion, paint, or loading and visible obstructions?																								
</span><?php 
			echo $form->select('SprinklerReportPiping.piping_b',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?></span><div style="margin:10px 0px;"></div>
				
				
				<span style="display: block; width: 740px; float: left; margin-right: 80px;"> c. Are extra sprinklers and sprinkler wrench available on the premises?																								
</span><?php 
			echo $form->select('SprinklerReportPiping.piping_c',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?></span><div style="margin:10px 0px;"></div>
			
				
				<span style="display: block; width: 740px; float: left; margin-right: 80px;">  d. Does the exposed exterior condition of piping, drain valves, check valves, hangers, pressure gauges, open sprinklers and strainers appear to be satisfactory?																							
</span><?php 
			echo $form->select('SprinklerReportPiping.piping_d',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?></span><div style="margin:10px 0px;"></div>
			<span style="display: block; width: 740px; float: left; margin-right: 80px;">    e. Does the hand hose on the sprinkler system appear to be in satisfactory condition?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportPiping.piping_e',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
				
				<span style="display: block; width: 740px; float: left; margin-right: 80px;">   f.  Does there appear to be proper clearance between the top of all storage and the sprinkler deflector?																								
</span><?php 
			echo $form->select('SprinklerReportPiping.piping_f',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?></span><div style="margin:10px 0px;"></div>