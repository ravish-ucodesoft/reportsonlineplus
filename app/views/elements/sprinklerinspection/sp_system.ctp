<table>		    <?php    $data_a=array("Yes"=>"Yes","No"=>"No"); ?>
                                    <tr><td  width="20%">  a. No. of systems:</td>
                                        <td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_a',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td>
                                        <td >Make & Model :</td>
                                        <td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_a_makemodel',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td>
                                    </tr>
                                    <tr><td > Type:</td>
                                        <td  colspan="3"><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_a_type',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td></tr>	 
                                        </table>
				<span style="display: block; width: 740px; float: left; margin-right: 80px;">   b. Were valves tested as required?</span><?php ;
				    echo $form->select('SprinklerReportSpecialsystem.specialsystem_b',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
				<span style="display: block; width: 740px; float: left; margin-right: 80px;">  c. Did all heat responsive systems operate satisfactorily?</span><?php 
				    echo $form->select('SprinklerReportSpecialsystem.specialsystem_c',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
                               <span style="display: block; width: 740px; float: left; margin-right: 80px;">  d. Did the supervisory features operate during testing?</span><?php 
			             echo $form->select('SprinklerReportSpecialsystem.specialsystem_d',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
				<table><tr><td>Heat Responsive Devices:	
				    </td><td ><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_d_heat',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td><td >Type	
				    </td><td ><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_d_type',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td>
				<tr>
                                    <td>Type of test</td>
                                    <td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_d_type_test',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td><td colspan="2"></tr><tr><td>Valve No.</td>
                                    <td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_d_valve_1',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td>
                                    <td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_d_valve_2',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td><td colspan="2"></tr>
				<tr><td>Seconds</td><td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_d_second_1',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td><td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_d_second_2',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td><td colspan="2"></tr>
				<tr><td colspan="4"> e. Has a supplemental test form for this system been completed and provided to the customer?     (Please attach)</td></tr>
				
				<tr><td colspan="4" >  Auxiliary equipment: </td></tr>
				<tr><td width="10%">No.</td><td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_e_number',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td></td><td width="10%">Type</td>
                                    <td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_e_type',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td></tr>
				<tr><td width="10%">Location</td><td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_e_location',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td></td>
                                    <td width="10%">Test results</td><td><?php echo $form->input('SprinklerReportSpecialsystem.specialsystem_e_test_result',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td></tr>
				
				</table>
				