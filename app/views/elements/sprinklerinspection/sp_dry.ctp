<table><tr><td  width="20%">  a. No. of systems:	
</td><td ><?php echo $form->input('SprinklerReportDrysystem.drysystem_a',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td><td >Make & Model :	Partial				
</td><td>
<?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_a_makemodel',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?>
<?php //echo $form->input('SprinklerReportDrysystem.drysystem_a_makemodel',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?>
</td><td>	Full			
</td><td><?php echo $form->input('SprinklerReportDrysystem.drysystem_a_full',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td></tr>
				<tr>
						    <td> Date last trip tested:</td><td><?php echo $form->input('SprinklerReportDrysystem.drysystem_a_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td></tr>
				</table> 
				<span style="display: block; width: 740px; float: left; margin-right: 80px;">  b. Are the air pressure(s) and priming water level(s) normal?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_b',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
			<span style="display: block; width: 740px; float: left; margin-right: 80px;">    c. Did the air compressor(s) operate satisfactorily?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_c',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
			<span style="display: block; width: 740px; float: left; margin-right: 80px;">    d. Air compressor(s) oil checked?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_d',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
			
			<span style="display: block; width: 740px; float: left; margin-right: 80px;"> Belt(s)?</span><?php 
			echo $form->select('SprinklerReportDrysystem.drysystem_d_belt',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
			<span style="display: block; width: 740px; float: left; margin-right: 80px;">   e. Were Low Point drains drained during this inspection?</span>
                        <?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_e',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
				
				<table><tr><td  width="20%"> No. of Drains:</td>
                                <td><?php echo $form->input('SprinklerReportDrysystem.drysystem_e_drains',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:125px;')); ?></td>
				<tr>
                                    <td>  Locations:</td></tr>
                                <tr>
                                    <td>1.</td>
                                    <td><?php echo $form->input('SprinklerReportDrysystem.drysystem_e_locations_1',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td>
                                    <td>2.</td><td><?php echo $form->input('SprinklerReportDrysystem.drysystem_e_locations_2',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td>
                                </tr>
				<tr><td>3.</td><td><?php echo $form->input('SprinklerReportDrysystem.drysystem_e_locations_3',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td><td>4.</td>
                                    <td><?php echo $form->input('SprinklerReportDrysystem.drysystem_e_locations_4',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td></tr>
				</table>
		     <span style="display: block; width: 740px; float: left; margin-right: 80px;"> f. Did all quick opening devices operate satisfactorily?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_f',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
				<table>
				    <tr><td>Make & Model:</td><td><?php echo $form->input('SprinklerReportDrysystem.drysystem_f_makemodel',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 130px; margin-right: 3px;')); ?></td></tr>
				    
				</table>
				<span style="display: block; width: 740px; float: left; margin-right: 80px;">      g. Did all the dry valves operate satisfactorily during this inspection?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_g',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
			
			<span style="display: block; width: 740px; float: left; margin-right: 80px;">     h. Is the dry valve house heated?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_h',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
			
			<span style="display: block; width: 740px; float: left; margin-right: 80px;">     i.  Do the dry valves appear to be protected from freezing?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
			echo $form->select('SprinklerReportDrysystem.drysystem_i',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>