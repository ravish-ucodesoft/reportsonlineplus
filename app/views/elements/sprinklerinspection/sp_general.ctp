<div class="lineContainer">
    <span class="general_left">&nbsp;</span>
    <span class="">Select Yes or No</span>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">a.</b> Have there been any changes in the occupancy classification, machinery or operations since the last inspection?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
    echo $form->select('SprinklerReportGeneral.general_a',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">b.</b> Have there been any changes or repairs to the fire protection systems since the last inspection?</span><?php 
    echo $form->select('SprinklerReportGeneral.general_b',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?></span><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">c.</b> If a fire has occurred since the last inspection, have all damaged sprinkler system components been replaced?</span><?php 
    echo $form->select('SprinklerReportGeneral.general_c',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b>If answered "yes" to  a, b or c, list changes in Section 13</b></span><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">d.</b> Has the piping in all dry systems been checked for proper pitch within the past five years?</span><?php
    echo $form->select('SprinklerReportGeneral.general_d',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left">Date last checked:<?php echo $form->input('SprinklerReportGeneral.general_d_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>(check recommended at least every 5 years)</span><?php 
    echo $form->select('SprinklerReportGeneral.general_d_1',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">e.</b> Has the piping in all systems been checked for obstructive materials?</span><?php 
    echo $form->select('SprinklerReportGeneral.general_e',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">			
    <span class="general_left">Date last checked:<?php echo $form->input('SprinklerReportGeneral.general_e_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>(check required at least every 5 years)</span><?php 
    echo $form->select('SprinklerReportGeneral.general_e_1',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">f.</b> Have all fire pumps been tested to full capacity using hose streams or flow meters within the past 12 months?</span>
    <div class=="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_f_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_f',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>    
</div>
<div class="lineContainer">
    <span class="general_left"> <b class="alpha">g.</b> Are gravity, surface or pressure tanks protected from freezing?</span><?php 
    echo $form->select('SprinklerReportGeneral.general_g',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">h.</b> Are sprinklers newer than 50 years old?           QR (20yr)        Dry (10 yr)         >325F/163C (5yr)           Corrosive env't.  (5yr.)					
    </span><?php 
    echo $form->select('SprinklerReportGeneral.general_h',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left">(Testing or replacement required for sprinklers past these age limits.)</span><div style="margin:10px 0px;"></div>
</div>    
<div class="lineContainer">				
    <span class="general_left"><b class="alpha">i.</b> Are extra high temperature solder sprinklers free from regular exposure to temperatures near 300F/149C?</span><?php $data_a=array("Yes"=>"Yes","No"=>"No","Na"=>"N/A");
    echo $form->select('SprinklerReportGeneral.general_i',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">				
    <span class="general_left"><b class="alpha">j. </b> Have gauges been tested, calibrated or replaced in the last 5 years?</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_j_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_j',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>    
<div class="lineContainer">    			
    <span class="general_left"><b class="alpha">k. </b>Alarm valves and associated trim been internally inspected past 5 years?</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_k_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_k',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>    
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">l.</b> Check valves internally inspected in the last 5 years?</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_l_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_l',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">m.</b> Has the private fire main been flow tested in last 5 years?</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_m_date',array('maxLength'=>'30','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_m',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>
<div class="lineContainer">
    <span class="general_left"> <b class="alpha">n.</b> Standpipe 5 and 3 year requirements.</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_n_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_n',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>
<div class="lineContainer">
    <span class="general_left">1. Dry standpipe hydrostatic test</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_n_1_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_n_1',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>
<div class="lineContainer">
    <span  class="general_left">2. Flow test</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_n_2_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_n_2',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>
<div class="lineContainer">
    <span  class="general_left">3. Hose hydrostatic test (5 years from new, every 3 years after)</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_n_3_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_n_3',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>
<div class="lineContainer">
    <span  class="general_left">4. Pressure reducing/control valve test</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_n_4_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_n_4',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>
<div class="lineContainer">
    <span  class="general_left"><b class="alpha"> o.</b> Have pressure reducing/control valves been tested at full flow within the past 5 years?</span>
    <div class="dateSelect">
        <?php echo $form->input('SprinklerReportGeneral.general_o_date',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width:130px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?>
        <?php echo $form->select('SprinklerReportGeneral.general_o',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?></span><div style="margin:10px 0px;"></div>
    </div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">p.</b> Have master pressure reducing/control valves been tested at full flow within the past  1 year?</span><?php 
    echo $form->select('SprinklerReportGeneral.general_p',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?></span><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">q.</b> Have the sprinkler systems been extended to all areas of the building?</span><?php 
    echo $form->select('SprinklerReportGeneral.general_q',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha"> r.</b>  Are the building areas protected by a wet system heated, including its blind attics and perimeter areas?</span><?php 
    echo $form->select('SprinklerReportGeneral.general_r',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px; ')); ?></span><div style="margin:10px 0px;"></div>
</div>
<div class="lineContainer">
    <span class="general_left"><b class="alpha">s. </b> Are all exterior openings protected against the entrance of cold air?</span><?php 
    echo $form->select('SprinklerReportGeneral.general_s',$data_a,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required] general_right','div'=>false,"label"=>false, 'style'=>'width:75px;')); ?><div style="margin:10px 0px;"></div>
</div>    
<style type="text/css">
    span.general_left{
	width: 65%;	  
    }    
    .dateSelect input,.dateSelect img,.dateSelect select{
	float: left;	  
    }
    div.lineContainer{
        float: left;
	width: 100%;
    }
</style>