<section class="inner_r">
    <!-- Questions? -->
    <section class="ques_bg">
         <section class="ques_txt">
                <h1>Do you have questions  about life safety inspections  or Code?</h1>
                <?php echo $html->link('Click here',array('controller'=>'homes','action'=>'codeInspectionQuery'),array('class'=>'clickhere ajax'));?>                
         </section>
    </section>
    
    <!-- Free Site Survey -->
    <section class="survey_bg">
         <section class="survey_txt">
                <h1>Free Site Survey</h1>
                <section class="grey_btn"><a href="<?php echo $html->url(array('controller'=>'users','action'=>'clientsignup'));?>" class="clickhere"><span>Click here</span></a></section>
         </section>
    </section>
    
    <!-- Up to date -->
    <section class="uptodate_bg">
         <section class="uptodate_txt">
                <h1>Are you up to Date?</h1>
                <p>with your Fire Safety</p>
                <section class="red_btn"><a href="<?php echo $html->url(array('controller'=>'homes','action'=>'up_to_date'));?>" class="clickhere"><span>Click here</span></a></section>
         </section>
    </section>
</section>