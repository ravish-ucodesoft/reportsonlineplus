<table width="600px" border="0" cellspacing="2" cellpadding="2">
    <tr>
	<td>	
	Dear [firstname],<br /><br />
We recently received a password reset request for your account.<br /><br />
Your new temporary password is: [password]<br /><br />
Please be sure to update your password when you log into your account.<br /><br />

Thanks,<br />
The SchedulePal Team	
		
	</td>
    </tr>
</table>