<table width="600px" border="0" cellspacing="2" cellpadding="2">
<tr>
<td>
Dear [personname],<br><br>

Your shift offer request has been accepted. Please see the details below:<br><br>

Shift Date: [shiftdate]<br>
Start Time: [starttime]<br>
End Time: [endtime]<br><br>

Assigned to: [assigned_to]<br><br>

Thanks,<br>
The SchedulePal Team

	</td>
    </tr>
</table>