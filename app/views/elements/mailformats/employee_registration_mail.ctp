<table width="600px" border="0" cellspacing="2" cellpadding="2">
    <tr>
	<td>	
	Dear [firstname],<br/><br/>
   This is your employee registration e-mail.<br/><br/>
   Your login/email is: [username]<br/>
   Your password is: [password]<br/><br/>

   <a href="http://[APPLICATION_NAME]/homes/login">Click here to log into your account.</a><br/><br/>

  Thanks,<br/>
  The SchedulePal Team

	</td>
    </tr>
</table>