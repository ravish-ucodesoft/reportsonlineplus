<style>
.error-message{
 background-image: url("/img/negative.gif");
    color: #B86464;
	background-position: left bottom;
    background-repeat: no-repeat;
    display: block;
    float: left;
    font-weight: bold;
    padding: 10px 0 0 19px;
    white-space: nowrap;
    position:absolute;
    
}
</style>
<?php echo $this->Html->script('fckeditor'); ?>

<?php 
	//echo $crumb->getHtml('Add', 'null','') ;
?>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Edit Template</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'editemailtemplate') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <div class="row">
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		      	<?php echo $form->hidden('EmailTemplate.id'); ?>
  		    </span>
  		 </div>	 
		<div class="row">
				<label>Subject : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:500px; margin-bottom:20px">
					<?php echo $form->input('EmailTemplate.mail_subject', array('maxLength'=>'150','class'=>'text',"div"=>false,"label"=>false));  ?>
					</span>
					
				</div>
		</div>
				
		<div class="row">
				<label>Content : </label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:750px;">
					<?php echo $form->input('EmailTemplate.mail_body', array('type'=>'textarea','rows' => 10,'cols' =>23));  
					      echo $fck->load('EmailTemplateMailBody');
					?>
					</span>
				</div>
		</div>
			<!--[if !IE]>start row<![endif]-->
		<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="<?php echo $html->url(array('action'=>'emailtemplist'));?>" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
		</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
