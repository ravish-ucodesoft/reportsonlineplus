<?php 
  echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Email Templates</h2>					
					<div id="product_list_menu">
						<!--<a href="/admin/admins/add" class="update"><span><span><em>Add New</em><strong></strong></span></span></a>-->
					</div>				
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
								<th>No.</th>								
								<th width='40%'>Template For</th>
								<th width='20%'>Mail Subject</th>
								<th width='10%'>Last Edited</th>
								<th width='5%'><a href="#">Action</a></th>
						</tr>
							<?php  if(count($listing)) { ?>
						<tbody>		
							<?php $i=1; foreach ($listing as $data):
						    switch($data['EmailTemplate']['template_for']){
							case "1":
							       $temlate_for = "Account Activation Link On Report Online Plus";
							break;
						        case "5":
							       $temlate_for = "Template for forgot password notification";
							break;
							case "3":
							       $temlate_for = "Template for forward lead request to SP";
							break;
						       
						        case "4":
							       $temlate_for = "Template to send email to client about their account detail";
							break;
							case "9":
							       $temlate_for = "Template for Comment submitted by the Front user on Contact us page";
										 
							break;
						      case "16":
							       $temlate_for = "Template for Fire inspector account deactivation";
							break;
						      case "10":
							       $temlate_for = "Template for Credentials to access admin panel";
							break;
						
						      case "2":
							       $temlate_for = "Template to send email to inspector about their account detail";
							break;
						
							case "11":
							       $temlate_for = "Template to notify Inspectors for inspection Schedule when SP create a new client";
							break;
						
							case "12":
							$temlate_for = "Email notification to administrator when new client registered on the site and directly approch for his inspection";
							break;
						
							case "13":
							$temlate_for = "Email notification to Client and SP when inspector finish the report.";
							break;
							
							case "6":
							$temlate_for = "Email notification to Client about new service call information.";
							break;
						      
						        case "7":
							$temlate_for = "Email notification to Client about auto populate to the call.";
							break;
						      
							case "14":
							$temlate_for = "Email notification to administrator regarding the life safety inspection or code";
							break;
							
							case "15":
							$temlate_for = "Email notification to sp regarding the site address max limit";
							break;
						  
							case "17":
							$temlate_for = "Email notification to Administrator for site address activation";
							break;
						      
						      case "18":
							$temlate_for = "Email notification to client and service provider for the approved service call";
							break;
						      
						      case "19":
							$temlate_for = "Email notification to inspector for the client approval service call";
							break;
						      case "20":
							$temlate_for = "Email notification to service provider about the change in service call schedule from the client";
							break;
						  case "21":
							$temlate_for = "Email notification to service provider if any inspector request for vacation leave";
							break;
						  case "22":
							$temlate_for = "Email notification to client for the quote from Service Provider";
							break;
						  case "23":
						    $temlate_for = "Email notification to service provider for the accepted quote";
						    break;
						  case "24":
						    $temlate_for = "Email notification to service provider for the denied quote";
						    break;
						  case "25":
						    $temlate_for = "Email notification to service provider for the not to fix deficiencies";
						    break;
						  }
							       //$activeImg = $userlist['EmailTemplate']['status']==1 ?'/img/icons/status_star_green.gif' : '/img/icons/status_star_red.gif'; ?>

							<tr class="first">
								<td><?php echo $i; ?>.</td>
								<td><strong><?php echo $temlate_for; ?></strong></td>
								<td><?php echo $data['EmailTemplate']['mail_subject'];?></td>
								<td><?php echo $data['EmailTemplate']['created'];?></td>
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:150px'>			
											<li><?php echo $html->link('Edit',array('action'=>'editemailtemplate',$data['EmailTemplate']['id']),array('class'=>'edit')) ?></li>
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
<?php } else { ?>
	<tr class="first"><td colspan='6'>No Record Found</td> </tr>
<?php } ?>
</table>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->					
					<div class="pagination">
					<span class="page_no">Page <?php echo $paginator->counter(); ?></span>
    			<ul class="pag_list">
    				<li class="prev" style="display:block;" ><?php echo $paginator->prev('Previous'); ?> </li>
    				<li><?php echo $paginator->numbers(); ?></li>
    				<li class="next" ><?php echo $paginator->next('Next'); ?></li>
    			</ul>	
										
					</div>
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->