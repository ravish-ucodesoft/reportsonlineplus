<?php 
	echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
                <div class="title_wrapper">					
                        <h2>Home Page Banner</h2>			
                        <div id="product_list_menu">						
                                <a href="<?php echo $this->Html->url(array('controller' => 'banners', 'action' => 'addbanner','admin'=>true)); ?>" class="update"><span><span><em>Add Banner</em><strong></strong></span></span></a>
                              
                        </div>				
                </div>
				
	<!-- Header pages-->
	
	<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">
					
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
                            <?php echo $this->Form->create('', array('controller'=>'banners','action' => 'bannerlisting','name'=>'galleries','id'=>'galleriesShowForm1') );?>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<th width='5%'><input type="checkbox"  name='checkall1' onclick='checkedAllpage1();' id='checkall1'></th>								
								<th width='30%'>Image</th>
								<th width='20%'>title</th>
								<th width='20%'>Description</th>
								<th width='10%'>Status</th>								
								<th width='5%'>Action</th>
							</tr>
							<?php  if(count($bannerdata)) { ?>
						<tbody>		
							<?php $i=1; foreach ($bannerdata as $page): ?>							
							      <tr class="first">
								<td valign="top"><input type="checkbox" name="box[]" value="<?php echo $page['Banner']['id']; ?>" onclick='uncheck(this);'></td>
								<td valign="top"><?php echo $html->image('banner/'.$page['Banner']['banner_img'],array('width'=>100,'height'=>100)); ?></td>
                                
                                                                
								<td valign="top"><?php echo $page['Banner']['title'];?></td>
								<td valign="top"><?php echo $page['Banner']['description'];?></td>
								
								<td valign="top"><?php echo $page['Banner']['status']=='1'?__('Active'):__('Inactive'); ?></td>
								
								<td valign="top">
									<div class="actions_menu" style='width:100%'>
										<ul style='width:120px'>			
											<li><?php echo $html->link('Edit',array('controller'=>'banners','action'=>'editbanner',$page['Banner']['id']),array('class'=>'edit')) ?></li>
										
										<li><?php echo $html->link('Delete',array('controller'=>'banners','action'=>'delete',$page['Banner']['id']),array('class'=>'delete'),'Are you sure you want to delete this banner?') ?></li>
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>
                        
						</tbody>
                        <table>
                            <tr>
                                <td><?php echo $this->Form->submit("Active",array('id'=>'active','div'=>false,'class'=>'iconlink','name' => 'active',"onClick"=>'return prompt_activepage1();')); ?></td>
                                <td><?php echo $this->Form->submit("Inactive",array('id'=>'Inactive','div'=>false,'class'=>'iconlink','name' => 'inactive',"onClick"=>'return prompt_inactivepage1();')); ?></td>
                                <td><?php //echo $this->Form->submit("Save",array('id'=>'save','div'=>false,'class'=>'iconlink','name' => 'save')); ?></td>
                            </tr>    
                        <table>                       
<?php } else { ?>
	<tr class="first"><td colspan='6'>No Record Found</td> </tr>
<?php } ?>
</table>
                        <?php echo $this->Form->end(); ?>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
	<!--Header pages END-->

			
				<!--[if !IE]>start pagination<![endif]-->					
				
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->
<script>
function prompt_activepage1()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('galleriesShowForm1').elements.length; i++) 
      {
	

	if(document.getElementById('galleriesShowForm1').elements[i].checked == true)
	{
	      flag=1;
	}
	
	
      }
      
      if(flag==1)
      {
	      if(confirm('Are you sure you want to make this image active ?')){
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one image to active");
      return false;
      }
}

function prompt_inactivepage1()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('galleriesShowForm1').elements.length; i++) 
      {
	
	if(document.getElementById('galleriesShowForm1').elements[i].checked == true)
	{
	      flag=1;
	}
	
      }
      
      if(flag==1)
      {
      
	      if(confirm('Are you sure you want to make this image inactive ?')){
		//document.webpages.action = '/navcake/admin/websitepages/inactive';
		//document.webpages.submit;
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one page to inactive");
      return false;
      }
}


function checkedAllpage1()
      {        
        if (document.getElementById('checkall1').checked == false)
        {
        checked = false
        }
        else
        {
        checked = true
        }
        for (var i = 0; i < document.getElementById('galleriesShowForm1').elements.length; i++)
        {
          document.getElementById('galleriesShowForm1').elements[i].checked = checked;
        }
      }

</script> 
                                
                                