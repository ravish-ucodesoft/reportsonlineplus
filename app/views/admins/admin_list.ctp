<?php 
  echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Admin Staff Members (Sub Admins)</h2>					
					<div id="product_list_menu">
						<a href="/admin/admins/add" class="update"><span><span><em>Add New</em><strong></strong></span></span></a>
					</div>				
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
								<th>No.</th>								
								<th width='20%'><?php echo $paginator->sort('First Name','fname');?> </th>
								<th width='20%'><?php echo $paginator->sort('Last Name','lname');?> </th>
								<th width='30%'><?php echo $paginator->sort('Email','email');?> </th>
							  <th width='5%'><a href="#">Status</a></th>
								<th width='15%'>Actions</th>
							</tr>
							<?php  if(count($listing)) { ?>
						<tbody>		
					<?php $i=1; foreach ($listing as $userlist): 							
          	$activeImg = $userlist['Admin']['status']==1 ?'/img/icons/status_star_green.gif' : '/img/icons/status_star_red.gif'; ?>

							<tr class="first">
								<td><?php echo $i; ?>.</td>
								<td><?php echo $userlist['Admin']['fname']; ?></td>
                <td><?php echo $userlist['Admin']['lname']; ?></td>
								<td><?php echo $userlist['Admin']['email']; ?></td>
								<td>
								<?php
									$id='activeImg'.$userlist['Admin']['id'];
								?>
					<div id="<?php echo $id;?>">
					<?php 
						echo $html->link($html->image($activeImg,array('border'=>'0')),'/admin/admins/activate/'.$userlist['Admin']['id']."/".$id,array('update'=>$id,'escape'=>false,'onClick'=>'return status();'),null,null);
								?>
								</div>
								</td>
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:150px'>			
											<li><?php echo $html->link('Edit',array('action'=>'edit',$userlist['Admin']['id']),array('class'=>'edit')) ?></li>
											<!--<li><?php //echo $html->link('Change Password',array('action'=>'changepassword',$userlist['Admin']['id']),array('class'=>'action2')) ?></li>-->
											<li><?php echo $html->link('Delete',array('action'=>'delete',$userlist['Admin']['id']),array('class'=>'delete'),'Are you sure you want to delete this admin staff?') ?></li>
											<li><?php echo $html->link('Resend Credentials',array('action'=>'credentialmail',$userlist['Admin']['id'])) ?></li>
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
<?php } else { ?>
	<tr class="first"><td colspan='6'>There no member</td> </tr>
<?php } ?>
</table>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->					
					<div class="pagination">
					<span class="page_no">Page <?php echo $paginator->counter(); ?></span>
    			<ul class="pag_list">
    				<li class="prev" style="display:block;" ><?php echo $paginator->prev('Previous'); ?> </li>
    				<li><?php echo $paginator->numbers(); ?></li>
    				<li class="next" ><?php echo $paginator->next('Next'); ?></li>
    			</ul>	
										
					</div>
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->