<?php
   // echo $this->Html->css('manager/theme.css');
    echo $this->Html->css('manager/style.css');
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    echo $this->Html->css('reports');
    echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox'));
   
    $FaAlarmDeviceType = '';
    foreach($device_type as $skey=>$delist):
    	$FaAlarmDeviceType .= '<option value="'.$skey.'">'.$delist."</option>"; 
    endforeach;
    
?>

<style>
.summary-tbl2 p {
    float: left;
    margin-bottom: 0;
    margin-left: 5px;
    width: 10%;
    font-size: 10px;
    padding-left: 7px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
.lbtxt{text-align:left; font-weight:bold; width:auto !important; }
.lb1txt{text-align:right;width:20%}
.input-chngs1 input {
    margin-right: 3px;
    width: 136px;
}
</style>
<style>
.tdLabel {font-size:16px;font-weight:bold;text-align:center;}
.brdr {border:2px solid #666666;}
.input-chngs1 input {
    margin-right: 3px;
    width: 296px;
}
.lbl{color:#666666;font-size:11px;text-align:left;}
td{
	padding:4px;
}
.firealramreport td{ border: 1px solid #000000;}
.alramreporttest{text-align:left;background-color:#CCCCCC;}
#accordionlist input {
    
    width: 160px;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	 jQuery(".ajax").colorbox();
});
$(function(){
    // Accordion
    $("#accordionlist").accordion({ header: "h3"});
   
       
});


function AddMore(divid){
	
   var counter = jQuery('#optioncount').val();
    
    counter = parseInt(counter) + 1;
    jQuery('#optioncount').val(counter);
    OptionHtml ='<tr id="option_'+counter+'">';
 
    OptionHtml = OptionHtml+'<td><select name="data[FaLocationDescription]['+divid+']['+counter+'][fa_alarm_device_type_id]" id="FaAlarmDeviceType"><?php echo $FaAlarmDeviceType ; ?></select></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[FaLocationDescription]['+divid+']['+counter+'][zone_address]"  id="test3" style="" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[FaLocationDescription]['+divid+']['+counter+'][location]"  id="test2" style="" ></td>';
    OptionHtml = OptionHtml+'<td><input type="text" name="data[FaLocationDescription]['+divid+']['+counter+'][model]"  id="test1" style="" ></td>';
    OptionHtml = OptionHtml+'<td><select name="data[FaLocationDescription]['+divid+']['+counter+'][physical_condition]" id="physical_condition"><option value="Y">Yes</option><option value="N">No</option></select></td>';
    OptionHtml = OptionHtml+'<td><select name="data[FaLocationDescription]['+divid+']['+counter+'][functional_test]" id="functional_test"><option value="Y">Yes</option><option value="N">No</option></select></td>';
    OptionHtml = OptionHtml+'<td><a href="javascript:void(0)" onclick="RemoveRow('+counter+')"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></td>';    
    OptionHtml = OptionHtml+'</tr>';
    jQuery('#alarm_device_list'+divid).append(OptionHtml);
     
    
}
function RemoveRow(divid){
    jQuery('#option_'+divid).remove();
}



</script>

<section style="">
	<section class="register-wrap">
		<table>
			<tr><td colspan="2"><div class="headingtxt"> Fire Alarm</div></td><tr>
		</table>
		<div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
		 <?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'firealarm','name'=>'specialhazardReport','id'=>'specialhazardReportId','inputDefaults' => array('label' => false,'div' => false))); ?>
 <?php echo $form->input('FirealarmReport.sp_id',array('type'=>'hidden','value'=>$spId)); ?>
 <?php //echo $form->input('FirealarmReport.inspector_id',array('type'=>'hidden','value'=>$sessionInspectorId)); ?>
 <?php echo $form->input('FirealarmReport.servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
 <?php echo $form->input('FirealarmReport.client_id',array('type'=>'hidden','value'=>$clientId)); ?>
 <?php echo $form->input('FirealarmReport.report_id',array('type'=>'hidden','value'=>$reportId)); ?>
		<?php echo $this->Form->hidden('FirealarmReport.optioncount',array('value'=>'1','id'=>'optioncount')); ?>
	<table class="tbl input-chngs1" >
	    <tr>
	      <td>&nbsp;</td>
	      <td><b>Client Info auto-populated</b></td>
	      <td>&nbsp;</td>
	      <td><b>Service Provider Info auto-populated</b></td>
	    </tr>
      
      
      <tr>
	<td>&nbsp;</td>
	<td align="left" style="padding-left:70px;">
	<?php echo $html->image('arrow_down_animated.gif'); ?>
	</td>
	<td>&nbsp;</td>
	<td align="left" style="padding-left:70px;">
	<?php echo $html->image('arrow_down_animated.gif'); ?>
	</td>
      </tr>
      
      <tr>
	<td class="lb1txt">Client Name</td>
	<td><?php echo $form->input('FirealarmReport.client_name',array('value'=>$clientResult['User']['fname'],'readonly'=>true,'label'=>false,'type'=>'text')); ?>
	</td>
	<td class="lb1txt">Service Provider Name</td>
	<td><?php echo $form->input('FirealarmReport.sp_name',array('readonly'=>true,'type'=>'text','div'=>false,"label"=>false,'value'=>ucwords($spResult['User']['fname'].' '.$spResult['User']['lname']))); ?>
	</td>
      </tr>
      
      <tr>
	<td class="lb1txt">Address</td>
	<td><?php echo $form->input('FirealarmReport.client_address',array('value'=>$clientResult['User']['site_address'],'readonly'=>true,'label'=>false,'type'=>'text')); ?>
	</td>
	<td class="lb1txt">Address</td>
	<td><?php echo $form->input('FirealarmReport.sp_address',array('value'=>$spResult['User']['address'],'readonly'=>true,'label'=>false,'type'=>'text')); ?>
	</td>
      </tr>
      
      <tr>
	<td class="lb1txt">Country</td>
	<td><?php echo $form->input('FirealarmReport.country',array('value'=>$this->Common->getCountryName($clientResult['User']['site_country_id']),'readonly'=>true,'type'=>'text','label'=>false)); ?>
	</td>
	<td class="lb1txt">Country</td>
	<td><?php echo $form->input('FirealarmReport.country',array('maxLength'=>'','type'=>'text','div'=>false,"label"=>false,'style'=>'','value'=>$spResult['Country']['name'])); ?>
	</td>
      </tr>
      
      <tr>
	<td class="lb1txt">State</td>
	<td><?php echo $form->input('FirealarmReport.state',array('value'=>$this->Common->getStateName($clientResult['User']['site_state_id']),'readonly'=>true,'type'=>'text','label'=>false,'value'=>$spResult['State']['name'])); ?>
	</td>
	<td class="lb1txt">State</td>
	<td><?php echo $form->input('FirealarmReport.state',array('value'=>'','readonly'=>true,'type'=>'text','label'=>false,'value'=>$spResult['State']['name'])); ?>
	</td>
      </tr>         
     
       <tr>
	<td class="lb1txt">City</td>
	<td><?php echo $form->input('FirealarmReport.city',array('value'=>$clientResult['User']['site_city'],'readonly'=>true,'type'=>'text','label'=>false,)); ?>
	</td>
	<td class="lb1txt">City</td>
	<td><?php echo $form->input('FirealarmReport.city',array('maxLength'=>'','type'=>'text','label'=>false,'value'=>$spResult['User']['city'])); ?>
	</td>
      </tr>
       <tr>
	<td class="lb1txt">Zip</td>
	<td><?php echo $form->input('FirealarmReport.zip',array('value'=>$clientResult['User']['site_zip'],'readonly'=>true,'type'=>'text','label'=>false,)); ?>
	</td>
	<td class="lb1txt">Zip</td>
	<td><?php echo $form->input('FirealarmReport.zip',array('maxLength'=>'','type'=>'text','label'=>false,'value'=>$spResult['User']['zip'])); ?>
	</td>
      </tr>
       <tr>
	<td class="lb1txt">Owner Contact</td>
	<td><?php echo $form->input('FirealarmReport.phone',array('value'=>$clientResult['User']['site_phone'],'readonly'=>true,'type'=>'text','label'=>false,)); ?>
	</td>
	<td class="lb1txt">SP Phone</td>
	<td><?php echo $form->input('FirealarmReport.sp_phone',array('maxLength'=>'','type'=>'text','label'=>false,'value'=>$spResult['User']['phone'])); ?>
	</td>
      </tr>
       <tr>
	<td class="lb1txt">Building</td>
	<td><?php echo $form->input('FirealarmReport.client_building',array('value'=>$clientResult['User']['site_name'],'readonly'=>true,'type'=>'text','label'=>false,)); ?>
	</td>
	<td class="lb1txt">&nbsp;</td>
	<td>&nbsp;
	</td>
      </tr>
    </table>
		
	
	<table style='margin-left:50px; width: 90%;'>
	    <tr>
	    <td class="lb1txt" >&nbsp;</td>  
	</tr>
       
	 <tr> 
	    <td class="lb1txt" style='text-align:left;background-color:#CCCCCC;'>System Function Summary & Test</td>
	    </tr>
       
	 </table>
	
	
	<table style='margin-left:50px;width: 90%;'  class='tbl input-chngs1 firealramreport' >
	    <tr>
		<td>Functions</td>
		<td>Description</td>
		<td>Test Result</td>
	    </tr>
	    <tr>
		<td colspan='3' class='alramreporttest'>Alarm Functions</td>
	    </tr>
	    
	    <tr>
		<td>Alarm Bells/Horns/Strobes<?php echo $form->hidden('FaAlarmFunction.function_name',array('value'=>'Alarm Bells/Horns/Strobes')); ?></td>
		<td><?php echo $form->input('FaAlarmFunction.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmFunction.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	     <tr>
		<td colspan='3' class='alramreporttest'>Alarm Panel Supervisory Functions</td>
	    </tr>
	    
	    <tr>
		<td>Panel AC Power Loss<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.1.function_name',array('value'=>'Panel AC Power Loss')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.1.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.1.result',array('readonly'=>false,'type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	    
	    <tr>
		<td>Panel Sec Power Loss<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.2.function_name',array('value'=>'Panel Sec Power Loss')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.2.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.2.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	    
	    <tr>
		<td>Open Alarm Circuits<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.3.function_name',array('value'=>'Open Alarm Circuits')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.3.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.3.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Short Alarm Circuits<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.4.function_name',array('value'=>'Short Alarm Circuits')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.4.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.4.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    <tr>
		<td>Panel to Panel Circuits<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.5.function_name',array('value'=>'Panel to Panel Circuits')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.5.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAlarmPanelSupervisoryFunction.5.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Ground Faults Detected<?php echo $form->hidden('FaAlarmPanelSupervisoryFunction.6.function_name',array('value'=>'Ground Faults Detected')); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.6.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php echo $form->input('FaAlarmPanelSupervisoryFunction.6.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	     <tr>
		<td colspan='3' class='alramreporttest'>Auxiliary Functions</td>
	    </tr>
	    
	    <tr>
		<td>Remote Annunciator<?php echo $form->hidden('FaAuxiliaryFunction.1.function_name',array('value'=>'Remote Annunciator')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.1.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.1.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	     <tr>
		<td>Stair Pressurization<?php echo $form->hidden('FaAuxiliaryFunction.2.function_name',array('value'=>'Stair Pressurization')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.2.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.2.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	     <tr>
		<td>Elevator Recall<?php echo $form->hidden('FaAuxiliaryFunction.3.function_name',array('value'=>'Elevator Recall')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.3.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.3.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	     <tr>
		<td>Elevator Recall Primary Fl<?php echo $form->hidden('FaAuxiliaryFunction.4.function_name',array('value'=>'Elevator Recall Primary Fl')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.4.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.4.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>HVAC Shutdown<?php echo $form->hidden('FaAuxiliaryFunction.5.function_name',array('value'=>'HVAC Shutdown')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.5.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.5.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    <tr>
		<td>High/Low Air<?php echo $form->hidden('FaAuxiliaryFunction.6.function_name',array('value'=>'High/Low Air')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.6.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.6.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Building Temp<?php echo $form->hidden('FaAuxiliaryFunction.7.function_name',array('value'=>'Building Temp')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.7.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.7.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    <tr>
		<td>Site Water Temp<?php echo $form->hidden('FaAuxiliaryFunction.8.function_name',array('value'=>'Site Water Temp')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.8.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.8.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Site Water Level<?php echo $form->hidden('FaAuxiliaryFunction.9.function_name',array('value'=>'Site Water Level')); ?></td>
		<td><?php echo $form->input('FaAuxiliaryFunction.9.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaAuxiliaryFunction.9.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	     <tr>
		<td colspan='3' class='alramreporttest'>Fire Pump Supervisory Functions</td>
	    </tr>
	    
	     <tr>
		<td>Fire Pump Power<?php echo $form->hidden('FaPumpSupervisoryFunction.function_name.1',array('value'=>'Fire Pump Power')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.1.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.1.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	    
	    <tr>
		<td>Fire Pump Running<?php echo $form->hidden('FaPumpSupervisoryFunction.2.function_name',array('value'=>'Fire Pump Running')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.2.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.2.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	      <tr>
		<td>Fire Pump Phase Reversal<?php echo $form->hidden('FaPumpSupervisoryFunction.3.function_name',array('value'=>'Fire Pump Phase Reversal')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.3.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.3.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>
	      
	     </tr>
	      <tr>
		<td>Fire Pump Auto Position<?php echo $form->hidden('FaPumpSupervisoryFunction.4.function_name',array('value'=>'Fire Pump Auto Position')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.4.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.4.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>  
	    
	    <tr>
		<td>Fire Pump or Pump Controller Trouble<?php echo $form->hidden('FaPumpSupervisoryFunction.5.function_name',array('value'=>'Fire Pump or Pump Controller Trouble')); ?></td>
		<td><?php echo $form->input('FaPumpSupervisoryFunction.5.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaPumpSupervisoryFunction.5.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>   
	     <tr>
		<td colspan='3' class='alramreporttest'>Generator Supervisory Functions</td>
	    </tr>
	     
	      <tr>
		<td>Generator In Auto Position<?php echo $form->hidden('FaGeneratorSupervisoryFunction.function_name.1',array('value'=>'Generator In Auto Position')); ?></td>
		<td><?php echo $form->input('FaGeneratorSupervisoryFunction.1.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaGeneratorSupervisoryFunction.1.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>  
	    
	    <tr>
		<td>Generator or Controller Trouble<?php echo $form->hidden('FaGeneratorSupervisoryFunction.2.function_name',array('value'=>'Generator or Controller Trouble')); ?></td>
		<td><?php echo $form->input('FaGeneratorSupervisoryFunction.2.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaGeneratorSupervisoryFunction.2.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>   
	    <tr>
		<td>Switch Transfer<?php echo $form->hidden('FaGeneratorSupervisoryFunction.3.function_name',array('value'=>'Switch Transfer')); ?></td>
		<td><?php echo $form->input('FaGeneratorSupervisoryFunction.3.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaGeneratorSupervisoryFunction.3.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>  
	    
	    <tr>
		<td>Generator Engine Running<?php echo $form->hidden('FaGeneratorSupervisoryFunction.4.function_name',array('value'=>'Generator Engine Running')); ?></td>
		<td><?php echo $form->input('FaGeneratorSupervisoryFunction.4.description',array('readonly'=>false,'type'=>'text','label'=>false,)); ?></td>
		<td><?php
		    echo $form->input('FaGeneratorSupervisoryFunction.4.result',array('type'=>'select','label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false)); ?></td>
	    </tr>    
	
	 
	</table>
	 
	 <table style='width:90%;margin-left:50px;'>
	    <tr>
		<td><?php echo $html->link('ADD NEW DEVICE LOCATION',array('controller'=>'reports','action'=>'addFireAlarmLoc?reportID='.$reportId.'&clientID='.$clientId.'&spID='.$spId.'&serviceCallID='.$servicecallId),array('escape'=>false,'class'=>'ajax'));?></td>	
	    </tr>    
	 </table>
	    <div id="accordionlist" style='width:90%;margin-left:50px;'>
	    <?php foreach($device_place as $key=>$deviceplace): ?>
	   
		
		<div>
		<h3><a href="#"><?php echo $deviceplace .$form->hidden('FaLocationDescription.'.$key.'.1.location_id',array('value'=>$key)); ?></a></h3>
		
		<table style='width:100%;margin-bottom:10px;' class='tbl input-chngs1 firealramreport' id='alarm_device_list<?php echo $key; ?>' >
		<tr>
		<td colspan='7' class='alramreporttest'>Alarm Device List</td>
	    </tr>
	 
	    <tr>
		<td>Device Type</td>
		<td>Zone/ Address</td>
		<td>Location/Description</td>
		<td>Manufacturer Model</td>
		<td>Physical Condition</td>
		<td>Functional Test</td>
		<td>&nbsp;</td>
	    </tr>
		<tr>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.fa_alarm_device_type_id',array('type'=>'select','div'=>false,'label'=>false,'options'=>$device_type,'empty'=>false));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.zone_address',array('type'=>'text','div'=>false,'label'=>false));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.location',array('type'=>'text','div'=>false,'label'=>false));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.model',array('type'=>'text','div'=>false,'label'=>false));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.physical_condition',array('type'=>'select','div'=>false,'label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false));?></td>
		    <td><?php echo $form->input('FaLocationDescription.'.$key.'.1.functional_test',array('type'=>'select','div'=>false,'label'=>false,'options'=>array('Y'=>'Yes','N'=>'No'),'empty'=>false));?></td>
		    <td>&nbsp;</td>
		</tr>
	    
		<tr>
		    <td colspan='7' style='text-align:right;'><?php echo $html->link('ADD MORE','javascript:void(0);',array('escape'=>false,'onclick'=>"AddMore('".$key."');"));?></td>
		</tr>
	     
	     </table>
	    </div>
	    <?php endforeach; ?>
		
	    </div>
	</section>
</section>