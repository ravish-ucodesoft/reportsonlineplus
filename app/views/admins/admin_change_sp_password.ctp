<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#changepwdform").validationEngine();
});
</script>
<div id="content">
    	
     <div id="box" style="width:500px;padding-left:10px;">
        <?php echo $form->create('', array('type'=>'POST', 'url'=>'changeSpPassword' ,'name'=>'addform','id'=>'changepwdform', 'class'=>'search_form general_form')); ?>
        <?php echo $form->input('User.id', array('maxLength'=>'40','class'=>'text','value'=>$id));  ?>  
               <h3 id="adduser">Change Password</h3>
                   <br/>
                        <table class="tbl">
                        <tr>
                            <td>New Password:</td>
                            <td><?php echo $form->password('User.password', array('maxLength'=>'30','class'=>'text','class'=>'validate[required]')); ?></td>
                        </tr>
                        <tr>
                            <td>Confirm Password:</td>
                            <td><?php echo $form->password('User.cpwd', array('maxLength'=>'30','id'=>'password','class'=>'text','class'=>'validate[required,equals[password]]'));  ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div align="left">
                                    <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
                                </div>
                            </td>
                        </tr>
                        </table>
        <?php echo $form->end(); ?>
       </div>
</div>
