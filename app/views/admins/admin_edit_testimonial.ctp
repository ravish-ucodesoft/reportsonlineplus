<div class="section">	
	<div class="title_wrapper">
            <?php echo "<h2>Edit Testimonial</h2>"; ?>
	</div>
        <div class="section_inner">
            <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'edit_testimonial/'.$testimonial[0]['Testimonials']['id']) ,'name'=>'addform','id'=>'addtestimonial', 'class'=>'search_form general_form','enctype' => 'multipart/form-data')); ?>
            <fieldset>
                <div class="forms">
                    <div class="row">
                        <span style="float:right">
                          [<span class="star">*</span>] marked fields are mandatory.
                        </span>
  		    </div>
                    <div class="row">
				<label style="width:200px;">Image Upload(84 x 84): <span class="star">*</span></label>
				<div class="inputs" style='float:left;' >
					<span class="input_wrapper">
					<?php echo $form->input('Testimonials.image', array('type'=>'file','class'=>'text','label'=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Testimonials.image'); ?></span>
				</div>
		    </div>
                    <div class="row" style='margin-left:200px;'>
				<?php echo $html->image('testimonial/'.$testimonial[0]['Testimonials']['image'],array('width'=>100, 'height'=>100,'alt'=>''));?>
				<?php echo $form->hidden('Testimonial.prev_image',array('value'=>$testimonial[0]['Testimonials']['image']));?>
                    </div>    
                    <div class="row">
				<label style="width:200px;">Testimonial  : <span class="star">*</span></label>
				<div class="inputs" style='float:left;' >
					<span class="input_wrapper" style="width:592px;" >
                                            <?php echo $form->input('Testimonials.body', array('type'=>'textarea','value'=>$testimonial[0]['Testimonials']['body'],'rows' => 10,'cols' =>80,'label'=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Testimonial.body'); ?></span>
				</div>
		    </div>
                    <div class="row">
				<label style="width:200px;">from  : <span class="star">*</span></label>
				<div class="inputs" style='float:left;' >
					<span class="input_wrapper">
                                            <?php echo $form->input('Testimonials.from', array('type'=>'text','class'=>'text','value'=>$testimonial[0]['Testimonials']['from'],'label'=>false));  ?>
                                            <?php echo $form->hidden('Testimonials.id', array('class'=>'text','value'=>$testimonial[0]['Testimonials']['id'],'label'=>false));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Testimonial.from'); ?></span>
				</div>
		    </div>
                    <div class="row" style='margin-left:198px;' >
				<div class="inputs" style='float:left;'>
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
                                        <span class="button red_button"><a href="<?php echo $html->url(array('controller'=>'admins','action'=>'view_testimonials','admin'=>true));?>" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
		    </div>
                </div>    
            </fieldset>
            <?php echo $form->end(); ?>
        </div>    
</div>        