<?php 
	echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Site Address Activation Request</h2>									
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<th>No.</th>								
								<th width='20%'>Request From (SP)</th>
								<th width='40%'>Site Address Request</th>
                                <th width='40%'>Action</th>
							</tr>
							<?php  if(count($request)) { ?>
						<tbody>		
							<?php $i=1; foreach ($request as $request): ?>							
							      <tr class="first">
								<td valign="top"><?php echo $i; ?>.</td>
								<td valign="top"><?php echo $this->Common->getClientName($request['SpSiteAddress']['sp_id']);?></td>
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:250px'>			
											<li>Client Name:<?php echo $this->Common->getClientName($request['SpSiteAddress']['client_id']);?></li>
                                            <li>Client Company:<?php echo $this->Common->getClientCompanyName($request['SpSiteAddress']['client_id']);?></li>
                                            <li>Site Name:<?php echo $request['SpSiteAddress']['site_name'];?></li>
                                            <li>Site Address:<?php echo $request['SpSiteAddress']['site_address'];?></li>
                                             <li><?php echo $request['SpSiteAddress']['site_city'];?>,<?php echo $request['State']['name'];?>&nbsp;<?php echo $request['SpSiteAddress']['site_zip'];?></li>
                                            <li><?php echo $request['Country']['display_name'];?></li>
                                        </ul>
									</div>
								</td>
                                <td valign="top">
                                    <ul style='width:150px'>			
											<li><?php echo $html->link('Accept Request',array('controller'=>'admins','action'=>'siteaddr_accept_request',$request['SpSiteAddress']['id']),array('class'=>'','confirm'=>'Are you sure to activate this site address for SP?')) ?></li>
                                            <!--<li><?php echo $html->link('Decline Request',array('controller'=>'admins','action'=>'siteaddr_decline_request',$request['SpSiteAddress']['id']),array('class'=>'','confirm'=>'Are you sure to decline the site address for SP?')) ?></li>-->
                                    </ul>
                                </td>
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
<?php } else { ?>
	<tr class="first"><td colspan='6' align='center'>No Record Found</td> </tr>
<?php } ?>
</table>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->					
				
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->