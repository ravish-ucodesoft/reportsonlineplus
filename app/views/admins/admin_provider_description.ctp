<script type="text/javascript">
function showhide(obj)
{
	 var el = document.getElementById(obj);
	if ( el.style.display != 'none' ) {
		el.style.display = 'none';
	}
	else {
		el.style.display = '';
	}
}
</script>

<div id="content">
     <div id="box" style="width:800px;">
	 <div style="padding: 0 0 8px 10px;"><?php echo $html->image($this->Common->getUserProfilePhoto($providerListing['User']['profilepic']),array('style'=>'vertical-align:middle')); ?></div>
               <h3 id="adduser"><?php echo ucfirst($providerListing['User']['fname'])." (".$providerListing['User']['lname'].")"; ?></h3>
                   <br/>
		   
                        <table class="tbl" cellpadding="3" cellspacing="3">
                        <tr>
                       	<td >First Name </td><td>:</td><td ><?php echo $providerListing['User']['fname'];  ?></td></tr><tr>	<td >Last Name </td><td>:</td><td ><?php echo $providerListing['User']['lname'];  ?></td>          
                        </tr>
			
			<tr>
                       	<td >Email </td><td>:</td><td ><?php echo $providerListing['User']['email'];  ?></td> </tr>
                        </tr>
			</table>
			<br/>
			<b><ul>Company Detail</ul></b>
                   
		   
		   <table class="tbl" cellpadding="3" cellspacing="3">
                        <tr>
                       	<td >Company Name </td><td>:</td><td ><?php echo $providerListing['Company']['name'];  ?></td></tr><tr>	<td >Address </td><td>:</td><td ><?php echo $providerListing['Company']['address'];  ?></td>          
                        </tr>
			
			<tr>
                       	<td >Phone </td><td>:</td><td ><?php echo $providerListing['Company']['phone'];  ?></td> </tr>
			<tr>
                       	<td >Website </td><td>:</td><td ><?php echo $providerListing['Company']['website'];  ?></td> </tr>
			<tr>
                       	<td >Site Manager </td><td>:</td><td ><?php echo $providerListing['Company']['site_manager'];  ?></td> </tr>
			<tr>
                       	<td > Site Manager Phone </td><td>:</td><td ><?php echo $providerListing['Company']['phone_site_manager'];  ?></td> </tr>
			<tr>
                       	<td >Site Manager Email  </td><td>:</td><td ><?php echo $providerListing['Company']['email_site_manager'];  ?></td> </tr>
                        </tr>
			<tr>
                       	<td >Company Biodata </td><td>:</td><td ><p><?php echo $providerListing['Company']['company_bio'];  ?></p></td></tr><tr>	<td >Address </td><td>:</td><td ><?php echo $providerListing['Company']['address'];  ?></td>          
                        </tr>
			<tr><td colspan="2"><b>Company Principals</b></td></tr>
			<?php if(!empty($providerListing['Company']['UserCompanyPrincipalRecord'])){ foreach($providerListing['Company']['UserCompanyPrincipalRecord'] as $key=>$val){ ?>
			<tr><td><?php echo $name = $this->Common->getPrincipalName($val['user_company_principal_id']); ?></td><td><?php echo $val['manager_name']." ".$val['manager_email']; ?></td></tr>
			<?php } }?>
			
                        </table>
		   
		   <br/>
			<b><ul><a href="javascript:void(0)" onclick="showhide('inspector')">Inspectors(<?php echo count($inspection)?>)</a></ul></b>
                   
		   
		   <table class="tbl" id="inspector" style="display:none;" cellpadding="3" cellspacing="3">
		    <?php  $i=1; foreach ($inspection as  $val) {  //pr($val);  ?>
		    <tr><td><b>Inspector <?php echo $i ?></b></td><td>:</td><td><?php echo $html->image($this->Common->getUserProfilePhoto($val['User']['profilepic']),array('style'=>'vertical-align:middle')); ?></td></tr>
		    <tr><td>First Name</td><td>:</td><td><?php  echo $val['User']['fname']  ?></td></tr><tr><td>Last Name</td><td>:</td><td><?php echo $val['User']['lname']   ?></td></tr><tr><td>Qualification</td><td>:</td><td valign="top"><?php 
            $servid = explode(",",$val['User']['report_id']);
            foreach($servid as $value){       
              echo $this->Common->getReportName($value).","; ?><?php 
            } ?></td></tr><tr><td>Phone</td><td>:</td><td><?php echo $val['User']['phone'] ?></td></tr><tr><td>Email</td><td>:</td><td><?php echo $val['User']['email'] ?></td></tr>
		   
                   <?php $i++;   }  ?>
		   </table>
		   
		   
		    <br/>
		    <b><ul><a href="javascript:void(0)" onclick="showhide('client')">Clients Detail(<?php echo count($client_profile)?>)</a></ul></b>
                   
		   
		   <table class="tbl" id="client" style="display:none;" cellpadding="3" cellspacing="3">
		    <?php  $cp=1; foreach ($client_profile as  $client_profile_data) {     ?>
		    <tr><td><b>Client <?php echo $cp ?></b></td>
			 <td>:</td>
			 <td><?php echo $html->image($this->Common->getUserProfilePhoto($client_profile_data['User']['profilepic']),array('style'=>'vertical-align:middle')); ?></td>
		    </tr>
		    <tr><td>First Name</td>
			 <td>:</td>
			 <td><?php  echo $client_profile_data['User']['fname']  ?></td>
		    </tr>
		    <tr>
			 <td>Last Name</td>
			 <td>:</td>
			 <td><?php echo $client_profile_data['User']['lname'] ?></td></tr>
		   
		    <tr>
			 <td>Phone</td>
			 <td>:</td>
			 <td><?php echo $client_profile_data['User']['phone'] ?></td>
		    </tr>
		    <tr>
			 <td>Email</td>
			 <td>:</td>
			 <td><?php echo $client_profile_data['User']['email'] ?></td>
		    </tr>
		   
		    <tr>
			 <td>Client Company Name</td>
			 <td>:</td>
			 <td><?php echo $client_profile_data['User']['client_company_name'] ?></td>
		    </tr>
		     
		    
                   <?php $cp++;   }  ?>
		   </table>
			
       </div>
</div>