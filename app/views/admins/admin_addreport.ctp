<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Add Sample Reports</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addreport') ,'name'=>'addreport','id'=>'addform','enctype' => 'multipart/form-data')); ?>
	
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <table width="100%">
			<tr><td>First Report<span class="star">*</span></td>
			<td><?php echo $this->Form->input('Report.firealarm_report.rname',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			<td><?php echo $this->Form->input('Report.firealarm_report',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			</tr>
			
			<tr><td>Second Report<span class="star">*</span></td>
			<td><?php echo $this->Form->input('Report.sprinkler_report.rname',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			<td><?php echo $this->Form->input('Report.sprinkler_report',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			</tr>
			
			<tr><td>Third Report<span class="star">*</span></td>
			<td><?php echo $this->Form->input('Report.kitchen_report.rname',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			<td><?php echo $this->Form->input('Report.kitchen_report',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			</tr>
			
			<tr><td>fourth Report<span class="star">*</span></td>
			<td><?php echo $this->Form->input('Report.emergency_report.rname',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			<td><?php echo $this->Form->input('Report.emergency_report',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			</tr>
			
			<tr><td>fivth Report<span class="star">*</span></td>
			<td><?php echo $this->Form->input('Report.extinguishers_report.rname',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			<td><?php echo $this->Form->input('Report.extinguishers_report',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			</tr>
			
			<tr><td>sixth Report<span class="star">*</span></td>
			<td><?php echo $this->Form->input('Report.specialhazard_report.rname',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			<td><?php echo $this->Form->input('Report.specialhazard_report',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			</tr>
			<tr><td>&nbsp;</td>
			<td colspan="2" style="color:red;">Only PDF,XLS,DOCX extentions are allowed</td>
			</tr>
			
			<tr><td>&nbsp;</td>
			<td colspan="2">Please Upload your water mark sample reports,that is download able for the frontend users</td>
			</tr>
		</table>
			<!--[if !IE]>start row<![endif]-->
		<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save');  ?></span>
    			<!--span class="button red_button"><a href="/wellnesscenter1/websitepages/pagelisting" ><span><span style="color:white">Cancel</span></span></a></span-->
			<span class="button red_button"><a href="javascript:history.go(-1);" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
		</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
