<div class="section">
			<div class='message'><?php echo $this->Session->flash();?></div>
                        <div class="title_wrapper">					
					<h2>Testimonials List</h2>
                                        <div id="product_list_menu">
						<a href="/admin/admins/add_testimonial" class="update"><span><span><em>Add New</em><strong></strong></span></span></a>
					</div>
			</div>
                        <div class="section_inner" >
                            <div  id="testimonial_list">
                                <div class="table_wrapper">
                                <table cellpadding="0" cellspacing="0" width="100%" >
                                    <tr>
                                        <th rowspan="3" width='10%' style="color: #4A6300;" >Profile Pic</th>
                                        <th rowspan="3" width='30%' style="color: #4A6300;" >Testimonial</th>
                                        <th rowspan="3" width='15%' style="color: #4A6300;" >From</th>
                                        <th rowspan="3" width='10%' style="color: #4A6300;" >Actions</th>
                                    </tr>
                                    <tbody>
                                    <?php foreach($testimonials as $testval) { ?>
                                     <tr>
                                        <td><img width="100" height="100" alt="" src="/img/testimonial/<?php echo $testval['Testimonials']['image']; ?>"> </td>
                                        <td><?php echo $testval['Testimonials']['body']; ?> </td>
                                        <td><?php echo $testval['Testimonials']['from']; ?> </td>
                                        <td><div class="actions_menu" style='width:100%'>
                                                    <ul style='width:120px'>			
                                                            <li><?php echo $html->link('Edit',array('controller'=>'admins','action'=>'edit_testimonial',$testval['Testimonials']['id']),array('class'=>'edit')) ?></li>
							    <li><?php echo $html->link('Delete',array('action'=>'deleteTestimonial',$testval['Testimonials']['id']),array('class'=>'delete'),'Are you sure you want to delete this testimonial?') ?></li>
                                                    </ul>
                                            </div>
                                        </td>
                                      </tr>   
                                    <?php } ?> 
                                    <tbody>    
                                </table>
                                </div>
                            </div>    
                        </div>    
</div>