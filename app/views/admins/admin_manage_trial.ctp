<style>
.error-message{
	position:relative;color:#B86464;	
}
</style>
<?php echo $this->Html->script('fckeditor'); ?>

<?php 
	//echo $crumb->getHtml('Add', 'null','') ;
?>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Manage Trial period</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('controller'=>'admins','action'=>'manageTrial') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
		<?php echo $form->hidden('TrialOption.id',array('value'=>$this->data['TrialOption']['id'])); ?></php?>
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <div class="row">
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		    </span>
  		 </div>	 

		<div class="row">
				<label>Trial Status : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					
					<?php echo $this->Form->radio('TrialOption.trial_status',array('Off'=>'OFF','On'=>'ON'),array('id'=>'radiobtn','class'=>'','legend'=>false,'label'=>false,'div' => false,'default'=>'F')); ?>	
					<span class="system negative" id="err1"><?php echo $form->error('Websitepage.page_type'); ?></span>
				</div>
		</div>
	
			<!--[if !IE]>start row<![endif]-->
		<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="index" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
		</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
