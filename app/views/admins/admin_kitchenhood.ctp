<?php
	echo $this->Html->css('manager/theme.css');
	echo $this->Html->css('manager/style.css');
	echo $this->Html->script('jquery.1.6.1.min');   
	echo $this->Html->css('validationEngine.jquery');
	echo $this->Html->css(array('ajaxuploader/fileuploader'));
	
	echo $this->Html->script('languages/jquery.validationEngine-en.js');
	echo $this->Html->script('jquery.validationEngine.js');
	
	echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
	echo $this->Html->script(array('ajaxuploader/fileuploader'));
	echo $this->Html->script(array('ui/ui.core.js','ui/ui.datepicker.js'))
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript">
$(document).ready(function(){
	//jQuery(".ajax").colorbox();
	jQuery(".inline").colorbox({inline:true, width:"50%"});
	jQuery("#kitchenreport").validationEngine();

});
</script>
<script type="text/javascript">

function createUploader(){	    
         var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
	    }
            });           
    }        
window.onload = createUploader;
 function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
/*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

 jQuery(function() {
        jQuery(".calender").datepicker({ dateFormat: 'mm/dd/yy',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });});

function RemoveAttachment(attachId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delkitchenhoodattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}
 

 function checkUploadedAttachment(){
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
  function checkNotification(){
	var notifyVal = jQuery("#ExtinguisherReportNotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }

function confirmattach(attachId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId);    
  }
  else{
    return false;
  }
 }
function RemoveNotifier(notifierId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delkitchenhoodnotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#pnotify_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId);    
  }
  else{
    return false;
  }
 } 
</script> 
<style>
.message{text-align:center;color:#54A41A;font-weight:bold;}
.headingtxt {
    background-color: #2B92DD;
    color: #FFFFFF;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    margin-bottom: 10px;
    padding: 5px;
    width: 100%;
}
/*----------- 30052012---------------------*/
.input-chngs input{ background: #ebebeb; /* Old browsers */
background: -moz-linear-gradient(top, #ebebeb 0%, #ffffff 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ebebeb), color-stop(100%,#ffffff)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* IE10+ */
background: linear-gradient(top, #ebebeb 0%,#ffffff 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebebeb', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
border:1px solid #bbb; width:66%; margin:0 30% 0 0;
}
.input-chngs td { color:#666;}

table td { border:none;}
#box .blue-hdng { background: none repeat scroll 0 0 #F4FAFF; border:none;
    border-bottom: 1px solid #AECEEF;
    color: #2281D4;
    font-size: 16px; font-weight:normal;
}
</style>
<div id="content" >
	      <div class='message'><?php echo $this->Session->flash();?></div>
              <div id="box">
              <div class="headingtxt">Kitchenhood</div>
				 
		
  <?php echo $this->element('reports/kitchenhood_form'); ?>
										  <tr>      
										    <td>
										      &nbsp;
										  </td>
										  </tr>
			</table>
                    	
                </div>
</div>
    <!-- This contains the hidden content for inline calls -->
	