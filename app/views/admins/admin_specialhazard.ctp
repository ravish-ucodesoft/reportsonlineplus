<?php
    echo $this->Html->css('manager/theme.css');
    echo $this->Html->css('manager/style.css');
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    echo $this->Html->css('reports');
    echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox'));
?>
<style>
.summary-tbl2 p {
    float: left;
    margin-bottom: 0;
    margin-left: 5px;
    width: 10%;
    font-size: 10px;
    padding-left: 7px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
.lbtxt{text-align:right;width:20%}
.input-chngs1 input {
    margin-right: 3px;
    width: 136px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
jQuery(".inline").colorbox({inline:true, width:"50%"});
	loadDatePicker();
	jQuery("#EmergencyexitReportId").validationEngine();
	jQuery(".ajax").colorbox();
	<?php
	
	 if(isset($this->data['SpecialhazardReport']['finish_date']) && ($this->data['SpecialhazardReport']['finish_date'] != "")){ ?>
	   
	    for(i = 0; i < document.getElementById('specialhazardReportId').length; i++){
		var formElement = document.getElementById('specialhazardReportId').elements[i];	
		    formElement.disabled = true;
	    }
	    
            <?php } ?>
});

 function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
function loadDatePicker(){
        jQuery(".calender").datepicker({ dateFormat: 'mm/dd/yy',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });
}

function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
	    }
            });           
}        
window.onload = createUploader;

 function checkUploadedAttachment(){
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
  function checkNotification(){
	var notifyVal = jQuery("#SpecialhazardReportNotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }

function RemoveAttachment(attachId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delspecialhzardattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
			//alert('#p_'+attachId);
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmattach(attachId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId);    
  }
  else{
    return false;
  }
}

function RemoveNotifier(notifierId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delspecialhzardnotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#pnotify_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId);    
  }
  else{
    return false;
  }
 }
 function RemoveRecord(rId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delexitrecord/" + rId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#LI_'+rId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmbox(rId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveRecord(rId);    
  }
  else{
    return false;
  }
 }
 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}


</script>
<style>
.tdLabel {font-size:16px;font-weight:bold;text-align:center;}
.brdr {border:2px solid #666666;}
.input-chngs1 input {
    margin-right: 3px;
    width: 296px;
}
.lbl{color:#666666;font-size:11px;text-align:left;}
</style>


 
<div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
<section style="min-width:980px">
<section class="register-wrap">
<div class="headingtxt"> SPECIAL HAZARD</div>
<?php echo $form->create('Reports', array('type'=>'POST', 'action'=>'specialhazard','name'=>'specialhazardReport','id'=>'specialhazardReportId','inputDefaults' => array('label' => false,'div' => false))); ?>
 <!-- Element inlcuded -->
 
   <?php echo $this->element('reports/special_hazard_form'); ?>
   
    <!-- Element inlcuded -->
    <tr><td align="center">&nbsp;</td></tr>
    
  </table>  
</section>
</section>
