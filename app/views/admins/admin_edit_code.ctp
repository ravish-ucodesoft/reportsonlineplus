<?php echo $this->Html->script('fckeditor'); ?>
<?php 
 		//echo $crumb->getHtml('Add', 'null','') ;
	?>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Edit Code Information</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'editCode') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
		<?php echo $form->hidden('Code.id'); ?>
    <!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms">
	
  		 <div class="row">
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		    </span>
  		 </div>	 
				<div class="row">
				<label>Report : <span class="star">*</span></label>
				<div class="inputs">
					<span class="">
					<?php echo $form->select('Code.report_id',$options,$this->data['Code']['report_id'],array('legend'=>false,'label'=>false));?>
					</span>
					
					<span style="float:right" class="system negative" id="err1"><?php echo $form->error('Code.report_id'); ?></span>
				</div>
			</div>
				
				<div class="row">
				<label>Code : </label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Code.code', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					
					<span class="system negative" id="err1"><?php echo $form->error('Code.code'); ?></span>
				</div>
			</div>
			<div class="row">
				<label>Type : <span class="star">*</span> </label>
				<div class="inputs">
					<span class="">
					<?php foreach($codetype as $type){
					$option[$type['CodeType']['id']]= $type['CodeType']['code_type']; ?>
						<?php	}  ?>
					<?php echo $form->radio('Code.code_type_id', $option,array('div'=>false,'label'=>false,'legend'=>false));  ?>
			
          </span>
					<span class="system negative" id="err1"><?php echo $form->error('Code.code_type_id'); ?></span>
				</div>
			</div>
			
		<div class="row">
				<label>Description : <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper" style="width:500px;">
					<?php echo $form->input('Code.description', array('type'=>'textarea','rows' => 10,'cols' =>23,'maxLength'=>'50','class'=>'text'));  
         echo $fck->load('CodeDescription','Basic'); ?>
					</span>
					
					<span class="system negative" id="err1"><?php echo $form->error('Code.description'); ?></span>
				</div>
			</div>	
		
			<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="/admin/admins/codeListing" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
			</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
