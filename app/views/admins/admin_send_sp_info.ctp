<?php 
    echo $this->Html->script('jquery.1.6.1.min.js');
    echo $this->Html->css('style.css'); 
?>

<?php echo $this->Html->script(array('validation/jquery.validate','validation/jquery.validation.functions')); // FOR validations ?>
<script type='text/Javascript'>
jQuery(document).ready(function() {
    jQuery('#composeMszForm').validate();
});
    
</script>

<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;     
}
#box {
    border: none;
}
.error{
color:red;
}

</style>
<div id="content">
     <div id="box" style="width:700px;padding-left:10px;">
      <?php echo $form->create('',array('controller'=>'admins','action' => 'sendSpInfo','method'=>'post','id'=>'composeMszForm','name'=>'ListForm'));
                echo $form->input('Message.receiver_id',array('type' => 'hidden','value'=>$clientId));	             
                echo $form->input('Message.sender_id',array('type' => 'hidden','value'=>$session->read('Spid'))); 
      ?>
     <div class="form-box" style="width:90%">
     <?php $client_name = $this->Common->getclientName($clientId); ?>
        <h3 id="adduser"> <?php echo $client_name; ?> would like ReportOnlinePlus to send him the information about <br/>qualified service providers that can perform the work listed</h3>
        <table class="register-form">
        <tr>
	    <td style="vertical-align:top;"> <label>To:<span style="color:red">*</span></label></td>
	    <td><p><span class="input-field"><?php echo $form->input('Message.message_to',array('size'=>'45','maxlength'=>'100','readonly'=>true,'div'=>false,'label'=>false,'value'=>$data['User']['email'],'class'=>'required')); ?></span></p></td>
			
        </tr>
         <tr>
	    <td style="vertical-align:top;"> <label>Email Subject:<span style="color:red">*</span></label></td>
	    <td> <p><span class="input-field"><?php echo $form->input('Message.message_subject',array('size'=>'45','maxlength'=>'100','div'=>false,'label'=>false,'value'=>'','class'=>'required')); ?></span></p></td>
			
        </tr>
        <tr>
	    <td style="vertical-align:top;"><label>Email Body:<span style="color:red">*</span></label></td>
	    <td> <p><span class=""><?php echo $form->input('Message.message_body',array('type'=>'textarea','rows'=>7,'cols'=>59,'label'=>false,'div'=>false,'id'=>'ContentMessage','class'=>'required')); ?></span></p></td>
			
        </tr>
        <tr>
            <td colspan="2"><?php echo $form->submit('Send',array('id'=>'','div'=>false,'class'=>'','style'=>'float:right;')); ?></td>
        </tr>
	
    	
        </table>
    </div>
    <!--one complete form ends-->

  <?php echo $form->end(); ?>
</div>
</div>
