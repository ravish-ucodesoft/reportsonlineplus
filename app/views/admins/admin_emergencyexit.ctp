<?php
    //echo $this->Html->css('manager/theme.css');
    echo $this->Html->css('manager/style.css');
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    echo $this->Html->css('reports');
    echo $this->Html->css(array('gallery/colorbox'));
    echo $this->Html->script(array('colorbox/jquery.colorbox'));
?>
<style>
.summary-tbl2 p {
    float: left;
    margin-bottom: 0;
    margin-left: 5px;
    width: 10%;
    font-size: 10px;
    padding-left: 7px;
}
.headingtxt{
    background-color: #2B92DD;
    color: #fff;
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 14px;
    padding: 5px;
    width: 100%;
    margin-bottom:10px;
}
.lbtxt{text-align:right;width:20%}
.input-chngs1 input {
    margin-right: 3px;
    width: 136px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
jQuery(".inline").colorbox({inline:true, width:"50%"});
	loadDatePicker();
	jQuery("#EmergencyexitReportId").validationEngine();
});
 function changevalExit(){
	var passExit = 0;
	var failExit = 0;
	$(".dropdownexit").each(function(){
	  switch(this.value){
		case "Pass":
			passExit = passExit+1;
			break;
		case "Fail":
			failExit = failExit+1;
			break;
	  }
      });
	
	$("#EmergencyexitReportSurveyExitPass").val(passExit);
	$("#EmergencyexitReportSurveyExitFail").val(failExit);
	$("#EmergencyexitReportTotalExitPass").val(passExit);
	$("#EmergencyexitReportTotalExitFail").val(failExit);
 }
  function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
  function changevalEmergency(){
	var passEmergency = 0;
	var failEmergency = 0;
	$(".dropdownemergency").each(function(){
	  switch(this.value){
		case "Pass":
			passEmergency = passEmergency+1;
			break;
		case "Fail":
			failEmergency = failEmergency+1;
			break;
	  }
      });
	
	$("#EmergencyexitReportSurveyEmergencyPass").val(passEmergency);
	$("#EmergencyexitReportSurveyEmergencyFail").val(failEmergency);
	$("#EmergencyexitReportTotalEmergencyPass").val(passEmergency);
	$("#EmergencyexitReportTotalEmergencyFail").val(failEmergency);
  }

function loadDatePicker(){
        jQuery(".calender").datepicker({ dateFormat: 'yy-mm-dd',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });
}

function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
	    }
            });           
}        
window.onload = createUploader;
function AppendExit(divid){
    var counter = jQuery('#optioncountExit').val();
    counter = parseInt(counter) + 1;
    jQuery('#optioncountExit').val(counter);
    OptionHtml ='<div id="Newdiv_'+counter+'">';
    OptionHtml = OptionHtml+'<section style="width:54%">';
    OptionHtml = OptionHtml+'<p style="width:5%">&nbsp;</p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="location_'+counter+'" class="validate[required]" name="data[EmergencyexitReportsRecord]['+counter+'][location]" id=""></span></p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="mfd_'+counter+'" class="calender validate[required]" name="data[EmergencyexitReportsRecord]['+counter+'][mfd_date]" id="" readonly="readonly" style="width:78%"></span></p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="type_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][type]" id="" readonly="readonly" value="Exit Signs"></span></p>';
    OptionHtml = OptionHtml+'<p style="width:20%"><select id="status_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][status]"  class="dropdownexit wdth-250 validate[required]" style="width:78%" onchange="changevalExit()"><option value="" selected="selected">Select</option><option value="Pass">PASS</option><option value="Fail">FAIL</option></select></p>';
    OptionHtml = OptionHtml+'</section>';
    OptionHtml = OptionHtml+'<section style="width:46%; float:left;">';
    OptionHtml = OptionHtml+'<p style="width:100%;"><span class="input-field2"><input type="text" id="comment_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][comment]" style="width:70%" class="validate[required]" value=""><a href="javascript:void(0)" id="'+counter+'" onclick="RemoveExit(this.id);"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></span></p>';
    OptionHtml = OptionHtml+'</section><br/>';
    OptionHtml = OptionHtml+'</div>';
    jQuery('#Exitoption').append(OptionHtml);
    loadDatePicker();
    var incrementExit = $("#EmergencyexitReportTotalExit").val();
    incrementExit = parseInt(incrementExit)+1;
    $("#EmergencyexitReportTotalExit").val(incrementExit);
    var diffTotal = incrementExit - parseInt($("#EmergencyexitReportSurveyExit").val());
    $("#EmergencyexitReportDiffExit").val(diffTotal);
}

function RemoveExit(id){
	if($("#status_"+id).val() == "Pass"){
		var ExitPass = $("#EmergencyexitReportSurveyExitPass").val();
		ExitPass = parseInt(ExitPass)-1;
		$("#EmergencyexitReportSurveyExitPass").val(ExitPass);
		
		var ExitPassTotal = $("#EmergencyexitReportTotalExitPass").val();
		ExitPassTotal = parseInt(ExitPassTotal)-1;
		$("#EmergencyexitReportTotalExitPass").val(ExitPassTotal);
		
		var valTotalExit = $("#EmergencyexitReportTotalExit").val();
		    valTotalExit = parseInt(valTotalExit)-1;
		 $("#EmergencyexitReportTotalExit").val(valTotalExit);
		 
		var diff =  (parseInt($("#EmergencyexitReportTotalExit").val()))-(parseInt($("#EmergencyexitSurveyExit").val()))
		 $("#EmergencyexitReportDiffExit").val(diff);
		 
		 var valTotalExitHidden = $("#optioncountExit").val();
		valTotalExitHidden = parseInt(valTotalExitHidden)-1;
		$("#optioncountExit").val(valTotalExitHidden); 
		
	}
	if($("#status_"+id).val() == "Fail"){
		var ExitFail = $("#EmergencyexitReportSurveyExitFail").val();
		ExitFail = parseInt(ExitFail)-1;
		$("#EmergencyexitReportSurveyExitFail").val(ExitFail);
		
		var ExitFailTotal = $("#EmergencyexitReportTotalExitFail").val();
		ExitFailTotal = parseInt(ExitFailTotal)-1;
		$("#EmergencyexitReportTotalExitFail").val(ExitFailTotal);
		
		var valTotalExit = $("#EmergencyexitReportTotalExit").val();
		valTotalExit = parseInt(valTotalExit)-1;
		 $("#EmergencyexitReportTotalExit").val(valTotalExit);
		 
		 var diff =  (parseInt($("#EmergencyexitReportTotalExit").val()))-(parseInt($("#EmergencyexitSurveyExit").val()))
		 $("#EmergencyexitReportDiffExit").val(diff);
		 
		 var valTotalExitHidden = $("#optioncountExit").val();
		 valTotalExitHidden = parseInt(valTotalExitHidden)-1;
		 $("#optioncountExit").val(valTotalExitHidden);
		
	}
	if($("#status_"+id).val() == ""){
		var valTotalExit = $("#EmergencyexitReportTotalExit").val();
		    valTotalExit = parseInt(valTotalExit)-1;
		 $("#EmergencyexitReportTotalExit").val(valTotalExit);
		
		var valTotalExitHidden = $("#optioncountExit").val();
		valTotalExitHidden = parseInt(valTotalExitHidden)-1;
		$("#optioncountExit").val(valTotalExitHidden);
		
		var diff =  (parseInt($("#EmergencyexitReportTotalExit").val()))-(parseInt($("#EmergencyexitSurveyExit").val()))
		 $("#EmergencyexitReportDiffExit").val(diff);
	 }
        jQuery('#Newdiv_'+id).remove();
}

function AppendEmergency(divid){
    var counter = jQuery('#optioncountEmergency').val();
    counter = parseInt(counter) + 1;
    jQuery('#optioncountEmergency').val(counter);
    OptionHtml ='<div id="NewdivEmergency_'+counter+'">';
    OptionHtml = OptionHtml+'<section style="width:54%">';
    OptionHtml = OptionHtml+'<p style="width:5%">&nbsp;</p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="location_'+counter+'" class="validate[required]" name="data[EmergencyexitReportsRecord]['+counter+'][location]" id=""></span></p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="mfd_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][mfd_date]" id="" readonly="readonly" class="calender validate[required]" style="width:78%"></span></p>';
    OptionHtml = OptionHtml+'<p style="width:25%"><span class="input-field2"><input type="text" id="type_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][type]" id="" readonly="readonly" value="Emergency Lights"></span></p>';
    OptionHtml = OptionHtml+'<p style="width:20%"><select id="status_'+counter+'" name="data[EmergencyexitReportsRecord]['+counter+'][status]" class="dropdownemergency wdth-250 validate[required]" style="width:78%" onchange="changevalEmergency();"><option value="" selected="selected">Select</option><option value="Pass">PASS</option><option value="Fail">FAIL</option></select></p>';
    OptionHtml = OptionHtml+'</section>';
    OptionHtml = OptionHtml+'<section style="width:46%; float:left;">';
    OptionHtml = OptionHtml+'<p style="width:100%;"><span class="input-field2"><input type="text" id="comment_'+counter+'" class="validate[required]" name="data[EmergencyexitReportsRecord]['+counter+'][comment]" style="width:70%" id="" value=""><a href="javascript:void(0)" id="'+counter+'" onclick="RemoveEmergency(this.id);"><img src="../../img/minus.png" alt="Remove" title="Remove" /></a></span></p>';
    OptionHtml = OptionHtml+'</section><br/>';
    OptionHtml = OptionHtml+'</div>';
    jQuery('#Emergencyoption').append(OptionHtml);
    var incrementEmergency = $("#EmergencyexitReportTotalEmergency").val();
    incrementEmergency = parseInt(incrementEmergency)+1;
    $("#EmergencyexitReportTotalEmergency").val(incrementEmergency);
    
     var diffTotal = incrementEmergency - parseInt($("#EmergencyexitReportSurveyEmergency").val());
    $("#EmergencyexitReportDiffEmergency").val(diffTotal);
    
    loadDatePicker();
}
function RemoveEmergency(id){
	if($("#status_"+id).val() == "Pass")
	{
		var EmergencyPass = $("#EmergencyexitReportSurveyEmergencyPass").val();
		EmergencyPass = parseInt(EmergencyPass)-1;
		$("#EmergencyexitReportSurveyEmergencyPass").val(EmergencyPass);
		
		var EmergencyPassTotal = $("#EmergencyexitReportTotalEmergencyPass").val();
		EmergencyPassTotal = parseInt(EmergencyPassTotal)-1;
		$("#EmergencyexitReportTotalEmergencyPass").val(EmergencyPassTotal);
		
		var valTotalEmergency = $("#EmergencyexitReportTotalEmergency").val();
		    valTotalEmergency = parseInt(valTotalEmergency)-1;
		    $("#EmergencyexitReportTotalEmergency").val(valTotalEmergency);
		 
		var valTotalEmergencyHidden = $("#optioncountEmergency").val();
		    valTotalEmergencyHidden = parseInt(valTotalEmergencyHidden)-1;
		    $("#optioncountEmergency").val(valTotalEmergencyHidden);
		    
		    
		var diff =  (parseInt($("#EmergencyexitReportTotalEmergency").val()))-(parseInt($("#EmergencyexitSurveyEmergency").val()))
		 $("#EmergencyexitReportDiffEmergency").val(diff);
		 
	}
	if($("#status_"+id).val() == "Fail")
	{
		var EmergencyFail = $("#EmergencyexitReportSurveyEmergencyFail").val();
		EmergencyFail = parseInt(EmergencyFail)-1;
		$("#EmergencyexitReportSurveyEmergencyFail").val(EmergencyFail);
		
		var EmergencyFailTotal = $("#EmergencyexitReportTotalEmergencyFail").val();
		EmergencyFailTotal = parseInt(EmergencyFailTotal)-1;
		$("#EmergencyexitReportTotalEmergencyFail").val(EmergencyFailTotal);
		
		var valTotalEmergency = $("#EmergencyexitReportTotalEmergency").val();
		valTotalEmergency = parseInt(valTotalEmergency)-1;
		$("#EmergencyexitReportTotalEmergency").val(valTotalEmergency);
		
		var valTotalEmergencyHidden = $("#optioncountEmergency").val();
		valTotalEmergencyHidden = parseInt(valTotalEmergencyHidden)-1;
		$("#optioncountEmergency").val(valTotalEmergencyHidden);
		
		var diff =  (parseInt($("#EmergencyexitReportTotalEmergency").val()))-(parseInt($("#EmergencyexitSurveyEmergency").val()))
		 $("#EmergencyexitReportDiffEmergency").val(diff);
	}
	 if($("#status_"+id).val() == "")
	 {
		var valTotalEmergency = $("#EmergencyexitReportTotalEmergency").val();
		valTotalEmergency = parseInt(valTotalEmergency)-1;
		$("#EmergencyexitReportTotalEmergency").val(valTotalEmergency);
		
		var valTotalEmergencyHidden = $("#optioncountEmergency").val();
		valTotalEmergencyHidden = parseInt(valTotalEmergencyHidden)-1;
		$("#optioncountEmergency").val(valTotalEmergencyHidden);
		
		var diff =  (parseInt($("#EmergencyexitReportTotalEmergency").val()))-(parseInt($("#EmergencyexitSurveyEmergency").val()))
		 $("#EmergencyexitReportDiffEmergency").val(diff);
	 }
    jQuery('#NewdivEmergency_'+id).remove();
}

 function checkUploadedAttachment(){
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
  function checkNotification(){
	var notifyVal = jQuery("#ExtinguisherReportNotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }

function RemoveAttachment(attachId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delemergencyattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmattach(attachId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId);    
  }
  else{
    return false;
  }
}

function RemoveNotifier(notifierId){	
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delemergencynotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#pnotify_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId);    
  }
  else{
    return false;
  }
 }
 function RemoveRecord(rId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delexitrecord/" + rId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#LI_'+rId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmbox(rId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveRecord(rId);    
  }
  else{
    return false;
  }
 }
 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
</script>
 
<section style="min-width:980px">
<section class="register-wrap">
<div class="headingtxt"> Emergency Light / Exit Sign</div>
 
<h3 style="margin-top:10px;" align="center" ><?php echo ucwords($spResult['User']['fname'].' '.$spResult['User']['lname']);?></h3></br>
 <?php echo $this->element('reports/emergency_exit_form'); ?>
    <tr>      
      <td>
	&nbsp;</td>
    </tr>
  </table>  
</section>
</section>

    <!-- This contains the hidden content for inline calls -->
