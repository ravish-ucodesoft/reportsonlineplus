<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#addprincipalform").validationEngine();
});
</script>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Add Company Principals</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addcompanyprincipals') ,'name'=>'addform','id'=>'addprincipalform', 'class'=>'search_form general_form')); ?>
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms">
	
  		 <div class="row">
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		    </span>
  		 </div>	 
				<div class="row">
				<label style="width:260px;">Principal Name: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('UserCompanyPrincipal.name', array('class'=>'text validate[required]'));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('UserCompanyPrincipal.name'); ?></span>
				</div>
			</div>
			<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="list" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
			</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
