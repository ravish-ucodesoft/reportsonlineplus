<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery(".ajax").colorbox();
});
</script>
<style>
.table_wrapper_inner th{
	padding:0px; text-align:center;
}
</style>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Direct Client List</h2>
					
							
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
								<th width='2%'>No.</th>								
								<th width='15%'><?php echo $paginator->sort('First Name','first.name');?> </th>
								<th width='15%'><?php echo $paginator->sort('Last Name','last.name');?> </th>
								<th width='15%'><?php echo $paginator->sort('Email','email');?> </th>
								<th width='15%'><?php echo $paginator->sort('Daytime Phone Number','site_phone');?> </th>
								<th width='15%'><?php echo $paginator->sort('Address','site_address');?> </th>	
								<th width='13%'><?php echo $paginator->sort('Site Name','site_name');?> </th>	
								<th width='10%'>Actions</th>
							</tr>
							<?php  if(count($clientListing)) { ?>
						<tbody>		
					<?php $i=1; foreach ($clientListing as $clist):
					
					
          	//$activeImg = $userlist['Admin']['status']==1 ?'/img/icons/status_star_green.gif' : '/img/icons/status_star_red.gif'; ?>

							<tr class="first">
								<td><?php echo $i; ?>.</td>
								<td>
								<?php if($clist['User']['direct_client_preference']==1)
														{
														$title= "I would like ReportsOnline Plus to send my information to qualified service providers in my area that are qualified to perform the services that i requested?";
														}else
														{
														$title =  "I would like ReportsOnline Plus to send me the information about qualified service providers that can perform the work listed?";
														}
								?>	
								<?php echo $this->Html->link(ucWords($clist['User']['fname']),'javascript:void()',array('title'=>$title,'escape'=>false)); ?></td>
									<td><?php echo ucWords($clist['User']['lname']); ?></td>
									<td><?php echo $clist['User']['email']; ?></td>
									<td><?php echo $clist['User']['site_phone']; ?></td>
									<td><?php echo $clist['User']['site_address']; ?></td>
									<td><?php echo $clist['User']['site_name']; ?></td>
									
								
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:150px'>			
											<li>          
											<?php echo $this->Html->link("View Detail",array('controller'=>'admins','action'=>'clientDescription',base64_encode($clist['User']['id'])), array('class' => 'ajax','title'=>"",'escape'=>false)); ?>
											</li>
											<?php if($clist['User']['direct_client_preference']==1)
														{
														echo $this->Html->link("Forward Lead",array('controller'=>'admins','action'=>'fwdLeadToSp',base64_encode($clist['User']['id'])), array('class' => 'ajax','title'=>"Forward ".ucwords($clist['User']['fname'])."'s"." request to sp",'escape'=>false));
														}else
														{
														echo $this->Html->link("Forward Lead",array('controller'=>'admins','action'=>'sendSpInfo',base64_encode($clist['User']['id'])), array('class' => 'ajax','title'=>"Send SP info to ".ucwords($clist['User']['fname']),'escape'=>false));
														}
								?>	
											
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
<?php } else { ?>
	<tr class="first"><td colspan='6'>No record found</td> </tr>
<?php } ?>
</table>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->
				
					<div class="pagination">
						
					<span class="page_no">Page <?php echo $paginator->counter(); ?></span>
    			<ul class="pag_list">
    				<li class="prev" style="display:block;" ><?php echo $paginator->prev('Previous'); ?> </li>
    				<li><?php echo $paginator->numbers(); ?></li>
    				<li class="next" ><?php echo $paginator->next('Next'); ?></li>
    			</ul>	
										
					</div>
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->