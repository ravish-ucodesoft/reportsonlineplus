<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Add Sample Reports</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php
		echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'editreport') ,'name'=>'addreport','id'=>'addform','enctype' => 'multipart/form-data'));
		echo $this->Form->hidden('SampleReport.id',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false));
		echo $this->Form->hidden('SampleReport.old_file_name',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false,'value'=>$this->data['SampleReport']['file_name']));
	?>
	
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <table width="100%">
			<tr><td>Report Name<span class="star">*</span></td>
			<td><?php echo $this->Form->input('SampleReport.name',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			
			</tr>
			<tr><td>File<span class="star">*</span></td>
			<td><?php echo $this->Form->input('SampleReport.file_name',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			
			</tr>
		</table>
			<!--[if !IE]>start row<![endif]-->
		<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save');  ?></span>
    			<!--span class="button red_button"><a href="/wellnesscenter1/websitepages/pagelisting" ><span><span style="color:white">Cancel</span></span></a></span-->
			<span class="button red_button"><a href="javascript:history.go(-1);" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
		</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
