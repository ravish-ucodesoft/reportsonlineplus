<?php echo $crumb->getHtml('Change Password', 'null','' ); ?>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
		<h2>Change Password</h2>		
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>'changepasswd' ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
	
	<?php echo $form->hidden('Admin.id', array('maxLength'=>'40','class'=>'text'));  ?>  
  	<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms">				
			<!--[if !IE]>start row<![endif]-->
			<div class="row">
				<label>Old password:<span class="message">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->password('Admin.oldpwd', array('maxLength'=>'30','class'=>'text')); ?>
					</span>
					<span class="system negative"><?php echo $form->error('Admin.oldpwd'); ?></span>
				</div>
			</div>		
			<div class="row">
				<label>New Password:<span class="message">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->password('Admin.pwd', array('maxLength'=>'30','class'=>'text')); ?>
					</span>
					<span class="system negative"><?php echo $form->error('Admin.pwd'); ?></span>
				</div>
			</div>
			
			<div class="row">
				<label>Confirm Password:</label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->password('Admin.cpwd', array('maxLength'=>'30','class'=>'text'));  ?>
					</span>	
          <span class="system negative"><?php echo $form->error('Admin.cpwd'); ?></span>				
				</div>
			</div>		
			<!--[if !IE]>start row<![endif]-->
			<div class="row">
				<div class="inputs">
					<span class="button orange_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
			</div>
			<!--[if !IE]>end forms<![endif]-->		
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->		
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->