<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery(".ajax").colorbox();
});
</script>
<?php 
  //echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Sample Reports</h2>
					
					<div id="product_list_menu">
						<!--<a href="/admin/admins/addreport" class="update"><span><span><em>Add Report</em><strong></strong></span></span></a>-->
					</div>	
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
	<?php echo $this->Form->create('', array('controller'=>'admin','action' => 'inspectionDeviceList','name'=>'webpages','id'=>'WebsitepagesShowForm') );?>
						<table cellpadding="0" cellspacing="0" width="100%">
						<tr>								
                                                    <th width='40%'>Report name</th>								
                                                    <th width='30%'>&nbsp;</th>
													<th width='30%'>Action</th>
						</tr>
							<?php  if(count($data)){ ?>
						<tbody>
							 <?php foreach($data as $data){ ?>
							<tr class="first">
								<td><?php echo $data['SampleReport']['name']; ?></td>
								<td><?php echo $data['SampleReport']['file_name']; ?></td>
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:150px'>			
											<li><?php echo $html->link('Edit',array('action'=>'editreport',$data['SampleReport']['id']),array('class'=>'edit')) ?></li>
										</ul>
									</div>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					
							
<?php } else { ?>
	<tr class="first"><td colspan='6'>No record found</td> </tr>
<?php } ?>
</table>
						 <?php echo $this->Form->end(); ?>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				</div>
			
				<!--[if !IE]>end section<![endif]-->
				
