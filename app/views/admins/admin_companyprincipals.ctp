<?php 
	echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Company Principals</h2>			
					<div id="product_list_menu">
						<a href="/admin/admins/addcompanyprincipals" class="update"><span><span><em>Add New</em><strong></strong></span></span></a>
					</div>				
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<th>No.</th>								
								<th width='80%'>Principal Name</th>
								<th width='10%'>Action</th>
							</tr>
							<?php  if(count($principal)) { ?>
						<tbody>		
							<?php $i=1; foreach ($principal as $principal): ?>							
							      <tr class="first">
								<td><?php echo $i; ?>.</td>
								<td><?php echo $principal['UserCompanyPrincipal']['name']; ?></td>
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:120px'>			
											<li><?php echo $html->link('Edit',array('controller'=>'admins','action'=>'editcompanyprincipal',$principal['UserCompanyPrincipal']['id']),array('class'=>'edit')) ?></li>
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
<?php } else { ?>
	<tr class="first"><td colspan='6'>There is no Company Plan</td> </tr>
<?php } ?>
</table>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->					
				
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->