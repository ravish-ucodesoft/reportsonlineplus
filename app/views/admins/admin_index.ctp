
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Year');
        data.addColumn('number', 'SPs');
        data.addRows(12);
        data.setValue(0, 0, 'January');
        data.setValue(0, 1, <?php echo $final_value[0];?>);
        data.setValue(1, 0, 'Feburary');
        data.setValue(1, 1, <?php echo $final_value[1];?>);
        data.setValue(2, 0, 'March');
        data.setValue(2, 1, <?php echo $final_value[2];?>);
        data.setValue(3, 0, 'April');
        data.setValue(3, 1, <?php echo $final_value[3];?>);
        data.setValue(4, 0, 'May');
        data.setValue(4, 1, <?php echo $final_value[4];?>);
        data.setValue(5, 0, 'June');
        data.setValue(5, 1, <?php echo $final_value[5];?>);
        data.setValue(6, 0, 'July');
        data.setValue(6, 1, <?php echo $final_value[6];?>);
        data.setValue(7, 0, 'Augest');
        data.setValue(7, 1, <?php echo $final_value[7];?>);
        data.setValue(8, 0, 'September');
        data.setValue(8, 1, <?php echo $final_value[8];?>);
        data.setValue(9, 0, 'October');
        data.setValue(9, 1, <?php echo $final_value[9];?>);
        data.setValue(10, 0, 'November');
        data.setValue(10, 1, <?php echo $final_value[10];?>);
        data.setValue(11, 0, 'December');
        data.setValue(11, 1, <?php echo $final_value[11];?>);

        var options = {
          title: 'Service Provider Registered',
          hAxis: {title: 'Months', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

<div id="content">	
			<!--[if !IE]>start content bottom<![endif]-->
			<div id="content_bottom">
				<?php //echo $crumb->getHtml('Home', 'reset' );?>	
			<div class="inner">
				
				<!--[if !IE]>start info<![endif]-->
				<div id="">

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td align="left" valign="top" colspan='3';><h2></h2></td>
	</tr>
	<tr>
	<td align="center" valign="top">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" valign="top">
		<?php
			echo $html->link($html->image('/img/icons/user-management.png',array('alt'=>'Members','border'=>'0','title'=>'Managers')),array('controller'=>'admins','action'=>'#'),array('class'=>'maybelightbox','escape'=>false),false,false)
		?>
		</td>
		<td align="center" valign="top">
		<?php
			echo $html->link($html->image('/img/tutorial.png',array('alt'=>'tutorial','border'=>'0','title'=>'tutorial')),array('controller'=>'admins','action'=>'tutorial'),array('class'=>'maybelightbox','escape'=>false),false,false)
		?></td>
		<td align="center" valign="top">
		<?php    
			echo $html->link($html->image('/img/icons/manage-content.png',array('alt'=>'Manage Inspection Device','border'=>'0','title'=>'Manage Inspection Device')),array('controller'=>'admins','action'=>'inspectionDeviceList'),array('class'=>'maybelightbox','escape'=>false),false,false)
		?>
		</td>
	</tr>
	<tr>
		<td align="center" valign="top">
		<?php echo $html->link('Manage Fire Inspectors',array('controller'=>'companies','action'=>'subscriberslist')); ?>
		</td>
		<td align="center" valign="top">
		<?php echo $html->link('Manage Tutorials',array('controller'=>'admins','action'=>'tutorial')); ?>
		</td>
		</td>
		<td align="center" valign="top">
		<?php echo $html->link('Manage Inspection Device',array('controller'=>'admins','action'=>'inspectionDeviceList')); ?>
		</td>
	</tr>
    <tr>
         <td colspan='3'>&nbsp;</td>
	</tr>
	<tr>
	<td align="center" valign="top">
	<?php    
		echo $html->link($html->image('/img/ReportIcon.png',array('alt'=>'View Sample Reports','border'=>'0','title'=>'View Sample Reports')),array('controller'=>'admins','action'=>'viewSampleReport'),array('class'=>'maybelightbox','escape'=>false),false,false)
	?>
	</td>
	
	<td align="center" valign="top">
	<?php    
		echo $html->link($html->image('/img/principal.png',array('alt'=>'Company Principals','border'=>'0','title'=>'Company Principals')),array('controller'=>'admins','action'=>'companyprincipals'),array('class'=>'maybelightbox','escape'=>false),false,false)
	?>
	</td>
	<td align="center" valign="top"><?php    
		echo $html->link($html->image('/img/trialicon.png',array('alt'=>'Manage trial','border'=>'0','title'=>'Manage Trial')),array('controller'=>'admins','action'=>'manageTrial'),array('class'=>'maybelightbox','escape'=>false),false,false)
	?></td>
	</tr>
	<tr>
		<td align="center" valign="top">
		<?php echo $html->link('View Sample Reports',array('controller'=>'admins','action'=>'viewSampleReport')); ?>
		</td>
		<td align="center" valign="top">
		<?php echo $html->link('Manage Company Principals',array('controller'=>'admins','action'=>'companyprincipals')); ?>
		</td>
		</td>
		<td align="center" valign="top">
		<?php echo $html->link('Manage Trial Period',array('controller'=>'admins','action'=>'manageTrial')); ?>
		</td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<!--  #third row  -->
	<tr>
	      <td align="center" valign="top">
	    <?php    
		    echo $html->link($html->image('video.png',array('alt'=>'Home Video','border'=>'0','title'=>'Home Video')),array('controller'=>'admins','action'=>'home_video_list'),array('class'=>'maybelightbox','escape'=>false),false,false)
	    ?>
	    </td>
	    <td align="center" valign="top">
	    <?php    
		    echo $html->link($html->image('testimonials.png',array('alt'=>'Testimonials','border'=>'0','title'=>'Testimonials')),array('controller'=>'admins','action'=>'view_testimonials'),array('class'=>'maybelightbox','escape'=>false),false,false)
	    ?>
	    </td>
	    <td align="center" valign="top">
	    <?php    
		    echo $html->link($html->image('aboutus.png',array('alt'=>'About us','border'=>'0','title'=>'About us')),array('controller'=>'admins','action'=>'aboutus'),array('class'=>'maybelightbox','escape'=>false),false,false)
	    ?>
	    </td>
	</tr>		
	
	<tr>
	  <td align="center" valign="top">
		<?php echo $html->link('Home Video',array('controller'=>'admins','action'=>'home_video_list')); ?>
	  </td>
	  <td align="center" valign="top">
		<?php echo $html->link('Testimonials',array('controller'=>'admins','action'=>'view_testimonials')); ?>
	  </td>
	  <td align="center" valign="top">
		<?php echo $html->link('About us Text',array('controller'=>'admins','action'=>'aboutus')); ?>
	  </td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td align="center" valign="top">
	    <?php    
		    echo $html->link($html->image('/img/inquiry.png',array('alt'=>'Inspection Query','border'=>'0','title'=>'Inspection Query')),array('controller'=>'admins','action'=>'inspectionQuery'),array('class'=>'maybelightbox','escape'=>false),false,false)
	    ?>
	    </td>
	    <td align="center" valign="top">
	    <?php    
		    echo $html->link($html->image('text_warning.png',array('alt'=>'Max Site Address Text','border'=>'0','title'=>'Max Site Address Text')),array('controller'=>'admins','action'=>'limitText'),array('class'=>'maybelightbox','escape'=>false),false,false)
	    ?>
	    </td>
		
		<td align="center" valign="top">
	    <?php    
		    echo $html->link($html->image('activation.png',array('alt'=>'Activation Request for site address','border'=>'0','title'=>'Activation Request for site address')),array('controller'=>'admins','action'=>'siteadd_activation_request'),array('class'=>'maybelightbox','escape'=>false),false,false)
	    ?>
	    </td>
		
	</tr>
	
	<tr>
	  <td align="center" valign="top">
		<?php echo $html->link('Query regarding life inspection security or code',array('controller'=>'admins','action'=>'inspectionQuery')); ?>
	  </td>
	  <td align="center" valign="top">
		<?php echo $html->link('Max Site Address Text',array('controller'=>'admins','action'=>'limitText')); ?>
	  </td>
	  <td align="center" valign="top">
		<?php echo $html->link('Site address Activation Request',array('controller'=>'admins','action'=>'siteadd_activation_request')); ?>
	  </td>
	</tr>
	
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
	    <td align="center" valign="top">
	    <?php    
		    echo $html->link($html->image('/img/reports.png',array('alt'=>'Sample Reports','border'=>'0','title'=>'Sample Reports')),array('controller'=>'admins','action'=>'sampleReports'),array('class'=>'maybelightbox','escape'=>false),false,false)
	    ?>
	    </td>	    
		
	</tr>
	<tr>
	  <td align="center" valign="top">
		<?php echo $html->link('Sample Reports',array('controller'=>'admins','action'=>'sampleReports')); ?>
	  </td>	  
	</tr>
	
	
	<tr><td colspan="3">&nbsp;</td></tr>
 
</table>				
		 <div id="chart_div" style="width: 900px; height: 300px;"></div>
			</div>
				<!--[if !IE]>end info<![endif]-->
								
				<!--[if !IE]>start sidebar<![endif]-->
			<!--	<div id="sidebar">					
					<!--[if !IE]>start sidebar module<![endif]
					<div class="sidebar_module">
						<div class="title_wrapper">
							<h3>Alerts</h3>
						</div>
						 

						
						</div>
				</div> -->
					<!--[if !IE]>end sidebar module<![endif]-->							
				</div>
				<!--[if !IE]>end sidebar<![endif]-->								
			</div>
			<!--[if !IE]>end content bottom<![endif]-->
			</div>			
		</div>
		<!--[if !IE]>end content<![endif]-->				