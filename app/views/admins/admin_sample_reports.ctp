<div class="section">				
    <div class="title_wrapper">					
            <h2>Sample Reports</h2>			
            <div id="product_list_menu">                    
            </div>				
    </div>								
    
				<div class="section_inner">							
				<div  id="product_list">        													
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<th>No.</th>								
								<th width='40%'>Reports</th>
								<th width='40%'>Download</th>								
							</tr>
							
						<tbody>		
														
							      <tr class="first">
								<td>1.</td>
								<td>Firealarm Report</td>
								<td><?php echo $html->link('Download',array('controller'=>'admins','action'=>'downloadReport','admin'=>true,'Firealarm'))?></td>
								
							</tr>
                                                        <tr class="first">
								<td>2.</td>
								<td>Sprinkler Report</td>
								<td><?php echo $html->link('Download',array('controller'=>'admins','action'=>'downloadReport','admin'=>true,'Sprinkler'))?></td>
								
							</tr>
                                                        <tr class="first">
								<td>3.</td>
								<td>Kitchenhood Report</td>
								<td><?php echo $html->link('Download',array('controller'=>'admins','action'=>'downloadReport','admin'=>true,'Kitchenhood'))?></td>
								
							</tr>
							<tr class="first">
								<td>4.</td>
								<td>Emergency Exit Lights Report</td>
								<td><?php echo $html->link('Download',array('controller'=>'admins','action'=>'downloadReport','admin'=>true,'Emergencyexit_lights'))?></td>
								
							</tr>
							<tr class="first">
								<td>5.</td>
								<td>Extinguisher Report</td>
								<td><?php echo $html->link('Download',array('controller'=>'admins','action'=>'downloadReport','admin'=>true,'Extinguisher'))?></td>
								
							</tr>
							<tr class="first">
								<td>6.</td>
								<td>Special Hazard Report</td>
								<td><?php echo $html->link('Download',array('controller'=>'admins','action'=>'downloadReport','admin'=>true,'Specialhazard'))?></td>
								
							</tr>
						
						</tbody>

</table>
                                                </div>
                                        </div>
                                </div>
                                </div>
   </div>
   

						