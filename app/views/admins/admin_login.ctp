<!--[if !IE]>start content<![endif]-->
		<div id="content" >
				<!--[if !IE]>start login_wrapper<![endif]-->
				<div id="login_wrapper">
				
					<span class="extra1">
         </span>
					<div class="title_wrapper">
						<h2>Please Login</h2>						
					</div>					
						<?php echo $form->create('admin', array('type'=>'POST', 'action'=>'login','name'=>'loginform','id'=>'loginform')); ?>					
						<fieldset>
				    <div class="row">		 
            <?php if($session->check('Message.flash')) {
    				   ?>
    				 <div class='message'><?php echo $this->Session->flash();?></div>
    				 <?php
    				  }
    				  ?>
    				  </div>
							<!--[if !IE]>start row<![endif]-->
							<div class="row">
								<label>Username:</label>
								<span class="input_wrapper">
								<?php echo $form->text('username', array('class'=>'text'));  ?>
								</span>
							</div>
							<!--[if !IE]>end row<![endif]-->
							<!--[if !IE]>start row<![endif]-->
							<div class="row">
								<label>Password:</label>
								<span class="input_wrapper">
								<?php echo $form->password('pwd', array('class'=>'text'));  ?>
								</span>
							</div>
							<!--[if !IE]>end row<![endif]-->
							<!--[if !IE]>start row<![endif]-->
							<div class="row">
								<!-- <label class="inline"><input class="checkbox" name="" type="checkbox" value="" /> remember me on this computer</label> -->
							</div>
							<!--[if !IE]>end row<![endif]-->
							<!--[if !IE]>start row<![endif]-->
								<div class="row">
									<div class="inputs small_inputs">
										<span class="button blue_button unlock_button"><span><span><em><span class="button_ico"></span>Login</em></span></span><input name="" type="submit" /></span>
										<a href="/" target="_blank" class="button gray_button"><span><span>Back to site</span></span></a> 
									</div>
								</div>
								<!--[if !IE]>end row<![endif]-->
						</fieldset>
					<?php echo $form->end(); ?>
				</div>
				<!--[if !IE]>end login_wrapper<![endif]-->
			
		</div>
		<!--[if !IE]>end content<![endif]-->



	