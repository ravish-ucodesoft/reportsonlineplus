<?php 
	echo $crumb->getHtml('Edit', 'null','') ;
?>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Edit Staff (Sub admin)</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'edit') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <div class="row">
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		      	<?php echo $form->hidden('Admin.id'); ?>
  		    </span>
  		 </div>	 
				<div class="row">
				<label>First Name : <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Admin.fname', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					
					<span class="system negative" id="err1"><?php echo $form->error('Admin.fname'); ?></span>
				</div>
			</div>
				
				<div class="row">
				<label>Last Name : </label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Admin.lname', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					
					<span class="system negative" id="err1"><?php echo $form->error('Admin.lname'); ?></span>
				</div>
			</div>
			
	
			<div class="row">
				<label>Email : <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Admin.email', array('maxLength'=>'50','class'=>'text','readonly'=>true));  ?>
					</span>
					<span class="system negative" id="err2"><?php echo $form->error('Admin.email'); ?></span>
				</div>
			</div>
					
			<!--[if !IE]>start row<![endif]-->
			<div class="row">
				<label>Status : <span class="star">*</span></label>
				<div class="inputs">
					<ul class="inline">
					<li><?php $option=array('1'=>'Enabled','0'=>'Disabled'); 
          echo $form->radio('Admin.status',$option,array('legend'=>false,'label'=>false,'default'=>1));?></li>
					</ul>
          </div>
			</div>			
			<!--[if !IE]>start row<![endif]-->
			<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="/admin/admins/list" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
			</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
