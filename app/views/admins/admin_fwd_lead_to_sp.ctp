
<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core','ui/ui.datepicker','jquery-ui-1.8.20.custom.min'));
    echo $this->Html->css('style.css');
    //echo $this->Html->script('countdown.js');
?>


<?php echo $this->Html->script(array('validation/jquery.validate','validation/jquery.validation.functions')); // FOR validations ?>
<script type='text/Javascript'>
jQuery(document).ready(function() {
    loadDatePicker();
    jQuery('#composeMszForm').validate();
});
function loadDatePicker()
{   
   jQuery(".calender").datepicker({
    dateFormat: 'mm/dd/yy',
    minDate: 1,
    showOn: 'button',
    buttonImage: '/img/calendar.gif',
    buttonImageOnly: true
    });
}
$(function () {
	var austDay = new Date();
	austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
	$('#defaultCountdown').countdown({until: austDay});
	$('#year').text(austDay.getFullYear());
});
function confirmbox(id,recordId)
 {
  var r = confirm("Are you sure you want to delete this record?");
  
  if (r==true)
  {
     deleteData(recordId);
    // return true;
      
  }
  else
  {
    return false;
  }
 }
   function deleteData(recordId)
   {
    
      jQuery.ajax({
                type : "GET",
                url  : "/sps/deleteLeadData/"+recordId,
                success : function(opt){
			if(opt==1){
			jQuery("#dataTr_"+recordId).fadeOut(500);
			jQuery("#deleteFlashmessage").fadeIn(500);
			jQuery("#deleteFlashmessage").fadeOut(5000);   
			}
			
                }
        });
   }   
</script>

<style>
.register-form > li > label {   
    padding-top: 3px;    
    width: 120px;	 
}
.register-form > li > p {   
    padding-left: 10px;     
}
#box {
    border: none;
}
.error{
color:red;
}
th{
    text-align:left;
}
#ui-datepicker-div{
    z-index:99999999;
}
</style>
<div id="content">
     <div id="box" style="width:700px;padding-left:10px;">
	 <?php 
	 $flag='';
	 $check= 0 ;
	  foreach($leadData as $lData){ 	  
	    if($lData['Lead']['status'] == 1 || $lData['Lead']['status'] == 0){  $flag=1;}else{$flag = 0; }
	   
	    $flagnew[]=$flag;
	  }	 
	if(!empty($flagnew)){
	$check = end($flagnew); }  ?>
	<?php if($check == 0){?>
      <?php echo $form->create('',array('controller'=>'admins','action' => 'fwdLeadToSp','method'=>'post','id'=>'composeMszForm','name'=>'ListForm'));?>
      <?php echo $form->hidden('Lead.lead_amt',array('size'=>'45','maxlength'=>'3','div'=>false,'label'=>false,'value'=>'$20','class'=>'')); ?>
      <?php echo $form->hidden('Lead.client_id',array('size'=>'45','maxlength'=>'3','div'=>false,'label'=>false,'value'=>$clientId,'class'=>'')); ?>
     <div class="form-box" style="width:90%">
     <?php $client_name = $this->Common->getclientName($clientId); ?>
        <h3 id="adduser"> <?php echo $client_name; ?> would like ReportOnlinePlus to send his/her information to  <br/>qualified service providers in his/her area that are qualified to perform <br/> the services that he/she requested</h3>
        
        <table class="register-form">
        <tr>
	    <td width="20%" style="vertical-align:top;"> <label>Choose SP:<span style="color:red">*</span></label></td>
	    <td><p><span>
            <?php  $option = $this->Common->getServiceProviderList();
	    echo $form->select('Lead.sp_id',$option,'',array('id'=>'sp','legend'=>false,'label'=>false,'empty'=>'--sp--',"class"=>"wdth-140 required",'style'=>'width:150px;')); ?>
            </span></p></td>
			
        </tr>
         <tr>
	    <td style="vertical-align:top;"> <label>Lead Amount:<span style="color:red">*</span></label></td>
	    <td> <p><span class="input-field">$20</span></p></td>
			
        </tr>
         <tr>
	    <td style="vertical-align:top;"> <label>Time Given:<span style="color:red">*</span></label></td>
	    <td> <p><span class="input-field"><?php echo $form->input('Lead.time_given',array('size'=>'45','maxlength'=>'','div'=>false,'label'=>false,'value'=>'','class'=>'calender required','style'=>'width:150px')); ?></span></p></td>
			
        </tr>
        <tr>
	    <td style="vertical-align:top;"> <label>&nbsp;</label></td>
	    <td style="vertical-align:top;"> <p><span style="color:#B1B179; font-size:11px;font-weight:bold;">This is the time limit to be given to SP for responding for the given request</span></p></td>
			
        </tr>
        <tr>
	    <td colspan="2" style="vertical-align:top;"> <p><span style="color:#949494;font-weight:bold;">If SP is not responding in given time limit,the request will be declined automatically.</span></p></td>
			
        </tr>        
        <tr>
            <td colspan="2"><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'float:right;')); ?></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        </table>
    </div>
    <!--one complete form ends-->

  <?php echo $form->end(); ?>
  <?php }else{
    echo "<p style='color:red; font-weight:bold;'>As you have already forwarded the client's lead to  one SP, so you are not able to forward request to another sp until he accept or decline the request </p>";
    } ?>
     <?php if(!empty($leadData)){ ?>
  <h3 id="adduser">Leads Sent</h3>
  <table width="100%">
    <tr style="background-color:#B1B179">
        <th>SP Name</th>
        <th>Time Given</th>
        <th>Time Left</th>
        <th>Status</th>        
    </tr>
    <?php foreach($leadData as $lData){ ?>
    <tr id="dataTr_<?php echo $lData['Lead']['id']; ?>">
        <td><?php echo $this->Common->getClientName($lData['Lead']['sp_id']); ?></td>
        <td><?php echo $lData['Lead']['time_given']; ?></td>
        <td>Time Left</td>
        <td><?php if($lData['Lead']['status']==0){
            $status='Pending'; }elseif($lData['Lead']['status']==1){
            $status='Approved';}else{
            $status='Declined';}
            echo $status; if($lData['Lead']['status']==0){ ?>
	    <a href="javascript:void(0);" style="float:right;padding-top:5px;" onclick="confirmbox(this.id,<?php echo $lData['Lead']['id']?>);" id="delete_<?php echo $lData['Lead']['id']; ?>"><?php echo $html->image('close.gif'); ?></a>
	    <?php } ?></td>        
    </tr>
     <?php } ?>
  </table>
  <?php } ?>
 <!--  <div id="defaultCountdown"></div>-->

</div>
</div>
