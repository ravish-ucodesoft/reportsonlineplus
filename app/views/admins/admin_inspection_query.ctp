<?php 
  echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
<div class="section">				
    <div class="title_wrapper">					
        <h2>Code Inspection Queries</h2>					            
    </div>								
    <!--[if !IE]>start section_inner<![endif]-->
    <div class="section_inner">							
	<div  id="product_list">        								
            <!--[if !IE]>start table_wrapper<![endif]-->
            <div class="table_wrapper">
		<div class="table_wrapper_inner">
                    <table cellpadding="0" cellspacing="0" width="100%">
			<tr>
                            <th>No.</th>								
                            <th width='20%'><?php echo $paginator->sort('Name','name');?> </th>                            
                            <th width='30%'><?php echo $paginator->sort('Email','email');?> </th>
                            <th width='30%'><?php echo $paginator->sort('Query','query');?> </th>
                            <th width='15%'>Actions</th>
			</tr>
			<?php  if(count($listing)) { ?>
			<tbody>		
                            <?php $i=1; foreach ($listing as $querylist):?>                            
			<tr class="first">
                            <td><?php echo $i; ?>.</td>
                            <td><?php echo $querylist['Inquiry']['name']; ?></td>
                            <td><?php echo $querylist['Inquiry']['email']; ?></td>
                            <td><?php echo nl2br($querylist['Inquiry']['query']); ?></td>
                            <td>
				<div class="actions_menu" style='width:100%'>
                                    <ul style='width:150px'>			                                        
                                        <li><?php echo $html->link('Delete',array('action'=>'deleteQuery',$querylist['Inquiry']['id']),array('class'=>'delete'),'Are you sure you want to delete this query?') ?></li>
                                    </ul>
				</div>
                            </td>
			</tr>
			<?php $i=$i+1; endforeach; ?>							
                    </tbody>
                    <?php } else { ?>
                    <tr class="first"><td colspan='6'>There is no query submitted yet</td> </tr>
                    <?php } ?>
                </table>
            </div>
	</div>
	<!--[if !IE]>end table_wrapper<![endif]-->					
    </div>
</div>
<!--[if !IE]>end section inner<![endif]-->
        	
<!--[if !IE]>start pagination<![endif]-->					
<div class="pagination">
    <span class="page_no">Page <?php echo $paginator->counter(); ?></span>
        <ul class="pag_list">
            <li class="prev" style="display:block;" ><?php echo $paginator->prev('Previous'); ?> </li>
            <li><?php echo $paginator->numbers(); ?></li>
            <li class="next" ><?php echo $paginator->next('Next'); ?></li>
    	</ul>	
    </div>
    <!--[if !IE]>end pagination<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->