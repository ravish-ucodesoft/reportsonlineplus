<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php //echo $this->Html->css(array('gallery/colorbox'));
echo $this->Html->css(array('facebox')); 
?>
<?php //echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<?php echo $this->Html->script(array('facebox')); ?>

<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				//jQuery(".ajax").colorbox();
});
function confirmbox(id,recordId){

  var r = confirm("Are you sure you want to delete this screenshot?");
  
  if (r==true)
  {
    if(id == "delete_"+recordId){
		deleteImage(recordId);
	}
	if(id == "deletetxt_"+recordId){
			deletetxt(recordId);
	}
    
      
  }
  else
  {
    return false;
  }
}
function deletetxt(recordId){
jQuery.ajax({
                type : "GET",
                url  : "/admins/deletetxt/"+recordId,
                success : function(opt){
			if(opt==1){
			jQuery("#TxtTd_"+recordId).fadeOut(500);			   
			}
			
                }
        });	
	
}
function deleteImage(recordId){
	jQuery.ajax({
                type : "GET",
                url  : "/admins/deleteImage/"+recordId,
                success : function(opt){
			if(opt==1){
			jQuery("#ImageTd_"+recordId).fadeOut(500);
			//jQuery("#deleteFlashmessage").fadeIn(500);
			//jQuery("#deleteFlashmessage").fadeOut(5000);   
			}
			
                }
        });
	
	
}
</script>
<div class="section">				
    <div class="title_wrapper">					
        <h2>Tutorials</h2>					
            <div id="product_list_menu">
                    <a href="/admin/admins/addVideo" class="update"><span><span><em>Add Video</em><strong></strong></span></span></a>
                    <a href="/admin/admins/addScreenshot" class="update"><span><span><em>Add Snapshot</em><strong></strong></span></span></a>
                    <a href="/admin/admins/editTutorial" class="update"><span><span><em>Edit Tutoial Text</em><strong></strong></span></span></a>                    
            </div>				
    </div>								
    <!--[if !IE]>start section_inner<![endif]-->
    <div class="section_inner">							
    <div  id="product_list">        								
            <!--[if !IE]>start table_wrapper<![endif]-->
        <div class="table_wrapper">
            <div class="table_wrapper_inner">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="3">
                           <h1>Videos</h1> 
                        </td>
                    </tr>
					<?php if(!empty($videoData)){ ?>
                    <tr>
                        <th>Video</th>
			<th>Title</th>
                        <th>Action</th>
                    </tr>
		    
		    <?php foreach($videoData as $vData){?>
		    <tr>
                        <td><a href="javascript:void(0)" onclick="return playVideo('<?php echo $vData['TutorialVideo']['video']?>','<?php echo $vData['TutorialVideo']['video_thumb']?>','0'); " title="Click to View the Video"><img src="<?php echo $vData['TutorialVideo']['video_thumb']?>" width="100px;height:80px;"></a></td>
			<td><?php echo $vData['TutorialVideo']['title']?></td>
                        <td><?php echo $html->link('Delete',array('controller'=>'admins','action'=>'delete_video',$vData['TutorialVideo']['id']),array('class'=>''),'Are you sure you want to delete this Video tutorial?') ?></td>
                    </tr>
		    <?php } ?>
					<?php  } else{
						echo "<tr><td>No Video Uploaded</td></tr>";
					}?>
                </table>
                <br/><br/>
                <table cellpadding="0" cellspacing="0" width="100%">
                     <tr>
                        <td colspan="2">
                           <h1>Screenshots</h1> 
                        </td>
                    </tr>
					 <?php if(!empty($snapshotData)){?>
                    <tr>
                        <th>
                           Screenshot  
                        </th>
                        
                    </tr>
					<?php foreach($snapshotData as $snapData){?>
					<tr id="ImageTd_<?php echo $snapData['TutorialScreenshot']['id'];?>">
						
                        <td width="20%" >
							<?php echo $snapData['TutorialScreenshot']['title'].'</br>'. $this->Html->image('snapshots/'.$snapData['TutorialScreenshot']['image'],array('height'=>'50px','width'=>'50px')); ?>
							<a href="javascript:void(0);" style="padding-left:5px;" onclick="confirmbox(this.id,<?php echo $snapData['TutorialScreenshot']['id']; ?>);" id="delete_<?php echo $snapData['TutorialScreenshot']['id']; ?>"><?php echo $html->image('close.gif'); ?></a>
                        </td>
						
                    </tr>
			<?php } }else{
						echo "<tr><td>No Screenshot Uploaded</td></tr>";
					}?>		
					
                </table>
                <br/>
                <br/>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                           <h1>Educational Text Tutorial</h1> 
                        </td>
                    </tr>
					<?php if(!empty($textData)){?>
                    <tr>
                        <th>
                           Tutorial  
                        </th>
                        <th>&nbsp;</th>
                    </tr>
					<?php foreach($textData as $tData){ ?>
					<tr id="TxtTd_<?php echo $tData['TutorialText']['id'] ?>">
						
                        <td width="80%" id="TxtTd_<?php echo $tData['TutorialText']['id'] ?>">
                           <?php echo $tData['TutorialText']['tutorial']; ?>
						   
                        </td>
						<td width="80%"><a href="javascript:void(0);" style="float:right;padding-top:5px;" onclick="confirmbox(this.id,<?php echo $tData['TutorialText']['id']; ?>);" id="deletetxt_<?php echo $tData['TutorialText']['id']; ?>"><?php echo $html->image('close.gif'); ?></a>
						<?php //echo $html->link('Edit',array('controller'=>'admins','action'=>'editTutorial'),array('class'=>'')) ?></td>
						
                    </tr>
			<?php } } else{
						 echo "<tr><td>No Text Added</td></tr>";
						} ?>		
					
                </table>

            </div>
        </div>
            <!--[if !IE]>end table_wrapper<![endif]-->					
    </div>
    </div>
    <!--[if !IE]>end section inner<![endif]-->
        	
				
						
</div>
<script type="text/javascript">
function playVideo(vidPath,thumbPath)
{	var str = '';
	str+='<div id="video">';
	//str+='<object width="220" height="21" type="application/x-shockwave-flash" data="/img/OriginalThinMusicPlayer.swf">';
	//str+='<param name="movie" value="/img/OriginalThinMusicPlayer.swf" />';
	//str+='<param name="flashvars" value="autoPlay=true&firstColor=0072bc&mediaPath='+audPath+'" />';
	//str+='</object>';
	str+='<object width="550" height="400">';
	
	str+='<param name="movie" value="/players/player_a.swf">';
	str+='<param name="flashvars" value="video='+vidPath+'&streamer=true">';
	str+='<embed src="/players/player_v.swf?video='+vidPath+'&streamer=true" width="550" height="400">';
	//str+='<embed src="/players/player_v.swf?video='+vidPath+'&streamer=true" width="550" height="400">';
	str+='</embed>';
	
	str+='</object>';
	str+='</div>';
	jQuery.facebox(str);
	
}

</script>

