<style>
.error-message{
	position:relative;color:#B86464;	
}
</style>
<?php echo $this->Html->script('fckeditor'); ?>

<?php 
	//echo $crumb->getHtml('Add', 'null','') ;
?>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Edit Inspection Device</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('controller'=>'admins','action'=>'editInspectionDevice') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form'));
            echo $form->hidden('Service.id',array('value'=>$id));
        ?>
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <div class="row">
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		    </span>
  		 </div>	 
		              
                <div class="row">
				<label>Report : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="">
					<?php echo $form->select('Service.report_id',$options,$this->data['Service']['report_id'],array('legend'=>false,'class'=>'text validate[required]','label'=>false));?>
					</span>					
				</div>
		</div>
		              
                <div class="row">
				<label>Device Name : <span class="star">*</span></label>
				<div class="inputs" style="float:left">
					<span class="input_wrapper" style="width:200px; margin-bottom:20px">
					<?php echo $form->text('Service.name', array('maxLength'=>'100','class'=>'text validate[required]',"div"=>false,"label"=>false));  ?>
					</span>
					
				</div>
		</div>
		
			<!--[if !IE]>start row<![endif]-->
		<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
		</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
