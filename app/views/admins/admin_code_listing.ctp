<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery(".ajax").colorbox();
});
</script>
<?php 
  //echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Code Chart</h2>
					
					<div id="product_list_menu">
			                 <!--Start of serach form -->
						<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'codeListing') ,'name'=>'searchform','id'=>'searchform')); ?>
					  <div class="inputs" style="float:left; margin-right: 7px;">
						<span class="">
						<?php echo $form->select('Code.report_id',$options,'',array('legend'=>false,'label'=>false,'empty'=>'Please Select'));?>
						</span>
			                  </div>
			                 <div class="inputs" style="float:right; ">
						<span class="button red_button"><span><span>Search</span></span><?php echo $form->submit('Search',array('action'=>''));  ?></span>
			                  </div>
					  <?php echo $form->end(); ?>
					  <!--End of serach form -->
						<?php if(isset($reportid) && !empty($reportid)){ ?>
						<!--<a href="/admin/admins/codeListing" class="update"><span><span><em>Back</em><strong></strong></span></span></a>-->
						<a href="/admin/admins/addCode" class="update"><span><span><em>Add New</em><strong></strong></span></span></a>
						<?php } else { ?>
						<a href="/admin/admins/addCode" class="update"><span><span><em>Add New</em><strong></strong></span></span></a>
						<?php } ?>
					</div>				
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						 <?php echo $this->Form->create('', array('controller'=>'admin','action' => 'codeListing','name'=>'webpages','id'=>'WebsitepagesShowForm') );?>
						<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
								<th><input type="checkbox"  name='checkall' onclick='checkedAllpage();' id='checkall'></th>																
								<th width='15%'><?php echo $paginator->sort('Report Name','report.name');?> </th>
								<th width='15%'><?php echo $paginator->sort('Code','Code.code');?> </th>
								<th width='35%'><?php echo $paginator->sort('Description','Code.description');?> </th>
								<th width='20%'><?php echo $paginator->sort('Code Type','Code.code_type_id');?> </th>
								<th width='10%'><?php echo $paginator->sort('Status','Code.status');?> </th>								
								<th width='5%'>Actions</th>
							</tr>
							<?php  if(count($codeListing)) {  ?>
						<tbody>		
					<?php $i=1; foreach ($codeListing as $codelist): 						
          	//$activeImg = $userlist['Admin']['status']==1 ?'/img/icons/status_star_green.gif' : '/img/icons/status_star_red.gif'; ?>

							<tr class="first">
								<td><input type="checkbox" name="box[]" value="<?php echo $codelist['Code']['id']; ?>" onclick='uncheck(this);'></td>
								<td><?php echo $codelist['Report']['name']; ?></td>
                <td><?php echo $codelist['Code']['code']; ?></td>
								<td><?php echo $this->Html->link(substr(strip_tags($codelist['Code']['description']),0,60)."...",array('controller'=>'admins','action'=>'codeDescription',base64_encode($codelist['Code']['id'])), array('class' => 'ajax','title'=>"",'escape'=>false)); ?></td>
								<td><?php echo $codelist['CodeType']['code_type']; ?></td>
								<td><?php echo $codelist['Code']['status']=='1'?__('Active'):__('Inactive'); ?></td>
								
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:150px'>			
											<li><?php echo $html->link('Edit',array('action'=>'editCode',base64_encode($codelist['Code']['id'])),array('class'=>'edit')) ?></li>
											<!--<li><?php //echo $html->link('Change Password',array('action'=>'changepassword',$userlist['Admin']['id']),array('class'=>'action2')) ?></li>-->
											<li><?php echo $html->link('Delete',array('action'=>'deleteCode',$codelist['Code']['id']),array('class'=>'delete'),'Are you sure you want to delete this Code?') ?></li>
											
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
						<table>
                            <tr>
                                <td><?php echo $this->Form->submit("Active",array('id'=>'active','div'=>false,'class'=>'iconlink','name' => 'active',"onClick"=>'return prompt_activepage();')); ?></td>
                                <td><?php echo $this->Form->submit("Inactive",array('id'=>'Inactive','div'=>false,'class'=>'iconlink','name' => 'inactive',"onClick"=>'return prompt_inactivepage();')); ?></td>
				<td><?php echo $this->Form->submit("View All",array('div'=>false,'class'=>'iconlink','name' => 'viewall')); ?></td>
                                
                            </tr>
						</table>
                        </table>
							
<?php } else { ?>
	<tr class="first"><td colspan='6'>No record found</td> </tr>
<?php } ?>
</table>
						 <?php echo $this->Form->end(); ?>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->
				<?php if(isset($reportid) && !empty($reportid)){
					     $paginator->options(array('url'=> array(
						'controller' => 'admins', 
						'action' => 'codeListing',
						 "rid"=>$reportid))
						);
			               }
				        if(!(isset($viewall) && $viewall == "allData")){
			         ?>
				  
					<div class="pagination">
						
					<span class="page_no">Page <?php echo $paginator->counter(); ?></span>
    			<ul class="pag_list">
    				<li class="prev" style="display:block;" ><?php echo $paginator->prev('Previous'); ?> </li>
    				<li><?php echo $paginator->numbers(); ?></li>
    				<li class="next" ><?php echo $paginator->next('Next'); ?></li>
    			</ul>	
										
					</div>
					<!--[if !IE]>end pagination<![endif]-->
			<?php } ?>	
				</div>
		
				<!--[if !IE]>end section<![endif]-->
				
<script>
function prompt_activepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	

	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
	
      }
      
      if(flag==1)
      {
	      if(confirm('Are you sure you want to make this deficiency code active ?')){
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one deficiency code to active");
      return false;
      }
}

function prompt_inactivepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	
	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
      }
      
      if(flag==1)
      {
      
	      if(confirm('Are you sure you want to make this deficiency code inactive ?')){
		//document.webpages.action = '/navcake/admin/websitepages/inactive';
		//document.webpages.submit;
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one deficiency code to inactive");
      return false;
      }
}

function checkedAllpage ()
      {        
        if (document.getElementById('checkall').checked == false)
        {
        checked = false
        }
        else
        {
        checked = true
        }
        for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++)
        {
          document.getElementById('WebsitepagesShowForm').elements[i].checked = checked;
        }
      }
</script> 				