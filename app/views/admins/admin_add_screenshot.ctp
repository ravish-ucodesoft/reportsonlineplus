<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
?>
<script type="text/javascript">
function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxuploadSnapshot',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#ProfileImage").val(responseJSON.profile_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/screenshots/'+responseJSON.profile_image+'">');
	    }
            });           
    }        
	window.onload = createUploader;
</script>
<style>
  .qq-uploader {
    margin-left: 6px;
    position: relative;
    width: 200px;
}  
    
</style>

<div id="content">
     <div id="box" style="width:1000px;padding-left:10px;">
	 
      <?php echo $form->create('',array('controller'=>'admins','action' => 'addScreenshot','method'=>'post','id'=>'composeMszForm','name'=>'ListForm','enctype'=>'multipart/form-data'));?>
     
     <div class="form-box" style="width:90%">
   
        <h3 id="adduser"> Add Screenshot</h3>
        
        <table class="register-form" style="border:1px dotted black" width="100%">
            <tr><td colspan="2">&nbsp;
            
            </td></tr>
       
         <tr><td>Upload Screenshot <span class="star">*</span></td>
			<td><?php echo $this->Form->input('TutorialScreenshot.image',array('type'=>'file','label'=>'','readonly'=>'','div'=>false,'label'=>false)); ?></td>
			</tr>
			<tr><td>Title: <span class="star">*</span></td>
			<td><?php echo $this->Form->input('TutorialScreenshot.title',array('type'=>'text','label'=>'','readonly'=>'','div'=>false,'label'=>false,'size'=>50)); ?></td>
			</tr>
         <tr>       
        <tr>
           <td>&nbsp;</td> <td><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'float:left;')); ?></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        </table>
    </div>
    <!--one complete form ends-->

  <?php echo $form->end(); ?>



</div>
</div>