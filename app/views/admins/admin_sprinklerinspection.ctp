<?php
    echo $this->Html->css('manager/theme.css');
    echo $this->Html->css('manager/style.css');
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));    
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
    echo $this->Html->css(array('validationEngine.jquery','ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('jquery.validationEngine.js','ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js')); 
     echo $this->Html->script(array('ajaxuploader/fileuploader'));
    
?>

<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript">
 jQuery(function(){
    jQuery(".calender").datepicker({ dateFormat: 'mm/dd/yy',showOn: 'button', buttonImage: '/img/calendar.gif', buttonImageOnly: true
    });});
</script>
<script type="text/javascript">
$(document).ready(function(){
	//jQuery(".ajax").colorbox();
	jQuery(".inline").colorbox({inline:true, width:"50%"});	


});
			$(function(){
				// Accordion
				$("#accordion").accordion({ header: "h3" });
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); },
					function() { $(this).removeClass('ui-state-hover'); }
				);
			});
 function limit(field, chars) {	
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }		
    
    function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/reports/ajaxuploadAttachment',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){
			jQuery("#uploadattachment").val(responseJSON.attachment);jQuery("#separate-list").hide();
			jQuery("#uploaded_picture").html(responseJSON.attachment);
	    }
            });           
    }        
window.onload = createUploader;
		
			/*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
     function checkUploadedAttachment(){
	var uploadVal = jQuery("#uploadattachment").val();
	if(uploadVal != ""){
		return true;
	}else{
		alert("Please upload a file");
		return false;
	}
 }
  function checkNotification(){
	var notifyVal = jQuery("#SprinklerReportNotificationNotification").val();
	if(notifyVal != ""){
		return true;
	}else{
		alert("Please add notification");
		return false;
	}
 }
 
 
 function RemoveAttachment(attachId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delsprinklerattachment/" + attachId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#p_'+attachId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmattach(attachId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveAttachment(attachId);    
  }
  else{
    return false;
  }
 }
function RemoveNotifier(notifierId){
       jQuery.ajax({
                type : "GET",
                url  : "/reports/delsprinklernotifier/" + notifierId,                
                success : function(opt){
		    if(opt == 1){
		       jQuery('#pnotify_'+notifierId).remove();
		       jQuery("#deleteFlashmessage").show();		       
		    }
                }
        });
}

function confirmnotifier(notifierId){
  var r = confirm("Are you sure you want to delete this record");
  if (r==true){
     RemoveNotifier(notifierId);    
  }
  else{
    return false;
  }
 }  
		</script>
		
		<style type="text/css">
			/*demo page css*/
			/*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
                        .ui-accordion-content{}
		</style>
<style>
.message{text-align:center;color:#54A41A;font-weight:bold;}
</style>
<div id="content" >
	      <div class='message'><?php echo $this->Session->flash();?></div>
              <div id="box">
	      
                	<h3  align="center" >Sprinkler Inspection Report</h3>
                	 
                	    
                        <br/>
			<table class="tbl">
			       	<tr>
                                    <td><b>Owner and Owners onsite Represtor </b> </td>
                                    <td> <?php echo $form->input('SprinklerReport.owner_onsite_represtor',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px;')); ?></td>
                                </tr>
					<tr>
                                            <td><b>Owner's Address/phone & Owners onsite Represtor </b> </td>
                                            <td><?php echo $form->input('SprinklerReport.owner_onsite_address',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px;')); ?> &nbsp;&nbsp; <?php echo $form->input('SprinklerReport.owner_onsite_phone',array('type'=>'text','maxlength'=>20,'class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px;')); ?></td>
                                        </tr>
					<tr>
                                             <td><b>Property being Inspected</b> </td>
                                             <td><?php echo $form->input('SprinklerReport.propert_inspected',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px;')); ?></td>
                                        </tr>
					<tr><td><b>Property Address:</b></td><td>
                                                <?php echo $form->input('SprinklerReport.property_address',array('id'=>'address','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px; height:45px;')); ?></td>
                                        </tr>
					<tr>
					    <td width="20%"><b >Date of Work</b></td><td><?php echo $form->input('SprinklerReport.date_of_work',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?></td></tr>
					<tr><td  width="20%"><b>This inspection is </b></td><td>
                                        <?php
						$data=array("monthly"=>"Monthly","Quarterly"=>"Quarterly","Annual"=>"Annual","ThirdYear"=>"ThirdYear","FifthYear"=>"FifthYear");
                                                echo $form->select('SprinklerReport.inspection_type',$data,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:190px;'));						
						?>
                                            </td>
                                        </tr>
					<tr>
                                            <td  colspan="2"><b>Note: A) All questions are to be answered Yes, No or NA. All answers are to be explained in Def/Rec section.</b></td></tr>
					<tr>
                                            <td colspan="2"><b>Note: B) Inspection,Testing and Maintenance</b></td></tr>
					    <tr><td width="20%"><b>Service Provider</b></td><td><?php echo $form->input('SprinklerReport.service_provider',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px;','readonly'=>'readonly','value'=>@$result['User']['fname']." ".@$result['User']['lname'])); ?></td></tr>
			
		    			<tr><td width="20%"><b>SP's Address,Phone Number</b></td><td> <?php echo $form->input('SprinklerReport.sp_address',array('id'=>'address','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px; height:45px;','readonly'=>'readonly','value'=>@$result['Company']['address'])); ?>&nbsp;&nbsp;<?php echo $form->input('SprinklerReport.sp_phone',array('id'=>'address','type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px;','readonly'=>'readonly','value'=>@$result['User']['phone'])); ?></td></tr>
					<tr><td width="20%"><b>Inspectors</b></td><td><?php echo $form->input('SprinklerReport.inspectors',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false,'style'=>'width:180px;')); ?></td></tr>
					<tr>
						    <td width="20%"><b >Start Date of the Servic</b></td><td><?php echo $form->input('SprinklerReport.start_date_service',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?></td></tr>
					<tr>
						    <td width="20%"><b >End Date of the Service</b></td><td><?php echo $form->input('SprinklerReport.end_date_service',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?></td></tr>
					<tr>
						    <td width="20%"><b >Date of Performed Work</b></td><td><?php echo $form->input('SprinklerReport.date_performed_work',array('maxLength'=>'10','type'=>'text','div'=>false,"label"=>false,'style'=>'width: 180px; margin-right: 3px;','class'=>'calender','readonly'=>'readonly')); ?></td></tr>
					<tr><td  width="20%"><b>Deficiency yes or no</b></td><td><?php
						$data1=array("Yes"=>"Yes","No"=>"No");
                                                echo $form->select('SprinklerReport.deficiency',$data1,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:190px;'));
						?></td></tr>
					<tr><td  width="20%"><b>Recommendations yes or no  </b></td><td><?php
                                                echo $form->select('SprinklerReport.recommendations',$data1,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:190px;'));
						?></td></tr>
					<tr><td  width="23%"><b>Inspection 100% complete and Passed</b></td><td><?php
                                                echo $form->select('SprinklerReport.inspection',$data1,null,array('empty'=>'Select','maxLength'=>'10','class'=>'validate[required]','div'=>false,"label"=>false, 'style'=>'width:190px;'));
						?></td></tr>
			    </table>
			
			<div id="accordion">
			<div>
				<h3><a href="#" id=""> 1. GENERAL(To be answered by Customer.)</a></h3>
				<div >
				<?php     echo $this->element("sprinklerinspection/sp_general");    ?>
				</div>
			</div>
                               
			<div>
				<h3><a href="#" id="65">2. CONTROL VALVES</a></h3>
				<div>
                                 <?php echo $this->element("sprinklerinspection/sp_control"); ?>
			</div>
                                
			<div>
				<h3><a href="#">3. WATER SUPPLIES</a></h3>
				<div>
					 <?php  echo $this->element("sprinklerinspection/sp_watersupplies");   ?>	</div>
			</div>
			<div>
				<h3><a href="#"> 4. TANKS, PUMPS, FIRE DEPT. CONNECTION</a></h3>
				<div>
                                  <?php echo $this->element("sprinklerinspection/sp_tankconnection");   ?>  
                        </div>
                                
			<div>
				<h3><a href="#" id="42">5. WET SYSTEMS</a></h3>
				<div>
                                 <?php  echo $this->element("sprinklerinspection/sp_wet");   ?>
				</div>
			</div>
                                
                            
			<div>
				<h3><a href="#" id="43">6. DRY SYSTEM</a></h3>
				<div>
                                  <?php  echo $this->element("sprinklerinspection/sp_dry");   ?>
				</div>
			</div>
                              
			<div>
				<h3><a href="#">7. SPECIAL SYSTEMS</a></h3>
				<div>
				 <?php  echo $this->element("sprinklerinspection/sp_system");   ?>
				</div>
			<div>
				<h3><a href="#">8. ALARMS</a></h3>
				<div>
				 <?php  echo $this->element("sprinklerinspection/sp_alarm");   ?>
			</div>
			<div>
				<h3><a href="#">9. SPRINKLERS - PIPING</a></h3>
				<div>
                                 <?php  echo $this->element("sprinklerinspection/sp_piping");   ?>
			</div>
		</div>
                       <!--<table class="tbl">
                      <div align="left">
                      <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
                      </div>
                      </td>
                      </tr>
                      </table>--> 

                    	
                </div>
</div>

  
		
	