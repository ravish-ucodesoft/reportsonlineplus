<?php echo $crumb->getHtml('My Account', 'null','null' ); ?>
<!--[if !IE]>start section<![endif]-->
<div class="section">
	<div class="title_wrapper">
		<h2>My Account</h2>		
	</div>
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>'account' ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>	
	<?php echo $form->hidden('Admin.id', array('maxLength'=>'40','class'=>'text')); ?>  
  	<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms">				
			<!--[if !IE]>start row<![endif]-->
			<div class="row">
				<label>User Name:<span class="message">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Admin.username', array('maxLength'=>'70','class'=>'text','readonly'=>true)); ?>
					</span>
					<span class="system negative"><?php echo $form->error('Admin.username'); ?></span>
				</div>
			</div>
			
			<div class="row">
				<label>Email:<span class="message">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Admin.email', array('maxLength'=>'40','class'=>'text'));  ?>
					</span>
					<span class="system negative"><?php echo $form->error('Admin.email'); ?></span>
				</div>
			</div>			
			
			<div class="row">
				<label>Page Limit:<span class="message">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Admin.pagelimit', array('maxLength'=>'40','class'=>'text'));  ?>
					</span>
					<span class="system negative"><?php echo $form->error('Admin.pagelimit'); ?></span>
				</div>
			</div>
			
			<div class="row">
				<label>Pay pal Account:</label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Admin.paypalid', array('maxLength'=>'40','class'=>'text'));  ?>
					</span>
					<span class="system negative"><?php echo $form->error('Admin.paypalid'); ?></span>
				</div>
			</div>
			
			<!--[if !IE]>start row<![endif]-->
			<div class="row">
				<div class="inputs">
					<span class="button orange_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
			</div>
			<!--[if !IE]>end forms<![endif]-->		
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->		
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->