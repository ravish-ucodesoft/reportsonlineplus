<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery(".ajax").colorbox();
});
</script>
<?php 
  //echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">
			<div class='message'><?php echo $this->Session->flash();?></div>
				<div class="title_wrapper">					
					<h2>Service Provider List</h2>
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner" >							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
							<?php echo $this->Form->create('', array('controller'=>'admin','action' => 'serviceProviderList','name'=>'webpages','id'=>'WebsitepagesShowForm') );?>
						<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
								<th rowspan="2" width='5%'><input type="checkbox"  name='checkall' onclick='checkedAllpage();' id='checkall'></th>																
								<th rowspan="3" width='30%' style="color: #4A6300;">Profile Pic </th>								
								<th rowspan="3" width='20%'><?php echo $paginator->sort('Email','email');?></th>
								
								<th rowspan="3" width='10%' style="color: #4A6300;"><?php echo 'Inspector';?> </th>
								<th rowspan="3" width='10%' style="color: #4A6300;"><?php echo 'Client';?> </th>
								<th rowspan="3" width='10%' style="color: #4A6300;"><?php echo 'Status';?></th>
								<th colspan="3" width='5%' style="color: #4A6300;">Leads</th>
								<th rowspan="3" width='5%' style="color: #4A6300;">Actions</th>
							</tr>
						<tr>
							<th style="color: #4A6300;">Accepted</th>
							<th style="color: #4A6300;">Declined</th>
							<th style="color: #4A6300;">Pending</th>
						</tr>
							<?php  if(count($providerListing)) { ?>
						<tbody>		
					<?php $i=1; foreach ($providerListing as $providerlist):
					
					
          	//$activeImg = $userlist['Admin']['status']==1 ?'/img/icons/status_star_green.gif' : '/img/icons/status_star_red.gif'; ?>

							<tr class="first">
								<td><input type="checkbox" name="box[]" value="<?php echo $providerlist['User']['id']; ?>" onclick='uncheck(this);'></td>
								<td style="padding:10px"><?php echo $html->image($this->Common->getUserProfilePhoto($providerlist['User']['profilepic']),array('style'=>'vertical-align:middle')); ?><br/><?php echo ucwords($providerlist['User']['fname'].' '.$providerlist['User']['lname']); ?></td>
								<td><?php echo $providerlist['User']['email'];?><br/>Company: <?php echo @$providerlist['Company']['name'];?><br/>Address: <?php echo @$providerlist['Company']['address'];?></td>
								<td><?php
								echo $this->Common->getno_of_inspector($providerlist['User']['id']);
								  ?></td>
								<td><?php echo $this->Common->getno_of_client($providerlist['User']['id']); ?></td>
								<td>
								<?php echo $providerlist['User']['deactivate']=='1'?$this->Html->image('/img/icons/status_star_green.gif') :$this->Html->image('/img/icons/status_star_red.gif') ; ?></td>
								<td><?php echo $this->Common->acceptLeadCount($providerlist['User']['id']); ?></td>
								<td><?php echo $this->Common->declineLeadCount($providerlist['User']['id']); ?></td>
								<td><?php echo $this->Common->pendingLeadCount($providerlist['User']['id']); ?></td>
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:150px'>			
											<li>          
								<?php echo $this->Html->link("View Detail",array('controller'=>'admins','action'=>'providerDescription',base64_encode($providerlist['User']['id'])), array('class' => 'ajax','title'=>"",'escape'=>false)); ?>
								<?php echo $this->Html->link("Change Password",array('controller'=>'admins','action'=>'changeSpPassword',base64_encode($providerlist['User']['id'])), array('class' => 'ajax','title'=>"",'escape'=>false)); ?>
								<?php echo $this->Html->link("Delete",array('controller'=>'admins','action'=>'SpDelete',base64_encode($providerlist['User']['id'])), array('confirm' => 'Are you sure you wish to delete?','class' => '','title'=>"",'escape'=>false)); ?>
								
											</li>
											<!--<li><?php //echo $html->link('Change Password',array('action'=>'changepassword',$userlist['Admin']['id']),array('class'=>'action2')) ?></li>-->
											<li><?php //echo $html->link('Delete',array('action'=>'delete',$userlist['Admin']['id']),array('class'=>'delete'),'Are you sure you want to delete this admin staff?') ?></li>
											
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
						<table>
                            <tr>
                                <td><?php echo $this->Form->submit("Active",array('id'=>'active','div'=>false,'class'=>'iconlink','name' => 'active',"onClick"=>'return prompt_activepage();')); ?></td>
                                <td><?php echo $this->Form->submit("Inactive",array('id'=>'Inactive','div'=>false,'class'=>'iconlink','name' => 'inactive',"onClick"=>'return prompt_inactivepage();')); ?></td>
                                
                            </tr>    
                        <table>
							
<?php } else { ?>
	<tr class="first"><td colspan='6'>No record found</td> </tr>
<?php } ?>
</table>
 <?php echo $this->Form->end(); ?>
						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->
				
					<div class="pagination">
						
					<span class="page_no">Page <?php echo $paginator->counter(); ?></span>
    			<ul class="pag_list">
    				<li class="prev" style="display:block;" ><?php echo $paginator->prev('Previous'); ?> </li>
    				<li><?php echo $paginator->numbers(); ?></li>
    				<li class="next" ><?php echo $paginator->next('Next'); ?></li>
    			</ul>	
										
					</div>
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->
				
<script>
function prompt_activepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	

	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
	
      }
      
      if(flag==1)
      {
	      if(confirm('Are you sure you want to make this service provider active ?')){
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one service provider to active");
      return false;
      }
}

function prompt_inactivepage()
{
      var flag=0;
      for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++) 
      {
	
	if(document.getElementById('WebsitepagesShowForm').elements[i].checked == true)
	{
	      flag=1;
	}
	
      }
      
      if(flag==1)
      {
      
	      if(confirm('Are you sure you want to make this service provider inactive ?')){
		//document.webpages.action = '/navcake/admin/websitepages/inactive';
		//document.webpages.submit;
		return true;
	      }else{
		      return false;
	      }
      }
      else
      {
      alert("Please select atleast one service provider to inactive");
      return false;
      }
}

function checkedAllpage ()
      {        
        if (document.getElementById('checkall').checked == false)
        {
        checked = false
        }
        else
        {
        checked = true
        }
        for (var i = 0; i < document.getElementById('WebsitepagesShowForm').elements.length; i++)
        {
          document.getElementById('WebsitepagesShowForm').elements[i].checked = checked;
        }
      }
</script> 				