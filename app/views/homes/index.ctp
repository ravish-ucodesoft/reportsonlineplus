    <?php echo $this->element('front/banner_element');?>
		    
		   
		   <!-- Container Starts-->
           <section class="container">
               <!-- Container Left -->
               <section class="cont_l">
                      <section class="content_box">
                             <section class="content_box_title gears"><h2>Service Companies</h2></section>
                             <section class="content_box_txt">
                                <p><?php echo $tabData['MainTab']['description1']; ?></p>
<section class="txt_center"><a href="/users/signup" class="cont_btn">Register for Services Companies</a></section>
                             </section>
                      </section>
               </section>
               
               <!-- Container Right -->
               <section class="cont_r">
                      <section class="content_box">
                             <section class="content_box_title home"><h2>Property Owners</h2></section>
                             <section class="content_box_txt">
                                <p><?php echo $tabData['MainTab']['description2']; ?></p>
<section class="txt_center"><a href="/users/clientsignup" class="cont_btn">Register for Property Owner</a></section>
                             </section>
                      </section>
               </section>
               <section class="clear"></section>
               
               <!-- Three Boxes -->
               <section class="three_boxes">
                    <!-- Questions? -->
                    <section class="ques_bg lt">
                         <section class="ques_txt">
                                <h1>Do you have questions  about life safety inspections  or Code?</h1>
				<?php echo $html->link('Click here',array('controller'=>'homes','action'=>'codeInspectionQuery'),array('class'=>'clickhere ajax'));?>
                                <!--<a href="#" class="clickhere">Click here</a>-->
                         </section>
                    </section>
                    
                    <!-- Free Site Survey -->
                    <section class="survey_bg lt">
                         <section class="survey_txt">
                                <h1>Free Site Survey</h1>
                                <section class="grey_btn"><a href="<?php echo $html->url(array('controller'=>'users','action'=>'clientsignup'));?>" class="clickhere"><span>Click here</span></a></section>
                         </section>
                    </section>
                    
                    <!-- Up to date -->
                    <section class="uptodate_bg lt">
                         <section class="uptodate_txt">
                                <h1>Are you up to Date?</h1>
                                <p>with your Fire Safety</p>
                                <section class="red_btn"><a href="<?php echo $html->url(array('controller'=>'homes','action'=>'up_to_date'));?>" class="clickhere"><span>Click here</span></a></section>
                         </section>
                    </section>
               </section>               
           </section>
           <!-- Container Ends-->