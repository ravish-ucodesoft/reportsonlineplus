 <!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	<?php echo $form->create('homes',array('action'=>'customerservice','id'=>'customerform','name'=>'customerform')); ?>
                    <!-- Faq Content Starts -->
                    <div class="faq-section">
                    <h2>Customer Service</h2>
                        <div class="content faq-content">
                        <div align="center" style="color:red;"><?php echo $this->Session->flash();?></div>
                        <p>We offer live customer service from 8:00 A.M. to 5:00 P.M. CST, Monday through Friday. To reach a customer service representative, please call us at 888-557-5297.<br><br>
For inquiries after 5:00 P.M. CST M-F, you may contact us using the form below, or you can e-mail us at contact@schedulepal.com. <br><br>

We appreciate your business!</p><br >
                           <div class="contact-form">
                           		
                                <div class="new-form">
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Name:</label><?php echo $form->input('Customerservice.name',array('label'=>'','div'=>'','maxlength'=>'30','class'=>'name-input','error'=>false));?>
                                     <div style="padding-left:150px;color:red;"><?php echo $form->error('Customerservice.name'); ?></div>
                                    </div>
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Email Address:</label><?php echo $form->input('Customerservice.email',array('label'=>'','maxlength'=>'50','class'=>'name-input','error'=>false));?>
                                      <div style="padding-left:150px;color:red;"><?php echo $form->error('Customerservice.email'); ?></div>
                                    </div>
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Phone Number:</label><?php echo $form->input('Customerservice.phone',array('label'=>'','maxlength'=>'30','class'=>'name-input','error'=>false));?>
                                      <div style="padding-left:150px;color:red;"><?php echo $form->error('Customerservice.phone'); ?></div>
                                    </div>
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Enter Your Message:</label><?php echo $form->input('Customerservice.textmessage',array('type'=>'textarea','label'=>'','class'=>'message-in','error'=>false));?>
                                      <div style="padding-left:150px;color:red;"><?php echo $form->error('Customerservice.textmessage'); ?></div>
                                    </div>
                                    <div class="inner-new-form">
                                    	
                                       <label style="color:#454545">Enter the text shown: </label>
                                       <div>
                                    <p><img src="<?php echo $captcha_image_url; ?>" id="captcha" alt="CAPTCHA Image" />
                                    </p>
                                    <br/><label></label>
                                    <input type="text" name="data[Customerservice][captcha_code]" size="10" maxlength="6" value="" style="border:3px solid #C7E5C8;color:#666666"/>
                                    <div style="padding-left:150px;color:red;"><?php if(isset($error_captcha)) { echo @$error_captcha; } ?></div>
                                    </div>
                                    </div>
                                    <div class="inner-new-form">
                                    	<div class="button-section">
                                			<?php echo $form->button('<span>Submit</span>',array('escape'=>false,'class'=>"green"));?>
                                		</div>
                                    </div>
                                </div>                                
                           </div>
                        </div>
                     </div>
                    <!-- Faq Content Ends -->
                    </form>      
                      <?php  echo $this->element('right_section_front'); ?>
                                                            
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends -->
 
 
 