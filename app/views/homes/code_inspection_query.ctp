<?php
    echo $this->Html->script('jquery.1.6.1.min');    
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');    
?>
<?php echo $html->css('newdesign_css/style'); ?>
<script type="text/javascript">
jQuery(document).ready(function(){    
    jQuery("#inspectionForm").validationEngine();
});
</script>
<!-- Inner Starts-->
<section class="inner_full">
   <section class="inner_l">  
    <h2>Do you have questions about life safety inspections or Code?</h2>
    <div class='message'><?php echo $this->Session->flash();?></div>    
    <?php echo $form->create('Inquiry',array('id'=>'inspectionForm','url'=>array('controller'=>'homes','action'=>'codeInspectionQuery'),'onsubmit'=>'close_box(); return false;'));?>
        <ul class="sign-form">
	     <li>
	       <label> Your Name<span class="astric">*</span> </label>
	       <div class="input-cont">		 
                 <?php echo $form->input('Inquiry.name',array('maxLength'=>100,'class'=>'search_bg validate[required]','label'=>false, 'div'=>false));?>
	       </div>
	     </li>
	     <li>
	       <label>Your Email<span class="astric">*</span> </label>
	       <div class="input-cont">		 
                 <?php echo $form->input('Inquiry.email',array('maxLength'=>100,'class'=>'search_bg validate[required,custom[email]]','label'=>false, 'div'=>false));?>
	       </div>
	     </li>
	     <li>
	       <label>Query<span class="astric">*</span> </label>
	       <div class="input-cont">		  
                  <?php echo $form->textarea('Inquiry.query',array('class'=>'search_bg validate[required]','label'=>false, 'div'=>false, 'rows'=>3, 'cols'=>22));?>
	       </div>
	     </li>
             <li>
                <label>&nbsp;</label>
                <section class="login_btn">
                    <span>
                        <input name="Submit" type="submit" value="Submit" class="" />
		    </span> 
                </section>
	     </li>
        </ul>
        <?php echo $form->end(); ?>
        </section>
</section>
<style type="text/css">
    div#cboxContent{
        background:#3C444F;
    }
</style>
<script type="text/javascript">
    function hidePopup(){ 
        jQuery("#cboxOverlay").attr('style','display: none;');
        jQuery("#colorbox").attr('style','display: none; padding-bottom: 42px; padding-right: 42px;');
    }
    function close_box(){
        if(jQuery('#inspectionForm').validationEngine('validate')){
            $.ajax({
                url : '<?php echo BASE_URL?>homes/saveQuery',
                data : jQuery("#inspectionForm").serialize(),
                type : 'post',
                success : function(msg){
                    if(msg=='OK'){
                        jQuery(".message").html('Your Query has been submitted successfully').css('color','#ff0000');
                        setTimeout(hidePopup, 3000);
                    }else{
                        alert('There is an error in submitting the query. Please try again');
                    }
                }
            });
            return false;
        }
        return false;
    }
</script>