<style>

.message{ font-weight:bold;font-size:12px;color:red;}
</style>


<!-- Inner Starts-->
               <section class="login_wrap">
					<section class="member_login">
                        <section class="member_bg">
                           <section class="member_grain_bg">
                             <h3 style="padding-left:24px;">Welcome to ReportOnlinePlus</h3>
							 <div align="center"><span id="err1"><?php echo $this->Session->flash();?></span></div>
                             <?php echo $form->create('', array('type'=>'POST', 'url'=>array('controller'=>'homes','action'=>'login'),'id'=>'login')); ?>
                                 <ul class="login_form">
									
                                    <li>
                                        <label>Email</label>
                                        <section class="input_wrap">											
											<?php 
                      if(isset($urlemail)){  
                        echo $form->input('User.email', array('maxLength'=>'50','class'=>'text validate[required,custom[email]]',"div"=>false,"label"=>false, 'value' => $urlemail));
                      }
                        else{
                      echo $form->input('User.email', array('maxLength'=>'50','class'=>'text validate[required,custom[email]]',"div"=>false,"label"=>false)); 
                      }
                       ?>
										</section>
                                    </li>
                                    <li>
                                        <label>Password</label>
                                        <section class="input_wrap">
											<?php echo $form->input('User.password', array('maxLength'=>'50','class'=>'text password validate[required]',"type"=>"password","div"=>false,"label"=>false));  ?>
										</section>
                                    </li>
									<li>
                                        <label><?php echo $form->input('User.remember_me', array("type"=>"checkbox","div"=>false,"label"=>false));
                                        ?><span style="font-size: 10px;
    font-weight: bold;">Remember me</span>
                                        </label>
                                        <section class="login_btn"><?php echo $html->link('Forgot Password ?',array('controller'=>'homes','action'=>'forgotpassword'),array('class'=>'forgot'))?><span><input type="submit" value="Login" /></span></section>
                                    </li>
                                    
                                 </ul>
                             <?php echo $form->end(); ?>
                           </section>
                        </section>
                    </section>
               </section>
               <section class="clear"></section>
           <!-- Inner Ends-->
