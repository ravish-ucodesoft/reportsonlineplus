<?php
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>

<script type="text/javascript">
    jQuery(document).ready(function(){
      
   jQuery("#loginform").validationEngine();
   } 
   );   
</script>

<!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	<?php echo $form->create('', array('type'=>'POST', 'action'=>'login','name'=>'loginform','id'=>'loginform')); ?>
                    <!-- Faq Content Starts -->
                    <div class="faq-section">
                    <h2>Sign In</h2>
                        <div class="content faq-content">
                        <div align="center" class="error-message"><span class="system negative" id="err1"><?php echo $this->Session->flash();?></span></div>
                        
                           <div class="contact-form">
                           		
                                <div class="new-form">
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Email:</label><?php echo $form->input('User.email',array('label'=>'','div'=>'','maxlength'=>'50','class'=>'name-input validate[required] custom[email]'));?>
                                     
                                    </div>
                                    
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Password:</label><?php echo $form->input('User.pwd',array('type'=>'password','label'=>'','div'=>'','maxlength'=>'30','class'=>'name-input validate[required]','error'=>false));?>
                                     
                                    </div>
                                    
                                    <div class="inner-new-form">                                    	
                                      <div style="float:right;padding-right:80px;padding-top:5px;">Have you forgot your password ? <?php echo $html->link('<b>Click Here</b>' ,array('controller'=>'homes','action'=>'forgotpassword'),array('escape'=>false)); ?></div>
                                     
                                    </div>
                                    
                                    <div class="inner-new-form">
                                    	<div class="button-section">
                                			<?php echo $form->button('<span>Submit</span>',array('escape'=>false,'class'=>"green"));?>
                                		</div>
                                    </div>
                                </div>
                           </div>
                        </div>
                     </div>
                    <!-- Faq Content Ends -->
                    </form>      
                      <?php  //echo $this->element('right_section_front'); ?>
                                                            
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends --> 
            
            
