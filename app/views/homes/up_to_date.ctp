<div class="tblContainer roundborder">
    <?php echo $form->create('',array('id'=>'uptoDateForm','url'=>array('controller'=>'homes','action'=>'up_to_date')));?>
    <section class="">
        <section class="radio_btns">
            <?php
                $options = array('1'=>'Sprinkle System','2'=>'Fire Alarm','3'=>'Suppression');        
                $attributes=array('legend'=>false,'class'=>'chkType','value'=>'1');
                echo $form->radio('type',$options,$attributes);
            ?>        
        </section>
        <section class="input_fields">
            Your name <?php echo $form->input('SprinklerSystemInspectionData.name',array('class'=>'search_bg lsmall','div'=>false,'label'=>false));?>
            Your email <?php echo $form->input('SprinklerSystemInspectionData.email',array('class'=>'search_bg lsmall','div'=>false,'label'=>false));?>
        </section>
    </section>
    <div id="sprinkle">
        <table border="1px solid #000" cellspacing="0" cellpadding="0">
            <caption>Inspection and Testing Requirements for Fire Sprinkler Systems</caption>
            <thead> 
                <tr>
                    <th>Sprinkler Systems</th>
                    <th>Monthly</th>
                    <th>Quarterly</th>
                    <th>Semi-annual</th>
                    <th>Annual</th>
                    <th>Other</th>
                    <th>Code</th>
                    <th>NFPA 25</th>
                </tr>    
            </thead>
            <tbody>
                <tr>
                    <td class="boldTxt">Inspection</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>            
                </tr>
                <?php
                    if(sizeof($sprSysInspection)>0){
                        $r = 0;
                        foreach($sprSysInspection as $sprSysIns){
                ?>
                <tr>
                    <?php echo $form->hidden('SprinklerSystemInspectionData.'.$r.'.sprinkle_system_inspection_id',array('value'=>$sprSysIns['SprinklerSystemInspection']['id']));?>
                    <td><?php echo $sprSysIns['SprinklerSystemInspection']['name'];?></td>
                    <td class="chkBox">
                        <?php
                            if($sprSysIns['SprinklerSystemInspection']['monthly']==1){
                                echo $form->input('SprinklerSystemInspectionData.'.$r.'.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysIns['SprinklerSystemInspection']['quartely']==1){
                                echo $form->input('SprinklerSystemInspectionData.'.$r.'.quartely',array('type'=>'checkbox','label'=>false,'div'=>false));                                
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysIns['SprinklerSystemInspection']['semi_annual']==1){                                
                                echo $form->input('SprinklerSystemInspectionData.'.$r.'.semi_annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysIns['SprinklerSystemInspection']['annual']==1){                                
                                echo $form->input('SprinklerSystemInspectionData.'.$r.'.annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysIns['SprinklerSystemInspection']['other']==1){                                
                                echo $form->input('SprinklerSystemInspectionData.'.$r.'.other',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td><?php echo $sprSysIns['SprinklerSystemInspection']['code'];?></td>
                    <td><?php echo $sprSysIns['SprinklerSystemInspection']['nfpa'];?></td>
                </tr>    
                <?php
                        $r++;}
                    }    
                ?>
                <tr>
                    <td class="boldTxt">Testing</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>            
                </tr>
                <?php
                    if(sizeof($sprSysTesting)>0){
                        $t = 0;
                        foreach($sprSysTesting as $sprSysTes){
                ?>
                <tr>
                    <?php echo $form->hidden('SprinklerSystemTestingData.'.$t.'.sprinkle_system_testing_id',array('value'=>$sprSysTes['SprinklerSystemTesting']['id']));?>
                    <td><?php echo $sprSysTes['SprinklerSystemTesting']['name'];?></td>
                    <td class="chkBox">
                        <?php
                            if($sprSysTes['SprinklerSystemTesting']['monthly']==1){                                
                                echo $form->input('SprinklerSystemTestingData.'.$t.'.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysTes['SprinklerSystemTesting']['quartely']==1){                                
                                echo $form->input('SprinklerSystemTestingData.'.$t.'.quartely',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysTes['SprinklerSystemTesting']['semi_annual']==1){                                
                                echo $form->input('SprinklerSystemTestingData.'.$t.'.semi_annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysTes['SprinklerSystemTesting']['annual']==1){                                
                                echo $form->input('SprinklerSystemTestingData.'.$t.'.annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysTes['SprinklerSystemTesting']['other']==1){                                
                                echo $form->input('SprinklerSystemTestingData.'.$t.'.other',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td><?php echo $sprSysTes['SprinklerSystemTesting']['code'];?></td>
                    <td><?php echo $sprSysTes['SprinklerSystemTesting']['nfpa'];?></td>
                </tr>    
                <?php
                        $t++;}
                    }    
                ?>
                <tr>
                    <td class="boldTxt">Maintaince</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>            
                </tr>
                <?php
                    if(sizeof($sprSysMaintaince)>0){
                        $m = 0;
                        foreach($sprSysMaintaince as $sprSysMain){
                ?>
                <tr>
                    <?php echo $form->hidden('SprinklerSystemMaintainceData.'.$m.'.sprinkle_system_maintaince_id',array('value'=>$sprSysMain['SprinklerSystemMaintaince']['id']));?>
                    <td><?php echo $sprSysMain['SprinklerSystemMaintaince']['name'];?></td>
                    <td class="chkBox">
                        <?php
                            if($sprSysMain['SprinklerSystemMaintaince']['monthly']==1){                                
                                echo $form->input('SprinklerSystemMaintainceData.'.$m.'.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysMain['SprinklerSystemMaintaince']['quartely']==1){                                
                                echo $form->input('SprinklerSystemMaintainceData.'.$m.'.quartely',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysMain['SprinklerSystemMaintaince']['semi_annual']==1){                                
                                echo $form->input('SprinklerSystemMaintainceData.'.$m.'.semi_annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysMain['SprinklerSystemMaintaince']['annual']==1){                                
                                echo $form->input('SprinklerSystemMaintainceData.'.$m.'.annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprSysMain['SprinklerSystemMaintaince']['other']==1){                                
                                echo $form->input('SprinklerSystemMaintainceData.'.$m.'.other',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td><?php echo $sprSysMain['SprinklerSystemMaintaince']['code'];?></td>
                    <td><?php echo $sprSysMain['SprinklerSystemMaintaince']['nfpa'];?></td>
                </tr>    
                <?php
                        $m++;}
                    }    
                ?>
            </tbody>
        </table>
        
        <table border="1px solid #000" cellspacing="0" cellpadding="0">    
            <thead> 
                <tr>
                    <th>Valves</th>
                    <th>Monthly</th>
                    <th>Quarterly</th>
                    <th>Semi-annual</th>
                    <th>Annual</th>
                    <th>Other</th>
                    <th>Code</th>
                    <th>NFPA 25</th>
                </tr>    
            </thead>
            <tbody>
                <tr>
                    <td class="boldTxt">Inspection</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>            
                </tr>
                <?php
                    if(sizeof($sprValInspection)>0){
                        foreach($sprValInspection as $sprValIns){
                ?>
                <tr>
                    <td><?php echo $sprValIns['SprinklerValvesInspection']['name'];?></td>
                    <td class="chkBox">
                        <?php
                            if($sprValIns['SprinklerValvesInspection']['monthly']==1){                                
                                echo $form->input('SprinklerValveInspectionDatas.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValIns['SprinklerValvesInspection']['quartely']==1){                                
                                echo $form->input('SprinklerValveInspectionDatas.quartely',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValIns['SprinklerValvesInspection']['semi_annual']==1){                                
                                echo $form->input('SprinklerValveInspectionDatas.semi_annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValIns['SprinklerValvesInspection']['annual']==1){                                
                                echo $form->input('SprinklerValveInspectionDatas.annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValIns['SprinklerValvesInspection']['other']==1){                                
                                echo $form->input('SprinklerValveInspectionDatas.other',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td><?php echo $sprValIns['SprinklerValvesInspection']['code'];?></td>
                    <td><?php echo $sprValIns['SprinklerValvesInspection']['nfpa'];?></td>
                </tr>    
                <?php
                        }
                    }    
                ?>
                <tr>
                    <td class="boldTxt">Testing</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>            
                </tr>
                <?php
                    if(sizeof($sprValTesting)>0){
                        foreach($sprValTesting as $sprValTes){
                ?>
                <tr>
                    <td><?php echo $sprValTes['SprinklerValvesTesting']['name'];?></td>
                    <td class="chkBox">
                        <?php
                            if($sprValTes['SprinklerValvesTesting']['monthly']==1){                                
                                echo $form->input('SprinklerValveTestingDatas.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValTes['SprinklerValvesTesting']['quartely']==1){                                
                                echo $form->input('SprinklerValveTestingDatas.quartely',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValTes['SprinklerValvesTesting']['semi_annual']==1){                                
                                echo $form->input('SprinklerValveTestingDatas.semi_annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValTes['SprinklerValvesTesting']['annual']==1){
                                echo $form->input('SprinklerValveTestingDatas.annual',array('type'=>'checkbox','label'=>false,'div'=>false));                                
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValTes['SprinklerValvesTesting']['other']==1){                                
                                echo $form->input('SprinklerValveTestingDatas.other',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td><?php echo $sprValTes['SprinklerValvesTesting']['code'];?></td>
                    <td><?php echo $sprValTes['SprinklerValvesTesting']['nfpa'];?></td>
                </tr>    
                <?php
                        }
                    }    
                ?>
                <tr>
                    <td class="boldTxt">Maintaince</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>            
                </tr>
                <?php
                    if(sizeof($sprValMaintaince)>0){
                        foreach($sprValMaintaince as $sprValMain){
                ?>
                <tr>
                    <td><?php echo $sprValMain['SprinklerValvesMaintaince']['name'];?></td>
                    <td class="chkBox">
                        <?php
                            if($sprValMain['SprinklerValvesMaintaince']['monthly']==1){                                
                                echo $form->input('SprinklerValveMaintainceDatas.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValMain['SprinklerValvesMaintaince']['quartely']==1){                                
                                echo $form->input('SprinklerValveMaintainceDatas.quartely',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValMain['SprinklerValvesMaintaince']['semi_annual']==1){                                
                                echo $form->input('SprinklerValveMaintainceDatas.semi_annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValMain['SprinklerValvesMaintaince']['annual']==1){                                
                                echo $form->input('SprinklerValveMaintainceDatas.annual',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($sprValMain['SprinklerValvesMaintaince']['other']==1){                                
                                echo $form->input('SprinklerValveMaintainceDatas.other',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td><?php echo $sprValMain['SprinklerValvesMaintaince']['code'];?></td>
                    <td><?php echo $sprValMain['SprinklerValvesMaintaince']['nfpa'];?></td>
                </tr>    
                <?php
                        }
                    }    
                ?>
            </tbody>
        </table>
    </div>
    <div id="fire">        
        <div class="txtContainer">
            <div class="titleDiv">Methods and Frequencies for the Testing, Inspection, Maintenance of Fire Detection and Alarm Systems</div>     
            <div class="paraDiv">
                Fire Safety Services, Inc. has organized the following information regarding the testing and maintenance for fire alarm
                and fire detection systems.  The following has been extracted from the 2002 edition of the National Fire Alarm Code,
                NFPA-72. It is intended as a reference guide only and not a substitute for the actual standard. Please refer to the 2002
                edition of NFPA-72 for definitions, details and procedures.
            </div>
            <div class="titleDiv">
                Testing requirements of NFPA-72:
            </div>
            <div class="paraDiv">
                10.4.3 Testing shall be performed in accordance with the schedules in Table 10.4.3, except as modified in other paragraphs of 10.4.3, or more often if required by authority having jurisdiction.
                <br/><br/>
                Exception:<br/>
                Devices or equipment that is inaccessible for safety considerations (e.g., continuous process operations, energized
                electrical equipment, radiation, and excessive height) shall be tested during scheduled shutdowns if approved by the
                authority having jurisdiction but shall not be tested at intervals exceeding 18 months.
            </div>
            <div class="titleDiv">
                Specific requirements for smoke detector testing from NFPA-72:
            </div>
            <div class="paraDiv">
                10.4.3.4.1 Two or more detectors shall be tested on each initiating circuit annually.
                10.4.3.4.2 Different detectors shall be tested each year, with records kept by the building owner specifying which
                detectors have been tested.
                10.4.3.4.3 Within 5 years, each detector shall have been tested.                
            </div>
            <div class="titleDiv">
                ** Smoke detector sensitivity testing requirements of NFPA-72:
            </div>
            <div class="paraDiv">
                10.4.3.2.1 Sensitivity of smoke detectors and single- and multiple-station smoke alarms (in other than one- and two- 
                family dwellings) shall be checked within 1 year after installation.
                10.4.3.2.2 Sensitivity shall be checked every alternate year thereafter unless otherwise permitted by compliance with
    10.4.3.2.3.
                10.4.3.2.3 After the second required calibration test, if sensitivity tests indicate that the device has remained within its 
                listed and marked sensitivity range (or 4 percent obscuration gray smoke, if not marked); the length of time 
                between calibration tests shall be permitted to be extended to a maximum of 5 years.
                10.4.3.2.3.1 If the frequency is extended, records of nuisance alarms and subsequent trends of these alarms shall be
    maintained.
                10.4.3.2.3.2 In zones or in areas where nuisance alarms show any increase over the previous year, calibration tests shall be performed.
            </div>
            <div class="titleDiv">
                Other testing requirements of NFPA-72:
            </div>
            <div class="paraDiv">
                10.4.3.1 If automatic testing is performed at least weekly by a remotely monitored fire alarm control unit specifically 
                listed for the application, the manual testing frequency shall be permitted to be extended to
    annually.  Table 10.4.3 shall apply.
                10.4.3.3 Test frequency of interfaced equipment shall be the same as specified by the applicable NFPA standards for the 
                equipment being supervised.
                10.4.8 Tests of all circuits extending from the central station shall be made at intervals of not more than 24 hours.
            </div>
            <div class="titleDiv">
                Inspection requirements of NFPA-72:
            </div>
            <div class="paraDiv">
                10.3.1 Visual inspections shall be performed in accordance with the schedules in Table 10.3.1 or more often if 
                required by the authority having jurisdiction.<br/><br/>
                Exception:<br/>
                Devices or equipment that are inaccessible for safety considerations shall be inspected during scheduled shutdowns if 
                approved by the authority having jurisdiction. Extended intervals shall not exceed 18 months.
            </div>
        </div>
        <table border="1px solid #000" cellspacing="0" cellpadding="0">               
            <thead> 
                <tr>
                    <th>Component</th>
                    <th>Initial/Reacceptance</th>
                    <th>Monthly</th>
                    <th>Quarterly</th>
                    <th>Semiannually</th>
                    <th>Annually</th>
                    <th>Table 10.4.2.2 Reference</th>                
                </tr>    
            </thead>
            <tbody>            
                <?php
                    if(sizeof($componentData)>0){
                        $topLineCount=0;
                        foreach($componentData as $cmpData){ $topLineCount++;
                ?>
                <tr>                
                    <td><?php echo $topLineCount.'. '.$cmpData['UdfaComponent']['name'];?></td>
                    <td class="chkBox">
                        <?php
                            if($cmpData['UdfaComponent']['initial_reacceptance']==1){
                                echo $form->input('UpToDate.initial_reacceptance',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($cmpData['UdfaComponent']['monthly']==1){
                                echo $form->input('UpToDate.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($cmpData['UdfaComponent']['quarterly']==1){
                                echo $form->input('UdfaComponent.quarterly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($cmpData['UdfaComponent']['semiannualy']==1){
                                echo $form->input('UdfaComponent.semiannualy',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($cmpData['UdfaComponent']['annualy']==1){
                                echo $form->input('UdfaComponent.annualy',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>                
                    <td><?php echo $cmpData['UdfaComponent']['table_refernce'];?></td>                
                </tr>
                <?php
                    if(sizeof($cmpData['UdfaComponentPart'])>0){
                        $k='a';
                    foreach($cmpData['UdfaComponentPart'] as $parts ){
                        if($parts['parent_id']!=0){
                            continue;
                        }
                ?>
                <tr>
                    <td class="padSingle"><?php echo $k.')'.$parts['name'];?></td>                
                    <td class="chkBox">
                        <?php
                            if($parts['initial_reacceptance']==1){
                                echo $form->input('UpToDate.initial_reacceptance',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($parts['monthly']==1){
                                echo $form->input('UpToDate.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($parts['quarterly']==1){
                                echo $form->input('UpToDate.quarterly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($parts['semiannualy']==1){
                                echo $form->input('UpToDate.semiannualy',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($parts['annualy']==1){
                                echo $form->input('UpToDate.annualy',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>                
                    <td><?php echo $parts['table_refernce'];?></td>                
                </tr>
                <?php
                    if(sizeof($parts['Children'])>0){
                        $childCount=1;
                        foreach($parts['Children'] as $childrens ){
                ?>
                <tr>
                    <td class="padDouble"><?php echo $childCount.')'.$childrens['name'];?></td>                
                    <td class="chkBox">
                        <?php
                            if($childrens['initial_reacceptance']==1){
                                echo $form->input('UpToDate.initial_reacceptance',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($childrens['monthly']==1){
                                echo $form->input('UpToDate.monthly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($childrens['quarterly']==1){
                                echo $form->input('UpToDate.quarterly',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($childrens['semiannualy']==1){
                                echo $form->input('UpToDate.semiannualy',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>
                    <td class="chkBox">
                        <?php
                            if($childrens['annualy']==1){
                                echo $form->input('UpToDate.annualy',array('type'=>'checkbox','label'=>false,'div'=>false));
                            }
                        ?>
                    </td>                
                    <td><?php echo $childrens['table_refernce'];?></td>                
                </tr>
                <?php
                        $childCount++;}
                    }    
                ?>
                <?php $k++;}}?>
                <?php
                        }
                    }    
                ?>            
            </tbody>
        </table>
    </div>
    <div id="suppression">
        Under progress
    </div>
    <div id="submitContainer">
        <section class="login_btn">
            <span>			 
                <?php echo $form->submit('Submit',array('div'=>false));?>
            </span> 
        </section>        
    </div>
    <?php echo $form->end();?>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){        
        jQuery("div#fire").hide();
        jQuery("div#suppression").hide();
        jQuery("div#suppression").hide();
        jQuery(".chkType").change(function(){
            if(jQuery(this).val()==1){
                jQuery("div#sprinkle").show();
                jQuery("div#fire").hide();
                jQuery("div#suppression").hide();
            }else if(jQuery(this).val()==2){
                jQuery("div#sprinkle").hide();
                jQuery("div#fire").show();
                jQuery("div#suppression").hide();
            }else if(jQuery(this).val()==3){
                jQuery("div#sprinkle").hide();
                jQuery("div#fire").hide();
                jQuery("div#suppression").show();
            }
        });
    });
</script>