<?php
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>


<script type="text/javascript">
    jQuery(document).ready(function(){
      
   jQuery("#forgetpass").validationEngine();
   } 
   );   
</script>
<!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	<?php echo $form->create('', array('type'=>'POST', 'action'=>'forgotpassword','name'=>'loginform','id'=>'forgetpass')); ?>
                    <!-- Faq Content Starts -->
                    <div class="faq-section">
                    
                    <h2>Forgot Password</h2>
                        <div class="content faq-content">
                        
                        <div align="center" class="error-message"><span class="system negative" id="err1"><?php echo $this->Session->flash();?></span></div>
                        
                           <div class="contact-form">
                           		
                                <div class="new-form">
                               <!-- <div>Before we can reset your password, you need to enter the information below to help identify your account:</div> -->
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Email:</label><?php echo $form->input('User.email',array('label'=>'','div'=>'','maxlength'=>'50','class'=>'name-input validate[required] custom[email]','error'=>false));?>
                                     
                                    </div>
                                    <div class="inner-new-form">
                                    	<div class="button-section">
                                			<?php echo $form->button('<span>Submit</span>',array('escape'=>false,'class'=>"green"));?>
                                		</div>
                                    </div>
                                </div>
                           </div>
                        </div>
                     </div>
                    <!-- Faq Content Ends -->
                    </form>      
                      <?php  echo $this->element('right_section_front'); ?>
                                                            
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends --> 
            
            
