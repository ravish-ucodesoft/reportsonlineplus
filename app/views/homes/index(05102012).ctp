<!-- Begin: Banner -->
<section id="banner_full">
      <section id="banner_wrap">
            <section id="banner_l"><?php echo $html->image('banner/'.$bannerData['Banner']['banner_img']); ?></section>
            <section id="banner_r">
              	<h1><?php echo $bannerData['Banner']['title']; ?></h1>
                <p><?php echo $bannerData['Banner']['description']; ?></p>

            </section>
      </section>
</section>
<!-- End: Banner -->

<!-- Begin: Container -->
<section id="container_bg">
      <section id="container_wrap">
	          <!-- Begin: Menu -->
              <section id="menu_shadow">
                   <section id="menu_bg">
                      <ul id="menu_link">
                        <li class="first"><a href="/users/signup">Service Providers</a></li>                                               
                        <li class="last"><a href="/users/clientsignup">Property Owners</a></li>
                      </ul>
                   </section>
              </section>
              <!-- End: Menu -->
              
              <!-- Begin: 3 boxes -->
              <section id="cols">
                 <section class="col_1">
                   <section class="box_wrap">
                     <section class="box_top">
                   		<span class="box_img"><img src="../img/frontend/service_icon.jpg" /></span><h1>Service Companies</h1>
                     </section>
                     <section class="box_mid">
                        <section class="box_mid_wrap"><p><?php echo $tabData['MainTab']['description1']; ?></p>
                          <div class="read_btn"><a href="/users/signup"><span>Read More</span></a></div>
                        </section>
                     </section>
                     <section class="box_btm"></section>
                   </section>
                 </section>
                 <section class="col_1">
                   <section class="box_wrap">
                     <section class="box_top">
                   		<span class="box_img"><img src="../img/frontend/property_owner_icn.jpg" /></span><h1>Property Owners</h1>
                     </section>
                     <section class="box_mid">
                        <section class="box_mid_wrap"><p><?php echo $tabData['MainTab']['description2']; ?></p>
                          <div class="read_btn"><a href="/users/clientsignup"><span>Read More</span></a></div>
                        </section>
                     </section>
                     <section class="box_btm"></section>
                   </section>
                 </section>
                 <!--<section class="col_1">
                   <section class="box_wrap">
                     <section class="box_top">
                   		<span class="box_img"><img src="../img/frontend/fire_officials.jpg" /></span><h1>Fire Officials</h1>
                     </section>
                     <section class="box_mid">
                        <section class="box_mid_wrap"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.</p>
                          <div class="read_btn"><a href="#"><span>Read More</span></a></div>
                        </section>
                     </section>
                     <section class="box_btm"></section>
                   </section>
                 </section>-->
     		  </section>