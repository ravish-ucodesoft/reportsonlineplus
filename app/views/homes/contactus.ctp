 <!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	<?php echo $form->create('homes',array('action'=>'contactus','id'=>'contactform','name'=>'contactform')); ?>
                    <!-- Faq Content Starts -->
                    <div class="faq-section">
                    <h2>Contact Us</h2>
                        <div class="content faq-content">
                        <div align="center" style="color:red;"><?php echo $this->Session->flash();?></div>
                        <p>If you have any questions and would like to speak a customer service associates, you can contact us by:<br /> <br />
Phone: Call us anytime during normal business hours at 267-625-9158. We're here Monday through Friday 8 A.M. to 5 P.M. EST.<br /> <br />

E-mail: E-mail us at michaelkrusz@gmail.com or click here to ask a question, make a suggestion or get any assistance you may need. Most e-mails are answered within 24 hours.<br/> <br/>

Mail:<br /> 

Wayman Fire Protection <br />
C: 267-625-9158 | O: 888-4Wayman | F: 302-994-5750 <br />
403 Meco Drive Wilmington, Delaware 19804 <br />
                                
         <br /><br /> </p>
                           <div class="contact-form">
                           		
                                <div class="new-form">
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">First Name:</label><?php echo $form->input('Contactus.fname',array('label'=>'','div'=>'','maxlength'=>'30','class'=>'name-input','error'=>false));?>
                                     <div style="padding-left:150px;color:red;"><?php echo $form->error('Contactus.fname'); ?></div>
                                    </div>
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Last Name:</label><?php echo $form->input('Contactus.lname',array('label'=>'','div'=>'','maxlength'=>'30','class'=>'name-input','error'=>false));?>
                                     <div style="padding-left:150px;color:red;"><?php echo $form->error('Contactus.lname'); ?></div>
                                    </div>
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Email Address:</label><?php echo $form->input('Contactus.email',array('label'=>'','maxlength'=>'50','class'=>'name-input','error'=>false));?>
                                      <div style="padding-left:150px;color:red;"><?php echo $form->error('Contactus.email'); ?></div>
                                    </div>
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Phone Number:</label><?php echo $form->input('Contactus.phone',array('label'=>'','maxlength'=>'30','class'=>'name-input','error'=>false));?>
                                      <div style="padding-left:150px;color:red;"><?php echo $form->error('Contactus.phone'); ?></div>
                                    </div>
                                    
                                    
                                    
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Country:</label><?php echo $form->select('Contactus.country',$country,'1',array('empty'=>false,'style'=>'width:345px;')); ?>
                                      <div style="padding-left:150px;color:red;"></div>
                                    </div>
                                    
                                    
                                    <div class="inner-new-form">
                                    	<label style="color:#454545">Enter Your Comment:</label><?php echo $form->input('Contactus.comment',array('type'=>'textarea','label'=>'','class'=>'message-in','error'=>false));?>
                                      <div style="padding-left:150px;color:red;"><?php echo $form->error('Contactus.comment'); ?></div>
                                    </div>
                                    
                                    <div class="inner-new-form">
                                    	<div class="button-section">
                                			<?php echo $form->button('<span>Submit</span>',array('escape'=>false,'class'=>"green"));?>
                                	</div>
                                    </div>
                                </div>                                
                           </div>
			   
                        </div>
                     </div>
                    <!-- Faq Content Ends -->
                    <?php echo $form->end(); ?>     
                      <?php  //echo $this->element('right_section_front'); ?>
                                                            
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends -->
 
 
 
