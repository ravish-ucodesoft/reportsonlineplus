﻿            <!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">                  
                     <!-- Faq Content Starts -->
                    <div class="faq-section">
                    	<h2>Testimonials</h2>
                        <div class="content faq-content testimonial-content">
                        	<div class="inner-about">
                                <p>"Since using SchedulePal at my restaurant my manager has reduced his scheduling time from 3.5 hours to just 45 minutes on a weekly basis. The extra time gives us a more concentrated effort on customer service."</p>
                                <a href="#">Jim Morris</a>
                            </div>
                            <div class="inner-about">
                                <p>"SchedulePal has helped us improve employee retention. Numerous employees have told me how much they like our new scheduling system."</p>
                                <a href="#">Karen Bosch</a>
                            </div>
                            <div class="inner-about">
                                <p>"I really enjoy the online access to my schedule. I’ve been able to pick up some extra shifts through the shift trading feature, and I don’t even need to go into the restaurant to do this."</p>
                                <a href="#">Jamie Meyers</a>
                            </div>
                            <div class="inner-about">
                                <p>"As a district manager overseeing 9 stores, SchedulePal lets me see all of my store's schedules and hours on a daily basis from the comfort of my laptop."</p>
                                <a href="#">Bob Steele</a>
                            </div>
                            
                        </div>
                    </div>
                    <!-- faq-section Ends -->
                    <?php  echo $this->element('right_section_front'); ?>                                               
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends -->