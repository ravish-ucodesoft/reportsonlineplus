            
       <!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	
                   
                     <!-- Faq Content Starts -->
                    <div class="faq-section">
                    	<h2>Benefits</h2>
                        <!-- content faq-content start -->
                        <div class="content faq-content">
                       	  
                        <div class="inner-about">
                               <div class="thumb-left"> <a href="#"><?php echo $html->image('make-it-easy.png'); ?></a>
                            </div>
                                <div class="about-right">
                                	<h2 class="snap-title">We make it easy!</h2>
                                    <h2 class="sub-title">Save Time.Save Money. Reduce Employee Turnover.</h2>
                                    <p>SchedulePal is a fast, easy and affordable online employee scheduling software system designed for any size business in the restaurant and retail industry.</p>
                              </div>
                            </div>
                            <div class="inner-about">
                               <div class="thumb-left"> <a href="#"><?php echo $html->image('Save-Time.png'); ?></a>
                            </div>
                                <div class="about-right">
                                	<h2 class="snap-title">Save Time.</h2>
                                    <h2 class="sub-title">What if you were able to decrease the time spent creating work schedules by 80%?</h2>
                                    <p>Improve your product? Improve your customer service? Improve employee morale? Improve staff training development...</p>
                              </div>
                            </div>
                            <div class="inner-about">
                               <div class="thumb-left"> <a href="#"><?php echo $html->image('banner-3_thub.jpg'); ?></a>
                            </div>
                                <div class="about-right">
                                	<h2 class="snap-title">Time = Money</h2>
                                    
                                    <p>SchedulePal allows you to tightly monitor and control your labor costs. Our system does the work for you so you can increase labor productivity, limit overtime, and eliminate diffusion.</p>
                              </div>
                            </div>
                            <div class="inner-about" style="margin-bottom:10px">
                               <div class="thumb-left"><?php echo $html->image('Employee-Turnover.png'); ?></a>
                            </div>
                                <div class="about-right">
                                	<h2 class="snap-title">Reduce Employee Turnover</h2>
                                  <h2 class="sub-title">SchedulePal = Happy Employees</h2>
                                  <p>SchedulePal gives your employees the flexibility and convenience of accessing their schedule online anytime and anywhere.</p>
                              </div>
                            </div>
                        </div>
                        <!-- content faq-content end -->
                        <h2>Pricing</h2>
                        <!-- content faq-content start -->
                        <div class="content faq-content">
                        	<table cellpadding="4" cellspacing="4" border="0" style="border:1px solid grey;" width="100%">
                      						<tr style="background-color:#818285;color:#FFFFFF;font-size:13px">
                      								<!--<th>No.</th> -->								
                      								<th width='25%' >Number of Employee</th>
                      								<th width='35%'>Monthly Charges</th>								
                                      <th >Yearly Charges</th>							  
                      						</tr>
                      							<?php  if(count($subscription)) { ?>
                      						<tbody>		
                      					<?php $i=1; foreach ($subscription as $subscription): 
                                      if($i%2==0) { $style='style="background-color:#E2E2E2;font-weight:bold;"';} else{ $style='style="background-color:#F4F4F4;font-weight:bold;"'; }?>
                                	<tr class="first" style="font-size:13px">
                      								<!--<td align="center" <?php echo $style;?>><?php echo $i; ?>.</td> -->
                      								<td align="center" <?php echo $style;?>><?php echo $subscription['SubscriptionDuration']['min_allowed_employees'].'-'.$subscription['SubscriptionDuration']['max_allowed_employees']; ?></td>
                                      <td align="center" <?php echo $style;?>><span ><?php echo "$".$subscription['SubscriptionDuration']['monthlycharges']; ?></span>
                                        <?php //$imgp= $html->image('/img/sign-sm_new.png', array('alt'=>'','class'=>'new_signup')); ?>
                                        <span style="float:right; margin:0 10px 0 0">
                                          <?php echo $html->link("",array('controller'=>'owners','action'=>'signup',$subscription['SubscriptionDuration']['id'],'Monthly'), array('class'=>'new_signup'));?>
                                        </span>
                                      </td>
                      							  
                                      <td align="center" <?php echo $style;?>><?php echo "$".$subscription['SubscriptionDuration']['1_year']; ?>
                                        <span style="float:right; margin:0 10px 0 0">
                                        <?php echo $html->link("",array('controller'=>'owners','action'=>'signup',$subscription['SubscriptionDuration']['id'],'Yearly'),array('class'=>'new_signup')); ?></span>
                                      </td>
                      					  </tr>
                      					  <tr class="first" style="font-size:13px">
                      						<?php $i=$i+1; endforeach; ?>
                                  <td align="center" <?php echo $style;?>>101+ Employees</td>			
                                  <td align="center" <?php echo $style;?> colspan="2">Call 888-557-5297 for pricing</td>										
                      						</tr>
                                  </tbody>
                                <?php } else { ?>
                                	<tr class="first"><td colspan='6'>There no subscription</td></tr>
                                <?php } ?>
                                </table>
                             <br/>
                             <div align="center"><strong><?php echo $html->link('Please contact us',array('controller'=>'homes','action'=>'contactus')); ?> if you have any questions.</strong></div>
                            <br/><br/>
                        </div>
                        <!-- content faq-content end -->
                    </div>
                    <!-- faq-section Ends -->
                     <?php  echo $this->element('right_section_front'); ?>
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends -->     
            
            
            