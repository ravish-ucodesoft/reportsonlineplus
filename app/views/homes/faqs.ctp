 <!-- Middle Starts -->
              <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	
                    <!-- faq-section Starts -->
                    <div class="faq-section">
                    <h2>Frequently Asked Questions</h2>
                    <!-- content faq-content Starts -->
                    <div class="content faq-content">
                    	<div class="wrap-faq">
                        	<div class="inner-faq">
                            	<div class="faq-title"><a href="#sub">Subscribing</a></div>
                                <ul class="faq-listing">
                                	<li><a href="#q1">How do I subscribe online?</a></li>
                                    <li><a href="#q2">Can I subscribe by phone?</a></li>
                                    <li><a href="#q3">What are my payment options?</a></li>
                                    <li><a href="#q4">Do you charge sales tax?</a></li>
                                </ul>
                            </div>
                            <div class="inner-faq">
                            	<div class="faq-title"><a href="#confirmation">Subscription Confirmation</a></div>
                                <ul class="faq-listing">
                                	<li><a href="#q5">Will I get an e-mail confirmation after I subscribe?</a></li>
                                    <li><a href="#q6">How do I make changes or cancel my subscription?</a></li>
                                </ul>
                            </div>
                            <div class="inner-faq">
                            	<div class="faq-title"><a href="#Satisfaction Guarantee">Satisfaction Guarantee</a></div>
                                <ul class="faq-listing">
                                	<li><a href="#q7">What is your refund or cancellation policy?</a></li>
                                    <li><a href="#q8">How long before I receive my refund?</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="wrap-faq">
                        	<div class="inner-faq">
                            	<div class="faq-title"><a href="#Other Frequently Asked Questions">Other Frequently Asked Questions</a></div>
                                <ul class="faq-listing">
                                	<li><a href="#q9">What is your contact information for the press?</a></li>
                                </ul>
                            </div>
                            <div class="inner-faq">
                            	<div class="faq-title"><a href="#Privacy and Security Policy">Privacy and Security Policy</a></div>
                                <ul class="faq-listing">
                                	<li><a href="#q10">Are online transactions on your site secure?</a></li>
                                    <li><a href="#q11">How do you use my contact information?</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                    <!-- content faq-content end -->
                    <h2>FAQ</h2>
                    	<!-- content faq-content Starts -->
                        <div class="content faq-content">
                            <h3 id="sub" class="link-title">Subscribing</h3>
                            <ul class="faq-list">
                            	<li>
                                	<div class="question"><a id="q1" name="q1"></a>How do I subscribe online?</div>
                                    <div class="answer">Subscribing is easy and convenient! Simply select your employee category and provide us with your relevant contact and payment information.</div>
                                </li>
                                
                                <li>
                                	<div class="question"><a id="q2" name="q2"></a>Can I subscribe by phone?</div>
                                    <div class="answer">We accept phone subscriptions at 888-557-5297. Our customer service department is available Monday through Friday 8 A.M. to 5 P.M. CST to answer your questions.</div>
                                </li>
                                
                                <li>
                                	<div class="question"><a id="q3" name="q3"></a>What are my payment options?</div>
                                    <div class="answer">Subscriptions can be placed online using Visa, MasterCard, American Express or Discover Cards.</div>
                                </li>
                                <li>
                                	<div class="question"><a id="q4" name="q4"></a>Do you charge sales tax?</div>
                                    <div class="answer">No, we do not charge sales tax.</div>
                                </li>
                            </ul>
                            <h3 id="confirmation" class="link-title">Subscription Confirmation</h3>
                            <ul class="faq-list">
                            	<li>
                                	<div class="question"><a id="q5" name="q5"></a>Will I get an e-mail confirmation after I subscribe?</div>
                                    <div class="answer">Yes, you will immediately receive an e-mail confirmation of your subscription.</div>
                                </li>
                                
                                <li>
                                	<div class="question"><a id="q6" name="q6"></a>How do I make changes or cancel my subscription?</div>
                                    <div class="answer">Please verify your subscription confirmation immediately upon receipt to verify the details of your subscription. To make changes or cancel your subscription please contact our customer service associate Mon-Fri 8 A.M. to 5 P.M. CST at 888-557-5297 so we can accommodate your request.</div>
                                </li>
                            </ul>
                            <h3 id="Satisfaction Guarantee" class="link-title">Satisfaction Guarantee</h3>
                            <ul class="faq-list">
                            	<li>
                                	<div class="question"><a id="q7" name="q7"></a>What is your refund or cancellation policy?</div>
                                    <div class="answer">Any cancellation received before the 10th of the month will receive a full 100% refund for that given month. All cancellations received after the 10th of the month will receive a refund on a pro-rated basis.<br/>All annual subscription cancellations are refunded on a pro-rated basis.</div>
                                </li>
                                
                                <li>
                                	<div class="question"><a id="q8" name="q8"></a>How long before I receive my refund?</div>
                                    <div class="answer">Refunds will be issued 3 to 7 business days after we receive and verify your cancellation request. Please allow adequate time for your banking institution to process the refund.</div>
                                </li>
                            </ul>
                            <h3 id="Other Frequently Asked Questions" class="link-title">Other Frequently Asked Questions</h3>
                            <ul class="faq-list">
                            	<li>
                                	<div class="question"><a id="q9" name="q9"></a>What is your contact information for the press?</div>
                                    <div class="answer">All press inquiries should be e-mailed to the following address:joek@schedulepal.com</div>
                                </li>
                            </ul>
                            <h3 id="Privacy and Security Policy" class="link-title">Privacy and Security Policy</h3>
                            <ul class="faq-list">
                            	<li>
                                	<div class="question"><a id="q10" name="q10"></a>Are online transactions on your site secure?</div>
                                    <div class="answer">All the information you provide, including your credit card information, is secured using Secure Sockets Layer (SSL) encryption technology. We use SSL technology to prevent your information from being intercepted and read as it is transmitted over the Internet.</div>
                                </li>
                                
                                <li>
                                	<div class="question"><a id="q11" name="q11"></a>How do you use my contact information?</div>
                                    <div class="answer">We request your e-mail address so that we can e-mail you an subscription confirmation. We request your phone number in case we need to contact you. We never rent or sell your contact information to anybody.</div>
                                </li>
                            </ul>
                        </div>
                        <!-- content faq-content end -->
                     </div>
                    <!-- faq-section ends -->
                     <!-- Right Starts -->	
                   <?php echo $this->element('right_section_front'); ?>
                    <!-- Right Ends -->
                                                            
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends -->