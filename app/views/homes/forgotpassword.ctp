<?php    
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');   
?>
<script type="text/javascript">
jQuery(document).ready(function(){  
   jQuery("#forgotpwd").validationEngine();
});   

</script>
<style>
.password {
width:185px;
border:1px solid #74B2E2;color:#023e6f;
float:left;
}
.pstrength-minchar {
    display:none;
font-size : 10px;
float:left;

}
#validpassword_text{
    float:left;
}
#ValidConfirmPassword { margin-bottom:10px;}
#UserPassword_bar{margin-top:-10px;width:65%;border:0px solid white;}
.error-message{
 /* background-image: url("/img/negative.gif"); */
    color: #B86464;
	background-position: center bottom;
    background-repeat: no-repeat;
    display: block;
    float: left;
    font-weight: bold;
    padding: 30px 0 0 172px;
    white-space: nowrap;
    position:absolute; 
}


ul.login_form {
	margin: 0;
	}

</style>
<script>
function valid(){
var password = $("#UserPassword").val();
var ValidPassword = $("#UserConpassword").val();
	if(password != ValidPassword){  
//$("#UserConpassword").css("border", "2px solid red");
 $("#UserConpasswordp").text( "*Confirm Password is not match " );
 	return false;
 }
}
</script>
<!-- Inner Starts-->
<section class="login_wrap">
    <section class="member_login">
	<section class="member_bg">
	    <section class="member_grain_bg">
		<h3>Forgot Password</h3>
		<div align="center"><span id="err1"><?php echo $this->Session->flash();?></span></div>
		<!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.</p>-->
		<p style="padding-bottom: 35px;"></p>
<!-- first for answer -->		
		<?php if(isset($question)){ ?>
			<?php echo $form->create('', array('type'=>'POST', 'url'=>array('controller'=>'homes','action'=>'forgotpassword'),'id'=>'forgotpwd')); ?>
		<ul class="login_form">
		    <li>
		    <section>
			<b>Question : </b> <span><?php echo $question['Question']['name'];  ?></span>
			</section>
			</li>		    
		    <li>
			<section class="input_wrap">											
			    <?php echo $form->input('User.email', array('type' => 'hidden','maxLength'=>'50','class'=>'text',"div"=>false,"label"=>false));  ?>
			    <?php echo $form->input('User.answer', array('class'=>'text validate[required]',"div"=>false,"label"=>false));  ?>
			</section>
		    </li>		    
		    <li>
		    <?php if(isset($question['ans'])) { ?>
		    <?php if($question['ans'] == 'wrong') {?>
			 <p style="color:red;">Wrong answer</p>
			 <?php } } ?>
			 <section class="login_btn"><span><input type="submit" value="Submit" /></span></section>
		    </li>
		     
		</ul>
		<?php echo $form->end();  ?>
<!-- second for new password -->
<?php } else if(isset($newpass)){ ?>
			<?php echo $form->create('', array('onsubmit' => 'return valid();','type'=>'POST', 'url'=>array('controller'=>'homes','action'=>'forgotpassword2'),'id'=>'forgotpwd')); ?>
		<ul class="login_form">		    
		    <li>
		    <label>&nbsp;</label>
			<section class="input_wrap">											
			    <?php echo $form->input('User.email', array('type' => 'hidden','maxLength'=>'50','class'=>'text',"div"=>false,"label"=>false));  ?>
			    <?php echo $form->input('User.password', array('class'=>'text validate[required, minSize[5]]',"div"=>false,"label"=>false, 'placeholder' => 'New Password'));  ?>
			</section>
		    </li>
		    <li>
		    <label>&nbsp;</label>
			<section class="input_wrap">											
			    <?php echo $form->input('User.conpassword', array('type' => 'password', 'class'=>'text validate[required, minSize[5]]', 'placeholder' => 'Confirm Password',"div"=>false,"label"=>false));  ?>
			</section>
		    </li>

		    <li>
		    <p style="color:red;margin-top: -2px;" id="UserConpasswordp"></p>
			 <section class="login_btn"><span><input type="submit" value="Submit" /></span></section>
		    </li>
		     
		</ul>
		<?php echo $form->end(); ?>		

<!-- third for email -->





		<?php }
		else{ ?>
		<?php echo $form->create('', array('type'=>'POST', 'url'=>array('controller'=>'homes','action'=>'forgotpassword'),'id'=>'forgotpwd')); ?>
		<ul class="login_form">
		    <li>
			<label>Email</label>
			<section class="input_wrap">											
			    <?php echo $form->input('User.email', array('maxLength'=>'50','class'=>'text validate[required,custom[email]]',"div"=>false,"label"=>false));  ?>
			</section>
		    </li>		    
		    <li>
		    <?php if(isset($email_error)) { ?>
		    <?php if($email_error == 'wrong') {?>
			 <p style="color:red;">Email is not register</p>
			 <?php } } ?>
			 <label>&nbsp;</label>
			 <section class="login_btn"><span><input type="submit" value="Submit" /></span></section>
		    </li>
		     
		</ul>
		<?php echo $form->end(); ?>
		<?php } ?>
	    </section>
	</section>
    </section>
</section>
<section class="clear"></section>
<!-- Inner Ends-->