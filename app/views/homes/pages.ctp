<section class="inner_full">
    <?php if($this->params['pass'][0]!=81){?>              
    <section class="inner_l">
        <h1><?php echo $websitepages['Websitepage']['page_name'];?></h1>
        <p><?php echo $websitepages['Websitepage']['page_content'];?></p>
    </section>
    <section class="inner_r">
    
        <!-- Questions? -->
        <section class="ques_bg">
            <section class="ques_txt">
                <h1>Do you have questions  about life safety inspections  or Code?</h1>
                <?php echo $html->link('Click here',array('controller'=>'homes','action'=>'codeInspectionQuery'),array('class'=>'clickhere ajax'));?>
            </section>
        </section>
        <!-- Free Site Survey -->
        <section class="survey_bg">
            <section class="survey_txt">
                <h1>Free Site Survey</h1>
                <section class="grey_btn"><a href="<?php echo $html->url(array('controller'=>'users','action'=>'clientsignup'));?>" class="clickhere"><span>Click here</span></a></section>
            </section>
        </section>
                        
        <!-- Up to date -->
        <section class="uptodate_bg">
            <section class="uptodate_txt">
                <h1>Are you up to Date?</h1>
                <p>with your Fire Safety</p>
                <section class="red_btn"><a href="<?php echo $html->url(array('controller'=>'homes','action'=>'up_to_date'));?>" class="clickhere"><span>Click here</span></a></section>
            </section>
        </section>
    </section>
    <?php }else{?>
   
    <section class="inner_l">  
                    <h1>Subscription Plan</h1>
                    <h4>Service Provider Recommendation</h4>
                    
                    <ul class="plans">
                        <?php 
                        foreach($subData as $plans): ?>
                       <li>
                          <section class="subs_bg">
                              <section class="plan_title"><h2><?php echo $plans['SubscriptionDuration']['plan_name'];?></h2></section>
                              <section class="plan_price_monthly"><h3>Monthly charges</h3></section>
                              <section class="plan_price"><h1><span class="font34">$</span><span class="font26"><?php echo $plans['SubscriptionDuration']['monthlycharges'];?></span></h1></section>
                              <section class="plan_per_client"><h5><?php echo $plans['SubscriptionDuration']['max_allowed_site_address'];?> site address</h5></section>
                              <section class="subscribe"><span><input type="submit" value="Subscribe now"/></span></section>
                          </section>
                       </li>
                       <?php endforeach; ?>
                        
                    </ul>
                         
	</section>
    <section class="inner_r">
    
        <!-- Questions? -->
        <section class="ques_bg">
            <section class="ques_txt">
                <h1>Do you have questions  about life safety inspections  or Code?</h1>
                <?php echo $html->link('Click here',array('controller'=>'homes','action'=>'codeInspectionQuery'),array('class'=>'clickhere ajax'));?>
            </section>
        </section>
        <!-- Free Site Survey -->
        <section class="survey_bg">
            <section class="survey_txt">
                <h1>Free Site Survey</h1>
                <section class="grey_btn"><a href="<?php echo $html->url(array('controller'=>'users','action'=>'clientsignup'));?>" class="clickhere"><span>Click here</span></a></section>
            </section>
        </section>
                        
        <!-- Up to date -->
        <section class="uptodate_bg">
            <section class="uptodate_txt">
                <h1>Are you up to Date?</h1>
                <p>with your Fire Safety</p>
                <section class="red_btn"><a href="<?php echo $html->url(array('controller'=>'homes','action'=>'up_to_date'));?>" class="clickhere"><span>Click here</span></a></section>
            </section>
        </section>
    </section>
    
    
   
    <?php }?>
</section>
<section class="clear"></section>
<!-- Inner Ends-->