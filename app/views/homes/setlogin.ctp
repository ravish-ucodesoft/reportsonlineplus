<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js'); 
?>
<script type="text/javascript">
    jQuery(document).ready(function(){      
        jQuery("#signup").validationEngine();
    });
</script>

<style type="text/css">
.password {
width:185px;
border:1px solid #74B2E2;color:#023e6f;
float:left;
}
.pstrength-minchar {
    display:none;
    font-size : 10px;
    float:left;
}
#validpassword_text{
    float:left;
}
#ValidConfirmPassword { margin-bottom:10px;}
#UserPassword_bar{margin-top:-10px;width:65%;border:0px solid white;}
.error-message{
 /* background-image: url("/img/negative.gif"); */
    color: #B86464;
	background-position: center bottom;
    background-repeat: no-repeat;
    display: block;
    float: left;
    font-weight: bold;
    padding: 30px 0 0 172px;
    white-space: nowrap;
    position:absolute;   
}
form{
    padding: 0px;
}
</style>
<!-- Inner Starts-->
<section class="login_wrap">
    <section class="member_login">
        <section class="member_bg">
            <section class="member_grain_bg">
                <h3>Welcome to <?php echo $spsession['Company']['name'];?> (Login)</h3>
                <div align="center"><span id="err1"><?php echo $this->Session->flash();?></span></div>
                <?php echo $form->create('', array('type'=>'POST', 'url'=>array('controller'=>'homes','action'=>'setlogin',$this->params['pass'][0]),'id'=>'signup')); ?>
                <ul class="login_form">                                                         
                    <li>
                        <label>Email</label>
                        <section class="input_wrap">											
                            <?php echo $form->input('User.email', array('maxLength'=>'50','class'=>'text validate[required,custom[email]]',"div"=>false,"label"=>false));?>
                        </section>
                    </li>
                    <li>
                        <label>Password</label>
                        <section class="input_wrap">
                            <?php echo $form->input('User.password', array('maxLength'=>'50','class'=>'text password validate[required]',"type"=>"password","div"=>false,"label"=>false));?>
                        </section>
                    </li>
                    <li>
                        <label>&nbsp;</label>
                        <section class="login_btn"><?php echo $html->link('Forgot Password ?',array('controller'=>'homes','action'=>'forgotpassword'),array('class'=>'forgot'))?><span><input type="submit" value="Login" /></span></section>
                    </li>                     
                </ul>
                <?php echo $form->end(); ?>
            </section>
        </section>
    </section>
</section>
<section class="clear"></section>
<!-- Inner Ends-->