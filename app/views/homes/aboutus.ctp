            <!-- Middle Starts -->
            <div id="middle">
            	<!-- Middle Section Starts -->
                <div class="middle-section">
                	
                   
                     <!-- Faq Content Starts -->
                    <div class="faq-section">
                    	<h2>About Us</h2>
                        <div class="content faq-content">
                        <p>Businesses across the country trust SchedulePal with their employee scheduling needs.  Our program was created and custom tailored for the restaurant and retail industries.  With over 25 years of experience in these industries, SchedulePal has been designed to meet your business’ needs.<br /><br />      
SchedulePal is the single source to reduce costs and increase productivity while giving managers and employees online access from anywhere. SchedulePal is founded on over 25 years of retail and restaurant management experience and its founders have faced all sides of the hospitality industry. <br /><br />

Our website is a software as a service (SaaS) program designed to save you time, save you money, and reduce employee turnover. Your initial setup with SchedulePal may average one to two hours, depending on the number of employees working for you. Once the initial data is entered, your future schedules are easily created. According to our research, SchedulePal can save you up to 80% of the time it would take you to write a schedule by hand. At SchedulePal, we make it easy. <br /><br />
Jay Hochenedel and Joe Kopke founded SchedulePal with a passion to help businesses and their employees become more productive and profitable. Simplicity and functionality are the keys to success at SchedulePal.</p>
                        
                     <!--   <p>Hundreds of businesses trust SchedulePal with their employee scheduling needs.
                        <br /><br />
SchedulePal is the single source to reduce costs and increase productivity while giving managers and employees online access from anywhere.  SchedulePal  is founded on over 25 years of retail and restaurant management experience and its founders have faced all sides of the hospitality industry.
Our website is a software as a service (SaaS) program designed to save you time, save you money, and reduce employee turnover. 
<br /><br />Our service simplifies the scheduling process for not only the manager, but also the employee. At schedulePal “We make it Easy.” and stand by a 100% customer satisfaction guarantee.</p><br/> -->
                       <!-- <h3 class="link-title">Management</h3><br />
                        	<div class="inner-about">
                            	<div class="about-left">
                                	<?php //echo $html->image('about-01.gif'); ?>
                                </div>
                                <p><b>Joseph Kopke – President and Co-owner</b></p>
                                <p>Joseph Kopke has 13 years experience in restaurant and retail management. His knowledge and experience in the industry has given him a clear insight into the challenges businesses face with employee scheduling. Joe’s passion is to create technological solutions to increase restaurant and retail profitability.
</p>
                            </div>
                            
                            <div class="inner-about">
                            	<div class="about-left">
                                	<?php //echo $html->image('about-01.gif'); ?>
                                </div>
                                <p><b>Jay Hochenedel – Vice President and Co-owner</b></p>
                                <p>Jay has 15 years experience in restaurant and retail management. Jay maintains close relationships with restaurant and retail owners across the United States. These relationships Jay has built over the years have allowed him to keep current with the restaurant and retail industry. Jay is the eyes and ears for SchedulePal.
</p>
                            </div>
                            
                            <div class="inner-about">
                            	<div class="about-left">
                                	<?php //echo $html->image('about-01.gif'); ?>
                                </div>
                                <p><b>Brian Kaldenberg - Chief Technology Officer</b></p>
                                <p>Brian Kaldenberg has over eight years' experience in eCommerce business management.
</p>
                            </div>-->
                           
                        </div>
                    </div>
                    <!-- faq-section Ends -->
                     <?php  echo $this->element('right_section_front'); ?>                                        
                </div>
                <!-- Middle Section Ends -->
            </div>
            <!-- Middle Ends -->