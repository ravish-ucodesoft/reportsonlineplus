<?php 
	echo $html->script('adminfunction');
	//echo $crumb->getHtml('Members', 'null','dashboard' ) ;
?>
   <div class="section">				
				<div class="title_wrapper">					
					<h2>Subscription Charges</h2>			
					<div id="product_list_menu">
						<a href="/admin/companies/add_subscription" class="update"><span><span><em>Add New</em><strong></strong></span></span></a>
					</div>				
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<th>No.</th>								
								<th width='40%'>Max. site address</th>
								<th width='30%'>Plan Name</th>
								<th width='20%'>Monthly Charges</th>								
								<th width='10%'>Action</th>
							</tr>
							<?php  if(count($subscription)) { ?>
						<tbody>		
							<?php $i=1; foreach ($subscription as $subscription): ?>							
							      <tr class="first">
								<td><?php echo $i; ?>.</td>
								<td><?php echo $subscription['SubscriptionDuration']['max_allowed_site_address']; ?></td>
								<td><?php echo $subscription['SubscriptionDuration']['plan_name']; ?></td>
								<td><?php echo "$ ".$subscription['SubscriptionDuration']['monthlycharges']; ?></td>								
								<td>
									<div class="actions_menu" style='width:100%'>
										<ul style='width:120px'>			
											<li><?php echo $html->link('Edit',array('controller'=>'Companies','action'=>'edit_subscription',$subscription['SubscriptionDuration']['id']),array('class'=>'edit')) ?></li>
										
											<li><?php echo $html->link('Delete',array('controller'=>'Companies','action'=>'subscriptionchargedelete',$subscription['SubscriptionDuration']['id']),array('class'=>'delete'),'Are you sure you want to delete Subscription Charge?') ?></li>
										</ul>
									</div>
								</td>
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
<?php } else { ?>
	<tr class="first"><td colspan='6'>There no subscription</td> </tr>
<?php } ?>
</table>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->					
				
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->