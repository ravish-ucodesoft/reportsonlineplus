<?php 
 		//echo $crumb->getHtml('Add', 'null','') ;
?>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Add Subscription</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner">	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'add_subscription') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms">
	
  		 <div class="row">
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		    </span>
  		 </div>	 
				<div class="row">
				<label style="width:260px;">Minimum Allowed Site Addresses: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('SubscriptionDuration.min_allowed_site_address', array('maxLength'=>'3','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('SubscriptionDuration.min_allowed_site_address'); ?></span>
				</div>
			</div>
				
				<div class="row">
				<label style="width:260px;">Maximum Allowed Site Addresses : </label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('SubscriptionDuration.max_allowed_site_address', array('maxLength'=>'5','class'=>'text'));  ?>
					</span>
					
					<span class="system negative" id="err1"><?php echo $form->error('SubscriptionDuration.max_allowed_site_address'); ?></span>
				</div>
			</div>
		       <div class="row">
				<label style="width:260px;">Plan Name: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('SubscriptionDuration.plan_name', array('maxLength'=>'15','class'=>'text'));  ?>
					</span>
					
					<span class="system negative" id="err1"><?php echo $form->error('SubscriptionDuration.plan_name'); ?></span>
				</div>
			</div>
			
		        <div class="row">
				<label style="width:260px;">Monthly Charges($): <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('SubscriptionDuration.monthlycharges', array('maxLength'=>'6','class'=>'text'));  ?>
					</span>
					
					<span class="system negative" id="err1"><?php echo $form->error('SubscriptionDuration.monthlycharges'); ?></span>
				</div>
			</div>
		
				
			
					
		
			<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="list" ><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
			</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->
