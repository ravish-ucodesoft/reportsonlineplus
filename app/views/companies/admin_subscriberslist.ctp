<?php 
  echo $html->script('adminfunction');
  echo $crumb->getHtml('Registered Fire Inspector/Agent', 'null','dashboard') ;
?>

<div class="section">				
				<div class="title_wrapper">					
					<h2>Registered Fire Inspector/Agent</h2>					
					<div id="product_list_menu">
						<!-- a href="/admin/admins/add" class="update"><span><span><em>Add New</em><strong></strong></span></span></a -->
					</div>				
				</div>								
				<!--[if !IE]>start section_inner<![endif]-->
				<div class="section_inner">							
				<div  id="product_list">        								
					<!--[if !IE]>start table_wrapper<![endif]-->
					<div class="table_wrapper">
						<div class="table_wrapper_inner">
						<table cellpadding="0" cellspacing="0" width="100%" border="0">
						<tr>
								<th> No.</th>								
								<th><?php echo $paginator->sort('First Name','fname');?> </th>
								<th><?php echo $paginator->sort('Last Name','lname');?> </th>
								<th><?php echo $paginator->sort('Email','email');?> </th>
								<th><?php echo $paginator->sort('Company','name');?> </th>
								<th width="25%">PaymentDetails</th>
							  <th>Status</th>
							  <!--<th>Action</th>-->
							</tr>
							<?php  if(count($listing)) { ?>
						<tbody>		
					<?php $i=1; foreach ($listing as $Userlist): ?>
							<tr class="first">
								<td valign="top"><?php echo $i; ?>.</td>
								<td valign="top"><?php echo $Userlist['User']['fname']; ?></td>
								<td valign="top"><?php echo $Userlist['User']['lname']; ?></td>
								<td valign="top"><a href="mailto:<?php echo $Userlist['User']['email']; ?>"><?php echo $Userlist['User']['email']; ?></a></td>
								<td valign="top"><?php echo $Userlist['Company']['name'].'<br />';
								  if (isset($Userlist['Company']['adress']))
									  echo  $Userlist['Company']['adress'].'<br />';
								  
								  if (isset($Userlist['Company']['city']))
									  echo  $Userlist['Company']['city'].', ';
								  
								  if (isset($Userlist['Company']['state']))
									  echo  $Userlist['Company']['state'].', ';
								  
								  echo  $Userlist['Company']['zip'];
								  
								  if (isset($Userlist['Company']['phone']))
									  echo "<br>".$Userlist['Company']['phone']; 
								  
								  if (isset($Userlist['Company']['url']))
									  echo  "<br /><a href='".$Userlist['Company']['url']."' target='_blank'>".$Userlist['Company']['url']."</a>";
                                      
					?></td>
                <td valign="top">
                <?php 
                      echo 'Amount: $' .$Userlist['User']['amount'].'<br />';
                      echo 'ProfileID:'.$Userlist['User']['profile_id'].'<br />';
                      echo 'TransDate:'.date('m-d-Y',strtotime($Userlist['User']['created']));
                      echo '<br/>'.$html->link('See Details','/RecurringPayments/GetRPProfileDetailss.php?ProfileID='.$Userlist['User']['profile_id']);
                 ?>
                </td>
							 	<td>
								<?php
									$id='activeImg'.$Userlist['Company']['id'];
									$activeImg = $Userlist['Company']['status']==1 ?'/img/icons/status_star_green.gif' : '/img/icons/status_star_red.gif'; 
								?>
					<div id="<?php echo $id;?>">
					<?php 
						echo $html->link($html->image($activeImg,array('border'=>'0')),'/admin/companies/activate/'.$Userlist['Company']['id'],array('update'=>$id,'escape'=>false,'onClick'=>'return status();'),null,null);
								?>
								</div>
								</td>
								<!--<td valign="top">
									<div class="actions_menu" style='width:100%'>
										<ul style='width:50px'>			
											<li><?php //echo $html->link('Edit',array('action'=>'editsubscriber',$Userlist['User']['id']),array('class'=>'edit')) ?></li>
											<!--li><?php //echo $html->link('Delete',array('action'=>'delete',$userlist['Admin']['id']),array('class'=>'delete'),'Are you sure you want to delete Member?') ?></li-->
										<!--</ul>
									</div>
								</td>-->
							</tr>
						<?php $i=$i+1; endforeach; ?>							
						</tbody>
<?php } else { ?>
	<tr class="first"><td colspan='6'>There no member</td> </tr>
<?php } ?>
</table>

						</div>
					</div>
					<!--[if !IE]>end table_wrapper<![endif]-->					
				</div>
				</div>
				<!--[if !IE]>end section inner<![endif]-->
        	
				<!--[if !IE]>start pagination<![endif]-->					
					<div class="pagination">
					<span class="page_no">Page <?php echo $paginator->counter(); ?></span>
    			<ul class="pag_list">
    				<li class="prev" style="display:block;" ><?php echo $paginator->prev('Previous'); ?> </li>
    				<li><?php echo $paginator->numbers(); ?></li>
    				<li class="next" ><?php echo $paginator->next('Next'); ?></li>
    			</ul>	
										
					</div>
					<!--[if !IE]>end pagination<![endif]-->
						
				</div>
			
				<!--[if !IE]>end section<![endif]-->