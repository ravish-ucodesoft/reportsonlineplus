<?php 
	echo $crumb->getHtml('Edit', 'null','') ;
?>
<!--[if !IE]>start section<![endif]-->
<div class="section">	
	<div class="title_wrapper">
	<?php echo "<h2>Edit Subscriber</h2>"; ?>
	</div>	
	<!--[if !IE]>start section inner <![endif]-->
	<div class="section_inner" >	
	<!--[if !IE]>start forms<![endif]-->
	<?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'admin/editsubscriber') ,'name'=>'addform','id'=>'addform', 'class'=>'search_form general_form')); ?>
		<!--[if !IE]>start fieldset<![endif]-->
		<fieldset>
			<!--[if !IE]>start forms<![endif]-->
			<div class="forms"> 	
  		 <div class="row" >
  		    <span style="float:right">
  		      [<span class="star">*</span>] marked fields are mandatory.
  		      	<?php echo $form->hidden('Owner.id'); ?>
  		      	<?php echo $form->hidden('Company.id'); ?>
  		    </span>
  		 </div>	 
				<div class="row" >
				<label>First name: <span class="star">*</span></label>
				<div class="inputs" >
					<span class="input_wrapper">
					<?php echo $form->text('Owner.fname', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Owner.fname'); ?></span>
				</div>
			</div>
				
<div class="row">
				<label>Last name : </label>
    		<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Owner.lname', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err1"><?php echo $form->error('Owner.lname'); ?></span>
				</div>
</div>

<div class="row">
				<label>Email: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Owner.email', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err2"><?php echo $form->error('Owner.email'); ?></span>
        </div>
</div>
		<!--[if !IE]>start row<![endif]-->
<div class="row">
				<label>Payment status: <span class="star">*</span></label>
				<div class="inputs">
					<ul class="inline">
					<li><?php $option=array('1'=>'Paid','0'=>'Not Paid'); 
          echo $form->radio('Owner.pay_status',$option,array('legend'=>false,'label'=>false,'default'=>1));?></li>
					</ul>
          </div>
</div>			
			<!--[if !IE]>start row<![endif]-->
<div class="row">
				<label >Company name: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Company.name', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err2"><?php echo $form->error('Company.name'); ?></span>
        </div>
</div>			
<div class="row">
				<label >Address: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Company.address', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err2"><?php echo $form->error('Company.address'); ?></span>
        </div>
</div>
			
<div class="row">
				<label>City: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Company.city', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err2"><?php echo $form->error('Company.adress'); ?></span>
        </div>
</div>

<div class="row">
				<label >State: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Company.state', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err2"><?php echo $form->error('Company.state'); ?></span>
        </div>
</div>

<div class="row">
				<label>Zip: <span class="star">*</span></label>
				<div class="inputs">
					<span class="input_wrapper">
					<?php echo $form->text('Company.zip', array('maxLength'=>'50','class'=>'text'));  ?>
					</span>
					<span class="system negative" id="err2"><?php echo $form->error('Company.zip'); ?></span>
        </div>
</div>

<div class="row">
				<div class="inputs">
					<span class="button red_button"><span><span>Save</span></span><?php echo $form->submit('Save',array('action'=>''));  ?></span>
    			<span class="button red_button"><a href="/admin/companies/subscriberslist"><span><span style="color:white">Cancel</span></span></a></span>
				</div>
			</div>
			<!--[if !IE]>end row<![endif]-->			
			</div>
			<!--[if !IE]>end forms<![endif]-->						
		</fieldset>
		<!--[if !IE]>end fieldset<![endif]-->			
	<?php echo $form->end(); ?>
	<!--[if !IE]>end forms<![endif]-->	
</div>
<!--[if !IE]>end section inner<![endif]-->
</div>
<!--[if !IE]>end section<![endif]-->