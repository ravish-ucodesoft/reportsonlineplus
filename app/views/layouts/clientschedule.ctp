<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title_for_layout; ?></title>
<?php echo $html->css('manager/theme'); ?>
<?php echo $html->css('manager/style'); ?>
<script type="text/javascript">
   var StyleFile = "theme" + document.cookie.charAt(6) + ".css";
   document.writeln('<link rel="stylesheet" type="text/css" href="/css/manager/' + StyleFile + '">');
</script>
<script type="text/javascript" src="/js/jquery/jquery.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.scripts.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.alerts.js"></script>
<link href="/css/jquery.alerts.css" rel="stylesheet"/>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="/css/manager/ie-sucks.css" />
<![endif]-->


<style type="text/css">
tr.navrow  td {
background-color: #E5EEF5; font-size:11px; color:#E5EEF5; text-align:center; border:1px solid #E5EEF5;
}
tr.navrow a:link, a:hover a:visited {
font-size:11px;
}
.initial td { background-color: #E5EEF5; font-size:11px; color:#E5EEF5; border:1px solid #E5EEF5; }
.normal td{ background-color: #E5EEF5; font-size:11px;  color:#E5EEF5; border:1px solid #E5EEF5; }
.highlight td { background-color: #FFFFFF; font-size:11px; color:#000000; border:1px solid black; text-align:center; cursor:pointer;}
#calbtn  {
background-color: #E5EEF5;
color:black;
font-size:11px;
font-weight:bold;}
#calbtn a {
font-size:11px;
}
</style>


</head>

<body>
	<div id="container">
    		<!--[if !IE]>start head<![endif]-->
		    <?php 
        	 $action = $this->params['action'];
				 if($action=='manager_login') {
				  echo $this->element('client_login_header');
          } else {
         echo $this->element('client_header');         
         }
          ?>
		    <!--[if !IE]>end head<![endif]-->
		    
       	<?php 
         if($session->check('Message.flash'))
			  	{
        ?>
			  	 <div id="top-panel" style='margin-top:10px;'><div id="panel"><?php echo $this->Session->flash();?></div></div>
			  <?php
				  }
			  ?>          
           
       
        <div id="wrapper">
           	<?php  echo $content_for_layout; ?>
           	 
        </div>
       
      <!--[if !IE]>start footer<![endif]-->
     	<?php echo $this->element('inspector_footer');?>
    	<!--[if !IE]>end footer<![endif]-->
       
</div>

	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
