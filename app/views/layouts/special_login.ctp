<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php echo $this->Html->meta('favicon.ico','/img/favicon.ico',array('type' => 'icon'));?>
<title><?php echo $title_for_layout; ?></title>
<?php //echo $html->css('manager/theme'); ?>
<?php echo $html->css('newdesign_css/style'); ?>
<?php echo $html->css('manager/style'); ?>
<?php echo $this->Html->css('newdesign_css/superfish'); ?>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript">
   var StyleFile = "theme" + document.cookie.charAt(6) + ".css";
   document.writeln('<link rel="stylesheet" type="text/css" href="/css/manager/' + StyleFile + '">');
</script>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="/css/manager/ie-sucks.css" />
<![endif]-->
</head>
<body>
   <div id="container">    		
      <?php echo $this->element('special_header');?>		    
      <?php 
         if($session->check('Message.flash'))
         {
      ?>
            <div id="top-panel" ><div id="panel"><?php echo $this->Session->flash();?></div></div>
      <?php
         }
      ?>
      <div id="wrapper">
         <?php  echo $content_for_layout; ?>           	 
      </div>      
   </div>
   <?php echo $this->element('sql_dump'); ?>
   <div class="loadingImage">
      <?php echo $html->image('loading.gif');?>
   </div>
</body>
</html>