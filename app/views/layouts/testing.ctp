<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php echo $this->Html->charset(); ?>
<title><?php echo $title_for_layout; ?></title>
<?php echo $this->Html->css('front_style'); ?>
<?php //echo $this->Html->css('prettyphoto/prettyPhoto.css'); ?>
<?php //echo $this->Html->script('prettyphoto/jquery-1.4.4.min'); ?>
<?php //echo $this->Html->script('prettyphoto/jquery.prettyPhoto'); ?>
 
</head>
<body>
	<!-- Container Starts -->
	<div id="container">
    	<!-- Page Holder Starts -->
    	<div id="header-container">
        	<!-- Header Starts -->
	    	<div id="header">
          
            	<!-- Header Top Starts -->
                <div class="header-top">
                	<div class="head-links">
                    	<div class="wrap-head-left">
                        <a href="http://172.24.0.9:9437/categories/login?iframe=true&height=400&width=500" rel="prettyPhoto[iframe]">Google.ca</a>
		
                      <a href="/owners/signup" alt="Register" title="Register">Register</a>
                      

                      
                      </div>
                      <div class="wrap-head-right"></div>
                    </div>
                    <div class="menu">
                    	<div class="logo"><a href="/homes/index"><img src="/img/logo.png" alt="SchedulePal" title="SchedulePal" /></a></div>
                        <ul class="menu-list">
                        	   <li><?php echo $html->link('<span>Home</span>',array('controller'=>'homes','action'=>'index'),array('title'=>'Home','alt'=>'Home','escape'=>false));?></li>
                            <li><?php echo $html->link('<span>About Us</span>',array('controller'=>'homes','action'=>'aboutus'),array('title'=>'About Us','alt'=>'About Us','escape'=>false));?></li>
                            <li><?php echo $html->link('<span>Benefits and Pricing</span>',array('controller'=>'homes','action'=>'products'),array('title'=>'Products','alt'=>'Products','escape'=>false));?></li>
                            <li><?php echo $html->link('<span>Frequent Questions</span>',array('controller'=>'homes','action'=>'faqs'),array('title'=>'FAQ','alt'=>'FAQ','escape'=>false));?></li>
                            <li><?php echo $html->link('<span>Testimonials</span>',array('controller'=>'homes','action'=>'testimonials'),array('title'=>'Testimonials','alt'=>'Testimonials','escape'=>false));?></li>
                            
                            <li><?php echo $html->link('<span>Customer Service</span>',array('controller'=>'homes','action'=>'customerservice'),array('title'=>'Customer Service','alt'=>'Customer Service','escape'=>false));?></li>
                                                                                                                      
                        </ul>
                    </div>
                </div>
                <!-- Header Top Ends -->
                
                <!-- Content for layout [STARTS] -->
                
                <?php  echo $content_for_layout; ?>
                
                <!-- Content for layout [ENDS] -->
            
            <!-- Footer Starts -->
            <div id="footer">
            	<div class="footer-section">
                	<!-- Footer Row Starts -->
                    <div class="row1">
                        <!-- Footer Col Starts -->
                        <div >
                            
                            <ul>                                
                            <li style="color:#FFF;"><?php echo $html->link('Home',array('controller'=>'homes','action'=>'index'),array('title'=>'Home','alt'=>'Home','escape'=>false));?> | 
                            <?php echo $html->link('About Us',array('controller'=>'homes','action'=>'aboutus'),array('title'=>'About Us','alt'=>'About Us','escape'=>false));?> | 
                            <?php echo $html->link('Benefits and Pricing',array('controller'=>'homes','action'=>'products'),array('title'=>'Products','alt'=>'Products','escape'=>false));?> | 
                            <?php echo $html->link('Frequent Questions',array('controller'=>'homes','action'=>'faqs'),array('title'=>'FAQ','alt'=>'FAQ','escape'=>false));?> | 
                            <?php echo $html->link('Testimonials',array('controller'=>'homes','action'=>'testimonials'),array('title'=>'Testimonials','alt'=>'Testimonials','escape'=>false));?> | 
                            <?php echo $html->link('Customer Service',array('controller'=>'homes','action'=>'customerservice'),array('title'=>'Customer Service','alt'=>'Customer Service','escape'=>false));?></li>
                            </ul>
                            <div class="right1">SchedulePal LLC<br/>
                                                P.O. Box 1800<br />
                                                Iowa City, IA 52244</div>
                        </div>
                        <!-- Footer Col Ends -->
                        
                    </div>
                    <!-- Footer Row Ends -->
                    
                 <div class="copyright">
                 	<div class="footer-left">SchedulePal &copy; 2010. All rights reserved.</div>
                 </div>
                    
                </div>
            </div>
            <!-- Footer Ends -->
        </div>
        <!-- Page Holder Ends -->
    </div>
    

    <!-- Container Ends -->
    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
