<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title_for_layout; ?></title>
<?php echo $html->css('manager/theme'); ?>
<?php echo $html->css('manager/style'); ?>
<script>
   var StyleFile = "theme" + document.cookie.charAt(6) + ".css";
   document.writeln('<link rel="stylesheet" type="text/css" href="/css/manager/' + StyleFile + '">');
</script>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="/css/manager/ie-sucks.css" />
<![endif]-->
</head>

<body>
	<div id="container">
    		<!--[if !IE]>start head<![endif]-->
		    <div id="header" style='padding-bottom: 60px;'>
        	 <h2>Login area</h2>
   
      </div>
		    <!--[if !IE]>end head<![endif]-->
		    
       	<?php 
         if($session->check('Message.flash'))
			  	{
        ?>
			  	 <div id="top-panel" style='margin-top:10px;'><div id="panel"><?php echo $this->Session->flash();?></div></div>
			  <?php
				  }
			  ?>          
           
       
        <div id="wrapper">
           	<?php  echo $content_for_layout; ?>
           	 
        </div>
       
      <!--[if !IE]>start footer<![endif]-->
     	<?php echo $this->element('manager_footer');?>
    	<!--[if !IE]>end footer<![endif]-->
       
</div>

	<?php echo $this->element('sql_dump'); ?>
</body>
</html>