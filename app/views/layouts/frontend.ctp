<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php echo $this->Html->meta('favicon.ico','/img/favicon.ico',array('type' => 'icon'));?>
<title><?php echo $title_for_layout; ?></title>
<?php echo $html->css('front_style'); ?>
<?php
echo $this->Html->script("jquery.1.6.1.min.js"); ?>
<script type='text/javascript' src='http://www.gayadesign.com/scripts/queryLoader/js/queryLoader.js'></script>
</head>
<style>
.QOverlay {
	background-color: #000000;
	z-index: 9999;
}

.QLoader {
	background-color: #CCCCCC;
	height: 1px;
}
</style>
<body>

<!-- Container Starts -->
<div id="container">
	<!-- Page Holder Starts -->
	<div id="header-container">
		<!-- Header Starts -->
		<div id="">
            	<!-- Header Top Starts -->
              <?php echo $this->element('front/front_header'); ?>
                <!-- Header Top Ends -->
                
                <!-- Content for layout [STARTS] -->
                
                <?php  echo $content_for_layout; ?>
                
                <!-- Content for layout [ENDS] -->
            
             <?php echo $this->element('front/front_footer'); ?>
		</div>
	
    </div>
	<!-- Page Holder Ends -->
</div>
	
    <!-- Container Ends -->
    <?php echo $this->element('sql_dump'); ?>
    <script>
		QueryLoader.selectorPreload = "body";
		QueryLoader.init();
	</script>
</body>
</html>
