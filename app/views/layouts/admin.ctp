<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $title_for_layout;?></title>

<?php echo $html->css('meta-admin'); ?>

<?php echo $html->css('general'); ?>
<?php $action = $this->params['action'];
		if($action=='admin_login') 
		  { 
     echo $html->css('meta-admin-login');
     echo $html->css('general');  
}?>

<?php //echo $html->css('forms'); ?>
<?php echo $html->script('jquery/jquery.1.4.2'); ?>
<style>
.message{ text-align:center;}
</style>
</head>

<body>
	<!--[if !IE]>start wrapper<![endif]-->
	<div id="wrapper">
		<!--[if !IE]>start head<![endif]-->
		
<?php
		$action = $this->params['action'];
				 
         if($action=='admin_login') 
         {
		echo $this->element('admin_login_header');
         } 
          else
         {
		 echo $this->element('admin_header');     
         } 		 
?>
		<!--[if !IE]>end head<![endif]-->
		<?php  if($action=='admin_login') 
		       {
		?>
		
		<?php  echo $content_for_layout; // only show output when no error occured ?>
				  
		<?php } else {  ?>

		<!--[if !IE]>start content<![endif]-->
		<div id="content">
			<!--[if !IE]>start content bottom<![endif]-->
			<div id="content_bottom">
			<div class="inner">
				<!--[if !IE]>start info<![endif]-->
				<div id="info">
			<!-- ########### main body part start ########## -->
<?php 
       if($session->check('Message.flash'))
       {
?>
				<div class='message'><?php echo $this->Session->flash();?></div>
<?php
       }
?>
<?php  echo $content_for_layout; //only show output when no error occured ?>
		       <!-- ########### main body part ends ########## -->	
				</div>
				<!--[if !IE]>end info<![endif]-->
			</div>
			<!--[if !IE]>end content bottom<![endif]-->
			</div>
			
		</div>
		<!--[if !IE]>end content<![endif]-->
		
		</div>
	<!--[if !IE]>end wrapper<![endif]-->
	
	<!--[if !IE]>start footer<![endif]-->

	<?php 
   echo $this->element('admin_footer');
   }
   ?>
	<!--[if !IE]>end footer<![endif]-->
	
	<?php echo $this->element('sql_dump'); ?>
	
</body>
</html>