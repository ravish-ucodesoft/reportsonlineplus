<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php echo $this->Html->meta('favicon.ico','/img/favicon.ico',array('type' => 'icon'));?>
<title><?php echo $title_for_layout; ?></title>
<?php echo $html->css('newdesign_css/style'); ?>
<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<?php echo $this->Html->css('newdesign_css/superfish'); ?>
<?php echo $this->Html->script('jquery.1.6.1.min');?>
<?php   
    echo $this->Html->script('jquery-ui-1.8.20.custom.min.js');    
?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript">
    $(function(){
	jQuery(".ajax").colorbox();
    });    
</script>

<!--[if lt IE 9]>

<![endif]-->
<script type="text/javascript">

		// initialise plugins
		jQuery(function(){
			//jQuery('ul.sf-menu').superfish();
		});

		</script>
</head>

<body>
	<!-- Top Blue Bar-->
  <section class="top_blue_bar"></section>
<!-- Top Blue Bar-->

<!-- Wrapper-->
<section class="wrapper"> 
<?php //pr($this->params);
if(isset($this->params['pass'][0]))
{
    $res=mysql_query("SELECT page_type FROM `websitepages` WHERE id =".$this->params['pass'][0]);
    $row=mysql_fetch_array(@$res);  
}
     if($this->params['controller'] == "users" && $this->params['action'] == "signup"){
        echo $this->element('front/front_first_sp_header');
     }else if($this->params['controller'] == "users" && $this->params['action'] == "step1"){
        //echo $this->element('front/front_second_sp_header');
	echo $this->element('front/front_first_sp_header');
     }else if($this->params['controller'] == "users" && $this->params['action'] == "step2"){
        //echo $this->element('front/front_second_sp_header');
	echo $this->element('front/front_first_sp_header');
     }
       
     else if($this->params['controller'] == "homes" && $this->params['action'] == "pages" && ($row['page_type']=='D')){
        echo $this->element('front/front_second_sp_header');
     }else {
        echo $this->element('front/front_header');
     }
?>
  <!-- Content for layout [STARTS] -->
                
        <?php  echo $content_for_layout; ?>
        <?php echo $this->element('front/bottom_row');?>        
  <!-- Content for layout [ENDS] -->
</section>
  <!-- Wrapper-->
		<?php echo $this->element('front/front_footer'); ?>	
  <!-- End: Footer -->

</body>
</html>
