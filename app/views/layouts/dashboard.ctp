<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $title_for_layout; ?></title>
<?php echo $html->css('meta-admin'); ?>
<?php echo $html->css('general'); ?>
</head>

<body>
	<!--[if !IE]>start wrapper<![endif]-->
	<div id="wrapper" >
		<!--[if !IE]>start head<![endif]-->
		<?php
		 echo $this->element('admin_header')
		 ?>
		<!--[if !IE]>end head<![endif]-->
			
			<!-- ########### main body part start ########## -->				
        <?php 
        if($session->check('Message.flash'))
				{
				?>
				  <div class='message'><?php $session->flash(); ?></div>
				<?php
				}
				?>
				
				<?php echo $content_for_layout; ?>
			<!-- ########### main body part ends ########## -->	
	
	</div>
	<!--[if !IE]>end wrapper<![endif]-->	
	<!--[if !IE]>start footer<![endif]-->
	<?php echo $this->element('admin_footer');?>
	<!--[if !IE]>end footer<![endif]-->
  	
</body>
</html>
