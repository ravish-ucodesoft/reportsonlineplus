
<?php
    echo $this->Html->script('jquery.1.6.1.min');    
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');    
?>
<style>
	td{
		border:none;
	}
</style>
<link rel=stylesheet href="http://static.thomasjbradley.ca/signature-pad/jquery.signaturepad.css">

<div id="content" style="min-height:500px" >
	<div id="box" style="border:none;">
		<h3 style="color:#2281CF; padding-left:10px;">Inspector's Signature</h3><br/>
		<table style="padding-left:10px;">
			<tr>
				<td width="15%"><b>Lead Inspector</b></td><td><?php echo $this->Common->getClientName($res['Schedule']['lead_inspector_id']);?></td>
			</tr>
			<tr>
				<td><b>Helper</b></td>
				<td><?php
							$helper_ins_arr=explode(',',$res['Schedule']['helper_inspector_id']);
							echo $this->Common->getHelperName($helper_ins_arr); ?></td>
			</tr>
			<tr>
				<td><b>Sch Date(M/D/Y)</b></td>
				<td>
					<?php echo date('m/d/Y',strtotime($res['Schedule']['schedule_date_1'])).' '.$res['Schedule']['schedule_from_1'].'-'.$res['Schedule']['schedule_to_1']?><br/>
						<?php if(!empty($res['Schedule']['schedule_date_2'])) { ?>
						<?php echo date('m/d/Y',strtotime($res['Schedule']['schedule_date_2'])).' '.$res['Schedule']['schedule_from_2'].'-'.$res['Schedule']['schedule_to_2']?><br/>
						<?php } ?>
						<?php if(!empty($res['Schedule']['schedule_date_3'])) { ?>
						<?php echo date('m/d/Y',strtotime($res['Schedule']['schedule_date_3'])).' '.$res['Schedule']['schedule_from_3'].'-'.$res['Schedule']['schedule_to_3']?><br/>
						<?php } ?>
						<?php if(!empty($res['Schedule']['schedule_date_4'])) { ?>
						<?php echo date('m/d/Y',strtotime($res['Schedule']['schedule_date_4'])).' '.$res['Schedule']['schedule_from_4'].'-'.$res['Schedule']['schedule_to_4']?><br/>
						<?php } ?>
						<?php if(!empty($res['Schedule']['schedule_date_5'])) { ?>
						<?php echo date('m/d/Y',strtotime($res['Schedule']['schedule_date_5'])).' '.$res['Schedule']['schedule_from_5'].'-'.$res['Schedule']['schedule_to_5']?>
						<?php } ?>
							    
				</td>
			</tr>
		</table>
		
	</div>
    <div role=main style="margin-left:200px;">
  
  <h2 style="color:#2281CF"> Signature For <?php echo $this->Common->getReportName($reportId); ?></h2>
  <div style="margin-top:25px">
  <form method=post action="getSignature" class=sigPad>
    <?php echo $form->input('sp_id',array('type'=>'hidden','value'=>$spId)); ?>
	<?php //echo $form->input('Signature.inspector_id',array('type'=>'hidden','value'=>$inspector_id)); ?>
	<?php echo $form->input('servicecall_id',array('type'=>'hidden','value'=>$servicecallId)); ?>
	<?php echo $form->input('client_id',array('type'=>'hidden','value'=>$clientId)); ?>
	<?php echo $form->input('report_id',array('type'=>'hidden','value'=>$reportId)); ?>
	<?php echo $form->input('old_signature',array('type'=>'hidden','value'=>$datasign['Signature']['signature_image'])); ?>
	<?php echo $form->input('id',array('type'=>'hidden','value'=>$datasign['Signature']['id'])); ?>
	<?php if(!empty($datasign['Signature']['id'])) { ?>
	<label style="font-size:15px;"> Old Signature:</label>
	
	<?php echo $this->Html->image('signature_images/'.$datasign['Signature']['signature_image']);  ?>
    <label for=name style="font-size:15px;">Change your Signature</label>
	<?php }else{ ?>
	<label for=name style="font-size:15px;">Print your Signature</label>
	<?php } ?>
    <input type=text name=name id=name class=name>
      <p class=typeItDesc>Review your signature</p>
      <p class=drawItDesc>Draw your signature</p>
      <ul class=sigNav>
      <!--  <li class=typeIt><a href="#type-it" class=current>Type It</a></li> -->
        <li class=drawIt><a href="#draw-it" class=current>Draw It</a></li>
        <li class=clearButton><a href="#clear">Clear</a></li>
      </ul>
      <div class="sig sigWrapper">
        <div class=typed></div>
        <canvas class=pad width=198 height=55></canvas>
        <input type="hidden" name="output" class="output" id="coutput">
      </div>
	  <br/>
      <!--<input type=submit name=OK value=OK>-->
	   <input type="submit" id="linkFinish", name="OK" value="OK" style="margin-left:6; border:0; color:#fff; width:50px; height:31px; padding:0px 18px 0px 10px; background:url(../../img/manager/blue-btn-rt.png) no-repeat right top !important;"/>
  </form>
  </div>
  <?php echo $this->Html->script('signature_pad/jquery.signaturepad.min.js');  ?>
  <script>
 
 jQuery(document).ready(function () {    
  jQuery('.sigPad').signaturePad();
  });
  </script>
  <?php echo $this->Html->script('signature_pad/json2.min.js');  ?>
  </div>
             
		</form>
            </div>