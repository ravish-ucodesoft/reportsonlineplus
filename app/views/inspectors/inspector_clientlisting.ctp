<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;"><?php echo $html->link('Add Client',array('controller'=>'agents','action'=>'addclients'),array("style"=>"padding-right:20px;")); ?></div>
	      
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addclients'),'id'=>'addclient')); ?>
              <div id="box" >
                	<h3 id="adduser">Client Listing</h3>
                        <br/>
                        <table class="tbl">
                        <tr>
			    <th width="20%">Name</th>
			    <th width="20%">Email</th>
			    <th width="20%">Company Name</th>
			    <th width="20%">Action</th>
                        </tr>
			<?php foreach($result  as $res) {
			    $cid = base64_encode($res['User']['id']);
			?>
			<tr>
			    <td width="20%" align="center"><?php echo $res['User']['fname']." ".$res['User']['lname']; ?></th>
			    <td width="20%" align="center"><?php echo $res['User']['email']; ?></td>
			    <td width="20%" align="center"><?php echo $res['Company'][0]['name']; ?></td>
			    <td width="20%" align="center">
				<?php echo $html->link('Edit '.$html->image('user_edit.png',array('alt'=>'User Edit','title'=>'User Edit')),array('controller'=>'agents','action'=>'editclient',$cid),array('escape'=>false)); ?>
				<?php echo $html->link('Resend Credentials',array('controller'=>'agents','action'=>'credentialmail',$cid),array('')); ?>
			    </td>
                        </tr>
			<?php } ?>
                      </table>
                </div>
		<?php echo $form->end(); ?>
            </div>