<?php echo $this->Html->script(array('jquery.1.6.1.min')); ?>
<?php echo $this->Html->css(array('gallery/colorbox')); ?>
<?php echo $this->Html->script(array('colorbox/jquery.colorbox')); ?>
<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery(".ajax").colorbox();
});
</script>
<style>
a, a:visited {
    color: #666666;
}
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <h3 id="adduser">Code Chart List</h3>
                <table class="tbl">
                <thead>
                <tr>
                <th width="10%" align="left"><?php echo $paginator->sort('Report Name','report.name');?></th>		
			    <th width="10%" align="left"><?php echo $paginator->sort('Code','Code.code');?> </th>			    
			    <th width="45%" align="left"><?php echo $paginator->sort('Description','Code.description');?></th>
                <th width="15%" align="left"><?php echo $paginator->sort('Code Type','Code.code_type_id');?></th>	
                </tr>
                </thead>
                <?php $i=1;
                foreach ($codeListing as $codelist): 		    
                ?>
                <tr>			    
                    <td align="left"><?php echo $codelist['Report']['name']; ?></td>			    
                    <td><?php echo $codelist['Code']['code']; ?></td>
                    <td align="left"><?php echo $this->Html->link(substr($codelist['Code']['description'],0,60)."...",array('controller'=>'inspectors','action'=>'codeDescription',base64_encode($codelist['Code']['id'])), array('class' => 'ajax','title'=>"",'escape'=>false)); ?></td>  
                    <td align="left"><?php echo $codelist['CodeType']['code_type']; ?></td>
                                
                </tr>
                <?php
                $i++;
                endforeach; ?>
                </table>
                
			
</div>