<?php
    echo $this->Html->script('jquery.1.6.1.min');   
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core.js','ui/ui.datepicker.js','jquery-ui-1.8.20.custom.min.js'));
    /*** alert box**/    
    echo $this->Html->css('alertbox/jquery.alerts.css');
    echo $this->Html->css('jquery/jquery.ui.draggable.js');
    echo $this->Html->script('alertbox/jquery.alert.js');
    echo $this->Html->css(array('tooltip')); 
    echo $this->Html->script(array('tooltip'));
?>

<style>
.inner{
	width:100%;
}
.inner td {font-size:12px;color:#3E4061}
.linktoreport{color:red;text-decoration:none;}
.thheading{ background-color:#999;color:#FFF;}
</style>
<script type="text/javascript">
 /*****Function for preview thw report in popup window*****/
function PopupCenter(pageURL, title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
jQuery(document).ready(function(){


});
$(function(){
	// Accordion
    $("#accordion").accordion({ header: "h3",active:false,collapsible: true});
    $('#dialog_link, ul#icons li').hover(
		function() { $(this).addClass('ui-state-hover'); },
		function() { $(this).removeClass('ui-state-hover'); }
    );   
    
});
function switchView(id,parameter)
{
    
	
	if(id=='accordingView')
	{
	    if(parameter=='cl'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    if(parameter=='calender'){
		  
		$("#calendarView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#accordingView").slideUp(500);
	    }
		
		
	}
	if(id=='clientView'){
	    if(parameter=='c'){
		$("#calendarView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#clientView").slideUp(500);
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    
	}
	else
	{
	    if(parameter=='c'){
		$("#clientView").slideDown(500);
		$("#accordingView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
	    if(parameter=='a'){
		$("#accordingView").slideDown(500);
		$("#clientView").slideUp(500);
		$("#calendarView").slideUp(500);
	    }
		//$("#calendarView").slideUp(500);
		//$("#accordingView").slideDown(500);
	}
}
</script>
<style type="text/css">
    /*demo page css*/
    /*body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}*/
    .demoHeaders { margin-top: 2em; }
    #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
    #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
    ul#icons {margin: 0; padding: 0;}
    ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
    ul#icons span.ui-icon {float: left; margin: 0 4px;}
    
    
    inner{ width:100%; }
    .inner td {font-size:12px;color:#3E4061}
    .normal{
	background-color: #e8e8e8;
	border: 1px solid #EEEEEE;
	color: #000000;
	font-size: 11px;
	    text-align: center;
	    font-weight:bold;
    }
    .normal td {
	background-color: #e8e8e8;
	border: 1px solid #EEEEEE;
	color: #000000;
	font-size: 11px;
	    text-align: center;
	    font-weight:bold;
    }
    .highlight td:hover { background:#fff; }
    .highlight td {
	background-color: #e8e8e8;
	border: 1px solid #c6c6c6;
	color: #000000;
	cursor: pointer;
	font-size: 11px;
	text-align: center;
	      font-weight:bold;
    }
    #calbtn {background:url("../../../img/newdesign_img/nav_bg.png") 0 -7px #808080; }
    #calbtn p { color:#fff}
    #schw{height:90px; text-align:left; width:100%; margin:2px;border:1px solid #FFF; color:#000;}
    .viewmode {font-weight:bold; text-align:left; font-size:14px; color:grey; float:left; margin-right:10px;}
    
    .butn {float:right;padding-right:20px;margin-bottom:10px;}
    .butn a { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -5px; padding:5px; color:#000; border-radius:5px;}
    .butn a:hover { color:#fff; background:url("../../../img/newdesign_img/nav_bg.png") 0 -14px; color:#fff}
</style>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <div style="float:right;margin-top:7px;"></div>
    <div id="box" >
	<h3 id="adduser">Assigned Schedule as Service Calls </h3>
        <br/>
	<div id="accordingView" style="display: none">
	    <div class="butn"><a href="javascript:void(0);" onclick="switchView('clientView','c');">Switch to Calendar View</a></div>
	    <br/>
	    <table class="tbl">
		<tr></tr>
		<tr>
		    <td width="70%" align="left">
			<div id="accordion">			
			    <?php
				if(!empty($data))
				{
				       foreach($data as $key=>$res):
				       
				       if(!empty($res['Schedule']))
				       {
					   $serviceCall_ID=$res['ServiceCall']['id'];
						$client_ID=$res['ServiceCall']['client_id'];
						$sp_ID=$res['ServiceCall']['sp_id'];
						 
			    ?>
					<div>
					<h3><a href="#" style="color: #000 !important"><?php echo 'Name : '.$res['ServiceCall']['name'].' <span style="color:grey">[Sch. requested : '.date('m/d/Y',strtotime($res['ServiceCall']['created'])).']</span>'; ?></a></h3>
					<?php $cid = base64_encode($client_ID);?>
					<table class="inner" cellpadding="0" cellspacing="0">
						<tr>
						    <td colspan="8" style="background:url('../../../img/newdesign_img/nav_active.png');color:#FFF;font-size:14px;">
							<strong>Client :: <?php echo $this->Common->getClientCompanyName($res['ServiceCall']['client_id']);?></strong>
						    </td>
						</tr>
						<tr>
						    <td colspan="7" style="background-color:#fff;color:#817679;font-size:14px;">
							<strong>Site Address : <?php echo $this->Common->getSiteInfo($res['ServiceCall']['sp_site_address_id']);?></strong>													
						    </td>
						    <td style="background-color:#fff;color:#817679;font-size:14px;">
							<?php echo $html->link($html->image('map.png',array('alt'=>'Address Map','title'=>'Address Map')),'javascript:void(0)',array('escape'=>false,'onclick'=>"PopupCenter('".$html->url(array('controller'=>'sps','action'=>'siteaddress_mapdirection',base64_encode($res['ServiceCall']['sp_site_address_id']),'inspector'=>false))."','','760','490');")); ?>
							<?php echo $html->link('Direction',array('controller'=>'inspectors','action'=>'getsitedirection',base64_encode($res['ServiceCall']['sp_site_address_id']),'inspector'=>false),array('class'=>'ajax','style'=>'font-size:9px;color:#817679;'));?>
						    </td>
						</tr>
						<tr style="background:url('../../../img/newdesign_img/nav_bg.png') bottom;">
						<th width="15%" class="thheading" align="center" style="color:#FFF">Report</th>
						<th width="10%" class="thheading" align="center" style="color:#FFF">Lead Inspector</th>
						<th width="15%" class="thheading" align="center" style="color:#FFF">Helper</th>
						<th width="17%" class="thheading" align="center" style="color:#FFF">Sch Date(M/D/Y)</th>
						<th width="14%" class="thheading" align="center" style="color:#FFF">Submission Days Left</th>
						<th width="10%" class="thheading" align="center" style="color:#FFF">Status</th>
						<th width="9%" class="thheading" align="center" style="color:#FFF">&nbsp;</th>
						<th width="5%" class="thheading" align="center" style="color:#FFF">&nbsp;</th>	
						</tr>
						<?php foreach($res['Schedule'] as $key1=>$schedule):?>
						<tr style="background-color:#eaeaea;">
							<td align="center"><strong><?php echo $this->Common->getReportName($schedule['report_id']);?></strong></td>
							<td align="center"><?php echo $this->Common->getClientName($schedule['lead_inspector_id']);?></td>
							<td align="center"><?php
							$helper_ins_arr=explode(',',$schedule['helper_inspector_id']);
							echo $this->Common->getHelperName($helper_ins_arr); ?></td>
							<td align="center">
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_1'])).' '.$time->format('g:ia', $schedule['schedule_from_1']).'-'.$time->format('g:ia', $schedule['schedule_to_1']); ?>
<br/>
							    <?php if(!empty($schedule['schedule_date_2'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_2'])).' '.$time->format('g:ia', $schedule['schedule_from_2']).'-'.$time->format('g:ia', $schedule['schedule_to_2'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_3'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_3'])).' '.$time->format('g:ia', $schedule['schedule_from_3']).'-'.$time->format('g:ia', $schedule['schedule_to_3'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_4'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_4'])).' '.$time->format('g:ia', $schedule['schedule_from_4']).'-'.$time->format('g:ia', $schedule['schedule_to_4'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_5'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_5'])).' '.$time->format('g:ia', $schedule['schedule_from_5']).'-'.$time->format('g:ia', $schedule['schedule_to_5'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_6'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_6'])).' '.$time->format('g:ia', $schedule['schedule_from_6']).'-'.$time->format('g:ia', $schedule['schedule_to_6'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_7'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_7'])).' '.$time->format('g:ia', $schedule['schedule_from_7']).'-'.$time->format('g:ia', $schedule['schedule_to_7'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_8'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_8'])).' '.$time->format('g:ia', $schedule['schedule_from_8']).'-'.$time->format('g:ia', $schedule['schedule_to_8'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_9'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_9'])).' '.$time->format('g:ia', $schedule['schedule_from_9']).'-'.$time->format('g:ia', $schedule['schedule_to_9'])?><br/>
							    <?php } ?>
							    <?php if(!empty($schedule['schedule_date_10'])) { ?>
							    <?php echo date('m/d/Y',strtotime($schedule['schedule_date_10'])).' '.$time->format('g:ia', $schedule['schedule_from_10']).'-'.$time->format('g:ia', $schedule['schedule_to_10'])?>
							    <?php } ?>
							</td>
							<td style="text-align: center;">
							    <?php echo $this->Common->getRemainingDays($schedule['id']);?>
							</td>
							<td align='center'>
							<?php
							 //echo $schedule['report_id']."--".$res['ServiceCall']['id']."---".$res['ServiceCall']['client_id']."--".$res['ServiceCall']['sp_id'];
							      $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							      $getstarted = $this->Common->getReportStarted($schedule['report_id'],$res['ServiceCall']['id'],$res['ServiceCall']['client_id'],$res['ServiceCall']['sp_id']);
							     if($getCompleted != "" && $getstarted == 1){
								echo $html->image('fineimages/finished.png');
								 ?>								
							     <?php
							    }
							     if($getstarted == 0 && $getCompleted == ""){
								echo $html->image('fineimages/not_started.png');
							      }
							      else if($getstarted == 1 && $getCompleted == ""){
								echo $html->image('fineimages/pending.png'); 
							      }
							?>
						    </td>
							<td align="center">
								<?php
								$permissionfill=$this->Common->checkPermission(4,$session_inspector_id);
								$getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$serviceCall_ID,$client_ID,$sp_ID);
								//if($session_inspector_id!=$schedule['lead_inspector_id'])
								if($permissionfill!=1)
								{
								    echo $html->link($html->image('fineimages/link_to_report.png'),'javascript:void(0)',array('title'=>'Fill the report','class'=>'linktoreport','onclick'=>'jAlert("You do not have the permission to access the files")','escape'=>false));
								?>								    
									
								<?php } else {
								    echo $html->link($html->image('fineimages/link_to_report.png'),'/inspector/reports/'.$this->Common->getReportLinkName($schedule['report_id']).'?reportID='.$schedule['report_id'].'&clientID='.$client_ID.'&spID='.$sp_ID.'&serviceCallID='.$serviceCall_ID,array('title'=>'Fill the report','class'=>'linktoreport','escape'=>false));	
								?>								
							    <?php
							    } 
							    if($getCompleted != ""){ ?>
								<br/>
								<span style="font-size:10px;">Completed on <?php echo date('m/d/Y',strtotime($getCompleted)); ?></span>
							    <?php } ?>
								</td>
							
							<td>
								<!--SIGNATURE ICON-->
								<?php $permissionsign=$this->Common->checkPermission(5,$session_inspector_id);
								if($permissionsign!=1)
								{?>
								<a href="javascript:void(0)" class="linktoreport" onclick="jAlert('<?php echo $permissionsign;?>')" title="Add your signature"><?php echo $html->image('icon_signature.png',array('title'=>'Add your signature'));?></a>
									
								<?php
								}
								else
								{?>
								<a class="linktoreport" title="Add your signature" href="/inspector/inspectors/sign?reportID=<?php echo $schedule['report_id']; ?>&clientID=<?php echo $client_ID; ?>&spID=<?php echo $sp_ID; ?>&serviceCallID=<?php echo $serviceCall_ID; ?>"><?php echo $html->image('icon_signature.png',array('title'=>'Add your signature'));?></a>									
								<?php
								}
								?>
								&nbsp;
								<!--PREVIEW ICON-->
								<?php $permissionreview=$this->Common->checkPermission(3,$session_inspector_id);
								$linkname=$this->Common->getReportViewLink($schedule['report_id']);
								if($permissionreview!=1)
								{?>
								
								<!--<a href="javascript:void(0)" class="linktoreport" onclick="jAlert('<?php echo $permissionreview;?>')" title="Review the Report"><?php echo $html->image('icon_preview.png',array('title'=>'Review the Report'));?></a>-->
									
								<?php
								}
								else
								{?>
								
								<!--<a href="/inspector/reports/<?php echo $linkname;?>?reportID=<?php echo $schedule['report_id']; ?>&clientID=<?php echo $client_ID; ?>&spID=<?php echo $sp_ID; ?>&serviceCallID=<?php echo $serviceCall_ID; ?>" id="reviewanswer" target="_blank">
								
								<?php echo $html->image('icon_preview.png',array('title'=>'Review the Report'));?></a>-->
								<?php
								}
								?>
								
							</td>
							
						</tr>
						<tr><td colspan="8" align="left">
						<div class="service-hdline">Services Taken</div>
						<ul class="services-tkn"><li>
						<?php
						    $i=0;$j=1;
						$row = mysql_query("select * from schedule_services where schedule_id = '".$schedule['id']."'");
						$affected_row=mysql_num_rows($row);
						 while($res = mysql_fetch_array($row)){
							
							echo $html->image('tick.png');?><?php echo $this->Common->getServiceName(@$res['service_id']);?>(<?php echo @$res['amount'] ?>,<?php echo $frequency[@$res['frequency']]; ?>)
						<?php if($j%4==0){
							echo '</li></ul><ul class="services-tkn"><li>';
						}
						else{
							echo '</li><li>';
						}
						 $j++;
						}
						?>
                    
						</td></tr>
						<?php
						    $certs = $this->Common->getAllSchCerts($schedule['id']);//echo '<pre>';print_r($certs);die;
						    if(sizeof($certs)>0){
						?>
						<tr><td colspan="8">
						<h2 style="margin:5px;">CERT attached by Service Provider</h2>
						<!--<div class="certificates-hdline"><?php echo $html->image('fineimages/cert_attached.png');?></div>-->
						<ul class="services-tkn"><li>
						<?php $cr=1;
						foreach($certs as $cert)
						{
							echo $html->link($cert['SpBlankCertForm']['title'],array("controller"=>"messages","action"=>"download_cert_form",$cert['SpBlankCertForm']['cert_form']),array('title'=>'DownLoad/View'));
							echo '<div style="padding-left:20px">'.$html->link($html->image('submit.gif',array('style'=>'float:none')),array('controller'=>'messages','action'=>'uploadReportCert',$cert['ReportCert']['id']),array('escape'=>false,'class'=>'ajax')).'</div>';
						?>
						<?php if($cr%4==0){
							echo '</li></ul><ul class="services-tkn"><li>';
						}
						else{
							echo '</li><li>';
						}
						 $cr++;
						}
						?>		
						</td></tr>
						<?php						
						}		
						?>
						
						<?php endforeach;?>
					</table>				
					</div>
					
					
				<?php 	
					}  // END IF AFTER FOREACH
					 endforeach;
					
				}?>
				</div>
		    </td>
		</tr>
	    </table>
	</div>
	
	<!-- calendarView div ID closed-->
	<div id="calendarView" style="display:block">
	    <div class="viewmode">You are watching Weekly View</div>
	    <div class="viewmode"><a href="/inspector/inspectors/schedule">&raquo; Switch to Monthly View</a></div>	    	    	    
            <div class="butn"><a href="javascript:void(0);" onclick="switchView('calendarView','a');">Switch to Accordian View</a></div>
            
	    <!--Calendar Start-->
            <table width="100%" cellpadding='0' cellspacing='0' border='0' style="clear:both">                        
                <tr><td><?php echo $calendar->calmenu($tstamp); ?></td></tr>
                <tr>
                    <td colspan="2">        
                        <?php $arr=array();		
                                foreach($data as $rec){
                                        foreach($rec['Schedule'] as $sch){
                                                unset($sch['ScheduleService']);
                                                $arr[] = $sch;
                                        }				
                                }
                        ?>        
                        <table width="100%">
                            <thead> 
                                <tr>                  
                                    <th width="14%"><?php echo $calendar->getday($tstamp,1); ?></th>
                                    <th><?php echo $calendar->getday($tstamp,2); ?></th>
                                    <th><?php echo $calendar->getday($tstamp,3); ?></th>
                                    <th><?php echo $calendar->getday($tstamp,4); ?></th>
                                    <th><?php echo $calendar->getday($tstamp,5); ?></th>
                                    <th><?php echo $calendar->getday($tstamp,6); ?></th>
                                    <th><?php echo $calendar->getday($tstamp,7); ?></th>
                                </tr>
                            </thead>
                            <tbody>            
                                <tr>
                                <?php 
                                    for($i=1;$i<=7;$i++){
                                    $dateYMD=$calendar->getdateYMD($tstamp,$i);
                                ?>
                                    <td valign="top" style="height:100px;" width="14%" class="boxtd" style="cursor:pointer;">
                                        <div style='width:100%; text-align: center; margin:2px 0 10px 0; border: 1px solid #EEEEEE;min-height:500px;'>
                                        <?php
                                            foreach($arr as $key1=>$schedule):
						$siteName = $this->Common->getSiteNameByServiceCallID($schedule['service_call_id']);
                                                if($dateYMD==@$schedule['schedule_date_1'] || $dateYMD==@$schedule['schedule_date_2'] || $dateYMD==@$schedule['schedule_date_3'] || $dateYMD==@$schedule['schedule_date_4'] || $dateYMD==@$schedule['schedule_date_5'] || $dateYMD==@$schedule['schedule_date_6'] || $dateYMD==@$schedule['schedule_date_7'] || $dateYMD==@$schedule['schedule_date_8'] || $dateYMD==@$schedule['schedule_date_9'] || $dateYMD==@$schedule['schedule_date_10']){
						    $serviecCallData = $this->Common->getServiceDataByServiceId($schedule['service_call_id']);
						    $getCompleted = $this->Common->getReportCompleted($schedule['report_id'],$serviecCallData['ServiceCall']['id'],$serviecCallData['ServiceCall']['client_id'],$serviecCallData['ServiceCall']['sp_id']);
						    $getstarted = $this->Common->getReportStarted($schedule['report_id'],$serviecCallData['ServiceCall']['id'],$serviecCallData['ServiceCall']['client_id'],$serviecCallData['ServiceCall']['sp_id']);
						    if($getCompleted != "" && $getstarted == 1){						   
							$callStatus = 'Completed';
						    }
						    if($getstarted == 0 && $getCompleted == ""){						   						    
							$callStatus = 'Not Started yet';
						    }
						    else if($getstarted == 1 && $getCompleted == ""){
							$callStatus = 'Pending';
						    }
                                ?>
                                                    <div id='schw' style="background-color:<?php echo $this->Common->getUserColorCode($schedule['lead_inspector_id']);?>">
							<?php
						    $txtToShow='<strong>Name:</strong>'.$this->Common->getServiceCallName($schedule['service_call_id']);						    
						    $txtToShow.='<br/><strong>Site Name:</strong>'.$siteName;
						    $txtToShow.='<br/><strong>Lead:</strong>'.$this->Common->getClientName($schedule['lead_inspector_id']);
						    $txtToShow.='<br/><strong>Report:</strong>'.$this->Common->getReportName($schedule['report_id']);
						    $txtToShow.='<br/><strong>Client:</strong>'.$this->Common->getCompanyNameByServiceCallID($schedule['service_call_id']);
						    $txtToShow.='<br/><strong>Days left for report submission:</strong>'.$this->Common->getRemainingDays($schedule['id']);
						    $txtToShow.='<br/><strong>Report Status:</strong>'.$callStatus;
						?>		   
					       <p onmouseover="tooltip.show('<?php echo $txtToShow;?>');" onmouseout="tooltip.hide();" class="intp"><strong>Name:</strong><?php echo $this->Common->getServiceCallName($schedule['service_call_id']);?><br/>
							    <strong>Site Name:</strong><?php echo $siteName;?><br/>
                                                            <strong>Report:</strong><?php echo $this->Common->getReportName($schedule['report_id']);?>                                                            
                                                        </p>
                                                    </div>  
                                                <?php }                       
                                            endforeach;                                                              
                                        ?>
                                        </div>  
                                    </td>
                                <?php
                                    }
                                ?>            
                                </tr>        
                            </tbody>    
                        </table>
                    </td>
                </tr>
            </table>        
	</div>
	<!-- calendarView div ID closed-->
    </div>
</div>
<script type="text/javascript">
function redirecturl(tstamp)
{
 window.location='/inspector/inspectors/weekly_view/'+tstamp;
}
</script>