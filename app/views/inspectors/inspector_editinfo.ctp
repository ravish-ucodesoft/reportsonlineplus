<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css(array('ajaxuploader/fileuploader'));
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
    echo $this->Html->script(array('ajaxuploader/fileuploader'));
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#editinfo").validationEngine();
});

 function limit(field, chars) {
        if (document.getElementById(field).value.length > chars) {
            document.getElementById(field).value = document.getElementById(field).value.substr(0, chars);
            var errMsg="You are only allowed to enter "+chars+" characters.";
            document.getElementById('essay_Error').innerHTML= errMsg;
        }
        var len=document.getElementById(field).value.length;
        if(len < chars){document.getElementById('essay_Error').innerHTML='';}
        document.getElementById('limitCounter').innerHTML=len;
        document.getElementById('limitCounterLeft').innerHTML=chars-len;
    }
    
    function createUploader(){	    
            var uploader = new qq.FileUploader({
                element: document.getElementById('demo'),
                listElement: document.getElementById('separate-list'),
                action: '/users/ajaxupload',		
		debug: true,
		onComplete: function(id, fileName, responseJSON){ $("#CompanyImage").val(responseJSON.company_image);$("#separate-list").hide();
		$("#uploaded_picture").html('<img src="/img/company_logo_temp/'+responseJSON.company_image+'" height="100px" width="200px">');
	    }
            });           
    }        
	window.onload = createUploader;
</script>
<style>
input { width:250px;}
.qq-uploader {
    margin-left: 2px;
}
</style>
<div id="content" >
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'editinfo'),'id'=>'editinfo')); ?>
	      <?php echo $form->input('User.id', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$this->data['User']['id']));  ?>
	      <?php //echo $form->input('Company.id', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$this->data['Company']['id']));  ?>
	      <?php //echo $form->input('Company.company_logo_old', array('type'=>'hidden',"div"=>false,"label"=>false,'value'=>$this->data['Company']['company_logo']));  ?>
	      <?php //echo $form->input('Company.company_logo_new', array('id'=>'CompanyImage','type'=>'hidden',"div"=>false,"label"=>false));  ?>
              <div id="box" >
                	<h3 id="adduser">Edit Personnel Information</h3>
	                
                        <br/>
                        <table class="tbl">
                        <tr><td width="20%">First Name<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?>           
                        </tr>   
                        
                        <tr><td>Last Name<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false));  ?></td>
                        </tr>
			
			<tr><td>Qualification:</td>
                        <td><?php echo $form->input('User.qualification', array('maxLength'=>'50',"div"=>false,"label"=>false));  ?></td>
                        </tr>
                        <tr><td>Phone<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.phone', array('maxLength'=>'50',"div"=>false,"label"=>false,'class'=>'validate[required]'));  ?></td>
                        </tr>
			
			<tr><td>Email<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.email', array('maxLength'=>'50','readonly'=>true,"div"=>false,"label"=>false,'class'=>'validate[required,custom[email]]','style'=>'background-color:#C2DFFF'));  ?></td>
                        </tr>
			
			<tr><td><div align="right">
			    <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
			    </div></td><td>			    
			    </td>
	                  </tr>
              
                      </table>
                </div>

              <!--<div id="box" >
                	<h3 id="adduser">Edit Company Information</h3>
                        <br/>
                        <table class="tbl">
                        <tr><td width="20%">Company Name<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->input('Company.name', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?>           
                        </tr>   
                        
                        <tr><td>Address<span class="astric">*</span> </td>
                        <td><?php echo $form->input('Company.address', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?></td>
                        </tr>                         
                        
                        <tr><td>Phone</td>
                        <td><?php echo $form->input('Company.phone', array('maxLength'=>'20','class'=>'text',"div"=>false,"label"=>false));  ?></td>                       
                        </tr>
			
			<tr><td>Website</td>
                        <td><?php echo $form->input('Company.website', array('maxLength'=>'50',"div"=>false,"label"=>false));  ?></td>                       
                        </tr>
			
			<tr><td>Authorized/Designated Manager of the site<span class="reqd">*</span></td>
                        <td><?php echo $form->input('Company.site_manager', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?></td>                       
                        </tr>
			
			<tr><td>Direct Phone of Designated Site Manager<span class="reqd">*</span></td>
                        <td><?php echo $form->input('Company.phone_site_manager', array('maxLength'=>'50','class'=>'validate[required,custom[number]]',"div"=>false,"label"=>false)); ?></td>                       
                        </tr>
			
			<tr><td>Direct email of Designated Site Manager<span class="reqd">*</span></td>
                        <td><?php echo $form->input('Company.email_site_manager', array('maxLength'=>'50','class'=>'validate[required,custom[email]]',"div"=>false,"label"=>false)); ?></td>                       
                        </tr>
			
			<tr><td valign="top">Company Bio<span class="reqd">*</span></td>
                        <td>
			    <?php echo $form->input('Company.company_bio', array('type'=>"textarea",'class'=>'validate[required]',"div"=>false,"label"=>false,'rows'=>7,'cols'=>43,'onKeyUp' =>"return limit(this.id,200);"));  ?><br/>
			    <div style="float:left;">
			         <span id='limitCounter'>0</span>&nbsp;characters entered&nbsp;|&nbsp;<span id='limitCounterLeft'>200</span>&nbsp;characters remaining
			    </div>
			    <div id="essay_Error" class='error'>&nbsp;</div>
			
			</td>                       
                        </tr>
			
			<tr><td>Company Logo<span class="reqd">*</span></td>
                        <td>
			    <div id="demo"></div>
			    <ul id="separate-list"></ul>
			    <div id="Errormsg" style="padding-left:265px;color:red;font-weight:bold;"></div>
			</td>                       
                        </tr>
			
			<tr><td>&nbsp;</td>
                        <td><div id="uploaded_picture"  style="float:left;"></div></td>                       
                        </tr>
			
			<tr><td>&nbsp;</td>
                        <td><div id="already_uploaded_picture"  style="float:left;"><?php echo $html->image('/img/company_logo/'.$agentsession['Company'][0]['company_logo'],array('height'=>100,'width'=>200)); ?></div></td>                       
                        </tr>
			
			<tr><td>Company Principal #1 name and email (Sales Manager)</td>
                        <td><?php echo $form->input('Company.principle_name_salemanager', array('maxLength'=>'50',"div"=>false,"label"=>false,"style"=>"width:200px;padding-right:10px;"));  ?>
			    &nbsp;&nbsp;<?php echo $form->input('Company.principle_email_salemanager', array("div"=>false,"label"=>false,"style"=>"width:200px;")); ?></td>                       
                        </tr>
			
			<tr><td>Company Principal #2 name and email (Service Manager/Administrator)</td>
                         <td><?php echo $form->input('Company.principle_name_servicemanager', array('maxLength'=>'50',"div"=>false,"label"=>false,"style"=>"width:200px;"));  ?>&nbsp;&nbsp;
			    <?php echo $form->input('Company.principle_email_servicemanager', array("div"=>false,"label"=>false,"style"=>"width:200px;"));  ?>
	                 </td>                       
                        </tr>
			
			<tr><td>Company Principal #3 name and email (Inspection Manager)</td>
                         <td><?php echo $form->input('Company.principle_name_inspectionmanager', array('maxLength'=>'50',"div"=>false,"label"=>false,"style"=>"width:200px;"));  ?>&nbsp;&nbsp;
			     <?php echo $form->input('Company.principle_email_inspectionmanager', array("div"=>false,"label"=>false,"style"=>"width:200px;"));  ?>
	                 </td>                       
                        </tr>
			
			<tr><td>Company Principal #4 name and email (Scheduler)</td>
                         <td><?php echo $form->input('Company.principle_name_scheduler', array('maxLength'=>'50',"div"=>false,"label"=>false,"style"=>"width:200px;"));  ?>&nbsp;&nbsp;
			     <?php echo $form->input('Company.principle_email_scheduler', array("div"=>false,"label"=>false,"style"=>"width:200px;"));  ?>
	                 </td>                       
                        </tr>
			
			
			<tr><td>Company Principal #5 name and email (Deficiency Manager)</td>
                         <td><?php echo $form->input('Company.principle_name_deficiencymanager', array('maxLength'=>'50',"div"=>false,"label"=>false,"style"=>"width:200px;"));  ?>
			     <?php echo $form->input('Company.principle_email_deficiencymanager', array("div"=>false,"label"=>false,"style"=>"width:200px;"));  ?>
	                 </td>                       
                        </tr>
			
	                 <tr><td></td><td>
			    <div align="right">
			    <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
			    </div>
			    </td>
	                  </tr>
                      </table>
                    	
                </div>-->
		<?php echo $form->end(); ?>
            </div>