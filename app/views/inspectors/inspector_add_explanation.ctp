<?php
    echo $this->Html->css('manager/style.css');
    echo $this->Html->css('manager/theme.css');
    echo $this->Html->script('jquery.1.6.1.min');  
    echo $this->Html->css('validationEngine.jquery');    
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');    
?>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#expForm").validationEngine();   
});
function openForm(){
    jQuery("#expForm").slideDown(300);
}
</script>
<div id="content">
    <h2 style="font-size: 24px; text-align: center;"><strong>Explanation Text</strong></h2>
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>    
    <?php if(is_array($textInfo) && !empty($textInfo)){?>
    <div style="float: left; width: 100%; text-align: center; padding-top: 10px;">
        Text added. Want to update, <?php echo $html->link('Click Here','javascript:void(0)',array('color:#ff0000','onclick'=>'openForm();'));?>
    </div>
    <?php echo $this->Form->create('', array('url'=>array('controller'=>'inspectors','action' =>'addExplanation?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID']),'id'=>'expForm','name'=>'expForm','style'=>'display: none;','enctype'=>'multipart/form-data') );?>
        <section class="form-box" style="width:100%; margin: 0;">                
            <ul class="register-form">                
                <li>
                    <label>Missed Device Explanation</label>                    
                    <p><span class=""><?php echo $form->textarea('ExplanationText.missing_reason', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','rows'=>'3','cols'=>'25','value'=>$textInfo['ExplanationText']['missing_reason']));  ?></span></p>                       
                </li>
                <!--<li>
                    <label>Missed Device Attachment(if any)</label>                    
                    <p>
                        <span class=""><?php //echo $form->input('ExplanationText.missingfile', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','type'=>'file'));?></span>
                        <?php if($textInfo['ExplanationText']['missing_file']!="" && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$textInfo['ExplanationText']['missing_file'])){?>
                        <span style="padding-left: 5px;"><?php //echo $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$textInfo['ExplanationText']['missing_file'],'inspector'=>false))?></span><span style="padding-left: 5px"><?php echo $html->link('X',array('controller'=>'inspectors','action'=>'deleteRecord',$textInfo['ExplanationText']['missing_file']),array('style'=>'color:red','title'=>'Delete','onclick'=>'if(!confirm("Are you sure, you want to delete this attachment?")){ return false;}'));?></span>
                        <?php }?>
                    </p>                       
                </li>-->
                
                <li>
                    <label>Additional Device Explanation</label>                    
                    <p><span class=""><?php echo $form->textarea('ExplanationText.additional_reason', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','rows'=>'3','cols'=>'25','value'=>$textInfo['ExplanationText']['additional_reason']));  ?></span></p>                       
                </li>
                <!--<li>
                    <label>Additional Device Attachment(if any)</label>                    
                    <p>
                        <span class=""><?php //echo $form->input('ExplanationText.addfile', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','type'=>'file'));?></span>
                        <?php if($textInfo['ExplanationText']['additional_file']!="" && file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$textInfo['ExplanationText']['additional_file'])){?>
                        <span style="padding-left: 5px;"><?php //echo $html->link('Attachment',array('controller'=>'reports','action'=>'downloadImage',$textInfo['ExplanationText']['additional_file']))?></span><span style="padding-left: 5px"><?php echo $html->link('X',array('controller'=>'inspectors','action'=>'deleteRecord',$textInfo['ExplanationText']['additional_file']),array('style'=>'color:red','title'=>'Delete','onclick'=>'if(!confirm("Are you sure, you want to delete this attachment?")){ return false;}'));?></span>
                        <?php }?>
                    </p>                       
                </li>-->
                <li>
                    <label>&nbsp;</label>
                    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>                   
                </li>              
            </ul>
        </section>
    <?php echo $form->end(); ?>
    <?php }else{?>
    <?php echo $this->Form->create('', array('url'=>array('controller'=>'inspectors','action' =>'addExplanation?reportID='.$_REQUEST['reportID'].'&clientID='.$_REQUEST['clientID'].'&spID='.$_REQUEST['spID'].'&serviceCallID='.$_REQUEST['serviceCallID']),'id'=>'expForm','name'=>'expForm','enctype'=>'multipart/form-data') );?>
        <section class="form-box" style="width:100%; margin: 0">                
            <ul class="register-form">                
                <li>
                    <label>Missing Device Explanation</label>
                    <p><span class=""><?php echo $form->textarea('ExplanationText.missing_reason', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','rows'=>'3','cols'=>'25'));  ?></span></p>                       
                </li>
                <!--<li>
                    <label>Missing Device Attachment(if any)</label>                    
                    <p><span class=""><?php //echo $form->input('ExplanationText.missingfile', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','type'=>'file'));?></span></p>                       
                </li>-->
                <li>
                    <label>Additional Device Explanation</label>                    
                    <p><span class=""><?php echo $form->textarea('ExplanationText.additional_reason', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','rows'=>'3','cols'=>'25'));  ?></span></p>                       
                </li>
                <!--<li>
                    <label>Additional Device Attachment(if any)</label>                    
                    <p>
                        <span class=""><?php //echo $form->input('ExplanationText.addfile', array("div"=>false,"label"=>false, 'style'=>'border:1px solid #AAAAAA','type'=>'file'));?></span>                        
                    </p>                       
                </li>-->
                <li>
                    <label>&nbsp;</label>
                    <p class="bttm-btns"><span class="blue-btn"><span><?php echo $form->submit('Submit',array('id'=>'','div'=>false,'class'=>'','style'=>'width:60px;background-color:transparent;border:none; color:#FFFFFF; padding-top:5px')); ?></span></span></p>                   
                </li>              
            </ul>
        </section>
    <?php echo $form->end(); ?>
    <?php }?>
     <br/>   
</div>