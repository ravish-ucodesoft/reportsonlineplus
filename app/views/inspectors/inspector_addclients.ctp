<?php
    echo $this->Html->script('jquery.1.6.1.min');
    echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->script('languages/jquery.validationEngine-en.js');
    echo $this->Html->script('jquery.validationEngine.js');
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#addclient").validationEngine();
});
function getStates(id,value){
    var optArr = $("#"+id).val();
	jQuery("#UserStateId").html("<option value=''>Select</option>");
	jQuery.ajax({
                type : "GET",
                url  : "/users/getByCountry/" + value,
                success : function(opt){
			jQuery('#UserStateId').html(opt);
                }
        });
}
</script>
<style>
.error-message {
    background-position: center bottom;
    background-repeat: no-repeat;
    color: #B86464;
    display: block;
    float: right;
    font-weight: bold;
    position: relative;
    top: 30px;
    white-space: nowrap;
}
</style>
<div id="content">
	      <div class='message'><?php echo $this->Session->flash();?></div>
	      <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'addclients'),'id'=>'addclient')); ?>
              <div id="box" >
                	<h3 id="adduser">Add Client</h3>
                        <br/>
                        <table class="tbl">
                        <tr><td width="20%">First Name<span class="reqd">*</span>:</td>
                       	<td><?php echo $form->input('User.fname', array('maxLength'=>'50','class'=>'validate[required]',"div"=>false,"label"=>false));  ?></td>           
                        </tr>   
                        
                        <tr><td>Last Name:</td>
                        <td><?php echo $form->input('User.lname', array('maxLength'=>'50',"div"=>false,"label"=>false));  ?></td>
                        </tr>
			
			<tr><td>Email<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.email', array('maxLength'=>'50',"div"=>false,"label"=>false,'class'=>'validate[required,custom[email]]'));  ?></td>
                        </tr>
			
			<tr><td>Password<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.password',array('id'=>'password','maxLength'=>'10','type'=>'password','class'=>'validate[required]','div'=>false,"label"=>false)); ?></td>
                        </tr>
			
			<tr><td>Confirm Password<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('User.cpwd',array('type'=>'password','class'=>'validate[required,equals[password]]','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>
			
			<tr><td>Company Name<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('Company.name',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>
			
			<tr><td>Contact Phone Number<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('Company.phone',array('type'=>'text','class'=>'validate[required,custom[number]]','div'=>false,"label"=>false,'maxLength'=>'10')); ?>
                        </td>                       
                        </tr>
			
			<tr><td>Company Street Address:</td>
                        <td><?php echo $form->input('Company.address',array('type'=>'text','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>
			
			<tr><td>Country<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('Company.country_id',array('options' =>$country,'id'=>'UserCountryId','label'=>false,"style"=>"width:208px;",'empty'=>'Please Select',"onchange"=>"getStates(this.id,this.value);")); ?>
                        </td>                       
                        </tr>
			
			<tr><td>state<span class="reqd">*</span>:</td>
                        <td>
			    <?php $state = ""; ?>
			    <?php  echo $this->Form->input('Company.state_id',array('options' =>$state,'id'=>'UserStateId','label' => false,'style'=>'width:208px;','empty'=>'Please Select'));?>
                        </td>                       
                        </tr>
			
			<tr><td>City<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('Company.city',array('type'=>'text','class'=>'validate[required]','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>
			
			<tr><td>Zip<span class="reqd">*</span>:</td>
                        <td><?php echo $form->input('Company.zip',array('type'=>'text','class'=>'validate[required,custom[number]]','div'=>false,"label"=>false)); ?>
                        </td>                       
                        </tr>
			
			<tr><td valign="top">Service on your property<span class="reqd">*</span>:</td>
                        <td>
			    <?php foreach($services as $serv){ ?>
			    <?php echo $form->input('User.property_services.'.$serv['PropertyService']['id'],array('type'=>'checkbox','value'=>$serv['PropertyService']['id'],'div'=>false,"label"=>false)); ?><?php echo $serv['PropertyService']['name']; ?><br/>
			    <?php } ?>
                        </td>                       
                        </tr>
			<tr><td>&nbsp;</td>
                        <td>
			    <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;')); ?>
                        </td>                       
                        </tr>
			
                      </table>
                </div>
		<?php echo $form->end(); ?>
            </div>