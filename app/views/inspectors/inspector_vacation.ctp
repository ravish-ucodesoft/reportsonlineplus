<?php
    echo $this->Html->script('jquery.1.6.1.min');
    //echo $this->Html->css('validationEngine.jquery');
    echo $this->Html->css(array('ui/themes/base/ui.all.css'));
    echo $this->Html->css('ui-lightness/jquery-ui-1.8.20.custom');
    echo $this->Html->script(array('ui/ui.core','ui/ui.datepicker','jquery-ui-1.8.20.custom.min'));
    echo $this->Html->script(array('timepicker/jquery-1.7.1.min'));
    echo $this->Html->script(array('timepicker/jquery-ui-1.8.16.custom.min','timepicker/jquery-ui-timepicker-addon','timepicker/jquery-ui-sliderAccess'));
    //echo $this->Html->script('languages/jquery.validationEngine-en.js');
    //echo $this->Html->script('jquery.validationEngine.js');
    
     /*** alert box**/
    
    echo $this->Html->css('alertbox/jquery.alerts.css');
    echo $this->Html->css('jquery/jquery.ui.draggable.js');
    echo $this->Html->script('alertbox/jquery.alert.js'); 
?>
<script type="text/javascript">

function loadDatePicker()
{
   jQuery(".calender").datepicker({
	dateFormat: 'mm/dd/yy',
	showOn: 'button',
	minDate:1 ,
	buttonImage: '/app/webroot/img/calendar.gif',
	buttonImageOnly: true,
	onSelect: function(){
	    //setMultiDate(jQuery(this).attr('name'),jQuery(this).val());    	    
	}
    });
}
jQuery(document).ready(function(){
loadDatePicker();
jQuery("#inspector_vacation").submit(function(){	
	if(jQuery("#InspectorVacationStartDate").val()==""){	    
	    jAlert('Please select the start date');
	    jQuery("#InspectorVacationStartDate").focus();
	    return false;
	}
	
	if(jQuery("#InspectorVacationEndDate").val()==""){	    
	    jAlert('Please select the end date');
	    jQuery("#InspectorVacationEndDate").focus();
	    return false;
	}
    
    if(jQuery("#InspectorVacationEndDate").val() < jQuery("#InspectorVacationStartDate").val()){	    
	    jAlert('End date cannot be less than start date');
	    jQuery("#InspectorVacationEndDate").focus();
	    return false;
	}
    
    
 });
 });
</script>
<div id="content">
    <div class='message' style="text-align:center;"><?php echo $this->Session->flash();?></div>
    <h3 id="adduser">Vacation List</h3>
                <table class="tbl">
                <thead>
                <tr>
                <th width="5%">S.No</th>		
			    <th width="45%">Start Date</th>			    
			    <th width="45%">End Date</th>			   
                </tr>
                </thead>
                <?php $i=1;
                foreach($vacation  as $res) {			    
                ?>
                <tr>			    
                    <td align="center"><?php echo $i;?></td>			    
                    <td align="center"><?php echo date('m/d/Y',strtotime($res['InspectorVacation']['start_date'])); ?></td>  
                    <td align="center"><?php echo date('m/d/Y',strtotime($res['InspectorVacation']['end_date'])); ?></td>
                                
                </tr>
                <?php
                $i++;
                } ?>
                </table>
                
                <br/>
            <div id="box" >
            <h3 id="adduser">Add New Vacation(Date Range)</h3>    
            <?php echo $form->create('', array('type'=>'POST', 'url'=>array('action'=>'inspector_vacation'),'id'=>'inspector_vacation','div'=>false,'label'=>false)); ?>
            <?php echo $form->input('InspectorVacation.user_id', array('type'=>'hidden',"div"=>false,"label"=>false,'readonly'=>true,'value'=>$inspector_id));  ?>
		   <table width="100%" class="tbl">
		    <tr><td width="20%">Start Date<span class="reqd">*</span>:</td>
                <td><?php echo $form->input('InspectorVacation.start_date', array('type'=>'text','class'=>'calender',"div"=>false,"label"=>false,'readonly'=>true));  ?></td>           
            </tr>   
                
            <tr><td>End Date<span class="reqd">*</span>:</td>
                <td><?php echo $form->input('InspectorVacation.end_date', array('type'=>'text','class'=>'calender',"div"=>false,"label"=>false,'readonly'=>true));  ?></td>
            </tr>
            
            <tr><td colspan="2">
			    <?php echo $form->submit('Submit',array('id'=>'button1','style'=>'width:80px;'));  ?>                  
			    <span style="font-style:italic; font-size:10px;color:#3D94DC;">After Submit,an email notification about your vacation leave will send to service provider.</span>		    
			    </td>
	        </tr>
           </table>
           <?php echo $form->end(); ?>
            </div>
                
                
</div>
                     
