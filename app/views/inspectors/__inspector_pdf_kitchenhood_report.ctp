<?php

App::import('Vendor','tcpdf'); 
$tcpdf = new TCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
$tcpdf->setPrintHeader(false);
//$tcpdf->SetAuthor("smartData.com");
$tcpdf->SetAutoPageBreak( true );
$tcpdf->xfootertext = '';
$tcpdf->AddPage();


$html = '<h2 align="center">'. ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']) .'</h2>
<table border="0" cellspacing="0" cellpadding="2">
	<tr>
	   <td colspan="5" align="center"><strong> RANGE HOOD SYSTEMS REPORT</strong> </td>
	</tr>
  <tr>
      <td><strong>Date of Service:</strong>'. $kitchenData['KitchenhoodReport']['service_date'] .	'</td>
      <td><strong>Time:</strong> ' . $kitchenData['KitchenhoodReport']['totaltime'].' '.$kitchenData['KitchenhoodReport']['totaltime_format'] .'</td>
	   <td colspan="3">&nbsp;</td>
	</tr>
	<tr>
      <td><strong>Service Provider Info.</strong></td>
      <td>' . ucWords($spResult['User']['fname'].' '.$spResult['User']['lname']) .'</td>
	   <td colspan="3">Location of System Cylinders:&nbsp;'. $kitchenData['KitchenhoodReport']['locationsystem'].'</td>
	</tr>
	<tr>      
      <td><strong>Customer:</strong></td>
	    <td><strong>Manufacturer:</strong></td>
	    <td><strong>Model:</strong></td>
	    <td><strong>Wet:</strong></td>
	    <td><strong>Dry Chemical:</strong></td>
	</tr>
	<tr>      
      <td>'.ucWords($clientResult['User']['fname'].' '.$clientResult['User']['lname']) . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['manufacturer'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['model'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['wet'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['chemical'] . '</td>
	</tr>
	<tr>
	    <td colspan="5">&nbsp;</td>	    
	</tr>
	<tr>      
      <td><strong>Address:</strong></td>
	    <td><strong>Cylinder Size (Master):</strong></td>
	    <td><strong>Cylinder Size (Slave):</strong></td>
	    <td><strong>Cylinder Size (Slave):</strong></td>
	    <td><strong>&nbsp;</strong></td>
	</tr>
	<tr>      
      <td>'. $clientResult['User']['address']. '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['mastersize'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['slavesize'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['slavesize1'] . '</td>
	    <td>&nbsp;</td>
	</tr>
	<tr>
	    <td colspan="5">&nbsp;</td>	    
	</tr>
	<tr>      
      <td><strong>Location:</strong></td>
	    <td><strong>Fuse Links 360 F:</strong></td>
	    <td><strong>Fuse Links 450 F:</strong></td>
	    <td><strong>Fuse Links 500 F:</strong></td>
	    <td><strong>Other</strong></td>
	</tr>
	<tr>      
      <td>'. $clientResult['User']['site_address']. '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['fuselink1'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['fuselink2'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['fuselink3'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['fuselink_other'] . '</td>
	</tr>
	<tr>
	    <td colspan="5">&nbsp;</td>	    
	</tr>
	<tr>      
      <td><strong>Attention:</strong></td>
	    <td><strong>Fuel Shut Off:</strong></td>
	    <td><strong>Electric:</strong></td>
	    <td><strong>Gas:</strong></td>
	    <td><strong>Size</strong></td>
	</tr>
	<tr>      
      <td>'. $kitchenData['KitchenhoodReport']['attention']. '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['shutoff'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['electric'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['gas'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['size'] . '</td>
	</tr>
	<tr>
	    <td colspan="5">&nbsp;</td>	    
	</tr>
	<tr>      
      <td><strong>Phone:</strong></td>
	    <td><strong>Serial Number:</strong></td>
	    <td><strong>Last Hydro Test Date:</strong></td>
	    <td><strong>Last Recharge Date:</strong></td>
	    <td><strong>&nbsp;</strong></td>
	</tr>
	<tr>      
      <td>'. $clientResult['User']['phone']. '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['serialnumber'] . '</td>
	    <td>'. $time->Format('m-d-Y',$kitchenData['KitchenhoodReport']['hydro_date']) . '</td>
	    <td>'. $time->Format('m-d-Y',$kitchenData['KitchenhoodReport']['recharge_date']) . '</td>
	    <td>&nbsp;</td>
	</tr>
	<tr>
	    <td colspan="5">&nbsp;</td>	    
	</tr>
	<tr>
	    <td colspan="5"><strong><i>COOKING APPLIANCE SIZES (NOTE: List appliances from left to right and indicate Nozzles used for each)</i></strong></td>	    
	</tr>
	<tr>
	    <td colspan="5"><strong><i>(Please Note the Plenum and Duct Size(s) in appropriate boxes below)</i></strong></td>	    
	</tr>
	<tr>      
      <td><strong>Plenum Size(s):</strong></td>
	    <td>'. $kitchenData['KitchenhoodReport']['plenumsize1'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['plenumsize2'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['plenumsize3'] . '</td>
	    <td><strong>&nbsp;</strong></td>
	</tr>
	<tr>      
      <td><strong>Duct Size(s):</strong></td>
	    <td>'. $kitchenData['KitchenhoodReport']['ductsize1'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['ductsize2'] . '</td>
	    <td>'. $kitchenData['KitchenhoodReport']['ductsize3'] . '</td>
	    <td><strong>&nbsp;</strong></td>
	</tr>
	<tr>
	    <td colspan="5">&nbsp;</td>	    
	</tr>	
	</table>
	<table  border="0" cellspacing="0" cellpadding="2">
	<tr>
	    <td colspan="2">1.All appliances properly covered w/ correct Nozzles</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_1'].'</td>	
      <td colspan="2">19. Check travel of able nuts/S-hooks</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_19'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">2.Duct and plenum covered w/ correct Nozzles</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_2'].'</td>	
      <td colspan="2">20. Piping and conduit securely bracketed</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_20'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">3.Check positioning of all Nozzles</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_3'].'</td>	
      <td colspan="2">21.Proper separation between fryers & flame</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_21'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">4.System installed in accordance w/ Mfg UL listing</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_4'].'</td>	
      <td colspan="2">22.Proper clearance-flame to filters</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_22'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">5.Hood/duct penetrations sealed w/weld or UL device</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_5'].'</td>	
      <td colspan="2">23.Exhaust fan operating properly</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_23'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">6. Check if seals intact, evidence of tampering</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_6'].'</td>	
      <td colspan="2">24. All filters reinstalled</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_24'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">7. If system has been discharged, report same</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_7'].'</td>	
      <td colspan="2">25. Fuel shut-off in ON position 	</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_25'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">8. Pressure gauge in proper range (if gauged)</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_8'].'</td>	
      <td colspan="2">26.Manual & Remove set/seals in place</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_26'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">9. Check cartridge weight (Replace, if needed)</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_9'].'</td>	
      <td colspan="2">27.Reinstall systems covers</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_27'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">10.Hydrostatic/6 year maintenance date</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_10'].'</td>	
      <td colspan="2">28.System operational and seals in place</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_28'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">11. Inspect cylinder and mount</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_11'].'</td>	
      <td colspan="2">29.Slave system operational</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_29'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">12. Operate system from terminal link</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_12'].'</td>	
      <td colspan="2">30.Clean cylinder and mount</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_30'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">13.Test for proper operation from remote</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_13'].'</td>	
      <td colspan="2">31.Fan warning sign on hood</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_31'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">14. Check operation of micro switch</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_14'].'</td>	
      <td colspan="2">32. Personnel instructed in manual operation of system</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_32'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">15. Check operation of gas valve</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_15'].'</td>	
      <td colspan="2">33.Proper hand portable extinguishers(K Class and ABC)</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_33'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">16.Proper Nozzle covers in place/clean Nozzles</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_16'].'</td>	
      <td colspan="2">34.Portable extinguishers properly serviced</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_34'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">17.Check fuse links and clean</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_17'].'</td>	
      <td colspan="2">35.Service & certification tag on system</td>	 
      <td>'.$kitchenData['KitchenhoodReport']['question_35'].'</td>	         
	</tr>
	<tr>
	    <td colspan="2">18. Replaced fuse links (Record date here)</td>	
      <td>'.$kitchenData['KitchenhoodReport']['question_18'].'</td>	
      <td colspan="3">&nbsp;</td>	      	         
	</tr>
	<tr>
	    <td colspan="2">NoTE DISCREPANCIES OR DEFICIENCIES:</td>	
      <td>'.$kitchenData['KitchenhoodReport']['discrepancies'].'</td>	
      <td colspan="3">&nbsp;</td>	      	         
	</tr>
	<tr>
	    <td colspan="2">Service Technician:&nbsp;&nbsp;'.$kitchenData['KitchenhoodReport']['service_technician'].'</td>	
      <td>Date:&nbsp;&nbsp;'.$time->Format('m-d-Y',$kitchenData['KitchenhoodReport']['k_date']).'</td>	
      <td colspan="2">Time:&nbsp;&nbsp;'.$kitchenData['KitchenhoodReport']['k_time'].' '.$kitchenData['KitchenhoodReport']['k_timeformat'].'</td>	 
      <td>Customer Authorized Agent:&nbsp;&nbsp;'.$kitchenData['KitchenhoodReport']['authorizedagent'].'</td>	         
	</tr>
	<tr>
	    <td colspan="6">&nbsp;</td>	    
	</tr>
	<tr>
	    <td colspan="6">&nbsp;</td>	    
	</tr>
  <tr>
	    <td colspan="6"><u>Attachments</u>:</td>	    
	</tr>	'; 
  foreach($attachmentdata as $attachdata){
 	$html .='<tr>
	    <td colspan="2">'.$attachdata['KitchenhoodReportAttachment']['attach_file'].'</td>	
      <td>'.date('m/d/Y',strtotime($attachdata['KitchenhoodReportAttachment']['created'])).'</td>	
      <td colspan="2">'.$this->Common->getClientName($attachdata['KitchenhoodReportAttachment']['user_id']).'('.$attachdata['KitchenhoodReportAttachment']['added_by'].')</td>	 
      <td>&nbsp;</td>	         
	</tr>'; }
  	$html .='<tr>
	    <td colspan="6">&nbsp;</td>	    
	</tr>
  <tr>
	    <td colspan="6"><u>Notifications</u>:</td>	    
	</tr>';
  foreach($notifierdata as $notifydata){
  $html .='<tr>
	    <td colspan="2">'.$notifydata['KitchenhoodReportNotification']['notification'].'</td>	
      <td>'.date('m/d/Y',strtotime($notifydata['KitchenhoodReportNotification']['created'])).'</td>	
      <td colspan="2">'.$this->Common->getClientName($notifydata['KitchenhoodReportNotification']['user_id']).'('.$notifydata['KitchenhoodReportNotification']['added_by'].')</td>	 
      <td>&nbsp;</td>	         
	</tr>'; }
  
  ;
	 

	
	 
	 $html .= '</table>';

$tcpdf->Ln();

// set UTF-8 Unicode font
$tcpdf->SetFont('dejavusans', '', 8);
 
// output the HTML content
$tcpdf->writeHTML($html, true, 0, true, true);
//echo $html;die;

// Now you position and print your page content
// example: 
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'B',20);
$tcpdf->Cell(0,20, "", 0,1,'L');


// see the TCPDF examples 
ob_clean();
echo $tcpdf->Output('ReportOnlinePlus.pdf', 'D');die;