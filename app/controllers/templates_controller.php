<?php 
class TemplatesController extends AppController {
  
	var $name = 'Templates';	
	var $layout ="manager";
	var $components = array('Calendar','Email','Session','Cookie','Sms');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Calendar','Common','Text','Timeframe');
  
	function manager_index()
	{
      $this->loadModel('TempData');   
      $this->TempData->bindModel(array(
			'belongsTo' => array(
                    			'Position' => array(
                    			'foreignKey' => false,
                    			'conditions' => array('Position.id=TempData.position_id'),
                    			'fields' => array('Position.designation'),
                    			'order' => 'Position.designation ASC'
							           ),
			                     	 
			   )));
    
    $condition=array("TempData.owner_id"=>$this->Session->read('OwnerId'),"TempData.weekday  >= "=>1,"TempData.weekday <= "=>7);
    $alldata =$this->TempData->find('all', array('conditions' => $condition));
    $this->set('alldata',$alldata);
  }
  
  
  function manager_deletetempschedule($id=null)
  {
  $this->loadModel('TempData'); 
    if(isset($id)){
      if($this->TempData->delete($id)){
         
      $this->redirect(array('controller'=>'templates','action'=>'index'));
      }else {
      $this->redirect(array('controller'=>'templates','action'=>'index'));
      }
    }  
  }
  
  
   function manager_edit_schedule($id=null)
   {
    $this->layout='ajax';
     
    $this->loadModel('TempData'); 
    //pr($this->data); die;
    if(!empty($this->data))
    {    
     
      //$this->data['TempData']['weekday']=$key;
     
      $this->data['TempData']['shift_start_time']=$this->convertTo24Format($this->data['TempData']['shift_start']['hour'],$this->data['TempData']['shift_start']['min'],$this->data['TempData']['shift_start']['meridian']);
      $this->data['TempData']['shift_end_time']=$this->convertTo24Format($this->data['TempData']['shift_end']['hour'],$this->data['TempData']['shift_end']['min'],$this->data['TempData']['shift_end']['meridian']);
      $this->data['TempData']['owner_id']=$this->Session->read('OwnerId');
      $this->TempData->create();
      $this->TempData->save($this->data['TempData']);
      
      
      $this->Session->setFlash('Shifts added in the template successfully'); 
      $this->redirect(array('controller'=>'templates','action'=>'index'));
    }
    $this->data=$this->TempData->find('first',array('conditions'=>array('TempData.id'=>$id)));
    
       $timeStart=$this->data['TempData']['shift_start_time'];
       $timeEnd=$this->data['TempData']['shift_end_time'];
       
       if(isset($timeStart)){
       $shift_start_time=@explode(':',$timeStart);
       $shift_start_time=$this->convertTo12Format($shift_start_time[0],$shift_start_time[1]);
       list($hs,$mms)=@explode(':',$shift_start_time);
       list($mins,$mars)=@explode(' ',$mms);
       $this->data['TempData']['shift_start']['hour']=$hs;
       $this->data['TempData']['shift_start']['min']=$mins;
       $this->data['TempData']['shift_start']['meridian']=$mars;
       }
       if(isset($timeEnd)){
       $shift_end_time=@explode(':',$timeEnd);
       $shift_end_time=$this->convertTo12Format($shift_end_time[0],$shift_end_time[1]);
       list($hss,$mmss)=@explode(':',$shift_end_time);
       list($minss,$marss)=@explode(' ',$mmss);
       $this->data['TempData']['shift_end']['hour']=$hss;
       $this->data['TempData']['shift_end']['min']=$minss;
       $this->data['TempData']['shift_end']['meridian']=$marss;
       }
    
     $this->set('posOption',$this->getPosition());     
     $this->set('daynum',$this->data['TempData']['weekday']);  
    
    //$this->render('manager_build_schedule');
  }
  
  
  
  
  function manager_build_schedule($daynum=null)
  {
    $this->layout='ajax';
    $this->set('posOption',$this->getPosition());     
    $this->set('daynum',$daynum);    
   
    if(!empty($this->data))
    {    
      $this->loadModel('TempData'); 
     foreach($this->data['TempData']['day'] as $key=>$dd)
     {
     if($dd!=0)
     {
     $this->data['TempData']['id']='';
      $this->data['TempData']['weekday']=$key;
     
      $this->data['TempData']['shift_start_time']=$this->convertTo24Format($this->data['TempData']['shift_start']['hour'],$this->data['TempData']['shift_start']['min'],$this->data['TempData']['shift_start']['meridian']);
      $this->data['TempData']['shift_end_time']=$this->convertTo24Format($this->data['TempData']['shift_end']['hour'],$this->data['TempData']['shift_end']['min'],$this->data['TempData']['shift_end']['meridian']);
      $this->data['TempData']['owner_id']=$this->Session->read('OwnerId');
      $this->TempData->create();
      $this->TempData->save($this->data['TempData']);
      }
      }
      $this->Session->setFlash('Shifts added in the template successfully'); 
      $this->redirect(array('controller'=>'templates','action'=>'index'));
    }
    
  }
  
  function manager_mytemplates()
  {
    $this->layout='manager';
    $this->loadModel('ScheduleTemplate');
    $condition=array("ScheduleTemplate.owner_id"=>$this->Session->read('OwnerId'));
    $mytemplates =$this->ScheduleTemplate->find('all', array('conditions' => $condition));
    $this->set('mytemplates',$mytemplates); 
  }
  
  function manager_savetemplates()
  {
    $this->layout='ajax';
    
    /*** CHECK CONDITION ITS FIRST TIME SAVE THE TEMPLATE NAME ******/
    $this->loadModel('TempData');
    $condition=array("TempData.owner_id"=>$this->Session->read('OwnerId'));
    $mytemplates =$this->TempData->find('count', array('conditions' => $condition));
    $this->set('mytemplates',$mytemplates); 
    /***  CONDITION END ITS FIRST TIME SAVE THE TEMPLATE NAME ******/
    
    if(!empty($this->data))
    {        
        $this->loadModel('ScheduleTemplate');
        $this->data['ScheduleTemplate']['owner_id']=$this->Session->read('OwnerId');        
        $this->ScheduleTemplate->set($this->data);
      if($this->ScheduleTemplate->validates())
      {
        if($this->ScheduleTemplate->save($this->data['ScheduleTemplate']))
        {
            /**** NOW TRANSFER DATA FROM temp_data table to schedule_template_data table ****/
            $schedule_template_id=$this->ScheduleTemplate->getLastInsertId();
            $this->loadModel('ScheduleTemplateData');            
            $this->loadModel('TempData');
            $condition=array("TempData.owner_id"=>$this->Session->read('OwnerId'));
            $temp_data =$this->TempData->find('all', array('conditions' => $condition));
            foreach($temp_data as $temp_data):
            $data['schedule_template_id']=$schedule_template_id;
            $data['weekday']=$temp_data['TempData']['weekday'];
          
            $data['shift_start_time']=DATE("H:i",STRTOTIME($temp_data['TempData']['shift_start_time']));
            $data['shift_end_time']=DATE("H:i",STRTOTIME($temp_data['TempData']['shift_end_time']));;
            $data['position_id']=$temp_data['TempData']['position_id'];
            //$data['paid_hours']=$temp_data['TempData']['paid_hours'];
            $data['optional_category']=$temp_data['TempData']['optional_category'];
            $data['comments']=$temp_data['TempData']['comments'];
            $this->ScheduleTemplateData->create();
            $this->ScheduleTemplateData->save($data);            
            endforeach;
            $this->TempData->query('TRUNCATE  TABLE  temp_data');
            /**** END TRANSFER DATA FROM temp_data table to schedule_template_data table ****/
          
          $this->Session->setFlash('Schedule template saved successfully'); 
          $this->redirect(array('controller'=>'templates','action'=>'mytemplates'));
        }
      }
      else
      {
          $errors = $this->ScheduleTemplate->invalidFields();           
          $this->Session->setFlash($errors['name']); 
          $this->redirect(array('controller'=>'templates','action'=>'index'));
      }
        
    }
    
  }
  
  #FUNCTION TO DELETE THE TEMPLATE & TEMPLATE SCHEDULE DATA
  function manager_delete($id=null)
  {
      $this->loadModel('ScheduleTemplate');
      $conditions=array('ScheduleTemplate.id'=>$id,'ScheduleTemplate.owner_id'=>$this->Session->read('OwnerId'));
      $this->ScheduleTemplate->deleteAll($conditions, true, false);
      $this->Session->setFlash('Schedule template deleted successfully'); 
      $this->redirect(array('controller'=>'templates','action'=>'mytemplates'));      
  }
  
  #FUNCTION TO EDIT THE TEMPLATE & TEMPLATE SCHEDULE DATA
  function manager_edit($schedule_templates_id=null)
  {
    $this->loadModel('ScheduleTemplate');  
    $this->ScheduleTemplate->bindModel(array(
			'hasMany' => array(
                    			'ScheduleTemplateData' => array(
                    			'className'     => 'ScheduleTemplateData',
                          'foreignKey'    => 'schedule_template_id',
                          //'conditions'    =>'ScheduleTemplateData.status="Active"',
                          'order'    => 'ScheduleTemplateData.id DESC',
                          'limit'        => '',
                          'dependent'=> true
							           ),
			                     	 
			   )));
    
    $condition=array("ScheduleTemplate.owner_id"=>$this->Session->read('OwnerId'),"ScheduleTemplate.id"=>$schedule_templates_id);
    $alldata =$this->ScheduleTemplate->find('all', array('conditions' => $condition));    
    $this->set('alldata',$alldata);
  }
  
  #FUNCTION TO ADD MORE SCHEDULES IN SAVED TEMPLATES
  function manager_edit_build_schedule($daynum=null,$schedule_templates_id=null)
  {
    $this->layout='ajax';
    $this->set('posOption',$this->getPosition());     
    $this->set('daynum',$daynum);
    $this->set('schedule_templates_id',$schedule_templates_id);    
    
    if(!empty($this->data))
    {
      $this->loadModel('ScheduleTemplateData');
     foreach($this->data['ScheduleTemplateData']['day'] as $key=>$dd)
      {
       if($dd!=0)
       { 
        $this->data['ScheduleTemplateData']['id']='';
        $this->data['ScheduleTemplateData']['weekday']=$key;
        $this->data['ScheduleTemplateData']['shift_start_time']=$this->convertTo24Format($this->data['ScheduleTemplateData']['shift_start']['hour'],$this->data['ScheduleTemplateData']['shift_start']['min'],$this->data['ScheduleTemplateData']['shift_start']['meridian']);
        $this->data['ScheduleTemplateData']['shift_end_time']=$this->convertTo24Format($this->data['ScheduleTemplateData']['shift_end']['hour'],$this->data['ScheduleTemplateData']['shift_end']['min'],$this->data['ScheduleTemplateData']['shift_end']['meridian']);
        $this->data['ScheduleTemplateData']['owner_id']=$this->Session->read('OwnerId');
        $this->ScheduleTemplateData->create();
        $this->ScheduleTemplateData->save($this->data['ScheduleTemplateData']);
        }
     }
      $this->Session->setFlash('Shifts added in the template successfully'); 
      $this->redirect(array('controller'=>'templates','action'=>'edit/'.$this->data['ScheduleTemplateData']['schedule_template_id']));
    }
  }
  
  #FUNCTION TO SAVE THE TEMPLATES
  function manager_edit_savetemplates($schedule_templates_id=null)
  {
      $this->loadModel('ScheduleTemplateData');
      $this->ScheduleTemplateData->updateAll(array('ScheduleTemplateData.status' => "'Active'"),array('ScheduleTemplateData.schedule_template_id' => $schedule_templates_id));
      $this->Session->setFlash('Schedule template Saved successfully'); 
      $this->redirect(array('controller'=>'templates','action'=>'mytemplates'));       
  }
  
  #FUNCTION TO DELETE SCHEDULE FROM SAVED TEMPLATE
  function manager_delete_schedule($schedule_template_data_id=null)
  {
      $this->loadModel('ScheduleTemplateData');
      $ScheduleTemplateData =$this->ScheduleTemplateData->findById($schedule_template_data_id);    
      $this->ScheduleTemplateData->id=$schedule_template_data_id;
      $this->ScheduleTemplateData->delete();
      $this->Session->setFlash('Shifts deleted from the template successfully'); 
      $this->redirect(array('controller'=>'templates','action'=>'edit/'.$ScheduleTemplateData['ScheduleTemplateData']['schedule_template_id']));
  }
}
?>