<?php
class BannersController extends AppController {
	var $name = 'Banners';
	var $components = array('Email','Session','Cookie','Customupload','Uploader.Uploader');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Fck','Time');
        var $paginate = array('limit'=>'10');
  


  function admin_bannerlisting(){
  
	if(isset($_POST['active'])){
	   	 $this->admin_active();
	}
	    
	if(isset($_POST['inactive'])){
	   	 $this->admin_inactive();
	}
	    
	if(isset($_POST['save'])){
	   	 $this->admin_setorder();
	}
	 
	#other pages find     
	$data = $this->Banner->find('all',array('conditions'=>array(''),'order'=>array('Banner.id desc')));
	$this->set("bannerdata", $data);
	
    }
  
  
  function admin_addbanner(){
	if(!empty($this->data) && isset($this->data)){
	
        if(isset($this->data['Banner']['banner_img']['name'])){  
          $this->Uploader->uploadDir = BANNERIMG;
	  $temp_exp = explode('.',$this->data['Banner']['banner_img']['name']);             
          $filenametosave= uniqid('IMG').".".$temp_exp[1];
	  $this->Uploader->_data = $this->data['Banner'];
	  $up = $this->Uploader->upload('banner_img', array('name'=>$filenametosave,'overwrite' => false));
	  if($up){		  
	  $resized_path =$this->Uploader->crop(array('width' => BANNERIMGW, 'height' => BANNERIMGH, 'append' => 'cropped', 'location' => UploaderComponent::LOC_TOP),true);
	  $this->data['Banner']['banner_img'] = $resized_path['name'];
	    if($this->Banner->save($this->data['Banner'])){
		$this->Session->setFlash('Banner Image has been added successfully','success');
                $this->Redirect(array('action' => 'bannerlisting')); 
	      }
	  }
        }
      }
    }

/***************************************************************************************
 #function to edit banner
****************************************************************************************/    
  function admin_editbanner($id=null)
  {
    $this->loadModel('Banner');    
    $this->Banner->id = $id;
            
        if(!empty($this->data)){
       
	    if(isset($this->data['Banner']['banner_img']['name'])){  
	    
	    $this->Uploader->uploadDir = BANNERIMG;
	    $temp_exp = explode('.',$this->data['Banner']['banner_img']['name']);             
	    $filenametosave= uniqid('IMG').".".$temp_exp[1];
	    $this->Uploader->_data = $this->data['Banner'];
	    $up = $this->Uploader->upload('banner_img', array('name'=>$filenametosave,'overwrite' => false));
	    $this->data['Banner']['banner_img'] = $this->data['Banner']['old_banner_img'];
	    if($up){		  
		$resized_path =$this->Uploader->crop(array('width' => BANNERIMGW, 'height' => BANNERIMGH, 'append' => 'cropped', 'location' => UploaderComponent::LOC_TOP),true);
		$this->data['Banner']['banner_img'] = $resized_path['name'];
		if(file_exists(WWW_ROOT .BANNERIMG. $this->data['Banner']['old_banner_img'])) {
		unlink(WWW_ROOT .BANNERIMG. $this->data['Banner']['old_banner_img']);
		}
	      
	    }
	    if($this->Banner->save($this->data['Banner'])){
		  $this->Session->setFlash('Banner has been updated successfully','success');
		  $this->redirect(array("controller" => "banners", "action" => "bannerlisting"));
		}
        }        
        }else{	
          $this->data = $this->Banner->read();
          $data=$this->data['Banner']['banner_img'];
          $this->set('pic',$data);
	}
  }
  
  
  
  function admin_delete($id=null){
       
      $data = $this->Banner->find('first',array('conditions'=>array('Banner.id'=>$id),'recursive'=>1));
    
	
	if($this->Banner->delete($id))
	{
	if(file_exists(WWW_ROOT .BANNERIMG. $data['Banner']['banner_img'])) {
		unlink(WWW_ROOT .BANNERIMG. $data['Banner']['banner_img']);
		}	
	$this->Session->setFlash('Banner has been deleted successfully','success');
	$this->redirect(array("controller" => "banners", "action" => "bannerlisting"));
        }
    
    }
  
  

/***************************************************************************************
 #function to activate
****************************************************************************************/
    function admin_active(){
       
      
      for($i=0; $i<count($_POST['box']); $i++){    		
    		$this->Banner->id = $_POST['box'][$i];    		
		    $data['Banner']['status'] = 1;		    
		    $this->Banner->save($data,array('validate'=>false));        	
    	}
      	
     		$this->Session->setFlash('Banner has been activated successfully','success');	
	   	  $this->redirect(array("controller" => "banners", "action" => "bannerlisting"));
    }
    
/***************************************************************************************
 #function to Inactivate 
****************************************************************************************/ 
    		
    function admin_inactive(){
      
      for($i=0; $i<count($_POST['box']); $i++){    		
    	   $this->Banner->id = $_POST['box'][$i];
		     $data['Banner']['status'] = 0;
		     $this->Banner->save($data,array('validate'=>false));        	
    	}
    	
    		 $this->Session->setFlash('Banner has been inactivated successfully','success');	
	       $this->redirect(array("controller" => "banners", "action" => "bannerlisting"));
    }
    

}