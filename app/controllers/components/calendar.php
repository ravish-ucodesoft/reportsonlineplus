<?php 
class CalendarComponent extends Object {


    function getday($tstamp,$weekday)
     {
      $curentday =date('D',$tstamp);
      
      //for first block Mon
      if($weekday==1){
          if($curentday=='Sun') {
          $tempstamp=$tstamp;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 2*24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 3*24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 4*24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 5*24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 6*24 * 60 * 60;
          }
      }
      //for second block thu
      if($weekday==2){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp -2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 5 * 24 * 60 * 60;
          }
      }
      //for second block Wed
       if($weekday==3){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 4 * 24 * 60 * 60;
          }
      }
      
      //for second block Thu
       if($weekday==4){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 3 * 24 * 60 * 60;
          }
      }
      //for second block Fri
       if($weekday==5){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 24 * 60 * 60;;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp ;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp -  24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 2 * 24 * 60 * 60;
          }
      }
        //for second block Sat
       if($weekday==6){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          }
      }
      
      //for second block Sat
       if($weekday==7){
          if($curentday=='Sun') {
          $tempstamp=$tstamp + 6 * 24 * 60 * 60;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp + 5 * 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp + 4 * 24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp + 3 * 24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp + 2 * 24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp + 24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp;
          }
      }
       $rdate=date('Y-m-d',$tempstamp);
      return $rdate; 
     }
     
     function calmenu($tstamp) {
     
     App::import('Helper', 'Html');
     $this->html=new HtmlHelper();
     $weekdiff=7 * 24 * 60 * 60;
     $tstamp=$this->getweekfirstday($tstamp);
     
     $tspre1=$tstamp - $weekdiff;
     $tspre2=$tstamp - 2 * $weekdiff;
     $tspre3=$tstamp - 3 * $weekdiff;
     $tspre4=$tstamp - 4 * $weekdiff;
     $tspre5=$tstamp - 5 * $weekdiff;
     $tspre6=$tstamp - 6 * $weekdiff;
    
     $tsnext1=$tstamp + $weekdiff;
     $tsnext2=$tstamp + 2 * $weekdiff;
     $tsnext3=$tstamp + 3 * $weekdiff;
     $tsnext4=$tstamp + 4 * $weekdiff;
     $tsnext5=$tstamp + 5 * $weekdiff;
     $tsnext6=$tstamp + 6 * $weekdiff;
     
     $table='<table border="0" width="100%" style="border-collapse: collapse; margin-bottom: 2px;">
              <tbody>
              <tr onmouseout="this.className=\'normal\'" onmouseover="this.className=\'highlight\'" class="navrow">
                   <td onclick="redirecturl(\''.$tspre6.'\')">'.date('M-d',$tstamp - 6*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre5.'\')">'.date('M-d',$tstamp - 5*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre4.'\')">'.date('M-d',$tstamp - 4*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre3.'\')">'.date('M-d',$tstamp - 3*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre2.'\')">'.date('M-d',$tstamp - 2*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tspre1.'\')">'.date('M-d',$tstamp - $weekdiff).'</td>
                   <td id="calbtn" ><p align="center" id="inputField">Week of '.date('M-d, Y',$tstamp). $this->html->image('/img/calendar.gif').'</p></td>
                   <td onclick="redirecturl(\''.$tsnext1.'\')">'.date('M-d',$tstamp + $weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext2.'\')">'.date('M-d',$tstamp + 2*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext3.'\')">'.date('M-d',$tstamp + 3*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext4.'\')">'.date('M-d',$tstamp + 4*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext5.'\')">'.date('M-d',$tstamp + 5*$weekdiff).'</td>
                   <td onclick="redirecturl(\''.$tsnext6.'\')">'.date('M-d',$tstamp + 6*$weekdiff).'</td>
              </tr></tbody>
              </table>';   
              return $table;
     }
     
    
     
     private function getweekfirstday($tstamp)
     {
     $curentday =date('D',$tstamp);
     
       if($curentday=='Sun') {
          $tempstamp=$tstamp;
          } else if($curentday=='Mon') {
          $tempstamp=$tstamp - 24 * 60 * 60;
          } else if($curentday=='Tue') {
          $tempstamp=$tstamp - 2*24 * 60 * 60;
          } else if($curentday=='Wed') {
          $tempstamp=$tstamp - 3*24 * 60 * 60;
          } else if($curentday=='Thu') {
          $tempstamp=$tstamp - 4*24 * 60 * 60;
          } else if($curentday=='Fri') {
          $tempstamp=$tstamp - 5*24 * 60 * 60;
          } else if($curentday=='Sat') {
          $tempstamp=$tstamp - 6*24 * 60 * 60;
          }
          return $tempstamp;
          
     }
     
     function dateToTimeStamp($date)
     {        
        list($y,$m,$d)=explode('-',$date);     
        return mktime(0, 0, 0, $m, $d, $y);
     }
     
     function addDate($date,$time)
     {
          $timestamp=$this->dateToTimeStamp($date);
          return date('Y-m-d',$timestamp+$time);
     }

}
?>