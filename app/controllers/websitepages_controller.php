<?php
class WebsitepagesController extends AppController {
	var $name = 'Websitepages';
	var $components = array('Email','Session','Cookie');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Fck','Time');
  var $paginate = array('limit'=>'20');
  
/*
*************************************************************************
*Function Name		 :	admin_listpages
*Functionality		 :	to  list content pages like contactus, termsconditions
*************************************************************************
*/

  function admin_pagelisting(){
  
	if(isset($_POST['active'])){
	   	 $this->admin_active();
	}
	    
	if(isset($_POST['inactive'])){
	   	 $this->admin_inactive();
	}
	    
	if(isset($_POST['save'])){
  
	   	 $this->admin_setorder();
	}
	     
	//$data = $this->Websitepage->find('all',array('order'=>array('Websitepage.id desc')));
	//$this->set("websitePageName", $data);
	$this->set('websitePageName',$this->paginate('Websitepage'));
  
  } 
/*
*************************************************************************
*Function Name		 :	admin_addpage
*Functionality		 :	to  add new content pages like contactus, termsconditions
*************************************************************************
*/

  function admin_addpage(){
	if (!empty($this->data)){
		if($this->Websitepage->set($this->data) && $this->Websitepage->validates()) { 
			if ($this->Websitepage->save($this->data)){
			      $this->Session->setFlash('Page has been added successfully','success');
			      $this->redirect(array("controller" => "websitepages", "action" => "pagelisting"));
			  }
		}	 
    	}    
  }
/*
*************************************************************************
*Function Name		 :	admin_editpage
*Functionality		 :	to  edit content pages like contactus, termsconditions
*************************************************************************
*/

  function admin_editpage($id = null){
	$this->Websitepage->id=$id;
    	if(empty($this->data)){ 
    		$this->Websitepage->id = $id;
    		$this->data=$this->Websitepage->read();	
    	}else{
    	
    	  if($this->Websitepage->save($this->data['Websitepage'])){ 
    		  echo $this->Session->setFlash('Page has been updated successfully','success');    			 
		  $this->redirect(array("controller" => "websitepages", "action" => "pagelisting"));
    		} 
          
    	 }    
  }

/***************************************************************************************
 #function to activate website pages		
****************************************************************************************/
    function admin_active(){
       
      
      for($i=0; $i<count($_POST['box']); $i++){    		
    		$this->Websitepage->id = $_POST['box'][$i];    		
		    $data['Websitepage']['status'] = 1;		    
		    $this->Websitepage->save($data,array('validate'=>false));        	
    	}
      	
     		$this->Session->setFlash('Page has been activated successfully','success');	
	   	  $this->redirect(array("controller" => "websitepages", "action" => "pagelisting"));
    }
    
/***************************************************************************************
 #function to Inactivate website pages
****************************************************************************************/ 
    		
    function admin_inactive(){
      
      for($i=0; $i<count($_POST['box']); $i++){    		
    	   $this->Websitepage->id = $_POST['box'][$i];
		     $data['Websitepage']['status'] = 0;
		     $this->Websitepage->save($data,array('validate'=>false));        	
    	}
    	
    		 $this->Session->setFlash('Page has been inactivated successfully','success');	
	       $this->redirect(array("controller" => "websitepages", "action" => "pagelisting"));
    }
    
/***************************************************************************************
 #function to set order of website pages	
****************************************************************************************/ 
    	
    function admin_setorder(){
    		
      for($i=0; $i<count($_POST['orderingids']); $i++){    		
    		$this->Websitepage->id = $_POST['orderingids'][$i];
		    $data['Websitepage']['ordering'] = $_POST['orderbox'][$i];
		    $this->Websitepage->save($data,array('validate'=>false));        	
    	}
    	
	      $this->Session->setFlash('Order has been saved successfully','success');	
	      $this->redirect(array("controller" => "websitepages", "action" => "pagelisting"));
    }
    
/***************************************************************************************
 #function to mange home page tabs data
****************************************************************************************/ 
    	
    function admin_manageMainTabs(){
	$this->loadModel('MainTab');	
    	if(empty($this->data)){ 
    		$this->MainTab->id = 1;
    		$this->data=$this->MainTab->read();	
    	}else{
    	
    	  if($this->MainTab->save($this->data['MainTab'])){ 
    		  echo $this->Session->setFlash('Tabs description updated successfully','success');    			 
		  $this->redirect(array("controller" => "websitepages", "action" => "pagelisting"));
    		} 
          
    	 } 
	
    }
}