<?php
/*
Purpose: CronsController  class handles all user functionalities
D.O.M  :01-08-2012
*/
class CronsController extends AppController {
  var $uses = array('ServiceCall','Schedule','User','Report','EmailTemplate');
  var $components = array('Calendar','Email','Session','Cookie','Sms');
  var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Calendar','Common','Text','Timeframe','Time');
  var $layout = '';
  var $name = 'Crons'; 

  function cronjobtest() 
{
    
    //echo $_SERVER['DOCUMENT_ROOT'];
    //Check the action is being invoked by the cron dispatcher
    if (!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }
   
    /**** Test for cron ****/
    $subjecttest = 'Cron Reportsonlineplus';
    // message
    $messagetest = 'Reportsonlineplus Reportsonlineplus';
    
    // To send HTML mail, the Content-type header must be set
    $headerstest  = 'MIME-Version: 1.0' . "\r\n";
    $headerstest .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    // Additional headers    
    $headerstest .= 'From: Reportsonlineplus.com <info@reportsonlineplus.com>' . "\r\n";
    // Mail it
    mail('testing.sdei@gmail.com', $subjecttest, $messagetest, $headerstest);
  }

  #Yearly Case
  function cronAutoPopulateServiceCallYearly(){
  echo $today_date=date('Y-m-d');
  //$today_date=date('Y-m-d',strtotime('+1 year',strtotime($today_date)));
  //Check the action is being invoked by the cron dispatcher
  if(!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }  
  #FIND ALL SERVICE CALLS WITH POPULATE FREQUENCY IS NOT 'No'  
  $servicecall = $this->ServiceCall->find('all',array('conditions'=>array('ServiceCall.service_call_frequency'=>'Y','ServiceCall.created_type'=>'manually'),'recursive'=>2));
  #check to see if the parent_id exist in the table or not
  foreach($servicecall as $serviceData):
  
  $servicecallchild = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.parent_id'=>$serviceData['ServiceCall']['id']),'order'=>'ServiceCall.id DESC','recursive'=>2));
  if(!empty($servicecallchild))
  {
    #Means we need to set the new service call based on last service call created by cron job
    $record_created=date('Y-m-d',strtotime('+1 year',strtotime($servicecallchild['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($servicecallchild['ServiceCall']['created']));
    #Check to see if today date is equal to the created date + 1 year
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first
          
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$servicecallchild['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$servicecallchild['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$servicecallchild['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$servicecallchild['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$servicecallchild['ServiceCall']['parent_id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
       
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($servicecallchild['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL)
          {
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL)
          {
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL)
          {
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL)
          {
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL)
          {
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL)
          {
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL)
          {
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL)
          {
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL)
          {
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL)
          {
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_10'])));
          }
        
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($servicecallchild['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;
          
          endforeach;   //Schedule for each END     
    }      
    else{
       echo 'No Date Match';
    }
  }
  else{    
    #Means we need to set the new service call based on manual frequency
    $record_created=date('Y-m-d',strtotime('+1 year',strtotime($serviceData['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($serviceData['ServiceCall']['created']));
    #Check to see if today date is equal to the created date + 1 year
    if($record_created==$today_date){
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$serviceData['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$serviceData['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$serviceData['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$serviceData['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$serviceData['ServiceCall']['id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($serviceData['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL){
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL){
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL){
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL){
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL){
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL){
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL){
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL){
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL){
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL){
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+1 year',strtotime($schedule['schedule_date_10'])));
          }
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($serviceData['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;
          endforeach;      //Schedule for each END  
    }      
    else{
       echo 'No Date Match';
    }
  }
  endforeach; 
  }
  //*******************HalfYearly Case****************************
  //*******************HalfYearly Case****************************
  
  #HalfYearly Case
  function cronAutoPopulateServiceCallHalfYearly()
  {
     echo $today_date=date('Y-m-d');
    //$today_date=date('Y-m-d',strtotime('+6 month',strtotime($today_date)));
    //Check the action is being invoked by the cron dispatcher
    if(!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }  
    #FIND ALL SERVICE CALLS WITH POPULATE FREQUENCY IS NOT 'No'  
    $servicecall = $this->ServiceCall->find('all',array('conditions'=>array('ServiceCall.service_call_frequency'=>'H','ServiceCall.created_type'=>'manually'),'recursive'=>2));
    #check to see if the parent_id exist in the table or not
  foreach($servicecall as $serviceData):
  $servicecallchild = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.parent_id'=>$serviceData['ServiceCall']['id']),'order'=>'ServiceCall.id DESC','recursive'=>2));
  if(!empty($servicecallchild))
  {
    #Means we need to set the new service call based on last service call created by cron job
    $record_created=date('Y-m-d',strtotime('+6 month',strtotime($servicecallchild['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($servicecallchild['ServiceCall']['created']));
    #Check to see if today date is equal to the created date +6 month
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$servicecallchild['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$servicecallchild['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$servicecallchild['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$servicecallchild['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$servicecallchild['ServiceCall']['parent_id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
       
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($servicecallchild['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL){
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL){
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL){
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL){
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL){
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL){
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL){
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL){
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL){
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL){
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_10'])));
          }
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($servicecallchild['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;
          endforeach;   //Schedule for each END     
    }      
    else{
       echo 'No Date Match';
    }
  }
  else
  {    
    #Means we need to set the new service call based on manual frequency
    $record_created=date('Y-m-d',strtotime('+6 month',strtotime($serviceData['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($serviceData['ServiceCall']['created']));
    #Check to see if today date is equal to the created date +6 month
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first        
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$serviceData['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$serviceData['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$serviceData['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$serviceData['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$serviceData['ServiceCall']['id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($serviceData['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL){
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL){
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL){
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL){
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL){
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL){
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL){
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL){
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL){
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL){
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+6 month',strtotime($schedule['schedule_date_10'])));
          }
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($serviceData['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;
          
          endforeach;      //Schedule for each END  
    }      
    else{
       echo 'No Date Match';
    }
  }
  endforeach; 
  }
  
  //*******************Quarterly Case****************************
  //*******************Quarterly Case****************************
  
  #Quarterly Case
  function cronAutoPopulateServiceCallQuarterly() 
  {
  echo $today_date=date('Y-m-d');
  //$today_date=date('Y-m-d',strtotime('+3 month',strtotime($today_date)));
  //Check the action is being invoked by the cron dispatcher
  if(!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }  
  #FIND ALL SERVICE CALLS WITH POPULATE FREQUENCY IS NOT 'No'  
  $servicecall = $this->ServiceCall->find('all',array('conditions'=>array('ServiceCall.service_call_frequency'=>'Q','ServiceCall.created_type'=>'manually'),'recursive'=>2));
  #check to see if the parent_id exist in the table or not
  foreach($servicecall as $serviceData):
  $servicecallchild = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.parent_id'=>$serviceData['ServiceCall']['id']),'order'=>'ServiceCall.id DESC','recursive'=>2));
  if(!empty($servicecallchild))
  {
    #Means we need to set the new service call based on last service call created by cron job
    $record_created=date('Y-m-d',strtotime('+3 month',strtotime($servicecallchild['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($servicecallchild['ServiceCall']['created']));
    #Check to see if today date is equal to the created date +3 month
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first
          
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$servicecallchild['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$servicecallchild['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$servicecallchild['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$servicecallchild['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$servicecallchild['ServiceCall']['parent_id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
       
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($servicecallchild['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL)
          {
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL)
          {
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL)
          {
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL)
          {
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL)
          {
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL)
          {
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL)
          {
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL)
          {
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL)
          {
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL)
          {
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_10'])));
          }
          
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($servicecallchild['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;
          endforeach;   //Schedule for each END     
    }      
    else{
       echo 'No Date Match';
    }
  }
  else
  {    
    #Means we need to set the new service call based on manual frequency
    $record_created=date('Y-m-d',strtotime('+3 month',strtotime($serviceData['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($serviceData['ServiceCall']['created']));
    #Check to see if today date is equal to the created date +3 month
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first     
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$serviceData['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$serviceData['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$serviceData['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$serviceData['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$serviceData['ServiceCall']['id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
       
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($serviceData['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL)
          {
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL)
          {
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL)
          {
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL)
          {
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL)
          {
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL)
          {
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL)
          {
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL)
          {
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL)
          {
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL)
          {
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+3 month',strtotime($schedule['schedule_date_10'])));
          } 
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($serviceData['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;  
          endforeach;      //Schedule for each END  
    }      
    else{
       echo 'No Date Match';
    } 
  }
  endforeach;   
  }
  
   /********Monthly Case************/
  function cronAutoPopulateServiceCallMonthly() 
  {
  echo $today_date=date('Y-m-d');
  //$today_date=date('Y-m-d',strtotime('+1 month',strtotime($today_date)));
  //Check the action is being invoked by the cron dispatcher
  if(!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }  
  #FIND ALL SERVICE CALLS WITH POPULATE FREQUENCY IS NOT 'No'  
  $servicecall = $this->ServiceCall->find('all',array('conditions'=>array('ServiceCall.service_call_frequency'=>'M','ServiceCall.created_type'=>'manually'),'recursive'=>2));
  #check to see if the parent_id exist in the table or not
  foreach($servicecall as $serviceData):
  
  $servicecallchild = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.parent_id'=>$serviceData['ServiceCall']['id']),'order'=>'ServiceCall.id DESC','recursive'=>2));
  if(!empty($servicecallchild))
  {
    #Means we need to set the new service call based on last service call created by cron job
    $record_created=date('Y-m-d',strtotime('+1 month',strtotime($servicecallchild['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($servicecallchild['ServiceCall']['created']));
    #Check to see if today date is equal to the created date + 1 month
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first
          
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$servicecallchild['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$servicecallchild['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$servicecallchild['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$servicecallchild['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$servicecallchild['ServiceCall']['parent_id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
       
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($servicecallchild['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL)
          {
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL)
          {
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL)
          {
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL)
          {
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL)
          {
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL)
          {
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL)
          {
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL)
          {
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL)
          {
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL)
          {
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_10'])));
          }
          
          
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($servicecallchild['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;  
          endforeach;   //Schedule for each END     
    }      
    else{
       echo 'No Date Match';
    }
  }
  else{    
    #Means we need to set the new service call based on manual frequency
    $record_created=date('Y-m-d',strtotime('+1 month',strtotime($serviceData['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($serviceData['ServiceCall']['created']));
    #Check to see if today date is equal to the created date + 1 month
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first
          
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$serviceData['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$serviceData['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$serviceData['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$serviceData['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$serviceData['ServiceCall']['id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
       
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($serviceData['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL)
          {
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL)
          {
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL)
          {
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL)
          {
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL)
          {
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL)
          {
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL)
          {
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL)
          {
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL)
          {
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL)
          {
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+1 month',strtotime($schedule['schedule_date_10'])));
          }
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($serviceData['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;  
          endforeach;      //Schedule for each END  
    }      
    else{
       echo 'No Date Match';
    } 
  }  
    endforeach;
  }
  
     /********Weekly Case************/
  function cronAutoPopulateServiceCallWeekly(){
  echo $today_date=date('Y-m-d');
  //$today_date=date('Y-m-d',strtotime('Week',strtotime($today_date)));
  //Check the action is being invoked by the cron dispatcher
  if(!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }  
  #FIND ALL SERVICE CALLS WITH POPULATE FREQUENCY IS NOT 'No'  
  $servicecall = $this->ServiceCall->find('all',array('conditions'=>array('ServiceCall.service_call_frequency'=>'W','ServiceCall.created_type'=>'manually'),'recursive'=>2));
  #check to see if the parent_id exist in the table or not
  foreach($servicecall as $serviceData):
  
  $servicecallchild = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.parent_id'=>$serviceData['ServiceCall']['id']),'order'=>'ServiceCall.id DESC','recursive'=>2));
  if(!empty($servicecallchild))
  {
    #Means we need to set the new service call based on last service call created by cron job
    $record_created=date('Y-m-d',strtotime('+7 DAY',strtotime($servicecallchild['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($servicecallchild['ServiceCall']['created']));
    #Check to see if today date is equal to the created date 7 DAY
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first
          
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$servicecallchild['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$servicecallchild['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$servicecallchild['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$servicecallchild['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$servicecallchild['ServiceCall']['parent_id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
       
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($servicecallchild['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL)
          {
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL)
          {
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL)
          {
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL)
          {
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL)
          {
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL)
          {
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL)
          {
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL)
          {
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL)
          {
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL)
          {
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_10'])));
          }
          
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($servicecallchild['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;  
          endforeach;   //Schedule for each END     
    }      
    else{
       echo 'No Date Match';
    }
  }
  else{    
    #Means we need to set the new service call based on manual frequency
    $record_created=date('Y-m-d',strtotime('+7 DAY',strtotime($serviceData['ServiceCall']['created'])));
    //$record_created=date('Y-m-d',strtotime($serviceData['ServiceCall']['created']));
    #Check to see if today date is equal to the created date + 7 DAY
    if($record_created==$today_date)
    {
          #Yes Date match i.e we need to fire the cron and system will add the new record automatically in servicecalls table,schedules table,schedule_services table 
          echo 'Yes Date Match';
          #save the record in the service_calls table first
          $this->ServiceCall->create();
          $data['ServiceCall']['client_id']=$serviceData['ServiceCall']['client_id'];
          $data['ServiceCall']['sp_id']=$serviceData['ServiceCall']['sp_id'];
          $data['ServiceCall']['service_call_frequency']=$serviceData['ServiceCall']['service_call_frequency'];
          $data['ServiceCall']['created_type']='cron';
          $data['ServiceCall']['reminder_day']=$serviceData['ServiceCall']['reminder_day'];
          $data['ServiceCall']['parent_id']=$serviceData['ServiceCall']['id'];
          $this->ServiceCall->save($data['ServiceCall'],array('validate'=>false));
       
          $serviceCallID = $this->ServiceCall->getLastInsertId();
          #SAVE the record in schedule table also
          foreach($serviceData['Schedule'] as $schedule):
          $this->Schedule->create();
          $sch['Schedule']['service_call_id']=$serviceCallID;
          $sch['Schedule']['report_id']=$schedule['report_id'];
          $sch['Schedule']['lead_inspector_id']=$schedule['lead_inspector_id'];
          $sch['Schedule']['helper_inspector_id']=$schedule['helper_inspector_id'];
          $sch['Schedule']['schedule_from_1']=$schedule['schedule_from_1'];
          $sch['Schedule']['schedule_to_1']=$schedule['schedule_to_1'];
          $sch['Schedule']['schedule_from_2']=$schedule['schedule_from_2'];
          $sch['Schedule']['schedule_to_2']=$schedule['schedule_to_2'];
          $sch['Schedule']['schedule_from_3']=$schedule['schedule_from_3'];
          $sch['Schedule']['schedule_to_3']=$schedule['schedule_to_3'];
          $sch['Schedule']['schedule_from_4']=$schedule['schedule_from_4'];
          $sch['Schedule']['schedule_to_4']=$schedule['schedule_to_4'];
          $sch['Schedule']['schedule_from_5']=$schedule['schedule_from_5'];
          $sch['Schedule']['schedule_to_5']=$schedule['schedule_to_5'];
          $sch['Schedule']['schedule_from_6']=$schedule['schedule_from_6'];
          $sch['Schedule']['schedule_to_6']=$schedule['schedule_to_6'];
          $sch['Schedule']['schedule_from_7']=$schedule['schedule_from_7'];
          $sch['Schedule']['schedule_to_7']=$schedule['schedule_to_7'];
          $sch['Schedule']['schedule_from_8']=$schedule['schedule_from_8'];
          $sch['Schedule']['schedule_to_8']=$schedule['schedule_to_8'];
          $sch['Schedule']['schedule_from_9']=$schedule['schedule_from_9'];
          $sch['Schedule']['schedule_to_9']=$schedule['schedule_to_9'];
          $sch['Schedule']['schedule_from_10']=$schedule['schedule_from_10'];
          $sch['Schedule']['schedule_to_10']=$schedule['schedule_to_10'];
          if($schedule['schedule_date_1']!=NULL)
          {
            $sch['Schedule']['schedule_date_1']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_1'])));
          }
          if($schedule['schedule_date_2']!=NULL)
          {
            $sch['Schedule']['schedule_date_2']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_2'])));
          }
          if($schedule['schedule_date_3']!=NULL)
          {
            $sch['Schedule']['schedule_date_3']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_3'])));
          }
          if($schedule['schedule_date_4']!=NULL)
          {
            $sch['Schedule']['schedule_date_4']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_4'])));
          }
          if($schedule['schedule_date_5']!=NULL)
          {
            $sch['Schedule']['schedule_date_5']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_5'])));
          }
          if($schedule['schedule_date_6']!=NULL)
          {
            $sch['Schedule']['schedule_date_6']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_6'])));
          }
          if($schedule['schedule_date_7']!=NULL)
          {
            $sch['Schedule']['schedule_date_7']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_7'])));
          }
          if($schedule['schedule_date_8']!=NULL)
          {
            $sch['Schedule']['schedule_date_8']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_8'])));
          }
          if($schedule['schedule_date_9']!=NULL)
          {
            $sch['Schedule']['schedule_date_9']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_9'])));
          }
          if($schedule['schedule_date_10']!=NULL)
          {
            $sch['Schedule']['schedule_date_10']=date('Y-m-d',strtotime('+7 DAY',strtotime($schedule['schedule_date_10'])));
          }
          $this->Schedule->save($sch['Schedule'],array('validate'=>false));
          $ScheduleID = $this->Schedule->getLastInsertId();  
          foreach($serviceData['Schedule']['ScheduleService'] as $scheduleservices):
             $this->ScheduleService->create();
             $schservice['ScheduleService']['schedule_id']=$ScheduleID;
             $schservice['ScheduleService']['service_id']=$scheduleservices['service_id'];
             $schservice['ScheduleService']['frequency]']=$scheduleservices['frequency]'];
             $schservice['ScheduleService']['amount]']=$scheduleservices['amount]'];
             $this->Schedule->save($schservice['ScheduleService'],array('validate'=>false));
          endforeach;  
          endforeach;      //Schedule for each END  
    }      
    else{
       echo 'No Date Match';
    } 
  }  
    endforeach;
  }
  
  //Notification mail For all cases
  function cronMailAutoPopulateServiceCall(){
  echo $today_date=date('Y-m-d');
  
  //$today_date=date('Y-m-d',strtotime('+1 year',strtotime($today_date)));
  //Check the action is being invoked by the cron dispatcher
  if(!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }  
  #FIND ALL SERVICE CALLS WITH POPULATE FREQUENCY IS NOT 'No'  
  $servicecall = $this->ServiceCall->find('all',array('conditions'=>array('ServiceCall.created_type'=>'manually'),'recursive'=>2));
  #check to see if the parent_id exist in the table or not
  foreach($servicecall as $serviceData):
  $servicecallchild = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.parent_id'=>$serviceData['ServiceCall']['id']),'order'=>'ServiceCall.id DESC','recursive'=>2));
    if(!empty($servicecallchild)){
      #Means we need to set the new service call based on last service call created by cron job
     if($servicecallchild['ServiceCall']['service_call_frequency'] == "Y"){
      $record_created=date('Y-m-d',strtotime('+1 year',strtotime($servicecallchild['ServiceCall']['created'])));
      //$mailDate = =date('Y-m-d',strtotime($reminder_days,strtotime($record_created)));
      
    }else if($servicecallchild['ServiceCall']['service_call_frequency'] == "H"){
      $record_created=date('Y-m-d',strtotime('+6 month',strtotime($servicecallchild['ServiceCall']['created'])));
    }else if($servicecallchild['ServiceCall']['service_call_frequency'] == "Q"){
      $record_created=date('Y-m-d',strtotime('+3 month',strtotime($servicecallchild['ServiceCall']['created'])));
    }
    else if($servicecallchild['ServiceCall']['service_call_frequency'] == "M"){
      $record_created=date('Y-m-d',strtotime('+1 month',strtotime($servicecallchild['ServiceCall']['created'])));
    }
    else if($servicecallchild['ServiceCall']['service_call_frequency'] == "W"){
      $record_created=date('Y-m-d',strtotime('+7 Day',strtotime($servicecallchild['ServiceCall']['created'])));
    }
   
      $reminder_days = $serviceData['ServiceCall']['reminder_day'];
      $mail_date = strtotime($record_created.'-'.$reminder_days.' days');
      $mailDate = date('Y-m-d',$mail_date);
      #Check to see if today date is equal to the new find date
    if($mailDate==$today_date){
      //mail code
      $result = $this->User->find('first',array('conditions'=>array('User.id'=>$serviceData['ServiceCall']['client_id']),'fields'=>array('User.email','User.fname','User.lname')));
      $link = "<a href=" . $this->selfURL() . "/homes/clientlogin>Click here</a>"; 
      $this->loadmodel('EmailTemplate');
      $autopopulate_service_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>7)));
      $autopopulate_service_temp['EmailTemplate']['mail_body']=str_replace(array('#FIRSTNAME','#LINK'),array($result['User']['fname']." ".$result['User']['lname'],$link),$autopopulate_service_temp['EmailTemplate']['mail_body']);
      $this->set('autopopulate_service_temp',$autopopulate_service_temp['EmailTemplate']['mail_body']);
      $this->Email->to = $result['User']['email'];
      $this->Email->subject = $autopopulate_service_temp['EmailTemplate']['mail_subject'];
      $this->Email->from = EMAIL_FROM;
      $this->Email->template = 'autopopulateservice_mail'; // note no '.ctp'
      $this->Email->sendAs = 'html'; // because we like to send pretty mail		
      $this->Email->send(); //Do not pass any args to send()    
    } 
    else{
       echo 'No Date Match';
    }
  }
  else{
   //pr($serviceData);
    #Means we need to set the new service call based on last service call created by cron job
    if($serviceData['ServiceCall']['service_call_frequency'] == "Y"){
      $record_created=date('Y-m-d',strtotime('+1 year',strtotime($serviceData['ServiceCall']['created'])));
    }else if($serviceData['ServiceCall']['service_call_frequency'] == "H"){
      $record_created=date('Y-m-d',strtotime('+6 month',strtotime($serviceData['ServiceCall']['created'])));
    }else if($serviceData['ServiceCall']['service_call_frequency'] == "Q"){
      $record_created=date('Y-m-d',strtotime('+3 month',strtotime($serviceData['ServiceCall']['created'])));
    }else if($serviceData['ServiceCall']['service_call_frequency'] == "M"){
      $record_created=date('Y-m-d',strtotime('+1 month',strtotime($serviceData['ServiceCall']['created'])));
    }else if($serviceData['ServiceCall']['service_call_frequency'] == "W"){
      $record_created=date('Y-m-d',strtotime('+7 Day',strtotime($serviceData['ServiceCall']['created'])));
    }
      $reminder_days = $serviceData['ServiceCall']['reminder_day'];
      $mail_date = strtotime($record_created.'-'.$reminder_days.' days');
      $mailDate = date('Y-m-d',$mail_date);
      #Check to see if today date is equal to the new find date
    if($mailDate==$today_date){
      //mail code
      $result = $this->User->find('first',array('conditions'=>array('User.id'=>$serviceData['ServiceCall']['client_id']),'fields'=>array('User.email','User.fname','User.lname')));
      $link = "<a href=" . $this->selfURL() . "/homes/clientlogin>Click here</a>"; 
      $this->loadmodel('EmailTemplate');
      $autopopulate_service_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>7)));
      $autopopulate_service_temp['EmailTemplate']['mail_body']=str_replace(array('#FIRSTNAME','#LINK'),array($result['User']['fname']." ".$result['User']['lname'],$link),$autopopulate_service_temp['EmailTemplate']['mail_body']);
      $this->set('autopopulate_service_temp',$autopopulate_service_temp['EmailTemplate']['mail_body']);
      $this->Email->to = $result['User']['email'];
      $this->Email->subject = $autopopulate_service_temp['EmailTemplate']['mail_subject'];
      $this->Email->from = EMAIL_FROM;
      $this->Email->template = 'autopopulateservice_mail'; // note no '.ctp'
      $this->Email->sendAs = 'html'; // because we like to send pretty mail		
      $this->Email->send(); //Do not pass any args to send()  
    }
  }
  endforeach;  
  }
  
//function: autoReminderEmail
//Desc: To send the auto reminder email to clients based on set reminder
//Created By: Manish Kumar On dec 26, 2012
  function autoReminderEmail(){
    if (!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); }
    $this->loadModel('ServiceCall');
    $serviceDatas = $this->ServiceCall->find('all',array('conditions'=>array('ServiceCall.call_status'=>0,'ServiceCall.reminder_days >'=>0)));
    if(count($serviceDatas)>0){
      foreach($serviceDatas as $datas){
        $reminderDate = $datas['ServiceCall']['reminder_date'];
        $daysInterval = $datas['ServiceCall']['reminder_days'];
        $callID = $datas['ServiceCall']['id'];
        $currdate = date('Y-m-d');
        $diff = abs(strtotime($currdate) - strtotime($reminderDate));
        $days = floor(($diff)/ (60*60*24));
        if($days==$daysInterval){
          /*Send Email code starts here*/
           //App::import('Controller', 'Schedules');
           //$Schedule = new SchedulesController();
           //$Schedule->acceptanceMailCode($callID);
           
           $serviceCallId = $callID;
           $this->loadModel('Schedule');
          $this->loadModel('ServiceCall');
          $this->loadModel('User');
          $this->loadModel('EmailTemplate');
          $this->loadModel('ClientSchedule');
          
          $res=$this->ServiceCall->find("first",array('conditions'=>array('ServiceCall.id'=>$serviceCallId),'recursive'=>1));
          
          $sp = $this->User->find('first',array('conditions'=>array('User.id'=>$res['ServiceCall']['sp_id']),'fields'=>array('fname','lname','phone','email'),'recursive'=>2));
          $spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
          $spschedularphone = $sp['User']['phone']; //sp-schedulerphone
          $spcompany=$sp['Company']['name'];
          
          $client = $this->User->find('first',array('conditions'=>array('User.id'=>$res['ServiceCall']['client_id']),'fields'=>array('fname','lname','site_name','email','client_company_name'),'recursive'=>2));
          
          $client_name = $client['User']['fname']." ".$client['User']['lname']." (".$client['User']['client_company_name'].")";
          $client_email = $client['User']['email'];
                  
                  
                  $this->Schedule->bindModel(
                                          array('belongsTo' => array(
                                          'Report' => array(
                                          'className' => 'Report',
                                          'fields'=>array('Report.name')
                                                  )
                                              )
                                           )
                                          );
                      
                          $Schdata=$this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$serviceCallId)));
                  
                          $email_string ='';
                          $email_string .= '<table align="center" width="400" height="88" cellspacing="1" cellpadding="1" border="1">
                                                  <tbody>
                                                  <tr style="background-color:#0B83A6">
                                                      <td><b>ReportName</b></td>
                                                      <td><b>Days</b></td>
                                                      <td><b>Schedule Date/Time</b></td>
                                                      <td><b>Inspector Assigned</b></td>
                                                  </tr>';
                          $reportdata='';
                         foreach($Schdata as $key=>$sch){
                                          $leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['Schedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
                                 $reportdata   .=$sch['Report']['name'].',';
                                 $email_string .= "<tr>";
                                 $days=0;
                                 $email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';       
                                 $sch1= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_1'])); $days=$days+1;
                                  $sch2="";$sch3="";$sch4="";$sch5="";$sch6="";$sch7="";$sch8="";$sch9="";$sch10="";
                                  if(!empty($sch['Schedule']['schedule_date_2'])) { 
                                          $sch2= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_2'])); $days=$days+1;
                                  }
                                  if(!empty($sch['Schedule']['schedule_date_3'])) { 
                                          $sch3= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_3']));$days=$days+1;
                                  }
                                  if(!empty($sch['Schedule']['schedule_date_4'])) { 
                                          $sch4= date('m/d/Y',strtotime($sch['schedule_date_4'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_4']));$days=$days+1;
                                  }
                                  if(!empty($schedule['schedule_date_5'])) { 
                                          $sch5= date('m/d/Y',strtotime($sch['schedule_date_5'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['schedule_to_5']));$days=$days+1;
                                  }
                                  if(!empty($sch['Schedule']['schedule_date_6'])) { 
                                          $sch6= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_6']));$days=$days+1;
                                  }
                                  if(!empty($sch['Schedule']['schedule_date_7'])) {
                                          $sch7= date('m/d/Y',strtotime($sch['schedule_date_7'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_7']));$days=$days+1;
                                  }
                                  if(!empty($sch['Schedule']['schedule_date_8'])) {
                                          $sch8= date('m/d/Y',strtotime($sch['schedule_date_8'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_8']));$days=$days+1;
                                  }
                                  if(!empty($sch['Schedule']['schedule_date_9'])) { 
                                          $sch9= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_9']));$days=$days+1;
                                  }
                                  if(!empty($sch['Schedule']['schedule_date_10'])) { 
                                          $sch10= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_10']));$days=$days+1;
                                  }
                                  
                                 $email_string .='<td valign="top">'.$days.'</td>';		    
                                 $email_string .='<td valign="top">'.$sch1.'<br/>'.$sch2.'<br/>'.$sch3.'<br/>'.$sch4.'<br/>'.$sch5.'<br/>'.$sch6.'<br/>'.$sch7.'<br/>'.$sch8.'<br/>'.$sch9.'<br/>'.$sch10.'</td>';
                                 $email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
                                 $email_string .= "</tr>";
                         }
                         
                          $email_string .= '</tbody></table>';
                  
                  
          $servicecall_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>6)));
          $link = "<a href=" . $this->selfURL() . "/homes/clientlogin>Click here</a>"; 
          $servicecall_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENTNAME','#SPNAME','#SPCOMPANY','#DETAIL','#SCHEDULEPHONE','#REPORTS','#LINK'),array($this->selfURL() ,$client_name,$spname,$spcompany,$email_string,$spschedularphone,$reportdata,$link),$servicecall_temp['EmailTemplate']['mail_body']);
          $this->set('servicecall_temp',$servicecall_temp['EmailTemplate']['mail_body']);
          $this->Email->to = $client_email;
          $this->Email->subject = $servicecall_temp['EmailTemplate']['mail_subject'];
          $this->Email->from = EMAIL_FROM;
          $this->Email->template = 'service_call_temp'; // note no '.ctp'
          $this->Email->sendAs = 'both'; // because we like to send pretty mail
          $this->Email->send();//Do not pass any args to send()
           
           
           $this->ServiceCall->updateAll(array('ServiceCall.reminder_days'=>'"0"','ServiceCall.reminder_date'=>'""'),array('ServiceCall.id'=>$callID));
          /*Send Email code ends here*/
        }
      }
    }
    die;
  }
}