<?php 
class ClientsController extends AppController {
	
	var $name = 'Clients';
        var $uses = array('User','Company');
	var $components = array('Calendar','Email','Session','Cookie');
	var $helpers = array('Html','Ajax','Javascript','Session','Calendar','Common','Time');

	function beforeFilter(){
	 parent::beforeFilter();
	}
	
	
	function client_annoucement()
	{
		$this->layout=null;
		$client = $this->Session->read('Log');
		if(empty($client))
		{
			$this->redirect('/homes/login');
		}		
		
		$sp_id = $client['User']['sp_id'];
		$this->loadModel('SpGeneralCompanyAnnouncement');
		$annoucedata = $this->SpGeneralCompanyAnnouncement->find('first', array('conditions'=>array('SpGeneralCompanyAnnouncement.sp_id'=>$sp_id),
						  'fields' => array('SpGeneralCompanyAnnouncement.for_clients'),		        
						  'recursive' => 1
					  ));
		$this->set('annoucedata',$annoucedata);
		if(!empty($this->data))
		{			
			$data=$this->data['User']['prompt'];
			$this->Session->write('likeprompt',$data);
			$this->redirect('dashboard');
		}
	}
	

/*****************************************************************/

#function name: client_dashboard()
#functiion description: dashboard for client/property owner.

  function client_dashboard(){
	  $this->layout='client';
	  $likeprompt=$this->Session->read('likeprompt');
	  $this->set('likeprompt',$likeprompt);
	  $client = $this->Session->read('Log');
	  $client_id = $this->Session->read('client_id');
	  $this->set('session_client_id',$client_id);
	  $sp_id = $client['User']['sp_id'];
	  $this->loadModel('User');
	  $spInfo = $this->User->findById($sp_id,array('Company.name','Company.company_logo'));
	  $this->set('spInfo',$spInfo);
	  $this->loadModel('SpGeneralCompanyAnnouncement');
	  $annoucedata = $this->SpGeneralCompanyAnnouncement->find('first', array('conditions'=>array('SpGeneralCompanyAnnouncement.sp_id'=>$sp_id),
				        'fields' => array('SpGeneralCompanyAnnouncement.for_clients'),		        
				        'recursive' => 1
				    ));
	  $this->set('annoucedata',$annoucedata);
	  #NEW CODE DONE BY VISHAL ON 05 June 2012
	  
	  
	  
	  $this->set('title_for_layout',"Client Dashboard");
	  $this->set('businessType',Configure::read('businessType'));
	  $this->set('reportType',Configure::read('reportType'));
	  $this->loadModel('Message');
	  #Calculate number of unread messages
	  $count_messages = $this->Message->find('count',array('conditions'=>array('Message.receiver_id'=>$client_id,'Message.message_read'=>'N')));
	  $this->set('count_messages',$count_messages);
	  
	
	$this->loadModel('ServiceCall');			
	#CODE to find all clients assigned in assigned table
	$res=$this->ServiceCall->find("count",array('conditions'=>array('ServiceCall.client_id'=>$client_id,'ServiceCall.call_status !='=>1)));
	$this->set('pendingCall',$res);
	
	$totalOrders=$this->ServiceCall->find("count",array('conditions'=>array('ServiceCall.client_id'=>$client_id,'ServiceCall.call_status'=>1)));
	$this->set('totalOrders',$totalOrders);
	
	$this->loadModel('Quote');			
	#CODE to find all clients assigned in assigned table
	$quote=$this->Quote->find("count",array('conditions'=>array('Quote.client_id'=>$client_id,'Quote.client_response'=>'')));
	$this->set('quote',$quote);
	}
/*****************************************************************/

#function name: client_logout()
#functiion description: TO LOGOUT FROM CLIENT PANEL.	

	function client_logout(){
		if ( $this->Cookie->read("Log.User") ) {
			$this->Cookie->delete('Log.User');
		}
		$this->Session->destroy('Log');
		$this->Session->setFlash('You have been logged out successfully');
		$this->redirect('/homes/login');
	}	
/*****************************************************************/

#function name: client_editinfo()
#functiion description: TO EDIT THE CLIENT PERSONAL INFORMATION.
	
  function client_editinfo($id = null){
	$this->loadModel('Company');
	$this->set('country',$this->getCountries());
	$this->set('state',$this->getallState());
	//pr($this->getallState());die;
    if(isset($this->data) && !empty($this->data)){
	//pr($this->data);die;
      if($this->User->save($this->data['User'],false)){
	$result1 = $this->Company->find('first',array('conditions'=>array('Company.user_id'=>$id)));
        $this->data['Company']['id'] = $result1['Company']['id'];
	$this->Company->save($this->data['Company']);
          $result = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['User']['id'])));
          $this->Session->write('Log',$result);
          $this->Session->setFlash('Information has been updated successfully',true);
          $this->redirect(array('controller'=>'clients','action'=>'dashboard'));
       }
     }else {
      $this->User->id = base64_decode($id); //
      $this->data = $this->User->read();
    }
  
  }
/*****************************************************************/

#function name: client_changepasswd()
#functiion description: TO Change cleint password.

	function client_changepasswd(){
		$this->loadModel('User');
		$this->layout='client';
		$clientsession = $this->Session->read('Log');
		$pass = $clientsession['User']['password'];
		if(isset($this->data) && !empty($this->data)){     		
			$this->User->set($this->data);					  
			$data['User']['id'] =  $this->data['User']['id'];
			$oldpass=md5($this->data['User']['oldpassword']);
			if($oldpass==$pass){
			$this->data['User']['password'];
			$data['User']['password'] = md5($this->data['User']['password']);
			if($this->User->save($data['User'],array('validate'=>false))){
				 $result = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['User']['id'])));
				 $this->Session->write('Log', $result);
				 $this->Session->setFlash('Password has been changed successfully');
				 $this->redirect(array('controller'=>'clients','action'=>'changepasswd'));
			}
			}else{
				 $this->Session->setFlash('Please enter correct old password');
				 $this->redirect(array('controller'=>'clients','action'=>'changepasswd'));
		 }
		}
	}
/*****************************************************************/
#function name: client_schedule()
#functiion description: find the schedule of the clients
	function client_schedule()
	{
		$clientses = $this->Session->read('Log');
		$client_id = $clientses['User']['id'];
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		$this->ServiceCall->bindModel(array('hasMany'=>array('ClientSchedule')));		
		#CODE to find all clients assigned in assigned table
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.client_id'=>$client_id,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));		
		$this->set("data",$res);
		$this->set('frequency',Configure::read('frequency'));
	}

#function name: client_pendingSchedule()
#functiion description: find the schedule that have to be approved from the clients
	function client_pendingSchedule()
	{
		$clientses = $this->Session->read('Log');
		$client_id = $clientses['User']['id'];
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		$this->ServiceCall->bindModel(array('hasMany'=>array('ClientSchedule')));		
		#CODE to find all clients assigned in assigned table
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.client_id'=>$client_id,'ServiceCall.call_status !='=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));		
		$this->set("data",$res);
		$this->set('frequency',Configure::read('frequency'));
	}

	
/*****************************************************************/
#function name: client_changeststatus()
#functiion description: change the status of service call	
	function client_changeststatus($id=null, $status){
		$id=base64_decode($id);
		$status=base64_decode($status);
		$clientses = $this->Session->read('Log');
		$client_id = $clientses['User']['id'];
		$statusWord=($status=='1')?'Approved':'Declined';		
		if($client_id!="" && $client_id!=null){
			$this->loadModel('ServiceCall');
			$this->ServiceCall->updateAll(array('ServiceCall.call_status'=>'"'.$status.'"','ServiceCall.modified'=>'"'.date('Y-m-d H:i:s').'"'),array('ServiceCall.id'=>$id));
			if($status=="1"){
				/*Send emails*/
				$this->sendApproveMail($id);
			}
			$this->Session->setFlash('Service Call has been '.$statusWord.' successfully');			
		}
		$this->redirect(array('controller'=>'clients','action'=>'pendingSchedule','client'=>true));
	}
	
/*****************************************************************/
#function name: sendApproveMail()
#functiion description: send the service call approval mail to inspector, client and service provider
#function added by Manish Kumar
	function sendApproveMail($serviceCallId=null){		
		$this->loadModel('ServiceCall');
		$serviceData = $this->ServiceCall->findById($serviceCallId,array('contain'=>false));
		
		$this->loadModel('User');
		$clientData = $this->User->findById($serviceData['ServiceCall']['client_id']);
		$sp = $this->User->findById($serviceData['ServiceCall']['sp_id']);
					 
		$client_name = $clientData['User']['fname'].' '.$clientData['User']['lname']; //cleintname
		$client_email = $clientData['User']['email'];
		$spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
		$spemail = $sp['User']['email']; //spemail
		$spschedularphone = $sp['User']['phone']; //sp-schedulerphone
		$spcompany=$sp['Company']['name'];
		
		$this->loadModel('Schedule');
		$this->loadmodel('EmailTemplate');
		$this->Schedule->bindModel(
				array('belongsTo' => array(
				'Report' => array(
				'className' => 'Report',
				'fields'=>array('Report.name')
					)
				    )
				 )
				);
		//$this->Schedule->bindModel('belongsTo'=>'Report');
		$Schdata=$this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$serviceCallId)));
		//pr($Schdata);
		//echo "===============>";
	       
		$email_string ='';
		$email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
					<tbody>
					<tr style="background-color:#0B83A6">
					    <td><b>ReportName</b></td>
					    <td><b>Days</b></td>
					    <td><b>Schedule Date/Time</b></td>
					    <td><b>Inspector Assigned</b></td>
					</tr>';
		$reportdata='';
		$sch2=$sch3=$sch4=$sch5=$sch6=$sch7=$sch8=$sch9=$sch10="";
	       foreach($Schdata as $key=>$sch){
		
		$ins_email_string ='';
		$ins_email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
					<tbody>
					<tr style="background-color:#0B83A6">
					    <td><b>ReportName</b></td>
					    <td><b>Days</b></td>
					    <td><b>Schedule Date/Time</b></td>
					    <td><b>Service Provider Name(Company)</b></td>
					    <td><b>Client Name</b></td>
					</tr>';
		
		$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['Schedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
		       $reportdata   .=$sch['Report']['name'].',';
		       
		       $email_string .= "<tr>";
		       $ins_email_string .= "<tr>";
		       
		       $days=0;
		       $email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';
		       $ins_email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';
		       
		       $sch1= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_1'])); $days=$days+1;
			if(!empty($sch['Schedule']['schedule_date_2'])) { 
				$sch2= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_2'])); $days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_3'])) { 
				$sch3= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_3']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_4'])) {
				$sch4= date('m/d/Y',strtotime($sch['schedule_date_4'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_4']));$days=$days+1;
			}
			if(!empty($schedule['schedule_date_5'])) {
				$sch5= date('m/d/Y',strtotime($sch['schedule_date_5'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['schedule_to_5']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_6'])) { 
				$sch6= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_6']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_7'])) { 
				$sch7= date('m/d/Y',strtotime($sch['schedule_date_7'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_7']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_8'])) { 
				$sch8= date('m/d/Y',strtotime($sch['schedule_date_8'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_8']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_9'])) { 
				$sch9= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_9']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_10'])) { 
				$sch10= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_10']));$days=$days+1;
			}
			
		       $ins_email_string .='<td valign="top">'.$days.'</td>';		    
		       $ins_email_string .='<td valign="top">'.$sch1.'<br/>'.$sch2.'<br/>'.$sch3.'<br/>'.$sch4.'<br/>'.$sch5.'<br/>'.$sch6.'<br/>'.$sch7.'<br/>'.$sch8.'<br/>'.$sch9.'<br/>'.$sch10.'</td>';
		       $ins_email_string .='<td valign="top">'.$spname.'('.$spcompany.')'.'</td>'.'<br/>';
		       $ins_email_string .='<td valign="top">'.$client_name.'</td>'.'<br/>';
		       $ins_email_string .= "</tr>";
		       $ins_email_string .= '</tbody></table>';
		       
		       /*Send Approval email to lead inspector code starts here*/
		        $link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
			$approved_servicecall_temp_ins = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>19)));		
			$approved_servicecall_temp_ins['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#CLIENTNAME','#DETAIL','#LINK'),array($this->selfURL(),$leader['User']['fname'].' '.$leader['User']['lname'],$client_name,$ins_email_string,$link),$approved_servicecall_temp_ins['EmailTemplate']['mail_body']);
			$this->set('servicecall_temp',$approved_servicecall_temp_ins['EmailTemplate']['mail_body']);
			$this->Email->to = $leader['User']['email'];
			$this->Email->subject = $approved_servicecall_temp_ins['EmailTemplate']['mail_subject'];
			$this->Email->from = EMAIL_FROM;
			$this->Email->template = 'service_call_temp'; // note no '.ctp'
			$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
			$this->Email->send();//Do not pass any args to send()
			/*Send Approval email to lead inspector code ends here*/
		       
		       $email_string .='<td valign="top">'.$days.'</td>';		    
		       $email_string .='<td valign="top">'.$sch1.'<br/>'.$sch2.'<br/>'.$sch3.'<br/>'.$sch4.'<br/>'.$sch5.'<br/>'.$sch6.'<br/>'.$sch7.'<br/>'.$sch8.'<br/>'.$sch9.'<br/>'.$sch10.'</td>';
		       $email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
		       $email_string .= "</tr>";
	       }
	       
		$email_string .= '</tbody></table>';
		
		/*Send Approval email to client code starts here*/
		$cllink = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
		$approved_servicecall_temp_client = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>18)));		
		$approved_servicecall_temp_client['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#DETAIL','#LINK'),array($this->selfURL() ,$client_name,$email_string,$cllink),$approved_servicecall_temp_client['EmailTemplate']['mail_body']);
		$this->set('servicecall_temp',$approved_servicecall_temp_client['EmailTemplate']['mail_body']);
		$this->Email->to = $client_email;
		$this->Email->subject = $approved_servicecall_temp_client['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'service_call_temp'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
		$this->Email->send();//Do not pass any args to send()
		/*Send Approval email to client code ends here*/
		
		/*Send Approval email to Service Provider code starts here*/
		$splink = "<a href=" . $this->selfURL() . "/homes/splogin>Click here</a>"; 
		$approved_servicecall_temp_sp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>18)));		
		$approved_servicecall_temp_sp['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#DETAIL','#LINK'),array($this->selfURL() ,$spname,$email_string,$splink),$approved_servicecall_temp_sp['EmailTemplate']['mail_body']);
		$this->set('servicecall_temp',$approved_servicecall_temp_sp['EmailTemplate']['mail_body']);
		$this->Email->to = $spemail;
		$this->Email->subject = $approved_servicecall_temp_sp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'service_call_temp'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
		$this->Email->send();//Do not pass any args to send()
		/*Send Approval email to Service Provider code ends here*/
		
	}
/*****************************************************************/
#function name: client_declineRequest()
#functiion description: to decline the service call and submit own timings
#created by: Manish Kumar
#on: Oct 27, 2012

	function client_declineRequest($serviceCallId = null){
	    $serviceCallId = base64_decode($serviceCallId);
	    $clientses = $this->Session->read('Log');
	    $client_id = $clientses['User']['id'];	    
	    $this->loadModel('ServiceCall');	    
	    $res=$this->ServiceCall->find("first",array('conditions'=>array('ServiceCall.id'=>$serviceCallId),'recursive'=>2));
	    $this->set("data",$res);
	    $this->set('frequency',Configure::read('frequency'));
	    
	    if(!empty($this->data)){
		$this->loadModel('ClientSchedule');
		foreach($this->data['ClientSchedule'] as $saveData){
			$scheduleData['ClientSchedule']['report_id']=$saveData['report_id'];
			$scheduleData['ClientSchedule']['lead_inspector_id']=$saveData['lead_inspector_id'];
			$scheduleData['ClientSchedule']['service_call_id']=$serviceCallId;
			$scheduleData['ClientSchedule']['schedule_date_1']=date("Y-m-d",strtotime($saveData['schedule_date_1']));
			$scheduleData['ClientSchedule']['schedule_from_1']=$saveData['schedule_from_1'];
			$scheduleData['ClientSchedule']['schedule_to_1']=$saveData['schedule_to_1'];
			if(isset($saveData['schedule_date_2']) && isset($saveData['schedule_from_2']) && isset($saveData['schedule_to_2'])){
				$scheduleData['ClientSchedule']['schedule_date_2']=date("Y-m-d",strtotime($saveData['schedule_date_2']));
				$scheduleData['ClientSchedule']['schedule_from_2']=$saveData['schedule_from_2'];
				$scheduleData['ClientSchedule']['schedule_to_2']=$saveData['schedule_to_2'];
			}
			if(isset($saveData['schedule_date_3']) && isset($saveData['schedule_from_3']) && isset($saveData['schedule_to_3'])){
				$scheduleData['ClientSchedule']['schedule_date_3']=date("Y-m-d",strtotime($saveData['schedule_date_3']));
				$scheduleData['ClientSchedule']['schedule_from_3']=$saveData['schedule_from_3'];
				$scheduleData['ClientSchedule']['schedule_to_3']=$saveData['schedule_to_3'];
			}
			if(isset($saveData['schedule_date_4']) && isset($saveData['schedule_from_4']) && isset($saveData['schedule_to_4'])){
				$scheduleData['ClientSchedule']['schedule_date_4']=date("Y-m-d",strtotime($saveData['schedule_date_4']));
				$scheduleData['ClientSchedule']['schedule_from_4']=$saveData['schedule_from_4'];
				$scheduleData['ClientSchedule']['schedule_to_4']=$saveData['schedule_to_4'];
			}
			if(isset($saveData['schedule_date_5']) && isset($saveData['schedule_from_5']) && isset($saveData['schedule_to_5'])){
				$scheduleData['ClientSchedule']['schedule_date_5']=date("Y-m-d",strtotime($saveData['schedule_date_5']));
				$scheduleData['ClientSchedule']['schedule_from_5']=$saveData['schedule_from_5'];
				$scheduleData['ClientSchedule']['schedule_to_5']=$saveData['schedule_to_5'];
			}
			if(isset($saveData['schedule_date_6']) && isset($saveData['schedule_from_6']) && isset($saveData['schedule_to_6'])){
				$scheduleData['ClientSchedule']['schedule_date_6']=date("Y-m-d",strtotime($saveData['schedule_date_6']));
				$scheduleData['ClientSchedule']['schedule_from_6']=$saveData['schedule_from_6'];
				$scheduleData['ClientSchedule']['schedule_to_6']=$saveData['schedule_to_6'];
			}
			if(isset($saveData['schedule_date_7']) && isset($saveData['schedule_from_7']) && isset($saveData['schedule_to_7'])){
				$scheduleData['ClientSchedule']['schedule_date_7']=date("Y-m-d",strtotime($saveData['schedule_date_7']));
				$scheduleData['ClientSchedule']['schedule_from_7']=$saveData['schedule_from_7'];
				$scheduleData['ClientSchedule']['schedule_to_7']=$saveData['schedule_to_7'];
			}
			if(isset($saveData['schedule_date_8']) && isset($saveData['schedule_from_8']) && isset($saveData['schedule_to_8'])){
				$scheduleData['ClientSchedule']['schedule_date_8']=date("Y-m-d",strtotime($saveData['schedule_date_8']));
				$scheduleData['ClientSchedule']['schedule_from_8']=$saveData['schedule_from_8'];
				$scheduleData['ClientSchedule']['schedule_to_8']=$saveData['schedule_to_8'];
			}
			if(isset($saveData['schedule_date_9']) && isset($saveData['schedule_from_9']) && isset($saveData['schedule_to_9'])){
				$scheduleData['ClientSchedule']['schedule_date_9']=date("Y-m-d",strtotime($saveData['schedule_date_9']));
				$scheduleData['ClientSchedule']['schedule_from_9']=$saveData['schedule_from_9'];
				$scheduleData['ClientSchedule']['schedule_to_9']=$saveData['schedule_to_9'];
			}
			if(isset($saveData['schedule_date_10']) && isset($saveData['schedule_from_10']) && isset($saveData['schedule_to_10'])){
				$scheduleData['ClientSchedule']['schedule_date_10']=date("Y-m-d",strtotime($saveData['schedule_date_10']));
				$scheduleData['ClientSchedule']['schedule_from_10']=$saveData['schedule_from_10'];
				$scheduleData['ClientSchedule']['schedule_to_10']=$saveData['schedule_to_10'];
			}
			$this->ClientSchedule->create();			
			$this->ClientSchedule->save($scheduleData);
		}
		$this->ServiceCall->updateAll(array('ServiceCall.call_status'=>"2",'ServiceCall.declined_reason'=>'"'.$this->data['ServiceCall']['declined_reason'].'"'),array('ServiceCall.id'=>$serviceCallId));
		$this->Session->setFlash('Your request has been sent to the scheduler.');
		//sendMailtoSp
		$this->sendMailToSp($serviceCallId);
		//sendMailtoSp
		$this->redirect(array('controller'=>'clients','action'=>'pendingSchedule'));		
		
	    }
	}
	
	function sendMailToSp($serviceCallId=null){
		#code to send email to sp to tell the changes in schedule from client
		$this->loadModel('ServiceCall');
		$serviceData = $this->ServiceCall->findById($serviceCallId,array('contain'=>false));
		$declinedReason = $serviceData['ServiceCall']['declined_reason'];
		
		$this->loadModel('User');
		$clientData = $this->User->findById($serviceData['ServiceCall']['client_id']);
		$sp = $this->User->findById($serviceData['ServiceCall']['sp_id']);
					 
		$client_name = $clientData['User']['fname'].' '.$clientData['User']['lname']; //cleintname		
		$spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
		$spemail = $sp['User']['email']; //spemail		
		
		$this->loadModel('ClientSchedule');
		$this->loadmodel('EmailTemplate');
		$this->ClientSchedule->bindModel(
				array('belongsTo' => array(
				'Report' => array(
				'className' => 'Report',
				'fields'=>array('Report.name')
					)
				    )
				 )
				);
		//$this->Schedule->bindModel('belongsTo'=>'Report');
		$Schdata=$this->ClientSchedule->find('all',array('conditions'=>array('ClientSchedule.service_call_id'=>$serviceCallId)));
		//pr($Schdata);
		//echo "===============>";
	       
		$email_string ='';
		$email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
					<tbody>
					<tr style="background-color:#0B83A6">
					    <td><b>ReportName</b></td>
					    <td><b>Days</b></td>
					    <td><b>Schedule Date/Time</b></td>
					    <td><b>Inspector Assigned</b></td>
					</tr>';
		$reportdata='';
		$sch2=$sch3=$sch4=$sch5=$sch6=$sch7=$sch8=$sch9=$sch10="";
	       foreach($Schdata as $key=>$sch){		
		$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['ClientSchedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
		       $reportdata   .=$sch['Report']['name'].',';
		       $email_string .= "<tr>";
		       $days=0;
		       $email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';       
		       $sch1= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_1'])); $days=$days+1;       
			if(!empty($sch['ClientSchedule']['schedule_date_2'])) { 
			    $sch2= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_2'])); $days=$days+1;
			}
			if(!empty($sch['ClientSchedule']['schedule_date_3'])) { 
			    $sch3= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_3']));$days=$days+1;
			}
			if(!empty($sch['ClientSchedule']['schedule_date_4'])) { 
			    $sch4= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_4'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_4']));$days=$days+1;
			}
			if(!empty($sch['ClientSchedule']['schedule_date_5'])) { 
			    $sch5= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_5'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_5']));$days=$days+1;
			}
			if(!empty($sch['ClientSchedule']['schedule_date_6'])) { 
			    $sch6= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_6']));$days=$days+1;
			}
			if(!empty($sch['ClientSchedule']['schedule_date_7'])) { 
			    $sch7= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_7'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_7']));$days=$days+1;
			} 
			if(!empty($sch['ClientSchedule']['schedule_date_8'])) { 
			    $sch8= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_8'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_8']));$days=$days+1;
			}
			if(!empty($sch['ClientSchedule']['schedule_date_9'])) { 
			    $sch9= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_9']));$days=$days+1;
			}
			if(!empty($sch['ClientSchedule']['schedule_date_10'])) { 
			    $sch10= date('m/d/Y',strtotime($sch['ClientSchedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['ClientSchedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['ClientSchedule']['schedule_to_10']));$days=$days+1;			    
			}	       
		       
		       $email_string .='<td valign="top">'.$days.'</td>';		    
		       $email_string .='<td valign="top">'.$sch1.'<br/>'.$sch2.'<br/>'.$sch3.'<br/>'.$sch4.'<br/>'.$sch5.'<br/>'.$sch6.'<br/>'.$sch7.'<br/>'.$sch8.'<br/>'.$sch9.'<br/>'.$sch10.'</td>';
		       $email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
		       $email_string .= "</tr>";
	       }
	       
		$email_string .= '</tbody></table>';
		
		
		/*Send rescheduled serice call notification to Service Provider*/
		$link = "<a href=".$this->selfURL()."/homes/splogin>Click here</a>"; 
		$rescheduled_servicecall_temp_sp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>20)));
		$rescheduled_servicecall_temp_sp['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#CLIENTNAME','#DETAIL','#REASON','#LINK'),array($this->selfURL() ,$spname,$client_name,$email_string,$declinedReason,$link),$rescheduled_servicecall_temp_sp['EmailTemplate']['mail_body']);
		$this->set('servicecall_temp',$rescheduled_servicecall_temp_sp['EmailTemplate']['mail_body']);
		$this->Email->to = $spemail;
		$this->Email->subject = $rescheduled_servicecall_temp_sp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'service_call_temp'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
		$this->Email->send();//Do not pass any args to send()
		/*Send rescheduled serice call notification to Service Provider*/
	}
/*****************************************************************/
#function name: client_viewQuote()
#functiion description: to view the quote submitted by sp and accept or reject it
#created by: Manish Kumar
#on: Nov 27, 2012
	function client_viewQuote(){
		$spId = $_REQUEST['spID'];
		$clientId = $_REQUEST['clientID'];
		$servicecallId = $_REQUEST['serviceCallID'];
		$reportId = $_REQUEST['reportID'];
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		
		$this->loadModel('Quote');
		$quoteData = $this->Quote->find('first',array('conditions'=>array('Quote.report_id'=>$reportId,'Quote.servicecall_id'=>$servicecallId)));//echo '<pre>';print_r($quoteData);die;
		$this->set('quoteData',$quoteData);
		
		if(isset($this->data) && !empty($this->data)){ 
			$clientResponse = $this->data['Quote']['client_response'];
			$data['Quote']['id']= $quoteData['Quote']['id'];
			if($clientResponse=='a'){
				$data['Quote']['client_response']= $quoteData['Quote']['client_response'];
				if(isset($this->data['Quote']['signed_form']['name']) && !empty($this->data['Quote']['signed_form']['name'])){
					$temp_exp = explode('.',$this->data['Quote']['signed_form']['name']);
					$extPart = end($temp_exp);
					    // Start of file uploading script		
					    $allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
					    if(in_array($extPart, $allowedExts)){
						    $target = WWW_ROOT.'quotefile/';
						    //$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
						    $randomName = md5(time()).'.'.$extPart;
						    $target = $target.$randomName;
						    if(move_uploaded_file($this->data['Quote']['signed_form']['tmp_name'], $target)){							    
							    $data['Quote']['signed_form'] = $randomName;
							    /*send mail to SP*/						    
							    $this->sendResponseMail($_REQUEST['spID'],$_REQUEST['clientID'],$_REQUEST['serviceCallID'],$_REQUEST['reportID'],'Accepted');
							    /*send mail to sp*/						       
						    } 
					    }
					    else{
						    $this->Session->setFlash(__('Sorry ! Please upload only pdf/xls/doc/xlsx/docx file',true));
						    $this->redirect($this->referer());
					    }
				}
			}else if($clientResponse=='d'){
				if(isset($this->data['Quote']['work_order_form']['name']) && !empty($this->data['Quote']['work_order_form']['name'])){
					$temp_exp = explode('.',$this->data['Quote']['work_order_form']['name']);
					$extPart = end($temp_exp);
					    // Start of file uploading script		
					    $allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
					    if(in_array($extPart, $allowedExts)){
						    $target = WWW_ROOT.'quotefile/';
						    //$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
						    $randomName = md5(time()).'.'.$extPart;
						    $target = $target.$randomName;
						    if(move_uploaded_file($this->data['Quote']['work_order_form']['tmp_name'], $target)){							    
							    $data['Quote']['work_order_form'] = $randomName;
							    /*send mail to SP*/						    
							    $this->sendResponseMail($_REQUEST['spID'],$_REQUEST['clientID'],$_REQUEST['serviceCallID'],$_REQUEST['reportID'],'Denied');
							    /*send mail to sp*/	
						    } 
					    }
					    else{
						    $this->Session->setFlash(__('Sorry ! Please upload only pdf/xls/doc/xlsx/docx file',true));
						    $this->redirect($this->referer());
					    }
				}
			}else if($clientResponse=='n'){
				$this->sendResponseMail($_REQUEST['spID'],$_REQUEST['clientID'],$_REQUEST['serviceCallID'],$_REQUEST['reportID'],'Not');
			}
			$data['Quote']['client_response'] = $clientResponse;
			$this->Quote->save($data['Quote']);
			$this->Session->setFlash(__('Response has been saved successfully',true));
			$this->redirect($this->referer());
		}
	}
	
/*
	Function: sendQuoteMail,
	desc: To send the email to client,
	created by: Manish Kumar,
	on: Nov 26, 2012
*/
	function sendResponseMail($spId,$clientId,$servicecallId,$reportId,$response){
		#code to send an email
		$this->loadModel('EmailTemplate');
		
		$this->loadModel('User');
		$sp = $this->User->findById($spId,array('User.email','User.fname','User.lname'));		
		$sp_name = $sp['User']['fname'].' '.$sp['User']['lname'];
		$sp_email = $sp['User']['email'];
		
		/*Client Data*/		
		$clientInfo = $this->Session->read('Log');
		$client_name = $clientInfo['User']['fname'].' '.$clientInfo['User']['lname'];
		$client_email = $clientInfo['User']['email'];
		/*Client Data*/
		
		/*Report Data*/
		$this->loadModel('Report');
		$reportInfo = $this->Report->findById($reportId,array('Report.name'));
		$reportName = $reportInfo['Report']['name'];
		/*Report Data*/
		
		//CODE TO FIRE AN EMAIL
		if($response == 'Accepted'){
			$body_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>23)));
		}else if($response == 'Denied'){
			$body_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>24)));
		}else if($response == 'Not'){
			$body_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>25)));
		}	
		$link = "<a href=".$this->selfURL()."/homes/splogin>Click here</a>";
		$code_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENT_NAME','#SP_NAME','#REPORTNAME','#LINK'),array($this->selfURL(),ucwords($client_name),ucwords($sp_name),ucwords($reportName),$link),$body_temp['EmailTemplate']['mail_body']);
		$this->set('responsecontent',$code_temp['EmailTemplate']['mail_body']);
		
		$this->Email->from = EMAIL_FROM;
		$this->Email->to = $sp_email;				
		$this->Email->subject = $body_temp['EmailTemplate']['mail_subject'];
		$this->Email->template = 'responsetemplate';
		$this->Email->sendAs = 'both'; // because we like to send pretty mail			
		$this->Email->send(); //Do not pass any args to send()
	}	
}

