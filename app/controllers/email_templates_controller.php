<?php
class EmailTemplatesController extends AppController{
	var $name='EmailTemplates';
	var $layout='admin';
	var $uses = array('User','EmailTemplate');
	var $helpers = array('Html','Js','Form','Session','Fck');
	var $components = array('Cookie','Session','RequestHandler');
	var $paginate = "";

/************************************************************************
list emailtemplates function for Emails controller
************************************************************************/  
	   function admin_emailtemplist(){
	     $this->set('listing',$this->paginate('EmailTemplate'));
	   }   
/************************************************************************
 function of Add Emailtemplate
************************************************************************/	   
	  
    function admin_addemailtemplate(){
  		$this->EmailTemplate->set($this->data);
  		if(!empty($this->data)){
  		
		    if ($this->EmailTemplate->save($this->data)) {			
  				$this->Session->setFlash('New Email has been Saved');
  				$this->redirect(array('action' => 'list'));
  		  }else{
  				$this->Session->setFlash('New Email could not be Saved. Please, try again.');
  		   }
  		
  		}	
  				$this->render('admin_editemailtemplate');
	   }

/************************************************************************
 function of Edit Emailtemplate
************************************************************************/
    function admin_editemailtemplate($id=null){
  			if (!empty($this->data)){
			$this->EmailTemplate->set($this->data);
			   if($this->EmailTemplate->validates())
			   {
			      if($this->EmailTemplate->save($this->data)){
					$this->Session->setFlash('Email template has been saved successfully');				 
					$this->redirect(array("controller" => "emailTemplates", "action" => "emailtemplist"));
			      }
			   }
			  } else {
					$this->data = $this->EmailTemplate->read(null, $id);
					$this->set('id',$id);			
			  }
		  }
}