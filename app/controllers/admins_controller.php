<?php
class AdminsController extends AppController {

	var $name = 'Admins';
	var $components = array('Email','Session','Cookie','Uploader.Uploader','VideoEncoder');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Fck','Common');
  var $paginate = array('limit'=>'10');
  
/*
*************************************************************************
*Function Name		 :	admin_list
*Functionality		 :	Login to Admin Panel.
*************************************************************************
*/ 	

function admin_list()
{
    $this->layout='admin';
    $this->set('title_for_layout',"Manager admin user");
    //$this->paginate=array('conditions'=>array('Admin.role !=' => 'superadmin'));
    $this->set('listing',$this->paginate('Admin'));   
}



/*
*************************************************************************
*Function Name		 :	admin_add
*Functionality		 :	Admin staff add 
*************************************************************************
*/
function admin_add() {		
		$this->layout='admin';	
 		 $this->set('title_for_layout',"Add Member");	
			if(!empty($this->data)) { 		   	
 		   	// $this->Admin->setValidate($this->data); 		 
            if($this->Admin->set($this->data) && $this->Admin->validates()) {
         	   $data['Admin']['fname']=$this->data['Admin']['fname'] ; 
             $data['Admin']['lname']=$this->data['Admin']['lname'] ;
             $data['Admin']['username']=$this->data['Admin']['username'] ; 
             $data['Admin']['pwd']	= md5($this->data['Admin']['pwd']);
             $data['Admin']['email']=$this->data['Admin']['email'] ; 
             $data['Admin']['status']=$this->data['Admin']['status'] ;               
             if($this->Admin->save($data,array('validate'=>false))) { 
             
                  //CODE TO FIRE AN EMAIL	
                $this->loadmodel('EmailTemplate');  		       
                $register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>10)));
                $link = "<a href=" . $this->selfURL() . "/admin/admins>Click here</a>";                  
                $register_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#USERNAME','#PASSWORD','#CLICKHERE'),array($this->selfURL() ,@$data['Admin']['fname'],@$data['Admin']['email'],$this->data['Admin']['pwd'],@$link),$register_temp['EmailTemplate']['mail_body']);                  
                $this->Email->from = EMAIL_FROM;
                $this->Email->to = $data['Admin']['email'];
                $this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
                $this->Email->template = 'register_mail_by_admin';
                $this->Email->sendAs = 'both'; // because we like to send pretty mail
                $this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);                  
                $this->Email->send(); //Do not pass any args to send()                  
             	  $this->Session->setFlash('Member added successfully','success' );               	
             } else {               
               $this->Session->setFlash('Member can not added.','message' );              
              }
              	 $this->redirect(array('action'=>'list'));	
				   }				 	
      }     	 
 }  	 
	 
/*
*************************************************************************
*Functionality		 :	User edit 
*************************************************************************
*/ 
	function admin_edit($id=null){	
    
		$this->layout='admin';	      
		if(!empty($id) && empty($this->data)){
        $users=$this->Admin->find("first",array("conditions"=>array("Admin.id"=>$id)));         
        $this->data = $users;	
        $this->set('title_for_layout',"Update Member");	
		    $this->set('flag',"Update");
		} else {			
			if (!empty($this->data)) {        
		   	 $this->Admin->set($this->data);		   	 
			    if($this->Admin->validates()){				        
				      if($this->Admin->save($this->data)) {
               	$this->Session->setFlash('Member updated successfully.','success' );               	
               } else {
 	              $this->Session->setFlash('Member can not be updated.','message' );              
               }			
               $this->redirect(array('action'=>'list'));		
				     }
			 }		
		}	
	}
	
 /**************************************************************************
        *Function Name : admin_delete
        *Functionality : use to delete single Member.
  **************************************************************************/  
	
	function admin_delete($id=null) {
		if($this->Admin->delete($id)) {
			$this->Session->setFlash('Member deleted successfully  ','success' );
		} else {
			$this->Session->setFlash('Member can not be deleted hello','message');
		}		
		$this->redirect(array('action'=>'list'));
	}	
/**************************************************************************
        *Function Name : admin_credentialmail
        *Functionality : If admin want to resend the credentials to the admin staff
  **************************************************************************/
    
     function admin_credentialmail($id = null){
			$this->Admin->id = $id;		
			$this->data = $this->Admin->read();
			$password = substr(md5(time()), 0, 6); // to generate a password of 6 characters
		  $encpt_newpassword = md5($password);		
			$data['Admin']['password']= $encpt_newpassword;
			
		  if($this->Admin->save($data)){		
  			$this->loadModel('EmailTemplate');
  			$loginlink=$this->selfURL().'/users/login';
  			$maillink="<a href=".$loginlink." target='_blank'>Click Here</a>";					
  			$staff_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>10)));				
  			$staff_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#USERNAME','#PASSWORD','#CLICKHERE'),array($this->selfURL() ,$this->data['Admin']['fname'],$this->data['Admin']['email'],$password,$maillink),$staff_temp['EmailTemplate']['mail_body']);	
  			$this->Email->to = $this->data['Admin']['email'];
  			$this->Email->subject = $staff_temp['EmailTemplate']['mail_subject'];
  			$this->Email->from = EMAIL_FROM;				
  			$this->Email->template = 'register_mail_by_admin'; // note no '.ctp'
  			$this->Email->sendAs = 'both'; // because we like to send pretty mail
  			$this->set('register_temp',$staff_temp['EmailTemplate']['mail_body']);
  			$this->Email->send(); //Do not pass any args to send()
  			$this->Session->setFlash('Credentials has been sent successfully to ' .$this->data['Admin']['fname'],'sucess');
  			$this->redirect(array('controller'=>'admins','action'=>'list'));
		  }
		}
	  
	
	
  
  /**************************************************************************
        *Function Name : admin_visitordelete
        *Functionality : use to delete single visitor record.
  **************************************************************************/  
	
	function admin_visitordelete($id=null) {
		if($this->SiteUsage->delete($id)) {
			$this->Session->setFlash('Record deleted successfully  ','success' );
		} else {
			$this->Session->setFlash('Record can not be deleted','message');
		}		
		$this->redirect(array('action'=>'visitorlist'));
	}	 
 /**************************************************************************
   *Function Name		 :	changeStatus
   *Functionality		 :	use to change Member status.
  ***************************************************************************/  
   private function changeStatus($id,$state) {	  
    		if(empty($id) || !$subs =$this->Admin->find('first',array('conditions'=>array('Admin.id'=>$id))))	{
    			$this->Session->setFlash('Invalid Member');	
    			$this->redirect('list');
    		}		
        if($state=='active') {
        	$this->data['Admin']['status']=1;
        } else if($state=='deactive') {
        $this->data['Admin']['status']=0;
        } else {
              if($subs['Admin']['status']==0) {
          			  $this->data['Admin']['status']=1;
          		}	else {
          			  $this->data['Admin']['status']=0;
           		}		
        }
      $this->data['Admin']['id'] = $id;
    	if($this->Admin->save($this->data,$validate = false)) {
    		   return true;
    		} else {
           return false;
        }
	} 	
	 /**************************************************************************
        *Function Name : admin_activate
        *Functionality : use to change single member status.
    **************************************************************************/  
  
  function admin_activate($id,$state=null) {			 	
		if($this->changeStatus($id,$state)) {
		   $data =$this->Admin->findById($id);
		      if($data['Admin']['status']==1) {
		           $this->Session->setFlash('Status has been activate.','success');
	         	 } else {
			         $this->Session->setFlash('Status has been deactive.','success');
		        }      
     }
		$this->redirect(array('action'=>'list'));
	}	
	
	
/*
*************************************************************************
*Function Name		 :	admin_changepassword
*Functionality		 :	Owner Change Password 
*************************************************************************
*/ 

/*function admin_changepassword($id=null) {$this->layout='admin';
     
           $this->layout='admin';      		
           $password = rand();  
           $this->data['Admin']['id'] = $id;      	
           $this->data['Admin']['password'] = md5($password);
           if($this->Admin->save($this->data)) {
           $this->_sendMail($id,$password);
             $this->Session->setFlash('Member password changed successfully','success' );
           } else {
             $this->Session->setFlash('Member password can not changed.','message' );              
           }
             $this->redirect(array('action'=>'list'));
         
         
         
        
        
    

}*/ //end function
	
	
/*
*************************************************************************
*Function Name		 :	_sendMail
*Functionality		 :	Send mail for change password
*************************************************************************
*/ 

function _sendMail($id=null,$password=null) 
{
    $User=$this->Admin->find("first",array("conditions"=>array("Admin.id"=>$id)));
        
    $this->Email->to = $User['Admin']['email'];
    // $this->Email->bcc = array('secret@example.com');  
    $this->Email->subject = 'Password has been changed';
    $this->Email->replyTo = 'contact@schedulepal.com';
    $this->Email->from = 'SchedulePal Customer Service <contact@schedulepal.com>';
    $this->Email->template = 'default'; // note no '.ctp'
    // Send as 'html', 'text' or 'both' (default is 'text')
    $this->Email->sendAs = 'html'; // because we like to send pretty mail
    // Set view variables as normal
    $this->set('name', $User['Admin']['fname'].' '.$User['Admin']['lname']);
    $this->set('username', $User['Admin']['username']);
    $this->set('password', $password);  
    
    // Do not pass any args to send()
    $this->Email->send(); 
}  
	 
/*
*************************************************************************
*Function Name		 :	admin_login
*Functionality		 :	Login to Admin Panel.
*************************************************************************
*/ 
   function admin_login(){ 
	$this->layout='admin';
	$this->set('title_for_layout',"Admin login");
	if(isset($this->data))
	{
    	    if(trim($this->data['admin']['username'])=='')
	   {
		$this->Session->setFlash('Please enter username!');
		$this->redirect(array('controller'=>'admins','action'=>'login'));
	   }
        else if(trim($this->data['admin']['pwd'])=='')
        {
		$this->Session->setFlash('Please enter Password!');
		$this->redirect(array('controller'=>'admins','action'=>'login'));
        }
		$admin = $this->Admin->find('first',array('fields'=>array('id','username'),'conditions'=>array('Admin.status'=>'1','Admin.pwd'=> md5($this->data['admin']['pwd']),'Admin.username'=>$this->data['admin']['username'])));
	 		if($admin)
  			{	
	 			$this->Session->write('AdminId',$admin['Admin']['id']);
  				$this->Session->write('Admin',$admin);
  				$this->redirect(array('controller'=>'admins','action'=>'index'));
  			} else {
  				$this->Session->setFlash('Login Failed !');
  			}
  			$this->data['admin']['username']='';
  			$this->data['admin']['pwd']='';  			
	}
    
  }
  
 function admin_index(){
     $this->layout='dashboard';
     $this->set('title_for_layout', 'Admin Panel'); 
     $this->loadModel('User');
     //For Line Chart 
      $result = $this->User->query("SELECT count(User.id) as total,Month(User.created) as month  FROM `users` AS `User` WHERE User.user_type_id = '1' AND Year(User.created) = Year(CURRENT_TIMESTAMP) GROUP BY Month( User.created )");
      $arr=array();
      $month=array();
      foreach($result as $res):      
        $month[$res[0]['month']]=$res[0]['total'];      
      endforeach;      
      for($i=0;$i<=12;$i++){
        if(array_key_exists($i,$month)){
          $arr[$i] = $month[$i];      
        }else{
          $arr[$i]='0';      
        }      
      }
      $this->set('final_value',$arr);    
 }                                                         
      
 /*
*************************************************************************
*Function Name		 :	admin_account
*Functionality		 :	Admin setting.
*************************************************************************
*/ 

function admin_account() {

		$this->layout='admin';

		if(empty($this->data)){
			$adminData=$this->Admin->find('first',array('conditions'=>array('Admin.id'=>'1')));
			$adminData['Admin']['pwd']='';
			$this->data=$adminData;
				
		$userTypes=$this->Admin->find("list",array("conditions"=>array("id ="=>1)));
		$this->set('userTypes',$userTypes);
		} 
    else if(!empty($this->data))
		{			
		$userTypes=$this->Admin->find("list",array("conditions"=>array("id ="=>1)));
		
		  	$this->Admin->set($this->data);
  		  if($this->Admin->validates()){				
			  //$this->data['Admin']['password'] = md5($this->data['Admin']['pwd']);
				$this->Admin->save($this->data);				
				$this->Session->setFlash('Admin account has been updated.','success');
    		$this->redirect(array('action' => 'account'));
    		
			}
		}
}    
  
/*
*************************************************************************
*Function Name		 :	admin_changepasswd
*Functionality		 :	use to change password of Admin Panel.
*************************************************************************
*/ 

function admin_changepasswd(){

	 	$this->layout='admin';
	if(isset($this->data)){
		$adminses=$this->Session->read('Admin');	
		$admindata=$this->Admin->find('first',array("conditions"=>array("username"=>$adminses['Admin']['username'],'Admin.pwd'=> md5($this->data['Admin']['oldpwd']))));
		$this->Admin->set($this->data);
		if(empty($admindata)){
			$this->Session->setFlash('Please enter correct old password','message');
			$this->redirect(array('controller'=>'admins','action'=>'changepasswd'));
		}else{
			if($this->Admin->validates()){
			$this->data['Admin']['id']=$adminses['Admin']['id'];
			$this->data['Admin']['pwd']=md5($this->data['Admin']['pwd']);
			  if($this->Admin->save($this->data,false)){
				$this->redirect(array('controller'=>'admins','action'=>'index'));
			  }    
			}
		}	
     
		$this->set('title_for_layout', 'Admin panel - Change password'); 
	}
}

/*
*************************************************************************
*Function Name		 :	admin_changeSpPassword
*Functionality		 :	use to change password of Sp by admin.
*************************************************************************
*/ 

function admin_changeSpPassword($id=null){
		$this->set('title_for_layout', 'Admin panel - Change Service Provider Password'); 
	 	$this->layout='';
		$this->loadModel('User');
		$id=base64_decode($id);
		$this->set('id',$id);
		if(isset($this->data) && !empty($this->data)){	
				$this->data['User']['id']=$this->data['User']['id'];		
				$this->data['User']['password']=md5($this->data['User']['password']);
				  if($this->User->save($this->data,false)){
					$this->Session->setFlash('SP password changed successfully.','success');
					$this->redirect(array('controller'=>'admins','action'=>'serviceProviderList'));
				  }    
				
		}  
		
	}

/*
*************************************************************************
*Function Name		 :	admin_logout
*Functionality		 :	Logout from Admin Panel.
*************************************************************************
*/ 
	function admin_logout(){
		$this->Session->delete('Admin');
		$this->redirect(array('controller'=>'admins','action'=>'login'));
	}  
	
#############################################################
# Functions For management of COde Chart
#############################################################

/*
*************************************************************************
*Function Name		 :	admin_codeListing
*Functionality		 :	Functionality for listing of codes for various reports
*************************************************************************
*/

  function admin_codeListing(){
	$this->loadModel('Code');
	$this->loadModel('Report');
	
	if(isset($_POST['active'])){
	   	$this->admin_active();
	}
	    
	if(isset($_POST['inactive'])){
	   	$this->admin_inactive();
	}
	
	$data = $this->Report->find('list', array(
				        'fields' => array('Report.id', 'Report.name'),				        
				        'recursive' => 0
				    ));				 
	$this->set('options',$data); 
     if(isset($this->data) && !empty($this->data)){
	$where = "Code.report_id = '".$this->data['Code']['report_id']."'";
	$this->paginate = array(        
				'conditions'=>array($where),
				'limit' => 50,
				'order' => array('Code.id' => 'desc')
	);
	$codeListing = $this->paginate('Code',false);
	$this->set('codeListing',$codeListing);
	$this->set('reportid',$this->data['Code']['report_id']);
     }else if(isset($this->params['named']['rid']) && !empty($this->params['named']['rid']))
     {
	$where = "Code.report_id = '".$this->params['named']['rid']."'";
	$this->paginate = array(        
				'conditions'=>array($where),
				'limit' => 10,
				'order' => array('Code.id' => 'desc')
	);
	$codeListing = $this->paginate('Code',false);
	$this->set('codeListing',$codeListing);
	$this->set('reportid',$this->params['named']['rid']);
     }else if(isset($_POST['viewall']) && ($this->params['form']['viewall'] == "View All")){
	$this->paginate = array(
				'limit' => 200,
				'order' => array('Code.id' => 'desc')
	);
	$this->set('codeListing',$this->paginate('Code',false));
	$this->set('viewall','allData');
     }else{
	$this->set('codeListing',$this->paginate('Code'));
     }
}

/***************************************************************************************
 #function to activate defiicienciy codes		
****************************************************************************************/
    function admin_active(){
      for($i=0; $i<count($_POST['box']); $i++){    		
    		$this->Code->id = $_POST['box'][$i];    		
		    $data['Code']['status'] = 1;		    
		    $this->Code->save($data,array('validate'=>false));        	
    	}
      	
     		$this->Session->setFlash('Deficiency has been activated successfully','success');	
	   	  $this->redirect(array("controller" => "admins", "action" => "codeListing"));
    }
    
/***************************************************************************************
 #function to Inactivate deficiency codes
****************************************************************************************/ 
    		
    function admin_inactive(){
      
      for($i=0; $i<count($_POST['box']); $i++){    		
    	   $this->Code->id = $_POST['box'][$i];
		     $data['Code']['status'] = 0;
		     $this->Code->save($data,array('validate'=>false));        	
    	}
    	
    		 $this->Session->setFlash('Deficiency has been inactivated successfully','success');	
	       $this->redirect(array("controller" => "admins", "action" => "codeListing"));
    }
    
/*
 
 
 
 
 
/*
*************************************************************************
*Function Name		 :	admin_serviceProviderList
*Functionality		 :	Functionality for listing of service provider
*Created                 :      Amit Kumar
*************************************************************************
*/

  function admin_serviceProviderList(){
	$this->loadModel('User');
	 $this->User->recursive = 2; 
		$condition['User.user_type_id']=1;			
			$this->paginate = array(				
				'conditions'=>array($condition),
				'fields'=>array('id','fname','lname','phone','profilepic','user_type_id','deactivate','email'),
				'limit' => 50,
				'order' => array('User.id' => 'desc')
	);				
	
	$providerListing = $this->paginate('User',false);
	//pr($providerListing); //die;
	 		
	$this->set('providerListing',$providerListing);
	if(isset($_POST['active'])){
	   	 $this->admin_sp_active();
	}
	    
	if(isset($_POST['inactive'])){
	   	 $this->admin_sp_inactive();
	}
}
/***************************************************************************************
 #function to activate website pages		
****************************************************************************************/
    function admin_sp_active(){
      for($i=0; $i<count($_POST['box']); $i++){    		
    		$this->User->id = $_POST['box'][$i];    		
		    $data['User']['deactivate'] = 1;		    
		    $this->User->save($data,array('validate'=>false));        	
    	}
     	  $this->Session->setFlash('Service provider has been activated successfully','success');	
	   	  $this->redirect(array("controller" => "admins", "action" => "serviceProviderList"));
    }
    
/***************************************************************************************
 #function to Inactivate website pages
****************************************************************************************/ 
    function admin_sp_inactive(){
      for($i=0; $i<count($_POST['box']); $i++){    		
    	   $this->User->id = $_POST['box'][$i];
		     $data['User']['deactivate'] = 0;
		     $this->User->save($data,array('validate'=>false));        	
    	}
    	   $this->Session->setFlash('Service provider has been inactivated successfully','success');	
	       $this->redirect(array("controller" => "admins", "action" => "serviceProviderList"));
    }

	/*
	*************************************************************************
	*Function Name		 :	admin_clientList
	*Functionality		 :	Functionality for listing of direct client
	*Created                 :      Vishal Yadav
	*************************************************************************
	*/
	function admin_clientList()
	{
		$this->loadModel('User');
		 $this->User->recursive = -1;					
			$this->paginate = array(				
				'conditions'=>array('User.user_type_id'=>3,'User.direct_client'=>'Yes'),
				'limit' => 50,
				'order' => array('User.id' => 'desc')
			);				
		
		$clientListing = $this->paginate('User',false);
		$this->set('clientListing',$clientListing);
	}
	/*
	*************************************************************************
	*Function Name		 :	admin_clientDescription
	*Functionality		 :	Functionality for detail of direct client
	*Created                 :      Vishal Yadav
	*************************************************************************
	*/
	function admin_clientDescription($id=null){	
	    configure::write('debug',2);
	    $this->loadModel('User');
	    $cid = base64_decode($id);     
	    $data= $this->User->find('first',array('conditions'=>array('User.id'=>$cid)));
	    $this->set('clientData',$data);
	    $this->set('businessType',Configure::read('businessType'));
	    $this->set('reportType',Configure::read('reportType'));
	}

	/*
	*************************************************************************
	*Function Name		 :	admin_addCode
	*Functionality		 :	Functionality to add code and description for various reports
	*************************************************************************
	*/

	function admin_addCode(){
	  $this->loadModel('Report');
	  $this->loadModel('Code');
	  $this->loadModel('CodeType');
		$data = $this->Report->find('list', array(
					      'fields' => array('Report.id', 'Report.name'),				        
					      'recursive' => 0
					  ));
	  $codetype	=		$this->CodeType->find('all');	
	  $this->set('codetype',$codetype);  
		$this->set('options',$data); 	
		if(!empty($this->data)) {     	 
		  if($this->Code->set($this->data) && $this->Code->validates()) {
		    if($this->Code->save($this->data,false)){
		      $this->Session->setFlash('Code information added successfully','success' ); 
					$this->redirect(array('controller'=>'admins','action'=>'codeListing'));
				}   
		  }
	       }	
	}
	/*
	*************************************************************************
	*Function Name		 :	admin_codeListing
	*Functionality		 :	Functionality for listing of codes for various reports
	*************************************************************************
	*/

	function admin_editCode($id = null){  
	  $this->loadModel('Code');    
	   $this->loadModel('CodeType'); 	
		if(!empty($this->data)) {         	 
		  if($this->Code->set($this->data) && $this->Code->validates()) {
		    if($this->Code->save($this->data,false)){
		      $this->Session->setFlash('Code information updated successfully','success' ); 
					$this->redirect(array('controller'=>'admins','action'=>'codeListing'));
				}   
		  }
	       }else{
	    $this->loadModel('Report'); 
		$data = $this->Report->find('list', array(
					      'fields' => array('Report.id', 'Report.name'),				        
					      'recursive' => 0
					  ));
		      $this->set('options',$data); 
		      $codetype	=		$this->CodeType->find('all');	
	  $this->set('codetype',$codetype); 
		      $cid = base64_decode($id);  		
	    $this->Code->id = $cid;
	    $this->data = $this->Code->read();
	   
	 }	
	}
	/*
	*************************************************************************
	*Function Name		 :	admin_deleteCode
	*Functionality		 :	Functionality for deleting  codes
	*************************************************************************
	*/  
		function admin_deleteCode($id=null){
			$this->loadModel('Code');
			$this->loadModel('Deficiency');
			$this->Code->delete($id);
			$this->Deficiency->deleteAll(array('Deficiency.code_id' => $id));
			$this->Session->setFlash('Code has been deleted Successfully');	
		    $this->redirect(array("controller" => "admins", "action" => "codeListing")); 
		}
	/*
	*************************************************************************
	*Function Name		 :	admin_codeListing
	*Functionality		 :	Functionality for listing of codes for various reports
	*************************************************************************
	*/

	function admin_codeDescription($id = null){
	 configure::write('debug',2);
	  $this->loadModel('Code');
	  $cid = base64_decode($id);  		
	  $this->Code->id = $cid;
	  $data= $this->Code->find('first',array('conditions'=>array('Code.id'=>$cid)));
	  $this->set('codeListing',$data);
      }


	/*
	*************************************************************************
	*Function Name		 :	admin_providerDescription
	*Functionality		 :	Functionality for deatil  of service provider user 
	*************************************************************************
	*/

	function admin_providerDescription($id = null){
	      configure::write('debug',2);
	      $this->loadModel('User');
	      $sid = base64_decode($id);  		
	       
	      $data = $this->User->find('first',array('conditions'=>array('User.id'=>$sid),
						      'order'=>'User.id DESC'
						      ,'recursive'=>2));
	      // get fire inpector profile data
	      $inspection = $this->User->find('all', array('contain'=>false,
							  'conditions' =>array('User.user_type_id' =>2,'User.sp_id'=>$sid),
							  'fields'=>array('id','user_type_id','sp_id','fname','lname','qualification','phone','profilepic','email','report_id'),
							  'order'=>'User.id DESC'));
	      
	      // get client profile data
	      $client_profile_data = $this->User->find('all', array('contain'=>false,
							  'conditions' =>array('User.user_type_id' =>3,'User.sp_id'=>$sid,'User.direct_client'=>'No'),
							  'fields'=>array('id','user_type_id','sp_id','fname','lname','phone','profilepic','email','site_name','site_email','client_company_name'),
							  'order'=>'User.id DESC'));
	      
	      $this->set('client_profile',$client_profile_data);
	      $this->set('inspection',$inspection);
	      $this->set('providerListing',$data);
      }
	/*
	*************************************************************************
	*Function Name		 :	admin_sendSpInfo
	*Functionality		 :	Functionality for send sp deatil to direct client 
	*************************************************************************
	*/
	function admin_sendSpInfo($client_id=null){
		$this->loadModel('User');
		$this->layout='';
		$client_id=base64_decode($client_id);
		$this->set('clientId',$client_id); 			  
		$data=$this->User->find('first',array('conditions'=>array('User.id'=>$client_id),'fields'=>'email'));
		$this->set('data',$data);
		if(isset($this->data) && !empty($this->data)){
			//pr($this->data); die;
			$to = $this->data['Message']['message_to'];
			$subject = $this->data['Message']['message_body'] ;
			$message = "<table cellspacing='0' cellpadding='0' border='0' width='620'>
						<tr>
						<td style='padding: 4px 8px; background: rgb(59, 89, 152) none repeat scroll 0% 0%; -mz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; color: rgb(255, 255, 255); font-weight: bold; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; vertical-align: middle; font-size: 16px; letter-spacing: -0.03em; text-align: left;'>
						<p>" .$this->data['Message']['message_body']." </p>
						</td>
					  
				  </table>";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= 'From: ReportOnlinePlus <'.EMAIL_FROM.'>' . "\r\n" ;
		  
		 mail($to, $subject, $message, $headers);   
		$this->Session->setFlash('Email Sent Successfully');	
		$this->redirect(array("controller" => "admins", "action" => "clientList")); 
		
		}
	}
	/*
	*************************************************************************
	*Function Name		 :	admin_fwdLeadToSp
	*Functionality		 :	Functionality for send direct client detail tp SP
	*************************************************************************
	*/	
	function admin_fwdLeadToSp($client_id=null){
		$this->loadModel('User');
		$this->loadModel('Lead');
		$this->layout='';
		$client_id=base64_decode($client_id);
		$this->set('clientId',$client_id);
		$leadData=$this->Lead->find('all',array('conditions'=>array('Lead.client_id'=>$client_id)));
		$this->set('leadData',$leadData);
		if(isset($this->data) && !empty($this->data)){
			if($this->Lead->save($this->data,false)){
				# fetch SP email
				$data=$this->User->find('first',array('conditions'=>array('User.id'=>$this->data['Lead']['sp_id']),'fields'=>'fname,email'));				
				 //CODE TO FIRE AN EMAIL	
                $this->loadmodel('EmailTemplate');  		       
                $register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>3)));
                $link = "<a href=" . $this->selfURL() . "/homes/login>Click Here</a>";                  
                $register_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#CLICK_HERE'),array($this->selfURL() ,@$data['User']['fname'],@$link),$register_temp['EmailTemplate']['mail_body']);                  
                $this->Email->from = EMAIL_FROM;
                $this->Email->to = @$data['User']['email'];
                $this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
                $this->Email->template = 'register_mail_by_admin';
                $this->Email->sendAs = 'both'; // because we like to send pretty mail
                $this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);
				//echo $register_temp['EmailTemplate']['mail_body']; die;
                $this->Email->send(); //Do not pass any args to send()                  
                $this->Session->setFlash('information forwarded sucessfully to SP','success' ); 
      			$this->redirect(array('controller'=>'admins','action'=>'clientList'));
      			  }  
		}
	}
/*
*************************************************************************
*Function Name		 :	admin_tutorial
*Functionality		 :	Functionality to list tutorial(video,screenshots,tutorial text)
*************************************************************************
*/
	function admin_tutorial(){
		$this->loadModel('TutorialScreenshot');
		$this->loadModel('TutorialText');
		$this->loadModel('TutorialVideo');
		$snapshotData =  $this->TutorialScreenshot->find('all',array('order'=>'TutorialScreenshot.id DESC'));
		$videoData = $this->TutorialVideo->find('all',array('order'=>'TutorialVideo.id DESC'));
		$textData=$this->TutorialText->find('all',array('order'=>'TutorialText.id DESC'));
		$this->set('snapshotData',$snapshotData);
		$this->set('videoData',$videoData);
		$this->set('textData',$textData);
	}
	
	function admin_addTutorial(){
		$this->loadModel('TutorialText');
		if(isset($this->data) && !empty($this->data)){
			if($this->TutorialText->save($this->data,false)){
				$this->Session->setFlash('Tutorial text saved sucessfully','success' ); 
      			$this->redirect(array('controller'=>'admins','action'=>'tutorial'));	
			}
		}
		
	}
	function admin_editTutorial(){
		$this->loadModel('TutorialText');	
		if(empty($this->data)){ 
			$this->TutorialText->id = 5;
			$this->data=$this->TutorialText->read();		
		}else{
		
		  if($this->TutorialText->save($this->data['TutorialText'])){ 
			  echo $this->Session->setFlash('Tutorial Updated Sucessfully','success');    			 
			  $this->redirect(array("controller" => "admins", "action" => "tutorial"));
			} 
		  
		 }
		
	}
	function admin_addVideo(){
		$this->loadModel('TutorialVideo');
		set_time_limit(0);
		set_time_limit(0);
ini_set('max_input_time',600);
ini_set('max_execution_time', 1200); 
ini_set('memory_limit', '528M'); 
ini_set('max_input_time', 600);
ini_set('post_max_size', '256M');
		if(isset($this->data) && !empty($this->data)){
			//pr($this->data); die;
			if(isset($this->data['TutorialVideo']['video']['name'])){
				$file_ext = explode('.',$this->data['TutorialVideo']['video']['name']);
				$file_ext = strtolower($file_ext[1]);
				//echo $file_ext; die;
				if($file_ext=='mov' || $file_ext=='avi' || $file_ext=='wmv' || $file_ext=='dat' || $file_ext=='mpeg' || $file_ext=='mpg' || $file_ext=='flv' || $file_ext=='mp4' || $file_ext=='mp2'){
					$this->Uploader->_data = $this->data['TutorialVideo'];
					if($this->data){
						//echo WWW_ROOT.'video/file/'; die;
						$this->data['TutorialVideo']['video_size'] = $this->data['TutorialVideo']['video']['size'];   
						$this->data['TutorialVideo']['video']='';
						//pr($this->TextCoaching);die;
						$this->TutorialVideo->set($this->data);		
						//pr($this->data); die;
						if($this->TutorialVideo->save($this->data, array('validate'=>false))){
							$video_id=$this->TutorialVideo->getLastInsertId();
							$path_video=WWW_ROOT."video/file/";
							$path_thumb=WWW_ROOT."video/thumb/";
							
							$vidName = "Video_".$video_id."_".rand(0,999999).".".$file_ext; 
							$filename_video=$path_video.$vidName;							
							$this->Uploader->uploadDir = "/video/file/";
							
	
							//$duration = $this->VideoEncoder->get_duration($filename_video);
							//$filesize = $this->VideoEncoder->get_filesize($filename_video);
							
							$video_data =  $this->Uploader->upload('video', array('name'=>$vidName,'overwrite' => false,'resize' => true));
							
							#calculate the duration                       
							$thumbName = "Video_".$video_id."_".rand(0,999999).".jpg";
							$filename_thumb=$path_thumb.$thumbName;
							
							//pr($video_data); die;
							$this->VideoEncoder->grab_image($filename_video, $filename_thumb);
							
							/*--- Calculate Video file duration start -----------   */
							$bitrate = 128; //in kbps
							$duration = $this->VideoEncoder->get_duration($filename_video,$bitrate);
							/*--- Calculate video file duration End -----------   */
							
							$this->data['TutorialVideo']['id'] = $video_id;
							$this->data['TutorialVideo']['video']='/video/file/'.$vidName;
							$this->data['TutorialVideo']['video_duration']=$duration;
							//$this->data['Video']['filesize']=$filesize;
							
							$this->data['TutorialVideo']['video_thumb'] = '/video/thumb/'.$thumbName;
							$this->TutorialVideo->save($this->data, array('validate'=>false));
							
							$this->Session->setFlash('Video has been saved successfully.');
							$this->redirect('tutorial');
						}
					}else{
					//pr($this->TextCoaching->validationErrors);die;
					//$this->set('errors', $this->Video->validationErrors);
					}
				}else{
					$this->Session->setFlash('Please upload a video file. Provided format is not supported.');
					$this->Session->write('sessionMsg','Please upload a video file. Provided format is not supported.');
					$this->redirect('tutorial');
					
				}	
			}
		}
		
	}
	function admin_addScreenshot(){
		$this->loadModel('TutorialScreenshot');
		if(isset($this->data) && !empty($this->data)){			
			if(isset($this->data['TutorialScreenshot']['image']['name'])){  
				$this->Uploader->uploadDir = SNAPSHOTTEMP;				
				$temp_exp = explode('.',$this->data['TutorialScreenshot']['image']['name']);             
				$filenametosave= uniqid('IMG').".".$temp_exp[1];
				$this->Uploader->_data = $this->data['TutorialScreenshot'];
				$up = $this->Uploader->upload('image', array('name'=>$filenametosave,'overwrite' => false));
				
				if($up){		  
				//$resized_path =$this->Uploader->crop(array('width' => BANNERIMGW, 'height' => BANNERIMGH, 'append' => 'cropped', 'location' => UploaderComponent::LOC_TOP),true);
				$this->data['TutorialScreenshot']['image'] = $up['name'];
					if($this->TutorialScreenshot->save($this->data['TutorialScreenshot'])){
					$this->Session->setFlash('Screenshot has been added successfully','success');
						  $this->Redirect(array('action' => 'tutorial')); 
					}
				}
			}
		}
		
	}
	
	function deleteImage($id=null){
		$this->layout = null;
		$this->loadModel('TutorialScreenshot');
		$data = $this->TutorialScreenshot->find('first',array('conditions'=>array('TutorialScreenshot.id'=>$id)));
		if(file_exists(WWW_ROOT."img/snapshots/".$data['TutorialScreenshot']['image'])){
		 
		  unlink(WWW_ROOT."img/snapshots/".$data['TutorialScreenshot']['image']); 
			
		}
		if($this->TutorialScreenshot->delete($id)){			
					echo "1";
				exit;	
		}	
		
	}
	
	function admin_delete_video($id=null)
	{
		$this->loadModel('TutorialVideo');
		$data = $this->TutorialVideo->find('first',array('conditions'=>array('TutorialVideo.id'=>$id),'recursive'=>1));
		
		
		if($this->TutorialVideo->delete($id))
		{
			$thumb=str_replace('/video/thumb/','',$data['TutorialVideo']['video_thumb']);
			if(file_exists(WWW_ROOT."video/thumb/".$thumb)) {
			unlink(WWW_ROOT."video/thumb/".$thumb);
			}
			$file=str_replace('/video/file/','',$data['TutorialVideo']['video']);
			if(file_exists(WWW_ROOT."video/file/".$file)) {
			unlink(WWW_ROOT."video/file/".$file);
			}	
			
			$this->Session->setFlash('Video has been deleted successfully','success');
			$this->redirect(array("controller" => "admins", "action" => "tutorial"));
		}
	}
	
	
	
	function deletetxt($id=null){
		$this->layout = null;
		$this->loadModel('TutorialText');
		$data = $this->TutorialText->find('first',array('conditions'=>array('TutorialText.id'=>$id)));
		
		if($this->TutorialText->delete($id)){			
					echo "1";
				exit;	
		}	
		
	}
	
	function admin_SpDelete($id = null)
	{
		$this->loadModel('User');
		$id = base64_decode($id);
		$this->User->id = $id;
		if($this->User->delete())
		{
			$this->Session->setFlash(__('SP has been deleted successfully.',true));
			$this->redirect(array('controller'=>'admins','action'=>'serviceProviderList'));
		}
		
	}
	function admin_resendSpCredential($id = null){
		$this->loadModel('User');
		$id = base64_decode($id);
		$this->User->id = $id;		
		$result = $this->User->read();
		$password = substr(md5(time()), 0, 6); // to generate a password of 6 characters
		$encpt_newpassword = md5($password);		
		$data['User']['password']= $encpt_newpassword;
	  if($this->User->save($data['User'],false)){	
		$this->loadmodel('EmailTemplate');
		$register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>4)));
		//$link = "<a href=" . $this->selfURL() . "/users/confirmation/".$activationnumber.">Click here</a>";
		$link = "<a href=" . $this->selfURL() . "/homes/splogin>Click here</a>";
		$register_temp['EmailTemplate']['mail_body']=str_replace(array('#FIRSTNAME','#USERNAME','#PASSWORD','#CLICK_HERE'),array($result['User']['fname'],$result['User']['username'],$password,$link),$register_temp['EmailTemplate']['mail_body']);
		//echo $register_temp['EmailTemplate']['mail_body']; die;
		$this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);
		$this->Email->to = $result['User']['email'];
		$this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'register_mail'; // note no '.ctp'
		$this->Email->sendAs = 'html'; // because we like to send pretty mail		
		$this->Email->send(); //Do not pass any args to send()
		$this->Session->setFlash(__('SP credential has been sent successfully.',true));
		$this->redirect(array('controller'=>'admins','action'=>'serviceProviderList'));
		
	}
	}
#### FUNCTION ADD NEW INSPECTION DEVICE FOR FIRE ALARM REPORT	
	function admin_addInspectionDevice(){
		$this->loadModel('Service');
		$this->loadModel('Report');
		$data = $this->Report->find('list', array(
				        'fields' => array('Report.id', 'Report.name'),				        
				        'recursive' => 0
				    ));
		$this->set('options',$data);
		if(!empty($this->data) && isset($this->data)){			
			if($this->Service->save($this->data)){
				$this->Session->setFlash(__('New device has been saved successfully.',true));
				$this->redirect(array('controller'=>'admins','action'=>'inspectionDeviceList'));
			}
		}
		
	}
	function admin_editInspectionDevice($id=null){
		$this->loadModel('Service');
		$id=base64_decode($id);
		$this->set('id',$id);
		
		if(empty($this->data)){
			$this->loadModel('Report');
			$data = $this->Report->find('list', array(
						'fields' => array('Report.id', 'Report.name'),				        
						'recursive' => 0
					    ));
			$this->set('options',$data);
			$this->Service->id = $id;
			$this->data=$this->Service->read();		
		}else{ 
		  if($this->Service->save($this->data['Service'])){ 
			  echo $this->Session->setFlash('Device has been updated sucessfully','success');    			 
			  $this->redirect(array("controller" => "admins", "action" => "inspectionDeviceList"));
			}
		 }
	}
	
	function admin_inspectionDeviceList(){
		$this->loadModel('Service');
		$this->loadModel('Report');					
		if(isset($_POST['active'])){
	   	 $this->admin_activateDevice();
		}
	    
		if(isset($_POST['inactive'])){
	   	 $this->admin_inactivateDevice();
		}
		
		$data = $this->Report->find('list', array(
				        'fields' => array('Report.id', 'Report.name'),				        
				        'recursive' => 0
				    ));				 
		$this->set('options',$data);
		$reportId = ((isset($this->params['url']['reportId']) && !empty($this->params['url']['reportId']))?$this->params['url']['reportId']:'');
		$this->set('selVal',$reportId);
		if(!empty($reportId)){
		   $where = "Service.report_id = '".$reportId."'";
		   $this->paginate = array(        
					   'conditions'=>array($where),					   
					   'order' => array('Service.name')
		   );
		   $this->set('deviceList',$this->paginate('Service'));
		   
		}else{
		   $this->paginate =  array('order'=>'Service.name');        
		   $this->set('deviceList',$this->paginate('Service'));
		}
	}
/***************************************************************************************
 #function to activate  inspection device		
****************************************************************************************/
    function admin_activateDevice(){      
      $this->loadModel('Service');
      for($i=0; $i<count($_POST['box']); $i++){    		
    		$this->Service->id = $_POST['box'][$i];    		
		    $data['Service']['status'] = 1;		    
		    $this->Service->save($data,array('validate'=>false));        	
    	}
      	
     		$this->Session->setFlash('device has been activated successfully','success');	
	   	  $this->redirect(array("controller" => "admins", "action" => "inspectionDeviceList"));
    }
    
/***************************************************************************************
 #function to Inactivate inspection device
****************************************************************************************/ 
    		
    function admin_inactivateDevice(){
      $this->loadModel('Service');
      for($i=0; $i<count($_POST['box']); $i++){    		
    	   $this->Service->id = $_POST['box'][$i];
		     $data['Service']['status'] = 0;
		     $this->Service->save($data,array('validate'=>false));        	
    	}
    	
    		 $this->Session->setFlash('device has been inactivated successfully','success');	
	       $this->redirect(array("controller" => "admins", "action" => "inspectionDeviceList"));
    }
/***************************************************************************************
 #function to View sample Reports
****************************************************************************************/ 
    		
    function admin_viewSampleReport(){
		$this->loadModel('SampleReport');
		$data=$this->SampleReport->find('all');
		$this->set('data',$data);
    }
	
/***************************************************************************************
	#function to Add sample Reports
****************************************************************************************/ 
	 /*function admin_addreport(){
		$this->loadModel('SampleReport');
		if(!empty($this->data) && isset($this->data)){
			$countError = 0;
			//START OF FIREALARM REPORT
        if((isset($this->data['Report']['firealarm_report']['name'])) && (!empty($this->data['Report']['firealarm_report']['name']))){
		    $temp_exp = explode('.',$this->data['Report']['firealarm_report']['name']);
			// Start of file uploading script		
			$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
			if(in_array($temp_exp[1], $allowedExts)){
				$target = WWW_ROOT.SAMPLEREPORT;
				$filenametosave= uniqid('FIREALARM').".".$temp_exp[1];
				$target = $target.$filenametosave;
				if(move_uploaded_file($this->data['Report']['firealarm_report']['tmp_name'], $target)){
					$data['SampleReport'][0]['name'] = $this->data['Report']['firealarm_report']['rname'];
					$data['SampleReport'][0]['file_name'] = $filenametosave;
				} 
			}
			else{
					$countError = $countError+1;	
			}
			//End of file uploading script
		}
		//END OF FIREALARM REPORT
		//START OF SPRINKLER REPORT
		if(isset($this->data['Report']['sprinkler_report']['name']) && !empty($this->data['Report']['sprinkler_report']['name'])){
		    $temp_exp = explode('.',$this->data['Report']['sprinkler_report']['name']);
			// Start of file uploading script		
			$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
			if(in_array($temp_exp[1], $allowedExts)){
				$target = WWW_ROOT.SAMPLEREPORT;
				$filenametosave= uniqid('SPRINKLER').".".$temp_exp[1];
				$target = $target.$filenametosave;
				if(move_uploaded_file($this->data['Report']['sprinkler_report']['tmp_name'], $target)){
					$data['SampleReport'][1]['name'] = $this->data['Report']['sprinkler_report']['rname'];
					$data['SampleReport'][1]['file_name'] = $filenametosave;
				} 
			}
			else{
					$countError = $countError+1;
			}
			///End of file uploading script
		}
		//END OF SPRINKLER REPORT
		
		//START OF KITCHEN REPORT
		if(isset($this->data['Report']['kitchen_report']['name']) && !empty($this->data['Report']['kitchen_report']['name'])){
		    $temp_exp = explode('.',$this->data['Report']['kitchen_report']['name']);
			// Start of file uploading script		
			$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
			if(in_array($temp_exp[1], $allowedExts)){
				$target = WWW_ROOT.SAMPLEREPORT;
				$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
				$target = $target.$filenametosave;
				if(move_uploaded_file($this->data['Report']['kitchen_report']['tmp_name'], $target)){
					$data['SampleReport'][2]['name'] = $this->data['Report']['kitchen_report']['rname'];
					$data['SampleReport'][2]['file_name'] = $filenametosave;
				} 
			}
			else{
				$countError = $countError+1;
			}
			///End of file uploading script
		}
		//END OF KITCHEN REPORT
		
		
		//START OF EMERGENCY REPORT
		if(isset($this->data['Report']['emergency_report']['name']) && !empty($this->data['Report']['emergency_report']['name'])){
		    $temp_exp = explode('.',$this->data['Report']['emergency_report']['name']);
			// Start of file uploading script		
			$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
			if(in_array($temp_exp[1], $allowedExts)){
				$target = WWW_ROOT.SAMPLEREPORT;
				$filenametosave= uniqid('EMERGENCY').".".$temp_exp[1];
				$target = $target.$filenametosave;
				if(move_uploaded_file($this->data['Report']['emergency_report']['tmp_name'], $target)){
					$data['SampleReport'][3]['name'] = $this->data['Report']['emergency_report']['rname'];
					$data['SampleReport'][3]['file_name'] = $filenametosave;
				} 
			}
			else{
				$countError = $countError+1;
			}
			///End of file uploading script
		}
		//END OF EMERGENCY REPORT
		
		
		//START OF EXTINGUSHER REPORT
		if(isset($this->data['Report']['extinguishers_report']['name']) && !empty($this->data['Report']['extinguishers_report']['name'])){
		    $temp_exp = explode('.',$this->data['Report']['extinguishers_report']['name']);
			// Start of file uploading script		
			$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
			if(in_array($temp_exp[1], $allowedExts)){
				$target = WWW_ROOT.SAMPLEREPORT;
				$filenametosave= uniqid('EXTINGUSHER').".".$temp_exp[1];
				$target = $target.$filenametosave;
				if(move_uploaded_file($this->data['Report']['extinguishers_report']['tmp_name'], $target)){
					$data['SampleReport'][4]['name'] = $this->data['Report']['emergency_report']['rname'];
					$data['SampleReport'][4]['file_name'] = $filenametosave;
				} 
			}
			else{
				$countError = $countError+1;
			}
			///End of file uploading script
		}
		//END OF EXTINGUSHER REPORT
		
		
		//START OF EXTINGUSHER REPORT
		if(isset($this->data['Report']['specialhazard_report']['name']) && !empty($this->data['Report']['specialhazard_report']['name'])){
		    $temp_exp = explode('.',$this->data['Report']['specialhazard_report']['name']);
			// Start of file uploading script		
			$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
			if(in_array($temp_exp[1], $allowedExts)){
				$target = WWW_ROOT.SAMPLEREPORT;
				$filenametosave= uniqid('SPECIAL').".".$temp_exp[1];
				$target = $target.$filenametosave;
				if(move_uploaded_file($this->data['Report']['specialhazard_report']['tmp_name'], $target)){
					$data['SampleReport'][5]['name'] = $this->data['Report']['emergency_report']['rname'];
					$data['SampleReport'][5]['file_name'] = $filenametosave;
				} 
			}
			else{
				$countError = $countError+1;
			}
			///End of file uploading script
		}
		//END OF specialhazard REPORT
		if(!empty($data['SampleReport'])){
			foreach($data['SampleReport'] as $key=>$val){
				$datas['SampleReport']['name'] = $val['name'];
				$datas['SampleReport']['file_name'] = $val['file_name'];
				$this->SampleReport->create();
				$this->SampleReport->save($datas['SampleReport']);	
			}
			
		}
		if($countError>0){
			$this->Session->setFlash($countError.'Sample report is not uploaded successfully because of invalid extention','success');	
			$this->redirect(array("controller" => "admins", "action" => "addreport"));	
		}else{
			$this->Session->setFlash('Record has been saved succesfully','success');
			$this->redirect(array("controller" => "admins", "action" => "viewSampleReport"));	
		}
       }
	 }
	 */
/***************************************************************************************
	#function to Edit sample Reports
****************************************************************************************/ 
	 function admin_editreport($id = null){
		$this->loadModel('SampleReport');
		if(!empty($this->data) && isset($this->data)){
			 if($this->data['SampleReport']['file_name']['name'] != ""){
				$temp_exp = explode('.',$this->data['SampleReport']['file_name']['name']);
				// Start of file uploading script		
				$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
				if(in_array($temp_exp[1], $allowedExts)){
					$target = WWW_ROOT.SAMPLEREPORT;
					$filenametosave= uniqid('REPORT').".".$temp_exp[1];
					$target = $target.$filenametosave;
					if(move_uploaded_file($this->data['SampleReport']['file_name']['tmp_name'], $target)){
						$data['SampleReport']['file_name'] = $filenametosave;
					} 
				}
			 }else{
				$data['SampleReport']['file_name'] = $this->data['SampleReport']['old_file_name'];
			 }
				$data['SampleReport']['id'] = $this->data['SampleReport']['id'];
				$data['SampleReport']['name'] = $this->data['SampleReport']['name'];
				if($this->SampleReport->save($data['SampleReport'])){
					$this->Session->setFlash('Record has been updated succesfully','success');
					$this->redirect(array("controller" => "admins", "action" => "viewSampleReport"));	
				}
       }else{
		 $this->data = $this->SampleReport->find('first',array('conditions'=>array('SampleReport.id'=>$id)));
	   }
	 }
	 
	function admin_sprinklerinspection(){
		$this->layout=null;
	}
	function admin_kitchenhood(){
    	$this->layout=null;
		Configure::write('debug',0);

	}
	function admin_extinguishers(){
    	$this->layout=null;
		Configure::write('debug',0);

	}
	function admin_emergencyexit(){
    	$this->layout=null;
		Configure::write('debug',0);

	}
	function admin_specialhazard(){
    	$this->layout=null;
		Configure::write('debug',0);

	}
	function admin_firealarm(){
    	$this->layout=null;
		Configure::write('debug',0);
		$this->loadModel('FaAlarmDeviceType');
		$this->loadModel('FaAlarmDevicePlace');
		$device_type = $this->FaAlarmDeviceType->find('list');
		$device_place = $this->FaAlarmDevicePlace->find('list');
		$this->set('device_type',$device_type);
		$this->set('device_place',$device_place);
	}
/*
*************************************************************************
*Function Name		 :	admin_companyprincipals
*Functionality		 :	Functionality to list Compny principals
*************************************************************************
*/	
	 function admin_companyprincipals(){
		$this->layout='admin';
		$this->loadModel('UserCompanyPrincipal');
		$principal = $this->UserCompanyPrincipal->find('all');
		$this->set('principal',$principal);
	 }
	 /*
*************************************************************************
*Function Name		 :	admin_addcompanyprincipals
*Functionality		 :	Functionality to add Compny principals
*************************************************************************
*/	
	 function admin_addcompanyprincipals(){
		$this->layout='admin';
		$this->loadModel('UserCompanyPrincipal');
		 if(!empty($this->data)){
			 pr($this->data);
			$this->UserCompanyPrincipal->set($this->data['UserCompanyPrincipal']);
              if($this->UserCompanyPrincipal->validates()){
                 if($this->UserCompanyPrincipal->save($this->data['UserCompanyPrincipal'])) {
		   	    	$this->Session->setFlash('Company principal has been Added successfully','success');
					$this->redirect(array('action'=>'companyprincipals')); 
              }               	
			}
		}
	 }
	 
	function admin_editcompanyprincipal($id=null){
	   $this->layout='admin';
	   $this->loadModel('UserCompanyPrincipal');
     if(isset($this->data) && !empty($this->data)){
         $this->UserCompanyPrincipal->set($this->data);
		if($this->UserCompanyPrincipal->validates()){
		   	  //  $this->SubscriptionDuration->id=$this->data['SubscriptionDuration']['id'];
		   	if($this->UserCompanyPrincipal->save($this->data)){
		   	    $this->Session->setFlash('Company Principal has been updated successfully','success' );
			    $this->redirect(array('action'=>'companyprincipals')); 
              }               	
         } else {
 	            $this->Session->setFlash('Company Principal can not be updated.','message');              
         }
      }else {
         $this->data = $this->UserCompanyPrincipal->find("first",array("conditions"=>array("UserCompanyPrincipal.id"=>$id)));
      }
  }
  function admin_manageTrial(){
	  $this->loadModel('TrialOption');	
		if(isset($this->data) && !empty($this->data)){  		
		   
		   	if($this->TrialOption->save($this->data)){
		   	    $this->Session->setFlash('Status has been updated successfully','success' );
			    $this->redirect(array('controller'=>'admins','action'=>'index')); 
			}         	
		}else{
				$this->TrialOption->id = 1;
			$this->data=$this->TrialOption->read();	
		}
		
  }
  
  /**************************************************************************
        *Function Name : admin_home_video
        *Functionality : use to add single video in admin panel.
  **************************************************************************/ 
  
  function admin_home_video_list(){
		$this->loadModel('HomeVideo');	
		$videoData = $this->HomeVideo->find('all',array('order'=>'HomeVideo.id DESC'));
		
		$this->set('videoData',$videoData);
		
  }
  
  function admin_delete__home_video($id=null)
	{
		$this->loadModel('HomeVideo');
		$data = $this->HomeVideo->find('first',array('conditions'=>array('HomeVideo.id'=>$id),'recursive'=>1));
		
		
		if($this->HomeVideo->delete($id))
		{
			$thumb=str_replace('/video/thumb/','',$data['HomeVideo']['video_thumb']);
			if(file_exists(WWW_ROOT."video/thumb/".$thumb)) {
			unlink(WWW_ROOT."video/thumb/".$thumb);
			}
			$file=str_replace('/video/file/','',$data['HomeVideo']['video']);
			if(file_exists(WWW_ROOT."video/file/".$file)) {
			unlink(WWW_ROOT."video/file/".$file);
			}	
			
			$this->Session->setFlash('Video has been deleted successfully','success');
			$this->redirect(array("controller" => "admins", "action" => "home_video_list"));
		}
	}
  
  function admin_home_video(){
	$this->layout='admin';
	$this->loadModel('HomeVideo');	
	set_time_limit(0);
	set_time_limit(0);
ini_set('max_input_time',600);
ini_set('max_execution_time', 1200); 
ini_set('memory_limit', '528M'); 
ini_set('max_input_time', 600);
ini_set('post_max_size', '256M');
		if(isset($this->data) && !empty($this->data)){
			//pr($this->data); die;
			if(isset($this->data['HomeVideo']['video']['name'])){
				$file_ext = explode('.',$this->data['HomeVideo']['video']['name']);
				$file_ext = strtolower($file_ext[1]);
				//echo $file_ext; die;
				if($file_ext=='mov' || $file_ext=='avi' || $file_ext=='wmv' || $file_ext=='dat' || $file_ext=='mpeg' || $file_ext=='mpg' || $file_ext=='flv' || $file_ext=='mp4' || $file_ext=='mp2'){
					$this->Uploader->_data = $this->data['HomeVideo'];
					if($this->data){
						//echo WWW_ROOT.'video/file/'; die;
						$this->data['HomeVideo']['video_size'] = $this->data['HomeVideo']['video']['size'];   
						$this->data['HomeVideo']['video']='';
						//pr($this->TextCoaching);die;
						$this->HomeVideo->set($this->data);		
						//pr($this->data); die;
						if($this->HomeVideo->save($this->data, array('validate'=>false))){
							$video_id=$this->HomeVideo->getLastInsertId();
							$path_video=WWW_ROOT."video/file/";
							$path_thumb=WWW_ROOT."video/thumb/";
							
							$vidName = "Video_".$video_id."_".rand(0,9999999).".".$file_ext; 
							$filename_video=$path_video.$vidName;							
							$this->Uploader->uploadDir = "/video/file/";
							
	
							//$duration = $this->VideoEncoder->get_duration($filename_video);
							//$filesize = $this->VideoEncoder->get_filesize($filename_video);
							
							$video_data =  $this->Uploader->upload('video', array('name'=>$vidName,'overwrite' => false,'resize' => true));
							
							#calculate the duration                       
							$thumbName = "Video_".$video_id."_".rand(0,9999999).".jpg";
							$filename_thumb=$path_thumb.$thumbName;
							
							//pr($video_data); die;
							$this->VideoEncoder->grab_image($filename_video, $filename_thumb);
							
							/*--- Calculate Video file duration start -----------   */
							$bitrate = 128; //in kbps
							$duration = $this->VideoEncoder->get_duration($filename_video,$bitrate);
							/*--- Calculate video file duration End -----------   */
							
							$this->data['HomeVideo']['id'] = $video_id;
							$this->data['HomeVideo']['video']='/video/file/'.$vidName;
							$this->data['HomeVideo']['video_duration']=$duration;
							//$this->data['Video']['filesize']=$filesize;
							
							$this->data['HomeVideo']['video_thumb'] = '/video/thumb/'.$thumbName;
							$this->HomeVideo->save($this->data, array('validate'=>false));
							
							$this->Session->setFlash(__('New Home Video has been added successfully',true));
							$this->redirect(array('controller'=>'admins','action'=>'home_video_list'));
						}
					}else{
					//pr($this->TextCoaching->validationErrors);die;
					//$this->set('errors', $this->Video->validationErrors);
					}
				}else{
					$this->Session->setFlash('Please upload a video file. Provided format is not supported.');
					$this->Session->write('sessionMsg','Please upload a video file. Provided format is not supported.');
					$this->redirect('home_video');
					
				}	
			}
		}
	
	
	/*if(!empty($this->data)){
		$data['HomeVideo']['id'] = $this->data['HomeVideo']['id'];
		$data['HomeVideo']['embed_code'] = $this->data['HomeVideo']['embed_code'];
		if($this->HomeVideo->save($data['HomeVideo'],false)){
			$this->Session->setFlash(__('New Video has been added successfully',true));
			$this->redirect(array('controller'=>'admins','action'=>'index'));
		}	
	}*/
  }
  
  /**************************************************************************
        *Function Name : admin_view_testimonials
        *Functionality : use to view all testimonials in admin panel.
  **************************************************************************/
  
  function admin_view_testimonials(){
	$this->layout='admin';
	$this->loadModel('Testimonials');
	$testimonials = $this->Testimonials->find('all');
	$this->set('testimonials',$testimonials);
  }
  
  /**************************************************************************
        *Function Name : admin_add_testimonial
        *Functionality : use to add testimonial in admin panel.
  **************************************************************************/
  
  function admin_add_testimonial(){
	$this->layout='admin';
	App::Import('Model', 'Testimonials');
	$this->Testimonials = new Testimonials(); 
	
	if(!empty($this->data) && isset($this->data)){
		        $this->Testimonials->set($this->data);
		        if($this->Testimonials->validates()){
				if(isset($this->data['Testimonials']['image']['name']) && $this->data['Testimonials']['image']['name']!=''){
					$this->Uploader->uploadDir = TESTIMONIALIMG;
					$temp_exp = explode('.',$this->data['Testimonials']['image']['name']);
					$filenametosave= uniqid('IMG').".".$temp_exp[1];
					$this->Uploader->_data = $this->data['Testimonials'];
					$up = $this->Uploader->upload('image', array('name'=>$filenametosave,'overwrite' => false));
					if($up){		  
						$resized_path =$this->Uploader->crop(array('width' => TESTIMONIALIMGW, 'height' => TESTIMONIALIMGH, 'append' => 'cropped', 'location' => UploaderComponent::LOC_CENTER),true);
						$data['Testimonials']['image'] = $resized_path['name'];
						$data['Testimonials']['from'] = $this->data['Testimonials']['from'];
						$data['Testimonials']['body'] = $this->data['Testimonials']['body'];
						if($this->Testimonials->save($data['Testimonials'])){
						      $this->Session->setFlash('Testimonial has been added successfully','success');
						      $this->Redirect(array('action' => 'view_testimonials')); 
						}
						else{
						      $this->Session->setFlash('Testimonial can not be Addedd.','message');
						}
					}
				}
				
			}else{
				
			}
			
			
	}	
  }
  
  /**************************************************************************
        *Function Name : admin_edit_testimonial
        *Functionality : use to edit testimonial in admin panel.
  **************************************************************************/
  
  function admin_edit_testimonial($id=null){
	$this->layout='admin';
	$this->loadModel('Testimonials');
	$testimonial_obj = $this->Testimonials->find('all',array('conditions'=>array('Testimonials.id'=>$id)));
	$this->set('testimonial',$testimonial_obj);
	if(!empty($this->data) && isset($this->data)){
		if(isset($this->data['Testimonials']['image']['name']) && $this->data['Testimonials']['image']['name']!=''){
		}else{ unset($this->data['Testimonials']['image']);}	
		$this->Testimonials->set($this->data);
		if($this->Testimonials->validates()){
			if(isset($this->data['Testimonials']['image']['name']) && $this->data['Testimonials']['image']['name']!=''){
				$this->Uploader->uploadDir = TESTIMONIALIMG;
				$temp_exp = explode('.',$this->data['Testimonials']['image']['name']);
				$filenametosave= uniqid('IMG').".".$temp_exp[1];
				$this->Uploader->_data = $this->data['Testimonials'];
				$up = $this->Uploader->upload('image', array('name'=>$filenametosave,'overwrite' => false));
				if($up){		  
					$resized_path =$this->Uploader->crop(array('width' => TESTIMONIALIMGW, 'height' => TESTIMONIALIMGH, 'append' => 'cropped', 'location' => UploaderComponent::LOC_CENTER),true);
				}
				$data['Testimonials']['image'] = $resized_path['name'];
				$filePath = ROOT.DS.'img'.DS.'testimonial'.DS.$this->data['Testimonial']['prev_image'];
				if(file_exists($filePath)){
					unlink($filePath);
				}
			}else{	
				$data['Testimonials']['image'] = $this->data['Testimonial']['prev_image'];
			}	
			$data['Testimonials']['from'] = $this->data['Testimonials']['from'];
			$data['Testimonials']['body'] = $this->data['Testimonials']['body'];
			$data['Testimonials']['id'] = $this->data['Testimonials']['id'];
			if($this->Testimonials->save($data['Testimonials'])){
			      $this->Session->setFlash('Testimonial has been edited successfully','success');
			      $this->Redirect(array('action' => 'view_testimonials')); 
			}
			else{
				$this->Session->setFlash('Testimonial can not be updated.','message');
			}			
		}	
			
	}
  }
  
  /**************************************************************************
        *Function Name : admin_aboutus
        *Functionality : use to chnage the home page about us text
  **************************************************************************/
  
  function admin_aboutus(){
	$this->layout='admin';
	$this->loadModel('AboutusText');
	$aboutText = $this->AboutusText->find('first');
	$this->set('aboutText',$aboutText);
	if(!empty($this->data)){
		$data['AboutusText']['id'] = $this->data['AboutusText']['id'];
		$data['AboutusText']['desc'] = $this->data['AboutusText']['desc'];
		if($this->AboutusText->save($data['AboutusText'],false)){
			$this->Session->setFlash(__('Text has been updated successfully',true));
			$this->redirect(array('controller'=>'admins','action'=>'index'));
		}	
	}
  }
  /*
  ************************************************************************* 
  *Function Name		 :	admin_inspectionQuery
  *Functionality		 :	List of inspection query
  *************************************************************************
 */ 	

  function admin_inspectionQuery()
  {
    $this->layout='admin';
    $this->set('title_for_layout',"Manager Inspection Query");
    $this->loadModel('Inquiry');
    $this->set('listing',$this->paginate('Inquiry'));   
 }
 
 /*
  ************************************************************************* 
  *Function Name		 :	admin_deleteQuery
  *Functionality		 :	delete inspection query
  *************************************************************************
 */ 	

  function admin_deleteQuery($id = null)
  {
	if($id!=null){
		$this->loadModel('Inquiry');
		if($this->Inquiry->delete($id)) {
			$this->Session->setFlash('Inquiry deleted successfully  ','success' );
		} else {
			$this->Session->setFlash('Inquiry can not be deleted','message');
		}
	}
    
	$this->redirect(array('action'=>'inspectionQuery','admin'=>true));
 }
 
 /*
  ************************************************************************* 
  *Function Name		 :	admin_deleteQuery
  *Functionality		 :	delete inspection query
  *************************************************************************
 */ 	

  function admin_deleteTestimonial($id = null)
  {
	if($id!=null){
		$this->loadModel('Testimonials');
		$imgData = $this->Testimonials->findById($id,array('Testimonials.image'));
		if(file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'testimonial'.DS.$imgData['Testimonials']['image'])){
			unlink(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'testimonial'.DS.$imgData['Testimonials']['image']);
		}
		if($this->Testimonials->delete($id)) {
			$this->Session->setFlash('Testimonial deleted successfully  ','success' );
		} else {
			$this->Session->setFlash('Testimonial can not be deleted','message');
		}
	}
    
	$this->redirect(array('action'=>'view_testimonials','admin'=>true));
 }
 
 /**************************************************************************
        *Function Name : admin_limitText
        *Functionality : use to chnage the max site address limit text
  **************************************************************************/
  
  function admin_limitText(){
	$this->layout='admin';
	$this->loadModel('MaxLimitAlertText');
	$maxText = $this->MaxLimitAlertText->find('first');
	$this->set('maxText',$maxText);
	if(!empty($this->data)){
		$data['MaxLimitAlertText']['id'] = $this->data['MaxLimitAlertText']['id'];
		$data['MaxLimitAlertText']['desc'] = $this->data['MaxLimitAlertText']['desc'];
		if($this->MaxLimitAlertText->save($data['MaxLimitAlertText'],false)){
			$this->Session->setFlash(__('Text has been updated successfully',true));
			$this->redirect(array('controller'=>'admins','action'=>'index'));
		}	
	}
  }
  
  #made on 22 oct,2012 by vishal
  function admin_siteadd_activation_request()
  {
	$this->layout='admin';
	$this->loadModel('User');
	$this->loadModel('SpSiteAddress');
	$request = $this->SpSiteAddress->find('all',array('conditions'=>array('SpSiteAddress.activation_request'=>1,'SpSiteAddress.idle'=>1)));
	$this->set('request',$request);
  }
  
  function admin_siteaddr_accept_request($sp_site_address_request=null)
  {
	$this->layout='admin';
	$this->loadModel('User');
	$this->loadModel('SpSiteAddress');
	$this->SpSiteAddress->id=$sp_site_address_request;
	$data['SpSiteAddress']['idle']=0;
	$data['SpSiteAddress']['activation_request']=0;
	if($this->SpSiteAddress->save($data['SpSiteAddress'],false)){
			$this->Session->setFlash(__('Site Address has been activated again.',true));
			$this->redirect(array('controller'=>'admins','action'=>'siteadd_activation_request'));
		}	
  }
  
  function admin_sampleReports(){
	$this->layout='admin';
  }
  function admin_downloadReport($fileName){
	switch($fileName){
		case 'Firealarm':
			$reportName = 'ReportOnlinePlus_Firealarm_Report.pdf';
			break;
		case 'Sprinkler':
			$reportName = 'ReportOnlinePlus_Sprinkler_Report.pdf';
			break;
		case 'Kitchenhood':
			$reportName = 'ReportOnlinePlus_Kitchenhood_Report.pdf';
			break;
		case 'Emergencyexit_lights':
			$reportName = 'ReportOnlinePlus_EmergencyExit_Report.pdf';
			break;
		case 'Extinguisher':
			$reportName = 'ReportOnlinePlus_Extinguisher_Report.pdf';
			break;
		case 'Specialhazard':
			$reportName = 'ReportOnlinePlus_Specialhazard_Report.pdf';
			break;
		default:
			$reportName = '';
			break;
	}
	$mimeType = array('docx' => 'application/pdf');
	$getname =  explode(".",$reportName);
	$this->view = 'Media';
	$params = array(
	'id' => $reportName,
	'name' => $getname[0],
	'download' => true,
	'extension' => $getname[1], // must be lower case
	'mimeType' => $mimeType,
	//'path' => APP . 'files' . DS // don't forget terminal 'DS'
	'path' => WWW_ROOT."files/sample_reports/"
	);
	$this->set($params);
  }
  
}

