<?php ob_start();
class UsersController extends AppController {
	var $name = 'Users';
	var $uses = array('User','Company');
	var $components = array('Uploader.Uploader','Email','Session','Cookie','Paypal','RequestHandler','Captcha');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Captcha');
	
	
   public function beforeFilter() {
      parent :: beforeFilter();
      $this->Uploader->tempDir = TMP;
    }
    
/*
 Method captcha
 Description to create the captcha
 Created By Manish Kumar on March 04, 2013
*/
function captcha(){
    //For Unix/Linux Servers it works fine.
        /*$this->Captcha->configCaptcha(array(
         'pathType'=>2
         ));*/
    $this->Captcha->getCaptcha();
    
}
/* function end */


/*
*************************************************************************
*Function Name		 :	manager_index
*Functionality		 :	use dashboard.
*************************************************************************
*/   
  function manager_index() { 
    $this->layout='manager';
    $this->set('title_for_layout', 'Manager panel'); 
  }
  
/*
*************************************************************************
*Function Name		 :	manager_changepasswd
*Functionality		 :	use to change password of Manager section.
*************************************************************************
*/ 
	function manager_changepasswd() {	 
      	  $UserId=$this->Session->read('UserId');	
      	  if(isset($this->data)){
          $this->User->set($this->data);
          $this->User->validate=$this->User->validateChangePass;
	   if($this->User->validates()){
             $this->data['User']['id']=$UserId;
             $this->data['User']['pwd']=md5($this->data['User']['pwd']);
          	 if($this->User->save($this->data,array('validate'=>false))) {		
          		  $this->Session->setFlash('Password has been changed.','success');
              	$this->redirect(array('controller'=>'Users','action'=>'index'));
              }  
        }
         $this->data['User']['pwd']='';
         $this->data['User']['cpwd']='';
        }          
        $this->set('id', $UserId);
      	$this->set('title_for_layout', 'Change Password');   
  }
/*
*************************************************************************
*Function Name		 :	manager_logout
*Functionality		 :	Logout from Manager panel.
*************************************************************************
*/
function manager_logout() {
	$this->Session->destroy('User');
	$this->redirect('/homes/login');
}

/*
*************************************************************************
*Function Name		 :	Client registration,if client directly approch to report online plus
*Functionality		 :	Registration panel.
*************************************************************************
*/
   function clientsignup(){
      //$this->Session->destroy('Log');
		$this->layout='indexlayout';
		$this->set('title_for_layout', 'Report Online plus -- Client SignUp');
		$this->loadModel('Country');
		$this->loadModel('CodeType');
		$this->set('businessType',Configure::read('businessType'));
		$this->set('reportType',Configure::read('reportType'));
		$this->set('country',$this->getCountries());
		$this->set('state',$this->getState(233));
		$this->loadModel('MainTab');
		$tabData=$this->MainTab->find('first');
		$this->set('tabData',$tabData);
		$codetype = $this->CodeType->find('all');	
		$this->set('codetype',$codetype); 
		if(isset($this->data) && !empty($this->data)){
			if(!$this->Captcha->validateCaptcha()){
				$this->Session->setFlash('Please enter correct captcha');
				$this->redirect($this->referer());
			}
	        $password=$this->data['User']['password'];
	        $this->User->set($this->data['User']);
	        $this->Company->set($this->data['Company']);
	        if($this->User->validates()){
				$this->data['User']['password'] = md5($this->data['User']['password']);
				$this->data['User']['username'] = $this->data['User']['email'];
				$this->data['User']['site_phone']=$this->data['User']['site_phone1'].'-'.$this->data['User']['site_phone2'].'-'.$this->data['User']['site_phone3'];
				$this->data['User']['user_type_id'] = 3;
				$this->data['User']['deactivate'] = 1;
				$this->data['User']['direct_client'] = "Yes";
				$this->data['User']['subscription_under_trial'] = "No";
				$this->data['User']['ip_address']=$this->RequestHandler->getClientIp();
		  
				# code to save code types
				$codetype = array();
				foreach($this->data['User']['code_type_id'] as $codeKey=>$codeValue){
				 if($codeValue != "0"){
					       $codetype[] = $codeKey;
				   }
				}
	     
				$this->data['User']['code_type_id'] = implode(",",$codetype);
		  
				if($this->User->save($this->data['User'],false)){
					$this->data['Company']['user_id'] = $this->User->getLastInsertId();
					# code to save comma separated type_business
					$btype = array();
					foreach($this->data['Company']['type_business'] as $key=>$value){
						if($value != "0"){
							$btype[] = $key;
						}
					} 

					$this->data['Company']['type_business'] = implode(",",$btype);
					# code to save comma separated interest_service
		            $servicetype = array();
		            foreach($this->data['Company']['interest_service'] as $key1=>$value1){
		            	if($value1 != "0"){
				            $servicetype[] = $key1;
						}
		            }
			     	$this->data['Company']['interest_service'] = implode(",",$servicetype);
					if($this->Company->save($this->data['Company'],false)){
		                $this->loadmodel('EmailTemplate');
						$link = "<a href=" . $this->selfURL() . "/admin>Click here</a>"; 
		                $client_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>12))); 
		                $client_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENT_NAME','#LINK'),array($this->selfURL() ,$this->data['User']['fname'],$link),$client_temp['EmailTemplate']['mail_body']);
		                $this->set('client_notification_temp',$client_temp['EmailTemplate']['mail_body']);
		                $this->Email->to = ADMIN_EMAIL;
		                $this->Email->subject = $client_temp['EmailTemplate']['mail_subject'];
		                $this->Email->from = EMAIL_FROM;
		                $this->Email->template = 'client_notification_mail'; // note no '.ctp'
		                $this->Email->sendAs = 'both'; // because we like to send pretty mail
		                $this->Email->send();//Do not pass any args to send() */
		                
						$this->Session->setFlash(__('Thank you taking the time to create an account with ReportsOnline Plus. We will review your information and send your inquiry to one or more of our certified partners that are qualified to perform the work that you inputted.',true));
		                $this->redirect(array('controller'=>'homes','action'=>'clientlogin'));
		            }            
		        }        
		    }
	    }
 	}
   
   
   /************ function to check unique email address at registeration time ************************/
	
	function chkuser(){
		$this->loadModel('User');
		$this->render(false);	
		$this->layout = 'ajax';
		Configure::write('debug', 0);
		if(isset($_POST['user'])) {
			$username = mysql_real_escape_string($_POST['user']);
			$Data = $this->User->find('count',array('conditions'=>array('User.email'=>$username)));
			
			if($Data == 0){
			echo "0"; //username does not exist in database
			}
			else{
			echo "1"; //username exist in database
			}
		}
		exit();
	}
	
   
   
/*
*************************************************************************
*Function Name		 :	user registration
*Functionality		 :	Registration panel.
*************************************************************************
*/
  function signup(){

  	  $this->Session->destroy();
      $this->Session->destroy('Log');       
      $this->layout='indexlayout';
      $this->set('title_for_layout', 'Report Online plus -- SP Registration');
      $this->loadModel('Country');
      $this->set('country',$this->getCountries());
      $this->set('state',$this->getState(233));
      $this->User->set($this->data['User']);
      //$this->Company->set($this->data['Company']);
      $this->set('businessType',Configure::read('businessType'));
      $this->set('reportType',Configure::read('reportType'));
      $this->loadModel('MainTab');
      $tabData=$this->MainTab->find('first');
      $this->set('tabData',$tabData);
		$this->loadModel("Squestion");
	    $this->set('question', $this->Squestion->find('list', array('fields' => array('Squestion.name'))));
		


      if(!empty($this->data)){
	 /* code commented on 22 jan 2016 [we need simple registration only]
	 if($this->User->validates() && $this->Company->validates()){
		$this->data['User']['username'] = $this->data['User']['email'];
		$this->data['User']['user_type_id'] = 1;
		$this->data['User']['deactivate'] = 1;
		$this->data['User']['phone'] = $this->data['User']['phone1'].'-'.$this->data['User']['phone2'].'-'.$this->data['User']['phone3'];
		$this->data['User']['password'] = $this->data['User']['password'];
		//company data
		$this->data['Company']['type_business'] = implode(",",$this->data['Company']['type_business']);
		$this->data['Company']['interest_service'] = implode(",",$this->data['Company']['interest_service']);
		if(!empty($this->data['Company']['interest_service_other']) && (isset($this->data['Company']['interest_service_other']))){
				$this->data['Company']['interest_service_other'] = $this->data['Company']['interest_service_other'];
		}
		
		if($this->Session->write('tempData[0][user]', $this->data['User']) && $this->Session->write('tempData[0][company]', $this->data['Company'])){
			$this->redirect('/users/step1/');
		}
	 } */
	 
	 if($this->User->validates()){
		$data['User']['username'] = $this->data['User']['email'];
		$data['User']['user_type_id'] = 1;
		$data['User']['deactivate'] = 0;
		$data['User']['phone'] = $this->data['User']['phone1'].'-'.$this->data['User']['phone2'].'-'.$this->data['User']['phone3'];
		$data['User']['password'] = md5($this->data['User']['password']);
	    $activationnumber = md5(rand());
	    $data['User']['activationkey'] = $activationnumber;
	    $data['User']['fname'] = $this->data['User']['fname'];
	    $data['User']['lname'] = $this->data['User']['lname'];
	    $data['User']['address'] = $this->data['User']['address'];
	    $data['User']['country_id'] = $this->data['User']['country_id'];
	    $data['User']['state_id'] = $this->data['User']['state_id'];
	    $data['User']['city'] = $this->data['User']['city'];
	    $data['User']['zip'] = $this->data['User']['zip'];
	    //$data['User']['phone'] = $this->data['User']['phone'];
	    $data['User']['email'] = $this->data['User']['email'];
	    $data['User']['question_id'] = $this->data['User']['question_id'];
	    $data['User']['answer'] = $this->data['User']['answer'];
		
		
	    if($this->User->save($data['User'],false)){
	    
		$this->loadmodel('EmailTemplate');
			$register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>1)));
			$link = "<a href=" . $this->selfURL() . "/users/activate_account/".$this->User->getLastInsertId()."/".$activationnumber.">Click here</a>";
			$register_temp['EmailTemplate']['mail_body']=str_replace(array('#FIRSTNAME','#LINK'),array($data['User']['fname'],$link),$register_temp['EmailTemplate']['mail_body']);
			$this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);
			/* SMTP Options */
$this->Email->smtpOptions = array(
     'port'=>'587',
     'timeout'=>'30',
     'host' => 'smtp.mandrillapp.com',
     'username'=>'admin@reportsonlineplus.com',
     'password'=>'ZfppYXZd9HHZAfLFnCPDZg',
);
		
			/* Set delivery method */ 
			$this->Email->delivery = 'smtp';
			$this->Email->to = $data['User']['email'];
			$this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
			$this->Email->from = EMAIL_FROM;
			$this->Email->template = 'register_mail'; // note no '.ctp'
			$this->Email->sendAs = 'html'; // because we like to send pretty mail		
			if($this->Email->send()) //Do not pass any args to send()
			{
				$this->Session->destroy('tempDatas[user]');
				$this->Session->destroy('tempDatas[company]');
				$this->Session->destroy('tempDatas[UserCompanyPrincipalRecord]');
			}
			/* Check for SMTP errors. */ 
			$this->set('smtp_errors', $this->Email->smtpError);

			$this->Session->setFlash(__('Your account has been registered successfully,please check your mailbox to activate your account.',true));
			$this->redirect(array('controller'=>'users','action'=>'thankSp'));
		}
		
		
		
	 }
	 
	 
     }
  }
  
  function activate_account($user_id=null,$activate_link=null)
  {
	  $this->loadmodel('User');
	  $this->User->id = $user_id;
	  if($this->User->saveField('deactivate', '1', $validate = false));
	  {
		$this->Session->setFlash(__('Your account has been activated successfully',true));
	    $this->redirect(array('controller'=>'homes','action'=>'login'));  
	  } 
	  
  }
  
   function step1(){
	 //pr($_SESSION);
	 //die();
	  if(!isset($_SESSION['tempData[0][user]']) && empty($_SESSION['tempData[0][user]'])){
		$this->redirect('/users/signup/');
	  }
	 $temp1_arr['user'] = $_SESSION['tempData[0][user]'];
	 $temp1_arr['company'] = $_SESSION['tempData[0][company]'];
	 $this->layout='indexlayout';
	 $this->set('title_for_layout', 'Report Online plus -- SP Registration Step1');
	 $this->Company->set($this->data['Company']);
	 $this->loadModel('MainTab');
	 $this->loadModel('UserCompanyPrincipal');
	 $this->loadModel('UserCompanyPrincipalRecord');
	 $tabData=$this->MainTab->find('first');
	 $this->set('tabData',$tabData);
	 $companyPrincipal = $this->UserCompanyPrincipal->find('list', array(
				'fields' =>array('id','name')
	));
	 $this->set('companyPrincipal',$companyPrincipal);
	 $cmpPrincipal = $this->UserCompanyPrincipal->find('all'
	);
	 $this->set('cmpPrincipal',$cmpPrincipal);
      if(!empty($this->data)){
	 if($this->Company->validates()){
			$src_img = WWW_ROOT."img/company_logo_temp/".$this->data['Company']['company_logo'];
		        $des_img = WWW_ROOT."img/company_logo/".$this->data['Company']['company_logo'];
			$rs_img =  rename($src_img,$des_img);
		       if($rs_img){
			    $this->data['Company']['company_logo'] = $this->data['Company']['company_logo'];
		       }
		       else{
			    $this->data['Company']['company_logo'] = "";
		       }
		       $temp1_arr['company']  = array_merge($temp1_arr['company'],$this->data['Company']);
		       $temp1_arr['UserCompanyPrincipalRecord'] = $this->data['UserCompanyPrincipalRecord'];
		       $temp1_arr['user'] = serialize($temp1_arr['user']);
		       $temp1_arr['company'] = serialize($temp1_arr['company']);
		       $temp1_arr['UserCompanyPrincipalRecord'] = serialize($temp1_arr['UserCompanyPrincipalRecord']);
		       
		       if($this->Session->write('tempDatas[user]', $temp1_arr['user']) && $this->Session->write('tempDatas[company]', $temp1_arr['company']) && $this->Session->write('tempDatas[UserCompanyPrincipalRecord]', $temp1_arr['UserCompanyPrincipalRecord'])){
			//pr($_SESSION['tempDatas']);
			//pr(unserialize($_SESSION['tempDatas[user]']));
			//pr(unserialize($_SESSION['tempDatas[company]']));
			//pr(unserialize($_SESSION['tempDatas[UserCompanyPrincipalRecord]']));
			//die();
			$this->redirect(array('controller'=>'users','action'=>'step2'));
		       }
	 }
     }
   }
  
/*
****************************************************************************
*Function Name		 :	user registration subscription plan function
*Functionality		 :	Registration panel.
****************************************************************************
*/
   function step2($subscription = null){
	if(!isset($_SESSION['tempData[0][user]']) && empty($_SESSION['tempData[0][user]'])){
		$this->redirect('/users/signup/');
	}
      //$this->Session->destroy();
      $this->layout='indexlayout';
      $this->loadModel('SubscriptionDuration');
      $this->loadModel('User');
      $this->loadModel('Company');
      $this->loadModel('TrialOption');
      $this->loadModel('UserCompanyPrincipalRecord');
      $SubscriptionData=$this->SubscriptionDuration->find("all");
      $this->set('SubscriptionData', $SubscriptionData);
      $this->loadModel('MainTab');
      $this->loadModel('Payment');
      $tabData=$this->MainTab->find('first');
      $this->set('tabData',$tabData);
      $trialOptions = $this->TrialOption->find('all');
      $this->set('option',$trialOptions['0']['TrialOption']['trial_status']);
       if(isset($this->data) && !empty($this->data)){
	    $userData = unserialize($_SESSION['tempDatas[user]']);
	    $companyData = unserialize($_SESSION['tempDatas[company]']);
	    $cprData = unserialize($_SESSION['tempDatas[UserCompanyPrincipalRecord]']);
	    $password = $userData['password']; // to generate a password of 6 characters
            $encpt_newpassword = md5($password);		
	    $data['User']['password'] = md5($userData['password']);
	    $paln_data = explode("-",$this->data['User']['subscription_duration_monthly']);
	    if(isset($userData['fname']) && !empty($userData['fname'])){
		if($this->data['User']['option'] == "On"){
		$data['User']['subscription_under_trial'] = "Yes";
		}else{
		$data['User']['subscription_under_trial'] = "No";
		}
	    }
	    $data['User']['subscription_duration_id'] = $paln_data[0];
	    $data['User']['amount'] = $paln_data[1];
	    $data['User']['subscription_duration_plan'] = $paln_data[2];
	    $data['User']['trial_date'] = date('Y-m-d');
	    $activationnumber = md5(rand());
	    $data['User']['activationkey'] = $activationnumber;
	    $data['User']['fname'] = $userData['fname'];
	    $data['User']['lname'] = $userData['lname'];
	    $data['User']['address'] = $userData['address'];
	    $data['User']['country_id'] = $userData['country_id'];
	    $data['User']['state_id'] = $userData['state_id'];
	    $data['User']['city'] = $userData['city'];
	    $data['User']['zip'] = $userData['zip'];
	    $data['User']['phone'] = $userData['phone'];
	    $data['User']['email'] = $userData['email'];
	    $data['User']['username'] = $userData['username'];
	    $data['User']['user_type_id'] = $userData['user_type_id'];
	    $data['User']['deactivate'] = $userData['deactivate'];
	    if($this->User->save($data['User'],false)){
			$compData['Company']['user_id'] = $this->User->getLastInsertId();
			$compData['Company']['name'] = $companyData['name'];
			$compData['Company']['type_business'] = $companyData['type_business'];
			$compData['Company']['interest_service'] = $companyData['interest_service'];
			$compData['Company']['interest_service_other'] = $companyData['interest_service_other'];
			$compData['Company']['company_logo'] = $companyData['company_logo'];
			$compData['Company']['website'] = $companyData['website'];
			$compData['Company']['site_manager'] = $companyData['site_manager'];
			$compData['Company']['phone_site_manager'] = $companyData['phone_site_manager'];
			$compData['Company']['email_site_manager'] = $companyData['email_site_manager'];
			$compData['Company']['company_bio'] = $companyData['company_bio'];
			if($this->Company->save($compData['Company'],false)){
				 $compData['Company']['user_id'];
				 $CPRdata['UserCompanyPrincipalRecord']['company_id'] = $this->Company->getLastInsertId();
				foreach($cprData as $key=>$val){
	  					$CPRdata['UserCompanyPrincipalRecord']['user_company_principal_id']=$val['user_company_principal_id']; 
	  					$CPRdata['UserCompanyPrincipalRecord']['manager_name'] =  $val['manager_name'];
	  					$CPRdata['UserCompanyPrincipalRecord']['manager_email'] =  $val['manager_email'];
	  					$CPRdata['UserCompanyPrincipalRecord']['new_client'] =  $val['new_client'];
						$CPRdata['UserCompanyPrincipalRecord']['previous_report'] =  $val['previous_report'];
						$CPRdata['UserCompanyPrincipalRecord']['new_scheduled'] =  $val['new_scheduled'];
	  					$this->UserCompanyPrincipalRecord->create();
	  					$this->UserCompanyPrincipalRecord->save($CPRdata['UserCompanyPrincipalRecord'],false);
	  				}
					
					
					/*if($this->data['User']['option'] != "On"){
						
						$paymentInfo = array('Member'=>
	                           array(
				       'first_name'=>$data['User']['fname'],
	                               'last_name'=>$data['User']['lname'],
	                               'email'=>$data['User']['email'],
	                               'billing_address'=>$data['User']['address'],
	                               'billing_address2'=>'559',
	                               'billing_country'=>'US',
	                               'billing_city'=>$data['User']['city'],
	                               'billing_state'=>$data['User']['state'],
	                               'billing_zip'=>$data['User']['zip'],
				       'billing_period'=>"Month"
									
	                           ),
	                          'CreditCard'=>
	                           array(
	                               'card_number'=>$this->data['Company']['cardnumber'],
				        'credit_type'=>$this->data['Company']['cardtype'],
	                               'expiration_month'=>$this->data['Company']['month'],
	                               'expiration_year'=>$this->data['Company']['year'],
	                               'cv_code'=>$this->data['Company']['cardcvnumber']
	                           ),
	                          'Order'=> 
	                          array('theTotal'=>$data['User']['amount'])
				   );
					/*
					 * On Success, $result contains [AMT] [CURRENCYCODE] [AVSCODE] [CVV2MATCH]
					 * [TRANSACTIONID] [TIMESTAMP] [CORRELATIONID] [ACK] [VERSION] [BUILD]
					 *
					 * On Fail, $ result contains [AMT] [CURRENCYCODE] [TIMESTAMP] [CORRELATIONID]
					 * [ACK] [VERSION] [BUILD] [L_ERRORCODE0] [L_SHORTMESSAGE0] [L_LONGMESSAGE0]
					 * [L_SEVERITYCODE0]
					 *
					 * Success or Failure is best tested using [ACK].
					 * ACK will either be "Success" or "Failure"
					 */
				 
					 /*$result = $this->Paypal->processPayment($paymentInfo,"CreateRecurringPayments");
					 //$result = $this->Paypal->processRecuringPayment($paymentInfo,"CreateRecurringPayments");
					 $ack = strtoupper($result["ACK"]);
				 
					 if($ack=="SUCCESS"){
					 // if(1==1){
	                                   $paymentData['Payment']['user_id'] = $compData['Company']['user_id'];
	                                   $card_number = substr($this->data['Company']['cardnumber'],-4);
	                                   $card_number = "XXXXXXXXXXX".$card_number;
					   $paymentData['Payment']['ccreditcard_number'] = $card_number;
	                                   $paymentData['Payment']['ccv_number'] = $this->data['Company']['cardcvnumber'];
	                                   $paymentData['Payment']['amount'] = $data['User']['amount'];
					   
	                                   $this->Payment->save($paymentData['Payment']);
					    }
					}
					
				
			}*/
					//$this->Session->setFlash(__('Your account has been registered successfully,please check your mailbox to activate your account.',true));
			
		     	}
	       	}

	       	$result = $this->User->find('first',array('conditions'=>array('User.id'=>$compData['Company']['user_id'])));
			$this->loadmodel('EmailTemplate');
			$register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>4)));
			
			$link = "<a href=" . $this->selfURL() . "/homes/splogin>Click here</a>";
			$register_temp['EmailTemplate']['mail_body']=str_replace(array('#FIRSTNAME','#USERNAME','#PASSWORD','#CLICK_HERE'),array($result['User']['fname'],$result['User']['username'],$password,$link),$register_temp['EmailTemplate']['mail_body']);
			$this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);
			
			$this->Email->to = $result['User']['email'];
			$this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
			$this->Email->from = EMAIL_FROM;
			$this->Email->template = 'register_mail'; // note no '.ctp'
			$this->Email->sendAs = 'html'; // because we like to send pretty mail		
			if($this->Email->send()) //Do not pass any args to send()
			{
				$this->Session->destroy('tempDatas[user]');
				$this->Session->destroy('tempDatas[company]');
				$this->Session->destroy('tempDatas[UserCompanyPrincipalRecord]');
			}

			$this->Session->setFlash(__('Your account has been registered successfully,please check your mailbox to activate your account.',true));
			$this->redirect(array('controller'=>'users','action'=>'thankSp'));
   		}
   }
/*
****************************************************************************
*Function Name		 :	user registration subscription plan function
*Functionality		 :	Registration panel.
****************************************************************************
*/  
   function thankSp(){
	$this->layout='indexlayout';
	$this->loadModel('TutorialText');
	$this->loadModel('TutorialScreenshot');
	$this->loadModel('TutorialVideo');
	$txtData=$this->TutorialText->find('first');	
	$this->set('txtData',$txtData);
	$this->loadModel('TutorialScreenshot');
	$snapData=$this->TutorialScreenshot->find('all',array('order'=>'TutorialScreenshot.id DESC'));	
	$this->set('snapData',$snapData);
	$videoData=$this->TutorialVideo->find('all',array('order'=>'TutorialVideo.id DESC'));	
	$this->set('videoData',$videoData);
   }
  /*****************************************************************************************
  * Ajax Upload company pic
  */
  function ajaxupload(){
    $this->autoRender = false;
    if(isset($_FILES['qqfile'])){
	      $this->Uploader->uploadDir = COMPANYPICTEMP;
	      $filename = uniqid("company");
	      $this->Uploader->_data = $_FILES;
	      if($this->Uploader->upload('qqfile', array('name'=>$filename,'overwrite' => true,'resize' => 400))) {
			$resized_path1 = $this->Uploader->resize(array('width' => COMPANYPICSIZE),true);
		    echo '{"success":true,"company_image":"'.$resized_path1['name'].'"}';
	      }else{
		    echo '{"success":false}';
	      }
    }    
}
  /*****************************************************************************************
  * Ajax Upload screenshots
  */
 function ajaxuploadSnapshot(){
    $this->autoRender = false;
    if(isset($_FILES['qqfile'])){	
	      $this->Uploader->uploadDir = SNAPSHOTTEMP;
	      $filename = uniqid("snapshot");
	      $this->Uploader->_data = $_FILES;
	      if($this->Uploader->upload('qqfile', array('name'=>$filename,'overwrite' => true,'resize' => 400))) {
			$resized_path1 = $this->Uploader->resize(array('width' => PROFILEPICSIZEW,'height'=>PROFILEPICSIZEW),true);
		    echo '{"success":true,"profile_image":"'.$resized_path1['name'].'"}';
	      }else{
		    echo '{"success":false}';
	      }
    }    
}
 /*****************************************************************************************
  * Ajax Upload user pic
  */
  function ajaxuploaduserpic(){
    $this->autoRender = false;
    if(isset($_FILES['qqfile'])){
	      $this->Uploader->uploadDir = USERPROFILEPIC;
	      $filename = uniqid("profilepic");
	      $this->Uploader->_data = $_FILES;
	      if($this->Uploader->upload('qqfile', array('name'=>$filename,'overwrite' => true,'resize' => 400))) {
			$resized_path1 = $this->Uploader->resize(array('width' => PROFILEPICSIZEW,'height'=>PROFILEPICSIZEW),true);
		    echo '{"success":true,"profile_image":"'.$resized_path1['name'].'"}';
	      }else{
		    echo '{"success":false}';
	      }
    }    
}

   function confirmation($activationcode = null)
   {
	 if(!empty($activationcode))
	 {
		$this->loadModel('User');
		$result = $this->User->find("first",array('conditions'=>array('User.activationkey'=>$activationcode)));
		$this->data['User']['id'] = $result['User']['id'];
		$this->data['User']['deactivate'] = 1;
		if($this->User->save($this->data['User'],false))
		{
			$this->Session->setFlash('Your account has been activated successfully',true);
			$this->redirect(array('controller'=>'homes','action'=>'login'));
		}
	 }
   }
  
  /* THIS PAGE IS FOR THE SHOWING THE ERROR MESSAGE IF PAYMENT FAILS */
function errorpage()
{
     $this->layout='frontend';
     $this->set('title_for_layout', 'errorpayment'); 
}
  /* THIS PAGE IS FOR THE SHOWING THE THANKYOU MESSAGE IF PAYMENT SUCCESS */
function thankyoupage()
{
     $this->layout='frontend';
     $this->set('title_for_layout', 'sucesspayment'); 
}
  
/************************************************************************
 function to get states corresponding to particular country
************************************************************************/
    public function getByCountry($cntid=null){
		configure::write('debug',2);
		$this->layout = 'ajax';
  		$this->loadModel('State');
		if(isset($cntid) && $cntid != "")
		{
			$states = $this->State->find('list', array(
				    'conditions' => array('State.country_id' => $cntid),
				    'order'=>array('State.name ASC'),
				    'recursive' => -1
				    ));
			if(!empty($states)){
					$this->set('states',$states);
					echo json_encode($states);
					die;
			}
			else {
					$this->set('states',0);		
			}
		}
		die;
    }
    
/************************************************************************
 function to get cities corresponding to particular state
 created by: Manish Kumar
************************************************************************/
    public function getByState($stateid=null){
		configure::write('debug',2);
		$this->layout = 'ajax';
  		$this->loadModel('City');
		if(isset($stateid) && $stateid != "")
		{
			$cities = $this->City->find('all', array(
				    'conditions' => array('City.state_id' => $stateid),
				    'order'=>array('City.name ASC'),
				    'fields' =>array('id','name'),
				    'recursive' => 1
				    ));
			if(!empty($cities)){
				
				$ajaxcity = array();
				$iii = 0;
				foreach($cities as $city){
					$ajaxcity[$iii] = $city['City'];
					$iii++;
				}
					$this->set('cities',$cities);
					echo json_encode($ajaxcity);
					die;
			}
			else {
					$this->set('cities',0);		
			}
		}
		die;
    }    

    /************************************************************************
 function to get states corresponding to particular country
************************************************************************/
    public function getByCountrySite($cntid=null){
		configure::write('debug',2);
		$this->layout = 'ajax';
  		$this->loadModel('State');
		if(isset($cntid) && $cntid != "")
		{
			$states = $this->State->find('list', array(
				    'conditions' => array('State.country_id' => $cntid),
				    'order'=>array('State.name ASC'),
				    'recursive' => -1
				    ));
			if(!empty($states)){
					$this->set('states',$states);
			}
			else {
					$this->set('states',0);		
			}
		}
		 	
    }
    /************************************************************************
 function to get helpers for the inspection of the service under lead
************************************************************************/
    function gethelper($user_id=null,$report_id = null,$sp_id=null){
		configure::write('debug',2);
		$this->layout = 'ajax';
  		$this->loadModel('User');
		if(isset($user_id) && $user_id != "")
		{
			$where = " User.id != '".$user_id."' AND User.sp_id = '".$sp_id."' AND User.user_type_id = 2 AND FIND_IN_SET('".$report_id."', report_id)";
			
			$helperOption = $this->User->find('all', array(
				    'conditions' => array($where),'fields' =>array('id','fname','lname'),
			));
			if(!empty($helperOption)){
				$this->set('helperOption',$helperOption);
			}
		}
    }

/************************************************************************
 function to get security questions
************************************************************************/
    function getsquestion(){
    	$this->loadModel('Securityq');
		echo 'test';
		exit;    	
    }

}