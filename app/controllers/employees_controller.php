<?php
class EmployeesController extends AppController {

	var $name = 'Employees';
	var $components = array('Email','Session','Cookie','Calendar','Sms');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Calendar','Common','Timeframe');
  
/*
*************************************************************************
*Function Name		 :	employee_login
*Functionality		 :	Login to Admin Panel.
*************************************************************************
*/ 		
	/* COMMENT DONE BY VISHAL ON 22-11-2010 NOW THE COMMON LOGIN WILL WORKS AT /HOMES/LOGIN
  function employee_login () {
    $this->layout='employee';
    $this->set('title_for_layout',"Login");   
      if(isset($this->data)) {
    		if($this->data['Employee']['email']=='') {
          $this->Session->setFlash('Please enter email!');
          $this->redirect(array('controller'=>'employees','action'=>'login'));
        } else if($this->data['Employee']['pwd']=='') {
          $this->Session->setFlash('Please enter Password!');
          $this->redirect(array('controller'=>'employees','action'=>'login'));
        }
				
			$admin = $this->Employee->find('first',array('fields'=>array('id','fname','lname','email'),'conditions'=>array('Employee.status'=>'1','Employee.pwd'=> md5($this->data['Employee']['pwd']),'Employee.email'=>$this->data['Employee']['email'])));

	 		if($admin){	
	 				$this->Session->write('EmployeeId',$admin['Employee']['id']);
	 				$this->Session->write('EmployeeName',$admin['Employee']['fname'].' '.$admin['Employee']['lname']);
  				$this->Session->write('Employee',$admin);
  				$this->redirect(array('controller'=>'employees','action'=>'index'));
  			} else {
  				$this->Session->setFlash('Login Failed !');
  			}
  			$this->data['Owner']['email']='';
  			$this->data['Owner']['pwd']='';
		}    
  }
  */
  
  /*
*************************************************************************
*Function Name		 :	employee_index
*Functionality		 :  Dashboard for employee section.
*************************************************************************
*/ 	
  
  function employee_index() {
    $this->layout='employee';
    $this->set('title_for_layout', 'Employee'); 
  }
  
/*
*************************************************************************
*Function Name		 :	employee_changepasswd
*Functionality		 :	use to change password of Employee section.
*************************************************************************
*/ 

function employee_changepasswd(){	 
      	 	$EmployeeId=$this->Session->read('EmployeeId');
      		if(isset($this->data)) {
          $this->Employee->set($this->data);
          $this->Employee->validate=$this->Employee->validateChangePass;
        if($this->Employee->validates()) {
             $this->data['Employee']['id']=$EmployeeId;
             $this->data['Employee']['pwd']=md5($this->data['Employee']['pwd']);
          	 if($this->Employee->save($this->data,array('validate'=>false))) {			
          	 $this->Session->setFlash('Password has been changed.','success');
              	$this->redirect(array('controller'=>'employees','action'=>'index'));
              }  
         }
         $this->data['Employee']['pwd']='';
         $this->data['Employee']['cpwd']='';
        }  
        $this->set('id', $EmployeeId);
      	$this->set('title_for_layout', 'Change password');
  }
  
/*
*************************************************************************
*Function Name		 :	employee_logout
*Functionality		 :	Logout.
*************************************************************************
*/ 
	function employee_logout(){
		$this->Session->destroy('Employee');
		$this->redirect('/homes/login');
	}  
	
/*
*************************************************************************
*Function Name		 :	manager_calendar
*Functionality		 :  calender for all employee for manager section.
*************************************************************************
*/ 	
  
function agent_calendar($tstamp=null){
    $this->set('posOption',$this->getPosition());
    $this->layout='managerschedule';
    $this->set('title_for_layout', 'Manager');
    if(!isset($tstamp)){
    $tstamp=time();
    }
    if(!empty($this->data))
    {
       if($this->data['Schedule']['optional_category']!='' && $this->data['Schedule']['position_id']=='')
      {
        //$condition=array('Schedule.optional_category'=>$this->data['Schedule']['optional_category']);
        $this->Employee->bindModel(array(
			'hasMany' => array(
                    			
			                'Schedule' => array(
			                'className' => 'Schedule',
                    			'foreignKey' => 'employee_id',
                    			'conditions' => array('Schedule.employee_id !='=>0,'Schedule.optional_category'=>$this->data['Schedule']['optional_category']),              			
		)		 
      )));
      $alldata =$this->Employee->find('all', array('conditions' => array('Employee.company_id'=>$this->Session->read('Company_ID'))));
      }
      else if($this->data['Schedule']['position_id']!='' && $this->data['Schedule']['optional_category']=='')
      {
        //$condition=array('Schedule.position_id'=>$this->data['Schedule']['position_id']);
        $this->Employee->bindModel(array(
			'hasMany' => array(
                    			
			                     'Schedule' => array(
			                     'className' => 'Schedule',
                    			'foreignKey' => 'employee_id',
                    			'conditions' => array('Schedule.employee_id !='=>0,'Schedule.position_id'=>$this->data['Schedule']['position_id']),
                    			
			     )		 
			   )));
      $alldata =$this->Employee->find('all', array('conditions' => array('Employee.company_id'=>$this->Session->read('Company_ID'))));
      }
      else if($this->data['Schedule']['position_id']!='' && $this->data['Schedule']['optional_category']!='')
      {
        //$condition=array('Schedule.optional_category'=>$this->data['Schedule']['optional_category'],'Schedule.position_id'=>$this->data['Schedule']['position_id']);
        $this->Employee->bindModel(array(
			 'hasMany' => array(
                    			
			                     'Schedule' => array(
			                     'className' => 'Schedule',
                    			'foreignKey' => 'employee_id',
                    			'conditions' => array('Schedule.employee_id !='=>0,'Schedule.optional_category'=>$this->data['Schedule']['optional_category'],'Schedule.position_id'=>$this->data['Schedule']['position_id']),
                    			
						             )		 
			   )));
        
      $alldata =$this->Employee->find('all', array('conditions' => array('Employee.company_id'=>$this->Session->read('Company_ID'))));
      }
      else if($this->data['Schedule']['optional_category']=='' && $this->data['Schedule']['position_id']=='')    
      {
        $this->Employee->bindModel(array(
			'hasMany' => array(
                    			
			                     'Schedule' => array(
			                     'className' => 'Schedule',
                    			'foreignKey' => 'employee_id',
                    			'conditions' => array('Schedule.employee_id !='=>0),
                    			
						             )		 
			   )));    
    
    $alldata =$this->Employee->find('all', array('conditions' => array('Employee.company_id'=>$this->Session->read('Company_ID'))));
      }
   }
   else
   {
      $this->Employee->bindModel(array(
			'hasMany' => array(
                    			
			                     'Schedule' => array(
			                     'className' => 'Schedule',
                    			'foreignKey' => 'employee_id',
                    			'conditions' => array('Schedule.employee_id !='=>0),
                    			
						             )		 
			   )));    
    
    $alldata =$this->Employee->find('all', array('conditions' => array('Employee.company_id'=>$this->Session->read('Company_ID'))));
   } 
  
        
    
   
   $this->set('alldata',$alldata);
   $this->set('tstamp',$tstamp); 
   $this->loadModel('Holiday');
    $currentweek_start=$this->Calendar->getday($tstamp,1);
    $currentweek_end=$this->Calendar->getday($tstamp,7);
       
    $holidays =$this->Holiday->find('all',array('conditions'=>array("Holiday.company_id"=>$this->Session->read('Company_ID'),'OR'=>array('Holiday.start_date BETWEEN ? AND ?' => array($currentweek_start,$currentweek_end),'Holiday.end_date BETWEEN ? AND ?' => array($currentweek_start,$currentweek_end)))));
   
   $this->set('holidays',$holidays);
      
}

/*
*************************************************************************
*Function Name		 :	employee_calendar
*Functionality		 :  calender for all employee for employee section.
*************************************************************************
*/ 	
  
function employee_calendar($tstamp=null) {
    //$this->set('posOption',$this->getPosition());
    //$this->layout='managerschedule';
     $this->layout='employee';
    $EmployeeId=$this->Session->read('EmployeeId');
    /*to compant id*/
    
     $employeedata =$this->Employee->find('first', array('conditions' => array('Employee.id'=>$EmployeeId)));
     $empcompanyid = $employeedata['Employee']['company_id'];
    /*end to featch company id*/
    if(!isset($tstamp)) {
    $tstamp=time();
    }
    
    
      $this->Employee->bindModel(array(
			'hasMany' => array(
                    			
			                     'Schedule' => array(
			                     'className' => 'Schedule',
                    			'foreignKey' => 'employee_id',
                    			'conditions' => array('Schedule.employee_id !='=>0),
                    			
						             )		 
			   )));    
    
    $alldata =$this->Employee->find('all', array('conditions' => array('Employee.company_id'=>$empcompanyid)));
      
   $this->set('alldata',$alldata);
   $this->set('tstamp',$tstamp); 
   $this->loadModel('Holiday');
    $currentweek_start=$this->Calendar->getday($tstamp,1);
    $currentweek_end=$this->Calendar->getday($tstamp,7);
       
    $holidays =$this->Holiday->find('all',array('conditions'=>array("Holiday.company_id"=>$empcompanyid,'OR'=>array('Holiday.start_date BETWEEN ? AND ?' => array($currentweek_start,$currentweek_end),'Holiday.end_date BETWEEN ? AND ?' => array($currentweek_start,$currentweek_end)))));
   
   $this->set('holidays',$holidays);
      
}


/*	

*************************************************************************
*Function Name		 :	mamanager_view_emp_schedules
*Functionality		 :  use to get list of employee schedules.
*************************************************************************
*/ 	
	
 function manager_view_emp_schedules($empID = null,$shiftdate = null){
	$empscheduledata = array();
	//$currdate=date('Y-m-d',$tstamp);
	$this->loadModel('Schedule');
	
	$empscheduledata=$this->Schedule->find('all',array('conditions'=>array('Schedule.shift_date= "'.$shiftdate.'" AND Schedule.employee_id  ='.$empID)));
	$this->set('empscheduledata',$empscheduledata);			 
 }
 
/*	

*************************************************************************
*Function Name		 :	manager_calendaralt
*Functionality		 :  use for generating timestamp for some special cases.
*************************************************************************
*/ 	
  
 function manager_calendaralt($dateMDY){
    list($y,$m,$d)= explode('-',$dateMDY);
    $tstamp= mktime(0, 0, 0, $m, $d, $y);
   	$this->redirect(array('controller'=>'employees','action'=>'calendar',$tstamp));    
  }
  
  function employee_calendaralt($dateMDY){
    list($y,$m,$d)= explode('-',$dateMDY);
    $tstamp= mktime(0, 0, 0, $m, $d, $y);
   	$this->redirect(array('controller'=>'preferences','action'=>'index',$tstamp));    
  }
  
  
  /**** CODE FOR EMPLOYEE INDEX PAGE WHERE TABS ARE SHOWN FOR EMPLOYEES ****/
  function manager_index()
  {
    $this->layout='manager';
     
  }
  
  /**** CODE FOR EMPLOYEE ADD IN MANAGER ****/
  function manager_add() {
    $this->set('positions',$this->get_emp_position());
    $this->layout='manager';
    $this->set('action',$this->params['action']);
    
    $this->loadModel('Company');
    $Companydata = $this->Company->findByOwnerId($this->Session->write('OwnerId'));
    //echo $Companydata['Company']['id'];
    $emp_count=$this->Employee->find('count',array('conditions'=>array('Employee.company_id'=>$this->Session->read('Company_ID'))));
  
    $this->loadModel('SubscriptionDuration');
    $SubDuration=$this->SubscriptionDuration->find('first',array('conditions'=>array('SubscriptionDuration.id'=>$Companydata['Owner']['subscription_duration_id'])));
    $SubDuration['SubscriptionDuration']['max_allowed_employees'];
    
    if($emp_count>=$SubDuration['SubscriptionDuration']['max_allowed_employees'])
    {      
      $this->Session->setFlash('Sorry! your subscription plan limit allow you to add max ' .$SubDuration['SubscriptionDuration']['max_allowed_employees']. ' employees.');
      $this->redirect(array('controller'=>'employees','action'=>'manager_list'));
      exit;
    }
    
    
    if (!empty($this->data))
    {     
        if($this->Employee->set($this->data) && $this->Employee->validates()) {
        $this->data['Employee']['position_id']=implode(',',$this->data['Employee']['position_id']);
        $pwd=substr((md5(mktime(date('h'),date('i'),date('s'),date('m'),date('d'),date('Y')))),0,6);
        $this->data['Employee']['pwd']=md5($pwd);
          if($this->Employee->save($this->data))
      		{
      			      /**** MAIL TO EMPLOYEE 
        					$message = file_get_contents(VIEWS .'elements' . DS . 'mailformats' . DS . 'employee_registration_mail.ctp');
        			    $create_initial_array=array('[firstname]','[username]','[password]','[APPLICATION_NAME]');
            			$replace_initial_array=array($this->data['Employee']['fname'],$this->data['Employee']['email'],$pwd,$_SERVER['HTTP_HOST']);
            			$message = str_replace($create_initial_array,$replace_initial_array,$message);            			
                  echo $message;
				  die;***/
                  // Subject of mail
				  
				  
				  $this->Email->to = $this->data['Employee']['email'];
                   $this->Email->from = 'customerservice@schedulepal.com';
                   $this->Email->replyTo = 'noreply@schedulepal.com';
                    
                     $this->set('data',$this->data);
                     $this->set('pass',$pwd);
                    
                     $this->Email->template = 'employee_registration';
					 $this->Email->sendAs = 'html';
					 $this->Email->subject = NEW_REGISTRATION_EMPLOYEE_SUBJECT;
                   
				     $this->Email->send(); 
				  
				  
        					/*
					$subject=NEW_REGISTRATION_EMPLOYEE_SUBJECT;    					
        					$to=$this->data['Employee']['email'];
        					// To send HTML mail, the Content-type header must be set
        					$headers  = 'MIME-Version: 1.0' . "\n";
        					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
        					// Additional headers
        					$headers .= 'From: SchedulePal Customer Service <contact@schedulepal.com>' . "\n";
							// Mail it
        					@mail($to, $subject, $message, $headers);*/
            
            $this->Session->setFlash('New Employee Added Successfully');
      			$this->redirect(array('controller'=>'employees','action'=>'manager_list'));
      		 }
    	}
    }
        
    $this->render('manager_edit');  
  }
  
  /**** CODE FOR EMPLOYEE EDIT IN MANAGER ****/
  function manager_edit($id=null)
  {
    $this->set('positions',$this->get_emp_position());
    $this->layout='manager';
    $this->set('action',$this->params['action']);   
    $this->Employee->id=$id;
  	if (empty($this->data))
  	{ 
  		$this->Employee->id = $id;
  		$this->data=$this->Employee->read();
  	}
  	else
  	{    
    	  if($this->Employee->set($this->data) && $this->Employee->validates()) { 
    	     $this->data['Employee']['position_id']=implode(',',$this->data['Employee']['position_id']);
      		if ($this->Employee->save($this->data['Employee']))
      		{
      			$this->Session->setFlash(__('Employee Information Sucessfully Saved',true));
      			$this->redirect(array('controller'=>'employees','action'=>'manager_list'));
      		}   
    	}
  	}
 }
  
  /**** CODE FOR EMPLOYEE LIST IN MANAGER ****/             
  function manager_list()
  {
    $this->layout='manager';
    $this->paginate = array(
                       'conditions'=>array('Employee.company_id'=>$this->Session->read('Company_ID')),
                        'limit' => 30,
                        'order' => array(
                        'Employee.id' => 'DESC'
                                        ));
    $Employeedata = $this->paginate('Employee');
    $this->set('Employeedata',$Employeedata);
    if(isset($_POST['action']) && $_POST['action']=='sendmessages')
    {   	  
        $this->manager_sendmessages();          // SEND MESSAGES
    }
    if(isset($_POST['action']) && $_POST['action']=='send_schedular_reminder')
    {   	  
        $this->manager_send_schedular_reminder();  // SEND MESSAGES AS SCHEDULAR REMINDER
    }			
  }
  
  /**** CODE FOR EMPLOYEE DELETE IN MANAGER ****/  
  function manager_delete($id=null)
  {
    $this->loadModel('Employee');
    if(!empty($id)) {
      $this->data = $this->Employee->find('all',array('conditions'=>array('id'=>$id)));
      $this->Employee->delete($id);
      $this->Session->setFlash('Employee record deleted successfully.');
      $this->redirect('list'); 
    } else {
      $this->redirect('list'); 
    }
  }
  
  /**** CODE FOR EMPLOYEE DETAIL PAGE IN MANAGER ****/ 
  function manager_emp_detail($id=null)
  {
    $this->layout='ajax';
    $this->set('title_for_layout', 'Manager');  
    if(!empty($id)) {
      $this->data = $this->Employee->find('first',array('conditions'=>array('id'=>$id)));
      }
  }
  
  /***** CODE TO ADD THE NEW POSITION *****/
  function manager_add_positions()
  {
    $this->layout='manager';
    $this->loadModel('Position');
    $this->set('action',$this->params['action']);
   // print_r($this->data);
    if (!empty($this->data))
    {    	   
        if($this->Position->set($this->data) && $this->Position->validates()) {
          if($this->Position->save($this->data))
      		{
      			$this->Session->setFlash('New Position Added Successfully');
      			$this->redirect(array('controller'=>'employees','action'=>'manager_view_positions'));
      		}
    	}
    } 
    
    $this->render('manager_edit_positions');  
  }
  
  /**** CODE FOR POSITION EDIT IN MANAGER ****/
  function manager_edit_positions($id=null)
  {
      $this->layout='manager';
      $this->loadModel('Position');
      $this->set('action',$this->params['action']);   
      $this->Position->id=$id;
    	if (empty($this->data))
    	{ 
    		$this->Position->id = $id;
    		$this->data=$this->Position->read();    		
    	}
    	else
    	{
    		if ($this->Position->save($this->data['Position']))
    		{
    			$this->Session->setFlash(__('Position Sucessfully Saved',true));
    			$this->redirect(array('controller'=>'employees','action'=>'manager_view_positions'));
    		}   
    	}    
  }
  
  /**** CODE FOR POSITIONS LIST IN MANAGER ****/             
  function manager_view_positions()
  {
    $this->layout='manager';
    $this->loadModel('Position');
    $this->paginate = array(
                       'conditions'=>array('Position.company_id'=>$this->Session->read('Company_ID')),
                        'limit' => 10,
                        'order' => array(
                        'Position.id' => 'DESC'
                                        ));
    $Positiondata = $this->paginate('Position');
    $this->set('Positiondata',$Positiondata);		
  }
  
  function manager_delete_positions($id=null)
  {
    $this->loadModel('Position');
    if(isset($id)){
       if($this->Position->delete($id)) {
       $this->Session->setFlash(__('Position deleted sucessfully',true));
        
        } else {
         $this->Session->setFlash(__('Position can not be deleted',true));
        }
    }
    	$this->redirect(array('controller'=>'employees','action'=>'manager_view_positions'));
  }
  
  
  /**** CODE FOR EMAIL NOTIFICATIONS IN MANAGER ****/
  function manager_email_notifications()
  {
    $this->layout='manager';
    $this->loadModel('Notification');
    $this->paginate = array(
                       'conditions'=>array('Employee.company_id'=>$this->Session->read('Company_ID')),
                        'limit' => 30,
                        'order' => array(
                        'Notification.id' => 'DESC'
                                        ));
    $Employeedata = $this->paginate('Notification');
    $this->set('Employeedata',$Employeedata);
    if(!empty($this->data)&& isset($this->data))
    {
    
      foreach($this->data['Notification']['employee_id'] as $key=>$employee_id)
      {      
          $data['Notification']['employee_id']=$employee_id;
          $data['Notification']['new_schedule_published']=$this->data['Notification']['new_schedule_published'][$key]; 
          $data['Notification']['new_schedule_unpublished']=$this->data['Notification']['new_schedule_unpublished'][$key];
          $data['Notification']['on_trade_request_approved']=$this->data['Notification']['on_trade_request_approved'][$key];
          $data['Notification']['on_trade_request_denial']=$this->data['Notification']['on_trade_request_denial'][$key];
          $data['Notification']['urgent_alerts']=$this->data['Notification']['urgent_alerts'][$key];
          $data['Notification']['publish_shift_changed']=$this->data['Notification']['publish_shift_changed'][$key];
          $emp_data = $this->Notification->find('first',array('conditions'=>array('Notification.employee_id'=>$employee_id),array('recursive'=>-1)));
          
          if(!empty($emp_data))
          {      
            $this->Notification->id=$emp_data['Notification']['id'];
            $this->Notification->save($data['Notification']);
          }
          else
          {
            $this->Notification->id=null;
            $this->Notification->save($data['Notification']);
          }        
      }
      $this->Session->setFlash(__('Records Sucessfully Saved',true));
    }
  }
  
  #FUNCTION TO SEND MESSAGES IN MANAGER FOR SELECTED EMPLOYEES
  function manager_sendmessages()
  {
    $this->render('manager_sendmessages');
    if(!empty($this->data))
    {
      foreach($this->data['Employee']['id'] as $emp_id)
      {
          $emp_data = $this->Employee->find('first',array('conditions'=>array('Employee.id'=>$emp_id),'recursive'=>-1,'fields' => array('Employee.id','Employee.fname','Employee.lname','Employee.email')));
          app::import("Component","Email");
      		$email = & new EmailComponent();
      		/*** CODE TO SAVE THE MESSAGE IN MESSAGE MODEL ***/
      		$this->loadModel('Message');
      		$this->Message->create();
      		$data['Message']['receiver_id']=$emp_data['Employee']['id'];
      		$data['Message']['sender_id']=$this->Session->read('OwnerId');
      		$data['Message']['company_id']=$this->Session->read('Company_ID');
      		$data['Message']['message_subject']=$this->data['Employee']['subject'];
      		$data['Message']['message_body']=$this->data['Employee']['message']; 
          $this->Message->id=null;    		
          $this->Message->save($data['Message']);
      		/** END CODE TO SAVE MESSAGE IN MESSAGE MODEL ***/
          $message =  "Dear ". $emp_data['Employee']['fname'].",<br><br><p>".$this->data['Employee']['message']."</p><br><br>Regards<br><br> SchedulePal";
        	$to=$emp_data['Employee']['email'];
      		$email->to = $to;
      		$email->subject = $this->data['Employee']['subject']; 
      		$email->from = 'SchedulePal Customer Service <contact@schedulepal.com>';
      		$email->sendAs = 'html';
      		$email->send($message);      		
       }
       $this->Session->setFlash(__('Message has been sent successfully',true));
       $this->redirect('list'); 
        
    }
  }
  
  #FUNCTION TO SEND SCHEDULER REMINDERS IN MANAGER 
  function manager_send_schedular_reminder()
  {
      $this->render('manager_send_schedular_reminder');
    if(!empty($this->data))
    {
      foreach($this->data['Employee']['id'] as $emp_id)
      {
        $emp_data = $this->Employee->find('first',array('conditions'=>array('Employee.id'=>$emp_id),'recursive'=>-1,'fields' => array('Employee.id','Employee.fname','Employee.lname','Employee.email')));
        app::import("Component","Email");
    		$email = & new EmailComponent();
    		$message =  "Dear ". $emp_data['Employee']['fname'].",<br><br><p>".$this->data['Employee']['message']."</p><br>Begin date: ".$this->data['Employee']['begin_date']."<br>End date: ".$this->data['Employee']['end_date']."<br>Regards<br><br> SchedulePal";
      	$to=$emp_data['Employee']['email'];
     		$email->to = $to;
     		$email->subject = $this->data['Employee']['subject']; 
     		$email->from = 'SchedulePal Customer Service <contact@schedulepal.com>';
    		$email->sendAs = 'html';
     		$email->send($message);
     		$this->Session->setFlash(__('Schedular reminder has been send successfully',true));
      	$this->redirect('list'); 
      }
      
    }
  }
  #FUNCTION TO Change notification (sms and email) setting  
  function employee_setting()
  {
  	 	$EmployeeId=$this->Session->read('EmployeeId');
  	 	if(!empty($this->data)) {
  	 	 $this->data['Employee']['id']=$EmployeeId;
       if($this->Employee->save($this->data))
          $this->Session->setFlash(__('Notification settings has been changed successfully.',true));
         else
           $this->Session->setFlash(__('Notification settings are not change.',true));
       } 	 	
  	 	
  	 	$this->data = $this->Employee->find('first',array('conditions'=>array('Employee.id'=>$EmployeeId),'recursive'=>-1,'fields' => array('Employee.id','Employee.emailnotification','Employee.smsnotification')));
  	 	
  	 	
  
  }
  
   
  function manager_testsms(){
  $masa=$this->Sms->smsSend("+919501546125","Hello Arvind. How r you?");
  echo $masa;
  die; 
  }
  
}
?>