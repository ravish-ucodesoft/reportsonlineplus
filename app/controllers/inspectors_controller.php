<?php ob_start();
class InspectorsController extends AppController {
	
	var $name = 'Inspectors';
	var $uses = array('User','Company');
	var $components = array('Email','Session','Cookie','Calendar');
	var $helpers = array('Html','Ajax','Javascript','Session','Common','Time','Calendar');

	function beforeFilter(){
	 parent::beforeFilter();
	}

	
	
	function inspector_annoucement()
	{
		$this->layout=null;
		$inspectorses = $this->Session->read('Log');
		
		if(empty($inspectorses))
		{
			$this->redirect('/homes/login');
		}		
		$inspector_id = $inspectorses['User']['id'];
		$sp_id = $inspectorses['User']['sp_id'];
		$this->loadModel('SpGeneralCompanyAnnouncement');
		$annoucedata = $this->SpGeneralCompanyAnnouncement->find('first', array('conditions'=>array('SpGeneralCompanyAnnouncement.sp_id'=>$sp_id),
						  'fields' => array('SpGeneralCompanyAnnouncement.for_inspectors'),		        
						  'recursive' => 1
					  ));
		$this->set('annoucedata',$annoucedata);
		
		if(!empty($this->data))
		{			
			$data=$this->data['User']['prompt'];
			$this->Session->write('likeprompt',$data);
			$this->redirect('dashboard');
		}
		
	}
	
	
	function inspector_dashboard($tstamp=null){
	  $this->layout='inspector';
	  $this->set('title_for_layout',"Inspector Dashboard");
	  $likeprompt=$this->Session->read('likeprompt');
	  $this->set('likeprompt',$likeprompt);
	  $inspectorses = $this->Session->read('Log');
	  $inspector_id = $inspectorses['User']['id'];
	  $sp_id = $inspectorses['User']['sp_id'];
	  $this->loadModel('SpGeneralCompanyAnnouncement');
	  $annoucedata = $this->SpGeneralCompanyAnnouncement->find('first', array('conditions'=>array('SpGeneralCompanyAnnouncement.sp_id'=>$sp_id),
				        'fields' => array('SpGeneralCompanyAnnouncement.for_inspectors'),		        
				        'recursive' => 1
				    ));
	  $this->set('annoucedata',$annoucedata);
	  #NEW CODE DONE BY VISHAL ON 05 June 2012
		
		if(!isset($tstamp)){
		$tstamp=time();
		}
		$current_month_no=date('m',$tstamp);
		$current_year=date('Y',$tstamp);
		$this->set('tstamp',$tstamp);
		
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];		
		$this->loadModel('ServiceCall');
		$this->loadModel('Schedule');
		$this->ServiceCall->Behaviors->attach('Containable');   
		$this->ServiceCall->contain('Schedule');
		
		$where = "Schedule.lead_inspector_id ='".$inspector_id."' OR FIND_IN_SET('".$inspector_id."', helper_inspector_id)";
		
		$res=$this->ServiceCall->find("all",array('contain'=>array('Schedule'=>array('conditions'=>array($where))),'order'=>array('ServiceCall.id DESC'),'conditions'=>array('ServiceCall.call_status'=>1),'recursive'=>2));
		//pr($res);
		$this->set("data",$res);
		$this->set('frequency',Configure::read('frequency'));
		$this->set('session_inspector_id',$inspector_id);
	}
	
	
	function inspector_codechart()
	{
		$this->layout='inspector';
		$this->set('title_for_layout',"Deficiency Code Chart-NFPA/Joint Commission");
		$this->loadModel('Code');
	$this->loadModel('Report');
	
	$data = $this->Report->find('list', array(
				        'fields' => array('Report.id', 'Report.name'),				        
				        'recursive' => 0
				    ));				 
	$this->set('options',$data); 
     if(isset($this->data) && !empty($this->data)){
	$where = "Code.report_id = '".$this->data['Code']['report_id']."'";
	$this->paginate = array(        
				'conditions'=>array($where),
				'limit' => 50,
				'order' => array('Code.id' => 'desc')
	);
	$codeListing = $this->paginate('Code',false);
	
	$this->set('codeListing',$codeListing);
	$this->set('reportid',$this->data['Code']['report_id']);
     }else if(isset($this->params['named']['rid']) && !empty($this->params['named']['rid']))
     {
	$where = "Code.report_id = '".$this->params['named']['rid']."'";
	$this->paginate = array(        
				'conditions'=>array($where),
				'limit' => 100,
				'order' => array('Code.id' => 'desc')
	);
	$codeListing = $this->paginate('Code',false);
	$this->set('codeListing',$codeListing);
	$this->set('reportid',$this->params['named']['rid']);
     }else if(isset($_POST['viewall']) && ($this->params['form']['viewall'] == "View All")){
	$this->paginate = array(
				'limit' => 200,
				'order' => array('Code.id' => 'desc')
	);
	$this->set('codeListing',$this->paginate('Code',false));
	$this->set('viewall','allData');
     }else{
	 
	$this->set('codeListing',$this->paginate('Code'));
	
	}
	}
	
	
	function inspector_codeDescription($id = null){
    $this->loadModel('Code');
    $cid = base64_decode($id);  		
    $this->Code->id = $cid;
    $data= $this->Code->find('first',array('conditions'=>array('Code.id'=>$cid)));
    $this->set('codeListing',$data);
}

	function inspector_getdirection($client_id = null){
    $this->layout = null;
	$this->loadModel('User');
	$client_id=base64_decode($client_id);
	$this->User->service = true;
	$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>3,'User.id'=>$client_id)));
	$this->set('result',$result);	
}

	function getsitedirection($siteId = null){
		$this->layout = null;
		$this->loadModel('SpSiteAddress');
		$site_id=base64_decode($siteId);
		$this->SpSiteAddress->service = true;
		$result = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$site_id)));
		$this->set('result',$result);	
	}
	
/*
*************************************************************************
*Function Name		 :	inspector Logout
*Functionality		 :	use to Logout of inspector section.
*************************************************************************
*/	
	function inspector_logout(){
		if ( $this->Cookie->read("Log.User") ) {
			$this->Cookie->delete('Log.User');
		}
		$this->Session->destroy('Log');
		$this->Session->destroy('likeprompt');
		$this->Session->setFlash('You have been logged out successfully');
		$this->redirect('/homes/login');
	}
/*
***************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of inspector section.
***************************************************************************
*/
      function inspector_clientlisting(){
	 $inspectorses = $this->Session->read('Log');
	 $inspector_id = $inspectorses['User']['id'];
	 $result = $this->User->find('all',array('conditions'=>array('User.inspector_id'=>$inspector_id),'order'=>'User.id DESC'));
	 $this->set('result',$result);
      }
	  
	  function inspector_client_mapdirection($client_id=null) {
		$this->layout=null;
		$client_id=base64_decode($client_id);
		$this->loadModel('User');
		$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>3,'User.id'=>$client_id)));
		
		$this->set('result',$result);	
		
	}
	  
	  
/*
*************************************************************************
*Function Name		 :	changepasswd
*Functionality		 :	use to change password of inspector section.
*************************************************************************
*/ 
	function inspector_changepasswd(){
		$this->loadModel('User');
		$this->layout='inspector';
		$inspectorses = $this->Session->read('Log');
		$pass = $inspectorses['User']['password'];
		if(isset($this->data) && !empty($this->data)){     		
			$this->User->set($this->data);					  
			$data['User']['id'] =  $this->data['User']['id'];
			$oldpass=md5($this->data['User']['oldpassword']);
			if($oldpass==$pass){
			$this->data['User']['password'];
			$data['User']['password'] = md5($this->data['User']['password']);
			if($this->User->save($data['User'],array('validate'=>false))){
				 $result = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['User']['id'])));
				 $this->Session->write('Log', $result);
				 $this->Session->setFlash('Password has been changed successfully');
				 $this->redirect(array('controller'=>'inspectors','action'=>'inspector_changepasswd'));
			}
			}else{
				 $this->Session->setFlash('Please enter correct old password');
				 $this->redirect(array('controller'=>'inspectors','action'=>'inspector_changepasswd'));
		 }
		}
	}
/*
*************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of Inspector section.
*************************************************************************
*/
   function inspector_editinfo($id = null)
   {
	 if(isset($this->data) && !empty($this->data))
	 {
		if($this->User->save($this->data['User'],false))
		{
		       if(isset($this->data['Company']['company_logo_new']) && !empty($this->data['Company']['company_logo_new'])){
		       
			       $src_img = WWW_ROOT."img/company_logo_temp/".$this->data['Company']['company_logo_new'];
			       $des_img = WWW_ROOT."img/company_logo/".$this->data['Company']['company_logo_new'];
			       $rs_img =  rename($src_img,$des_img);
			       if($rs_img){
				   $this->data['Company']['company_logo'] = $this->data['Company']['company_logo_new'];
			       }
			       }
			       else{
					  $this->data['Company']['company_logo'] = $this->data['Company']['company_logo_old'];
			       }
			       if($this->Company->save($this->data['Company'],false))
			       {
				       $result = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['User']['id'])));
				       $this->Session->write('Log',$result);
				       $this->Session->setFlash(__('Information has been updated successfully',true));
				       $this->redirect(array('controller'=>'inspectors','action'=>'inspector_dashboard'));
			       }
		}
	 }else {
			$this->User->id = base64_decode($id); //
			$this->data = $this->User->read();
	 }
   }
     
/*
*************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of Inspector section.
*************************************************************************
*/
      function inspector_credentialmail($id = null){
		$id = base64_decode($id);
		$this->User->id = $id;		
		$this->data = $this->User->read();
		$password = substr(md5(time()), 0, 6); // to generate a password of 6 characters
		$encpt_newpassword = md5($password);		
		$data['User']['password']= $encpt_newpassword;
	  if($this->User->save($data['User'],false)){		
		$this->loadModel('EmailTemplate');
		$loginlink=$this->selfURL().'/users/login';
		$maillink="<a href=".$loginlink." target='_blank'>Click Here</a>";					
		$staff_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>10)));				
		$staff_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#USERNAME','#PASSWORD','#CLICKHERE'),array($this->selfURL() ,$this->data['User']['fname'],$this->data['User']['email'],$password,$maillink),$staff_temp['EmailTemplate']['mail_body']);
		$this->Email->to = $this->data['User']['email'];
		$this->Email->subject = $staff_temp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;				
		$this->Email->template = 'register_mail_by_admin'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail
		$this->set('register_temp',$staff_temp['EmailTemplate']['mail_body']);
		$this->Email->send(); //Do not pass any args to send()
		$this->Session->setFlash('Credentials has been sent successfully to ' .$this->data['User']['fname']);
		$this->redirect(array('controller'=>'inspectors','action'=>'clientlisting'));
	   }
	}
/*
****************************************************************************************
*Function Name		 :	clientandschedule
*Functionality		 :	use to show thw listing of client and inspector schedule
****************************************************************************************
*/
	 function inspector_schedule($tstamp=null){		
		#NEW CODE DONE BY VISHAL ON 05 June 2012
		$this->set('title_for_layout',"Monthly View");
		if(!isset($tstamp)){
		$tstamp=time();
		}
		$current_month_no=date('m',$tstamp);
		$current_year=date('Y',$tstamp);
		$this->set('tstamp',$tstamp);
		$type = ((isset($this->params['named']['record']))?$this->params['named']['record']:'');
		if($type!=''){
			$this->set('type',$type);
		}	
		
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];		
		$this->loadModel('ServiceCall');
		$this->loadModel('Schedule');
		$this->ServiceCall->Behaviors->attach('Containable');   
		$this->ServiceCall->contain('Schedule');
		
		$where = "Schedule.lead_inspector_id ='".$inspector_id."' OR FIND_IN_SET('".$inspector_id."', helper_inspector_id)";
		
		$res=$this->ServiceCall->find("all",array('contain'=>array('Schedule'=>array('conditions'=>array($where))),'order'=>array('ServiceCall.id DESC'),'conditions'=>array('ServiceCall.call_status'=>1),'recursive'=>2));
		//pr($res);
		$this->set("data",$res);
		$this->set('frequency',Configure::read('frequency'));
		$this->set('session_inspector_id',$inspector_id);
	 }
	 
/*
****************************************************************************************
*Function Name		 :	inspector_scheduleweeklyview
*Functionality		 :	use to show thw listing of client and inspector schedule
****************************************************************************************
*/
	 function inspector_weekly_view($tstamp=null){		
		#NEW CODE DONE BY VISHAL ON 05 June 2012
		$this->set('title_for_layout',"Monthly View");
		if(!isset($tstamp)){
		$tstamp=time();
		}
		$shift_start_date=$this->Calendar->getday($tstamp,1);
		$shift_end_date=$this->Calendar->getday($tstamp,7);
		$this->set('tstamp',$tstamp);
		
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];		
		$this->loadModel('ServiceCall');
		$this->loadModel('Schedule');
		$this->ServiceCall->Behaviors->attach('Containable');   
		$this->ServiceCall->contain('Schedule');
		
		$where = "Schedule.lead_inspector_id ='".$inspector_id."' OR FIND_IN_SET('".$inspector_id."', helper_inspector_id)";
		
		$res=$this->ServiceCall->find("all",array('contain'=>array('Schedule'=>array('conditions'=>array($where))),'order'=>array('ServiceCall.id DESC'),'conditions'=>array('ServiceCall.call_status'=>1),'recursive'=>2));
		//pr($res);
		$this->set("data",$res);
		$this->set('frequency',Configure::read('frequency'));
		$this->set('session_inspector_id',$inspector_id);
	 }	 
	 
	 
/*
****************************************************************************************
*Function Name		 :	Inspector_sign
*Functionality		 :	use for digital signature
****************************************************************************************
*/	 
	function inspector_sign()
	{
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('ServiceCall');
		$this->loadModel('Schedule');
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);		
		$this->set('servicecallId',$_REQUEST['serviceCallID']);		
		$this->set('reportId',$_REQUEST['reportID']);
		$this->loadModel('Signature');
		$datasign = $this->Signature->find('first',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'],'Signature.sp_id'=>$_REQUEST['spID'])));
		$this->set('datasign',$datasign);
		$res=$this->Schedule->find("first",array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		$this->set('res',$res);
		
	}
	 
	 
/*
****************************************************************************************
*Function Name		 :	Inspector_getSignature
*Functionality		 :	To save signaure image
****************************************************************************************
*/	 
	function inspector_getSignature()
	{		
		$this->loadModel('Signature');
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];		
		if(!empty($_POST['data']['id'])){
			$this->data['Signature']['id']=$_POST['data']['id'];
			if(!empty($_POST['output'])){
				App::import('Vendor', 'signature-to-image');
				$json = $_POST['output'];
				$name = substr(md5(time()), 0, 6);
				$name=$name.'.png';		
					
				$img = sigJsonToImage($json);
				$data=imagepng($img, WWW_ROOT.'img/signature_images/'.$name);
				$this->data['Signature']['signature_image']=$name;
				$this->data['Signature']['sp_id']=$_POST['data']['sp_id'];
				$this->data['Signature']['client_id']=$_POST['data']['client_id'];
				$this->data['Signature']['report_id']=$_POST['data']['report_id'];
				$this->data['Signature']['servicecall_id']=$_POST['data']['servicecall_id'];
				$this->data['Signature']['inspector_id']=$inspector_id;	
				$this->data['Signature']['added_by']='Inspector';
				$this->data['Signature']['user_id']=$inspector_id;
				if($this->Signature->save($this->data['Signature'],false)){
					if(!empty($_POST['data']['old_signature'])){						
						if(file_exists(WWW_ROOT.'img/signature_images/'.$_POST['data']['old_signature'])){
							unlink(WWW_ROOT.'img/signature_images/'.$_POST['data']['old_signature']); 
						}
					}
					$this->Session->setFlash('Signature saved sucessfully');
					$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));	
				}	
					imagedestroy($img);	
			}else{
				$this->Session->setFlash('sorry! some error occured');
				$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));	
			}
			
		}else{
			if(!empty($_POST['output'])){
				App::import('Vendor', 'signature-to-image');
				$json = $_POST['output'];
				$name = substr(md5(time()), 0, 6);
				$name=$name.'.png';		
					
				$img = sigJsonToImage($json);
				$data=imagepng($img, WWW_ROOT.'img/signature_images/'.$name);
				$this->data['Signature']['signature_image']=$name;
				$this->data['Signature']['sp_id']=$_POST['data']['sp_id'];
				$this->data['Signature']['client_id']=$_POST['data']['client_id'];
				$this->data['Signature']['report_id']=$_POST['data']['report_id'];
				$this->data['Signature']['servicecall_id']=$_POST['data']['servicecall_id'];
				$this->data['Signature']['inspector_id']=$inspector_id;	
				$this->data['Signature']['added_by']='Inspector';
				$this->data['Signature']['user_id']=$inspector_id;
				if($this->Signature->save($this->data['Signature'],false)){
					$this->Session->setFlash('Signature saved sucessfully');
					$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));	
				}	
					imagedestroy($img);	
			}else{
				$this->Session->setFlash('sorry! some error occured');
				$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));	
			}
		}
		
		
	}
	
	
	#Added on 27 Oct,2012 by vishal
	function inspector_vacation()
	{
		$this->layout='inspector';
		$this->set('title_for_layout',"Inspector Vacations");
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->set('inspector_id',$inspector_id);
		$this->loadModel('InspectorVacation');
		$vacation = $this->InspectorVacation->find('all',array('conditions'=>array('InspectorVacation.user_id'=>$inspector_id)));
		$this->set('vacation',$vacation);
		$this->loadModel('User');
		$sp_data = $this->User->find('first',array('conditions'=>array('User.id'=>$inspectorses['User']['sp_id']),'fields'=>array('fname','lname','email'),'recursive'=>1));
		
		if(!empty($this->data))
		{
			$data['InspectorVacation']['user_id']=$this->data['InspectorVacation']['user_id'];
			$data['InspectorVacation']['start_date']=date('Y-m-d',strtotime($this->data['InspectorVacation']['start_date']));
			$data['InspectorVacation']['end_date']=date('Y-m-d',strtotime($this->data['InspectorVacation']['end_date']));
			$this->loadModel('Schedule');
			
			$resultalreadyaligned=$this->InspectorVacation->Query("SELECT * FROM `schedules` WHERE `lead_inspector_id`='".$data['InspectorVacation']['user_id']."' AND (`schedule_date_1` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_2` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_3` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_4` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_5` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_6` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_7` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_8` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_9` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											OR `schedule_date_10` BETWEEN '".$data['InspectorVacation']['start_date']."' AND '".$data['InspectorVacation']['end_date']."'
											) 											
											");  
			
			if(!empty($resultalreadyaligned))
			{
				$YESNO='YES';
			}
			else{
				$YESNO='NO';
			}
			
			
			if($this->InspectorVacation->save($data['InspectorVacation'],false)){
					//Code to send an email to SP if inspector apply for the leaves
					$this->loadModel('EmailTemplate');
					$link = "<a href=".$this->selfURL()."/homes/login/".urlencode($sp_data['User']['email']).">Click here</a>"; 
					$temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>21)));				
					$temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#SP_NAME','#INSPECTOR','#VACATION_SDATE','#VACATION_EDATE','#YES/NO','#LINK'),array($this->selfURL() ,$sp_data['User']['fname'].' '.$sp_data['User']['lname'],$inspectorses['User']['fname'].' '.$inspectorses['User']['lname'],$this->data['InspectorVacation']['start_date'],$this->data['InspectorVacation']['end_date'],$YESNO,$link),$temp['EmailTemplate']['mail_body']);
					$this->Email->to = $sp_data['User']['email'];
					$this->Email->subject = $temp['EmailTemplate']['mail_subject'];
	/* SMTP Options */
$this->Email->smtpOptions = array(
     'port'=>'587',
     'timeout'=>'30',
     'host' => 'smtp.mandrillapp.com',
     'username'=>'admin@reportsonlineplus.com',
     'password'=>'ZfppYXZd9HHZAfLFnCPDZg',
);
		
			/* Set delivery method */ 
			$this->Email->delivery = 'smtp';
					$this->Email->from = EMAIL_FROM;				
					$this->Email->template = 'register_mail_by_admin'; // note no '.ctp'
					$this->Email->sendAs = 'both'; // because we like to send pretty mail
					$this->set('register_temp',$temp['EmailTemplate']['mail_body']);
					$this->Email->send(); //Do not pass any args to send()
					
					
					
					$this->Session->setFlash('Your Vacation dates has been saved and email notification to service provider send sucessfully');
					$this->redirect(array('controller'=>'inspectors','action'=>'inspector_vacation'));	
			}
		}
		
		
	 
	}
	
//Function addExplanation
//Desc: To add the text for the missing devices and additional devices
//Created By: Manish Kumar On jan 17, 2013
	function inspector_addExplanation(){
		$reportId = $_REQUEST['reportID'];
		$clientId = $_REQUEST['clientID'];
		$spId = $_REQUEST['spID'];		
		$servicecallId = $_REQUEST['serviceCallID'];
		
		$this->loadModel('ExplanationText');
		$textInfo = $this->ExplanationText->find('first',array('conditions'=>array('ExplanationText.servicecall_id'=>$servicecallId,'ExplanationText.report_id'=>$reportId)));		
		$this->set('textInfo',$textInfo);
		
		if(isset($this->data) && !empty($this->data)){
			if(isset($this->data['ExplanationText']['missingfile']['name']) && !empty($this->data['ExplanationText']['missingfile']['name'])){
				$temp_exp = explode('.',$this->data['ExplanationText']['missingfile']['name']);
				$extPart = end($temp_exp);
				// Start of file uploading script		
				$allowedExts = array("pdf", "xls", "docx", "xlsx","doc","png","jpg","jpeg","gif","bmp");
				if(in_array($extPart, $allowedExts)){
					$target = WWW_ROOT.'img/defImage/';
					//$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
					$randomName = md5(time()).'.'.$extPart;
					$target = $target.$randomName;
					if(move_uploaded_file($this->data['ExplanationText']['missingfile']['tmp_name'], $target)){						
						$this->data['ExplanationText']['missing_file'] = $randomName;	
					} 
				}
				else{
					$this->Session->setFlash(__('Sorry ! Please upload valid extension missing dvice file',true));
					$this->redirect($this->referer());
				}
			}
			
			if(isset($this->data['ExplanationText']['addfile']['name']) && !empty($this->data['ExplanationText']['addfile']['name'])){
				$temp_exp1 = explode('.',$this->data['ExplanationText']['addfile']['name']);
				$extPart1 = end($temp_exp1);
				// Start of file uploading script		
				$allowedExts1 = array("pdf", "xls", "docx", "xlsx","doc","png","jpg","jpeg","gif","bmp");
				if(in_array($extPart1, $allowedExts1)){
					$target1 = WWW_ROOT.'img/defImage/';
					//$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
					$randomName1 = md5(time()).'.'.$extPart1;
					$target1 = $target1.$randomName1;
					if(move_uploaded_file($this->data['ExplanationText']['addfile']['tmp_name'], $target1)){						
						$this->data['ExplanationText']['additional_file'] = $randomName1;	
					} 
				}
				else{
					$this->Session->setFlash(__('Sorry ! Please upload valid extension addional device file',true));
					$this->redirect($this->referer());
				}
			}
			$this->data['ExplanationText']['servicecall_id']=$servicecallId;
			$this->data['ExplanationText']['report_id']=$reportId;
			$this->data['ExplanationText']['client_id']=$clientId;
			$this->data['ExplanationText']['sp_id']=$spId;
			
			if(!empty($textInfo)){
				$this->data['ExplanationText']['id']=$textInfo['ExplanationText']['id'];
			}
			$this->ExplanationText->save($this->data);
			$this->Session->setFlash('Explanation has been added successfully');
			$this->redirect($this->referer());
		}
	}
	
//Function inspector_deviceList
//Desc: To show the inspection devices for the report
//Created By: Manish Kumar On jan 17, 2013
	function inspector_deviceList(){
		$reportId = $_REQUEST['reportID'];
		//$clientId = $_REQUEST['clientID'];
		//$spId = $_REQUEST['spID'];		
		$servicecallId = $_REQUEST['serviceCallID'];
		
		$this->loadModel('Schedule');
		$schData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$servicecallId,'Schedule.report_id'=>$reportId),'fields'=>array('Schedule.id'),'recursive'=>-1));		
		$schId = $schData['Schedule']['id'];
		$this->loadModel('ScheduleService');
		$this->ScheduleService->bindModel(array('belongsTo'=>array('Service')));
		$schService = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$schId),'fields'=>array('Service.name')));		
		$this->set('schService',$schService);		
	}	
	
}