<?php ob_start();
class MessagesController extends AppController {

	var $name = 'Messages';
	var $components = array('Email','Session','Cookie','Uploader.Uploader');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Fck','Session','Calendar','Common');
  

#Added for Report Online Plus
###################################################################

/*****************************************************************/ 
 #Functions For sp panel
/*****************************************************************/ 
#function name: sp_messages()
#functiion description: listing  of messages in inspector/agent panel

  function sp_messages(){
  $logininfo = $this->Session->read('Log');
    if(empty($logininfo))
    {
	 $this->redirect(array("controller" => "homes", "action" => "login"));
    }     
    $this->Message->bindModel(array('belongsTo' => array(
					'User' => array(
					     'className'    => 'User',
					     'condition' => 'Message.sender_id = User.id',
					     'foreignKey'    => 'sender_id'          
					)
				)),false); 
    $this->paginate = array(
			  'conditions'=>array('OR'=>array('Message.remove_msg'=>array("0","1")),'Message.receiver_id'=>$logininfo['User']['id']),
  			  'limit' => 10,
  			  'order' => array('Message.id' => 'DESC'),
  			  'recursive' => 1
  			);
        $data = $this->paginate('Message');	
        $this->set('msg_data', $data);
  }

/*****************************************************************/

#function name: agent_create_message()
#functiion description: to create a new messages for property owner in inspector/agent panel

  function sp_create_message(){
    if(!empty($this->data) && isset($this->data)){ 
          
      if($this->Message->save($this->data,array('validate'=>false))){
        $this->Session->setFlash('Message has been sent successfully','green');
        $this->redirect('messages');    
      }
    }
  }
  
/*****************************************************************/

#function name: autoComplete()
#functiion description: to get the records for autocomplete functionality in agent panel

  function autoComplete($textbox_id = null,$query = null){   
   // $this->layout = 'ajax';
    $this->autoRender = false;
    $suggestions = "";
    $data = "";    
    App::import("Helper", "Html");
    $this->Html = new HtmlHelper;
    
    if($query == "")
    {
	if(isset($_GET["query"]) && $_GET["query"] != "")
	{
	    $query = $_GET["query"];
	}
    }
    
    configure::write('debug',2);
    //Partial strings will come from the autocomplete field as
    $logininfo = $this->Session->read('Log');
    
    $this->loadModel('User');    
    $posts=$this->User->find('all', array(
    'conditions' => array('User.deactivate'=>1,'User.sp_id'=>$logininfo['User']['id'],'User.user_type_id'=>3,'User.fname LIKE' => $_GET['query'].'%'
    
    )
    ));    
   
    $data = array();
    $results = array();
    if(!empty($posts)){
        foreach($posts as $post){
            $userName = $post['User']['email'].'('.$post['User']['fname'].' '.$post['User']['lname'].')';
	    $results[] = '"'.$userName.'"';
	    $ids = $post['User']['id'];
	    $data[] = '"'.$ids.'"';
	}
    if($this->RequestHandler->isAjax()){
            Configure::write('debug',0);
    }
    }
	    echo $return = "{query:'$query',suggestions:[".join(',',$results)."],data:[".join(',',$data)."]}";
	    $this->autoRender = false;   
            die;
  
 }  

/*****************************************************************/

#function name: sp_sentlist()
#functiion description: listing of agent/inspector sent items

  function sp_sentlist(){
    $logininfo = $this->Session->read('Log');
    if(empty($logininfo))
    {
    $this->redirect(array("controller" => "homes", "action" => "login"));
    }  
    $this->Message->bindModel(array('belongsTo' => array(
    'User' => array(
    'className'    => 'User',
    'condition' => 'Message.receiver_id = User.id',
    'foreignKey'    => 'receiver_id'          
    )
    )),false);  
  
    $this->paginate = array(
                        'conditions'=>array('OR'=>array('Message.remove_msg'=>array("0","2")),'Message.sender_id'=>$logininfo['User']['id']),
                        'limit' => 10,
                        'order' => array('Message.id' => 'DESC'),
                        'recursive' => 1
                      );
    $data = $this->paginate('Message');
    $this->set('msg_data', $data); 
  } 
  
/*****************************************************************/

#function name: sp_show()
#functiion description: to show full message of inbox

  function sp_show($msg_id = null){
	
    $logininfo = $this->Session->read('Log');
    if(empty($logininfo)){
      $this->redirect(array("controller" => "homes", "action" => "login"));
    }     
    if(isset($msg_id) && $msg_id != ""){
      $data['Message']['id'] = $msg_id;
      $data['Message']['message_read'] = 'Y';
      $this->Message->save($data['Message']);	
      $result = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));	
      $this->set('result', $result);      
    }
  }
/*****************************************************************/

#function name: sp_reply()
#functiion description: function to reply to a inbox message  
   
  function sp_reply($msg_id = null){
    $result = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
    $this->set("result",$result);    
    if(isset($this->data) && !empty($this->data)){    
      if($this->Message->save($this->data)){
      $this->Session->setFlash('Your reply has been sent successfully','message/green');	
      $this->redirect(array("controller" => "messages", "action" => "messages"));
      }
    }
  }


/*****************************************************************/

#function name: sp_delrecievedmsg()
#functiion description: to delete messages in client panel
  function sp_delrecievedmsg($msg_id = null){
   
    if(isset($msg_id) && !empty($msg_id))
     {
	$count = $this->Message->find('count',array('conditions'=>array('Message.id'=>$msg_id,'Message.remove_msg'=>1)));
	$msgdata = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
	if($count>0){
	  
	  if($this->Message->delete($msg_id))
	  {
		$this->Session->setFlash('Message has been deleted successfully','green');	
	  $this->redirect(array("controller" => "messages", "action" => "messages"));
	  }	  
	  
	  
	}else{
	  $data['Message']['id']=$msg_id;
	  $data['Message']['remove_msg']=2;
	
	if($this->Message->save($data['Message']))
	{
	  $this->Session->setFlash('Message has been deleted successfully','green');	
	  $this->redirect(array("controller" => "messages", "action" => "messages"));
	}    
    }
    }
  }  

/*****************************************************************/

#function name: sp_delsentdmsg()
#functiion description: to delete sent msz in SP panel  

  function sp_delsentmsg($msg_id = null){
   
    if(isset($msg_id) && !empty($msg_id))
     {
	$count = $this->Message->find('count',array('conditions'=>array('Message.id'=>$msg_id,'Message.remove_msg'=>2)));
	$msgdata = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
	if($count>0){
	  
	  if($this->Message->delete($msg_id))
	  {
		$this->Session->setFlash('Message has been deleted successfully','green');	
	  $this->redirect(array("controller" => "messages", "action" => "messages"));
	  }	  
	  
	  
	}else{
	  $data['Message']['id']=$msg_id;
	  $data['Message']['remove_msg']=1;
	
	if($this->Message->save($data['Message']))
	{
	  $this->Session->setFlash('Message has been deleted successfully','green');	
	  $this->redirect(array("controller" => "messages", "action" => "messages"));
	}    
    }
    }
  }   

/*****************************************************************/ 
 #Functions For Client Panel
/*****************************************************************/     
#function name: client_messages()
#functiion description: listing  of messages in client panel

  function client_messages(){
    $this->layout = 'client';
    $logininfo = $this->Session->read('Log');
    if(empty($logininfo))
    {
      $this->redirect(array("controller" => "homes", "action" => "login"));
    }     
    $this->Message->bindModel(array('belongsTo' => array(
                      'User' => array(
                      'className'    => 'User',
                      'condition' => 'Message.sender_id = User.id',
                      'foreignKey'    => 'sender_id'          
                      )
                )),false); 
    $this->paginate = array(
                        'conditions'=>array('OR'=>array('Message.remove_msg'=>array("0","1")),'Message.receiver_id'=>$logininfo['User']['id']),
                        'limit' => 10,
                        'order' => array('Message.id' => 'DESC'),
                        'recursive' => 1
                    );
    $data = $this->paginate('Message');	
    $this->set('msg_data', $data);
  }
/*****************************************************************/

#function name: client_sentlist()
#functiion description: listing of client sent items

  function client_sentlist(){
    $this->layout = 'client';
    $logininfo = $this->Session->read('Log');
    if(empty($logininfo))
    {
    $this->redirect(array("controller" => "homes", "action" => "login"));
    }  
    $this->Message->bindModel(array('belongsTo' => array(
    'User' => array(
    'className'    => 'User',
    'condition' => 'Message.receiver_id = User.id',
    'foreignKey'    => 'receiver_id'          
    )
    )),false);  
  
    $this->paginate = array(
                        'conditions'=>array('OR'=>array('Message.remove_msg'=>array("0","2")),'Message.sender_id'=>$logininfo['User']['id']),
                        'limit' => 10,
                        'order' => array('Message.id' => 'DESC'),
                        'recursive' => 1
                      );
    $data = $this->paginate('Message');
    $this->set('msg_data', $data); 
  }
/*****************************************************************/

#function name: client_show()
#functiion description: to show full message of inbox

  function client_show($msg_id = null){
    $this->layout = 'client';
    $logininfo = $this->Session->read('Log');
    if(empty($logininfo)){
      $this->redirect(array("controller" => "homes", "action" => "login"));
    }     
    if(isset($msg_id) && $msg_id != ""){
      $data['Message']['id'] = $msg_id;
      $data['Message']['message_read'] = 'Y';
      $this->Message->save($data['Message']);	
      $result = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));	
      $this->set('result', $result);      
    }
  }
/*****************************************************************/

#function name: client_reply()
#functiion description: function to reply to a inbox message  
   
  function client_reply($msg_id = null){
    $this->layout = 'client';
    $result = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
    $this->set("result",$result);    
    if(isset($this->data) && !empty($this->data)){    
      if($this->Message->save($this->data)){
      $this->Session->setFlash('Your reply has been sent successfully','message/green');	
      $this->redirect(array("controller" => "messages", "action" => "messages"));
      }
    }
  } 

/*****************************************************************/

#function name: client_create_message()
#functiion description: to create a new messages for property owner in client panel

  function client_create_message(){
    $logininfo = $this->Session->read('Log');
    if(empty($logininfo)){
      $this->redirect(array("controller" => "homes", "action" => "login"));
    }else{
      
      if(!empty($this->data) && isset($this->data)){           
        if($this->Message->save($this->data,array('validate'=>false))){
          $this->Session->setFlash('Message has been sent successfully','green');
          $this->redirect('messages');    
        }
      } else{
        $this->loadModel('User');        
        //$data=$this->User->find('first',array('conditions'=>array('User.id'=>$logininfo['User']['id']),'fields'=>'User.sp_id'));
        //$this->set('spdata',$data);
        $fulldata=$this->User->find('all',array('conditions'=>array('User.user_type_id'=>1),'fields'=>array('User.id','User.fname','User.lname','User.email')));
	$this->set('spAllData',$fulldata);
      }  
    
    }  
  } 

/*****************************************************************/

#function name: client_create_message()
#functiion description: to create a new messages for property owner in client panel
  function client_delrecievedmsg($msg_id = null){
   
    if(isset($msg_id) && !empty($msg_id))
     {
	$count = $this->Message->find('count',array('conditions'=>array('Message.id'=>$msg_id,'Message.remove_msg'=>1)));
	$msgdata = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
	if($count>0){
	  
	  if($this->Message->delete($msg_id))
	  {
		$this->Session->setFlash('Message has been deleted successfully','green');	
	  $this->redirect(array("controller" => "messages", "action" => "messages"));
	  }	  
	  
	  
	}else{
	  $data['Message']['id']=$msg_id;
	  $data['Message']['remove_msg']=2;
	
	if($this->Message->save($data['Message']))
	{
	  $this->Session->setFlash('Message has been deleted successfully','green');	
	  $this->redirect(array("controller" => "messages", "action" => "messages"));
	}    
    }
    }
  }  


/*****************************************************************/

#function name: client_delsentdmsg()
#functiion description: to delete sent msz in client panel  

  function client_delsentmsg($msg_id = null){
   
    if(isset($msg_id) && !empty($msg_id))
     {
	$count = $this->Message->find('count',array('conditions'=>array('Message.id'=>$msg_id,'Message.remove_msg'=>2)));
	$msgdata = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
	if($count>0){
	  
	  if($this->Message->delete($msg_id))
	  {
		$this->Session->setFlash('Message has been deleted successfully','green');	
	  $this->redirect(array("controller" => "messages", "action" => "messages"));
	  }	  
	  
	  
	}else{
	  $data['Message']['id']=$msg_id;
	  $data['Message']['remove_msg']=1;
	
	if($this->Message->save($data['Message']))
	{
	  $this->Session->setFlash('Message has been deleted successfully','green');	
	  $this->redirect(array("controller" => "messages", "action" => "messages"));
	}    
    }
    }
  }
/*****************************************************************/

#function name: sp_sendMessage()
#functiion description: to send message from sp to client   
  function sp_sendMessage($clientId= null){
  $clientId= base64_decode($clientId);
  $this->set('clientId',$clientId);   
      if(isset($this->data) && !empty($this->data)){
        if($this->Message->save($this->data,array('validate'=>false))){
      	  $this->Session->setFlash('Message Sent Successfully');	
      	  $this->redirect(array("controller" => "sps", "action" => "clientlisting"));
	     }
      }
  }
    


  
###################################################################
#Added for Report Online Plus(END)






#FUNCTION TO LIST THE MESSAGES IN EMPLOYEE SECTION DASHBOARD
  function employee_messages()
  {
      $this->layout='employee';
      $this->loadModel('Message');
      $count_new_message = $this->Message->find('count',array('conditions'=>array('Message.receiver_id'=>$this->Session->read('EmployeeId'),'Message.message_read'=>'N','Message.receiver_type'=>'Employee')));
      $this->set('count_new_message',$count_new_message);
      $this->paginate = array(
                       'conditions'=>array('Message.receiver_id'=>$this->Session->read('EmployeeId'),'Message.receiver_type'=>'Employee'),
                        'limit' => 30,
                        'order' => array(
                        'Message.id' => 'DESC'
                                        ));
    $msg_data = $this->paginate('Message');    
    $this->set('msg_data',$msg_data);
    if(isset($_POST['delete']))      
	  {
	   	   $this->employee_delete_message();           // Delete
	  }   
    
  }
  
   #FUNCTION TO MOVE MESSAGE TO DELETE IN EMPLOYEE SECTION DASHBOARD
    function employee_delete_message()
    {   
    
     $this->loadModel('Message');
      for($i=0; $i<count($_POST['box']); $i++)
    	{    		
    		 $this->Message->id = $_POST['box'][$i];			   
			   $this->Message->delete($this->Message->id);
    	} 
    	
     $this->Session->setFlash("Message deleted successfully");	
	   $this->redirect("messages");
    }
  
  #FUNCTION TO VIEW Detail THE MESSAGES IN EMPLOYEE SECTION DASHBOARD
  function employee_view_message($msg_id=null) {
    $this->layout='employee';
    $this->loadModel('Message');
    $this->Message->id = $msg_id;
		$data['Message']['message_read'] = 'Y';			   
		$this->Message->save($data); // UPDATE MESSAGE AS READ 
		$msg_data = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
    $this->set('msg_data',$msg_data);
    
	}
	
	#FUNCTION TO REPLY THE MESSAGES IN EMPLOYEE SECTION DASHBOARD
  function employee_reply_message($msg_id=null) {
    $this->layout='employee';
    $this->loadModel('Message');    
    $msg_data = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
    $this->set('msg_data',$msg_data);
    if(!empty($this->data))
    {
      		/*** CODE TO SAVE THE MESSAGE IN MESSAGE MODEL ***/
      		$this->loadModel('Message');
      		$this->Message->create();
      		$data['Message']['sender_type']='Employee';
      		$data['Message']['receiver_type']=$this->data['Message']['receiver_type'];
      		$data['Message']['receiver_id']=$this->data['Message']['receiver_id'];
      		$data['Message']['sender_id']=$this->data['Message']['sender_id'];
      		//$data['Message']['company_id']=$this->Session->read('Company_ID');
      		$data['Message']['message_subject']=$this->data['Message']['message_subject'];
      		$data['Message']['message_body']=$this->data['Message']['message_body'];
          $this->Message->id=null;    		
          $this->Message->save($data['Message']);
      		/** END CODE TO SAVE MESSAGE IN MESSAGE MODEL ***/             		
       
       $this->Session->setFlash(__('Message has been sent successfully',true));
       $this->redirect('messages');
    }
    
	}
	
	# FUNCTION TO WRITE A NEW MESSAGE IN EMPLOYEE SECTION DASHBOARD
  function employee_write_message() {
    $this->layout='employee';
    $this->loadModel('Message');
    $this->set('To',$this->to_for_employee($this->Session->read('EmployeeId')));
    if(!empty($this->data))
    {
      		/*** CODE TO SAVE THE MESSAGE IN MESSAGE MODEL ***/
      	
      		$this->loadModel('Message');
      		$this->Message->create();
      		$data['Message']['sender_type']='Employee';
      		$data['Message']['sender_id']=$this->Session->read('EmployeeId');
      		$test=explode('_',$this->data['Message']['receiver_id']);
      		if($test[1]=='MANAGER')
      		{
      		  $data['Message']['receiver_type']='Manager';
      		  $data['Message']['receiver_id']=$test[0];  
          }
          if($test[1]=='EMP')
      		{
      		  $data['Message']['receiver_type']='Employee';
      		  $data['Message']['receiver_id']=$test[0];  
          }
      		
      		$data['Message']['message_subject']=$this->data['Message']['message_subject'];
      		$data['Message']['message_body']=$this->data['Message']['message_body'];
          $this->Message->id=null;          		
          $this->Message->save($data['Message']);
      		/** END CODE TO SAVE MESSAGE IN MESSAGE MODEL ***/             		
       
       $this->Session->setFlash(__('Message has been sent successfully',true));
       $this->redirect('messages');
    }
	}
	
	/************************************************************/
	
	#FUNCTION TO LIST THE MESSAGES IN MANAGER SECTION DASHBOARD
  function manager_messages()
  {
      $this->layout='manager';
      $this->loadModel('Message');
      $count_new_message = $this->Message->find('count',array('conditions'=>array('Message.receiver_id'=>$this->Session->read('OwnerId'),'Message.message_read'=>'N','Message.receiver_type'=>'Manager')));
      $this->set('count_new_message',$count_new_message);
      $this->paginate = array(
                       'conditions'=>array('Message.receiver_id'=>$this->Session->read('OwnerId'),'Message.receiver_type'=>'Manager'),
                        'limit' => 30,
                        'order' => array(
                        'Message.id' => 'DESC'
                                        ));
    $msg_data = $this->paginate('Message');    
    $this->set('msg_data',$msg_data);
    if(isset($_POST['delete']))      
	  {
	   	   $this->manager_delete_message();           // Delete
	  }   
    
  }
  
   #FUNCTION TO MOVE MESSAGE TO DELETE IN MANAGER SECTION DASHBOARD
    function manager_delete_message()
    { 
     
     $this->loadModel('Message');
      for($i=0; $i<count($_POST['box']); $i++)
    	{    		
    		 $this->Message->id = $_POST['box'][$i];			   
			   $this->Message->delete($this->Message->id);
    	} 
    	
     $this->Session->setFlash("Message deleted successfully");	
	   $this->redirect("messages");
    }
  
  #FUNCTION TO VIEW Detail THE MESSAGES IN MANAGER SECTION DASHBOARD
  function manager_view_message($msg_id=null) {
    $this->layout='manager';
    $this->loadModel('Message');
    $this->Message->id = $msg_id;
		$data['Message']['message_read'] = 'Y';			   
		$this->Message->save($data); // UPDATE MESSAGE AS READ 
		$msg_data = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
    $this->set('msg_data',$msg_data);
    
	}
	
	#FUNCTION TO REPLY THE MESSAGES IN MANAGER SECTION DASHBOARD
  function manager_reply_message($msg_id=null) {
    $this->layout='manager';
    $this->loadModel('Message');    
    $msg_data = $this->Message->find('first',array('conditions'=>array('Message.id'=>$msg_id)));
    $this->set('msg_data',$msg_data);
    if(!empty($this->data))
    {
      		/*** CODE TO SAVE THE MESSAGE IN MESSAGE MODEL ***/
      		$this->loadModel('Message');
      		$this->Message->create();
      		$data['Message']['sender_type']='Manager';
      		$data['Message']['receiver_type']=$this->data['Message']['receiver_type'];
      		$data['Message']['receiver_id']=$this->data['Message']['receiver_id'];
      		$data['Message']['sender_id']=$this->data['Message']['sender_id'];
      		//$data['Message']['company_id']=$this->Session->read('Company_ID');
      		$data['Message']['message_subject']=$this->data['Message']['message_subject'];
      		$data['Message']['message_body']=$this->data['Message']['message_body'];
          $this->Message->id=null;    		
          $this->Message->save($data['Message']);
      		/** END CODE TO SAVE MESSAGE IN MESSAGE MODEL ***/             		
       
       $this->Session->setFlash(__('Message has been sent successfully',true));
       $this->redirect('messages');
    }
    
	}
	
	# FUNCTION TO WRITE A NEW MESSAGE IN MANAGER SECTION DASHBOARD
  function manager_write_message() {
    $this->layout='manager';
    $this->loadModel('Message');
    
   // print_r($_SESSION);
    //echo $this->Session->read('OwnerId');
    $this->set('To',$this->to_for_manager($this->Session->read('OwnerId')));
    if(!empty($this->data))
    {
      		/*** CODE TO SAVE THE MESSAGE IN MESSAGE MODEL ***/
      	
      		$this->loadModel('Message');
      		$this->Message->create();
      		$data['Message']['sender_type']='Manager';
      		//$data['Message']['sender_id']=$this->Session->read('EmployeeId');
			$data['Message']['sender_id']=$this->Session->read('OwnerId');
      		$test=explode('_',$this->data['Message']['receiver_id']);
      		if($test[1]=='MANAGER')
      		{
      		  $data['Message']['receiver_type']='Manager';
      		  $data['Message']['receiver_id']=$test[0];  
          }
          if($test[1]=='EMP')
      		{
      		  $data['Message']['receiver_type']='Employee';
      		  $data['Message']['receiver_id']=$test[0];  
          }
      		
      		$data['Message']['message_subject']=$this->data['Message']['message_subject'];
      		$data['Message']['message_body']=$this->data['Message']['message_body'];
          $this->Message->id=null;          		
          $this->Message->save($data['Message']);
      		/** END CODE TO SAVE MESSAGE IN MESSAGE MODEL ***/             		
       
       $this->Session->setFlash(__('Message has been sent successfully',true));
       $this->redirect('messages');
    }
	}
	
	
	 function to_for_employee($emp_id=null) {
  $arr=array();
  $this->loadModel('Employee');
  $this->loadModel('Company');
  $this->loadModel('Owner');
  $emp_data = $this->Employee->find('first',array('conditions'=>array('Employee.id'=>$emp_id)));
  $comp_id=$emp_data['Employee']['company_id'];
  $comp_data = $this->Company->find('first',array('conditions'=>array('Company.id'=>$comp_id)));
  $manager_ids=$comp_data['Company']['owner_id'];
  $owner_data = $this->Owner->find('all',array('conditions'=>array('Owner.id'=>$manager_ids)));
  foreach($owner_data as $key =>$owner_data):
  $owner_data['Owner']['id']=$owner_data['Owner']['id'].'_MANAGER';
  $arr[$owner_data['Owner']['id']]='Manager : '.$owner_data['Owner']['fname'].' '.$owner_data['Owner']['lname'];
  
  endforeach;
  
  $rest_emp_data = $this->Employee->find('all',array('conditions'=>array('Employee.company_id'=>$comp_id,'Employee.id<>'.$emp_id),'fields'=>array('Employee.id','Employee.fname','Employee.lname','Employee.email','Employee.company_id')),array('recursive'=>1));
  //pr($rest_emp_data);
  foreach($rest_emp_data as $key =>$rest_emp_data):
  $rest_emp_data['Employee']['id']=$rest_emp_data['Employee']['id'].'_EMP';
  $arr[$rest_emp_data['Employee']['id']]=$rest_emp_data['Employee']['fname'].' '.$rest_emp_data['Employee']['lname'];;
  
  endforeach;
  return $arr;
  
  }
  
  function to_for_manager($owner_id=null) {
  $arr=array();
  $this->loadModel('Employee');
  $this->loadModel('Company');
  $this->loadModel('Owner');
  $comp_data = $this->Company->find('first',array('conditions'=>array('Company.owner_id'=>$owner_id)));
  $comp_id=$comp_data['Company']['id'];  
  $emp_data = $this->Employee->find('all',array('conditions'=>array('Employee.company_id'=>$comp_id)));
  foreach($emp_data as $key =>$emp_data):
  $emp_data['Employee']['id']=$emp_data['Employee']['id'].'_EMP';
  $arr[$emp_data['Employee']['id']]=$emp_data['Employee']['fname'].' '.$emp_data['Employee']['lname'];
  
  endforeach;
  
  return $arr;
  
  }
  
  
  #Made on 27th Oct,2012
  function sp_inspector_leave_request()
  {
		$this->set('title_for_layout',"Inspector Vacations Request");
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->set('inspector_id',$inspector_id);
		$this->loadModel('InspectorVacation');
		$this->loadModel('User');
		$this->InspectorVacation->bindModel(
                   array(
                     'belongsTo' => array(
                         'User' => array(
                            'className' => 'User',
							'foreignKey'=>'user_id'                           
                         )
                     )
				));

		$vacation = $this->InspectorVacation->find('all',array('orders'=>array('InspectorVacation.id')));
		$this->set('vacation',$vacation);
  }
  
  #made on 16th Nov-2012 by vishal
  function sp_blank_cert_form()
  {
	$this->set('title_for_layout',"Upload Blank Cert Forms");
	$logininfo = $this->Session->read('Log');
	if(empty($logininfo))
	{
		$this->redirect('/homes/splogin');
	}	
	$sp_id = $logininfo['User']['id'];
	$this->loadModel('SpBlankCertForm');
	
	$this->paginate = array(
                       'conditions'=>array('SpBlankCertForm.sp_id'=>$sp_id),
                        'limit' => 30,
                        'order' => array(
                        'SpBlankCertForm.id' => 'DESC'
                                        ));
    $res = $this->paginate('SpBlankCertForm');    
    $this->set('resCertForm',$res);
	
	$this->set('reports',$this->getAllReportsListing());
	if(!empty($this->data))
	{
		if(isset($this->data['SpBlankCertForm']['cert_form']['name']) && !empty($this->data['SpBlankCertForm']['cert_form']['name'])){
		    $temp_exp = explode('.',$this->data['SpBlankCertForm']['cert_form']['name']);
			// Start of file uploading script		
			$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
			if(in_array($temp_exp[1], $allowedExts)){
				$target = WWW_ROOT.'blank_cert/';
				//$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
				$target = $target.$this->data['SpBlankCertForm']['cert_form']['name'];
				if(move_uploaded_file($this->data['SpBlankCertForm']['cert_form']['tmp_name'], $target)){
					$data['SpBlankCertForm']['title'] = $this->data['SpBlankCertForm']['title'];
					$data['SpBlankCertForm']['cert_form'] = $this->data['SpBlankCertForm']['cert_form']['name'];
					$data['SpBlankCertForm']['sp_id']=$sp_id;
					$data['SpBlankCertForm']['report_id']=$this->data['SpBlankCertForm']['report_id'];
					$this->SpBlankCertForm->save($data['SpBlankCertForm']);
					$this->Session->setFlash(__('Cert Form has been saved successfully',true));
					$this->redirect('blank_cert_form');
				} 
			}
			else{
				$this->Session->setFlash(__('Sorry ! Please upload only pdf/xls/doc/xlsx/docx file',true));
				$this->redirect('blank_cert_form');
			}
		}
	}
	
	if(isset($_POST['active']))      
	{
		 $this->sp_active_cert_form();           // active
	}
	if(isset($_POST['inactive']))      
	{
		 $this->sp_inactive_cert_form();            // inactive
	} 
	
	
  }
  
  function sp_active_cert_form()
  {
	$logininfo = $this->Session->read('Log');
	if(empty($logininfo))
	{
		$this->redirect('/homes/splogin');
	}	
	$this->loadModel('SpBlankCertForm');
      for($i=0; $i<count($_POST['box']); $i++)
    	{    		
    		 $this->SpBlankCertForm->id = $_POST['box'][$i];
			 $data['SpBlankCertForm']['status']='Active';
			 $this->SpBlankCertForm->save($data['SpBlankCertForm']);
    	} 
    	
     $this->Session->setFlash("Cert Form has been activated successfully");	
	 $this->redirect("blank_cert_form");
  }
  
  function sp_inactive_cert_form()
  {
	$logininfo = $this->Session->read('Log');
	if(empty($logininfo))
	{
		$this->redirect('/homes/splogin');
	}	
	$this->loadModel('SpBlankCertForm');
      for($i=0; $i<count($_POST['box']); $i++)
    	{    		
    		 $this->SpBlankCertForm->id = $_POST['box'][$i];
			 $data['SpBlankCertForm']['status']='Inactive';
			$this->SpBlankCertForm->save($data['SpBlankCertForm']);
    	} 
    	
     $this->Session->setFlash("Cert Form has been inactivated successfully");	
	 $this->redirect("blank_cert_form");
  }
  
  
  function sp_download_cert_form($fileName = null){
		$mimeType = array('bmp'=>'image/bmp','docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','gif'=>'image/gif','jpg'=>'image/jpeg','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT."blank_cert/"
		);
		$this->set($params);
	}
	
    function inspector_download_cert_form($fileName = null){
		$mimeType = array('bmp'=>'image/bmp','docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','gif'=>'image/gif','jpg'=>'image/jpeg','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT."blank_cert/"
		);
		$this->set($params);
	}
	
    function inspector_download_self_form($fileName = null){
		$mimeType = array('docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT."ins_report_cert/"
		);
		$this->set($params);
	}
	
	//Created By Manish Kumar
	//On November 20, 2012
	function inspector_deleteFile($fileName = null){
		if(file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'ins_report_cert'.DS.$fileName)){
			unlink(ROOT.DS.'app'.DS.'webroot'.DS.'ins_report_cert'.DS.$fileName);
			$this->loadModel('ReportCert');
			$this->ReportCert->updateAll(array('ReportCert.ins_cert_title'=>'""','ReportCert.ins_cert_form'=>'""'),array('ReportCert.ins_cert_form'=>$fileName));
			echo 'OK';
		}else{
			echo 'ERROR';
		}
		die;
	}
  
  
  
  #made on 16th Nov-2012 by vishal
  function sp_general_company_announcement()
  {
	$this->set('title_for_layout',"General Company Announcement");
	$logininfo = $this->Session->read('Log');
	$sp_id = $logininfo['User']['id'];
	$this->loadModel('SpGeneralCompanyAnnouncement');
	$res=$this->SpGeneralCompanyAnnouncement->find('first',array('conditions'=>array('SpGeneralCompanyAnnouncement.sp_id'=>$sp_id)));
	$this->set('res',$res);
    
	if(!empty($this->data))
	{
		if(!empty($res))
		{
			$this->SpGeneralCompanyAnnouncement->id=$res['SpGeneralCompanyAnnouncement']['id'];
		}
		else
		{
		$this->SpGeneralCompanyAnnouncement->id=null;
		}
		$data['SpGeneralCompanyAnnouncement']['sp_id']=$sp_id;
		$data['SpGeneralCompanyAnnouncement']['for_inspectors']=$this->data['SpGeneralCompanyAnnouncement']['for_inspectors'];
		$data['SpGeneralCompanyAnnouncement']['for_clients']=$this->data['SpGeneralCompanyAnnouncement']['for_clients'];
		$this->SpGeneralCompanyAnnouncement->save($data['SpGeneralCompanyAnnouncement']);
		$this->Session->setFlash(__('General Company Announcement has been saved successfully',true));
        $this->redirect('general_company_announcement');
	}
	
	
  }
  
  #made on 19th Nov-2012 by Manish Kumar
  function inspector_uploadReportCert($repCertId = null)
  {
	$this->set('title_for_layout',"Upload Cert Forms");
	$logininfo = $this->Session->read('Log');
	if(empty($logininfo))
	{
		$this->redirect('/homes/inspectorlogin');
	}	
	$ins_id = $logininfo['User']['id'];
	$this->loadModel('ReportCert');
	
	$res = $this->ReportCert->findById($repCertId);    
	$this->set('resCertForm',$res);
	
	if(!empty($this->data))
	{
		if(isset($this->data['ReportCert']['ins_cert_form']['name']) && !empty($this->data['ReportCert']['ins_cert_form']['name'])){
		    $temp_exp = explode('.',$this->data['ReportCert']['ins_cert_form']['name']);
		    $extPart = end($temp_exp);
			// Start of file uploading script		
			$allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
			if(in_array($extPart, $allowedExts)){
				$target = WWW_ROOT.'ins_report_cert/';
				//$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
				$randomName = md5(time()).'.'.$extPart;
				$target = $target.$randomName;
				if(move_uploaded_file($this->data['ReportCert']['ins_cert_form']['tmp_name'], $target)){
					$data['ReportCert']['ins_cert_title'] = $this->data['ReportCert']['ins_cert_title'];
					$data['ReportCert']['ins_cert_form'] = $randomName;
					$data['ReportCert']['id']=$repCertId;
					$this->ReportCert->save($data['ReportCert']);
					$this->Session->setFlash(__('Cert Form has been saved successfully',true));
					//$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
					$this->redirect($this->referer());
				} 
			}
			else{
				$this->Session->setFlash(__('Sorry ! Please upload only pdf/xls/doc/xlsx/docx file',true));
				$this->redirect($this->referer());
			}
		}
	}	
	
  }
  
  #made on 29th Nov-2012 by Manish Kumar
  function sp_chkUsed($certId = null){
	if($certId!=null){
		$this->loadModel('ReportCert');
		$countRecords = $this->ReportCert->find('count',array('conditions'=>array('ReportCert.cert_id'=>$certId)));
		if($countRecords>0){
			echo 'OK';
		}else{
			echo 'NO';
		}
		die;
	}
  }
  
  function sp_deleteCert($certId = null){
	if($certId!=null){
		$this->loadModel('SpBlankCertForm');
		$record = $this->SpBlankCertForm->findById($certId,array('SpBlankCertForm.cert_form'));
		$this->SpBlankCertForm->delete($certId);
		if(file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'blank_cert'.DS.$record['SpBlankCertForm']['cert_form'])){
			unlink(ROOT.DS.'app'.DS.'webroot'.DS.'blank_cert'.DS.$record['SpBlankCertForm']['cert_form']);
		}
		
		$this->loadModel('ReportCert');
		$allRecords = $this->ReportCert->find('all',array('conditions'=>array('ReportCert.cert_id'=>$certId),'fields'=>array('ReportCert.id','ReportCert.ins_cert_form')));
		if(count($allRecords)>0){
			foreach($allRecords as $datas){
				if($datas['ReportCert']['ins_cert_form']!=""){
					if(file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'ins_report_cert'.DS.$datas['ReportCert']['ins_cert_form'])){
						unlink(ROOT.DS.'app'.DS.'webroot'.DS.'ins_report_cert'.DS.$datas['ReportCert']['ins_cert_form']);
					}
				}	
				$this->ReportCert->delete($datas['ReportCert']['id']);
			}
		}
	}
	$this->Session->setFlash('Record has been deleted successfully.');
	$this->redirect(array('controller'=>'messages','action'=>'blank_cert_form'));
  }
}