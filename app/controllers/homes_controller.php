<?php
class HomesController extends AppController{
	var $name = 'Homes';
	var $uses = null;
	var $layout='frontend';
	var $components = array('Email','Session','Cookie','Captcha');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Calendar','Common');
	
/***************************************************************************************
	Home controller index function 
****************************************************************************************/ 	
	function index(){
		$this->layout='indexlayout';
		$this->loadModel('Banner');
		$this->loadModel('MainTab');
		$this->set('title_for_layout',"Report Online plus -- Home Page");
		$bannerData=$this->Banner->find('first',array('conditions'=>array('Banner.status'=>1),'order'=>'rand()'));
		$this->set('bannerData',$bannerData);
		$tabData=$this->MainTab->find('first');
		$this->set('tabData',$tabData);		
		$this->loadModel('HomeVideo');
		$video=$this->HomeVideo->find('first',array('order'=>array('HomeVideo.id DESC')));
		$this->set(compact('video'));		
	}
	 function samplereport(){
		$this->layout='indexlayout';
		$this->loadModel('SampleReport');
		$data=$this->SampleReport->find('all');
		$this->set('data',$data);
	 }
/***************************************************************************************
	#THIS FUNCTION IS USED TO DOWNLOAD A ATTACHMENT 
****************************************************************************************/ 	  
	function download ($fileName = null){
		$mimeType = array('bmp'=>'image/bmp','docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','gif'=>'image/gif','jpg'=>'image/jpeg','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel','xlsx'=>'application/vnd.ms-excel');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT."img/sample_reports/"
		);
		$this->set($params);
	}
/***************************************************************************************
 #function to add new website pages	
****************************************************************************************/  
   function pages($id=null){
    $this->layout='indexlayout';
    $this->loadModel('Websitepage');
    $data =  $this->Websitepage->find('first',array('conditions'=>array('Websitepage.id'=>$id)));
    $this->set('websitepages',$data);
    if($id==81)
    {
	$this->loadModel('SubscriptionDuration');
	$subData = $this->SubscriptionDuration->find('all',array('orders'=>array('SubscriptionDuration.id')));
	$this->set(compact('subData'));
    }
   }	
	
	#THIS FUNCTION IS USED FOR MANAGER / EMPLOYEE LOGIN AREA IN A SINGLE FORM
/***************************************************************************************
	Home controller index function 
****************************************************************************************/ 
	function cchecklogin(){
		$this->loadModel('User');
		$cookie = $this->Cookie->read('Log.User');

		if (!is_null($cookie)) {
		$cookie = explode("^", $cookie['remembertoken']);

$logindata = $this->User->find('first',array('conditions'=>array('User.remembertoken'=> $cookie[0], 'User.deactivate'=>1)));

if($logindata['User']['password'] == $cookie[1]){

	$this->Session->write('Log', $logindata);
				if($logindata['User']['user_type_id'] == 1){
				$this->Session->write('Spid', $logindata['User']['id']);
					$this->Session->setFlash('Logged in as Service Provider');
					$this->redirect("/sp/sps/dashboard");
				}elseif($logindata['User']['user_type_id'] == 2){
				$this->Session->write('inspectorid', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Fire Inspector');
					$this->redirect("/inspector/inspectors/dashboard");
				}
				elseif($logindata['User']['user_type_id'] == 3){
				$this->Session->write('client_id', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Client');
					$this->redirect("/client/clients/dashboard");
}

			} else { $this->Cookie->delete('Log.User'); }
		}
			else{

				$this->Session->setFlash('');
				$this->redirect(array("controller" => "homes", "action" => "login"));
			}
	
}

	function login($urlemail = null){
		$this->layout='indexlayout';
		$this->set('title_for_layout',"Report Online plus -- Login");
		$this->loadModel('User');
		if($urlemail != null){
			$this->set('urlemail', urldecode($urlemail));
		}

$cookie = $this->Cookie->read('Log.User');

		if($this->Session->read('Log.User')){
			$this->redirect("/");
		}
		if($cookie) {
			$this->cchecklogin();
		}
		if(!empty($this->data) && isset($this->data)){
			
			$logindata = $this->User->find('first',array('conditions'=>array('User.password'=> md5($this->data['User']['password']),'User.email'=>$this->data['User']['email'],'User.deactivate'=>1),'recursive'=>2));
			if(!empty($logindata)){
if(isset($this->data['User']['remember_me']) && !empty($this->data['User']['remember_me']))
				{
					
	$cookie = array();
	$cookie['remembertoken'] = md5($this->data['User']['email']) . "^" . md5($this->data['User']['password']);
	$data['User']['remembertoken'] = md5($this->data['User']['email']);
	$data['User']['deactivate'] = 1;
	$this->User->create();
	$this->User->id = $logindata['User']['id'];
	$this->User->save($data);
	$this->Cookie->write('Log.User', $cookie, false, '+2 weeks');
			}
				$this->Session->write('Log', $logindata);
				if($logindata['User']['user_type_id'] == 1){
				$this->Session->write('Spid', $logindata['User']['id']);
					$this->Session->setFlash('Logged in as Service Provider');
					$this->redirect("/sp/sps/dashboard");
				}elseif($logindata['User']['user_type_id'] == 2){
				$this->Session->write('inspectorid', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Fire Inspector');
					$this->redirect("/inspector/inspectors/dashboard");
				}
				elseif($logindata['User']['user_type_id'] == 3){
				$this->Session->write('client_id', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Client');
					$this->redirect("/client/clients/dashboard");
			}			
			}else{
				$this->Session->setFlash('Either Email or Password is incorrect.');
				$this->redirect(array("controller" => "homes", "action" => "login"));
			}
		}
  }
  
  
  function clientlogin(){
	$session_user=$this->Session->read('Log');
	if(!empty($session_user))
	{
	$this->redirect('/');	
	}
		$this->layout='indexlayout';
		$this->set('title_for_layout',"Report Online plus -- Client Login");
		$this->loadModel('User');
		if(!empty($this->data) && isset($this->data)){
			$logindata = $this->User->find('first',array('conditions'=>array('User.password'=> md5($this->data['User']['password']),'User.email'=>$this->data['User']['email'],'User.deactivate'=>1)));
			if(!empty($logindata)){
				$this->Session->write('Log', $logindata);
				if($logindata['User']['user_type_id'] == 1){
				$this->Session->write('Spid', $logindata['User']['id']);
					$this->Session->setFlash('Logged in as Service Provider');
					$this->redirect("/sp/sps/dashboard");
				}elseif($logindata['User']['user_type_id'] == 2){
				$this->Session->write('inspectorid', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Fire Inspector');
					$this->redirect("/inspector/inspectors/dashboard");
				}
				elseif($logindata['User']['user_type_id'] == 3){
				$this->Session->write('client_id', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Client');
					$this->redirect("/client/clients/dashboard");
			}			
			}else{
				$this->Session->setFlash('Either Email or Password is incorrect.');
				$this->redirect(array("controller" => "homes", "action" => "clientlogin"));
			}
		}
  }
  
  
  function splogin(){
	$session_user=$this->Session->read('Log');
	if(!empty($session_user))
	{
	//$this->redirect('/');	
	}
		$this->layout='indexlayout';
		$this->set('title_for_layout',"Report Online plus -- Service Provider Login");
		$this->loadModel('User');
		if(!empty($this->data) && isset($this->data)){
			$logindata = $this->User->find('first',array('conditions'=>array('User.password'=> md5($this->data['User']['password']),'User.email'=>$this->data['User']['email'],'User.deactivate'=>1),'recursive'=>2));
			if(!empty($logindata)){
				$this->Session->write('Log', $logindata);
				if($logindata['User']['user_type_id'] == 1){
				$this->Session->write('Spid', $logindata['User']['id']);
					$this->Session->setFlash('Logged in as Service Provider');
					$this->redirect("/sp/sps/dashboard");
				}elseif($logindata['User']['user_type_id'] == 2){
				$this->Session->write('inspectorid', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Fire Inspector');
					$this->redirect("/inspector/inspectors/dashboard");
				}
				elseif($logindata['User']['user_type_id'] == 3){
				$this->Session->write('client_id', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Client');
					$this->redirect("/client/clients/dashboard");
			}			
			}else{
				$this->Session->setFlash('Either Email or Password is incorrect.');
				$this->redirect(array("controller" => "homes", "action" => "splogin"));
			}
		}
  }
  
  function inspectorlogin(){
	$session_user=$this->Session->read('Log');
	if(!empty($session_user))
	{
	$this->redirect('/');	
	}
		$this->layout='indexlayout';
		$this->set('title_for_layout',"Report Online plus -- Inspector Login");
		$this->loadModel('User');
		if(!empty($this->data) && isset($this->data)){
			$logindata = $this->User->find('first',array('conditions'=>array('User.password'=> md5($this->data['User']['password']),'User.email'=>$this->data['User']['email'],'User.deactivate'=>1)));
			if(!empty($logindata)){
				$this->Session->write('Log', $logindata);
				if($logindata['User']['user_type_id'] == 1){
				$this->Session->write('Spid', $logindata['User']['id']);
					$this->Session->setFlash('Logged in as Service Provider');
					$this->redirect("/sp/sps/dashboard");
				}elseif($logindata['User']['user_type_id'] == 2){
				$this->Session->write('inspectorid', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Fire Inspector');
					$this->redirect("/inspector/inspectors/dashboard");
				}
				elseif($logindata['User']['user_type_id'] == 3){
				$this->Session->write('client_id', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Client');
					$this->redirect("/client/clients/dashboard");
			}			
			}else{
				$this->Session->setFlash('Either Email or Password is incorrect.');
				$this->redirect(array("controller" => "homes", "action" => "inspectorlogin"));
			}
		}
  }
/***************************************************************************************
 Forget password functionality for front end users
****************************************************************************************/
function forgotpassword(){
	$this->layout='indexlayout';
	$this->loadModel('User');
	if(isset($this->data) && !empty($this->data)){
		if(isset($this->data['User']['answer'])){
			$ans =$this->User->find('first', array('conditions'=>array('User.deactivate' => 1,'User.email'=>$this->data['User']['email'])));
		if($ans['User']['answer'] == $this->data['User']['answer'])
		{
				$this->set('newpass', $ans['User']['email']);
		}else{
			$ans['ans'] = 'wrong';
			$this->set('question', $ans);
		}
		}
			else{
		$u_data = $this->User->find('first', array('conditions'=>array('User.deactivate' => 1,'User.email'=>$this->data['User']['email'])));
		if($u_data){
		$this->set('question', $u_data);
	}else{
		$this->set('email_error', 'wrong');
	}

		}
		
	}
}

	function forgotpassword2(){
	$this->layout='indexlayout';
	$this->loadModel('User');	
		if(isset($this->data) && !empty($this->data)){		
		    $count = $this->User->find('count', array('conditions'=>array('User.email'=>$this->data['User']['email'])));
		    $result = $this->User->find('first',array('conditions'=>array('User.email'=>$this->data['User']['email'])));	
			if($count>0){
			     $newpassword = $this->data['User']['password']; // to generate a password of 6 characters		
			     $encpt_newpassword = md5($newpassword);		
			     $data['User']['id'] = $result['User']['id'];			     
			     $data['User']['password'] = $encpt_newpassword;           		
				   if($this->User->save($data['User'],array('validate'=>false))){
				   	/*
					$this->loadModel('EmailTemplate');
					  //CODE TO FIRE AN EMAIL			       
					$forgetpass_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>5)));	
					$forgetpass_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#USERNAME','#NEWPASSWORD'),array($this->selfURL() ,ucwords($result['User']['fname']),@$this->data['User']['email'],$newpassword),$forgetpass_temp['EmailTemplate']['mail_body']);									  
					$this->set('forgetpasstemplate',$forgetpass_temp['EmailTemplate']['mail_body']);
					$this->Email->from = EMAIL_FROM;
					$this->Email->to = $this->data['User']['email'];
					$this->Email->subject = $forgetpass_temp['EmailTemplate']['mail_subject'];
					$this->Email->template = 'forgetpass_mail';
					$this->Email->sendAs = 'both'; // because we like to send pretty mail			
					$this->Email->send(); //Do not pass any args to send() 
					*/
					$this->Session->setFlash('New password has been set successfully.');
					$this->redirect(array("controller" => "homes", "action" => "login"));			
				   }       
		    }else{
			     $this->Session->setFlash('This email is not registered in our system');
			     $this->redirect(array("controller" => "homes", "action" => "forgotpassword"));
		     }
      }
}

/**************************************************************************

 * Calls testimonials page.

 * @param  n/a

 * @return n/a

 * @access public

**************************************************************************/
	function testimonials(){
	 $this->layout='frontend';
	 $this->set('title_for_layout',"Report Online plus - Testimonials");   
	}
/**************************************************************************
	 *
	 *   code for attaching captcha 
	 *     	
**************************************************************************/
function securimage($random_number){      
   $this->autoLayout = false; //a blank layout
    //override variables set in the component - look in component for full list
    $this->captcha->image_height = 75;
    $this->captcha->image_width = 230;
    $this->captcha->image_bg_color = '#ffffff';
    $this->captcha->line_color = '#C6BCB9';
    $this->captcha->arc_line_colors = '#999999,#cccccc';
    $this->captcha->code_length = 5;
    $this->captcha->font_size = 40;
    $this->captcha->text_color = '#000000';
    $this->set('captcha_data', $this->captcha->show()); //dynamically creates an image
}

function captcha_image(){
    Configure::write('debug',0);
    $this->layout = null;  
    $this->Captcha->image();
    $this->render();
}
/**************************************************************************
 * Calls customer service page.
 * @param  n/a
 * @return n/a
 * @access public
**************************************************************************/	
function customerservice(){
  $this->layout='frontend'; 
  $this->set('captcha_form_url', $this->webroot.'/'); //url for the form
  $this->set('captcha_image_url', $this->webroot.'homes/securimage/0'); //url for the captcha image
  $this->set('title_for_layout',"Report Online plus - Customer Service");
  $this->loadModel('Customerservice');  
  $this->Customerservice->set($this->data);
  if(!empty($this->data)){
    if($this->Customerservice->validates())
    {
            $error=false;
            if($this->captcha->check($this->data['Customerservice']['captcha_code']) == false) 
          	{
				//the code was incorrect - display an error message to user
                $this->set('error_captcha', 'Please enter the valid captcha code'); //set error msg
                $error=true;               
            }
            if($error==false) 
            {
		      $submittername=$this->data['Customerservice']['name'];
    		      $submitteremail=$this->data['Customerservice']['email'];
    		      $submittercomments=$this->data['Customerservice']['textmessage']; 
    		      $submitterphone=$this->data['Customerservice']['phone'];		    
		      $message = file_get_contents(VIEWS .'elements' . DS . 'mailformats' . DS . 'contactmailformat.ctp');
		      $message = str_replace('[personname]',$submittername,$message);
		      $message = str_replace('[personcomment]',$submittercomments,$message);
		      $message = str_replace('[personphone]',$submitterphone,$message);
		      $message = str_replace('[personemail]',$submitteremail,$message);
                      $message = str_replace('[APPLICATION_NAME]',APPLICATION_OWNER,$message);  				  
              //Subject of mail
    			   $subject = "An Inquiry on Schedulepal.com customer service";
    			   $to=ADMIN_EMAIL;
    					// To send HTML mail, the Content-type header must be set
    					$headers  = 'MIME-Version: 1.0' . "\n";
    					$headers .= 'Content-type: text/html; charset=UTF-8' . "\n";
    					// Additional headers
    					$headers .= 'From: SchedulePal Customer Service <contact@schedulepal.com>' . "\n";
    					// Mail it
    					@mail($to, $subject, $message, $headers);
    					$this->Session->setFlash('Thank you ! we will contact you soon.');
						$this->data = array();
            }
    }
 }
}
/**************************************************************************
 * Calls contact us page.
 * @param  n/a
 * @return n/a
 * @access public
**************************************************************************/
function contactus() {
	 $this->layout='indexlayout';
	 $this->set('title_for_layout',"Report Online plus - Contact Us");
	 $this->set('country',$this->getCountry());
	 $this->loadModel('Contactus');
	 $this->loadModel('EmailTemplate');
	 $this->Contactus->set($this->data);
	if(!empty($this->data)){
		if ($this->Contactus->validates()) {	 
			 //CODE TO FIRE AN EMAIL
			$link = "<a href=" . $this->selfURL() . "/admin>Click here</a>"; 
			$result = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>9)));
			$result['EmailTemplate']['mail_body'] = str_replace(array('../../..','#FIRSTNAME','#LASTNAME','#EMAIL','#PHONE','#COMMENT','#LINK'),array($this->selfURL() ,
									   $this->data['Contactus']['fname'],
									   $this->data['Contactus']['lname'],
									   $this->data['Contactus']['email'],
									   $this->data['Contactus']['phone'],
									   $this->data['Contactus']['comment'],
									   $link),
									   $result['EmailTemplate']['mail_body']);			     			
			$this->Email->from = EMAIL_FROM;
			$this->Email->to = ADMIN_EMAIL;
			$this->Email->subject= $result['EmailTemplate']['mail_subject'];
			$this->Email->template = 'contactus_mail';
			$this->Email->sendAs = 'both'; // because we like to send pretty mail
			$this->set('contacttemp',$result['EmailTemplate']['mail_body']);			
			if($this->Email->send()){
				$this->Session->setFlash('Your Comment has been successfully submitted. Some one will contact you soon.'); 
				$this->data=null;
			}else{
				$this->Session->setFlash('Your Comment has not submitted.'); 
				}	
				
		}
	}
}
/**************************************************************************
 * Calls FAQs  page.
 * @param  n/a
 * @return n/a
 * @access public
**************************************************************************/
	function faqs() {
	 $this->layout='frontend';
	 $this->set('title_for_layout',"Report Online plus - FAQ");   
	}
/**************************************************************************
 * Calls Products  page.
 * @param  n/a
 * @return n/a
 * @access public
**************************************************************************/
function products() {
	 $this->layout='frontend';
	 $this->set('title_for_layout',"Report Online plus - Products"); 
	 $this->loadModel('SubscriptionDuration');
	 $subscription=$this->SubscriptionDuration->find('all');   
     $this->set('subscription',$subscription);  
	}
/**************************************************************************
 * Calls About Us  page.
 * @param  n/a
 * @return n/a
 * @access public
**************************************************************************/
function aboutus(){
	 $this->layout='frontend';
	 $this->set('title_for_layout',"Report Online plus - About Us");   
	}
function thankyoupage(){
     $this->layout='frontend';
	 $this->set('title_for_layout',"Report Online plus - Contact Us"); 
  }

function thankyoucontactpage(){
	  $this->layout='frontend';
	  $this->set('title_for_layout',"Report Online plus - Contact Us"); 
  }
function manager_printreport(){
  $this->layout='printout';
}  
function employee_printreport(){
  $this->layout='printout';
}

function codeInspectionQuery(){ 
	$this->layout = '';
	if(!empty($this->data)){
		$this->loadModel('Inquiry');
		$this->Inquiry->set($this->data);
		if($this->Inquiry->validates()){ 
			if($this->Inquiry->save($this->data)){
				
				$this->loadModel('EmailTemplate');
				//CODE TO FIRE AN EMAIL			       
				$codeinquiry_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>14)));	
				$code_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#EMAIL','#QUERY'),array($this->selfURL() ,ucwords($this->data['Inquiry']['name']),@$this->data['Inquiry']['email'],$this->data['Inquiry']['query']),$codeinquiry_temp['EmailTemplate']['mail_body']);									  
				$this->set('codeinquirytemplate',$code_temp['EmailTemplate']['mail_body']);
				$this->Email->from = EMAIL_FROM;
				$this->Email->to = ADMIN_EMAIL;
				//$this->Email->to = 'manishku@smartdatainc.net';
				$this->Email->subject = $codeinquiry_temp['EmailTemplate']['mail_subject'];
				$this->Email->template = 'code_inspection';
				$this->Email->sendAs = 'both'; // because we like to send pretty mail			
				$this->Email->send(); //Do not pass any args to send()				
				$this->Session->setFlash('Your Query has been submitted successfully');
			}else{
				$this->Session->setFlash('Query not submitted. Please try again');
			}
			//$this->redirect(array('controller'=>'homes','action'=>'codeInspectionQuery'));
			?>
			<script>parent.box_close();</script>
			<?php
			die;
		}
	}
}

function saveQuery(){
	$this->loadModel('Inquiry');
	$this->Inquiry->set($this->data);
	if($this->Inquiry->validates()){ 
		if($this->Inquiry->save($this->data)){
			
			$this->loadModel('EmailTemplate');
			//CODE TO FIRE AN EMAIL			       
			$codeinquiry_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>14)));	
			$code_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#EMAIL','#QUERY'),array($this->selfURL() ,ucwords($this->data['Inquiry']['name']),@$this->data['Inquiry']['email'],$this->data['Inquiry']['query']),$codeinquiry_temp['EmailTemplate']['mail_body']);									  
			$this->set('codeinquirytemplate',$code_temp['EmailTemplate']['mail_body']);
			$this->Email->from = EMAIL_FROM;
			$this->Email->to = ADMIN_EMAIL;
			//$this->Email->to = 'manishku@smartdatainc.net';
			$this->Email->subject = $codeinquiry_temp['EmailTemplate']['mail_subject'];
			$this->Email->template = 'code_inspection';
			$this->Email->sendAs = 'both'; // because we like to send pretty mail			
			$this->Email->send(); //Do not pass any args to send()				
			//$this->Session->setFlash('Your Query has been submitted successfully');
		}else{
			//$this->Session->setFlash('Query not submitted. Please try again');
		}
		//$this->redirect(array('controller'=>'homes','action'=>'codeInspectionQuery'));
		echo 'OK';
		
	}else{
		echo 'Error';
	}
	die;
}

function up_to_date(){
	$this->layout = 'indexlayout';
	$this->loadModel('SprinklerSystemInspection');
	$sprSysInspection = $this->SprinklerSystemInspection->find('all');
	$this->set(compact('sprSysInspection'));
	$this->loadModel('SprinklerSystemTesting');
	$sprSysTesting = $this->SprinklerSystemTesting->find('all');
	$this->set(compact('sprSysTesting'));
	$this->loadModel('SprinklerSystemMaintaince');
	$sprSysMaintaince = $this->SprinklerSystemMaintaince->find('all');
	$this->set(compact('sprSysMaintaince'));
	$this->loadModel('SprinklerValvesInspection');
	$sprValInspection = $this->SprinklerValvesInspection->find('all');
	$this->set(compact('sprValInspection'));
	$this->loadModel('SprinklerValvesTesting');
	$sprValTesting = $this->SprinklerValvesTesting->find('all');
	$this->set(compact('sprValTesting'));
	$this->loadModel('SprinklerValvesMaintaince');
	$sprValMaintaince = $this->SprinklerValvesMaintaince->find('all');
	$this->set(compact('sprValMaintaince'));
	
	$this->loadModel('UdfaComponent');
	//$componentData = $this->UdfaComponent->find('all');
	$componentData = $this->UdfaComponent->find('all',array('recursive'=>2));
	$this->set(compact('componentData'));
	/*
	if(!empty($this->data)){
		
		/*Inspection System Data*/
		/*$inspectionSystemData['SprinklerSystemInspectionData']['name'] = $this->data['SprinklerSystemInspectionData']['name'];
		$inspectionSystemData['SprinklerSystemInspectionData']['email'] = $this->data['SprinklerSystemInspectionData']['email'];
		foreach($this->data['SprinklerSystemInspectionData'] as $index => $inspectionSystemData) {
		  $inspectionSystemData = array('SprinklerSystemInspectionData' => $inspectionSystemData);		  
		  // clear any previous Book data
		  $this->loadModel('SprinklerSystemInspectionData');
		  $this->SprinklerSystemInspectionData->create();
		  // We set the Book data this way so that validation is processed correctly
		  $this->SprinklerSystemInspectionData->save($inspectionSystemData);
		}  
		/*Inspection System Data*/
		
		/*Testing System Data*/
		/*$testingSystemData['SprinklerSystemTestingData']['name'] = $inspectionSystemData['SprinklerSystemInspectionData']['name'];
		$testingSystemData['SprinklerSystemTestingData']['email'] = $inspectionSystemData['SprinklerSystemTestingData']['email'];
		foreach($this->data['SprinklerSystemTestingData'] as $index => $testingSystemData) {
		  $testingSystemData = array('SprinklerSystemTestingData' => $testingSystemData);		  
		  // clear any previous Book data
		  $this->loadModel('SprinklerSystemTestingData');
		  $this->SprinklerSystemTestingData->create();
		  // We set the Book data this way so that validation is processed correctly
		  $this->SprinklerSystemTestingData->save($testingSystemData);
		}
		/*Testing System Data*/
		
		/*Maintaince System Data*/
		/*$maintainceSystemData['SprinklerSystemMaintainceData']['name'] = $inspectionSystemData['SprinklerSystemMaintainceData']['name'];
		$maintainceSystemData['SprinklerSystemMaintainceData']['email'] = $inspectionSystemData['SprinklerSystemMaintainceData']['email'];
		foreach($this->data['SprinklerSystemMaintainceData'] as $index => $maintainceSystemData) {
		  $maintainceSystemData = array('SprinklerSystemMaintainceData' => $maintainceSystemData);		  
		  // clear any previous Book data
		  $this->loadModel('SprinklerSystemMaintainceData');
		  $this->SprinklerSystemMaintainceData->create();
		  // We set the Book data this way so that validation is processed correctly
		  $this->SprinklerSystemMaintainceData->save($maintainceSystemData);
		}
		/*Maintaince System Data*/	
	/*}*/
}
	function setlogin($sp_id=null){//echo base64_encode(10);die;
		$session_user=$this->Session->read('Log');
		if(!empty($session_user))
		{
			$this->redirect('/');	
		}
		$sp_id=base64_decode($sp_id);
		$this->layout='special_login';
		$this->set('title_for_layout',"Login");
		$this->loadModel('User');
		$data=$this->User->findById($sp_id);
		$this->set('spsession',$data);
		
		if(!empty($this->data) && isset($this->data)){
			$logindata = $this->User->find('first',array('conditions'=>array('User.password'=> md5($this->data['User']['password']),'User.email'=>$this->data['User']['email'],'User.deactivate'=>1)));
			if(!empty($logindata)){
				$this->Session->write('Log', $logindata);
				if($logindata['User']['user_type_id'] == 1){
				$this->Session->write('Spid', $logindata['User']['id']);
					$this->Session->setFlash('Logged in as Service Provider');
					$this->redirect("/sp/sps/dashboard");
				}elseif($logindata['User']['user_type_id'] == 2){
				$this->Session->write('inspectorid', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Fire Inspector');
					$this->redirect("/inspector/inspectors/dashboard");
				}
				elseif($logindata['User']['user_type_id'] == 3){
				$this->Session->write('client_id', $logindata['User']['id']);
				$this->Session->write('likeprompt',0);
					$this->Session->setFlash('Logged in as Client');
					$this->redirect("/client/clients/dashboard");
			}			
			}else{
				$this->Session->setFlash('Either Email or Password is incorrect.');
				$this->redirect(array("controller" => "homes", "action" => "setlogin",base64_encode($sp_id)));
			}
		}
	  }
}
