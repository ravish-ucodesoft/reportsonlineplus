<?php
class SchedulesController extends AppController {

	var $name = 'Schedules';
	var $components = array('Calendar','Email','Session','Cookie','Sms');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Calendar','Common','Text','Timeframe','Time');
  
/*
*************************************************************************
*Function Name		 : agent_add
*Functionality		 :  Add schedule.
*************************************************************************
*/
function sp_add($client_id = null,$agent_id = null){
    $this->layout='ajax';
    $this->set('title_for_layout', 'Manager');
    $this->loadModel('User');
    $this->loadModel('Schedule');
    $this->set('cid',$client_id);
    $this->set('agent_id',$agent_id);
    $sp = $this->Session->read('Log');
    $spId = $sp['User']['id'];
    $this->loadModel('Service');
    $data = $this->Service->find('list', array(
				'fields' => array('Service.id', 'Service.name'),
				'conditions' => array('Service.user_id'=>$spId,'Service.status' => 1),
				'recursive' => 0
				));				 
    $this->set('servicetype',$data);
    if(isset($this->data) && !empty($this->data)){
	 if($this->Schedule->save($this->data['Schedule'],false))
	 {
	       $this->Session->setFlash('Schedule has been added successfully'); 
               $this->redirect(array('controller'=>'sps','action'=>'clientlisting'));	
	 }
    }  
  }
  /*
*************************************************************************
*Function Name		 :  agent_delschedule
*Functionality		 :  delete schedule.
*************************************************************************
*/
   function agent_delschedule($sid = null)
   {
	$this->loadModel('Schedule');
    	if($this->Schedule->delete($id))
	{
	    $this->Session->setFlash('Schedule has been deleted successfully'); 
            $this->redirect(array('controller'=>'schedules','action'=>'monthly_view'));	
	}
   } 
    /*
*************************************************************************
  # FUNCTION USED TO SHOW SCHEDULES MONTHLY MADE ON 23-12-2010
*************************************************************************
*/
  function agent_monthly_view($tstamp=null){
    $this->layout='managerschedule';
    if(!isset($tstamp)){
      $tstamp=time();
    }
    $current_month_no=date('m',$tstamp);
    $current_year=date('Y',$tstamp);
    $agentses = $this->Session->read('Log');
    $agent_id = $agentses['User']['id'];
    $condition=array('MONTH(Schedule.schedule_date)'=>$current_month_no,'YEAR(Schedule.schedule_date)'=>$current_year,'Schedule.agent_id'=>$agent_id);
    $alldata =$this->Schedule->find('all', array('conditions' => $condition));
    $this->set('alldata',$alldata);
    $this->set('tstamp',$tstamp);   
}

/*
*************************************************************************
  #FUNCTION USED TO SHOW SCHEDULES 
*************************************************************************
*/
	function sp_schedule($tstamp=null){		
		$this->set('title_for_layout',"Monthly View");
		if(!isset($tstamp)){
		$tstamp=time();
		}
		$current_month_no=date('m',$tstamp);
		$current_year=date('Y',$tstamp);
		$this->set('tstamp',$tstamp);
		
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		
		
		#CODE 27-07-2012
		if(!empty($res)){
		foreach($res as $key1=>$servicecall):
		$client_arr[]=$servicecall['ServiceCall']['client_id'];
		endforeach;
		$clientFinalArr=array_unique($client_arr);
		}
		if(!empty($res)){		
		$i=0;$j=0;
		foreach($clientFinalArr as $clientID):
		//$arr[$i][$j]['client_id']=$clientID;
		$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.client_id'=>$clientID,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		foreach($res1 as $resFinal):		
		$arr[$clientID][$j]=$resFinal;
		$j++;
		endforeach;
		$i++;
		endforeach;
		//pr($arr);
		$this->set("Clientdata",$arr);	
		$this->set('frequency',Configure::read('frequency'));
		}
	}
	
/*
*************************************************************************
  #FUNCTION USED TO SHOW PENDING SERVICE CALLS 
*************************************************************************
*/
	function sp_pendingSchedule(){		
		$this->set('title_for_layout',"Pending Service Calls for waiting for client apporval");		
		
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$this->ServiceCall->bindModel(array('hasMany'=>array('ClientSchedule')));
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status !='=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));		
		$this->set("data",$res);
		
		
		#CODE 27-07-2012
		if(!empty($res)){	
			$this->set('frequency',Configure::read('frequency'));
		}
	}
	
	
	function sp_deleteServiceCall($serviceCallId=null)
	{
		$serviceCallId = base64_decode($serviceCallId);
		$this->loadModel('ServiceCall');
		if($this->ServiceCall->delete($serviceCallId))
		{
			$this->Session->setFlash('Service Call has been deleted successfully'); 
			$this->redirect(array('controller'=>'schedules','action'=>'pendingSchedule'));	
		}
	}
	
	
	
/*
 Function: sp_resubmitSereviceCall
 Description: Used to submit the service call from the sp again
 Created By: Manish Kumar
 Date: OCtober 29,2012
*/
	function sp_resubmitServiceCall($serviceCallId=null){ 
		if(!empty($this->data)){
			$serviceCallId = base64_decode($serviceCallId);
			$this->loadModel('Schedule');
			$this->loadModel('ServiceCall');
			$this->loadModel('User');
			$this->loadModel('EmailTemplate');
			$this->loadModel('ClientSchedule');
			
			foreach($this->data['Schedule'] as $saveData){
				
				$scheduleData['Schedule']['id']=$saveData['id'];
				$scheduleData['Schedule']['schedule_date_1']=date("Y-m-d",strtotime($saveData['schedule_date_1']));
				$scheduleData['Schedule']['schedule_from_1']=$saveData['schedule_from_1'];
				$scheduleData['Schedule']['schedule_to_1']=$saveData['schedule_to_1'];
				if(isset($saveData['schedule_date_2']) && isset($saveData['schedule_from_2']) && isset($saveData['schedule_to_2'])){
					$scheduleData['Schedule']['schedule_date_2']=date("Y-m-d",strtotime($saveData['schedule_date_2']));
					$scheduleData['Schedule']['schedule_from_2']=$saveData['schedule_from_2'];
					$scheduleData['Schedule']['schedule_to_2']=$saveData['schedule_to_2'];
				}
				if(isset($saveData['schedule_date_3']) && isset($saveData['schedule_from_3']) && isset($saveData['schedule_to_3'])){
					$scheduleData['Schedule']['schedule_date_3']=date("Y-m-d",strtotime($saveData['schedule_date_3']));
					$scheduleData['Schedule']['schedule_from_3']=$saveData['schedule_from_3'];
					$scheduleData['Schedule']['schedule_to_3']=$saveData['schedule_to_3'];
				}
				if(isset($saveData['schedule_date_4']) && isset($saveData['schedule_from_4']) && isset($saveData['schedule_to_4'])){
					$scheduleData['Schedule']['schedule_date_4']=date("Y-m-d",strtotime($saveData['schedule_date_4']));
					$scheduleData['Schedule']['schedule_from_4']=$saveData['schedule_from_4'];
					$scheduleData['Schedule']['schedule_to_4']=$saveData['schedule_to_4'];
				}
				if(isset($saveData['schedule_date_5']) && isset($saveData['schedule_from_5']) && isset($saveData['schedule_to_5'])){
					$scheduleData['Schedule']['schedule_date_5']=date("Y-m-d",strtotime($saveData['schedule_date_5']));
					$scheduleData['Schedule']['schedule_from_5']=$saveData['schedule_from_5'];
					$scheduleData['Schedule']['schedule_to_5']=$saveData['schedule_to_5'];
				}
				if(isset($saveData['schedule_date_6']) && isset($saveData['schedule_from_6']) && isset($saveData['schedule_to_6'])){
					$scheduleData['Schedule']['schedule_date_6']=date("Y-m-d",strtotime($saveData['schedule_date_6']));
					$scheduleData['Schedule']['schedule_from_6']=$saveData['schedule_from_6'];
					$scheduleData['Schedule']['schedule_to_6']=$saveData['schedule_to_6'];
				}
				if(isset($saveData['schedule_date_7']) && isset($saveData['schedule_from_7']) && isset($saveData['schedule_to_7'])){
					$scheduleData['Schedule']['schedule_date_7']=date("Y-m-d",strtotime($saveData['schedule_date_7']));
					$scheduleData['Schedule']['schedule_from_7']=$saveData['schedule_from_7'];
					$scheduleData['Schedule']['schedule_to_7']=$saveData['schedule_to_7'];
				}
				if(isset($saveData['schedule_date_8']) && isset($saveData['schedule_from_8']) && isset($saveData['schedule_to_8'])){
					$scheduleData['Schedule']['schedule_date_8']=date("Y-m-d",strtotime($saveData['schedule_date_8']));
					$scheduleData['Schedule']['schedule_from_8']=$saveData['schedule_from_8'];
					$scheduleData['Schedule']['schedule_to_8']=$saveData['schedule_to_8'];
				}
				if(isset($saveData['schedule_date_9']) && isset($saveData['schedule_from_9']) && isset($saveData['schedule_to_9'])){
					$scheduleData['Schedule']['schedule_date_9']=date("Y-m-d",strtotime($saveData['schedule_date_9']));
					$scheduleData['Schedule']['schedule_from_9']=$saveData['schedule_from_9'];
					$scheduleData['Schedule']['schedule_to_9']=$saveData['schedule_to_9'];
				}
				if(isset($saveData['schedule_date_10']) && isset($saveData['schedule_from_10']) && isset($saveData['schedule_to_10'])){
					$scheduleData['Schedule']['schedule_date_10']=date("Y-m-d",strtotime($saveData['schedule_date_10']));
					$scheduleData['Schedule']['schedule_from_10']=$saveData['schedule_from_10'];
					$scheduleData['Schedule']['schedule_to_10']=$saveData['schedule_to_10'];
				}
				$this->Schedule->create();			
				$this->Schedule->save($scheduleData);
			}
			$this->ServiceCall->updateAll(array('ServiceCall.call_status'=>"0",'ServiceCall.declined_reason'=>'""'),array('ServiceCall.id'=>$serviceCallId));
			$this->ClientSchedule->deleteAll(array('ClientSchedule.service_call_id'=>$serviceCallId));
			
			$this->ServiceCall->bindModel(array('belongsTo'=>array('Client'=>array('className'=>'User','foreignKey'=>'client_id','fields'=>array('Client.fname','Client.lname','Client.email')))));
			$serviceData = $this->ServiceCall->findById($serviceCallId);
			$client_name=$serviceData['Client']['fname'].' '.$serviceData['Client']['lname']; //cleintname
			$client_email = $serviceData['Client']['email'];
			$sp = $this->Session->read('Log');
			$spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
			$spschedularphone = $sp['User']['phone']; //sp-schedulerphone
			$spcompany=$sp['Company']['name'];
			$this->Schedule->bindModel(
					array('belongsTo' => array(
					'Report' => array(
					'className' => 'Report',
					'fields'=>array('Report.name')
						)
					    )
					 )
					);
			//$this->Schedule->bindModel('belongsTo'=>'Report');
			$Schdata=$this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$serviceCallId)));
			//pr($Schdata);
			//echo "===============>";
		       
			$email_string ='';
			$email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
						<tbody>
						<tr style="background-color:#0B83A6">
						    <td><b>ReportName</b></td>
						    <td><b>Days</b></td>
						    <td><b>Schedule Date/Time</b></td>
						    <td><b>Inspector Assigned</b></td>
						</tr>';
			$reportdata='';
			$sch2="";$sch3="";$sch4="";$sch5="";$sch6="";$sch7="";$sch8="";$sch9="";$sch10="";
		       foreach($Schdata as $key=>$sch){
			$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['Schedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
			       $reportdata   .=$sch['Report']['name'].',';
			       $email_string .= "<tr>";
			       $days=0;
			       $email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';       
			       $sch1= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_1'])); $days=$days+1;
				if(!empty($sch['Schedule']['schedule_date_2'])) { 
					$sch2= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_2'])); $days=$days+1;
				}
				if(!empty($sch['Schedule']['schedule_date_3'])) { 
					$sch3= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_3']));$days=$days+1;
				 } 
				if(!empty($sch['Schedule']['schedule_date_4'])) { 
					$sch4= date('m/d/Y',strtotime($sch['schedule_date_4'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_4']));$days=$days+1;
				} 
				if(!empty($schedule['schedule_date_5'])) { 
				$sch5= date('m/d/Y',strtotime($sch['schedule_date_5'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['schedule_to_5']));$days=$days+1;
				} 
				if(!empty($sch['Schedule']['schedule_date_6'])) { 
					$sch6= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_6']));$days=$days+1;
				}
				if(!empty($sch['Schedule']['schedule_date_7'])) { 
					$sch7= date('m/d/Y',strtotime($sch['schedule_date_7'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_7']));$days=$days+1;
				}
				if(!empty($sch['Schedule']['schedule_date_8'])) {
					$sch8= date('m/d/Y',strtotime($sch['schedule_date_8'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_8']));$days=$days+1;
				} 
				if(!empty($sch['Schedule']['schedule_date_9'])) {
				$sch9= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_9']));$days=$days+1;
				 }
				if(!empty($sch['Schedule']['schedule_date_10'])) { 
					$sch10= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_10']));$days=$days+1;
				}
				
			       $email_string .='<td valign="top">'.$days.'</td>';		    
			       $email_string .='<td valign="top">'.$sch1.'<br/>'.@$sch2.'<br/>'.@$sch3.'<br/>'.@$sch4.'<br/>'.@$sch5.'<br/>'.@$sch6.'<br/>'.@$sch7.'<br/>'.@$sch8.'<br/>'.@$sch9.'<br/>'.@$sch10.'</td>';
			       $email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
			       $email_string .= "</tr>";
		       }
		       
			$email_string .= '</tbody></table>';				
			$servicecall_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>6)));
			$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
			$servicecall_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENTNAME','#SPNAME','#SPCOMPANY','#DETAIL','#SCHEDULEPHONE'),array($this->selfURL() ,$client_name,$spname,$spcompany,$email_string,$spschedularphone),$servicecall_temp['EmailTemplate']['mail_body']);
			$this->set('servicecall_temp',$servicecall_temp['EmailTemplate']['mail_body']);			
			
			$this->Email->to = $client_email;
			$this->Email->subject = $servicecall_temp['EmailTemplate']['mail_subject'];
			$this->Email->from = EMAIL_FROM;
			$this->Email->template = 'service_call_temp'; // note no '.ctp'
			$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
			$this->Email->send();//Do not pass any args to send() 				
			$this->redirect(array('controller'=>'sps','action'=>'serviceCallThanks',base64_encode($serviceData['ServiceCall']['client_id'])));			
			
			
		}
	}
	
/*
 Function: sp_resubmitapprovalemail
 Description: Used to ressend the approval email to client for service call
 Created By: Vishal
 Date: Nov 15,2012
*/
	function sp_resendapprovalemail($servicecall_id=null)
	{
		$serviceCallId = base64_decode($servicecall_id);				
		$this->loadModel('User');		
		$this->loadModel('ServiceCall');
		$res=$this->ServiceCall->find("first",array('conditions'=>array('ServiceCall.id'=>$serviceCallId),'recursive'=>1));
		$client = $this->User->find('first',array('conditions'=>array('User.id'=>$res['ServiceCall']['client_id']),'fields'=>array('fname','lname','site_name','email','client_company_name'),'recursive'=>2));
		
		$client_name = $client['User']['fname']." ".$client['User']['lname']." (".$client['User']['client_company_name'].")";
		$client_email = $client['User']['email'];			
		
		$this->acceptanceMailCode($serviceCallId);			
		$this->Session->setFlash(__('Acceptance email has been sent successfully to '.$client['User']['client_company_name'],true));
		$this->redirect(array('controller'=>'schedules','action'=>'pendingSchedule'));
		exit;
	}
	
	function acceptanceMailCode($serviceCallId){	
		$this->loadModel('Schedule');
		$this->loadModel('ServiceCall');
		$this->loadModel('User');
		$this->loadModel('EmailTemplate');
		$this->loadModel('ClientSchedule');
		
		$res=$this->ServiceCall->find("first",array('conditions'=>array('ServiceCall.id'=>$serviceCallId),'recursive'=>1));
		
		$sp = $this->User->find('first',array('conditions'=>array('User.id'=>$res['ServiceCall']['sp_id']),'fields'=>array('fname','lname','phone','email'),'recursive'=>2));
		$spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
		$spschedularphone = $sp['User']['phone']; //sp-schedulerphone
		$spcompany=$sp['Company']['name'];
		
		$client = $this->User->find('first',array('conditions'=>array('User.id'=>$res['ServiceCall']['client_id']),'fields'=>array('fname','lname','site_name','email','client_company_name'),'recursive'=>2));
		
		$client_name = $client['User']['fname']." ".$client['User']['lname']." (".$client['User']['client_company_name'].")";
		$client_email = $client['User']['email'];
			
			
			$this->Schedule->bindModel(
						array('belongsTo' => array(
						'Report' => array(
						'className' => 'Report',
						'fields'=>array('Report.name')
							)
						    )
						 )
						);
			    
				$Schdata=$this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$serviceCallId)));
			
				$email_string ='';
				$email_string .= '<table align="center" width="400" height="88" cellspacing="1" cellpadding="1" border="1">
							<tbody>
							<tr style="background-color:#0B83A6">
							    <td><b>ReportName</b></td>
							    <td><b>Days</b></td>
							    <td><b>Schedule Date/Time</b></td>
							    <td><b>Inspector Assigned</b></td>
							</tr>';
				$reportdata='';
			       foreach($Schdata as $key=>$sch){
						$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['Schedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
				       $reportdata   .=$sch['Report']['name'].',';
				       $email_string .= "<tr>";
				       $days=0;
				       $email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';       
				       $sch1= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_1'])); $days=$days+1;
					$sch2="";$sch3="";$sch4="";$sch5="";$sch6="";$sch7="";$sch8="";$sch9="";$sch10="";
					if(!empty($sch['Schedule']['schedule_date_2'])) { 
						$sch2= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_2'])); $days=$days+1;
					}
					if(!empty($sch['Schedule']['schedule_date_3'])) { 
						$sch3= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_3']));$days=$days+1;
					}
					if(!empty($sch['Schedule']['schedule_date_4'])) { 
						$sch4= date('m/d/Y',strtotime($sch['schedule_date_4'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_4']));$days=$days+1;
					}
					if(!empty($schedule['schedule_date_5'])) { 
						$sch5= date('m/d/Y',strtotime($sch['schedule_date_5'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['schedule_to_5']));$days=$days+1;
					}
					if(!empty($sch['Schedule']['schedule_date_6'])) { 
						$sch6= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_6']));$days=$days+1;
					}
					if(!empty($sch['Schedule']['schedule_date_7'])) {
						$sch7= date('m/d/Y',strtotime($sch['schedule_date_7'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_7']));$days=$days+1;
					}
					if(!empty($sch['Schedule']['schedule_date_8'])) {
						$sch8= date('m/d/Y',strtotime($sch['schedule_date_8'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_8']));$days=$days+1;
					}
					if(!empty($sch['Schedule']['schedule_date_9'])) { 
						$sch9= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_9']));$days=$days+1;
					}
					if(!empty($sch['Schedule']['schedule_date_10'])) { 
						$sch10= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_10']));$days=$days+1;
					}
					
				       $email_string .='<td valign="top">'.$days.'</td>';		    
				       $email_string .='<td valign="top">'.$sch1.'<br/>'.$sch2.'<br/>'.$sch3.'<br/>'.$sch4.'<br/>'.$sch5.'<br/>'.$sch6.'<br/>'.$sch7.'<br/>'.$sch8.'<br/>'.$sch9.'<br/>'.$sch10.'</td>';
				       $email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
				       $email_string .= "</tr>";
			       }
			       
			        $email_string .= '</tbody></table>';
			
			
		$servicecall_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>6)));
		$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
		$servicecall_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENTNAME','#SPNAME','#SPCOMPANY','#DETAIL','#SCHEDULEPHONE','#REPORTS','#LINK'),array($this->selfURL() ,$client_name,$spname,$spcompany,$email_string,$spschedularphone,$reportdata,$link),$servicecall_temp['EmailTemplate']['mail_body']);
		$this->set('servicecall_temp',$servicecall_temp['EmailTemplate']['mail_body']);
		$this->Email->to = $client_email;
		$this->Email->subject = $servicecall_temp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'service_call_temp'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail
		$this->Email->send();//Do not pass any args to send()
	}
	
	#On Sept-28-2012
	function sp_weekly_view($tstamp=null) {
		$this->set('title_for_layout',"Weekly View"); 
		if(!isset($tstamp)) {
		$tstamp=time();
		}
		$shift_start_date=$this->Calendar->getday($tstamp,1);
		$shift_end_date=$this->Calendar->getday($tstamp,7);
		$this->set('tstamp',$tstamp);
		
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		
		
		#CODE 27-07-2012
		if(!empty($res)){
		foreach($res as $key1=>$servicecall):
		$client_arr[]=$servicecall['ServiceCall']['client_id'];
		endforeach;
		$clientFinalArr=array_unique($client_arr);
		}
		if(!empty($res)){		
		$i=0;$j=0;
		foreach($clientFinalArr as $clientID):
		//$arr[$i][$j]['client_id']=$clientID;
		$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>1,'ServiceCall.client_id'=>$clientID),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		foreach($res1 as $resFinal):		
		$arr[$clientID][$j]=$resFinal;
		$j++;
		endforeach;
		$i++;
		endforeach;
		
		$this->set("Clientdata",$arr);		
		$this->set('frequency',Configure::read('frequency'));
		}
		
		
		
		
		
	}
	
	
	#On Jan-07-2013 By Manish Kumar
	function sp_day_view($tstamp=null) {
		$this->set('title_for_layout',"Day View"); 
		if(!isset($tstamp)) {
		$tstamp=time();
		}		
		$this->set('tstamp',$tstamp);
		
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		
		
		#CODE 27-07-2012
		if(!empty($res)){
		foreach($res as $key1=>$servicecall):
		$client_arr[]=$servicecall['ServiceCall']['client_id'];
		endforeach;
		$clientFinalArr=array_unique($client_arr);
		}
		if(!empty($res)){		
		$i=0;$j=0;
		foreach($clientFinalArr as $clientID):
		//$arr[$i][$j]['client_id']=$clientID;
		$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>1,'ServiceCall.client_id'=>$clientID),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		foreach($res1 as $resFinal):		
		$arr[$clientID][$j]=$resFinal;
		$j++;
		endforeach;
		$i++;
		endforeach;
		
		$this->set("Clientdata",$arr);		
		$this->set('frequency',Configure::read('frequency'));
		}
		
		
		
		
		
	}
	
	
	/*
*************************************************************************
  #FUNCTION USED TO SHOW SCHEDULES 
*************************************************************************
*/
	function sp_inspector_schedule($inspector_id=null,$tstamp=null){
		$this->set('title_for_layout',"Inspector Monthly View");
		$this->loadModel('User');
		$inspector=$this->User->find("first",array('conditions'=>array('User.id'=>$inspector_id),'recursive'=>1));
		$this->set("inspector",$inspector);
		
		$allInspector = $this->User->find('all',array('conditions'=>array('User.user_type_id'=>2),'fields'=>array('User.fname','User.lname','User.id')));
		$this->set('allInspector',$allInspector);
		if(!isset($tstamp)){
		$tstamp=time();
		}
		$current_month_no=date('m',$tstamp);
		$current_year=date('Y',$tstamp);
		$this->set('tstamp',$tstamp);
		
		
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		
		$this->ServiceCall->bindModel(
                   array(
                     'hasMany' => array(
                         'Schedule' => array(
                            'className' => 'Schedule',
                            'conditions' => array('OR'=>
												  array('Schedule.lead_inspector_id' => $inspector_id,'Schedule.helper_inspector_id LIKE' => "%$inspector_id%")
                                
                            )
                         )
                     )
				));

		
		
		$res=$this->ServiceCall->find("all",array('conditions'=>array(''),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		
		
		#CODE 27-07-2012
		if(!empty($res)){
		foreach($res as $key1=>$servicecall):
		$client_arr[]=$servicecall['ServiceCall']['client_id'];
		endforeach;
		$clientFinalArr=array_unique($client_arr);
		}
		if(!empty($res)){		
		$i=0;$j=0;
		foreach($clientFinalArr as $clientID):
		//$arr[$i][$j]['client_id']=$clientID;
		$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.client_id'=>$clientID),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		foreach($res1 as $resFinal):		
		$arr[$clientID][$j]=$resFinal;
		$j++;
		endforeach;
		$i++;
		endforeach;
		//pr($arr);
		$this->set("Clientdata",$arr);
		
		
		$this->set('frequency',Configure::read('frequency'));
		}
	}
	
	
	
	#On Sept-28-2012
	function sp_inspector_weekly_view($inspector_id=null,$tstamp=null) {
		$this->set('title_for_layout',"Inspector Weekly View");
		$this->loadModel('User');
		$inspector=$this->User->find("first",array('conditions'=>array('User.id'=>$inspector_id),'recursive'=>1));
		$this->set("inspector",$inspector);
		if(!isset($tstamp)) {
		$tstamp=time();
		}
		$shift_start_date=$this->Calendar->getday($tstamp,1);
		$shift_end_date=$this->Calendar->getday($tstamp,7);
		$this->set('tstamp',$tstamp);		
		
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		
		$this->ServiceCall->bindModel(
                   array(
                     'hasMany' => array(
                         'Schedule' => array(
                            'className' => 'Schedule',
                            'conditions' => array('OR'=>
												  array('Schedule.lead_inspector_id' => $inspector_id,'Schedule.helper_inspector_id LIKE' => "%$inspector_id%")
                                
                            )
                         )
                     )
				));
		
		
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		
		
		#CODE 27-07-2012
		if(!empty($res)){
		foreach($res as $key1=>$servicecall):
		$client_arr[]=$servicecall['ServiceCall']['client_id'];
		endforeach;
		$clientFinalArr=array_unique($client_arr);
		}
		if(!empty($res)){		
		$i=0;$j=0;
		foreach($clientFinalArr as $clientID):
		//$arr[$i][$j]['client_id']=$clientID;
		$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.client_id'=>$clientID),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		foreach($res1 as $resFinal):		
		$arr[$clientID][$j]=$resFinal;
		$j++;
		endforeach;
		$i++;
		endforeach;
		
		$this->set("Clientdata",$arr);		
		$this->set('frequency',Configure::read('frequency'));
		}
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	#Created by Vishal on 31-07-2012 to change the service call frequency(Quarterly,Half Yearly,Yearly,No,i don't want)
	function sp_changeservicecallfreq($serviceCallID=null)
	{		
		$this->layout=null;
		$this->loadModel('ServiceCall');
		$res=$this->ServiceCall->find("first",array('conditions'=>array('ServiceCall.id'=>$serviceCallID),'recursive'=>2));
		$this->set("data",$res);
		$this->set('frequency',Configure::read('frequency'));
		if(!empty($this->data))
		{
			$this->ServiceCall->id=$this->data['ServiceCall']['id'];
			$this->ServiceCall->save($this->data['ServiceCall']);
			$this->Session->setFlash(__('Auto populate frequency of service call has been changed sucessfully',true));
			$this->redirect(array('controller'=>'schedules','action'=>'schedule'));
		}
		
	}
	
	
	#Create ServiceCallViaCalendar added on 01-08-2012
	function sp_createServiceCallByCalendar($sch_date=null)
	{
	if(isset($sch_date))
	{
		$this->set('selectedDate',date('m/d/Y',strtotime($sch_date)));		
	}
	
	#CODE to find all clients assigned in assigned table
	$sp = $this->Session->read('Log');
	$sp_id = $sp['User']['id'];
	
	#01-10-2012 CODE to fetch the site addresses for selection
	$this->set('siteAddresses',$this->getSiteaddress($sp_id));	
	//$this->set('siteAddresses',array());	
	
	$clientData = $this->getAllClientOfSp($sp_id);
	$this->set('clientData',$clientData);
	
	$this->loadModel('User');
	$clientCompanyData = $this->User->find('list', array(
				'conditions' => array('User.sp_id'=>$sp_id,'User.user_type_id'=>3),				
				'fields' =>array('id','client_company_name'),
				'order'=>'User.client_company_name',
				'limit'=>'',
				'page'=>''
	));
	$this->set('clientCompanyData',$clientCompanyData);
	
	$this->loadModel('Report');
	$this->set('frequency',Configure::read('frequency'));
	$sp = $this->Session->read('Log');	
	$this->set('sp_id',$sp_id);
	//$this->set('cid',$cid);
	$reportData = $this->getAllReports($sp_id);
	$this->set('rData',$reportData);
	$this->loadModel('User');
	$this->loadModel('Service');
	$this->loadModel('Schedule');
	$this->loadModel('ServiceCall');
	$this->loadModel('ScheduleService');
	$this->loadmodel('EmailTemplate');
	$this->loadmodel('Report');
	$this->loadmodel('ReportCert');
		
		$this->set('frequency',Configure::read('frequency'));
	 if(isset($this->data) && !empty($this->data))
	 {
	 configure::write('debug',0);	
		//pr($this->data); die;
		/*ByPass Customer Acceptance Call*/
		if($this->data['ServiceCall']['bypass']=='Yes'){
			$this->data['ServiceCall']['call_status']='1';
		}
		/*ByPass Customer Acceptance Call*/
		$this->ServiceCall->save($this->data['ServiceCall'],false);
		$serviceCallID = $this->ServiceCall->getLastInsertId();
		for($i=1;$i<=6;$i++)
		{
			
			if($this->data[$i]['Schedule']['report_id'] != "0")
			{
			$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data[$i]['Schedule']['lead_inspector']),'fields'=>array('fname','lname','email')));
			$leader_name = $leader['User']['fname']." ".$leader['User']['lname'];
			$client = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['ServiceCall']['client_id']),'fields'=>array('fname','lname','site_name','email')));
			$client_name = $client['User']['fname']." ".$client['User']['lname'];
			$client_email = $client['User']['email'];
			$report = $this->Report->find('first',array('conditions'=>array('Report.id'=>$this->data[$i]['Schedule']['report_id']),'fields'=>array('name')));
			$report_name = $report['Report']['name'];
			
			$data[$i]['Schedule']['sch_freq']=$this->data[$i]['Schedule']['sch_freq'];
			$data[$i]['Schedule']['service_call_id']=$serviceCallID;
			$data[$i]['Schedule']['schedule_date_1']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_1']));
			$data[$i]['Schedule']['schedule_from_1']=$this->data[$i]['Schedule']['schedule_from_1'];
			$data[$i]['Schedule']['schedule_to_1']=$this->data[$i]['Schedule']['schedule_to_1'];
			$data[$i]['Schedule']['submission_date']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['submission_date']));
			if(isset($this->data[$i]['Schedule']['schedule_date_2']) && isset($this->data[$i]['Schedule']['schedule_from_2']) && isset($this->data[$i]['Schedule']['schedule_to_2']))
			{
			  $date2= $this->data[$i]['Schedule']['schedule_date_2'];
			  $from2= $this->data[$i]['Schedule']['schedule_from_2'];
			  $to2= $this->data[$i]['Schedule']['schedule_to_2'];
				$data[$i]['Schedule']['schedule_date_2']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_2']));
				$data[$i]['Schedule']['schedule_from_2']=$this->data[$i]['Schedule']['schedule_from_2'];
				$data[$i]['Schedule']['schedule_to_2']=$this->data[$i]['Schedule']['schedule_to_2'];
			}else{
				$date2= '';
			    $from2= '';
			    $to2= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_3']) && isset($this->data[$i]['Schedule']['schedule_from_3']) && isset($this->data[$i]['Schedule']['schedule_to_3']))
			{
			  $date3= $this->data[$i]['Schedule']['schedule_date_3'];
			  $from3= $this->data[$i]['Schedule']['schedule_from_3'];
			  $to3= $this->data[$i]['Schedule']['schedule_to_3'];
				$data[$i]['Schedule']['schedule_date_3']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_3']));
				$data[$i]['Schedule']['schedule_from_3']=$this->data[$i]['Schedule']['schedule_from_3'];
				$data[$i]['Schedule']['schedule_to_3']=$this->data[$i]['Schedule']['schedule_to_3'];
			}else{
				$date3= '';
			    $from3= '';
			    $to3= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_4']) && isset($this->data[$i]['Schedule']['schedule_from_4']) && isset($this->data[$i]['Schedule']['schedule_to_4']))
			{
			  $date4= $this->data[$i]['Schedule']['schedule_date_4'];
			  $from4= $this->data[$i]['Schedule']['schedule_from_4'];
			  $to4= $this->data[$i]['Schedule']['schedule_to_4']; 
				$data[$i]['Schedule']['schedule_date_4']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_4']));
				$data[$i]['Schedule']['schedule_from_4']=$this->data[$i]['Schedule']['schedule_from_4'];
				$data[$i]['Schedule']['schedule_to_4']=$this->data[$i]['Schedule']['schedule_to_4'];
			}
			else{
				$date4= '';
			    $from4= '';
			    $to4= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_5']) && isset($this->data[$i]['Schedule']['schedule_from_5']) && isset($this->data[$i]['Schedule']['schedule_to_5']))
			{   
			  $date5= $this->data[$i]['Schedule']['schedule_date_5'];
			  $from5= $this->data[$i]['Schedule']['schedule_from_5'];
			  $to5= $this->data[$i]['Schedule']['schedule_to_5'];
				$data[$i]['Schedule']['schedule_date_5']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_5']));
				$data[$i]['Schedule']['schedule_from_5']=$this->data[$i]['Schedule']['schedule_from_5'];
				$data[$i]['Schedule']['schedule_to_5']=$this->data[$i]['Schedule']['schedule_to_5'];
			}else{
				$date5= '';
			    $from5= '';
			    $to5= '';
      
			}
			# Added 5 new schedule date, schedule_from and schedule date_to ON 26 JULY 2012(ADDED BY NAVDEEP)
			if(isset($this->data[$i]['Schedule']['schedule_date_6']) && isset($this->data[$i]['Schedule']['schedule_from_6']) && isset($this->data[$i]['Schedule']['schedule_to_6']))
			{   
			  $date6= $this->data[$i]['Schedule']['schedule_date_6'];
			  $from6= $this->data[$i]['Schedule']['schedule_from_6'];
			  $to6= $this->data[$i]['Schedule']['schedule_to_6'];
				$data[$i]['Schedule']['schedule_date_6']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_6']));
				$data[$i]['Schedule']['schedule_from_6']=$this->data[$i]['Schedule']['schedule_from_6'];
				$data[$i]['Schedule']['schedule_to_6']=$this->data[$i]['Schedule']['schedule_to_6'];
			}else{
				$date6= '';
			    $from6= '';
			    $to6= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_7']) && isset($this->data[$i]['Schedule']['schedule_from_7']) && isset($this->data[$i]['Schedule']['schedule_to_7']))
			{   
			  $date7= $this->data[$i]['Schedule']['schedule_date_7'];
			  $from7= $this->data[$i]['Schedule']['schedule_from_7'];
			  $to7= $this->data[$i]['Schedule']['schedule_to_7'];
				$data[$i]['Schedule']['schedule_date_7']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_7']));
				$data[$i]['Schedule']['schedule_from_7']=$this->data[$i]['Schedule']['schedule_from_7'];
				$data[$i]['Schedule']['schedule_to_7']=$this->data[$i]['Schedule']['schedule_to_7'];
			}else{
				$date7= '';
			    $from7= '';
			    $to7= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_8']) && isset($this->data[$i]['Schedule']['schedule_from_8']) && isset($this->data[$i]['Schedule']['schedule_to_8']))
			{   
			  $date8= $this->data[$i]['Schedule']['schedule_date_8'];
			  $from8= $this->data[$i]['Schedule']['schedule_from_8'];
			  $to8= $this->data[$i]['Schedule']['schedule_to_8'];
				$data[$i]['Schedule']['schedule_date_8']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_8']));
				$data[$i]['Schedule']['schedule_from_8']=$this->data[$i]['Schedule']['schedule_from_8'];
				$data[$i]['Schedule']['schedule_to_8']=$this->data[$i]['Schedule']['schedule_to_8'];
			}else{
				$date8= '';
			    $from8= '';
			    $to8= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_9']) && isset($this->data[$i]['Schedule']['schedule_from_9']) && isset($this->data[$i]['Schedule']['schedule_to_9']))
			{   
			  $date9= $this->data[$i]['Schedule']['schedule_date_9'];
			  $from9= $this->data[$i]['Schedule']['schedule_from_9'];
			  $to9= $this->data[$i]['Schedule']['schedule_to_9'];
				$data[$i]['Schedule']['schedule_date_9']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_9']));
				$data[$i]['Schedule']['schedule_from_9']=$this->data[$i]['Schedule']['schedule_from_9'];
				$data[$i]['Schedule']['schedule_to_9']=$this->data[$i]['Schedule']['schedule_to_9'];
			}else{
				$date9= '';
			    $from9= '';
			    $to9= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_10']) && isset($this->data[$i]['Schedule']['schedule_from_10']) && isset($this->data[$i]['Schedule']['schedule_to_10']))
			{   
			  $date10= $this->data[$i]['Schedule']['schedule_date_10'];
			  $from10= $this->data[$i]['Schedule']['schedule_from_10'];
			  $to10= $this->data[$i]['Schedule']['schedule_to_10'];
				$data[$i]['Schedule']['schedule_date_10']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_10']));
				$data[$i]['Schedule']['schedule_from_10']=$this->data[$i]['Schedule']['schedule_from_10'];
				$data[$i]['Schedule']['schedule_to_10']=$this->data[$i]['Schedule']['schedule_to_10'];
			}else{
				$date10= '';
			    $from10= '';
			    $to10= '';
      
			}
			
			$data[$i]['Schedule']['report_id']=$this->data[$i]['Schedule']['report_id'];			
			$data[$i]['Schedule']['lead_inspector_id']=$this->data[$i]['Schedule']['lead_inspector'];
			 
			if(!empty($this->data[$i]['Schedule']['helper_inspector'])){
				
			$helper_inspector = implode(',',$this->data[$i]['Schedule']['helper_inspector']);
			}
			else{
			$helper_inspector = '';	
			}
			$data[$i]['Schedule']['helper_inspector_id']=$helper_inspector;
			
			$this->Schedule->create();
			
			$this->Schedule->save($data[$i]['Schedule'],false);
			$scheduleId = $this->Schedule->getLastInsertId();
			for($j=1;$j<=count($this->data[$i]['Schedule']['ScheduleService']);$j++)
			{
			    if($this->data[$i]['Schedule']['ScheduleService'][$j]['service_id'] != "0")
			    {
				$datas['ScheduleService']['schedule_id'] = $scheduleId;
				$datas['ScheduleService']['service_id']=$this->data[$i]['Schedule']['ScheduleService'][$j]['service_id'];
				$datas['ScheduleService']['amount']=$this->data[$i]['Schedule']['ScheduleService'][$j]['amount'];
				$datas['ScheduleService']['frequency']=$this->data[$i]['Schedule']['ScheduleService'][$j]['frequency'];
				$this->ScheduleService->create();
				$this->ScheduleService->save($datas['ScheduleService'],false);				
			   }
			}
			
			/*Save the reports name to be attached code starts here*/
			for($cr=1;$cr<=count($this->data[$i]['ReportCert']);$cr++)
			{
			    if($this->data[$i]['ReportCert'][$cr]['cert_id'] != "0")
			    {
				$certdatas['ReportCert']['schedule_id'] = $scheduleId;
				$certdatas['ReportCert']['cert_id']=$this->data[$i]['ReportCert'][$cr]['cert_id'];				
				$this->ReportCert->create();
				$this->ReportCert->save($certdatas['ReportCert'],false);				
			   }
			}
			/*Save the reports name to be attached code ends here*/
			
					 
		   }
		} //end forloop
		
		/*ByPass Customer Acceptance Call*/
		if($this->data['ServiceCall']['bypass']=='Yes'){			
			$this->ServiceCall->updateAll(array('ServiceCall.bypass_mail'=>'"1"'),array('ServiceCall.id'=>$serviceCallID));			
			App::import('Controller', 'Clients');
			// We need to load the class
			$Clients = new ClientsController;
			// If we want the model associations, components, etc to be loaded
			$Clients->sendApproveMail($serviceCallID);
		}else{
		/*ByPass Customer Acceptance Call*/
		#code to send email to client to give new service call info(ADDED BY NAVDEEP ON 26 JULy2012)
			//$serviceCallID	 
				 
			$client_name; //cleintname
			$sp = $this->Session->read('Log');
			$spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
			$spschedularphone = $sp['User']['phone']; //sp-schedulerphone
			$spcompany=$sp['Company']['name'];
			$this->Schedule->bindModel(
					array('belongsTo' => array(
					'Report' => array(
					'className' => 'Report',
					'fields'=>array('Report.name')
						)
					    )
					 )
					);
			//$this->Schedule->bindModel('belongsTo'=>'Report');
			$Schdata=$this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$serviceCallID)));
			//pr($Schdata);
			//echo "===============>";
		       
			$email_string ='';
			$email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
						<tbody>
						<tr style="background-color:#0B83A6">
						    <td><b>ReportName</b></td>
						    <td><b>Days</b></td>
						    <td><b>Schedule Date/Time</b></td>
						    <td><b>Inspector Assigned</b></td>
						</tr>';
			$reportdata='';
			$sch2="";$sch3="";$sch4="";$sch5="";$sch6="";$sch7="";$sch8="";$sch9="";$sch10="";
			foreach($Schdata as $key=>$sch){
				$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['Schedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
				$reportdata   .=$sch['Report']['name'].',';
				$email_string .= "<tr>";
				$days=0;
				$email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';       
				$sch1= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_1'])); $days=$days+1;
				 if(!empty($sch['Schedule']['schedule_date_2'])) { 
					 $sch2= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_2'])); $days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_3'])) { 
					 $sch3= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_3']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_4'])) { 
					 $sch4= date('m/d/Y',strtotime($sch['schedule_date_4'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_4']));$days=$days+1;
				 }
				 if(!empty($schedule['schedule_date_5'])) { 
					 $sch5= date('m/d/Y',strtotime($sch['schedule_date_5'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['schedule_to_5']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_6'])) { 
					 $sch6= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_6']));$days=$days+1;
				 } 
				 if(!empty($sch['Schedule']['schedule_date_7'])) { 
					 $sch7= date('m/d/Y',strtotime($sch['schedule_date_7'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_7']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_8'])) { 
					 $sch8= date('m/d/Y',strtotime($sch['schedule_date_8'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_8']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_9'])) { 
					 $sch9= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_9']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_10'])) { 
					 $sch10= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_10']));$days=$days+1;
				 }
				 
				$email_string .='<td valign="top">'.$days.'</td>';		    
				$email_string .='<td valign="top">'.$sch1.'<br/>'.@$sch2.'<br/>'.@$sch3.'<br/>'.@$sch4.'<br/>'.@$sch5.'<br/>'.@$sch6.'<br/>'.@$sch7.'<br/>'.@$sch8.'<br/>'.@$sch9.'<br/>'.@$sch10.'</td>';
				$email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
				$email_string .= "</tr>";
			}
			       
			$email_string .= '</tbody></table>';				
			$servicecall_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>6)));
			$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
			$servicecall_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENTNAME','#SPNAME','#SPCOMPANY','#DETAIL','#SCHEDULEPHONE','#REPORTS','#LINK'),array($this->selfURL(),$client_name,$spname,$spcompany,$email_string,$spschedularphone,$reportdata,$link),$servicecall_temp['EmailTemplate']['mail_body']);
			$this->set('servicecall_temp',$servicecall_temp['EmailTemplate']['mail_body']);
/* SMTP Options */
$this->Email->smtpOptions = array(
     'port'=>'587',
     'timeout'=>'30',
     'host' => 'smtp.mandrillapp.com',
     'username'=>'admin@reportsonlineplus.com',
     'password'=>'ZfppYXZd9HHZAfLFnCPDZg',
);
		
			/* Set delivery method */ 
			$this->Email->delivery = 'smtp';
			
			$this->Email->to = $client_email;
			$this->Email->subject = $servicecall_temp['EmailTemplate']['mail_subject'];
			$this->Email->from = EMAIL_FROM;
			$this->Email->template = 'service_call_temp'; // note no '.ctp'
			$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
			$this->Email->send();//Do not pass any args to send() 
		}	
		$this->redirect(array('controller'=>'sps','action'=>'serviceCallThanks',base64_encode($this->data['ServiceCall']['client_id'])));
	 }
    }    
    
/*
*************************************************************************
  #FUNCTION USED TO SHOW COMPLETED SCHEDULES 
*************************************************************************
*/	
	function sp_completedschedule(){
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$this->set("sp_id",$sp_id);
		if(isset($this->data) && !empty($this->data)){			
			//Single combination
			if((isset($this->data['Search']['cname']) && !empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && empty($this->data['Search']['ccname']))){
				 $cname = $this->data['Search']['cname'];
				 $cond = "User.fname LIKE '%".$cname."%' OR User.lname LIKE '%".$cname."%' AND User.sp_id = '".$sp_id."'";
			}
			if((isset($this->data['Search']['cname']) && empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && !empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && empty($this->data['Search']['ccname']))){
				 $caddress = $this->data['Search']['caddress'];
				 $cond = "User.address LIKE '%".$caddress."%' AND User.sp_id = '".$sp_id."'";
			}
			if((isset($this->data['Search']['cname']) && empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && !empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && empty($this->data['Search']['ccname']))){
				 $cphone = $this->data['Search']['cphone'];
				 $cond = "User.phone = ".$cphone." AND User.sp_id = '".$sp_id."'";
			}
			if((isset($this->data['Search']['cname']) && empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && !empty($this->data['Search']['ccname']))){
				 $ccname = $this->data['Search']['ccname'];
				 $cond = "User.client_company_name = '".$ccname."' AND User.sp_id = '".$sp_id."'";
			}
			//Double combination
			if((isset($this->data['Search']['cname']) && !empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && !empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && empty($this->data['Search']['ccname']))){
				 $cname = $this->data['Search']['cname'];
				 $caddress = $this->data['Search']['caddress'];
				 $cond = "User.address LIKE '%".$caddress."%' AND User.sp_id = '".$sp_id."' AND User.fname LIKE '%".$cname."%' OR User.lname LIKE '%".$cname."%'";
			}
			if((isset($this->data['Search']['cname']) && empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && !empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && !empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && empty($this->data['Search']['ccname']))){
				 $cphone = $this->data['Search']['cphone'];
				 $caddress = $this->data['Search']['caddress'];
				 $cond = "User.address LIKE '%".$caddress."%' AND User.sp_id = '".$sp_id."' AND User.phone = '".$cphone."'";
			}
			if((isset($this->data['Search']['cname']) && empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && !empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && !empty($this->data['Search']['ccname']))){
				 $cphone = $this->data['Search']['cphone'];
				 $ccname = $this->data['Search']['ccname'];
				 $cond = "User.client_company_name ='".$ccname."' AND User.sp_id = '".$sp_id."' AND User.phone = '".$cphone."'";
			}
			if((isset($this->data['Search']['cname']) && !empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && !empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && empty($this->data['Search']['ccname']))){
				 $cname = $this->data['Search']['cname'];
				 $cphone = $this->data['Search']['cphone'];
				 $cond = "User.phone = '".$cphone."' AND User.sp_id = '".$sp_id."' AND User.fname LIKE '%".$cname."%' OR User.lname LIKE '%".$cname."%'";
			}
			if((isset($this->data['Search']['cname']) && !empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && !empty($this->data['Search']['ccname']))){
				 $cname = $this->data['Search']['cname'];
				 $ccname = $this->data['Search']['ccname'];
				 $cond = "User.client_company_name = '".$ccname."' AND User.sp_id = '".$sp_id."' AND User.fname LIKE '%".$cname."%' OR User.lname LIKE '%".$cname."%'";
			}
			if((isset($this->data['Search']['cname']) && empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && !empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && !empty($this->data['Search']['ccname']))){
				 $caddress = $this->data['Search']['caddress'];
				 $ccname = $this->data['Search']['ccname'];
				 $cond = "User.client_company_name = '".$ccname."' AND User.sp_id = '".$sp_id."' AND User.address LIKE '%".$caddress."%'";
			}
			//Three combinations
			if((isset($this->data['Search']['cname']) && !empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && !empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && !empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && empty($this->data['Search']['ccname']))){
				 $cname = $this->data['Search']['cname'];
				 $cphone = $this->data['Search']['cphone'];
				 $caddress = $this->data['Search']['caddress'];
				 $cond = "User.address LIKE '%".$caddress."%' AND User.phone = '".$cphone."' AND User.sp_id = '".$sp_id."' AND User.fname LIKE '%".$cname."%' OR User.lname LIKE '%".$cname."%'";
			}
			if((isset($this->data['Search']['cname']) && !empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && !empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && !empty($this->data['Search']['ccname']))){
				 $cname = $this->data['Search']['cname'];
				 $cphone = $this->data['Search']['cphone'];
				 $ccname = $this->data['Search']['ccname'];
				 $cond = "User.client_company_name = '".$ccname."' AND User.phone = '".$cphone."' AND User.sp_id = '".$sp_id."' AND User.fname LIKE '%".$cname."%' OR User.lname LIKE '%".$cname."%'";
			}
			if((isset($this->data['Search']['cname']) && empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && !empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && !empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && !empty($this->data['Search']['ccname']))){
				 $caddress = $this->data['Search']['caddress'];
				 $cphone = $this->data['Search']['cphone'];
				 $ccname = $this->data['Search']['ccname'];
				 $cond = "User.client_company_name = '".$ccname."' AND User.phone = '".$cphone."' AND User.sp_id = '".$sp_id."' AND User.address LIKE '%".$caddress."%'";
			}
			if((isset($this->data['Search']['cname']) && !empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && !empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && !empty($this->data['Search']['ccname']))){
				 $cname = $this->data['Search']['cname'];
				 $caddress = $this->data['Search']['caddress'];
				 $ccname = $this->data['Search']['ccname'];
				 $cond = "User.client_company_name = '".$ccname."' AND User.address LIKE '%".$caddress."%' AND User.sp_id = '".$sp_id."' AND User.fname LIKE '%".$cname."%' OR User.lname LIKE '%".$cname."%'";
			}
			//four combination
			if((isset($this->data['Search']['cname']) && !empty($this->data['Search']['cname'])) && (isset($this->data['Search']['caddress']) && !empty($this->data['Search']['caddress'])) && (isset($this->data['Search']['cphone']) && !empty($this->data['Search']['cphone'])) && (isset($this->data['Search']['ccname']) && !empty($this->data['Search']['ccname']))){
				 $cname = $this->data['Search']['cname'];
				 $caddress = $this->data['Search']['caddress'];
				 $ccname = $this->data['Search']['ccname'];
				 $cphone = $this->data['Search']['cphone'];
				 $cond = "User.client_company_name = '".$ccname."' AND User.phone = '".$cphone."' AND User.address LIKE '%".$caddress."%' AND User.sp_id = '".$sp_id."' AND User.fname LIKE '%".$cname."%' OR User.lname LIKE '%".$cname."%'";
			}
			
			
			 $result = $this->User->find("all",array('conditions'=>array($cond),'fields'=>array('User.id')));
			 
				 if(!empty($result)){
					$data = array();
					foreach($result as $key=>$val){
						$data[] = $val['User']['id'];
					}
						$datas = implode(",",$data);
						$where = "ServiceCall.client_id in ('".$datas."')";
						$res = $this->ServiceCall->find("all",array('conditions'=>array($where),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
				 }
		 }/*else {
						$res = $this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));			
		 }*/
		 $conditions = array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>1);
		 if(isset($this->params['url']['client_id']) && $this->params['url']['client_id']!=""){
			$conditions = array_merge($conditions,array('ServiceCall.client_id'=>$this->params['url']['client_id']));
		 }
		 $res = $this->ServiceCall->find("all",array('conditions'=>$conditions,'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		
		if(!empty($res)){
			foreach($res as $key1=>$servicecall):
				$client_arr[]=$servicecall['ServiceCall']['client_id'];
			endforeach;
			$clientFinalArr=array_unique($client_arr);
		}
		if(!empty($res)){		
			$i=0;$j=0;
			foreach($clientFinalArr as $clientID):				
				$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.client_id'=>$clientID,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
				foreach($res1 as $resFinal):		
					$arr[$clientID][$j]=$resFinal;
					$j++;
				endforeach;
				$i++;
			endforeach;		
			$this->set("Clientdata",$arr);
		}
		
		$clientData = $this->User->find('all', array(
					'conditions' => array('User.sp_id'=>$sp_id,'User.user_type_id'=>3),				
					'fields' =>array('id','client_company_name','fname','lname'),
					'order'=>'User.client_company_name',
					'limit'=>'',
					'page'=>''
		));
		$clientCompanyData = array();
		foreach($clientData as $clDatas){
			$clientId = $clDatas['User']['id'];
			$clientName = ucfirst($clDatas['User']['fname'].' '.$clDatas['User']['lname']);
			$clientCompany = ucfirst($clDatas['User']['client_company_name']);
			$clientCompanyData[$clientId] =  $clientName.' ('.$clientCompany.')';
		}
		$this->set('clientCompanyData',$clientCompanyData);
		$this->set('frequency',Configure::read('frequency'));	
	}
	
/*
*************************************************************************
  #FUNCTION USED TO change inspectors assigned to a service call
*************************************************************************
*/	
	function sp_changeInspector(){
    $this->loadModel('Schedule');  
    if(isset($_REQUEST['serviceCallID']) ){
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);		
		$this->set('servicecallId',$_REQUEST['serviceCallID']);		
		$this->set('reportId',$_REQUEST['reportID']);
    $scheduleData=$this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		$this->set('scheduleData',$scheduleData);
		}
		if(isset($this->data) && !empty($this->data)){     
			if(!empty($this->data['Schedule']['helper_inspector'])){
			 $helper_inspector = implode(',',$this->data['Schedule']['helper_inspector']);     
			}  
      $this->data['Schedule']['helper_inspector_id']= $helper_inspector;
      $this->data['Schedule']['lead_inspector_id']= $this->data['Schedule']['lead_inspector'];     
      if($this->Schedule->save($this->data)){     
         $this->Session->setFlash(__('Inspector changed Sucessfully',true));
				  $this->redirect(array('controller'=>'schedules','action'=>'schedule'));
      }else{
           $this->Session->setFlash(__('Not saved',true));
      }
		
    
    }
		
	
	}
	
	/************************************************************************
	function to get sites corresponding to particular client
       ************************************************************************/
	   public function getSiteByClient($clientid=null){
		       configure::write('debug',2);
		       $this->layout = 'ajax';
		       $this->loadModel('SpSiteAddress');
		       if(isset($clientid) && $clientid != "")
		       {
			       $sites = $this->SpSiteAddress->find('list', array(
					   'conditions' => array('SpSiteAddress.client_id' => $clientid,'SpSiteAddress.idle' => 0),
					   'fields'=>array('SpSiteAddress.id','SpSiteAddress.site_name'),
					   'order'=>array('SpSiteAddress.site_name ASC'),
					   'recursive' => -1
					   ));
			       if(!empty($sites)){
					       $this->set('sites',$sites);
					       echo json_encode($sites);
					       //$encodeId = base64_encode($clientid);
					       //$msg = '{"clientId":"'.$encodeId.'","siteData",'.json_encode($sites).'}';
					       //echo $msg;
					       die;
			       }
			       else {
					       $this->set('sites',0);		
			       }
		       }
		       die;
	   }	
	

	function getEncodeId($id = null){
		echo base64_encode($id);
		die;
	}
	
//Function sp_editServiceCall
//Desc To edit the contents of already created service call
//Created By: Manish Kumar on Dec 27, 2012
	function sp_editServiceCall($serviceCallId = null){
		$this->loadModel('User');
		$this->loadModel('SpSiteAddress');
		$this->loadModel('Service');
		$this->loadModel('Schedule');
		$this->loadModel('ServiceCall');
		$this->loadModel('ScheduleService');
		$this->loadmodel('EmailTemplate');
		$this->loadmodel('Report');
		$this->loadmodel('ReportCert');		
		
		$serviceCallId = base64_decode($serviceCallId);
		$this->Schedule->bindModel(array('hasMany'=>array('ReportCert')));
		$serviceData = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$serviceCallId,'ServiceCall.call_status'=>0),'recursive'=>2));
		
		if(empty($serviceData)){
			$this->Session->setFlash('Invalid URL');
			$this->redirect($this->referer());
		}else{			
			$this->set('serviceData',$serviceData);
			$sites = $this->SpSiteAddress->find('list', array(
			'conditions' => array('SpSiteAddress.client_id' => $serviceData['ServiceCall']['client_id'],'SpSiteAddress.idle' => 0),
			'fields'=>array('SpSiteAddress.id','SpSiteAddress.site_name'),
			'order'=>array('SpSiteAddress.site_name ASC'),
			'recursive' => -1
			));
			$this->set('sites',$sites);
		}//pr($serviceData);die;
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		
		#01-10-2012 CODE to fetch the site addresses for selection
		$this->set('siteAddresses',$this->getSiteaddress($sp_id));			
		
		$clientData = $this->getAllClientOfSp($sp_id);
		$this->set('clientData',$clientData);
		
		$this->loadModel('User');
		$clientCompanyData = $this->User->find('list', array(
					'conditions' => array('User.sp_id'=>$sp_id,'User.user_type_id'=>3),
					'fields' =>array('id','client_company_name'),
					'order'=>'User.client_company_name',
					'limit'=>'',
					'page'=>''
		));
		$this->set('clientCompanyData',$clientCompanyData);
		
		$this->loadModel('Report');
		$this->set('frequency',Configure::read('frequency'));
		$sp = $this->Session->read('Log');	
		$this->set('sp_id',$sp_id);		
		$reportData = $this->getAllReports($sp_id);
		$this->set('rData',$reportData);		
			
		$this->set('frequency',Configure::read('frequency'));
		if(isset($this->data) && !empty($this->data))
		{		
			/*ByPass Customer Acceptance Call*/
			if($this->data['ServiceCall']['bypass']=='Yes'){
				$this->data['ServiceCall']['call_status']='1';
			}
			/*ByPass Customer Acceptance Call*/
			$this->ServiceCall->save($this->data['ServiceCall'],false);			
			
			$serviceCallID = $this->ServiceCall->getLastInsertId();
			for($i=1;$i<=6;$i++)
			{				
				if($this->data[$i]['Schedule']['report_id'] != "0")
				{
					$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data[$i]['Schedule']['lead_inspector']),'fields'=>array('fname','lname','email')));
					$leader_name = $leader['User']['fname']." ".$leader['User']['lname'];
					$client = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['ServiceCall']['client_id']),'fields'=>array('fname','lname','site_name','email')));
					$client_name = $client['User']['fname']." ".$client['User']['lname'];
					$client_email = $client['User']['email'];
					$report = $this->Report->find('first',array('conditions'=>array('Report.id'=>$this->data[$i]['Schedule']['report_id']),'fields'=>array('name')));
					$report_name = $report['Report']['name'];
				
					$data[$i]['Schedule']['service_call_id']=$serviceCallID;
					$data[$i]['Schedule']['schedule_date_1']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_1']));
					$data[$i]['Schedule']['schedule_from_1']=$this->data[$i]['Schedule']['schedule_from_1'];
					$data[$i]['Schedule']['schedule_to_1']=$this->data[$i]['Schedule']['schedule_to_1'];
					$data[$i]['Schedule']['submission_date']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['submission_date']));
					if(isset($this->data[$i]['Schedule']['schedule_date_2']) && isset($this->data[$i]['Schedule']['schedule_from_2']) && isset($this->data[$i]['Schedule']['schedule_to_2']))
					{
						$date2= $this->data[$i]['Schedule']['schedule_date_2'];
						$from2= $this->data[$i]['Schedule']['schedule_from_2'];
						$to2= $this->data[$i]['Schedule']['schedule_to_2'];
						$data[$i]['Schedule']['schedule_date_2']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_2']));
						$data[$i]['Schedule']['schedule_from_2']=$this->data[$i]['Schedule']['schedule_from_2'];
						$data[$i]['Schedule']['schedule_to_2']=$this->data[$i]['Schedule']['schedule_to_2'];
					}else{
						$date2= '';
						$from2= '';
						$to2= '';
					}
					if(isset($this->data[$i]['Schedule']['schedule_date_3']) && isset($this->data[$i]['Schedule']['schedule_from_3']) && isset($this->data[$i]['Schedule']['schedule_to_3']))
					{
						$date3= $this->data[$i]['Schedule']['schedule_date_3'];
						$from3= $this->data[$i]['Schedule']['schedule_from_3'];
						$to3= $this->data[$i]['Schedule']['schedule_to_3'];
						$data[$i]['Schedule']['schedule_date_3']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_3']));
						$data[$i]['Schedule']['schedule_from_3']=$this->data[$i]['Schedule']['schedule_from_3'];
						$data[$i]['Schedule']['schedule_to_3']=$this->data[$i]['Schedule']['schedule_to_3'];
					}else{
						$date3= '';
						$from3= '';
						$to3= '';	      
					}
					if(isset($this->data[$i]['Schedule']['schedule_date_4']) && isset($this->data[$i]['Schedule']['schedule_from_4']) && isset($this->data[$i]['Schedule']['schedule_to_4']))
					{
						$date4= $this->data[$i]['Schedule']['schedule_date_4'];
						$from4= $this->data[$i]['Schedule']['schedule_from_4'];
						$to4= $this->data[$i]['Schedule']['schedule_to_4']; 
						$data[$i]['Schedule']['schedule_date_4']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_4']));
						$data[$i]['Schedule']['schedule_from_4']=$this->data[$i]['Schedule']['schedule_from_4'];
						$data[$i]['Schedule']['schedule_to_4']=$this->data[$i]['Schedule']['schedule_to_4'];
					}else{
						$date4= '';
						$from4= '';
						$to4= '';
					}
					if(isset($this->data[$i]['Schedule']['schedule_date_5']) && isset($this->data[$i]['Schedule']['schedule_from_5']) && isset($this->data[$i]['Schedule']['schedule_to_5']))
					{   
						$date5= $this->data[$i]['Schedule']['schedule_date_5'];
						$from5= $this->data[$i]['Schedule']['schedule_from_5'];
						$to5= $this->data[$i]['Schedule']['schedule_to_5'];
						$data[$i]['Schedule']['schedule_date_5']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_5']));
						$data[$i]['Schedule']['schedule_from_5']=$this->data[$i]['Schedule']['schedule_from_5'];
						$data[$i]['Schedule']['schedule_to_5']=$this->data[$i]['Schedule']['schedule_to_5'];
					}else{
						$date5= '';
						$from5= '';
						$to5= '';
					}
					# Added 5 new schedule date, schedule_from and schedule date_to ON 26 JULY 2012(ADDED BY NAVDEEP)
					if(isset($this->data[$i]['Schedule']['schedule_date_6']) && isset($this->data[$i]['Schedule']['schedule_from_6']) && isset($this->data[$i]['Schedule']['schedule_to_6']))
					{   
						$date6= $this->data[$i]['Schedule']['schedule_date_6'];
						$from6= $this->data[$i]['Schedule']['schedule_from_6'];
						$to6= $this->data[$i]['Schedule']['schedule_to_6'];
						$data[$i]['Schedule']['schedule_date_6']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_6']));
						$data[$i]['Schedule']['schedule_from_6']=$this->data[$i]['Schedule']['schedule_from_6'];
						$data[$i]['Schedule']['schedule_to_6']=$this->data[$i]['Schedule']['schedule_to_6'];
					}else{
						$date6= '';
						$from6= '';
						$to6= '';	      
					}
					if(isset($this->data[$i]['Schedule']['schedule_date_7']) && isset($this->data[$i]['Schedule']['schedule_from_7']) && isset($this->data[$i]['Schedule']['schedule_to_7']))
					{   
						$date7= $this->data[$i]['Schedule']['schedule_date_7'];
						$from7= $this->data[$i]['Schedule']['schedule_from_7'];
						$to7= $this->data[$i]['Schedule']['schedule_to_7'];
						$data[$i]['Schedule']['schedule_date_7']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_7']));
						$data[$i]['Schedule']['schedule_from_7']=$this->data[$i]['Schedule']['schedule_from_7'];
						$data[$i]['Schedule']['schedule_to_7']=$this->data[$i]['Schedule']['schedule_to_7'];
					}else{
						$date7= '';
						$from7= '';
						$to7= '';	      
					}
					if(isset($this->data[$i]['Schedule']['schedule_date_8']) && isset($this->data[$i]['Schedule']['schedule_from_8']) && isset($this->data[$i]['Schedule']['schedule_to_8']))
					{   
						$date8= $this->data[$i]['Schedule']['schedule_date_8'];
						$from8= $this->data[$i]['Schedule']['schedule_from_8'];
						$to8= $this->data[$i]['Schedule']['schedule_to_8'];
						$data[$i]['Schedule']['schedule_date_8']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_8']));
						$data[$i]['Schedule']['schedule_from_8']=$this->data[$i]['Schedule']['schedule_from_8'];
						$data[$i]['Schedule']['schedule_to_8']=$this->data[$i]['Schedule']['schedule_to_8'];
					}else{
						$date8= '';
						$from8= '';
						$to8= '';	      
					}
					if(isset($this->data[$i]['Schedule']['schedule_date_9']) && isset($this->data[$i]['Schedule']['schedule_from_9']) && isset($this->data[$i]['Schedule']['schedule_to_9']))
					{   
						$date9= $this->data[$i]['Schedule']['schedule_date_9'];
						$from9= $this->data[$i]['Schedule']['schedule_from_9'];
						$to9= $this->data[$i]['Schedule']['schedule_to_9'];
						$data[$i]['Schedule']['schedule_date_9']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_9']));
						$data[$i]['Schedule']['schedule_from_9']=$this->data[$i]['Schedule']['schedule_from_9'];
						$data[$i]['Schedule']['schedule_to_9']=$this->data[$i]['Schedule']['schedule_to_9'];
					}else{
						$date9= '';
						$from9= '';
						$to9= '';	      
					}
					if(isset($this->data[$i]['Schedule']['schedule_date_10']) && isset($this->data[$i]['Schedule']['schedule_from_10']) && isset($this->data[$i]['Schedule']['schedule_to_10']))
					{   
						$date10= $this->data[$i]['Schedule']['schedule_date_10'];
						$from10= $this->data[$i]['Schedule']['schedule_from_10'];
						$to10= $this->data[$i]['Schedule']['schedule_to_10'];
						$data[$i]['Schedule']['schedule_date_10']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_10']));
						$data[$i]['Schedule']['schedule_from_10']=$this->data[$i]['Schedule']['schedule_from_10'];
						$data[$i]['Schedule']['schedule_to_10']=$this->data[$i]['Schedule']['schedule_to_10'];
					}else{
						$date10= '';
						$from10= '';
						$to10= '';	      
					}				
					$data[$i]['Schedule']['report_id']=$this->data[$i]['Schedule']['report_id'];		
					$data[$i]['Schedule']['lead_inspector_id']=$this->data[$i]['Schedule']['lead_inspector'];
					if(!empty($this->data[$i]['Schedule']['helper_inspector'])){						$helper_inspector = implode(',',$this->data[$i]['Schedule']['helper_inspector']);
					}else{
						$helper_inspector = '';	
					}
					$data[$i]['Schedule']['helper_inspector_id']=$helper_inspector;				
					$this->Schedule->create();				
					$this->Schedule->save($data[$i]['Schedule'],false);
					$scheduleId = $this->Schedule->getLastInsertId();
					for($j=1;$j<=count($this->data[$i]['Schedule']['ScheduleService']);$j++)
					{
						if($this->data[$i]['Schedule']['ScheduleService'][$j]['service_id'] != "0")
						{
							$datas['ScheduleService']['schedule_id'] = $scheduleId;
							$datas['ScheduleService']['service_id']=$this->data[$i]['Schedule']['ScheduleService'][$j]['service_id'];
							$datas['ScheduleService']['amount']=$this->data[$i]['Schedule']['ScheduleService'][$j]['amount'];
							$datas['ScheduleService']['frequency']=$this->data[$i]['Schedule']['ScheduleService'][$j]['frequency'];
							$this->ScheduleService->create();
							$this->ScheduleService->save($datas['ScheduleService'],false);
						}
					}				
					/*Save the reports name to be attached code starts here*/
					for($cr=1;$cr<=count($this->data[$i]['ReportCert']);$cr++)
					{
						if($this->data[$i]['ReportCert'][$cr]['cert_id'] != "0")
						{
							$certdatas['ReportCert']['schedule_id'] = $scheduleId;
							$certdatas['ReportCert']['cert_id']=$this->data[$i]['ReportCert'][$cr]['cert_id'];				
							$this->ReportCert->create();
							$this->ReportCert->save($certdatas['ReportCert'],false);		
						}
					}
					/*Save the reports name to be attached code ends here*/
				}
			}
			//end forloop
			
			/*Delete Previous Entries*/
			$this->Schedule->bindModel(array('hasMany'=>array('ReportCert'=>array('foreignKey'=>'schedule_id','dependent'=>true))),true);
			$this->Schedule->deleteAll(array('Schedule.service_call_id'=>$serviceCallId),true);
			$this->ServiceCall->delete($serviceCallId,true);
			/*Delete Previous Entries*/
			
			/*ByPass Customer Acceptance Call*/
			if($this->data['ServiceCall']['bypass']=='Yes'){			
				$this->ServiceCall->updateAll(array('ServiceCall.bypass_mail'=>'"1"'),array('ServiceCall.id'=>$serviceCallID));			
				App::import('Controller', 'Clients');
				// We need to load the class
				$Clients = new ClientsController;
				// If we want the model associations, components, etc to be loaded
				$Clients->sendApproveMail($serviceCallID);
			}else{
			/*ByPass Customer Acceptance Call*/
			#code to send email to client to give new service call info(ADDED BY NAVDEEP ON 26 JULy2012)
				//$serviceCallID	 
					 
				$client_name; //cleintname
				$sp = $this->Session->read('Log');
				$spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
				$spschedularphone = $sp['User']['phone']; //sp-schedulerphone
				$spcompany=$sp['Company']['name'];
				$this->Schedule->bindModel(
						array('belongsTo' => array(
						'Report' => array(
						'className' => 'Report',
						'fields'=>array('Report.name')
							)
						    )
						 )
						);
				//$this->Schedule->bindModel('belongsTo'=>'Report');
				$Schdata=$this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$serviceCallID)));
				//pr($Schdata);
				//echo "===============>";
			       
				$email_string ='';
				$email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
							<tbody>
							<tr style="background-color:#0B83A6">
							    <td><b>ReportName</b></td>
							    <td><b>Days</b></td>
							    <td><b>Schedule Date/Time</b></td>
							    <td><b>Inspector Assigned</b></td>
							</tr>';
				$reportdata='';
				$sch2="";$sch3="";$sch4="";$sch5="";$sch6="";$sch7="";$sch8="";$sch9="";$sch10="";
				foreach($Schdata as $key=>$sch){
					$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['Schedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
					$reportdata   .=$sch['Report']['name'].',';
					$email_string .= "<tr>";
					$days=0;
					$email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';       
					$sch1= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_1'])); $days=$days+1;
					 if(!empty($sch['Schedule']['schedule_date_2'])) { 
						 $sch2= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_2'])); $days=$days+1;
					 }
					 if(!empty($sch['Schedule']['schedule_date_3'])) { 
						 $sch3= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_3']));$days=$days+1;
					 }
					 if(!empty($sch['Schedule']['schedule_date_4'])) { 
						 $sch4= date('m/d/Y',strtotime($sch['schedule_date_4'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_4']));$days=$days+1;
					 }
					 if(!empty($schedule['schedule_date_5'])) { 
						 $sch5= date('m/d/Y',strtotime($sch['schedule_date_5'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['schedule_to_5']));$days=$days+1;
					 }
					 if(!empty($sch['Schedule']['schedule_date_6'])) { 
						 $sch6= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_6']));$days=$days+1;
					 } 
					 if(!empty($sch['Schedule']['schedule_date_7'])) { 
						 $sch7= date('m/d/Y',strtotime($sch['schedule_date_7'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_7']));$days=$days+1;
					 }
					 if(!empty($sch['Schedule']['schedule_date_8'])) { 
						 $sch8= date('m/d/Y',strtotime($sch['schedule_date_8'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_8']));$days=$days+1;
					 }
					 if(!empty($sch['Schedule']['schedule_date_9'])) { 
						 $sch9= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_9']));$days=$days+1;
					 }
					 if(!empty($sch['Schedule']['schedule_date_10'])) { 
						 $sch10= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_10']));$days=$days+1;
					 }
					 
					$email_string .='<td valign="top">'.$days.'</td>';		    
					$email_string .='<td valign="top">'.$sch1.'<br/>'.@$sch2.'<br/>'.@$sch3.'<br/>'.@$sch4.'<br/>'.@$sch5.'<br/>'.@$sch6.'<br/>'.@$sch7.'<br/>'.@$sch8.'<br/>'.@$sch9.'<br/>'.@$sch10.'</td>';
					$email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
					$email_string .= "</tr>";
				}
				       
				$email_string .= '</tbody></table>';				
				$servicecall_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>6)));
				$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
				$servicecall_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENTNAME','#SPNAME','#SPCOMPANY','#DETAIL','#SCHEDULEPHONE','#REPORTS','#LINK'),array($this->selfURL(),$client_name,$spname,$spcompany,$email_string,$spschedularphone,$reportdata,$link),$servicecall_temp['EmailTemplate']['mail_body']);
				$this->set('servicecall_temp',$servicecall_temp['EmailTemplate']['mail_body']);
				
				
				$this->Email->to = $client_email;
				$this->Email->subject = $servicecall_temp['EmailTemplate']['mail_subject'];
				$this->Email->from = EMAIL_FROM;
				$this->Email->template = 'service_call_temp'; // note no '.ctp'
				$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
				$this->Email->send();//Do not pass any args to send() 
			}
			$this->Session->setFlash('Service Call has been updated successfully',true);
			$this->redirect(array('controller'=>'schedules','action'=>'pendingSchedule'));
		 }
	}
	
//Function sp_setScheduleTime
//Desc TO set the schedule to time on chnage of from schedule time
//Created By Manish Kumar on Jan 03, 2013
	function sp_setScheduleTime(){ 
		$fromTime = $this->params['form']['fromVal'];
		if($fromTime!=""){
			$timeArray = array('12:00 am','12:30 am','01:00 am','01:30 am','02:00 am','02:30 am','03:00 am','03:30 am','04:00 am','04:30 am','05:00 am','05:30 am','06:00 am','06:30 am','07:00 am','07:30 am','08:00 am','08:30 am','09:00 am','09:30 am','10:00 am','10:30 am','11:00 am','11:30 am','12:00 pm','12:30 pm','01:00 pm','01:30 pm','02:00 pm','02:30 pm','03:00 pm','03:30 pm','04:00 pm','04:30 pm','05:00 pm','05:30 pm','06:00 pm','06:30 pm','07:00 pm','07:30 pm','08:00 pm','08:30 pm','09:00 pm','09:30 pm','10:00 pm','10:30 pm','11:00 pm','11:30 pm');
			$timeArrayLength = count($timeArray);
			$startIndex = array_search($fromTime, $timeArray);
			//echo $timeArrayLength;die;
			$returnData = '<option value=""></option>';
			for($k = $startIndex;$k<$timeArrayLength;$k++){
				$valPart = $timeArray[$k];
				$returnData.='<option value="'.$valPart.'">'.$valPart.'</option>';
			}			
		}else{
			$returnData = '';
		}
		echo $returnData;
		die;
	}
	
//Function sp_changeSchedule
//Desc To change the schedule timings for expired and not started report
//Created By Manish Kumar on Feb 05, 2013
	function sp_changeSchedule($report_id=null,$servicecall_id=null,$client_id=null,$sp_id=null){
		$this->layout='';
		$this->loadModel('Schedule');
		$schedule=$this->Schedule->find('first',array('conditions'=>array('Schedule.report_id'=>$report_id,'Schedule.service_call_id'=>$servicecall_id)));
		$this->set('schedule',$schedule);
		if(!empty($this->data)){
			$this->data['Schedule']['schedule_date_1']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_1']));
			$this->data['Schedule']['submission_date']=date("Y-m-d",strtotime($this->data['Schedule']['submission_date']));
			if(isset($this->data['Schedule']['schedule_date_2'])){
				$this->data['Schedule']['schedule_date_2']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_2']));
			}
			if(isset($this->data['Schedule']['schedule_date_3'])){
				$this->data['Schedule']['schedule_date_3']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_3']));
			}
			if(isset($this->data['Schedule']['schedule_date_4'])){
				$this->data['Schedule']['schedule_date_4']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_4']));
			}
			if(isset($this->data['Schedule']['schedule_date_5'])){
				$this->data['Schedule']['schedule_date_5']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_5']));
			}
			if(isset($this->data['Schedule']['schedule_date_6'])){
				$this->data['Schedule']['schedule_date_6']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_6']));
			}
			if(isset($this->data['Schedule']['schedule_date_7'])){
				$this->data['Schedule']['schedule_date_7']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_7']));
			}
			if(isset($this->data['Schedule']['schedule_date_8'])){
				$this->data['Schedule']['schedule_date_8']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_8']));
			}
			if(isset($this->data['Schedule']['schedule_date_9'])){
				$this->data['Schedule']['schedule_date_9']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_9']));
			}
			if(isset($this->data['Schedule']['schedule_date_10'])){
				$this->data['Schedule']['schedule_date_10']=date("Y-m-d",strtotime($this->data['Schedule']['schedule_date_10']));
			}
			$this->Schedule->save($this->data);
			$this->Session->setFlash('Schedule has been updated successfully');
			$this->redirect($this->referer());
		}
	}
	
//Function sp_not_started_schedule()
//Desc For not started Schedule
//Created By Manish Kumar
	function sp_not_started_schedule(){		
		$this->set('title_for_layout',"Not Started Schedule");
		if(!isset($tstamp)){
			$tstamp=time();
		}
		$current_month_no=date('m',$tstamp);
		$current_year=date('Y',$tstamp);
		$this->set('tstamp',$tstamp);
		
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		
		
		#CODE 27-07-2012
		if(!empty($res)){
			foreach($res as $key1=>$servicecall):
			$client_arr[]=$servicecall['ServiceCall']['client_id'];
			endforeach;
			$clientFinalArr=array_unique($client_arr);
		}
		if(!empty($res)){		
			$i=0;$j=0;
			foreach($clientFinalArr as $clientID):				
				$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.client_id'=>$clientID,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
				foreach($res1 as $resFinal):		
					$arr[$clientID][$j]=$resFinal;
					$j++;
				endforeach;
				$i++;
			endforeach;
			//pr($arr);
			$this->set("Clientdata",$arr);	
			$this->set('frequency',Configure::read('frequency'));
		}	
	}
}