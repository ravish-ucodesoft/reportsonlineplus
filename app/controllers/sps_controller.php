<?php ob_start();
class SpsController extends AppController {
	
	var $name = 'Sps';
	var $uses = array('User','Company','AssignInspector');
	var $components = array('Email','Session','Cookie','Customupload','Paypal');
	var $helpers = array('Html','Ajax','Javascript','Session','Common','Pagination','Time','Fck');
	var $paginate_limit = 10;
	function beforeFilter(){
	 parent::beforeFilter();
	}
	
	function getselectarray($value){
	configure::write('debug',0);
		$this->layout = null;
	
		if($value == 'M'){
		 for($i=1;$i<=15;$i++){
		 $htmldata .= "<option value='$i'>$i</option>";
		 }  
		  echo $htmldata; exit;
		}elseif($value == 'W'){   
		 for($i=1;$i<=5;$i++){
		 $htmldata .= "<option value='$i'>$i</option>";  
		 }
		  echo $htmldata; exit;
		}else{
		  for($i=1;$i<=30;$i++){
		 $htmldata .= "<option value='$i'>$i</option>";  
		 }
		  echo $htmldata; exit;
		}
  	
  }
  
  
	function sp_tutorial(){
		$this->layout='sp';
		$this->loadModel('TutorialScreenshot');
		$this->loadModel('TutorialText');
		$this->loadModel('TutorialVideo');
		$snapshotData =  $this->TutorialScreenshot->find('all',array('order'=>'TutorialScreenshot.id DESC'));
		$videoData = $this->TutorialVideo->find('all',array('order'=>'TutorialVideo.id DESC'));
		$textData=$this->TutorialText->find('first',array('order'=>'TutorialText.id DESC'));
		$this->set('snapshotData',$snapshotData);
		$this->set('videoData',$videoData);
		$this->set('textData',$textData);
	}
	/*
****************************************************************************
*Function Name		 :	user registration subscription plan function
*Functionality		 :	Registration panel.
****************************************************************************
*/ 
	function sp_subscriptionplan(){
	  $this->layout = 'sp';
	  $this->loadModel('SubscriptionDuration');
      $this->loadModel('User');
      $this->loadModel('Company');
      $this->loadModel('UserCompanyPrincipalRecord');
      $SubscriptionData=$this->SubscriptionDuration->find("all");
      $this->set('SubscriptionData', $SubscriptionData);
      $this->loadModel('MainTab');
	  $this->loadModel('Payment');
      $tabData=$this->MainTab->find('first');
      $this->set('tabData',$tabData);
	  $data = $this->Session->read('Log');
	  $this->set('subscription_duration_monthly',$data['User']['subscription_duration_id']."-".$data['User']['subscription_amount']."-".$data['User']['subscription_duration_plan']);
	   if(isset($this->data) && !empty($this->data)){
			$paymentInfo = array('Member'=>
                           array(
			                    'first_name'=>$data['User']['fname'],
                               'last_name'=>$data['User']['lname'],
                               'email'=>$data['User']['email'],
                               'billing_address'=>$data['User']['address'],
                               'billing_address2'=>'559',
                               'billing_country'=>'US',
                               'billing_city'=>$data['User']['city'],
                               'billing_state'=>$data['State']['name'],
                               'billing_zip'=>$data['User']['zip'],
							   'billing_period'=>"Month"
								
                           ),
                          'CreditCard'=>
                           array(
                               'card_number'=>$this->data['Company']['cardnumber'],
			                    'credit_type'=>$this->data['Company']['cardtype'],
                               'expiration_month'=>$this->data['Company']['month'],
                               'expiration_year'=>$this->data['Company']['year'],
                               'cv_code'=>$this->data['Company']['cardcvnumber']
                           ),
                          'Order'=>
                          array('theTotal'=>$data['User']['subscription_amount'])
                    );
				/*
				 * On Success, $result contains [AMT] [CURRENCYCODE] [AVSCODE] [CVV2MATCH]
				 * [TRANSACTIONID] [TIMESTAMP] [CORRELATIONID] [ACK] [VERSION] [BUILD]
				 *
				 * On Fail, $ result contains [AMT] [CURRENCYCODE] [TIMESTAMP] [CORRELATIONID]
				 * [ACK] [VERSION] [BUILD] [L_ERRORCODE0] [L_SHORTMESSAGE0] [L_LONGMESSAGE0]
				 * [L_SEVERITYCODE0]
				 *
				 * Success or Failure is best tested using [ACK].
				 * ACK will either be "Success" or "Failure"
				 */
			 
				 $result = $this->Paypal->processPayment($paymentInfo,"CreateRecurringPayments");
				 //$result = $this->Paypal->processRecuringPayment($paymentInfo,"CreateRecurringPayments");
				 $ack = strtoupper($result["ACK"]);
			 
				 if($ack!="SUCCESS"){
								$paln_data = explode("-",$this->data['User']['subscription_duration_monthly']);
								$this->User->id = $data['User']['id'];
								$Userdata['User']['id'] = $data['User']['id'];
								$Userdata['User']['subscription_under_trial'] = "No";
								
								$Userdata['User']['subscription_duration_id'] = $paln_data[0];
								$Userdata['User']['amount'] = $paln_data[1];
								$Userdata['User']['subscription_duration_plan'] = $paln_data[2];
								
								$this->User->save($Userdata['User'],false);
								$paymentData['Payment']['user_id'] = $data['User']['id'];
								$card_number = substr($this->data['Company']['cardnumber'],-4);
								$card_number = "XXXXXXXXXXX".$card_number;
								$paymentData['Payment']['ccreditcard_number'] = $card_number;
								$paymentData['Payment']['ccv_number'] = $this->data['Company']['cardcvnumber'];
								$paymentData['Payment']['amount'] = $data['User']['subscription_amount'];
                                  if($this->Payment->save($paymentData['Payment'])){
                                       $this->Session->setFlash('Sucessful transaction');
				                       $this->redirect(array('controller'=>'sps','action'=>'dashboard'));

                                 }
				 }
				 
    }
}
	
	
	#Added on 28-09-2012
	function sp_view_site_address()
	{
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$this->layout='sp';
	        $this->set('title_for_layout',"Service Provider Site Addresses");
		$this->loadModel('SpSiteAddress');		
		$siteaddress = $this->SpSiteAddress->find('all',array('conditions'=>array('SpSiteAddress.sp_id'=>$sp_id),'recursive'=>2));
	  
		$this->set('siteaddress',$siteaddress);
		
		$this->loadModel('SubscriptionDuration');
		$result=$this->SubscriptionDuration->find('first',array('fields'=>array('SubscriptionDuration.max_allowed_site_address'),'conditions'=>array('SubscriptionDuration.id'=>$sp['User']['subscription_duration_id'])));
		$this->set('maxallowed',$result);
		
	}
	
	
	#Added on 23-11-2012
	function sp_inspector_changepwd($inspector_id=null)
	{
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$this->layout='ajax';
	    $this->set('title_for_layout',"Inspector Change Password");
		$inspector_id=base64_decode($inspector_id);
		$this->User->service = true;
		$this->User->unbindModel(array('hasMany'=>array('SpSiteAddress'),'belongsTo'=>array('BillCity','State','Country','Company','BillState','BillCountry')));
		$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>2,'User.id'=>$inspector_id)));
		$this->set('result',$result);
		if(!empty($this->data))
		{
			$this->loadModel('User');
			$this->User->id=$this->data['User']['id'];
			$pwd=$this->data['User']['password'];
			$data['User']['password']=md5($this->data['User']['password']);
			$this->User->unbindModel(array('hasMany'=>array('SpSiteAddress'),'belongsTo'=>array('BillCity','State','Country','Company','BillState','BillCountry')));
			$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>2,'User.id'=>$this->data['User']['id'])));
			if($this->User->save($data['User'],false)){					
					#send email to inspector
					$body="Dear ".$result['User']['fname'].",<br/><br/>Your Password has been changed by your Service provider on reportsonlineplus.com<br/>The New Password is : ".$pwd."<br/><br/>Thanks,<br/>Support team,<br/>Reportsonlineplus.com";
					$this->set('register_temp',$body);
	/* SMTP Options */
$this->Email->smtpOptions = array(
     'port'=>'587',
     'timeout'=>'30',
     'host' => 'smtp.mandrillapp.com',
     'username'=>'admin@reportsonlineplus.com',
     'password'=>'ZfppYXZd9HHZAfLFnCPDZg',
);
		
	/* Set delivery method */ 
$this->Email->delivery = 'smtp';
					$this->Email->to = $result['User']['email'];
					$this->Email->subject = "Your Password has been changed on ReportsOnlinePlus.com by Service provider";
					$this->Email->from = EMAIL_FROM;
					$this->Email->template = 'register_mail'; // note no '.ctp'
					$this->Email->sendAs = 'both'; // because we like to send pretty mail			
					$this->Email->send();//Do not pass any args to send()
/* Check for SMTP errors. */ 
$this->set('smtp_errors', $this->Email->smtpError);
					$this->Session->setFlash(__( ucwords($result['User']['fname']) .' Password has been changed successfully',true));
					$this->redirect(array('controller'=>'sps','action'=>'inspectorlisting'));
				
				}
		}
		
	}
	
	
	#Added on 23-11-2012
	function sp_client_changepwd($client_id=null)
	{
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$this->layout='ajax';
	    $this->set('title_for_layout',"Client Change Password");
		$client_id=base64_decode($client_id);
		$this->User->service = true;
		$this->User->unbindModel(array('hasMany'=>array('SpSiteAddress'),'belongsTo'=>array('BillCity','State','Country','Company','BillState','BillCountry')));
		$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>3,'User.id'=>$client_id)));
		$this->set('result',$result);		
		if(!empty($this->data))
		{
			$this->loadModel('User');
			$this->User->id=$this->data['User']['id'];
			$pwd=$this->data['User']['password'];
			$data['User']['password']=md5($this->data['User']['password']);
			$this->User->unbindModel(array('hasMany'=>array('SpSiteAddress'),'belongsTo'=>array('BillCity','State','Country','Company','BillState','BillCountry')));
			$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>3,'User.id'=>$this->data['User']['id'])));
			
			if($this->User->save($data['User'],false)){					
					#send email to inspector
					$body="Dear ".$result['User']['fname'].",<br/><br/>Your Password has been changed by your Service provider on reportsonlineplus.com<br/>The New Password is : ".$pwd."<br/><br/>Thanks,<br/>Support team,<br/>Reportsonlineplus.com";
					$this->set('register_temp',$body);
					$this->Email->to = $result['User']['email'];
					$this->Email->subject = "Your Password has been changed on ReportsOnlinePlus.com by Service provider";
					$this->Email->from = EMAIL_FROM;
					$this->Email->template = 'register_mail'; // note no '.ctp'
					$this->Email->sendAs = 'both'; // because we like to send pretty mail			
					$this->Email->send();//Do not pass any args to send()
					
					$this->Session->setFlash(__( ucwords($result['User']['client_company_name']) .' Company Password has been changed successfully',true));
					$this->redirect(array('controller'=>'sps','action'=>'clientlisting'));
				
				}
		}
		
	}
	
	
	#Added on 28-09-2012
	function sp_add_site_address(){
		$this->loadModel('SpSiteAddress');
		$sp = $this->Session->read('Log');
		$spId = $sp['User']['id'];
		$this->loadModel('SpSiteAddress');
		$siteaddress = $this->SpSiteAddress->find('all',array('conditions'=>array('SpSiteAddress.sp_id'=>$spId)));
		$this->loadModel('SubscriptionDuration');
		$result=$this->SubscriptionDuration->find('first',array('fields'=>array('SubscriptionDuration.max_allowed_site_address'),'conditions'=>array('SubscriptionDuration.id'=>$sp['User']['subscription_duration_id'])));
		if(count($siteaddress)<$result['SubscriptionDuration']['max_allowed_site_address']){
		//if(count($siteaddress)<8){
			$this->set('country',$this->getCountries());
			$this->set('state',$this->getState(233));
			if(!empty($this->data))
			{
				$data['SpSiteAddress']['sp_id'] = $spId;
				$data['SpSiteAddress']['site_name'] = $this->data['SpSiteAddress']['site_name'];
				$data['SpSiteAddress']['site_email'] = $this->data['SpSiteAddress']['site_email'];
				$data['SpSiteAddress']['site_phone']=$this->data['SpSiteAddress']['site_phone1'].'-'.$this->data['SpSiteAddress']['site_phone2'].'-'.$this->data['SpSiteAddress']['site_phone3'];
				$data['SpSiteAddress']['site_address']=$this->data['SpSiteAddress']['site_address'];
				$data['SpSiteAddress']['site_contact_name']=$this->data['SpSiteAddress']['site_contact_name'];
				$data['SpSiteAddress']['state_id']=$this->data['SpSiteAddress']['state_id'];
				$data['SpSiteAddress']['country_id']=$this->data['SpSiteAddress']['country_id'];
				$data['SpSiteAddress']['site_city']=$this->data['SpSiteAddress']['site_city'];
				$data['SpSiteAddress']['site_zip']=$this->data['SpSiteAddress']['site_zip'];
				if($this->SpSiteAddress->save($data['SpSiteAddress'],false)){
					$this->Session->setFlash(__('New Site Address has been added successfully',true));
					$this->redirect(array('controller'=>'sps','action'=>'view_site_address'));
				}
			}
		}
		else{
			$this->Session->setFlash(__('You can not add more than '.$result['SubscriptionDuration']['max_allowed_site_address'].' site addresses. Please subscribe any of the following subscription plans to add more sites',true));
			$this->redirect(array('controller'=>'sps','action'=>'subscriptionplan'));
		}
	}
	
	function sp_edit_site_address($addid = null){
		$this->loadModel('SpSiteAddress');
		$this->loadModel('ServiceCall');
		if($addid==null){
			$addid = $this->data['SpSiteAddress']['id'];
		}
		$serviceCount = $this->ServiceCall->find('count',array('conditions'=>array('ServiceCall.sp_site_address_id'=>$addid)));
		if($serviceCount>0){ 
			$this->Session->setFlash(__('This Site Address cannot be edited as it has already been used in earlier service call. ',true));
			$this->redirect(array('controller'=>'sps','action'=>'view_site_address'));
		}
	        if(isset($this->data) && !empty($this->data)){
			$data['SpSiteAddress']['id'] = $this->data['SpSiteAddress']['id'];
			$data['SpSiteAddress']['site_name'] = $this->data['SpSiteAddress']['site_name'];
			$data['SpSiteAddress']['site_email'] = $this->data['SpSiteAddress']['site_email'];
			$data['SpSiteAddress']['site_phone']=$this->data['SpSiteAddress']['site_phone1'].'-'.$this->data['SpSiteAddress']['site_phone2'].'-'.$this->data['SpSiteAddress']['site_phone3'];
			$data['SpSiteAddress']['site_address']=$this->data['SpSiteAddress']['site_address'];
			$data['SpSiteAddress']['site_contact_name']=$this->data['SpSiteAddress']['site_contact_name'];
			$data['SpSiteAddress']['state_id']=$this->data['SpSiteAddress']['state_id'];
			$data['SpSiteAddress']['country_id']=$this->data['SpSiteAddress']['country_id'];
			$data['SpSiteAddress']['site_city']=$this->data['SpSiteAddress']['site_city'];
			$data['SpSiteAddress']['site_zip']=$this->data['SpSiteAddress']['site_zip'];
			if($this->SpSiteAddress->save($data['SpSiteAddress'],false)){
				$this->Session->setFlash(__('Site Address has been updated successfully',true));
				$this->redirect(array('controller'=>'sps','action'=>'view_site_address'));
			}
		}
		else{
		     $this->set('country',$this->getCountries());
		     $this->set('state',$this->getState(233));
		     $siteaddress = $this->SpSiteAddress->find('all',array('conditions'=>array('SpSiteAddress.id'=>$addid)));
		     $this->set('siteaddress',$siteaddress);
		}
	}
	
	function sp_dashboard(){
	  $this->layout='sp';
	  
	  $this->set('title_for_layout',"Service Provider Landing Page");
	  $this->loadModel('ServiceCall');
	  $this->loadModel('Message');
	  $sp = $this->Session->read('Log');
	  $sp_id = $sp['User']['id'];
	  $sp_subscription_id = $sp['User']['subscription_duration_id'];
	  #CODE to fetch max site addresses 28-09-2012
	  $this->loadModel('SubscriptionDuration');
	  $max_siteaddress=$this->SubscriptionDuration->find('first',array('conditions'=>array('SubscriptionDuration.id'=>$sp_subscription_id)));
	  $this->set('max_site_address',$max_siteaddress);
	  
	  
	  
	  //$sp_subscription_day = $this->ServiceCall->query("SELECT sd.max_allowed_site_address from users u inner join subscription_durations sd on sd.id = u.subscription_duration_id where users.id=$sp_id");
	  $year = date('Y');
	  if(isset($this->data) && !empty($this->data)){
		    $this->set('year',$this->data['User']['year']);
			$chartData = $this->ServiceCall->query("SELECT count(ServiceCall.id) as total,Month(ServiceCall.created) as month FROM `service_calls` AS `ServiceCall` WHERE ServiceCall.sp_id =".$sp_id." AND Year(ServiceCall.created) = ".$this->data['User']['year']." GROUP BY Month( ServiceCall.created )");
	  }else {
			$chartData = $this->ServiceCall->query("SELECT count(ServiceCall.id) as total,Month(ServiceCall.created) as month FROM `service_calls` AS `ServiceCall` WHERE ServiceCall.sp_id =".$sp_id." AND Year(ServiceCall.created) = Year(CURRENT_TIMESTAMP) GROUP BY Month( ServiceCall.created )");
			$this->set('year',$year);
	  }
	  $month = array();
	  foreach($chartData as $res):      
		$month[$res[0]['month']]=$res[0]['total'];
          endforeach;
	  $sum = 0;
	  for($i=0;$i<=12;$i++){
		if(array_key_exists($i,$month)){
			  $arr[$i] = $month[$i];
			  $sum = $sum+$month[$i];
		}else{
			  $arr[$i]='0';      
		}
          }
	  
	  $upto = $year-5;
	  $preMonths = array();
	  for($a=$upto;$a<=$year;$a++){
		$preMonths[$a] = $a; 
	   }
	  
	  $this->set('preMonths',$preMonths);
          $this->set('final_value',$arr);
	  
	  $this->set('sum',$sum); 
	  #Calculate number of inspectors
	  $count_inspectors = $this->User->find('list',array('conditions'=>array('User.sp_id'=>$sp_id,'User.user_type_id'=>2)));

	  $this->set('count_inspectors',count($count_inspectors));
	  #Calculate number of clients
	  $count_clients = $this->User->find('count',array('conditions'=>array('User.sp_id'=>$sp_id,'User.user_type_id'=>3)));
	  $this->set('count_clients',$count_clients);
	  #Calculate number of servicecalls
	  $count_servicecalls = $this->ServiceCall->find('count',array('conditions'=>array('ServiceCall.sp_id'=>$sp_id)));
	  $this->set('count_servicecalls',$count_servicecalls);	
	  #Calculate number of unread messages
	  $count_messages = $this->Message->find('count',array('conditions'=>array('Message.receiver_id'=>$sp_id,'Message.message_read'=>'N')));
	  $this->set('count_messages',$count_messages);
	  $this->set('businessType',Configure::read('businessType'));
          $this->set('reportType',Configure::read('reportType'));
	  
	}
/*
*************************************************************************
*Function Name		 :	Agent Logout
*Functionality		 :	use to Logout of Agent section.
*************************************************************************
*/	
	function sp_logout()
	{
		
		if ( $this->Cookie->read("Log.User") ) {
			$this->Cookie->delete('Log.User');
		}
		$this->Session->destroy('Log');
		$this->Session->setFlash('You have been logged out successfully');
		$this->redirect('/homes/login');
	}
/*
***************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of Agent section.
***************************************************************************
*/
    function sp_clientlisting(){
	 $sp = $this->Session->read('Log');
	 $this->loadModel('Check');
	 $this->loadModel('ClientCheck');
	 $checkdata= $this->Check->find('list',array('fields'=>array('id','name')));
	 $this->set('checkdata',$checkdata);
	 $agent_id = $sp['User']['id'];
	 $conditions = array();
	 $conditions[] = 'User.user_type_id=3 AND User.sp_id='.$agent_id;
	 
	 if($this->data['User']['fname']!='' || $this->data['User']['lname']!='' || $this->data['User']['phone']!='' || $this->data['User']['email']!='' || $this->data['User']['address']!='' || $this->data['User']['site_name']!='' || $this->data['User']['site_address']!='' || $this->data['User']['site_cname']!='' || $this->data['User']['site_email']!='' || $this->data['User']['site_phone']!='' || $this->data['User']['site_phone']!=''){
				foreach ($this->data['User'] as $k=>$v){
					$url['User'.'.'.$k]=$v;
 				}
				$this->redirect($url, null, true);
		}
	 
	 
	 
	 if(!empty($this->passedArgs['User.fname'])){
			$fname = $this->passedArgs['User.fname'];
			$conditions[] = 'User.fname LIKE "%'.$fname.'%"';
		}
		if(!empty($this->passedArgs['User.lname'])){
			$lname = $this->passedArgs['User.lname'];
			$conditions[] = 'User.lname LIKE "%'.$lname.'%"';
		}
		if(!empty($this->passedArgs['User.phone'])){
			$phone = $this->passedArgs['User.phone'];
			$conditions[] = 'User.phone LIKE "%'.$phone.'%"';
		}
		if(!empty($this->passedArgs['User.email'])){
			$email = $this->passedArgs['User.email'];
			$conditions[] = 'User.email LIKE "%'.$email.'%"';
		}
		if(!empty($this->passedArgs['User.address'])){
			$address = $this->passedArgs['User.address'];
			$conditions[] = 'User.address LIKE "%'.$address.'%"';
		}
		if(!empty($this->passedArgs['User.site_name'])){
			$site_name = $this->passedArgs['User.site_name'];
			$conditions[] = 'User.site_name LIKE "%'.$site_name.'%"';
		}
		if(!empty($this->passedArgs['User.site_phone'])){
			$site_phone = $this->passedArgs['User.site_phone'];
			$conditions[] = 'User.site_phone LIKE "%'.$site_phone.'%"';
		}
		if(!empty($this->passedArgs['User.site_email'])){
			$site_email = $this->passedArgs['User.site_email'];
			$conditions[] = 'User.site_email LIKE "%'.$site_email.'%"';
		}
		if(!empty($this->passedArgs['User.site_address'])){
			$site_address = $this->passedArgs['User.site_address'];
			$conditions[] = 'User.site_address LIKE "%'.$site_address.'%"';
		}
		if(!empty($this->passedArgs['User.site_cname'])){
			$site_cname = $this->passedArgs['User.site_cname'];
			$conditions[] = 'User.site_cname LIKE "%'.$site_cname.'%"';
		}
	 
	 
	 
	 if(!empty($conditions)){  
				$conditionsstr =  implode(' AND ',$conditions);
		}else{
			$conditionsstr = 'User.user_type_id=3 AND User.sp_id='.$agent_id;
		}
	 $this->User->bindModel(
				array(
				      'hasMany'=>array(
							'SpSiteAddress'=>array(
							     'className'=>'SpSiteAddress',
							     'foreignKey'=>'client_id',
							     'conditions'=>array('SpSiteAddress.idle'=>array(0,1))
							     )
							)
				     ), false
				);	 
	 $this->paginate = array('conditions' =>$conditionsstr,'order'=>array('User.id desc'), 'limit' =>$this->paginate_limit);
	 //$result = $this->User->find('all',array('conditions'=>array('User.sp_id'=>$agent_id,'User.user_type_id'=>3),'order'=>'User.id DESC'));
	 $data = $this->paginate('User');	 
	 $this->set('result',$data);	
	 
	 
	 $tstamp=time();
	 $this->set('tstamp',$tstamp);
	 
      }
/*
*************************************************************************
*Function Name		 :	changepasswd
*Functionality		 :	use to change password of Agent section.
*************************************************************************
*/ 
	function sp_changepasswd(){
		$this->loadModel('User');
		$this->layout='sp';
		$sp = $this->Session->read('Log');
		$pass = $sp['User']['password'];
		if(isset($this->data) && !empty($this->data)){     		
			$this->User->set($this->data);					  
			$data['User']['id'] =  $this->data['User']['id'];
			$oldpass=md5($this->data['User']['oldpassword']);
			if($oldpass==$pass){
			$this->data['User']['password'];
			$data['User']['password'] = md5($this->data['User']['password']);
			if($this->User->save($data['User'],array('validate'=>false))){
				 $result = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['User']['id'])));
				 $this->Session->write('Log', $result);
				 $this->Session->setFlash('Password has been changed successfully');
				 $this->redirect(array('controller'=>'sps','action'=>'sp_changepasswd'));
			}
			}else{
				 $this->Session->setFlash('Please enter correct old password');
				 $this->redirect(array('controller'=>'sps','action'=>'sp_changepasswd'));
		 }
		}
	}
/*
*************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of Agent section.
*************************************************************************
*/
   function sp_editinfo($id = null)
   {
	$this->loadModel('UserCompanyPrincipalRecord');
	$this->loadModel('UserCompanyPrincipal');
	 if(isset($this->data) && !empty($this->data))
	 {
		$this->data['User']['phone']=$this->data['User']['phone1'].'-'.$this->data['User']['phone2'].'-'.$this->data['User']['phone3'];
		if(!empty($this->data['User']['newprofilepic']))
		{
			if(file_exists(WWW_ROOT.USERPROFILEPIC.$this->data['User']['oldprofilepic'])){
			unlink(WWW_ROOT.USERPROFILEPIC.$this->data['User']['oldprofilepic']); 
		}
			$this->data['User']['profilepic'] = $this->data['User']['newprofilepic'];
		}
		else {
			$this->data['User']['profilepic'] = $this->data['User']['oldprofilepic'];
		}
		if($this->User->save($this->data['User'],false))
		{
		       if(isset($this->data['Company']['company_logo_new']) && !empty($this->data['Company']['company_logo_new'])){
		       
			       $src_img = WWW_ROOT."img/company_logo_temp/".$this->data['Company']['company_logo_new'];
			       $des_img = WWW_ROOT."img/company_logo/".$this->data['Company']['company_logo_new'];
			       $rs_img =  rename($src_img,$des_img);
			       if($rs_img){
				   $this->data['Company']['company_logo'] = $this->data['Company']['company_logo_new'];
			       }
			       }
			       else{
					  $this->data['Company']['company_logo'] = $this->data['Company']['company_logo_old'];
			       }
			       if($this->Company->save($this->data['Company'],false)){
					$data['UserCompanyPrincipalRecord']['company_id'] = $this->data['Company']['id'];
					foreach($this->data['UserCompanyPrincipalRecord'] as $key=>$val){
  					$data['UserCompanyPrincipalRecord']['user_company_principal_id']=$val['user_company_principal_id']; 
  					$data['UserCompanyPrincipalRecord']['manager_name'] =  $val['manager_name'];
  					$data['UserCompanyPrincipalRecord']['manager_email'] =  $val['manager_email'];
  					$data['UserCompanyPrincipalRecord']['new_client'] =  $val['new_client'];
					$data['UserCompanyPrincipalRecord']['previous_report'] =  $val['previous_report'];
					$data['UserCompanyPrincipalRecord']['new_scheduled'] =  $val['new_scheduled'];
  					$this->UserCompanyPrincipalRecord->create();
  					$this->UserCompanyPrincipalRecord->save($data['UserCompanyPrincipalRecord'],false);
					}
				
				       $result = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['User']['id'])));
				       $this->Session->write('Log',$result);
				       $this->Session->setFlash(__('Information has been updated successfully',true));
				       $this->redirect(array('controller'=>'sps','action'=>'sp_dashboard'));
			       }
		}
	 }else{
			$id = base64_decode($id); //
			$this->data = $this->User->find('first',array('conditions'=>array('User.id'=>$id),'recursive'=>2));
			$this->data['Company'] = $this->data['Company'];
			$this->set('country',$this->getCountries());
			$cmpPrincipal = $this->UserCompanyPrincipal->find('all');
			$this->set('cmpPrincipal',$cmpPrincipal);
		 if(!empty($this->data['User']['country_id'])){
			$this->set('state',$this->getState($this->data['User']['country_id']));	
		 }
	 }
   }
/*
*************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of Agent section.
*************************************************************************
*/
    function sp_addclients()
    {
        $this->loadModel('CodeType');
	$this->loadModel('SubscriptionDuration');
  	$sp = $this->Session->read('Log');
  	$this->set('country',$this->getCountries());
  	$this->set('state',$this->getState(233));
  	$sp_id = $sp['User']['id'];
  	$this->set('sp_id',$sp_id);
	# To check the max limit of subscription plan
	if($sp['User']['subscription_under_trial'] == 'No'){
		
		/*$clientcount=$this->User->find('count',array('conditions'=>array('User.sp_id'=>$sp['User']['id'])));	
		$plantype=$this->SubscriptionDuration->find('first',array('conditions'=>array('id'=>$sp['User']['subscription_duration_id']),'fields'=>array('max_allowed_clients')));
		if($clientcount > $plantype['SubscriptionDuration']['max_allowed_clients']){
				$this->Session->setFlash(__('You are not authorised to add more client as maximum client limit of your subscption plan is over',true));
				$this->redirect(array('controller'=>'sps','action'=>'clientlisting'));		
		}*/
	}
	
  	$codetype = $this->CodeType->find('all');	
        $this->set('codetype',$codetype); 
  	 if(isset($this->data) && !empty($this->data))
  	 {
  		//pr($this->data); die; 
  	     $this->User->set($this->data['User']);	    
  	    if($this->User->validates())
  	    {
  		$password = substr(md5(time()), 0, 6); // to generate a password of 6 characters
  		$encpt_newpassword = md5($password);		
  		$this->data['User']['password'] = $encpt_newpassword;
  		$this->data['User']['username'] = $this->data['User']['email'];
  		$this->data['User']['user_type_id'] = 3;
  		$this->data['User']['phone']=$this->data['User']['phone1'].'-'.$this->data['User']['phone2'].'-'.$this->data['User']['phone3'];
		$this->data['User']['billto_phone']=$this->data['User']['billto_phone1'].'-'.$this->data['User']['billto_phone2'].'-'.$this->data['User']['billto_phone3'];
  		$this->data['User']['site_phone']=$this->data['User']['site_phone1'].'-'.$this->data['User']['site_phone2'].'-'.$this->data['User']['site_phone3'];
  		//$this->data['User']['responsible_contact']=$this->data['User']['availphone1'].'-'.$this->data['User']['availphone2'].'-'.$this->data['User']['availphone3'];
  		$this->data['User']['deactivate'] = 1;		
  		$this->data['User']['sp_id'] = $sp_id;
		
		# code to save code types
		$codetype = array();
		foreach($this->data['User']['code_type_id'] as $codeKey=>$codeValue){
		 if($codeValue != "0"){
			       $codetype[] = $codeValue;
		   }
		}
		$this->data['User']['code_type_id'] = implode(",",$codetype);
		
  		if($this->User->save($this->data['User'],false)){
			$clientId=$this->User->id;
			
			$this->data['SpSiteAddress']['site_phone']=$this->data['SpSiteAddress']['site_phone1'].'-'.$this->data['SpSiteAddress']['site_phone2'].'-'.$this->data['SpSiteAddress']['site_phone3'];
			$this->data['SpSiteAddress']['responsible_contact']=$this->data['SpSiteAddress']['availphone1'].'-'.$this->data['SpSiteAddress']['availphone2'].'-'.$this->data['SpSiteAddress']['availphone3'];
			$this->data['SpSiteAddress']['client_id']=$clientId;
			$this->data['SpSiteAddress']['sp_id'] = $sp_id;
			$this->loadModel('SpSiteAddress');
			$this->SpSiteAddress->save($this->data['SpSiteAddress']);
			
  			$this->loadmodel('EmailTemplate');
  			$register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>4)));
  			$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a><br><br><b>Username : </b> ".$this->data['User']['email']."<br><b>Password : </b> ".$password."<br>"; 
  			$register_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#USERNAME','#PASSWORD','#CLICK_HERE'),array($this->selfURL() ,$this->data['User']['fname'],$this->data['User']['email'],$password,@$link),$register_temp['EmailTemplate']['mail_body']);
  			$this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);
/* SMTP Options */
$this->Email->smtpOptions = array(
     'port'=>'587',
     'timeout'=>'30',
     'host' => 'smtp.mandrillapp.com',
     'username'=>'admin@reportsonlineplus.com',
     'password'=>'ZfppYXZd9HHZAfLFnCPDZg',
);
		
			/* Set delivery method */ 
			$this->Email->delivery = 'smtp';
  			$this->Email->to = $this->data['User']['email'];
  			$this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
  			$this->Email->from = EMAIL_FROM;
  			$this->Email->template = 'register_mail'; // note no '.ctp'
  			$this->Email->sendAs = 'both'; // because we like to send pretty mail			
  			$this->Email->send();//Do not pass any args to send()
  			//$this->Session->setFlash(__('New Client has been added successfully',true));
  			//$this->redirect(array('controller'=>'sps','action'=>'clientlisting'));
			$this->redirect(array('controller'=>'sps','action'=>'clientadded',base64_encode($clientId)));
  		}
	     }
	    }
    }
/*
*************************************************************************
*Function Name		 :	Createservicecall
*Functionality		 :	use to create the services for the client.
*************************************************************************
*/
    function sp_createservicecall($cid = null){
	$cid = base64_decode($cid);
	$result = $this->User->find('first',array('conditions'=>array('User.id'=>$cid),'fields'=>array('fname','lname','client_company_name')));
	$this->set('name',$result['User']['fname']." ".$result['User']['lname']);
	$this->set('cname',$result['User']['client_company_name']);
	$this->loadModel('Report');
	$this->set('frequency',Configure::read('frequency'));
	$sp = $this->Session->read('Log');
	$sp_id = $sp['User']['id'];
	$this->set('sp_id',$sp_id);
	
	
	#01-10-2012 CODE to fetch the site addresses for selection
	//$this->set('siteAddresses',$this->getSiteaddress($sp_id));
	$this->set('siteAddresses',$this->getClientSiteaddress($cid));		
	
	$this->set('cid',$cid);
	$reportData = $this->getAllReports($sp_id);
	$this->set('rData',$reportData);
	$this->loadModel('User');
	$this->loadModel('Service');
	$this->loadModel('Schedule');
	$this->loadModel('ServiceCall');
	$this->loadModel('ScheduleService');
	$this->loadmodel('EmailTemplate');
	$this->loadmodel('Report');
	$this->loadmodel('ReportCert');
	
		#CODE to find all clients assigned in assigned table
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.client_id'=>$cid),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("Servicedata",$res);
		$this->set('frequency',Configure::read('frequency'));
	 if(isset($this->data) && !empty($this->data)){//echo '<pre>';print_r($this->data);die;		
		
		/*ByPass Customer Acceptance Call*/
		if($this->data['ServiceCall']['bypass']=='Yes'){
			$this->data['ServiceCall']['call_status']='1';
		}
		/*ByPass Customer Acceptance Call*/
		
		$this->ServiceCall->save($this->data['ServiceCall'],false);
		$serviceCallID = $this->ServiceCall->getLastInsertId();
		for($i=1;$i<=6;$i++){
			if($this->data[$i]['Schedule']['report_id'] != "0")
			{
			$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data[$i]['Schedule']['lead_inspector']),'fields'=>array('fname','lname','email')));
			$leader_name = $leader['User']['fname']." ".$leader['User']['lname'];
			$client = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['ServiceCall']['client_id']),'fields'=>array('fname','lname','site_name','email')));
			$client_name = $client['User']['fname']." ".$client['User']['lname'];
			$client_email = $client['User']['email'];
			$report = $this->Report->find('first',array('conditions'=>array('Report.id'=>$this->data[$i]['Schedule']['report_id']),'fields'=>array('name')));
			$report_name = $report['Report']['name'];			
			
			$data[$i]['Schedule']['sch_freq']=$this->data[$i]['Schedule']['sch_freq'];
			$data[$i]['Schedule']['service_call_id']=$serviceCallID;
			$data[$i]['Schedule']['schedule_date_1']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_1']));
			$data[$i]['Schedule']['schedule_from_1']=$this->data[$i]['Schedule']['schedule_from_1'];
			$data[$i]['Schedule']['schedule_to_1']=$this->data[$i]['Schedule']['schedule_to_1'];
			$data[$i]['Schedule']['submission_date']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['submission_date']));
			if(isset($this->data[$i]['Schedule']['schedule_date_2']) && isset($this->data[$i]['Schedule']['schedule_from_2']) && isset($this->data[$i]['Schedule']['schedule_to_2']))
			{
			  $date2= $this->data[$i]['Schedule']['schedule_date_2'];
			  $from2= $this->data[$i]['Schedule']['schedule_from_2'];
			  $to2= $this->data[$i]['Schedule']['schedule_to_2'];
				$data[$i]['Schedule']['schedule_date_2']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_2']));
				$data[$i]['Schedule']['schedule_from_2']=$this->data[$i]['Schedule']['schedule_from_2'];
				$data[$i]['Schedule']['schedule_to_2']=$this->data[$i]['Schedule']['schedule_to_2'];
			}else{
				$date2= '';
			    $from2= '';
			    $to2= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_3']) && isset($this->data[$i]['Schedule']['schedule_from_3']) && isset($this->data[$i]['Schedule']['schedule_to_3']))
			{
			  $date3= $this->data[$i]['Schedule']['schedule_date_3'];
			  $from3= $this->data[$i]['Schedule']['schedule_from_3'];
			  $to3= $this->data[$i]['Schedule']['schedule_to_3'];
				$data[$i]['Schedule']['schedule_date_3']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_3']));
				$data[$i]['Schedule']['schedule_from_3']=$this->data[$i]['Schedule']['schedule_from_3'];
				$data[$i]['Schedule']['schedule_to_3']=$this->data[$i]['Schedule']['schedule_to_3'];
			}else{
				$date3= '';
			    $from3= '';
			    $to3= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_4']) && isset($this->data[$i]['Schedule']['schedule_from_4']) && isset($this->data[$i]['Schedule']['schedule_to_4']))
			{
			  $date4= $this->data[$i]['Schedule']['schedule_date_4'];
			  $from4= $this->data[$i]['Schedule']['schedule_from_4'];
			  $to4= $this->data[$i]['Schedule']['schedule_to_4']; 
				$data[$i]['Schedule']['schedule_date_4']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_4']));
				$data[$i]['Schedule']['schedule_from_4']=$this->data[$i]['Schedule']['schedule_from_4'];
				$data[$i]['Schedule']['schedule_to_4']=$this->data[$i]['Schedule']['schedule_to_4'];
			}
			else{
				$date4= '';
			    $from4= '';
			    $to4= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_5']) && isset($this->data[$i]['Schedule']['schedule_from_5']) && isset($this->data[$i]['Schedule']['schedule_to_5']))
			{   
			  $date5= $this->data[$i]['Schedule']['schedule_date_5'];
			  $from5= $this->data[$i]['Schedule']['schedule_from_5'];
			  $to5= $this->data[$i]['Schedule']['schedule_to_5'];
				$data[$i]['Schedule']['schedule_date_5']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_5']));
				$data[$i]['Schedule']['schedule_from_5']=$this->data[$i]['Schedule']['schedule_from_5'];
				$data[$i]['Schedule']['schedule_to_5']=$this->data[$i]['Schedule']['schedule_to_5'];
			}else{
				$date5= '';
			    $from5= '';
			    $to5= '';
      
			}
			# Added 5 new schedule date, schedule_from and schedule date_to ON 26 JULY 2012(ADDED BY NAVDEEP)
			if(isset($this->data[$i]['Schedule']['schedule_date_6']) && isset($this->data[$i]['Schedule']['schedule_from_6']) && isset($this->data[$i]['Schedule']['schedule_to_6']))
			{   
			  $date6= $this->data[$i]['Schedule']['schedule_date_6'];
			  $from6= $this->data[$i]['Schedule']['schedule_from_6'];
			  $to6= $this->data[$i]['Schedule']['schedule_to_6'];
				$data[$i]['Schedule']['schedule_date_6']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_6']));
				$data[$i]['Schedule']['schedule_from_6']=$this->data[$i]['Schedule']['schedule_from_6'];
				$data[$i]['Schedule']['schedule_to_6']=$this->data[$i]['Schedule']['schedule_to_6'];
			}else{
				$date6= '';
			    $from6= '';
			    $to6= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_7']) && isset($this->data[$i]['Schedule']['schedule_from_7']) && isset($this->data[$i]['Schedule']['schedule_to_7']))
			{   
			  $date7= $this->data[$i]['Schedule']['schedule_date_7'];
			  $from7= $this->data[$i]['Schedule']['schedule_from_7'];
			  $to7= $this->data[$i]['Schedule']['schedule_to_7'];
				$data[$i]['Schedule']['schedule_date_7']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_7']));
				$data[$i]['Schedule']['schedule_from_7']=$this->data[$i]['Schedule']['schedule_from_7'];
				$data[$i]['Schedule']['schedule_to_7']=$this->data[$i]['Schedule']['schedule_to_7'];
			}else{
				$date7= '';
			    $from7= '';
			    $to7= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_8']) && isset($this->data[$i]['Schedule']['schedule_from_8']) && isset($this->data[$i]['Schedule']['schedule_to_8']))
			{   
			  $date8= $this->data[$i]['Schedule']['schedule_date_8'];
			  $from8= $this->data[$i]['Schedule']['schedule_from_8'];
			  $to8= $this->data[$i]['Schedule']['schedule_to_8'];
				$data[$i]['Schedule']['schedule_date_8']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_8']));
				$data[$i]['Schedule']['schedule_from_8']=$this->data[$i]['Schedule']['schedule_from_8'];
				$data[$i]['Schedule']['schedule_to_8']=$this->data[$i]['Schedule']['schedule_to_8'];
			}else{
				$date8= '';
			    $from8= '';
			    $to8= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_9']) && isset($this->data[$i]['Schedule']['schedule_from_9']) && isset($this->data[$i]['Schedule']['schedule_to_9']))
			{   
			  $date9= $this->data[$i]['Schedule']['schedule_date_9'];
			  $from9= $this->data[$i]['Schedule']['schedule_from_9'];
			  $to9= $this->data[$i]['Schedule']['schedule_to_9'];
				$data[$i]['Schedule']['schedule_date_9']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_9']));
				$data[$i]['Schedule']['schedule_from_9']=$this->data[$i]['Schedule']['schedule_from_9'];
				$data[$i]['Schedule']['schedule_to_9']=$this->data[$i]['Schedule']['schedule_to_9'];
			}else{
				$date9= '';
			    $from9= '';
			    $to9= '';
      
			}
			if(isset($this->data[$i]['Schedule']['schedule_date_10']) && isset($this->data[$i]['Schedule']['schedule_from_10']) && isset($this->data[$i]['Schedule']['schedule_to_10']))
			{   
			  $date10= $this->data[$i]['Schedule']['schedule_date_10'];
			  $from10= $this->data[$i]['Schedule']['schedule_from_10'];
			  $to10= $this->data[$i]['Schedule']['schedule_to_10'];
				$data[$i]['Schedule']['schedule_date_10']=date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_10']));
				$data[$i]['Schedule']['schedule_from_10']=$this->data[$i]['Schedule']['schedule_from_10'];
				$data[$i]['Schedule']['schedule_to_10']=$this->data[$i]['Schedule']['schedule_to_10'];
			}else{
				$date10= '';
			    $from10= '';
			    $to10= '';
      
			}
			
			$data[$i]['Schedule']['report_id']=$this->data[$i]['Schedule']['report_id'];			
			$data[$i]['Schedule']['lead_inspector_id']=$this->data[$i]['Schedule']['lead_inspector'];
			 
			if(!empty($this->data[$i]['Schedule']['helper_inspector'])){
				
			$helper_inspector = implode(',',$this->data[$i]['Schedule']['helper_inspector']);
			}
			else{
			$helper_inspector = '';	
			}
			$data[$i]['Schedule']['helper_inspector_id']=$helper_inspector;
			
			$this->Schedule->create();
			
			$this->Schedule->save($data[$i]['Schedule'],false);
			$scheduleId = $this->Schedule->getLastInsertId();
			for($j=1;$j<=count($this->data[$i]['Schedule']['ScheduleService']);$j++)
			{
			    if($this->data[$i]['Schedule']['ScheduleService'][$j]['service_id'] != "0")
			    {
				$datas['ScheduleService']['schedule_id'] = $scheduleId;
				$datas['ScheduleService']['service_id']=$this->data[$i]['Schedule']['ScheduleService'][$j]['service_id'];
				$datas['ScheduleService']['amount']=$this->data[$i]['Schedule']['ScheduleService'][$j]['amount'];
				$datas['ScheduleService']['frequency']=$this->data[$i]['Schedule']['ScheduleService'][$j]['frequency'];
				$this->ScheduleService->create();
				$this->ScheduleService->save($datas['ScheduleService'],false);				
			   }
			}
			
			/*Save the reports name to be attached code starts here*/
			if(isset($this->data[$i]['ReportCert'])){
				for($cr=1;$cr<=count($this->data[$i]['ReportCert']);$cr++)
				{
				    if($this->data[$i]['ReportCert'][$cr]['cert_id'] != "0")
				    {
					$certdatas['ReportCert']['schedule_id'] = $scheduleId;
					$certdatas['ReportCert']['cert_id']=$this->data[$i]['ReportCert'][$cr]['cert_id'];				
					$this->ReportCert->create();
					$this->ReportCert->save($certdatas['ReportCert'],false);				
				   }
				}
			}
			/*Save the reports name to be attached code ends here*/
			
					 
		   }
		} //end forloop
		
		if($this->data['ServiceCall']['bypass']=='Yes'){			
			$this->ServiceCall->updateAll(array('ServiceCall.bypass_mail'=>'"1"'),array('ServiceCall.id'=>$serviceCallID));			
			//App::import('Controller', 'Clients');
			// We need to load the class
			//$Clients = new ClientsController;
			// If we want the model associations, components, etc to be loaded
			//$Clients->sendApproveMail($serviceCallID);
			$this->sendApproveMail($serviceCallID);
		}else{
		
			#code to send email to client to give new service call info(ADDED BY NAVDEEP ON 26 JULy2012)
			//$serviceCallID	 
					 
			$client_name; //cleintname
			$sp = $this->Session->read('Log');
			$spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
			$spschedularphone = $sp['User']['phone']; //sp-schedulerphone
			$spcompany=$sp['Company']['name'];
			$this->Schedule->bindModel(
				array('belongsTo' => array(
					'Report' => array(
						'className' => 'Report',
						'fields'=>array('Report.name')
						)
					)
				)
			);
			//$this->Schedule->bindModel('belongsTo'=>'Report');
			$Schdata=$this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$serviceCallID)));
			//pr($Schdata);
			//echo "===============>";
		       
			$email_string ='';
			$email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
						<tbody>
						<tr style="background-color:#0B83A6">
						    <td><b>ReportName</b></td>
						    <td><b>Days</b></td>
						    <td><b>Schedule Date/Time</b></td>
						    <td><b>Inspector Assigned</b></td>
						</tr>';
			$reportdata='';
			foreach($Schdata as $key=>$sch){
				$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['Schedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
				$reportdata   .=$sch['Report']['name'].',';
				$email_string .= "<tr>";
				$days=0;
				$email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';       
				$sch1= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_1'])); $days=$days+1;
				 $sch2="";$sch3="";$sch4="";$sch5="";$sch6="";$sch7="";$sch8="";$sch9="";$sch10="";
				 if(!empty($sch['Schedule']['schedule_date_2'])) { 
					 $sch2= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_2'])); $days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_3'])) { 
					 $sch3= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_3']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_4'])) { 
					 $sch4= date('m/d/Y',strtotime($sch['schedule_date_4'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_4']));$days=$days+1;
				 }
				 if(!empty($schedule['schedule_date_5'])) { 
					 $sch5= date('m/d/Y',strtotime($sch['schedule_date_5'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['schedule_to_5']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_6'])) { 
					 $sch6= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_6']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_7'])) {
					 $sch7= date('m/d/Y',strtotime($sch['schedule_date_7'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_7']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_8'])) {
					 $sch8= date('m/d/Y',strtotime($sch['schedule_date_8'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_8']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_9'])) { 
					 $sch9= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_9']));$days=$days+1;
				 }
				 if(!empty($sch['Schedule']['schedule_date_10'])) { 
					 $sch10= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_10']));$days=$days+1;
				 }
				 
				$email_string .='<td valign="top">'.$days.'</td>';		    
				$email_string .='<td valign="top">'.$sch1.'<br/>'.$sch2.'<br/>'.$sch3.'<br/>'.$sch4.'<br/>'.$sch5.'<br/>'.$sch6.'<br/>'.$sch7.'<br/>'.$sch8.'<br/>'.$sch9.'<br/>'.$sch10.'</td>';
				$email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
				$email_string .= "</tr>";
			}
			       
			$email_string .= '</tbody></table>';				
			$servicecall_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>6)));
			$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
			$servicecall_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENTNAME','#SPNAME','#SPCOMPANY','#DETAIL','#SCHEDULEPHONE','#REPORTS','#LINK'),array($this->selfURL(),$client_name,$spname,$spcompany,$email_string,$spschedularphone,$reportdata,$link),$servicecall_temp['EmailTemplate']['mail_body']);
			$this->set('servicecall_temp',$servicecall_temp['EmailTemplate']['mail_body']);
			$this->Email->to = $client_email;
			$this->Email->subject = $servicecall_temp['EmailTemplate']['mail_subject'];
			$this->Email->from = EMAIL_FROM;
			$this->Email->template = 'service_call_temp'; // note no '.ctp'
			$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
			$this->Email->send();//Do not pass any args to send() 
				
		}		
		$this->redirect(array('controller'=>'sps','action'=>'serviceCallThanks',base64_encode($cid)));
	 }
    }   
    
    
    /*
    ****************************************************************************
    *Function Name		 :	EditInfo
    *Functionality		 :	use to Edit information of Agent section.
    ****************************************************************************
    */
    function sp_serviceCallThanks($id = null){
	$cid = base64_decode($id);
	$result = $this->User->find('first',array('conditions'=>array('User.id'=>$cid),'fields'=>array('fname','lname','client_company_name')));
	$this->set('name',$result['User']['fname']." ".$result['User']['lname']);
	$this->set('cname',$result['User']['client_company_name']);
	$this->set('cid',$cid);
	$sp = $this->Session->read('Log');
	$sp_id = $sp['User']['id'];
	$this->set('sp_id',$sp_id);
    }
/*
****************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of Agent section.
****************************************************************************
*/
     function sp_editclient($cid = null)
     {	
         $this->loadModel('CodeType');
	 if(isset($this->data) && !empty($this->data))
	 {
		
		$this->User->set($this->data['User']);	    
		if($this->User->validates())
		{		    
		    $this->data['User']['phone']=$this->data['User']['phone1'].'-'.$this->data['User']['phone2'].'-'.$this->data['User']['phone3'];
		    $this->data['User']['billto_phone']=$this->data['User']['billto_phone1'].'-'.$this->data['User']['billto_phone2'].'-'.$this->data['User']['billto_phone3'];		    
		    
		    if(!empty($this->data['User']['newprofilepic'])){
			if(file_exists(WWW_ROOT.USERPROFILEPIC.$this->data['User']['oldprofilepic'])){
				unlink(WWW_ROOT.USERPROFILEPIC.$this->data['User']['oldprofilepic']); 
			}
			$this->data['User']['profilepic'] = $this->data['User']['newprofilepic'];
		    }else{
			$this->data['User']['profilepic'] = $this->data['User']['oldprofilepic'];
		    }
		    
		    # code to save code types
		    $codetype = array();
		    foreach($this->data['User']['code_type_id'] as $codeKey=>$codeValue){
		     if($codeValue != "0"){
				   $codetype[] = $codeValue;
		       }
		    }
		    $this->data['User']['code_type_id'] = implode(",",$codetype);
		    
		    if($this->User->save($this->data['User'],false)){			    
			    /*$this->data['SpSiteAddress']['site_phone']=$this->data['SpSiteAddress']['site_phone1'].'-'.$this->data['SpSiteAddress']['site_phone2'].'-'.$this->data['SpSiteAddress']['site_phone3'];
			    $this->data['SpSiteAddress']['responsible_contact']=$this->data['SpSiteAddress']['availphone1'].'-'.$this->data['SpSiteAddress']['availphone2'].'-'.$this->data['SpSiteAddress']['availphone3'];			    
			    $this->loadModel('SpSiteAddress');
			    $this->SpSiteAddress->save($this->data['SpSiteAddress']);*/
			    $this->Session->setFlash(__('Client infomation has been updated successfully',true));
			    $this->redirect(array('controller'=>'sps','action'=>'clientlisting'));
		    }
		 }		
	 } else {
		$cid = base64_decode($cid);
		$this->User->id = $cid;
		$this->data = $this->User->read();//pr($this->data);die;
		$this->set('country',$this->getCountries());
		 if(!empty($this->data['User']['country_id']))
		 {
			$this->set('state',$this->getState($this->data['User']['country_id']));	
		 }
		 if(!empty($this->data['User']['state_id']))
		 {
			$this->set('city',$this->getCity($this->data['User']['state_id']));	
		 }
		 if(!empty($this->data['User']['billto_country_id']))
		 {
			$this->set('state1',$this->getState($this->data['User']['billto_country_id']));	
		 }
		 if(!empty($this->data['User']['billto_state_id']))
		 {
			$this->set('city1',$this->getCity($this->data['User']['billto_state_id']));	
		 }
		 
		 /*if(!empty($this->data['SpSiteAddress'][0]['country_id']))
		 {
			$this->set('state2',$this->getState($this->data['SpSiteAddress'][0]['country_id']));	
		 }
		 if(!empty($this->data['SpSiteAddress'][0]['state_id']))
		 {
			$this->set('city2',$this->getCity($this->data['SpSiteAddress'][0]['state_id']));	
		 }*/
		 $codetype=$this->CodeType->find('all');	
                 $this->set('codetype',$codetype);
		
	 }
     }
     
/*
*************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of Agent section.
*************************************************************************
*/
      function sp_credentialmail($id = null){
		$id = base64_decode($id);
		$ddata = $this->User->find("first", array("conditions" => array("User.id" => $id)));
		$this->User->id = $id;		
		$result = $this->User->read();
		$password = substr(md5(time()), 0, 6); // to generate a password of 6 characters
		$encpt_newpassword = md5($password);		
		$data['User']['password']= $encpt_newpassword;
	  if($this->User->save($data['User'],false)){	
		$this->loadmodel('EmailTemplate');
		$register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>2)));
		$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
		$register_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#USERNAME','#PASSWORD','#CLICK_HERE'),array($this->selfURL() ,$result['User']['fname'],$result['User']['email'],$password,@$link),$register_temp['EmailTemplate']['mail_body']);
		$this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);
		/* SMTP Options */
$this->Email->smtpOptions = array(
     'port'=>'587',
     'timeout'=>'30',
     'host' => 'smtp.mandrillapp.com',
     'username'=>'admin@reportsonlineplus.com',
     'password'=>'ZfppYXZd9HHZAfLFnCPDZg',
);
		
			/* Set delivery method */ 
			$this->Email->delivery = 'smtp';
		$this->Email->to = $ddata['User']['email'];
		$this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'register_mail'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
		if($this->Email->send()){//Do not pass any args to send()
		$this->Session->setFlash(__('New credentials of inspector has been sent successfully',true));
		$this->redirect(array("controller" => "sps", "action" => "inspectorlisting"));
	}else{ $this->Session->setFlash(__('Server error',true));
		$this->redirect(array("controller" => "sps", "action" => "inspectorlisting")); }
		
	}
	}
	
	
	
	function sp_credentialmailclient($id = null){
		$id = base64_decode($id);
		$ddata = $this->User->find("first", array("conditions" => array("User.id" => $id)));
		$this->User->id = $id;		
		$result = $this->User->read();
		$password = substr(md5(time()), 0, 6); // to generate a password of 6 characters
		$encpt_newpassword = md5($password);		
		$data['User']['password']= $encpt_newpassword;
	  if($this->User->save($data['User'],false)){	
		$this->loadmodel('EmailTemplate');
		$register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>4)));
		$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
		$register_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#USERNAME','#PASSWORD','#CLICK_HERE'),array($this->selfURL() ,$result['User']['fname'],$result['User']['email'],$password,@$link),$register_temp['EmailTemplate']['mail_body']);
		$this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);
/* SMTP Options */
$this->Email->smtpOptions = array(
     'port'=>'587',
     'timeout'=>'30',
     'host' => 'smtp.mandrillapp.com',
     'username'=>'admin@reportsonlineplus.com',
     'password'=>'ZfppYXZd9HHZAfLFnCPDZg',
);
		
			/* Set delivery method */ 
			$this->Email->delivery = 'smtp';
		
		$this->Email->to = $ddata['User']['email'];
		$this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'register_mail'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
		
		$this->Email->send();//Do not pass any args to send()
		$this->Session->setFlash(__('New credentials of client has been sent successfully',true));
		$this->redirect(array("controller" => "sps", "action" => "clientlisting"));
		
	}
	}


/*
***************************************************************************
*Function Name		 :	sp_serviceslisting
*Functionality		 :	use to Edit information of Agent section.
***************************************************************************
*/
  function sp_serviceslisting(){
    $this->loadModel('Service');
	$this->loadModel('Report');
    $sp = $this->Session->read('Log');
    $spId = $sp['User']['id']; 
    if(isset($_POST['active'])){
	$this->sp_serviceactive();
    }
    if(isset($_POST['inactive'])){
  	$this->sp_serviceinactive();
    }
	$result = $this->Report->find('all',array('conditions'=>array('Report.sp_id'=>$spId),'order'=>'Report.name ASC'));
    $this->set('result',$result);
   $this->set('data',$this->Service->find('all'));
    // pr($result);
    // die();
  }
/***************************************************************************************
 #function to activate 
****************************************************************************************/
    function sp_serviceactive(){
      for($i=0; $i<count($_POST['box']); $i++){    		
    		$this->Service->id = $_POST['box'][$i];    		
		    $data['Service']['status'] = 1;		    
		    $this->Service->save($data,array('validate'=>false));        	
    	}
     		$this->Session->setFlash('Service has been activated successfully');	
	   	    $this->redirect(array("controller" => "sps", "action" => "serviceslisting"));
    }
    
/***************************************************************************************
 #function to Inactivate 
****************************************************************************************/ 
    		
    function sp_serviceinactive(){
      
      for($i=0; $i<count($_POST['box']); $i++){    		
    	   $this->Service->id = $_POST['box'][$i];
		     $data['Service']['status'] = 0;
		     $this->Service->save($data,array('validate'=>false));        	
    	}
    		 $this->Session->setFlash('Service has been inactivated successfully');	
	       $this->redirect(array("controller" => "sps", "action" => "serviceslisting"));
    }
 
/*
***************************************************************************
*Function Name		 :	sp_addService
*Functionality		 :	use to Edit information of Agent section.
***************************************************************************
*/
  function sp_addService(){
    $this->loadModel('Service');
	$sp = $this->Session->read('Log');
    $spId = $sp['User']['id'];
	$this->set('reports',$this->getAllReportsListing($spId));
    if(isset($this->data) && !empty($this->data)){
      
    $this->data['User']['user_id'] = $spId;  
	  $this->Service->set($this->data['User']);	    
	    if($this->Service->validates()){      
	      $this->Service->save($this->data);
	      $this->Session->setFlash('New service has been saved successfully.');
        $this->redirect(array('controller'=>'sps','action'=>'serviceslisting'));
	    }
	  }

  }
/***************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of Agent section.
***************************************************************************
*/
  function sp_editService($id=null){
    $this->loadModel('Service');
	$sp = $this->Session->read('Log');
    $spId = $sp['User']['id'];
	$this->set('reports',$this->getAllReportsListing($spId));
    if(isset($this->data) && !empty($this->data)){
	  $this->Service->set($this->data['User']);
	    
	    if($this->Service->validates()){
	      $this->Service->save($this->data);
	      $this->Session->setFlash('Service has been updated successfully.');
        $this->redirect(array('controller'=>'sps','action'=>'serviceslisting'));
	    }
	  }else {
			$this->Service->id = base64_decode($id); //
			$this->data = $this->Service->read();
			
	 }

  }
/*
*************************************************************************
*Function Name		 :	inspectorlisting
*Functionality		 :	use to list information of Sp section.
*************************************************************************
*/
  function sp_inspectorlisting(){
    $sp = $this->Session->read('Log');
    $this->loadModel('Report');
    $spId = $sp['User']['id'];    
    $data = $this->Report->find('all',array('fields'=>array('id','name')));
    $this->set('rptdata',$data);
    
    $conditions = array();
	 $conditions[] = 'User.user_type_id=2 AND User.sp_id='.$spId;

	 if($this->data['User']['fname']!='' || $this->data['User']['lname']!='' || $this->data['User']['qualification']!='' || $this->data['User']['email']!='' || $this->data['User']['check_1']!=''|| $this->data['User']['check_2']!='' || $this->data['User']['check_3']!='' || $this->data['User']['check_4']!='' || $this->data['User']['check_5']!=''){
				foreach ($this->data['User'] as $k=>$v){
					$url['User'.'.'.$k]=$v;
 				}
				$this->redirect($url, null, true);
		}
	 
	 
	 
	 if(!empty($this->passedArgs['User.fname'])){
			$fname = $this->passedArgs['User.fname'];
			$conditions[] = 'User.fname LIKE "%'.$fname.'%"';
		}
		if(!empty($this->passedArgs['User.lname'])){
			$lname = $this->passedArgs['User.lname'];
			$conditions[] = 'User.lname LIKE "%'.$lname.'%"';
		}
		if(!empty($this->passedArgs['User.qualification'])){
			$qualification = $this->passedArgs['User.qualification'];
			$conditions[] = 'User.qualification LIKE "%'.$qualification.'%"';
		}
		if(!empty($this->passedArgs['User.email'])){
			$email = $this->passedArgs['User.email'];
			$conditions[] = 'User.email LIKE "%'.$email.'%"';
		}
		if(!empty($this->passedArgs['User.check_5'])){
			$check_5 = $this->passedArgs['User.check_5'];
			$conditions[] = 'FIND_IN_SET('.$check_5.', User.report_id)';
		}
		if(!empty($this->passedArgs['User.check_4'])){
			$check_4 = $this->passedArgs['User.check_4'];
			$conditions[] = 'FIND_IN_SET('.$check_4.', User.report_id)';
		}
		if(!empty($this->passedArgs['User.check_3'])){
			$check_3 = $this->passedArgs['User.check_3'];
			$conditions[] = 'FIND_IN_SET('.$check_3.', User.report_id)';
		}
		if(!empty($this->passedArgs['User.check_2'])){
			$check_2 = $this->passedArgs['User.check_2'];
			$conditions[] = 'FIND_IN_SET('.$check_2.', User.report_id)';
		}
		if(!empty($this->passedArgs['User.check_1'])){
			$check_1 = $this->passedArgs['User.check_1'];
			$conditions[] = 'FIND_IN_SET('.$check_1.', User.report_id)';
		}
		
	 
	 
	 if(!empty($conditions)){  
				$conditionsstr =  implode(' AND ',$conditions);
				
		}else{
			$conditionsstr = 'User.user_type_id=2 AND User.sp_id='.$spId;
		}	
		$this->paginate = array('conditions' =>$conditionsstr,'order'=>array('User.id desc'), 'limit' =>$this->paginate_limit);
	
	 $result = $this->paginate('User');

     
    $this->set('result',$result);
    
   }
   
#MADE BY VISHAL FOR INSPECTOR DETAIL PAGE
	function sp_inspector_detail($inspector_id=null)
	{
		$inspector_id=base64_decode($inspector_id);
		$this->User->service = true;
		$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>2,'User.id'=>$inspector_id)));
		$this->set('result',$result);		
	}
	
#MADE BY MAMTA BAJAJ FOR CLIENT DETAIL PAGE
	function sp_client_detail($client_id=null)
	{
		$this->loadModel('User');
		$client_id=base64_decode($client_id);
		$this->User->service = true;
		
		$this->User->unbindModel(array('hasMany'=>array('SpSiteAddress')));
		$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>3,'User.id'=>$client_id)));		
		$this->set('result',$result);
		
		$this->loadModel('SpSiteAddress');
		$allAddr = $this->SpSiteAddress->find('count',array('conditions'=>array('SpSiteAddress.client_id'=>$client_id)));
		$this->set('allCount',$allAddr);
		
		$activeAddr = $this->SpSiteAddress->find('all',array('conditions'=>array('SpSiteAddress.client_id'=>$client_id,'SpSiteAddress.idle'=>0)));
		$this->set('activeAddr',$activeAddr);
		
		$idleAddr = $this->SpSiteAddress->find('all',array('conditions'=>array('SpSiteAddress.client_id'=>$client_id,'SpSiteAddress.idle'=>1)));
		$this->set('idleAddr',$idleAddr);
		//$this->loadModel('Service');
		//$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		//$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.client_id'=>$client_id),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		//$this->set("data",$res);
		//$this->set('frequency',Configure::read('frequency'));
	}
#MADE BY Vishal FOR CLIENT GOOGLE MAP PAGE

	function sp_client_mapdirection($client_id=null)
	{
		$this->layout = null;
		$client_id=base64_decode($client_id);
		$this->User->service = true;
		$result = $this->User->find('first',array('conditions'=>array('User.user_type_id'=>3,'User.id'=>$client_id)));
		$this->set('result',$result);			
		
	}
#Made by Manish Kumar for the address map of site address	
	function siteaddress_mapdirection($siteaddressid=null)
	{
		$this->loadModel('SpSiteAddress');
		$this->layout = null;
		$siteaddress_id=base64_decode($siteaddressid);
		$this->SpSiteAddress->service = true;
		$result = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$siteaddress_id)));
		$this->set('result',$result);			
		
	}	
/*
*************************************************************************
*Function Name		 :	sp_addInspector
*Functionality		 :	use to Add information of Inspector by SP.
*************************************************************************
*/
  function sp_addInspector(){
      //$this->loadModel('Service');
      $this->set('country',$this->getCountries());
      $sp = $this->Session->read('Log');
      $sp_id = $sp['User']['id'];
      $this->loadModel('Service');
      $this->loadModel('Report');
      $this->loadModel('User');
      $reportData = $this->getAllReports($sp_id);
      $this->set('rData',$reportData);
	  $permissionData = $this->getAllInspectorPermissions();
	  $this->set('permissions',$permissionData);
      $userData = $this->User->find('first', array(				        
				        'conditions' => array('User.id' => $sp_id,'User.deactivate' => 1),
				        'recursive' => 2
      ));				 
      $this->set('userData',$userData);
      
      if(isset($this->data) && !empty($this->data)){
        $this->User->set($this->data['User']);
        $this->Company->set($this->data['Company']);
        if($this->User->validates() && $this->Company->validates()){  
          $password = substr(md5(time()), 0, 6); // to generate a password of 6 characters
		      $this->data['User']['password'] = md5($password);	       
          $this->data['User']['username'] = $this->data['User']['email'];
	  $this->data['User']['phone']=$this->data['User']['site_phone1'].'-'.$this->data['User']['site_phone2'].'-'.$this->data['User']['site_phone3'];
          $this->data['User']['user_type_id'] = 2;
          $this->data['User']['deactivate'] = 1;          
          $this->data['User']['sp_id'] = $sp_id;	       
      	  $serv = array();
      	  $reports = array();
      	  $flag = false;
      	  # foreach to get report_id as $key
      	  foreach($this->data['User']['service_id'] as $key=>$service){
      	    # foreach to get service_id as $val
        	  foreach($this->data['User']['service_id'][$key] as $key1=>$val){        	  
              if($val != "0"){
                $serv[] = $key1;
                $flag  = true;
              }
            } 
            if($flag == true){
              $reports[] = $key;
              $flag = false;
            }
      	  } 	  
	 
          //if(empty($serv)){
            //$this->Session->setFlash(__('Please select atleast one service from below',true));            
			//$this->redirect(array('controller'=>'sps','action'=>'addInspector'));
          //}
		  
		  foreach($this->data['User']['check'] as $key=>$check):
			if($check != "0"){
              $report_ids[] = $key;              
              }
			endforeach;
		  foreach($this->data['User']['permissions'] as $perm_key=>$perm):
			if($perm != "0"){
              $permissions_ids[] = $perm_key;              
              }
			endforeach;
          $this->data['User']['report_id'] = implode(",",$report_ids);
          $this->data['User']['service_id'] = implode(",",$serv);
	  $this->data['User']['permissions'] = implode(",",$permissions_ids);  
	  $this->data['User']['calendar_color_code'] = "#".$this->data['User']['calendar_color_code'];   
          if($this->User->save($this->data['User'],false)){
             $this->data['Company']['user_id'] = $this->User->getLastInsertId();
            if($this->Company->save($this->data['Company'],false)){
              $this->loadmodel('EmailTemplate');
              $register_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>2)));
              $link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
              $register_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#FIRSTNAME','#USERNAME','#PASSWORD','#CLICK_HERE'),array($this->selfURL() ,$this->data['User']['fname'],$this->data['User']['email'],$password,@$link),$register_temp['EmailTemplate']['mail_body']);
              $this->set('register_temp',$register_temp['EmailTemplate']['mail_body']);
/* SMTP Options */
$this->Email->smtpOptions = array(
     'port'=>'587',
     'timeout'=>'30',
     'host' => 'smtp.mandrillapp.com',
     'username'=>'admin@reportsonlineplus.com',
     'password'=>'ZfppYXZd9HHZAfLFnCPDZg',
);
		
/* Set delivery method */ 
$this->Email->delivery = 'smtp';
              $this->Email->to = $this->data['User']['email'];
              $this->Email->subject = $register_temp['EmailTemplate']['mail_subject'];
              $this->Email->from = EMAIL_FROM;
              $this->Email->template = 'register_mail'; // note no '.ctp'
              $this->Email->sendAs = 'both'; // because we like to send pretty mail
              $this->Email->send();//Do not pass any args to send()
    /* Check for SMTP errors. */ 
	//$this->set('smtp_errors', $this->Email->smtpError);
              $this->Session->setFlash(__('New inspector has been added successfully',true));
              $this->redirect(array('controller'=>'sps','action'=>'inspectorlisting'));
            }
          }
        }
      }
    }
    
    /*
*************************************************************************
*Function Name		 :	sp_subscriptionPlan
*Functionality		 :	use to buy the subscription plan to add more site addresses by SP.
*************************************************************************

  function sp_subscriptionPlan(){
      //$this->loadModel('Service');     
      $sp = $this->Session->read('Log');
      $sp_id = $sp['User']['id'];      
      $this->loadModel('User');      
      $userData = $this->User->find('first', array(				        
				        'conditions' => array('User.id' => $sp_id,'User.deactivate' => 1),
				        'recursive' => 2
      ));				 
      $this->set('userData',$userData);
      
      if(isset($this->data) && !empty($this->data)){
	
        
      }
    }*/
    
    function confirmation($activationcode = null)
   {
	 if(!empty($activationcode))
	 {
		$this->loadModel('User');
		$result = $this->User->find("first",array('conditions'=>array('User.activationkey'=>$activationcode)));
		$this->data['User']['id'] = $result['User']['id'];
		$this->data['User']['deactivate'] = 1;
		if($this->User->save($this->data['User'],false))
		{
			$this->Session->setFlash('Your account has been activated successfully',true);
			$this->redirect(array('controller'=>'homes','action'=>'login'));
		}
	 }
   }
/*
*************************************************************************
*Function Name		 :	sp_editInspector()
*Functionality		 :	use to Edit information of Inspector by SP.
*************************************************************************
*/    
    function sp_editInspector($id=null) {
		$sp = $this->Session->read('Log');
		$spId = $sp['User']['id'];
		
		$this->loadModel('Service');
		$this->loadModel('Report');
		$this->loadModel('User');
			
      if(isset($this->data) && !empty($this->data)){
		 
        $this->User->set($this->data['User']);
       
		$this->Company->set($this->data['Company']);
		 
		
        if($this->User->validates(array('fieldList' => array('email', 'fname'))) && $this->Company->validates()){
			 
            if(!empty($this->data['User']['newprofilepic'])){
              if(file_exists(WWW_ROOT.USERPROFILEPIC.$this->data['User']['oldprofilepic'])){
                unlink(WWW_ROOT.USERPROFILEPIC.$this->data['User']['oldprofilepic']); 
              }
              $this->data['User']['profilepic'] = $this->data['User']['newprofilepic'];
            }else{
                $this->data['User']['profilepic'] = $this->data['User']['oldprofilepic'];
              }
          $serv = array();
          $reports = array();
          $flag = false;
		  
          
			foreach($this->data['User']['check'] as $key=>$check):
			if($check != "0"){
              $report_ids[] = $key;              
              }
			endforeach;
			foreach($this->data['User']['permissions'] as $perm_key=>$perm):
			if($perm != "0"){
              $permissions_ids[] = $perm_key;              
              }
			endforeach;
			
			
		$this->data['User']['service_id'] = "";
		$this->data['User']['report_id'] = "";
		$this->data['User']['permissions'] = "";
		$this->data['User']['report_id'] = implode(",",$report_ids);
		$this->data['User']['service_id'] = implode(",",$serv);
		$this->data['User']['permissions'] = implode(",",$permissions_ids);
		$this->data['User']['phone']=$this->data['User']['site_phone1'].'-'.$this->data['User']['site_phone2'].'-'.$this->data['User']['site_phone3'];
		$this->data['User']['calendar_color_code'] = "#".$this->data['User']['calendar_color_code'];
	    if($this->User->save($this->data['User'],false)){
              $this->data['Company']['user_id'] = $this->User->id;
              if($this->Company->save($this->data['Company'],false)){              
                $this->Session->setFlash(__('The inspector information has been edited successfully',true));
                $this->redirect(array('controller'=>'sps','action'=>'inspectorlisting'));
              }
            }
			
          }else{
			
			$reportData = $this->getAllReports($spId);
			$this->set('rData',$reportData);
			$permissionData = $this->getAllInspectorPermissions();
			$this->set('permissions',$permissionData);
			$cid = base64_decode($id);
			 
			$this->data = $this->User->find('first',array('conditions'=>array('User.id'=>$cid)));
		 
			$servid = explode(",",$this->data['User']['service_id']);
			$reportid = explode(",",$this->data['User']['report_id']);
			$permission_ids = explode(",",$this->data['User']['permissions']);
			$this->set('permission_ids',$permission_ids);
			$this->set('rptid',$reportid);
			$this->data['User']['service_id'] = "";
			$this->data['User']['report_id'] = "";
			 
			foreach($reportid as $key=>$val){
				foreach($servid as $key1=>$serv1)
				{
					$this->data['User']['service_id'][$val][$serv1] = $serv1;
				}
			}  
				
		  }
		  
		} else {
			 
			$reportData = $this->getAllReports($spId);
			$this->set('rData',$reportData);
			$permissionData = $this->getAllInspectorPermissions();
			$this->set('permissions',$permissionData);
			$cid = base64_decode($id);
			$this->User->id = $cid;
			$this->data = $this->User->read();
			$servid = explode(",",$this->data['User']['service_id']);
			$reportid = explode(",",$this->data['User']['report_id']);
			$permission_ids = explode(",",$this->data['User']['permissions']);
			$this->set('permission_ids',$permission_ids);
			$this->set('rptid',$reportid);
			$this->data['User']['service_id'] = "";
			$this->data['User']['report_id'] = "";
			//pr($reportid); die;
			foreach($reportid as $key=>$val){
			foreach($servid as $key1=>$serv1)
			{
			$this->data['User']['service_id'][$val][$serv1] = $serv1;
			}
			}
      }
    }
    
    
    
    
    
    
    
/*
*********************************************************************************
*Function Name		 :	sp_assignagent
*Functionality		 :	use to assign a agent to a specific client by SP.
*********************************************************************************
*/ 
 function sp_assignagent($client_id = null)
 {
	$this->set('cid',base64_decode($client_id));
	$inspectorlist = $this->User->find('list',array('conditions'=>array('User.user_type_id'=>2,'User.deactivate'=>1),'fields'=>array('fname'),'order'=>array('User.fname ASC')));
	$this->set('inspectorlist',$inspectorlist);
	 if(isset($this->data) && !empty($this->data)){
		if($this->AssignInspector->save($this->data['AssignInspector'])){
		   $this->Session->setFlash(__('Inspector has been assigned successfully to the client',true));
		   $this->redirect(array('controller'=>'sps','action'=>'clientlisting'));
		}
	 }
 }
 /*
*************************************************************************
*Function Name		 :	sp_delete
*Functionality		 :	use to delete  information of inspector .
*************************************************************************
*/
 
	function sp_deleteInspector($id=null){
	       $this->loadModel('AssignInspector');
	       $this->loadModel('Schedule');
	       $this->loadModel('Company');
	       $this->loadModel('User');
	       $this->User->delete($id);
	       
		$c_id=$this->Company->find('first',array('fields'=>'id','conditions'=>array('Company.user_id'=>$id)));   /*  delete inspector data for comapny      */
		$this->Company->delete($c_id['Company']['id']);
		
		$ai_id=$this->AssignInspector->find('first',array('fields'=>'id','conditions'=>array('AssignInspector.inspector_id'=>$id)));  /*  delete inspector data for assign inspector      */
		$this->AssignInspector->delete($ai_id['AssignInspector']['id']);
		
		 $sc_id=$this->Schedule->find('first',array('fields'=>'id','conditions'=>array('Schedule.inspector_id'=>$id)));      /*  delete inspector data for schedule      */
		
		 $this->Schedule->delete($sc_id['Schedule']['id']);
		
	      
		$this->Session->setFlash(__('Inspector has been delete successfully','true'));	
	       
			    
		    
		     $this->redirect(array('controller' => 'sps', 'action' => 'inspectorlisting'));
	     }
	
	
 /*
*************************************************************************
*Function Name		 :	addstatus
*Functionality		 :	to save client status by ajax  .
*************************************************************************
*/
	
	function addstatus($uid){
	$this->layout=null;
	$this->loadModel('ClientCheck');
	 if(isset($this->data) && !empty($this->data)){
		$this->ClientCheck->deleteAll(array('ClientCheck.client_id' => $uid));
		$this->data['ClientCheck']['id'] = null;
		$this->data['ClientCheck']['client_id']= $uid;
		if($this->ClientCheck->save($this->data['ClientCheck'],false)){
			echo "1";
			exit;
		}
		else{
			echo "0";
			exit;
		}
	}
}

 /*
*************************************************************************
*Function Name		 :	deleteservicecal
*Functionality		 :	to delete service cal for exsitng cals listing  .
*************************************************************************
*/	
	function deleteservicecal($id){
	$this->loadModel('ServiceCall');
	$this->loadModel('Deficiency');
	$this->loadModel('InspectorLog');
	$this->loadModel('SpecialhazardReport');
	$this->loadModel('SpecialhazardReportAttachment');
	$this->loadModel('SpecialhazardReportNotification');
	$this->loadModel('SprinklerReport');
	$this->loadModel('SprinklerReportAttachment');
	$this->loadModel('SprinklerReportNotification');
	$this->loadModel('KitchenhoodReport');
	$this->loadModel('KitchenhoodReportAttachment');
	$this->loadModel('KitchenhoodReportNotification');
	$this->loadModel('ExtinguisherReport');
	$this->loadModel('ExtinguisherReportAttachment');
	$this->loadModel('ExtinguisherReportNotification');
	$this->loadModel('EmergencyexitReport');
	$this->loadModel('EmergencyexitReportAttachment');
	$this->loadModel('EmergencyexitReportNotification');
	$this->loadModel('FirealarmReport');
	$this->loadModel('FirealarmReportAttachment');
	$this->loadModel('FirealarmReportNotification');
	$this->loadModel('Signature');
	$this->loadModel('FaAlarmDevicePlace');
	$data=$this->SprinklerReportAttachment->find('all',array('conditions'=>array('SprinklerReportAttachment.servicecall_id'=>$id),'recursive'=> -1));
	$datakitchen=$this->KitchenhoodReportAttachment->find('all',array('conditions'=>array('KitchenhoodReportAttachment.servicecall_id'=>$id),'recursive'=> -1));
	$datafirealarm=$this->FirealarmReportAttachment->find('all',array('conditions'=>array('FirealarmReportAttachment.servicecall_id'=>$id),'recursive'=> -1));
	$dataemergency=$this->EmergencyexitReportAttachment->find('all',array('conditions'=>array('EmergencyexitReportAttachment.servicecall_id'=>$id),'recursive'=> -1));
	$dataextinguisher=$this->ExtinguisherReportAttachment->find('all',array('conditions'=>array('ExtinguisherReportAttachment.servicecall_id'=>$id),'recursive'=> -1));
	$dataspecial=$this->SpecialhazardReportAttachment->find('all',array('conditions'=>array('SpecialhazardReportAttachment.servicecall_id'=>$id),'recursive'=> -1));	
	
	if($this->ServiceCall->delete($id)){
		$this->Deficiency->deleteAll(array('Deficiency.servicecall_id' => $id));
		$this->InspectorLog->deleteAll(array('InspectorLog.servicecall_id' => $id));
		$this->SpecialhazardReport->deleteAll(array('SpecialhazardReport.servicecall_id' => $id));
		
		$this->SpecialhazardReportNotification->deleteAll(array('SpecialhazardReportNotification.servicecall_id' => $id));
		$this->SprinklerReport->deleteAll(array('SprinklerReport.servicecall_id' => $id));
		foreach($data as $value){
			if(file_exists(WWW_ROOT."img/uploaded_attachments/".$value['SprinklerReportAttachment']['attach_file'])){			
			unlink(WWW_ROOT."img/uploaded_attachments/".$value['SprinklerReportAttachment']['attach_file']);
			$this->SprinklerReportAttachment->delete($value['SprinklerReportAttachment']['id']);
			}
			
		}
		foreach($datakitchen as $value){
			if(file_exists(WWW_ROOT."img/uploaded_attachments/".$value['KitchenhoodReportAttachment']['attach_file'])){			
			unlink(WWW_ROOT."img/uploaded_attachments/".$value['KitchenhoodReportAttachment']['attach_file']);
			$this->KitchenhoodReportAttachment->delete($value['KitchenhoodReportAttachment']['id']);
			}
			
		}
		foreach($datafirealarm as $value){
			if(file_exists(WWW_ROOT."img/uploaded_attachments/".$value['FirealarmReportAttachment']['attach_file'])){			
			unlink(WWW_ROOT."img/uploaded_attachments/".$value['FirealarmReportAttachment']['attach_file']);
			$this->FirealarmReportAttachment->delete($value['FirealarmReportAttachment']['id']);
			}
			
		}
		foreach($dataemergency as $value){
			if(file_exists(WWW_ROOT."img/uploaded_attachments/".$value['EmergencyexitReportAttachment']['attach_file'])){			
			unlink(WWW_ROOT."img/uploaded_attachments/".$value['EmergencyexitReportAttachment']['attach_file']);
			$this->EmergencyexitReportAttachment->delete($value['EmergencyexitReportAttachment']['id']);
			}
			
		}
		foreach($dataextinguisher as $value){
			if(file_exists(WWW_ROOT."img/uploaded_attachments/".$value['ExtinguisherReportAttachment']['attach_file'])){			
			unlink(WWW_ROOT."img/uploaded_attachments/".$value['ExtinguisherReportAttachment']['attach_file']);
			$this->ExtinguisherReportAttachment->delete($value['ExtinguisherReportAttachment']['id']);
			}
			
		}
		foreach($dataspecial as $value){
			if(file_exists(WWW_ROOT."img/uploaded_attachments/".$value['SpecialhazardReportAttachment']['attach_file'])){			
			unlink(WWW_ROOT."img/uploaded_attachments/".$value['SpecialhazardReportAttachment']['attach_file']);
			$this->SpecialhazardReportAttachment->delete($value['SpecialhazardReportAttachment']['id']);
			}
			
		}				
		
		$this->SprinklerReportNotification->deleteAll(array('SprinklerReportNotification.servicecall_id' => $id));
		$this->KitchenhoodReport->deleteAll(array('KitchenhoodReport.servicecall_id' => $id));
		
		$this->KitchenhoodReportNotification->deleteAll(array('KitchenhoodReportNotification.servicecall_id' => $id));
		$this->ExtinguisherReport->deleteAll(array('ExtinguisherReport.servicecall_id' => $id));
		
		$this->ExtinguisherReportNotification->deleteAll(array('ExtinguisherReportNotification.servicecall_id' => $id));
		$this->EmergencyexitReport->deleteAll(array('EmergencyexitReport.servicecall_id' => $id));
		
		$this->EmergencyexitReportNotification->deleteAll(array('EmergencyexitReportNotification.servicecall_id' => $id));
		$this->FirealarmReport->deleteAll(array('FirealarmReport.servicecall_id' => $id));
		
		$this->FirealarmReportNotification->deleteAll(array('FirealarmReportNotification.servicecall_id' => $id));
		$this->Signature->deleteAll(array('Signature.servicecall_id' => $id));
		$this->FaAlarmDevicePlace->deleteAll(array('FaAlarmDevicePlace.servicecall_id' => $id));
		
	}
	//$this->ServiceCall->delete($id);
	echo "1"; exit;
		
	}
 /*
*************************************************************************
*Function Name		 :	sp_serviceCallDetail
*Functionality		 :	detail of approved service call according to the client id.
*************************************************************************
*/	
  function sp_serviceCallDetail($client_id=null){
	$client_id=base64_decode($client_id);
	$this->set("client_id",$client_id);
	$this->loadModel('ServiceCall');
	$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.client_id'=>$client_id,'ServiceCall.call_status'=>1),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
	$this->set("data",$res);
	$this->set('frequency',Configure::read('frequency'));		
  }
  
 /*
*************************************************************************
*Function Name		 :	sp_pendingServiceCallDetail
*Functionality		 :	detail of pending service call according to the client id.
*************************************************************************
*/	
  function sp_pendingServiceCallDetail($client_id=null){
	$client_id=base64_decode($client_id);
	$this->set("client_id",$client_id);
	$this->loadModel('ServiceCall');
	$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.client_id'=>$client_id,'ServiceCall.call_status'=>0),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
	$this->set("data",$res);
	$this->set('frequency',Configure::read('frequency'));		
  }  

 /*
*************************************************************************
*Function Name		 :	sp_sendEmail
*Functionality		 :	send mail to client  .
*************************************************************************
*/	
  function sp_sendEmail($client_id=null){
    $client_id=base64_decode($client_id);
    $this->set('clientId',$client_id); 
    $sp = $this->Session->read('Log');
	  $sp_email = $sp['User']['email'];	  
    $data=$this->User->find('first',array('conditions'=>array('User.id'=>$client_id),'fields'=>'email'));
    $this->set('data',$data);
      if(isset($this->data) && !empty($this->data)){
       $to = $this->data['Message']['message_to'];
			  $subject = $this->data['Message']['message_body'] ;
			  $message = "<table cellspacing='0' cellpadding='0' border='0' width='620'>
                          <tr>
                          <td style='padding: 4px 8px; background: rgb(59, 89, 152) none repeat scroll 0% 0%; -mz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; color: rgb(255, 255, 255); font-weight: bold; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; vertical-align: middle; font-size: 16px; letter-spacing: -0.03em; text-align: left;'>
                          <p>" .$this->data['Message']['message_body']." </p>
                          </td>
                        
                    </table>";
        $headers = "MIME-Version: 1.0" . "\r\n";
  			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
  			$headers .= 'From: ReportOnlinePlus <'.$sp_email.'>' . "\r\n" ;	       
          mail($to, $subject, $message, $headers);     
      	  $this->Session->setFlash('Email Sent Successfully');	
      	  $this->redirect(array("controller" => "sps", "action" => "clientlisting")); 
      
      }
	
  }
 /*
*************************************************************************
*Function Name		 :	sp_attachmentDetail
*Functionality		 :	to view detail of attachments of a client 
*************************************************************************
*/
    function sp_attachmentDetail($client_id=null){
      $client_id=base64_decode($client_id);
	  $this->loadModel('User');
	  
	  $user_res=$this->User->find("first",array('conditions'=>array('User.id'=>$client_id),'recursive'=>1));
      
	  $this->set("result",$user_res);
	  $this->loadModel('ServiceCall');
      $res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.client_id'=>$client_id),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
  		$this->set("data",$res);
  		$this->set('frequency',Configure::read('frequency'));
        
    }
/***************************************************************************************
	#THIS FUNCTION IS USED TO DOWNLOAD A ATTACHMENT 
****************************************************************************************/ 	  
	function sp_download ($fileName = null){
		$mimeType = array('bmp'=>'image/bmp','docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','gif'=>'image/gif','jpg'=>'image/jpeg','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel','xlsx'=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','doc'=>'application/msword [official]','png'=>'image/png','rar'=>'application/x-rar-compressed','pdf'=>'application/pdf');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT."img/uploaded_attachments/"
		);
		$this->set($params);
	}
/***************************************************************************************
	#THIS FUNCTION IS USED TO ADD A ATTACHMENT IN SP PANEL UNDER CLIENTS>>ATTACMENTS>>ADD ATTACHMENT 
****************************************************************************************/ 
	function sp_add_attachment(){
	
		if(isset($this->data) && !empty($this->data)){         
			$sp = $this->Session->read('Log');
			    $sp_id = $sp['User']['id'];
			$reportId=$this->data['Report']['report_id']; 
			$this->loadModel('Attachment');  			    
				$this->data['Attachment']['report_id'] = $this->data['Report']['report_id'];
				$this->data['Attachment']['servicecall_id'] = $this->data['Report']['servicecall_id'];
				$this->data['Attachment']['attach_file'] = $this->data['Report']['attach_file'];
				$this->data['Attachment']['user_id'] = $sp_id;
					    $this->data['Attachment']['added_by'] = 'SP';
					    $this->Attachment->save($this->data['Attachment']);
			       
			
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/sp/sps/clientlisting');
		}  
	}

/***************************************************************************************
	#THIS FUNCTION IS USED TO ADD A Notification IN SP PANEL UNDER CLIENTS>>ATTACMENTS>>ADD ATTACHMENT 
****************************************************************************************/ 
	function sp_add_notification(){
	
    if(isset($this->data) && !empty($this->data)){         
      $sp = $this->Session->read('Log');
	    $sp_id = $sp['User']['id'];
      $reportId=$this->data['Report']['report_id']; 
      $this->loadModel('Notification');    
                
                $this->data['Notification']['report_id'] = $this->data['Report']['report_id'];
                $this->data['Notification']['servicecall_id'] = $this->data['Report']['servicecall_id'];
                $this->data['Notification']['notification'] = $this->data['Report']['notification'];
                $this->data['Notification']['user_id'] = $sp_id;
		            $this->data['Notification']['added_by'] = 'SP';
		            $this->Notification->save($this->data['Notification']);
             
        
        $this->Session->setFlash('Notification has been saved successfully.');
			  $this->redirect('/sp/sps/clientlisting');
    }  
  }	
/***************************************************************************************
	#THIS FUNCTION IS USED TO DELETE A Attachment IN SP PANEL UNDER CLIENTS>>ATTACMENTS>>ADD ATTACHMENT 
****************************************************************************************/ 
  function delattachment($attachId,$reportId){
    $this->loadModel('Attachment');
             
                  $data=$this->Attachment->find('first',array('conditions'=>array('id'=>$attachId)));
              		if(file_exists(WWW_ROOT."/img/uploaded_attachments/".$data['Attachment']['attach_file'])){
              		  unlink(WWW_ROOT."/img/uploaded_attachments/".$data['Attachment']['attach_file']); 
              		}
              		if($this->Attachment->delete($attachId)){
              			echo "1"; exit; 
              		}
              		else {
              			echo "0"; exit; 
              		}
              		   
  
  }
/***************************************************************************************
	#THIS FUNCTION IS USED TO DELETE A Attachment IN SP PANEL UNDER CLIENTS>>ATTACMENTS>>ADD ATTACHMENT 
****************************************************************************************/ 
  function delnotifier($attachId,$reportId){
    $this->loadModel('Notification');
           
                    if($this->Notification->delete($attachId)){
              			echo "1";
				exit; 
              		}
              		else {
              			echo "0";
		   		exit; 		
              		}
                 
     
  }
  
/*****************************************************************************************
  * Ajax Upload attachment
*****************************************************************************************/
  function ajaxuploadAttachment(){
    $this->autoRender = false;
    if(isset($_FILES['qqfile'])){      
	      $result = $this->Customupload->uploadattachment($_FILES['qqfile']);
	      if($result != "0") {
		    //echo '{"success":true,"attachment:test"}';
		    echo '{"success":true,"attachment":"'.$result.'"}';
	      }else{
		    echo '{"success":false}';
	      }
    }    
}
/*****************************************************************************************
  function to view inspectors log in sp panel  
*****************************************************************************************/

	function sp_viewLog(){
		$this->loadModel('InspectorLog');
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);		
		$this->set('servicecallId',$_REQUEST['serviceCallID']);		
		$this->set('reportId',$_REQUEST['reportID']);
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			$logData=$this->InspectorLog->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'],'client_id'=>$_REQUEST['clientID'],'sp_id'=>$_REQUEST['spID']),'order'=>'InspectorLog.created DESC'));			
			$this->set('logData',$logData);
		}
		
	}
/*****************************************************************************************
  function to show lead info to SP 
*****************************************************************************************/
	function sp_leadInfo(){
		$sp = $this->Session->read('Log');
	    $spId = $sp['User']['id'];		
		$this->loadModel('Lead');		
		$leadData=$this->Lead->find('all',array('conditions'=>array('Lead.sp_id'=>$spId),'order'=>'Lead.created DESC'));			
		$this->set('leadData',$leadData);
	}
/*****************************************************************************************
  function to decline lead request sent by admin  
*****************************************************************************************/	
	function sp_declinedRequest(){
		$this->loadModel('Lead');
		
		if(!empty($this->data) && isset($this->data)){
			$this->data['Lead']['status']=2;
			if($this->Lead->save($this->data,array('validate'=>false))){
			$clientname=$this->User->find('first',array('conditions'=>array('User.id'=>$this->data['Lead']['client_id']),'fields'=>'fname'));
			$spname=$this->User->find('first',array('conditions'=>array('User.id'=>$this->data['Lead']['sp_id']),'fields'=>'fname'));
				
				$to = ADMIN_EMAIL;
				$subject = 'Declined Response To Lead' ;
				$message = "<table cellspacing='2' cellpadding='2' border='' width='620'>
							<tr>
							<td style='padding: 4px 8px; background: rgb(59, 89, 152) none repeat scroll 0% 0%; -mz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial; color: rgb(255, 255, 255); font-weight: bold; font-family: 'lucida grande',tahoma,verdana,arial,sans-serif; vertical-align: middle; font-size: 16px; letter-spacing: -0.03em; text-align: left;'>
							<p>Dear Admin, </p>							
							</td>
							<tr>
							<td>SP ".$spname['User']['fname']." has declined the lead request of your client (".$spname['User']['fname'].").The reason is: </td>
							</tr>
							<tr>							
							<td>&nbsp;</td>
							</tr>
							<tr>
							<td>".$this->data['Lead']['declinereason'] ."</td>
							</tr>
							<tr>
							<td>Now you can forward the request to another service provider from admin panel</td>
							</tr>
						  
					  </table>";
					
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
				$headers .= 'From: ReportOnlinePlus <'.EMAIL_FROM.'>' . "\r\n" ;
		  
				mail($to, $subject, $message, $headers);   
				$this->Session->setFlash('Request declined successfully');	
				$this->redirect(array("controller" => "sps", "action" => "leadInfo")); 
			}
		}	
	}
/*****************************************************************************************
  function to Accept lead request sent by admin  
*****************************************************************************************/		
	function sp_acceptedRequest(){
		$this->loadModel('Lead');
		$this->loadModel('AcceptedLead');
		if(!empty($this->data) && isset($this->data)){
			$this->data['Lead']['id']=$this->data['AcceptedLead']['id'];
			$this->data['Lead']['status']=1;
			if($this->Lead->save($this->data)){
				if($this->AcceptedLead->save($this->data['AcceptedLead'])){
					$this->Session->setFlash('Request accepted successfully');	
					$this->redirect(array("controller" => "sps", "action" => "leadInfo")); 	
				}
			}
		}
	}
/*****************************************************************************************
  function to delete lead request by admin  
*****************************************************************************************/			
	function deleteLeadData($id=null){		
		$this->loadModel('Lead');
		if($this->Lead->delete($id)){
			echo "1"; exit;	
		}
		
	}
/*****************************************************************************************
  function to check whether lead inspector is avialable for given schedule dates  
*****************************************************************************************/				
	function checkInspectorAvail($id=null){		
		$this->loadModel('Schedule');
		$this->autoRender=false;
		$arrscheduleDates=array();
		for($i=1;$i<=6;$i++){			
			if($this->data[$i]['Schedule']['report_id'] != "0"){				
				$scheduleData  = $this->Schedule->find('all',array('conditions'=>array('Schedule.lead_inspector_id'=>$this->data[$i]['Schedule']['lead_inspector']),'recursive'=>0));		
				
				//pr($scheduleData); die;
				
				$str='';
				$j=0;
				foreach($scheduleData as $sData):
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_1'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_2'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_3'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_4'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_5'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_6'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_7'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_8'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_9'];
				$arrscheduleDates[]=$sData['Schedule']['schedule_date_10'];
				$j++;
				endforeach;
				
				//pr($arrscheduleDates);die;
				
				//foreach($scheduleData as $sData){
					
					if(empty($this->data[$i]['Schedule']['schedule_date_1']) && empty($this->data[$i]['Schedule']['schedule_date_2']) && empty($this->data[$i]['Schedule']['schedule_date_3']) && empty($this->data[$i]['Schedule']['schedule_date_4']) && empty($this->data[$i]['Schedule']['schedule_date_5']) && empty($this->data[$i]['Schedule']['schedule_date_6']) && empty($this->data[$i]['Schedule']['schedule_date_7']) && empty($this->data[$i]['Schedule']['schedule_date_8']) && empty($this->data[$i]['Schedule']['schedule_date_9']) && empty($this->data[$i]['Schedule']['schedule_date_10'])){
						echo "Please select atleast one schedule date"; die;
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_1']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_1'])), $arrscheduleDates)) {
						$str.= $this->data[$i]['Schedule']['schedule_date_1'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_2']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_2'])), $arrscheduleDates)) {
					$str.= $this->data[$i]['Schedule']['schedule_date_2'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_3']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_3'])), $arrscheduleDates)) {
					
					$str.= $this->data[$i]['Schedule']['schedule_date_3'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_4']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_4'])), $arrscheduleDates)) {
					$str.= $this->data[$i]['Schedule']['schedule_date_4'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_5']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_5'])), $arrscheduleDates)) {
					$str.= $this->data[$i]['Schedule']['schedule_date_5'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_6']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_6'])), $arrscheduleDates)) {
					$str.= $this->data[$i]['Schedule']['schedule_date_6'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_7']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_7'])), $arrscheduleDates)) {
					$str.= $this->data[$i]['Schedule']['schedule_date_7'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_8']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_8'])), $arrscheduleDates)) {
					$str.= $this->data[$i]['Schedule']['schedule_date_8'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_9']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_9'])), $arrscheduleDates)) {
					$str.= $this->data[$i]['Schedule']['schedule_date_9'].', ';
					
					}
					if (isset($this->data[$i]['Schedule']['schedule_date_10']) && in_array(date("Y-m-d",strtotime($this->data[$i]['Schedule']['schedule_date_10'])), $arrscheduleDates)) {
					$str.= $this->data[$i]['Schedule']['schedule_date_10'];					
					}
					
					if(!empty($str)){
						echo "Lead inspector you have selected is not available on ".$str; die;
					}else{
						echo "Lead inspector you have selected is available"; die;
					}
			
					
				//}
			}
		}
		
		
	}
	
	#function to view upcoming cron schedules
	function sp_cronschedule(){
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		#CODE 27-07-2012
		foreach($res as $key1=>$servicecall):
		$client_arr[]=$servicecall['ServiceCall']['client_id'];
		endforeach;
		$clientFinalArr=array_unique($client_arr);	
		$i=0;$j=0;
		foreach($clientFinalArr as $clientID):
		//$arr[$i][$j]['client_id']=$clientID;
		$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.client_id'=>$clientID),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		foreach($res1 as $resFinal):		
		$arr[$clientID][$j]=$resFinal;
		$j++;
		endforeach;
		$i++;
		endforeach;
		//pr($arr);
		$this->set("Clientdata",$arr);
		$this->set('frequency',Configure::read('frequency'));		
	}
	
	 function delPrincipalRecord($rid = null){
		$this->layout = null;
		$this->loadModel('UserCompanyPrincipalRecord');
		 //if(1==1){
		if($this->UserCompanyPrincipalRecord->delete($rid)){
			echo "1";
			exit;
		}else{
			echo "0";
			exit;
		}
	 }
	 
	 /*
	  Function:     sps_showMessage
	  Description:  To show the message when the max site addresses limit reached
	  param:        None
	  return:       None
	  Created:      October 16, 2012 by Manish Kumar
	 */
	 function sp_showMessage(){
		$this->loadModel('MaxLimitAlertText');
		$maxText = $this->MaxLimitAlertText->find('first');
		$this->set(compact('maxText'));
		$spsession = $this->_checkSp();
		$link = "<a href=" . $this->selfURL() . "/homes/splogin>Click here</a>"; 
		/*Send a notification email to user code strats here*/
		$this->loadModel('EmailTemplate');
		//CODE TO FIRE AN EMAIL			       
		$limit_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>15)));	
		$code_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#LINK'),array($this->selfURL() ,ucwords($spsession['User']['fname'].' '.$spsession['User']['lname']),$link),$limit_temp['EmailTemplate']['mail_body']);
		$this->set('maxlimittemplate',$code_temp['EmailTemplate']['mail_body']);
		$this->Email->from = EMAIL_FROM;
		$this->Email->to = $spsession['User']['email'];
		//$this->Email->to = 'manishku@smartdatainc.net';
		$this->Email->subject = $limit_temp['EmailTemplate']['mail_subject'];
		$this->Email->template = 'maxlimit_warning';
		$this->Email->sendAs = 'both'; // because we like to send pretty mail			
		$this->Email->send(); //Do not pass any args to send()
		/*Send a notification email to user code ends here*/
	 }
	 
	 #Made on 22 Oct-2012 by Vishal
	 function sp_sitelist($type=null,$client_id=null)
	 {
		$this->loadModel('SpSiteAddress');
		$this->set('country',$this->getCountries());
		$this->set('state',$this->getState(233));
		if(isset($type)&& $type=='A')
		{
			$sitelists = $this->SpSiteAddress->find('all', array('conditions' => array('SpSiteAddress.client_id'=>$client_id,'SpSiteAddress.idle'=>0),'recursive'=>1));
			$this->set("sitelists",$sitelists);
			$this->set("type",'Active');
			$this->set("client_id",$client_id);
		}
		if(isset($type)&& $type=='I')
		{
			$sitelists = $this->SpSiteAddress->find('all', array('conditions' => array('SpSiteAddress.client_id'=>$client_id,'SpSiteAddress.idle'=>1),'recursive'=>1));
			$this->set("sitelists",$sitelists);
			$this->set("type",'Idle');			
			$this->set("client_id",$client_id);
		}
		
		if(!empty($this->data))
		{
			
		}
		else
		{
			
		}
		
		
	 }
	 
	 function sp_makeidle($sp_site_address_id=null){
		$this->loadModel('SpSiteAddress');
		$this->SpSiteAddress->id=$sp_site_address_id;
		$data['SpSiteAddress']['idle']=1;
		if($this->SpSiteAddress->save($data['SpSiteAddress']))
		{
			$this->Session->setFlash(__('Site address has been idle successfully',true));
			$this->redirect(array('controller'=>'sps','action'=>'sp_clientlisting'));
		}
		
	 }
	 
	 function sp_makedelete($sp_site_address_id=null){
		$this->loadModel('SpSiteAddress');
		$this->loadModel('ServiceCall');	
		if($this->SpSiteAddress->delete($sp_site_address_id))
		{
			$conditions=array('ServiceCall.sp_site_address_id'=>$sp_site_address_id);
			$this->ServiceCall->deleteAll($conditions, $cascade = true, $callbacks = false);
			$this->Session->setFlash(__('Site address and all documents and history associated with this site has been deleted successfully',true));
			$this->redirect(array('controller'=>'sps','action'=>'sp_clientlisting'));
		}
		
	 }
	 
	 function sp_request_activation($sp_site_address_id=null){
		$this->loadModel('SpSiteAddress');
		$this->SpSiteAddress->id=$sp_site_address_id;
		$data['SpSiteAddress']['activation_request']=1;
		if($this->SpSiteAddress->save($data['SpSiteAddress']))
		{
			
			#code to send an email
			$this->loadModel('EmailTemplate');
			$sp = $this->Session->read('Log');
			$sp_id = $sp['User']['id'];
			$sp_name = $sp['User']['fname'].' '.$sp['User']['lname'];
			$sp_email = $sp['User']['email'];
			$link = "<a href=" . $this->selfURL() . "/admin>Click here</a>"; 
				//CODE TO FIRE AN EMAIL			       
				$body_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>17)));	
				echo $code_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#EMAIL','#LINK'),array($this->selfURL() ,ucwords($sp_name),@$sp_email,$link),$body_temp['EmailTemplate']['mail_body']);									  
				$this->set('codeinquirytemplate',$body_temp['EmailTemplate']['mail_body']);
				
				$this->Email->from = EMAIL_FROM;
				$this->Email->to = ADMIN_EMAIL;				
				$this->Email->subject = $body_temp['EmailTemplate']['mail_subject'];
				$this->Email->template = 'code_inspection';
				$this->Email->sendAs = 'both'; // because we like to send pretty mail			
				$this->Email->send(); //Do not pass any args to send()	
			$this->Session->setFlash(__('Your request has been send successfully for site address activation',true));
			$this->redirect(array('controller'=>'sps','action'=>'sp_clientlisting'));
		}
	 }
	 
	function sp_addSiteAddress($client_id=null)
	{
		$this->loadModel('SubscriptionDuration');
		$this->loadModel('SpSiteAddress');
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$this->set('sp_id',$sp_id);		
		
		# To check the max limit of subscription plan
		/*if($sp['User']['subscription_under_trial'] == 'No'){			
			$sitecount=$this->SpSiteAddress->find('count',array('conditions'=>array('SpSiteAddress.sp_id'=>$sp['User']['id'])));	
			$plantype=$this->SubscriptionDuration->find('first',array('conditions'=>array('id'=>$sp['User']['subscription_duration_id']),'fields'=>array('max_allowed_site_address')));
			if($sitecount >= $plantype['SubscriptionDuration']['max_allowed_site_address']){
					$this->Session->setFlash(__('You are not authorised to add more site addresses as maximum site limit of your subscption plan is over',true));
					$this->redirect(array('controller'=>'sps','action'=>'clientlisting'));		
			}
		}*/
		
		$this->loadModel('CodeType');		
		$this->loadModel('User');
		$cid = base64_decode($client_id);
		$this->set('country',$this->getCountries());		
		$user_data = $this->User->find('first',array('conditions'=>array('User.id'=>$cid)));	
		$this->set('client_data',$user_data);
		$this->set('state',$this->getState($user_data['User']['country_id']));
		$this->set('billtostate',$this->getState($user_data['User']['billto_country_id']));
		$this->set('city',$this->getCity($user_data['User']['state_id']));		 
		$this->set('billtocity',$this->getCity($user_data['User']['billto_state_id']));			 
		
		$codetype = $this->CodeType->find('all');	
		$this->set('codetype',$codetype);
		
		if(isset($this->data) && !empty($this->data))
		{		
			$this->data['SpSiteAddress']['site_phone']=$this->data['SpSiteAddress']['site_phone1'].'-'.$this->data['SpSiteAddress']['site_phone2'].'-'.$this->data['SpSiteAddress']['site_phone3'];
			$this->data['SpSiteAddress']['responsible_contact']=$this->data['SpSiteAddress']['availphone1'].'-'.$this->data['SpSiteAddress']['availphone2'].'-'.$this->data['SpSiteAddress']['availphone3'];
			$this->data['SpSiteAddress']['client_id']=$cid;
			$this->data['SpSiteAddress']['sp_id'] = $sp_id;			
			$this->SpSiteAddress->save($this->data['SpSiteAddress']);		
			$this->Session->setFlash(__('New Site Address has been added successfully',true));
			$this->redirect(array('controller'=>'sps','action'=>'clientlisting'));
		       		    
		}		
	}
	
/*
*************************************************************************
  #FUNCTION USED TO SHOW response to PENDING SERVICE CALLS from admin
*************************************************************************
*/
	function sp_responseSchedule(){
		
		$this->set('title_for_layout',"Monthly View");	
		
		$this->loadModel('User');
		$this->loadModel('Service');
		$this->loadModel('ServiceCall');
		#CODE to find all clients assigned in assigned table
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$res=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.call_status'=>2),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		$this->set("data",$res);
		
		
		#CODE 27-07-2012
		if(!empty($res)){
		foreach($res as $key1=>$servicecall):
		$client_arr[]=$servicecall['ServiceCall']['client_id'];
		endforeach;
		$clientFinalArr=array_unique($client_arr);
		}
		if(!empty($res)){		
		$i=0;$j=0;
		foreach($clientFinalArr as $clientID):
		//$arr[$i][$j]['client_id']=$clientID;
		$res1=$this->ServiceCall->find("all",array('conditions'=>array('ServiceCall.sp_id'=>$sp_id,'ServiceCall.client_id'=>$clientID,'ServiceCall.call_status'=>2),'order'=>array('ServiceCall.id DESC'),'recursive'=>2));
		foreach($res1 as $resFinal):		
		$arr[$clientID][$j]=$resFinal;
		$j++;
		endforeach;
		$i++;
		endforeach;
		//pr($arr);
		$this->set("Clientdata",$arr);
		
		
		$this->set('frequency',Configure::read('frequency'));
		}
	}

/*
	Function: checkVacations,
	desc: To check the available inspector for service call,
	created by: Manish Kumar,
	on: Oct 27, 2012
*/	
	function checkVacations(){
		$inspectorId = $this->params['form']['insId'];
		$serviveDate = $this->params['form']['serveDate'];
		$this->loadModel('InspectorVacation');
		$this->InspectorVacation->bindModel(array(
							  'belongsTo'=>array('User')
							  )
						    );
		$serviveDate = date('Y-m-d',strtotime($serviveDate));		
		$vacationCount=$this->InspectorVacation->find('all',array('conditions'=>array('InspectorVacation.user_id'=>$inspectorId,'InspectorVacation.start_date <='=>$serviveDate,'InspectorVacation.end_date >='=>$serviveDate)));
		//$vacationCount=$this->InspectorVacation->find('all',array('conditions'=>array('InspectorVacation.user_id'=>$inspectorId)));
		
		$this->loadModel('Schedule');
		$this->Schedule->unbindModel(array(
							  'belongsTo'=>array('ServiceCall'),
							  'hasMany'=>array('ScheduleService')
							  )
						    );
		$this->Schedule->bindModel(array(
							  'belongsTo'=>array('User'=>array('className'=>'User','foreignKey'=>'lead_inspector_id'))
							  )
						    );
		$conditions=array('Schedule.lead_inspector_id'=>$inspectorId,'OR'=>array('Schedule.schedule_date_1'=>$serviveDate,'Schedule.schedule_date_2'=>$serviveDate,'Schedule.schedule_date_3'=>$serviveDate,'Schedule.schedule_date_4'=>$serviveDate,'Schedule.schedule_date_5'=>$serviveDate,'Schedule.schedule_date_6'=>$serviveDate,'Schedule.schedule_date_7'=>$serviveDate,'Schedule.schedule_date_8'=>$serviveDate,'Schedule.schedule_date_9'=>$serviveDate,'Schedule.schedule_date_10'=>$serviveDate));
		$inspectionCount=$this->Schedule->find('all',array('conditions'=>$conditions));
		
		if(count($vacationCount)>0){
			$startDate = date('F d, Y',strtotime($vacationCount[0]['InspectorVacation']['start_date']));
			$endDate = date('F d, Y',strtotime($vacationCount[0]['InspectorVacation']['end_date']));
			$inspectorName = $vacationCount[0]['User']['fname'].' '.$vacationCount[0]['User']['lname'];
			$message = '{"fineMsg":"OK","startDate":"'.$startDate.'","endDate":"'.$endDate.'","insId":"'.$inspectorId.'","insName":"'.$inspectorName.'"}';
		}else if(count($inspectionCount)>0){
			$insDate = date('F d, Y',strtotime($serviveDate));			
			$inspectorName = $inspectionCount[0]['User']['fname'].' '.$inspectionCount[0]['User']['lname'];
			$message = '{"fineMsg":"NA","insDate":"'.$insDate.'","insId":"'.$inspectorId.'","insName":"'.$inspectorName.'"}';
			
		}else{
			$message = '{"fineMsg":"NO"}';
		}
		echo $message;
		die;
	}
	
/*
	Function: sendQuote,
	desc: To send the quote from sp to client,
	created by: Manish Kumar,
	on: Nov 26, 2012
*/	
	function sp_sendQuote(){
		$this->loadModel('Quote');
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);		
		$this->set('servicecallId',$_REQUEST['serviceCallID']);		
		$this->set('reportId',$_REQUEST['reportID']);
		if(isset($_REQUEST['spID']) && isset($_REQUEST['clientID']) && isset($_REQUEST['serviceCallID']) && isset($_REQUEST['reportID'])){
			$chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
			$this->set('chkRecord',$chkRecord);
		}
		if(isset($this->data) && !empty($this->data)){
			if(isset($this->data['Quote']['attachment']['name']) && !empty($this->data['Quote']['attachment']['name'])){
				$temp_exp = explode('.',$this->data['Quote']['attachment']['name']);
				$extPart = end($temp_exp);
				    // Start of file uploading script		
				    $allowedExts = array("pdf", "xls", "docx", "xlsx","doc");
				    if(in_array($extPart, $allowedExts)){
					    $target = WWW_ROOT.'quotefile/';
					    //$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
					    $randomName = md5(time()).'.'.$extPart;
					    $target = $target.$randomName;
					    if(move_uploaded_file($this->data['Quote']['attachment']['tmp_name'], $target)){
						    $data['Quote']['title'] = $this->data['Quote']['title'];
						    $data['Quote']['attachment'] = $randomName;						    
						    $data['Quote']['sp_id']=$_REQUEST['spID'];
						    $data['Quote']['client_id']=$_REQUEST['clientID'];
						    $data['Quote']['report_id']=$_REQUEST['reportID'];
						    $data['Quote']['servicecall_id']=$_REQUEST['serviceCallID'];
						    if(isset($chkRecord) && is_array($chkRecord)){
							$data['Quote']['id']=$chkRecord['Quote']['id'];
							if(file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'quotefile'.DS.$chkRecord['Quote']['attachment'])){
								unlink(ROOT.DS.'app'.DS.'webroot'.DS.'quotefile'.DS.$chkRecord['Quote']['attachment']);								
							}
						    }
						    $this->Quote->save($data['Quote']);
						    /*send mail to SP*/
						    
						    $this->sendQuoteMail($_REQUEST['spID'],$_REQUEST['clientID'],$_REQUEST['serviceCallID'],$_REQUEST['reportID']);
						    
						    /*send mail to sp*/						    
						    $this->Session->setFlash(__('Quote has been saved successfully',true));						    
						    $this->redirect($this->referer());
					    } 
				    }
				    else{
					    $this->Session->setFlash(__('Sorry ! Please upload only pdf/xls/doc/xlsx/docx file',true));
					    $this->redirect($this->referer());
				    }
			    }
		}
	}
	
/*
	Function: sendQuoteMail,
	desc: To send the email to client,
	created by: Manish Kumar,
	on: Nov 26, 2012
*/
	function sendQuoteMail($spId,$clientId,$servicecallId,$reportId){
		#code to send an email
		$this->loadModel('EmailTemplate');
		$sp = $this->Session->read('Log');
		$sp_id = $sp['User']['id'];
		$sp_name = $sp['User']['fname'].' '.$sp['User']['lname'];
		$sp_email = $sp['User']['email'];
		
		/*Client Data*/
		$this->loadModel('User');
		$clientInfo = $this->User->findById($clientId,array('User.email','User.fname','User.lname'));
		$client_name = $clientInfo['User']['fname'].' '.$clientInfo['User']['lname'];
		$client_email = $clientInfo['User']['email'];
		/*Client Data*/
		
		/*Report Data*/
		$this->loadModel('Report');
		$reportInfo = $this->Report->findById($reportId,array('Report.name'));
		$reportName = $reportInfo['Report']['name'];
		/*Report Data*/
		
		//CODE TO FIRE AN EMAIL
		$link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
		$body_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>22)));	
		$code_temp['EmailTemplate']['mail_body']=str_replace(array('../../..','#CLIENT_NAME','#SP_NAME','#REPORTNAME','#LINK'),array($this->selfURL(),ucwords($client_name),ucwords($sp_name),ucwords($reportName),$link),$body_temp['EmailTemplate']['mail_body']);									  
		$this->set('quotecontent',$code_temp['EmailTemplate']['mail_body']);
		
		$this->Email->from = EMAIL_FROM;
		$this->Email->to = $client_email;				
		$this->Email->subject = $body_temp['EmailTemplate']['mail_subject'];
		$this->Email->template = 'quotetemplate';
		$this->Email->sendAs = 'both'; // because we like to send pretty mail			
		$this->Email->send(); //Do not pass any args to send()
	}
/*
	Function: sp_deleteQuote,
	desc: To delete the sent quote,
	created by: Manish Kumar,
	on: Nov 26, 2012
*/	
	function sp_deleteQuote($fileName = null){
		if(file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'quotefile'.DS.$fileName)){
			unlink(ROOT.DS.'app'.DS.'webroot'.DS.'quotefile'.DS.$fileName);
			$this->loadModel('Quote');
			$this->Quote->delete(array('Quote.attachment'=>$fileName));
			echo 'OK';
		}else{
			echo 'ERROR';
		}
		die;
	}
	
/*
	Function: sp_download_quote,
	desc: To download the quote file,
	created by: Manish Kumar,
	on: Nov 26, 2012
*/	
	function sp_download_file($fileName = null,$folderName = null){
		$mimeType = array('docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT.$folderName."/"
		);
		$this->set($params);
	}
	
/*
	Function: sp_sendManualMail,
	desc: To maually send the approval email,
	created by: Manish Kumar,
	on: Dec 26, 2012
*/
	function sp_sendManualMail(){
		$this->loadModel('ServiceCall');
		$pendingDatas = $this->ServiceCall->find('all',array('conditions'=>array('ServiceCall.call_status'=>0),'fields'=>array('ServiceCall.client_id'),'recursive'=>-1));
		$clientIds = array();
		foreach($pendingDatas as $data){
			$clientIds[] = $data['ServiceCall']['client_id'];
		}
		$clientIds = array_unique($clientIds);		
		
		$this->loadModel('User');
		$userData = $this->User->find('all',array('conditions'=>array('User.id'=>$clientIds),'fields'=>array('User.email','User.fname','User.lname')));
		$optionsData = array();
		foreach($userData as $userDatas){
			$optionsData[$userDatas['User']['email']] = $userDatas['User']['fname'].' '.$userDatas['User']['lname'];
		}
		$this->set('optionsData',$optionsData);
		
		if(isset($this->data) && !empty($this->data)){			
			$body=$this->data['SendMail']['message'];
			$this->set('re_manual_mail',$body);
			$this->Email->to = $this->data['SendMail']['to'];
			$this->Email->subject = $this->data['SendMail']['subject'];
			$this->Email->from = EMAIL_FROM;
			$this->Email->template = 're_manual_mail'; // note no '.ctp'
			$this->Email->sendAs = 'both'; // because we like to send pretty mail			
			$this->Email->send();//Do not pass any args to send()
			$this->Session->setFlash(__( 'Email has been sent successfully',true));
			$this->redirect(array('controller'=>'schedules','action'=>'pendingSchedule'));
		}
	}
/*
	Function: sp_setReminderMail,
	desc: To set the reminder for the automatic email send,
	created by: Manish Kumar,
	on: Dec 26, 2012
*/	
	function sp_setReminderMail($callId = null){
		$callId = base64_decode($callId);
		if(isset($this->data) && !empty($this->data)){
			$this->loadModel('ServiceCall');
			$remainigDays = $this->data['ReminderMail']['days'];
			$remiderDate = date('Y-m-d');
			if($this->ServiceCall->updateAll(array('ServiceCall.reminder_days'=>'"'.$remainigDays.'"','ServiceCall.reminder_date'=>'"'.$remiderDate.'"'),array('ServiceCall.id'=>$callId))){
				$this->Session->setFlash('Reminder has been set successfully',true);
			}else{
				$this->Session->setFlash('Reminder not set. Please try again',true);
			}
			$this->redirect(array('controller'=>'schedules','action'=>'pendingSchedule'));
		}
	}
	
	function sp_clientadded($id = null){//echo base64_encode(86);die;
		$id=base64_decode($id);
		$this->loadModel('User');
		$userData = $this->User->findById($id,array('User.fname','User.lname','User.id','User.client_company_name'));
		$this->set('userData',$userData);
	}
	
	#function name: sendApproveMail()
	#functiion description: send the service call approval mail to inspector, client and service provider
	#function added by Manish Kumar
	function sendApproveMail($serviceCallId=null){		
		$this->loadModel('ServiceCall');
		$serviceData = $this->ServiceCall->findById($serviceCallId,array('contain'=>false));
		
		$this->loadModel('User');
		$clientData = $this->User->findById($serviceData['ServiceCall']['client_id']);
		$sp = $this->User->findById($serviceData['ServiceCall']['sp_id']);
					 
		$client_name = $clientData['User']['fname'].' '.$clientData['User']['lname']; //cleintname
		$client_email = $clientData['User']['email'];
		$spname = $sp['User']['fname'].' '.$sp['User']['lname']; //spname,schedulername
		$spemail = $sp['User']['email']; //spemail
		$spschedularphone = $sp['User']['phone']; //sp-schedulerphone
		$spcompany=$sp['Company']['name'];
		
		$this->loadModel('Schedule');
		$this->loadmodel('EmailTemplate');
		$this->Schedule->bindModel(
				array('belongsTo' => array(
				'Report' => array(
				'className' => 'Report',
				'fields'=>array('Report.name')
					)
				    )
				 )
				);
		//$this->Schedule->bindModel('belongsTo'=>'Report');
		$Schdata=$this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$serviceCallId)));
		//pr($Schdata);
		//echo "===============>";
	       
		$email_string ='';
		$email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
					<tbody>
					<tr style="background-color:#0B83A6">
					    <td><b>ReportName</b></td>
					    <td><b>Days</b></td>
					    <td><b>Schedule Date/Time</b></td>
					    <td><b>Inspector Assigned</b></td>
					</tr>';
		$reportdata='';
		$sch2=$sch3=$sch4=$sch5=$sch6=$sch7=$sch8=$sch9=$sch10="";
	       foreach($Schdata as $key=>$sch){
		
		$ins_email_string ='';
		$ins_email_string .= '<table align="center" width="391" height="88" cellspacing="1" cellpadding="1" border="1">
					<tbody>
					<tr style="background-color:#0B83A6">
					    <td><b>ReportName</b></td>
					    <td><b>Days</b></td>
					    <td><b>Schedule Date/Time</b></td>
					    <td><b>Service Provider Name(Company)</b></td>
					    <td><b>Client Name</b></td>
					</tr>';
		
		$leader = $this->User->find('first',array('conditions'=>array('User.id'=>$sch['Schedule']['lead_inspector_id']),'fields'=>array('fname','lname','email')));
		       $reportdata   .=$sch['Report']['name'].',';
		       
		       $email_string .= "<tr>";
		       $ins_email_string .= "<tr>";
		       
		       $days=0;
		       $email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';
		       $ins_email_string .= "<td valign='top'>".$sch['Report']['name'].'</td>';
		       
		       $sch1= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_1'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_1'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_1'])); $days=$days+1;
			if(!empty($sch['Schedule']['schedule_date_2'])) { 
				$sch2= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_2'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_2'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_2'])); $days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_3'])) { 
				$sch3= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_3'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_3'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_3']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_4'])) {
				$sch4= date('m/d/Y',strtotime($sch['schedule_date_4'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_4'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_4']));$days=$days+1;
			}
			if(!empty($schedule['schedule_date_5'])) {
				$sch5= date('m/d/Y',strtotime($sch['schedule_date_5'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_5'])).'-'.date('g:ia', strtotime($sch['schedule_to_5']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_6'])) { 
				$sch6= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_6'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_6'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_6']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_7'])) { 
				$sch7= date('m/d/Y',strtotime($sch['schedule_date_7'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_7'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_7']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_8'])) { 
				$sch8= date('m/d/Y',strtotime($sch['schedule_date_8'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_8'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_8']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_9'])) { 
				$sch9= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_9'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_9'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_9']));$days=$days+1;
			}
			if(!empty($sch['Schedule']['schedule_date_10'])) { 
				$sch10= date('m/d/Y',strtotime($sch['Schedule']['schedule_date_10'])).' '.date('g:ia', strtotime($sch['Schedule']['schedule_from_10'])).'-'.date('g:ia', strtotime($sch['Schedule']['schedule_to_10']));$days=$days+1;
			}
			
		       $ins_email_string .='<td valign="top">'.$days.'</td>';		    
		       $ins_email_string .='<td valign="top">'.$sch1.'<br/>'.$sch2.'<br/>'.$sch3.'<br/>'.$sch4.'<br/>'.$sch5.'<br/>'.$sch6.'<br/>'.$sch7.'<br/>'.$sch8.'<br/>'.$sch9.'<br/>'.$sch10.'</td>';
		       $ins_email_string .='<td valign="top">'.$spname.'('.$spcompany.')'.'</td>'.'<br/>';
		       $ins_email_string .='<td valign="top">'.$client_name.'</td>'.'<br/>';
		       $ins_email_string .= "</tr>";
		       $ins_email_string .= '</tbody></table>';
		       
		       /*Send Approval email to lead inspector code starts here*/
		        $link = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
			$approved_servicecall_temp_ins = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>19)));		
			$approved_servicecall_temp_ins['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#CLIENTNAME','#DETAIL','#LINK'),array($this->selfURL(),$leader['User']['fname'].' '.$leader['User']['lname'],$client_name,$ins_email_string,$link),$approved_servicecall_temp_ins['EmailTemplate']['mail_body']);
			$this->set('servicecall_temp',$approved_servicecall_temp_ins['EmailTemplate']['mail_body']);
			$this->Email->to = $leader['User']['email'];
			$this->Email->subject = $approved_servicecall_temp_ins['EmailTemplate']['mail_subject'];
			$this->Email->from = EMAIL_FROM;
			$this->Email->template = 'service_call_temp'; // note no '.ctp'
			$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
			$this->Email->send();//Do not pass any args to send()
			/*Send Approval email to lead inspector code ends here*/
		       
		       $email_string .='<td valign="top">'.$days.'</td>';		    
		       $email_string .='<td valign="top">'.$sch1.'<br/>'.$sch2.'<br/>'.$sch3.'<br/>'.$sch4.'<br/>'.$sch5.'<br/>'.$sch6.'<br/>'.$sch7.'<br/>'.$sch8.'<br/>'.$sch9.'<br/>'.$sch10.'</td>';
		       $email_string .='<td valign="top">Lead:'.$leader['User']['fname'].' '.$leader['User']['lname'].'</td>'.'<br/>';
		       $email_string .= "</tr>";
	       }
	       
		$email_string .= '</tbody></table>';
		
		/*Send Approval email to client code starts here*/
		$cllink = "<a href=" . $this->selfURL() . "/homes/login>Click here</a>"; 
		$approved_servicecall_temp_client = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>18)));		
		$approved_servicecall_temp_client['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#DETAIL','#LINK'),array($this->selfURL() ,$client_name,$email_string,$cllink),$approved_servicecall_temp_client['EmailTemplate']['mail_body']);
		$this->set('servicecall_temp',$approved_servicecall_temp_client['EmailTemplate']['mail_body']);
		$this->Email->to = $client_email;
		$this->Email->subject = $approved_servicecall_temp_client['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'service_call_temp'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
		$this->Email->send();//Do not pass any args to send()
		/*Send Approval email to client code ends here*/
		
		/*Send Approval email to Service Provider code starts here*/
		$splink = "<a href=" . $this->selfURL() . "/homes/splogin>Click here</a>"; 
		$approved_servicecall_temp_sp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>18)));		
		$approved_servicecall_temp_sp['EmailTemplate']['mail_body']=str_replace(array('../../..','#NAME','#DETAIL','#LINK'),array($this->selfURL() ,$spname,$email_string,$splink),$approved_servicecall_temp_sp['EmailTemplate']['mail_body']);
		$this->set('servicecall_temp',$approved_servicecall_temp_sp['EmailTemplate']['mail_body']);
		$this->Email->to = $spemail;
		$this->Email->subject = $approved_servicecall_temp_sp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'service_call_temp'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail         			
		$this->Email->send();//Do not pass any args to send()
		/*Send Approval email to Service Provider code ends here*/
		
	}
}