<?php
class ReportsController extends AppController{

	var $name = 'Reports';
	var $uses = array('User');
	var $components = array('Customupload','Calendar','Email','Session','Cookie');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Calendar','Common','Text','Number','Time');

	function beforeFilter(){
	 parent::beforeFilter();
	}
/***************************************************************************************
	#THIS FUNCTION IS USED FOR OPEN THE REPORTS
	index function in reports controller 
****************************************************************************************/ 	
	function inspector_index(){

	}
/***************************************************************************************
	#THIS FUNCTION IS USED FOR SEND THE MAIL AFTER FINISH REPORT
****************************************************************************************/ 	
	 function SendMailAfterFinish($clientId = null,$reportId = null,$finishDate = null){
		$this->User->unbindModel(array('hasOne' => array('Company')));
		$this->loadModel('Report');
		$this->Report->unbindModel(array('hasMany' => array('Service')));
		$client_name = $this->User->find('first',array('User.id'=>$clientId));
		$cname = $client_name['User']['fname']." ".$client_name['User']['lname'];
		$report_name = $this->Report->find('first',array('Report.id'=>$reportId));
		$rname = $report_name['Report']['name'];
		$finish_date = $finishDate;
		$link = "<a href=" . $this->selfURL() . "/homes/clientlogin>Click here</a>"; 
		$this->loadmodel('EmailTemplate');
		$completed_temp = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_for'=>13))); 
		$completed_temp['EmailTemplate']['mail_body']=str_replace(array('#NAME','#REPORT_NAME','#FINISH_DATE','#LINK'),array($cname,$rname,$finish_date,$link),$completed_temp['EmailTemplate']['mail_body']);
		$this->set('completed_temp',$completed_temp['EmailTemplate']['mail_body']);
		$this->Email->to = $client_name['User']['email'];
		$this->Email->subject = $completed_temp['EmailTemplate']['mail_subject'];
		$this->Email->from = EMAIL_FROM;
		$this->Email->template = 'report_completion_mail'; // note no '.ctp'
		$this->Email->sendAs = 'both'; // because we like to send pretty mail
		$this->Email->send();//Do not pass any args to send() */
	 }
	
/***************************************************************************************
	#THIS FUNCTION IS USED tomaintain log of inspectors for a report
****************************************************************************************/
	function saveLog($spId,$reportId,$clientId,$servicecallId,$action){
		$this->loadModel('InspectorLog');
		$inspectorses = $this->Session->read('Log');
		$session_inspector_id = $inspectorses['User']['id'];
		$this->data['InspectorLog']['sp_id']=$spId;
		$this->data['InspectorLog']['report_id']=$reportId;
		$this->data['InspectorLog']['client_id']=$clientId;
		$this->data['InspectorLog']['servicecall_id']=$servicecallId;
		$this->data['InspectorLog']['action']=$action;
		$this->data['InspectorLog']['user_id']=$session_inspector_id;
		$this->InspectorLog->save($this->data['InspectorLog'],false);
		
		
	}
/***************************************************************************************
	#THIS FUNCTION IS USED TO SAVE KITCHENHOOD REPORTS IN INSPECTOR PANEL 
	Reports controller kitchenhood reportsfunction 
****************************************************************************************/
	function inspector_kitchenhood(){
	   $this->layout='inspector';
	   $this->loadModel('User');
	   $this->loadModel('Report');
	   $this->loadModel('KitchenhoodReport');
	   $this->loadModel('KitchenhoodReportRecord');
	   $this->loadModel('Attachment');
	   $this->loadModel('Notification');
	   $this->loadModel('ScheduleService');
	   $this->loadModel('Schedule');
	   $this->loadModel('Service');
	   
	   /*To Fetch and set Report Text*/
	   $this->loadModel('SpReportText');
	   $texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
	   $this->set('texts',nl2br($texts['SpReportText']['report_text']));
	   /*To Fetch and set Report Text*/
	   
	   $daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
	   $daysRemaining = $daysData[0]['days'];
	   if($daysRemaining!=null || $daysRemaining!=""){
	   }else{
	       $daysRemaining="";
	   }
	   $this->set(compact('daysRemaining'));
	   
	   $inspectorses = $this->Session->read('Log');
	   $inspector_id = $inspectorses['User']['id'];
	   $this->set('session_inspector_id',$inspector_id);
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
		
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID']))); 
		$this->set('spRes',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientRes',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);
		
		$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		$reqScheduleId = $schedule[0]['Schedule']['id'];
		$this->set('scheduleId',$reqScheduleId);
		$options = array();
		foreach($schedule[0]['ScheduleService'] as $schedule){
			$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			$options[$ServiceOptions['Service']['id']] = $ServiceOptions['Service']['name'];
		}
		$this->set('options',$options);
		
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
	   }
	   if(isset($this->data) && !empty($this->data)){		
		if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				
				$this->loadModel('ReportCert');
				$chkCert = $this->ReportCert->find('count',array('conditions'=>array('ReportCert.schedule_id'=>$reqScheduleId,'ReportCert.ins_cert_form'=>'')));			
				if($chkCert>0){
					$this->Session->setFlash('Please attach all the required certificates');
					$this->redirect($this->referer());
				}
				
				/*Chk Signature*/
				$this->loadModel('Signature');
				$chkSign = $this->Signature->find('count',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
				if($chkSign==0){
					$this->Session->setFlash('Please add your signature first.');
					$this->redirect($this->referer());
				}
				/*Chk Signature*/
				
				$this->data['KitchenhoodReport']['finish_date'] = date('Y-m-d H:i:s');
				$action='Finished the kitchenhood report';
		}
		 if(isset($this->data['KitchenhoodReport']['id']) && !empty($this->data['KitchenhoodReport']['id'])){
			$data['KitchenhoodReportRecord']['kitchenhood_report_id'] = $this->data['KitchenhoodReport']['id'];
			$this->KitchenhoodReport->id=$this->data['KitchenhoodReport']['id'];
			if(!isset($action)){
				$action='Edited the kitchenhood report';
			}
			//$this->data['KitchenhoodReport']['service_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['service_date']));
			//$this->data['KitchenhoodReport']['hydro_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['hydro_date']));
			//$this->data['KitchenhoodReport']['recharge_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['recharge_date']));
			//$this->data['KitchenhoodReport']['k_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['k_date']));
			if($this->KitchenhoodReport->save($this->data['KitchenhoodReport'],false))
		        {
			    if(isset($this->data['KitchenhoodReportRecord']) && !empty($this->data['KitchenhoodReportRecord']))
			    {
				foreach($this->data['KitchenhoodReportRecord'] as $key=>$val)
				{				     
				     $data['KitchenhoodReportRecord']['location'] =  $val['location'];
				     $data['KitchenhoodReportRecord']['type'] =  $val['type'];				     
				     $data['KitchenhoodReportRecord']['born_date'] =  $val['born_date'];
				     $data['KitchenhoodReportRecord']['deficiency'] =  $val['deficiency'];				     
				     $this->KitchenhoodReportRecord->create();
				     $this->KitchenhoodReportRecord->save($data['KitchenhoodReportRecord'],false);
				}
			    }
				
			}
		        
				
		 }
		 else{
			$action='Added the kitchenhood report';
			//$this->data['KitchenhoodReport']['service_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['service_date']));
			//$this->data['KitchenhoodReport']['hydro_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['hydro_date']));
			//$this->data['KitchenhoodReport']['recharge_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['recharge_date']));
			//$this->data['KitchenhoodReport']['k_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['k_date']));
		        if($this->KitchenhoodReport->save($this->data['KitchenhoodReport'],false))
		        {
			    if(isset($this->data['KitchenhoodReportRecord']) && !empty($this->data['KitchenhoodReportRecord']))
			    {
				foreach($this->data['KitchenhoodReportRecord'] as $key=>$val)
				{
				     $data['KitchenhoodReportRecord']['kitchenhood_report_id'] = $this->KitchenhoodReport->id;	
				     $data['KitchenhoodReportRecord']['location'] =  $val['location'];
				     $data['KitchenhoodReportRecord']['type'] =  $val['type'];				     
				     $data['KitchenhoodReportRecord']['born_date'] =  $val['born_date'];
				     $data['KitchenhoodReportRecord']['deficiency'] =  $val['deficiency'];				     
				     $this->KitchenhoodReportRecord->create();
				     $this->KitchenhoodReportRecord->save($data['KitchenhoodReportRecord'],false);
				}
			    }
				
			}
			if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				// this is the function for sending mail after press finish button
				$this->SendMailAfterFinish($this->data['KitchenhoodReport']['client_id'],$this->data['KitchenhoodReport']['report_id'],$this->data['KitchenhoodReport']['finish_date']);
			}
		 }			
		$this->saveLog($this->data['KitchenhoodReport']['sp_id'],$this->data['KitchenhoodReport']['report_id'],$this->data['KitchenhoodReport']['client_id'],$this->data['KitchenhoodReport']['servicecall_id'],$action);
		$this->Session->setFlash('Report has been submitted successfully',true);
		$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
	}
	else{
		$this->data = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'],'KitchenhoodReport.report_id'=>$_REQUEST['reportID'])));
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['KitchenhoodReport']['sp_id'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['KitchenhoodReport']['client_id'])));
		$this->set('clientResult',$clientResult);
		$this->set('spResult',$spResult);
		
		$result=$this->data['KitchenhoodReport'];
		$this->set('result',$result);
		
		$data = $this->data['KitchenhoodReportRecord'];
		$this->set('data',$data);
	}
}

/***************************************************************************************
	#THIS FUNCTION IS USED TO SAVE KITCHENHOOD REPORTS IN SP PANEL 
	Reports controller kitchenhood reportsfunction 
****************************************************************************************/
	function sp_kitchenhood(){
	   $this->layout='sp';
	   $this->loadModel('User');
	   $this->loadModel('Report');
	   $this->loadModel('KitchenhoodReport');
	   $this->loadModel('Attachment');
	   $this->loadModel('Notification');	  
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID']))); 
		$this->set('spRes',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientRes',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);		
		$this->set('reportId',$_REQUEST['reportID']);
		$attachmentdata= $this->Attachment->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
	  }
	   if(isset($this->data) && !empty($this->data)){
		if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				$this->data['KitchenhoodReport']['finish_date'] = date('Y-m-d H:i:s');
		}
		 if(isset($this->data['KitchenhoodReport']['id']) && !empty($this->data['KitchenhoodReport']['id'])){
			$this->KitchenhoodReport->id=$this->data['KitchenhoodReport']['id'];
			$this->data['KitchenhoodReport']['service_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['service_date']));
			$this->data['KitchenhoodReport']['hydro_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['hydro_date']));
			$this->data['KitchenhoodReport']['recharge_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['recharge_date']));
			$this->data['KitchenhoodReport']['k_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['k_date']));
		        $this->KitchenhoodReport->save($this->data['KitchenhoodReport'],false);
		 }
		 else{
			$this->data['KitchenhoodReport']['service_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['service_date']));
			$this->data['KitchenhoodReport']['hydro_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['hydro_date']));
			$this->data['KitchenhoodReport']['recharge_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['recharge_date']));
			$this->data['KitchenhoodReport']['k_date']= date("Y-m-d",strtotime($this->data['KitchenhoodReport']['k_date']));
		        $this->KitchenhoodReport->save($this->data['KitchenhoodReport'],false);
			if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				// this is the function for sending mail after press finish button
				$this->SendMailAfterFinish($this->data['KitchenhoodReport']['client_id'],$this->data['KitchenhoodReport']['report_id'],$this->data['KitchenhoodReport']['finish_date']);
			}
		 }
		        $this->Session->setFlash('Report has been submitted successfully',true);
		        $this->redirect(array('controller'=>'schedules','action'=>'completedschedule'));
	}
	else{
			$this->data = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'],'KitchenhoodReport.report_id'=>$_REQUEST['reportID'])));
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['KitchenhoodReport']['sp_id'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['KitchenhoodReport']['client_id'])));
			$this->set('clientResult',$clientResult);
			$this->set('spResult',$spResult);
	}
}

/***************************************************************************************
	#THIS FUNCTION IS USED FOR kitchen hood view in inspector panel
	Function: inspector_kitchenhoodView() 
****************************************************************************************/ 	
  function inspector_kitchenhoodView($kitchenhood_id=null){
  	$this->layout='';
	$inspectorses = $this->Session->read('Log');
	$inspector_id = $inspectorses['User']['id'];
	$this->loadModel('User');
	$this->loadModel('Schedule');
	$this->loadModel('KitchenhoodReport');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');
	$this->loadModel('Service');
	$this->loadModel('Quote');
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
		
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
		$record = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('record',$record);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		
		// start of script for find the summary page
		$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		$reqScheduleId = $schedule[0]['Schedule']['id'];
		$this->set('schId',$reqScheduleId);
		$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
		// pr($schedule);
		$result = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'],'KitchenhoodReport.report_id'=>$_REQUEST['reportID'])));
		//pr($result);die;
		 foreach($result['KitchenhoodReportRecord'] as $key1=>$record){
			     if($record['deficiency'] == "Yes"){
					$arr1[$record['type']]['fail'][$key1] = 1;
				 }
				 if($record['deficiency'] == "No"){
					$arr1[$record['type']]['pass'][$key1] = 1;
				 }				  
				$arr[$key1] = $record['type'];
			}//pr($arr);die;
		$arr_count = array_count_values($arr);
		$options = array();
		$val = "";
		 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
			$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			$options[$key]['name'] = $ServiceOptions['Service']['name'];
			$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
			$options[$key]['amount'] = $schedule['amount'];			
			if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
				$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
			}else{
				$options[$key]['served'] = 0;
			}
			 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
				 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
					$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
				 }
				 else{
					$options[$key]['pass'] = 0;	
				 }
				if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
					$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
				 }else{
					$options[$key]['fail'] = 0;	
				}	
			 }else{
				$options[$key]['pass'] = 0;	
				$options[$key]['fail'] = 0;	
			 }
		}
		$this->set('options',$options);
		//End of script of sumary page
		
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
									'Code'=> array(
											'className' => 'Code',
											'foreignKey' => 'code_id',
											'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											)
									)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
	}
  }
/***************************************************************************************
	#THIS FUNCTION IS USED AS KITCHEN HOOD REPORT VIEW for sp PANEL 
	Function: sp_kitchenhood() 
****************************************************************************************/ 
  function sp_kitchenhoodView(){
  	$this->layout='';
  	
  	$this->loadModel('User');
  	$this->loadModel('KitchenhoodReport');
  	$this->loadModel('Attachment');
	$this->loadModel('Notification');
  	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
  		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
  		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
  		$this->set('spResult',$spResult);
  		$this->set('spId',$_REQUEST['spID']);
  		$this->set('clientId',$_REQUEST['clientID']);
  		$this->set('clientResult',$clientResult);
  		$this->set('servicecallId',$_REQUEST['serviceCallID']);
  		//his->set('inspector_id',$inspector_id);
  		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
  		$record = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
  		$this->set('kitchenData',$record);
  		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		  #CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
														'Code'=> array(
																	'className' => 'Code',
																	'foreignKey' => 'code_id',
																	'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																	)
														)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
			//pr($Def_record);die;
  		$this->viewPath = 'reports';
	    $this->render('inspector_kitchenhoodView');
	  }
  }
/***************************************************************************************
	#THIS FUNCTION IS USED AS KITCHEN HOOD REPORT VIEW for CLIENT PANEL 
	Function: client_kitchenhood() 
****************************************************************************************/ 
  function client_kitchenhood(){
  	$this->layout='';
  	$inspectorses = $this->Session->read('Log');
  	$inspector_id = $inspectorses['User']['id'];
  	$this->loadModel('User');
  	$this->loadModel('KitchenhoodReport');
  	$this->loadModel('Attachment');
	$this->loadModel('Notification');
  	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
  		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
  		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
  		$this->set('spResult',$spResult);
  		$this->set('spId',$_REQUEST['spID']);
  		$this->set('clientId',$_REQUEST['clientID']);
  		$this->set('clientResult',$clientResult);
  		$this->set('servicecallId',$_REQUEST['serviceCallID']);
  		$this->set('inspector_id',$inspector_id);
  		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
  		$record = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
  		$this->set('kitchenData',$record);
  		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		  #CODE for deficiency page in view
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
									'Code'=> array(
												'className' => 'Code',
												'foreignKey' => 'code_id',
												'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
												)
									)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
  		$this->viewPath = 'reports';
		$this->loadModel('ServiceCall');
	$servicecallResult=$this->ServiceCall->findById($_REQUEST['serviceCallID'],array('ServiceCall.sp_site_address_id'));
	$this->loadModel('SpSiteAddress');
	$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
	$this->set('siteaddrResult',$siteaddrResult);	
	    $this->render('inspector_kitchenhoodView');
	  }
  }

/***************************************************************************************
		 	 function to export KITCHENHOOD REPORT  as PDF
****************************************************************************************/ 	 
	function pdfKitchenhoodReport(){
		$inspectorses = $this->Session->read('Log');			
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('User');
		$this->loadModel('KitchenhoodReport');
		$this->loadModel('Attachment');
		$this->loadModel('Notification');
      	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
      		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
      		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
      		$this->set('spResult',$spResult);
      		$this->set('spId',$_REQUEST['spID']);
      		$this->set('clientId',$_REQUEST['clientID']);
      		$this->set('clientResult',$clientResult);
      		$this->set('servicecallId',$_REQUEST['serviceCallID']);
      		$this->set('inspector_id',$inspector_id);
      		$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
      		$record = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
      		$this->set('kitchenData',$record);
      		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
			#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
														'Code'=> array(
																	'className' => 'Code',
																	'foreignKey' => 'code_id',
																	'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																	)
														)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
      		
        }
	}  
/***************************************************************************************
	#THIS FUNCTION IS USED FOR EMERGENCYEXIT REPORTS to save emergency exit report in inspector panel
	Reports controller emergencyexit reportsfunction 
****************************************************************************************/ 		
	function inspector_emergencyexit(){
	   $this->layout='inspector';	   
	   $this->loadModel('User');
	   $this->loadModel('EmergencyexitReport');
	   $this->loadModel('EmergencyexitReportRecord');
	   $this->loadModel('Attachment');
	   $this->loadModel('Notification');
	   $this->loadModel('ScheduleService');
	   $this->loadModel('Schedule');
	   $this->loadModel('Report');
	   $this->loadModel('Service');
	   
		/*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/
	   
	   $daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
	   $daysRemaining = $daysData[0]['days'];
	   if($daysRemaining!=null || $daysRemaining!=""){
	   }else{
	       $daysRemaining="";
	   }
 	   $this->set(compact('daysRemaining'));
	   
	   $inspectorses = $this->Session->read('Log');
	   $inspector_id = $inspectorses['User']['id'];
	    $this->set('session_inspector_id',$inspector_id);
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
		
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);	
		$attachmentdata= $this->Attachment->find('all',array('conditions'=>array('Attachment.user_id'=>$inspector_id,'Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.user_id'=>$inspector_id,'Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		$Result = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		$reqScheduleId = $Result[0]['Schedule']['id'];
		 $option1 = array();
		 $option2 = array();
		 
		#EXIT-SIGN SERVICE ID=75
		#Emergency Light SERVICE ID=76
		#FIND COUNT FOR EXIT-SIGN and EMERGENCY LIGHT SERVICE TAKEN BY CLIENT
		foreach($Result as $Res):
		$exit_sign=0;
		$emergency=0;
		foreach($Res['ScheduleService'] as $sch_services):
		if($sch_services['service_id']==75){
			$optionName = $this->Service->find('first',array('conditions'=>array('Service.id'=>$sch_services['service_id']),'fields'=>'Service.name'));
			$option1[$sch_services['service_id']] = $optionName['Service']['name'];
			$this->set('exit_amount',$sch_services['amount']);
			
		}else{
			$this->set('exit_amount',0);
		}
		if($sch_services['service_id']==76){
			$optionName = $this->Service->find('first',array('conditions'=>array('Service.id'=>$sch_services['service_id']),'fields'=>'Service.name'));
			$option2[$sch_services['service_id']] = $optionName['Service']['name'];
			$this->set('emergency_amount',$sch_services['amount']);			
		}else{
			$this->set('emergency_amount',0);
		}
		$this->set('option2',$option2);
		endforeach;
		endforeach;
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('option1',$option1);
	    }
	    if(isset($this->data) && !empty($this->data))
	    {
			//pr($this->data); die;
			if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
					
				$this->loadModel('ReportCert');
				$chkCert = $this->ReportCert->find('count',array('conditions'=>array('ReportCert.schedule_id'=>$reqScheduleId,'ReportCert.ins_cert_form'=>'')));			
				if($chkCert>0){
					$this->Session->setFlash('Please attach all the required certificates');
					$this->redirect($this->referer());
				}
				
				/*Chk Signature*/
				$this->loadModel('Signature');
				$chkSign = $this->Signature->find('count',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
				if($chkSign==0){
					$this->Session->setFlash('Please add your signature first.');
					$this->redirect($this->referer());
				}
				/*Chk Signature*/
					
				$this->data['EmergencyexitReport']['finish_date'] = date('Y-m-d H:i:s');
				$action="Finished the emergency exit report";
			}
			if(isset($this->data['EmergencyexitReport']['id']) && !empty($this->data['EmergencyexitReport']['id'])){
			   if($this->EmergencyexitReport->save($this->data['EmergencyexitReport'],false))
			   {
					$data['EmergencyexitReportRecord']['emergencyexit_report_id'] = $this->data['EmergencyexitReport']['id'];
					$datas['EmergencyexitReportRecord']['emergencyexit_report_id'] = $this->data['EmergencyexitReport']['id'];
					$this->EmergencyexitReport->id=$this->data['EmergencyexitReport']['id'];
					if(!empty($this->data['EmergencyexitReportRecord'])){
				 foreach($this->data['EmergencyexitReportRecord'] as $key=>$val)
				 {
					$data['EmergencyexitReportRecord']['location'] = $val['location'];
					$data['EmergencyexitReportRecord']['mfd_date'] = $val['mfd_date'];
					$data['EmergencyexitReportRecord']['type'] = $val['type'];
					$data['EmergencyexitReportRecord']['status'] = $val['status'];
					$data['EmergencyexitReportRecord']['comment'] = $val['comment'];
					$this->EmergencyexitReportRecord->create();
					$this->EmergencyexitReportRecord->save($data['EmergencyexitReportRecord'],false);
				 }
					}
				if(isset($this->data['EmergencyexitReportsRecord']) && !empty($this->data['EmergencyexitReportsRecord']))
				{
					foreach($this->data['EmergencyexitReportsRecord'] as $keys=>$vals){
						   $datas['EmergencyexitReportRecord']['location'] = $vals['location'];
						   $datas['EmergencyexitReportRecord']['mfd_date'] = $vals['mfd_date'];
						   $datas['EmergencyexitReportRecord']['type'] = $vals['type'];
						   $datas['EmergencyexitReportRecord']['status'] = $vals['status'];
						   $datas['EmergencyexitReportRecord']['comment'] = $vals['comment'];
						   $this->EmergencyexitReportRecord->create();
						   $this->EmergencyexitReportRecord->save($datas['EmergencyexitReportRecord'],false);
					}
				}
					if(!isset($action)){
						$action='Edited the emergency exit report';
					}
					$this->saveLog($this->data['EmergencyexitReport']['sp_id'],$this->data['EmergencyexitReport']['report_id'],$this->data['EmergencyexitReport']['client_id'],$this->data['EmergencyexitReport']['servicecall_id'],$action);
					$this->Session->setFlash('New record has been saved successfully.');
					$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
			   }
			}
			else {
			   if($this->EmergencyexitReport->save($this->data['EmergencyexitReport'],false)){
					$lastInsertId = $this->EmergencyexitReport->getLastInsertId();
					$data['EmergencyexitReportRecord']['emergencyexit_report_id'] = $lastInsertId;
					$datas['EmergencyexitReportRecord']['emergencyexit_report_id'] = $lastInsertId;
				 foreach($this->data['EmergencyexitReportRecord'] as $key=>$val){
					$data['EmergencyexitReportRecord']['location'] = $val['location'];
					$data['EmergencyexitReportRecord']['mfd_date'] = $val['mfd_date'];
					$data['EmergencyexitReportRecord']['type'] = $val['type'];
					$data['EmergencyexitReportRecord']['status'] = $val['status'];
					$data['EmergencyexitReportRecord']['comment'] = $val['comment'];
					$this->EmergencyexitReportRecord->create();
					$this->EmergencyexitReportRecord->save($data['EmergencyexitReportRecord'],false);
					$action='Edited the emergency exit report';
					$this->saveLog($this->data['EmergencyexitReport']['sp_id'],$this->data['EmergencyexitReport']['report_id'],$this->data['EmergencyexitReport']['client_id'],$this->data['EmergencyexitReport']['servicecall_id'],$action);
				 }
				 if(isset($this->data['EmergencyexitReportsRecord']) && !empty($this->data['EmergencyexitReportsRecord']))
				 {
					foreach($this->data['EmergencyexitReportsRecord'] as $keys=>$vals){
						   $datas['EmergencyexitReportRecord']['location'] = $vals['location'];
						   $datas['EmergencyexitReportRecord']['mfd_date'] = $vals['mfd_date'];
						   $datas['EmergencyexitReportRecord']['type'] = $vals['type'];
						   $datas['EmergencyexitReportRecord']['status'] = $vals['status'];
						   $datas['EmergencyexitReportRecord']['comment'] = $vals['comment'];
						   $this->EmergencyexitReportRecord->create();
						   $this->EmergencyexitReportRecord->save($datas['EmergencyexitReportRecord'],false);
					}
				 }
					if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
					// this is the function for sending mail after press finish button
					$this->SendMailAfterFinish($this->data['EmergencyexitReport']['client_id'],$this->data['EmergencyexitReport']['report_id'],$this->data['EmergencyexitReport']['finish_date']);
					}
					$this->Session->setFlash('New record has been saved successfully.');
					$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
			   }
			}
	    }else{
				$result = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
				$this->set('result',$result);
				$this->set('data',$result['EmergencyexitReportRecord']);
	    }
	}	
/***************************************************************************************
	#THIS FUNCTION IS USED FOR EMERGENCYEXIT REPORTS FOR THE SP PANEL
	Reports controller emergencyexit reportsfunction 
****************************************************************************************/ 		
	function sp_emergencyexit(){ 
	   $this->loadModel('User');
	   $this->loadModel('EmergencyexitReport');
	   $this->loadModel('EmergencyexitReportRecord');
	   $this->loadModel('Attachment');
	   $this->loadModel('Notification');
	   $this->loadModel('ScheduleService');
	   $this->loadModel('Schedule');
	   $inspectorses = $this->Session->read('Log');
	   $inspector_id = $inspectorses['User']['id'];
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);	
		$attachmentdata= $this->Attachment->find('all',array('conditions'=>array('Attachment.user_id'=>$inspector_id,'Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.user_id'=>$inspector_id,'Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		$Result = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		//$this->set('result',$Result);
		#EXIT-SIGN SERVICE ID=75
		#Emergency Light SERVICE ID=76
		#FIND COUNT FOR EXIT-SIGN and EMERGENCY LIGHT SERVICE TAKEN BY CLIENT
		foreach($Result as $Res):
		$exit_sign=0;
		$emergency=0;
		foreach($Res['ScheduleService'] as $sch_services):
		if($sch_services['service_id']==75){
			$this->set('exit_amount',$sch_services['amount']);
		}
		if($sch_services['service_id']==76){
			$this->set('emergency_amount',$sch_services['amount']);
		}
		endforeach;
		endforeach;
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);
	    }
	    if(isset($this->data) && !empty($this->data))
	    {
		if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				$this->data['EmergencyexitReport']['finish_date'] = date('Y-m-d H:i:s');
		}
		if(isset($this->data['EmergencyexitReport']['id']) && !empty($this->data['EmergencyexitReport']['id'])){
		   if($this->EmergencyexitReport->save($this->data['EmergencyexitReport'],false))
		   {
				$data['EmergencyexitReportRecord']['emergencyexit_report_id'] = $this->data['EmergencyexitReport']['id'];
				$datas['EmergencyexitReportRecord']['emergencyexit_report_id'] = $this->data['EmergencyexitReport']['id'];
				$this->EmergencyexitReport->id=$this->data['EmergencyexitReport']['id'];
			 foreach($this->data['EmergencyexitReportsRecord'] as $key=>$val)
			 {
				$data['EmergencyexitReportRecord']['location'] = $val['location'];
				$data['EmergencyexitReportRecord']['mfd_date'] = $val['mfd_date'];
				$data['EmergencyexitReportRecord']['type'] = $val['type'];
				$data['EmergencyexitReportRecord']['status'] = $val['status'];
				$data['EmergencyexitReportRecord']['comment'] = $val['comment'];
				$this->EmergencyexitReportRecord->create();
				$this->EmergencyexitReportRecord->save($data['EmergencyexitReportRecord'],false);
			 }
			if(isset($this->data['EmergencyexitReportsRecord']) && !empty($this->data['EmergencyexitReportsRecord']))
			{
				foreach($this->data['EmergencyexitReportsRecord'] as $keys=>$vals){
				       $datas['EmergencyexitReportRecord']['location'] = $vals['location'];
				       $datas['EmergencyexitReportRecord']['mfd_date'] = $vals['mfd_date'];
				       $datas['EmergencyexitReportRecord']['type'] = $vals['type'];
				       $datas['EmergencyexitReportRecord']['status'] = $vals['status'];
				       $datas['EmergencyexitReportRecord']['comment'] = $vals['comment'];
				       $this->EmergencyexitReportRecord->create();
				       $this->EmergencyexitReportRecord->save($datas['EmergencyexitReportRecord'],false);
				}
			}
				$this->Session->setFlash('New record has been saved successfully.');
				$this->redirect(array('controller'=>'schedules','action'=>'completedschedule'));
		   }
		   
		}
		else{
		   if($this->EmergencyexitReport->save($this->data['EmergencyexitReport'],false)){
				$lastInsertId = $this->EmergencyexitReport->getLastInsertId();
				$data['EmergencyexitReportRecord']['emergencyexit_report_id'] = $lastInsertId;
				$datas['EmergencyexitReportRecord']['emergencyexit_report_id'] = $lastInsertId;
			 foreach($this->data['EmergencyexitReportRecord'] as $key=>$val){
				$data['EmergencyexitReportRecord']['location'] = $val['location'];
				$data['EmergencyexitReportRecord']['mfd_date'] = $val['mfd_date'];
				$data['EmergencyexitReportRecord']['type'] = $val['type'];
				$data['EmergencyexitReportRecord']['status'] = $val['status'];
				$data['EmergencyexitReportRecord']['comment'] = $val['comment'];
				$this->EmergencyexitReportRecord->create();
				$this->EmergencyexitReportRecord->save($data['EmergencyexitReportRecord'],false);
			 }
			 if(isset($this->data['EmergencyexitReportsRecord']) && !empty($this->data['EmergencyexitReportsRecord']))
			 {
				foreach($this->data['EmergencyexitReportsRecord'] as $keys=>$vals){
				       $datas['EmergencyexitReportRecord']['location'] = $vals['location'];
				       $datas['EmergencyexitReportRecord']['mfd_date'] = $vals['mfd_date'];
				       $datas['EmergencyexitReportRecord']['type'] = $vals['type'];
				       $datas['EmergencyexitReportRecord']['status'] = $vals['status'];
				       $datas['EmergencyexitReportRecord']['comment'] = $vals['comment'];
				       $this->EmergencyexitReportRecord->create();
				       $this->EmergencyexitReportRecord->save($datas['EmergencyexitReportRecord'],false);
				}
			 }
				$this->Session->setFlash('New record has been saved successfully.');
				$this->redirect(array('controller'=>'schedules','action'=>'completedschedule'));
		   }
		}
	    }else{
				$result = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
				// pr($result);
				$this->set('result',$result);
				$this->set('data',$result['EmergencyexitReportRecord']);
	    }
	}
 /***************************************************************************************
	#THIS FUNCTION IS USED For REVIEW FUNCTIONALITY in EMERGENCYEXIT REPORTS IN INSPECTOR PANEL
	Reports controller EMERGENCYEXIT VIEW  for INSPECTOR PANEL
****************************************************************************************/
	function inspector_emergencyexitView(){
	$this->layout='';
  	$inspectorses = $this->Session->read('Log');
  	$inspector_id = $inspectorses['User']['id'];
  	$this->loadModel('User');
  	$this->loadModel('EmergencyexitReport');
	$this->loadModel('EmergencyexitReportRecord');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');
	$this->loadModel('Schedule');
	$this->loadModel('Service');
  	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
  		
  		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
  		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
  		$this->set('spResult',$spResult);
  		$this->set('spId',$_REQUEST['spID']);
  		$this->set('clientId',$_REQUEST['clientID']);
  		$this->set('clientResult',$clientResult);
  		$this->set('servicecallId',$_REQUEST['serviceCallID']);
  		$this->set('inspector_id',$inspector_id);
  		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
			
  		$result = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
  		$this->set('record',$result);
		// pr($result);
		$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array()));
		$reqScheduleId = $schedule[0]['Schedule']['id'];
		$this->set('schId',$reqScheduleId);
		$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
		 //pr($schedule);
		 //die();
		 
		foreach($result['EmergencyexitReportRecord'] as $key1=>$record){
			     if($record['status'] == "Pass"){
					$arr1[$record['type']]['fail'][$key1] = 1;
				 }
				 if($record['status'] == "Fail"){
					$arr1[$record['type']]['pass'][$key1] = 1;
				 }				  
					$arr[$key1] = $record['type'];
			}
		$arr_count = array_count_values($arr);
		 //pr($arr_count);
		// die();
		$options = array();
		 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
			$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			$options[$key]['name'] = $ServiceOptions['Service']['name'];
			$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
			$options[$key]['amount'] = $schedule['amount'];
		    $options[$key]['amount'] = $schedule['amount'];
			if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
				$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
			}else{
				$options[$key]['served'] = 0;
			}
			 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
				 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
					$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
				 }
				 else{
					$options[$key]['pass'] = 0;	
				 }
				if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
					$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
				 }else{
					$options[$key]['fail'] = 0;	
				}	
			 }else{
					$options[$key]['pass'] = 0;	
					$options[$key]['fail'] = 0;	
			 }
		}
		$this->set('options',$options);
  		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		#CODE for deficiency page in view
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
									'Code'=> array(
												'className' => 'Code',
												'foreignKey' => 'code_id',
												'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
												)
									)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
  	}
}	
/***************************************************************************************
	#THIS FUNCTION IS USED FOR EMERGENCYEXIT REPORTS TO REVIEW REPORT IN SP PANEL
	Reports controller EMERGENCYEXIT VIEW  reportsfunction 
****************************************************************************************/
function sp_emergencyexitView(){
	$this->layout='';
  	//$inspectorses = $this->Session->read('Log');
  	//$inspector_id = $inspectorses['User']['id'];
  	$this->loadModel('User');
  	$this->loadModel('EmergencyexitReport');
	$this->loadModel('EmergencyexitReportRecord');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');   
  	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
		
  		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
  		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
  		$this->set('spResult',$spResult);
  		$this->set('spId',$_REQUEST['spID']);
  		$this->set('clientId',$_REQUEST['clientID']);
  		$this->set('clientResult',$clientResult);
  		$this->set('servicecallId',$_REQUEST['serviceCallID']);
  		//$this->set('inspector_id',$inspector_id);
  		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
			
  		$record = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
  		$this->set('record',$record);
  		
  		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		#CODE for deficiency page in view
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
									'Code'=> array(
												'className' => 'Code',
												'foreignKey' => 'code_id',
												'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
												)
									)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);

					//pr($Def_record);die;
  	}
	$this->viewPath = 'reports';
	$this->render('inspector_emergencyexitView');
  }	

/***************************************************************************************
	#THIS FUNCTION IS USED AS EMERGENCY EXIT VIEW for CLIENT PANEL 
	Function: client_emergencyexit() 
****************************************************************************************/ 	
    function client_emergencyexit(){  
  	$this->layout='';
  	$this->loadModel('User');
  	$this->loadModel('EmergencyexitReport');
	$this->loadModel('EmergencyexitReportRecord');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');   
  	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
  		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
  		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
  		$this->set('spResult',$spResult);
  		$this->set('spId',$_REQUEST['spID']);
  		$this->set('clientId',$_REQUEST['clientID']);
  		$this->set('clientResult',$clientResult);
  		$this->set('servicecallId',$_REQUEST['serviceCallID']);
  		//$this->set('inspector_id',$inspector_id);
  		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
			
  		$record = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
  		$this->set('record',$record);
  		
  		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		#CODE for deficiency page in view
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
									'Code'=> array(
												'className' => 'Code',
												'foreignKey' => 'code_id',
												'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
												)
									)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);

  	}
	$this->viewPath = 'reports';
	$this->render('inspector_emergencyexitView');
  }

/***************************************************************************************
	function to export Emergency REPORT  as PDF
****************************************************************************************/ 	   
     function pdfEmergencyReport(){
	$this->layout='';
  	$inspectorses = $this->Session->read('Log');
  	$inspector_id = $inspectorses['User']['id'];
  	$this->loadModel('User');
  	$this->loadModel('EmergencyexitReport');
	$this->loadModel('EmergencyexitReportRecord');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');   
  	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
  		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
  		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
  		$this->set('spResult',$spResult);
  		$this->set('spId',$_REQUEST['spID']);
  		$this->set('clientId',$_REQUEST['clientID']);
  		$this->set('clientResult',$clientResult);
  		$this->set('servicecallId',$_REQUEST['serviceCallID']);
  		$this->set('inspector_id',$inspector_id);
  		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
  		$record = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
  		$this->set('record',$record);		
  		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
														'Code'=> array(
																	'className' => 'Code',
																	'foreignKey' => 'code_id',
																	'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																	)
														)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
	}
    }  
/***************************************************************************************
	function to SAVE SPECIAL HAZARD REPORT IN INSPECTOR PANEL
****************************************************************************************/ 	   
#SPECIAL HAZARD REPORT START ON 26 JUNE,2012

function inspector_specialhazard()
{
    $this->layout='inspector';
	$this->loadModel('User');
	$this->loadModel('SpecialhazardReport');
	$this->loadModel('SpecialhazardReportRecord');
	/*$this->loadModel('SpecialhazardAlarmIniDevice');
	$this->loadModel('SpecialhazardAlarmIndDevice');
	$this->loadModel('SpecialhazardPowerSystem');
	$this->loadModel('SpecialhazardSupIniDevice');
	$this->loadModel('SpecialhazardControlUnit');	
	$this->loadModel('SpecialhazardGasDevice');*/
	$this->loadModel('ScheduleService');	
	$this->loadModel('Schedule');
	$this->loadModel('Service');	
	/*$this->loadModel('Attachment');
	$this->loadModel('Notification');*/
	
	/*To Fetch and set Report Text*/
	$this->loadModel('SpReportText');
	$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
	$this->set('texts',nl2br($texts['SpReportText']['report_text']));
	/*To Fetch and set Report Text*/
		
	$daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
	$daysRemaining = $daysData[0]['days'];
	if($daysRemaining!=null || $daysRemaining!=""){
	}else{
	    $daysRemaining="";
	}
	$this->set(compact('daysRemaining'));	
	
	$inspectorses = $this->Session->read('Log');
	$inspector_id = $inspectorses['User']['id'];
	$session_inspector_id = $inspectorses['User']['id'];
	$this->set('session_inspector_id',$session_inspector_id);
	/*$specialHazardarr = Configure::read('specialHazardarr');
	$this->set('specialHazardarr',$specialHazardarr);
	$controlUnit = Configure::read('controlUnit');
	$this->set('controlUnit',$controlUnit);*/
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
	        #code on 4 oct,2012 for Site address display
	     $this->loadModel('ServiceCall');
	     $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
	     $this->loadModel('SpSiteAddress');
	     $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
	     $this->set('siteaddrResult',$siteaddrResult); 
		#end code
		
	     $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
	     $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
	     $this->set('spResult',$spResult);
	     $this->set('spId',$_REQUEST['spID']);
	     $this->set('clientId',$_REQUEST['clientID']);
	     $this->set('clientResult',$clientResult);
	     $this->set('servicecallId',$_REQUEST['serviceCallID']);
	     $this->set('inspector_id',$inspector_id);
	     $this->set('reportId',$_REQUEST['reportID']);
	     
	     $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
	     $reqScheduleId = $schedule[0]['Schedule']['id'];
	     $this->set('scheduleId',$reqScheduleId);
	     $options = array();
		foreach($schedule[0]['ScheduleService'] as $schedule){
			$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			$options[$ServiceOptions['Service']['id']] = $ServiceOptions['Service']['name'];
		}
		$this->set('options',$options);	
		
	     /*$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
	     $this->set('attachmentdata',$attachmentdata);
	     $notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
	     $this->set('notifierdata',$notifierdata);*/
	}
	 if(isset($this->data) && !empty($this->data)){
		if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				
				$this->loadModel('ReportCert');
				$chkCert = $this->ReportCert->find('count',array('conditions'=>array('ReportCert.schedule_id'=>$reqScheduleId,'ReportCert.ins_cert_form'=>'')));			
				if($chkCert>0){
					$this->Session->setFlash('Please attach all the required certificates');
					$this->redirect($this->referer());
				}
				
				/*Chk Signature*/
				$this->loadModel('Signature');
				$chkSign = $this->Signature->find('count',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
				if($chkSign==0){
					$this->Session->setFlash('Please add your signature first.');
					$this->redirect($this->referer());
				}
				/*Chk Signature*/
				
				$this->data['SpecialhazardReport']['finish_date'] = date('Y-m-d H:i:s');
				$action='Finished the special hazard report';
		}
		 if(isset($this->data['SpecialhazardReport']['id']) && !empty($this->data['SpecialhazardReport']['id'])){
			/*if(isset($this->data['SpecialhazardReport']['system_types']) && !empty($this->data['SpecialhazardReport']['system_types']))
			{
				foreach($this->data['SpecialhazardReport']['system_types'] as $key=>$val){
				if($val != "0"){
				   $arr[$val] = $val;
				}
			    }
			    $this->data['SpecialhazardReport']['system_types'] = implode(',',$arr); 
			}*/
			$data['SpecialhazardReportRecord']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
			$this->SpecialhazardReport->id = $this->data['SpecialhazardReport']['id'];
			if(!isset($action)){
				$action='Edited the specialhazard report';
			}
			$this->SpecialhazardReport->id = $this->data['SpecialhazardReport']['id'];
			if($this->SpecialhazardReport->save($this->data['SpecialhazardReport'],false))
		        {
			    if(isset($this->data['SpecialhazardReportRecord']) && !empty($this->data['SpecialhazardReportRecord']))
			    {
				foreach($this->data['SpecialhazardReportRecord'] as $key=>$val)
				{				     
				     $data['SpecialhazardReportRecord']['location'] =  $val['location'];
				     $data['SpecialhazardReportRecord']['type'] =  $val['type'];				     
				     $data['SpecialhazardReportRecord']['born_date'] =  $val['born_date'];
				     $data['SpecialhazardReportRecord']['deficiency'] =  $val['deficiency'];				     
				     $this->SpecialhazardReportRecord->create();
				     $this->SpecialhazardReportRecord->save($data['SpecialhazardReportRecord'],false);
				}
			    }
				
			}			
			/*if(isset($this->data['SpecialhazardAlarmIniDevice']['id']) && ($this->data['SpecialhazardAlarmIniDevice']['id'] != "")){
				$this->SpecialhazardAlarmIniDevice->id = $this->data['SpecialhazardAlarmIniDevice']['id'];
				$this->data['SpecialhazardAlarmIniDevice']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardAlarmIniDevice->save($this->data['SpecialhazardAlarmIniDevice'],false);
			}
			if(isset($this->data['SpecialhazardAlarmIndDevice']['id']) && ($this->data['SpecialhazardAlarmIndDevice']['id'] != "")){
				$this->SpecialhazardAlarmIndDevice->id = $this->data['SpecialhazardAlarmIndDevice']['id'];
				$this->data['SpecialhazardAlarmIndDevice']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardAlarmIndDevice->save($this->data['SpecialhazardAlarmIndDevice'],false);
			}
			if(isset($this->data['SpecialhazardPowerSystem']['id']) && ($this->data['SpecialhazardPowerSystem']['id'] != "")){
				$this->data['SpecialhazardPowerSystem']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardPowerSystem->save($this->data['SpecialhazardPowerSystem'],false);
			}
			if(isset($this->data['SpecialhazardSupIniDevice']['id']) && ($this->data['SpecialhazardSupIniDevice']['id'] != "")){
				$this->data['SpecialhazardSupIniDevice']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardSupIniDevice->save($this->data['SpecialhazardSupIniDevice'],false);
			}
			if(isset($this->data['SpecialhazardControlUnit']['id']) && ($this->data['SpecialhazardControlUnit']['id'] != "")){
				foreach($this->data['SpecialhazardControlUnit']['controlunit_type'] as $key1=>$val1){
					if($val1 != "0"){
					  $arr1[$val1] = $val1;
					}
				}
				$this->data['SpecialhazardControlUnit']['controlunit_type'] = implode(",",$arr1);
				$this->data['SpecialhazardControlUnit']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardControlUnit->save($this->data['SpecialhazardControlUnit'],false);
			}
			if(isset($this->data['SpecialhazardGasDevice']['id']) && ($this->data['SpecialhazardGasDevice']['id'] != "")){
				$this->data['SpecialhazardGasDevice']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardGasDevice->save($this->data['SpecialhazardGasDevice'],false);
			}
			if(!isset($action)){
				$action='Edited the special hazard report';
			}*/
		 }else{
			$action='Added the specialhazard report';
			 /*if(isset($this->data['SpecialhazardReport']['system_types']) && !empty($this->data['SpecialhazardReport']['system_types']))
			 {
				foreach($this->data['SpecialhazardReport']['system_types'] as $key=>$val){
				if($val != "0"){
				   $arr[$val] = $val;
				}
			    }	
			 }
			$this->data['SpecialhazardReport']['system_types'] = implode(',',$arr);*/			
			if($this->SpecialhazardReport->save($this->data['SpecialhazardReport'],false))
		        {
			    if(isset($this->data['SpecialhazardReportRecord']) && !empty($this->data['SpecialhazardReportRecord']))
			    {
				foreach($this->data['SpecialhazardReportRecord'] as $key=>$val)
				{
				     $data['SpecialhazardReportRecord']['specialhazard_report_id'] = $this->SpecialhazardReport->id;	
				     $data['SpecialhazardReportRecord']['location'] =  $val['location'];
				     $data['SpecialhazardReportRecord']['type'] =  $val['type'];				     
				     $data['SpecialhazardReportRecord']['born_date'] =  $val['born_date'];
				     $data['SpecialhazardReportRecord']['deficiency'] =  $val['deficiency'];				     
				     $this->SpecialhazardReportRecord->create();
				     $this->SpecialhazardReportRecord->save($data['SpecialhazardReportRecord'],false);
				}
			    }
				
			}
			/*$lastInsertId = $this->SpecialhazardReport->getLastInsertId();
			if(isset($this->data['SpecialhazardAlarmIniDevice']) && ($this->data['SpecialhazardAlarmIniDevice'] != "")){
				$this->data['SpecialhazardAlarmIniDevice']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardAlarmIniDevice->save($this->data['SpecialhazardAlarmIniDevice'],false);
			}
			if(isset($this->data['SpecialhazardAlarmIndDevice']) && ($this->data['SpecialhazardAlarmIndDevice'] != "")){
				$this->data['SpecialhazardAlarmIndDevice']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardAlarmIndDevice->save($this->data['SpecialhazardAlarmIndDevice'],false);
			}
			if(isset($this->data['SpecialhazardPowerSystem']) && ($this->data['SpecialhazardPowerSystem'] != "")){
				$this->data['SpecialhazardPowerSystem']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardPowerSystem->save($this->data['SpecialhazardPowerSystem'],false);
			}
			if(isset($this->data['SpecialhazardSupIniDevice']) && ($this->data['SpecialhazardSupIniDevice'] != "")){
				$this->data['SpecialhazardSupIniDevice']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardSupIniDevice->save($this->data['SpecialhazardSupIniDevice'],false);
			}
			if(isset($this->data['SpecialhazardControlUnit']) && ($this->data['SpecialhazardControlUnit'] != "")){
				if(isset($this->data['SpecialhazardControlUnit']['controlunit_type']) && !empty($this->data['SpecialhazardControlUnit']['controlunit_type'])){
					foreach($this->data['SpecialhazardControlUnit']['controlunit_type'] as $key1=>$val1){
					if($val1 != "0"){
					  $arr1[$val1] = $val1;
					}
				  }	
				}
				$this->data['SpecialhazardControlUnit']['controlunit_type'] = implode(",",$arr1);
				$this->data['SpecialhazardControlUnit']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardControlUnit->save($this->data['SpecialhazardControlUnit'],false);
			}
			if(isset($this->data['SpecialhazardGasDevice']) && ($this->data['SpecialhazardGasDevice'] != "")){
				$this->data['SpecialhazardGasDevice']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardGasDevice->save($this->data['SpecialhazardGasDevice'],false);
			}*/
			if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				// this is the function for sending mail after press finish button
				$this->SendMailAfterFinish($this->data['SpecialhazardReport']['client_id'],$this->data['SpecialhazardReport']['report_id'],$this->data['SpecialhazardReport']['finish_date']);
			}			
		 }
			$this->saveLog($this->data['SpecialhazardReport']['sp_id'],$this->data['SpecialhazardReport']['report_id'],$this->data['SpecialhazardReport']['client_id'],$this->data['SpecialhazardReport']['servicecall_id'],$action);
			$this->Session->setFlash('Report has been saved successfully.');
			$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
	 }
	 else {
			$this->data = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
			/*$result = explode(",",$this->data['SpecialhazardReport']['system_types']);
			$this->data['SpecialhazardReport']['system_types'] = "";
			foreach($result as $res1){
			  $this->data['SpecialhazardReport']['system_types'][intval(trim($res1))] = intval(trim($res1));
			}
			
			$result1 = explode(",",@$this->data['SpecialhazardControlUnit']['controlunit_type']);
			$this->data['SpecialhazardControlUnit']['controlunit_type'] = "";
			foreach($result1 as $res){
			  $this->data['SpecialhazardControlUnit']['controlunit_type'][intval(trim($res))] = intval(trim($res));
			}*/
			$result=$this->data['SpecialhazardReport'];
			$this->set('result',$result);
			
			$data = $this->data['SpecialhazardReportRecord'];
			$this->set('data',$data);
	 }
}

/***************************************************************************************
	function to SAVE SPECIAL HAZARD REPORT IN SP PANEL
****************************************************************************************/ 	   
function sp_specialhazard()
{
    $this->layout='sp';
	$this->loadModel('User');
	$this->loadModel('SpecialhazardReport');
	$this->loadModel('SpecialhazardAlarmIniDevice');
	$this->loadModel('SpecialhazardAlarmIndDevice');
	$this->loadModel('SpecialhazardPowerSystem');
	$this->loadModel('SpecialhazardSupIniDevice');
	$this->loadModel('SpecialhazardControlUnit');
	$this->loadModel('SpecialhazardReport');
	$this->loadModel('SpecialhazardGasDevice');
	$this->loadModel('ScheduleService');	
	$this->loadModel('Schedule');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');
	$inspectorses = $this->Session->read('Log');
	$inspector_id = $inspectorses['User']['id'];
	$specialHazardarr = Configure::read('specialHazardarr');
	$this->set('specialHazardarr',$specialHazardarr);
	$controlUnit = Configure::read('controlUnit');
	$this->set('controlUnit',$controlUnit);
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
	     $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
	     $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
	     $this->set('spResult',$spResult);
	     $this->set('spId',$_REQUEST['spID']);
	     $this->set('clientId',$_REQUEST['clientID']);
	     $this->set('clientResult',$clientResult);
	     $this->set('servicecallId',$_REQUEST['serviceCallID']);
	     $this->set('inspector_id',$inspector_id);
	     $this->set('reportId',$_REQUEST['reportID']);
	     $attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
	     $this->set('attachmentdata',$attachmentdata);
	     $notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
	     $this->set('notifierdata',$notifierdata);
	}
	 if(isset($this->data) && !empty($this->data)){
		if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				$this->data['SpecialhazardReport']['finish_date'] = date('Y-m-d H:i:s');
		}
		 if(isset($this->data['SpecialhazardReport']['id']) && !empty($this->data['SpecialhazardReport']['id'])){
			if(isset($this->data['SpecialhazardReport']['system_types']) && !empty($this->data['SpecialhazardReport']['system_types']))
			{
				foreach($this->data['SpecialhazardReport']['system_types'] as $key=>$val){
				if($val != "0"){
				   $arr[$val] = $val;
				}
			    }
			    $this->data['SpecialhazardReport']['system_types'] = implode(',',$arr); 
			}
			$this->SpecialhazardReport->id = $this->data['SpecialhazardReport']['id'];
			
			$this->SpecialhazardReport->save($this->data['SpecialhazardReport'],false);
			if(isset($this->data['SpecialhazardAlarmIniDevice']['id']) && ($this->data['SpecialhazardAlarmIniDevice']['id'] != "")){
				$this->SpecialhazardAlarmIniDevice->id = $this->data['SpecialhazardAlarmIniDevice']['id'];
				$this->data['SpecialhazardAlarmIniDevice']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardAlarmIniDevice->save($this->data['SpecialhazardAlarmIniDevice'],false);
			}
			if(isset($this->data['SpecialhazardAlarmIndDevice']['id']) && ($this->data['SpecialhazardAlarmIndDevice']['id'] != "")){
				$this->SpecialhazardAlarmIndDevice->id = $this->data['SpecialhazardAlarmIndDevice']['id'];
				$this->data['SpecialhazardAlarmIndDevice']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardAlarmIndDevice->save($this->data['SpecialhazardAlarmIndDevice'],false);
			}
			if(isset($this->data['SpecialhazardPowerSystem']['id']) && ($this->data['SpecialhazardPowerSystem']['id'] != "")){
				$this->data['SpecialhazardPowerSystem']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardPowerSystem->save($this->data['SpecialhazardPowerSystem'],false);
			}
			if(isset($this->data['SpecialhazardSupIniDevice']['id']) && ($this->data['SpecialhazardSupIniDevice']['id'] != "")){
				$this->data['SpecialhazardSupIniDevice']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardSupIniDevice->save($this->data['SpecialhazardSupIniDevice'],false);
			}
			if(isset($this->data['SpecialhazardControlUnit']['id']) && ($this->data['SpecialhazardControlUnit']['id'] != "")){
				foreach($this->data['SpecialhazardControlUnit']['controlunit_type'] as $key1=>$val1){
					if($val1 != "0"){
					  $arr1[$val1] = $val1;
					}
				}
				$this->data['SpecialhazardControlUnit']['controlunit_type'] = implode(",",$arr1);
				$this->data['SpecialhazardControlUnit']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardControlUnit->save($this->data['SpecialhazardControlUnit'],false);
			}
			if(isset($this->data['SpecialhazardGasDevice']['id']) && ($this->data['SpecialhazardGasDevice']['id'] != "")){
				$this->data['SpecialhazardGasDevice']['specialhazard_report_id'] = $this->data['SpecialhazardReport']['id'];
				$this->SpecialhazardGasDevice->save($this->data['SpecialhazardGasDevice'],false);
			}
		 }else{
			 if(isset($this->data['SpecialhazardReport']['system_types']) && !empty($this->data['SpecialhazardReport']['system_types']))
			 {
				foreach($this->data['SpecialhazardReport']['system_types'] as $key=>$val){
				if($val != "0"){
				   $arr[$val] = $val;
				}
			    }	
			 }
			$this->data['SpecialhazardReport']['system_types'] = implode(',',$arr);
			$this->SpecialhazardReport->save($this->data['SpecialhazardReport'],false);
			$lastInsertId = $this->SpecialhazardReport->getLastInsertId();
			if(isset($this->data['SpecialhazardAlarmIniDevice']) && ($this->data['SpecialhazardAlarmIniDevice'] != "")){
				$this->data['SpecialhazardAlarmIniDevice']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardAlarmIniDevice->save($this->data['SpecialhazardAlarmIniDevice'],false);
			}
			if(isset($this->data['SpecialhazardAlarmIndDevice']) && ($this->data['SpecialhazardAlarmIndDevice'] != "")){
				$this->data['SpecialhazardAlarmIndDevice']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardAlarmIndDevice->save($this->data['SpecialhazardAlarmIndDevice'],false);
			}
			if(isset($this->data['SpecialhazardPowerSystem']) && ($this->data['SpecialhazardPowerSystem'] != "")){
				$this->data['SpecialhazardPowerSystem']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardPowerSystem->save($this->data['SpecialhazardPowerSystem'],false);
			}
			if(isset($this->data['SpecialhazardSupIniDevice']) && ($this->data['SpecialhazardSupIniDevice'] != "")){
				$this->data['SpecialhazardSupIniDevice']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardSupIniDevice->save($this->data['SpecialhazardSupIniDevice'],false);
			}
			if(isset($this->data['SpecialhazardControlUnit']) && ($this->data['SpecialhazardControlUnit'] != "")){
				if(isset($this->data['SpecialhazardControlUnit']['controlunit_type']) && !empty($this->data['SpecialhazardControlUnit']['controlunit_type'])){
					foreach($this->data['SpecialhazardControlUnit']['controlunit_type'] as $key1=>$val1){
					if($val1 != "0"){
					  $arr1[$val1] = $val1;
					}
				  }	
				}
				$this->data['SpecialhazardControlUnit']['controlunit_type'] = implode(",",$arr1);
				$this->data['SpecialhazardControlUnit']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardControlUnit->save($this->data['SpecialhazardControlUnit'],false);
			}
			if(isset($this->data['SpecialhazardGasDevice']) && ($this->data['SpecialhazardGasDevice'] != "")){
				$this->data['SpecialhazardGasDevice']['specialhazard_report_id'] = $lastInsertId;
				$this->SpecialhazardGasDevice->save($this->data['SpecialhazardGasDevice'],false);
			}
			if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				// this is the function for sending mail after press finish button
				$this->SendMailAfterFinish($this->data['SpecialhazardReport']['client_id'],$this->data['SpecialhazardReport']['report_id'],$this->data['SpecialhazardReport']['finish_date']);
			}
		 }
			$this->Session->setFlash('Report has been saved successfully.');
			$this->redirect(array('controller'=>'schedules','action'=>'completedschedule'));
	 }
	 else {
			$this->data = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
			$result = explode(",",$this->data['SpecialhazardReport']['system_types']);
			$this->data['SpecialhazardReport']['system_types'] = "";
			foreach($result as $res1){
			  $this->data['SpecialhazardReport']['system_types'][intval(trim($res1))] = intval(trim($res1));
			}
			
			$result1 = explode(",",$this->data['SpecialhazardControlUnit']['controlunit_type']);
			$this->data['SpecialhazardControlUnit']['controlunit_type'] = "";
			foreach($result1 as $res){
			  $this->data['SpecialhazardControlUnit']['controlunit_type'][intval(trim($res))] = intval(trim($res));
			}
	 }
}

/***************************************************************************************
	function for specialhazard report view
****************************************************************************************/ 
      function inspector_specialhazardView(){
      $this->layout='';      	
      	$this->loadModel('User');
	$this->loadModel('Schedule');
      	/*$this->loadModel('Attachment');
      	$this->loadModel('Notification');*/
      	$this->loadModel('SpecialhazardReport');
	$this->loadModel('Service');
	$this->loadModel('Quote');
      	/*$specialHazardarr = Configure::read('specialHazardarr');
      	$this->set('specialHazardarr',$specialHazardarr);
      	$controlUnit = Configure::read('controlUnit');
      	$this->set('controlUnit',$controlUnit);*/
            	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
            		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
            		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
            		$this->set('spResult',$spResult);
            		$this->set('spId',$_REQUEST['spID']);
            		$this->set('clientId',$_REQUEST['clientID']);
            		$this->set('clientResult',$clientResult);
            		$this->set('servicecallId',$_REQUEST['serviceCallID']);      		
            		$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
            		$record = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
            		$this->set('record',$record);
            		//pr($record); die;
            		/*$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
            		$this->set('attachmentdata',$attachmentdata);
            		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
            		$this->set('notifierdata',$notifierdata);*/
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
			// pr($schedule);
			$result = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
			//pr($result);die;
			foreach($result['SpecialhazardReportRecord'] as $key1=>$record){
				if($record['deficiency'] == "Yes"){
				   $arr1[$record['type']]['fail'][$key1] = 1;
				}
				if($record['deficiency'] == "No"){
				   $arr1[$record['type']]['pass'][$key1] = 1;
				}				  
				$arr[$key1] = $record['type'];
			}//pr($arr);die;
			$arr_count = array_count_values($arr);
			$options = array();
			$val = "";
			foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];			
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					}
					else{
						$options[$key]['pass'] = 0;	
					}
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					}else{
						$options[$key]['fail'] = 0;	
					}	
				}else{
					$options[$key]['pass'] = 0;	
					$options[$key]['fail'] = 0;	
				}
			}
			$this->set('options',$options);
			
					
			#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
										'Code'=> array(
												'className' => 'Code',
												'foreignKey' => 'code_id',
												'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
												)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
			//pr($Def_record);die;
					
					
              }	
      }
/***************************************************************************************
	function for specialhazard report view for CLient Panel
****************************************************************************************/ 
      function client_specialhazard(){
      $this->layout='';      	
      	$this->loadModel('Attachment');
      	$this->loadModel('Notification');
      	$this->loadModel('SpecialhazardReport');
      	$specialHazardarr = Configure::read('specialHazardarr');
      	$this->set('specialHazardarr',$specialHazardarr);
      	$controlUnit = Configure::read('controlUnit');
      	$this->set('controlUnit',$controlUnit);
            	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
            		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
            		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
            		$this->set('spResult',$spResult);
            		$this->set('spId',$_REQUEST['spID']);
            		$this->set('clientId',$_REQUEST['clientID']);
            		$this->set('clientResult',$clientResult);
            		$this->set('servicecallId',$_REQUEST['serviceCallID']);      		
            		$this->set('reportId',$_REQUEST['reportID']);
					$this->set('reportType',Configure::read('reportType'));
            		$record = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
            		$this->set('record',$record);
            		//pr($record); die;
            		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
            		$this->set('attachmentdata',$attachmentdata);
            		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
            		$this->set('notifierdata',$notifierdata);
					
					#CODE for deficiency page in view
					$this->loadModel('Deficiency');
					$this->Deficiency->bindModel(array('belongsTo' => array(
																'Code'=> array(
																			'className' => 'Code',
																			'foreignKey' => 'code_id',
																			'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																			)
																)
											 )
									   );
					$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
					$this->set('Def_record',$Def_record);
					$this->set('Def_record_recomm',$Def_record);
			$this->viewPath = 'reports';
			$this->render('inspector_specialhazardView');
              }	
      }
/***************************************************************************************
	function for specialhazard report view for sp Panel
****************************************************************************************/ 
      function sp_specialhazardView(){
      $this->layout='';
		$this->loadModel('User');
      	$this->loadModel('Attachment');
      	$this->loadModel('Notification');
      	$this->loadModel('SpecialhazardReport');
      	$specialHazardarr = Configure::read('specialHazardarr');
      	$this->set('specialHazardarr',$specialHazardarr);
      	$controlUnit = Configure::read('controlUnit');
      	$this->set('controlUnit',$controlUnit);
            	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
            		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
            		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
            		$this->set('spResult',$spResult);
            		$this->set('spId',$_REQUEST['spID']);
            		$this->set('clientId',$_REQUEST['clientID']);
            		$this->set('clientResult',$clientResult);
            		$this->set('servicecallId',$_REQUEST['serviceCallID']);      		
            		$this->set('reportId',$_REQUEST['reportID']);
					$this->set('reportType',Configure::read('reportType'));
            		$record = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
            		$this->set('record',$record);
            		//pr($record); die;
            		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
            		$this->set('attachmentdata',$attachmentdata);
            		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
            		$this->set('notifierdata',$notifierdata);
					
					#CODE for deficiency page in view
					$this->loadModel('Deficiency');
					$this->Deficiency->bindModel(array('belongsTo' => array(
																'Code'=> array(
																			'className' => 'Code',
																			'foreignKey' => 'code_id',
																			'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																			)
																)
											 )
									   );
					$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
					$this->set('Def_record',$Def_record);
					$this->set('Def_record_recomm',$Def_record);
			$this->viewPath = 'reports';
			$this->render('inspector_specialhazardView');
              }	
      }      
/***************************************************************************************
	function to export Sprinkler REPORT  as PDF
****************************************************************************************/ 	   
     function pdfSpecialhazardReport(){
	set_time_limit(0);
      $this->loadModel('User');
    	$this->loadModel('Attachment');
      	$this->loadModel('Notification');
      	$this->loadModel('SpecialhazardReport');
      	$specialHazardarr = Configure::read('specialHazardarr');
      	$this->set('specialHazardarr',$specialHazardarr);
      	$controlUnit = Configure::read('controlUnit');
      	$this->set('controlUnit',$controlUnit);
            	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
            		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
            		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
            		$this->set('spResult',$spResult);
            		$this->set('spId',$_REQUEST['spID']);
            		$this->set('clientId',$_REQUEST['clientID']);
            		$this->set('clientResult',$clientResult);
            		$this->set('servicecallId',$_REQUEST['serviceCallID']);      		
            		$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
            		$record = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
            		$this->set('record',$record);
            		//pr($record); die;
            		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'])));
			$this->set('attachmentdata',$attachmentdata);
			$notifierdata= $this->Notification->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'])));
			$this->set('notifierdata',$notifierdata);
					
					#CODE for deficiency page in view
					$this->loadModel('Deficiency');
					$this->Deficiency->bindModel(array('belongsTo' => array(
																'Code'=> array(
																			'className' => 'Code',
																			'foreignKey' => 'code_id',
																			'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																			)
																)
											 )
									   );
					$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
					$this->set('Def_record',$Def_record);
					$this->set('Def_record_recomm',$Def_record);
	
            }	      
     } 

/***************************************************************************************
	#THIS FUNCTION IS USED FOR FIREALARM REPORTS
	Reports controller firealarm reportsfunction 
****************************************************************************************/
	function inspector_firealarm(){	 
	   $this->layout='inspector';
	   $firealarmarr = Configure::read('firealarmarr');
	   $this->set('firealarmarr',$firealarmarr);
	   $frequency = Configure::read('frequency');
	   $this->set('frequency',$frequency);
	   $this->loadModel('FaAlarmDeviceType');
	   $this->loadModel('FaAlarmDevicePlace');
	   $this->loadModel('FirealarmReport');
	   $this->loadModel('FaAlarmFunction');
	   $this->loadModel('FaAlarmPanelSupervisoryFunction');
	   $this->loadModel('FaAuxiliaryFunction');
	   $this->loadModel('FaPumpSupervisoryFunction');
	   $this->loadModel('FaGeneratorSupervisoryFunction');
	   $this->loadModel('FaLocationDescription');
	   $this->loadModel('Service');
	   $this->loadModel('Notification');
	   $this->loadModel('Attachment');
	   $this->loadModel('Schedule');
	   
	   /*To Fetch and set Report Text*/
	   $this->loadModel('SpReportText');
	   $texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
	   $this->set('texts',nl2br($texts['SpReportText']['report_text']));
	   /*To Fetch and set Report Text*/
	   	
	   $daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
	   $daysRemaining = $daysData[0]['days'];
	   if($daysRemaining!=null || $daysRemaining!=""){
	   }else{
	       $daysRemaining="";
	   }
	   $this->set(compact('daysRemaining'));
	   $inspectorses = $this->Session->read('Log');
	   $inspector_id = $inspectorses['User']['id'];
	   
	  if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
		
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
  		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
  		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->set('sessionInspectorId',$inspector_id); 
		$this->set('spResult',$spResult);
  		$this->set('spId',$_REQUEST['spID']);
  		$this->set('clientId',$_REQUEST['clientID']);
  		$this->set('clientResult',$clientResult);
  		$this->set('servicecallId',$_REQUEST['serviceCallID']);		
  		$this->set('reportId',$_REQUEST['reportID']);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
      		$this->set('attachmentdata',$attachmentdata);
      		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
      		$this->set('notifierdata',$notifierdata);
			$device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'],'FaAlarmDevicePlace.report_id'=>$_REQUEST['reportID'],'FaAlarmDevicePlace.servicecall_id'=>$_REQUEST['serviceCallID'],'FaAlarmDevicePlace.client_id'=>$_REQUEST['clientID'],'FaAlarmDevicePlace.inspector_id'=>$inspector_id)));
		  $this->set('device_place',$device_place);	
     // $device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
	   
	    ###########################################
      #Code to get devices added by a given sp
     	$this->loadModel('Schedule');
      $this->loadModel('ScheduleService'); 
     	$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
     	$this->ScheduleService->bindModel(
                            array('belongsTo' => array(
                              'Service' => array(
                                'className' => 'Service'
                              )
                            )
              )
            );
      if(!empty($scheduledata)){
        $servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));
	
	$this->set('scheduleId',$scheduledata['Schedule']['id']);
      }
      
      $this->set('servicedata',$servicedata);
      //pr($servicedata);
      $device_type_temp=array();
      foreach($servicedata as $value){
        $device_type_temp[$value['Service']['id']] = $value['Service']['name'];
      }
      $this->set('device_type',$device_type_temp);		
     	############################################
	  }
		
		if(isset($this->data) && !empty($this->data)){
		  if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
			
			$this->loadModel('ReportCert');
			$chkCert = $this->ReportCert->find('count',array('conditions'=>array('ReportCert.schedule_id'=>$scheduledata['Schedule']['id'],'ReportCert.ins_cert_form'=>'')));			
			if($chkCert>0){
				$this->Session->setFlash('Please attach all the required certificates');
				$this->redirect($this->referer());
			}
			
			/*Chk Signature*/
			$this->loadModel('Signature');
			$chkSign = $this->Signature->find('count',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
			if($chkSign==0){
				$this->Session->setFlash('Please add your signature first.');
				$this->redirect($this->referer());
			}
			/*Chk Signature*/
			
			$this->data['FirealarmReport']['finish_date'] = date('Y-m-d H:i:s');
			$action="Finished the Firealarm report";				
		  }
      if(isset($this->data['FirealarmReport']['id']) && !empty($this->data['FirealarmReport']['id'])){
//pr($this->data); die;
  			$this->data['FirealarmReport']['id'] = $this->data['FirealarmReport']['id'];
  	# delete data from all associated tables before saving in case of update		
  			$this->FaAlarmFunction->deleteAll(array('FaAlarmFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			$this->FaAlarmPanelSupervisoryFunction->deleteAll(array('FaAlarmPanelSupervisoryFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			$this->FaAuxiliaryFunction->deleteAll(array('FaAuxiliaryFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			$this->FaPumpSupervisoryFunction->deleteAll(array('FaPumpSupervisoryFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			$this->FaGeneratorSupervisoryFunction->deleteAll(array('FaGeneratorSupervisoryFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			if($this->FirealarmReport->save($this->data['FirealarmReport'],false)){
  				// save functionality for FaAlarmFunction Model
  				//$lastInsertId = $this->FirealarmReport->getLastInsertId();
  				$this->data['FaAlarmFunction']['firealarm_report_id'] = $this->data['FirealarmReport']['id'];
  				$this->FaAlarmFunction->save($this->data['FaAlarmFunction'],false); 
  				// save functionality for FaAlarmPanelSupervisoryFunction Model
  				foreach($this->data['FaAlarmPanelSupervisoryFunction'] as $key=>$val){
  					$data['FaAlarmPanelSupervisoryFunction']['firealarm_report_id']=$this->data['FirealarmReport']['id']; 
  					$data['FaAlarmPanelSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaAlarmPanelSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaAlarmPanelSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaAlarmPanelSupervisoryFunction->create();
  					$this->FaAlarmPanelSupervisoryFunction->save($data['FaAlarmPanelSupervisoryFunction'],false);
  				}
  				// save functionality for FaAuxiliaryFunction Model
  				foreach($this->data['FaAuxiliaryFunction'] as $key=>$val){
  					$data['FaAuxiliaryFunction']['firealarm_report_id']=$this->data['FirealarmReport']['id']; 
  					$data['FaAuxiliaryFunction']['function_name'] =  $val['function_name'];
  					$data['FaAuxiliaryFunction']['description'] =  $val['description'];
  					$data['FaAuxiliaryFunction']['result'] =  $val['result'];
  					$this->FaAuxiliaryFunction->create();
  					$this->FaAuxiliaryFunction->save($data['FaAuxiliaryFunction'],false);
  				}
  				// save functionality for FaPumpSupervisoryFunction Model
  				foreach($this->data['FaPumpSupervisoryFunction'] as $key=>$val){
  					$data['FaPumpSupervisoryFunction']['firealarm_report_id']=$this->data['FirealarmReport']['id']; 
  					$data['FaPumpSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaPumpSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaPumpSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaPumpSupervisoryFunction->create();
  					$this->FaPumpSupervisoryFunction->save($data['FaPumpSupervisoryFunction'],false);
  				}
  				// save functionality for FaGeneratorSupervisoryFunction Model
  				foreach($this->data['FaGeneratorSupervisoryFunction'] as $key=>$val){
  					$data['FaGeneratorSupervisoryFunction']['firealarm_report_id']=$this->data['FirealarmReport']['id']; 
  					$data['FaGeneratorSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaGeneratorSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaGeneratorSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaGeneratorSupervisoryFunction->create();
  					$this->FaGeneratorSupervisoryFunction->save($data['FaGeneratorSupervisoryFunction'],false);
  				}
  				// save functionality for FaLocationDescription Model
				if(isset($this->data['FaLocationDescription']) && is_array($this->data['FaLocationDescription'])){
					foreach($this->data['FaLocationDescription'] as $key=>$val){
						foreach($val as $key1 => $val1)	{
						if(!empty($val1['location_id']) && !empty($val1['fa_alarm_device_type_id']) && !empty($val1['zone_address']) && !empty($val1['model'])){
						  $data['FaLocationDescription']['firealarm_report_id']=$this->data['FirealarmReport']['id'];
							$data['FaLocationDescription']['fa_alarm_device_place_id'] = $val1['location_id'];
							$data['FaLocationDescription']['fa_alarm_device_type_id'] = $val1['fa_alarm_device_type_id'];
							$data['FaLocationDescription']['family'] = $val1['family'];
							$data['FaLocationDescription']['zone_address'] = $val1['zone_address'];
							$data['FaLocationDescription']['location'] = $val1['location'];
							$data['FaLocationDescription']['model'] = $val1['model'];
							$data['FaLocationDescription']['physical_condition'] = $val1['physical_condition'];
							$data['FaLocationDescription']['service'] = $val1['service'];
							$data['FaLocationDescription']['functional_test'] = $val1['functional_test'];
							$data['FaLocationDescription']['comment'] = $val1['comment'];
							$this->FaLocationDescription->create();
							$this->FaLocationDescription->save($data['FaLocationDescription'],false);
						}
						}
					}
				}	
  				if(!isset($action)){
  					$action="Edited the firealarm report";
  				}				
  				$this->saveLog($this->data['FirealarmReport']['sp_id'],$this->data['FirealarmReport']['report_id'],$this->data['FirealarmReport']['client_id'],$this->data['FirealarmReport']['servicecall_id'],$action);
  			     $this->Session->setFlash('Report has been updated successfully.');
  			     $this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
  		  }
      }else{      
  
  			if($this->FirealarmReport->save($this->data['FirealarmReport'],false)){
  				// save functionality for FaAlarmFunction Model
  				$lastInsertId = $this->FirealarmReport->getLastInsertId();
  				$this->data['FaAlarmFunction']['firealarm_report_id'] = $lastInsertId;
  				$this->FaAlarmFunction->save($this->data['FaAlarmFunction'],false); 
  				// save functionality for FaAlarmPanelSupervisoryFunction Model
  				foreach($this->data['FaAlarmPanelSupervisoryFunction'] as $key=>$val){
  					$data['FaAlarmPanelSupervisoryFunction']['firealarm_report_id']=$lastInsertId; 
  					$data['FaAlarmPanelSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaAlarmPanelSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaAlarmPanelSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaAlarmPanelSupervisoryFunction->create();
  					$this->FaAlarmPanelSupervisoryFunction->save($data['FaAlarmPanelSupervisoryFunction'],false);
  				}
  				// save functionality for FaAuxiliaryFunction Model
  				foreach($this->data['FaAuxiliaryFunction'] as $key=>$val){
  					$data['FaAuxiliaryFunction']['firealarm_report_id']=$lastInsertId; 
  					$data['FaAuxiliaryFunction']['function_name'] =  $val['function_name'];
  					$data['FaAuxiliaryFunction']['description'] =  $val['description'];
  					$data['FaAuxiliaryFunction']['result'] =  $val['result'];
  					$this->FaAuxiliaryFunction->create();
  					$this->FaAuxiliaryFunction->save($data['FaAuxiliaryFunction'],false);
  				}
  				// save functionality for FaPumpSupervisoryFunction Model
  				foreach($this->data['FaPumpSupervisoryFunction'] as $key=>$val){
  					$data['FaPumpSupervisoryFunction']['firealarm_report_id']=$lastInsertId; 
  					$data['FaPumpSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaPumpSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaPumpSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaPumpSupervisoryFunction->create();
  					$this->FaPumpSupervisoryFunction->save($data['FaPumpSupervisoryFunction'],false);
  				}
  				// save functionality for FaGeneratorSupervisoryFunction Model
  				foreach($this->data['FaGeneratorSupervisoryFunction'] as $key=>$val){
  					$data['FaGeneratorSupervisoryFunction']['firealarm_report_id']=$lastInsertId; 
  					$data['FaGeneratorSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaGeneratorSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaGeneratorSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaGeneratorSupervisoryFunction->create();
  					$this->FaGeneratorSupervisoryFunction->save($data['FaGeneratorSupervisoryFunction'],false);
  				}
  				// save functionality for FaLocationDescription Model
				if(isset($this->data['FaLocationDescription']) && is_array($this->data['FaLocationDescription'])){
					foreach($this->data['FaLocationDescription'] as $key=>$val){
						foreach($val as $key1 => $val1)	{
						if(!empty($val1['location_id']) && !empty($val1['fa_alarm_device_type_id']) && !empty($val1['zone_address']) && !empty($val1['model'])){
						  $data['FaLocationDescription']['firealarm_report_id']=$lastInsertId;
							$data['FaLocationDescription']['fa_alarm_device_place_id'] = $val1['location_id'];
							$data['FaLocationDescription']['fa_alarm_device_type_id'] = $val1['fa_alarm_device_type_id'];
							$data['FaLocationDescription']['family'] = $val1['family'];
							$data['FaLocationDescription']['zone_address'] = $val1['zone_address'];
							$data['FaLocationDescription']['location'] = $val1['location'];
							$data['FaLocationDescription']['model'] = $val1['model'];
							$data['FaLocationDescription']['physical_condition'] = $val1['physical_condition'];
							$data['FaLocationDescription']['service'] = $val1['service'];
							$data['FaLocationDescription']['functional_test'] = $val1['functional_test'];
							$data['FaLocationDescription']['comment'] = $val1['comment'];
							$this->FaLocationDescription->create();
							$this->FaLocationDescription->save($data['FaLocationDescription'],false);
						}
						}
					}
				}	
  				$action="Edited the fire alarm report";
  				$this->saveLog($this->data['FirealarmReport']['sp_id'],$this->data['FirealarmReport']['report_id'],$this->data['FirealarmReport']['client_id'],$this->data['FirealarmReport']['servicecall_id'],$action);
  			  $this->Session->setFlash('New record has been saved successfully.');
    			$this->redirect(array('controller'=>'inspectors','action'=>'schedule')); 
		    }
  			}
			}
		else{	        
			$this->data = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'],'FirealarmReport.report_id'=>$_REQUEST['reportID'])));	
	  }
	}

/***************************************************************************************
	#THIS FUNCTION IS USED FOR firealarm REPORT to delete the fire alarm loaction description record (ajax request)
	Reports controller firealam reportsfunction 
****************************************************************************************/	
	function delFaLocDescription($id = null){
	  $this->layout = null;
		$this->loadModel('FaLocationDescription');
		if($this->FaLocationDescription->delete($id)){
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
  }
/***************************************************************************************
	#THIS FUNCTION IS USED FOR firealarm REPORT to review the report
	Reports controller firealam reportsfunction 
****************************************************************************************/  
  function inspector_firealarmView(){
    $this->layout='';
    $this->loadModel('User');
	$this->loadModel('FirealarmReport');
	$this->loadModel('Attachment');
	$this->loadModel('Service');
	$this->loadModel('FaAlarmDevicePlace');
	$this->loadModel('Notification');
	$this->loadModel('Quote');
	
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code for quotes		
		$chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
		$this->set('chkRecord',$chkRecord);
		
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
		
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
    $device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'],'FaAlarmDevicePlace.report_id'=>$_REQUEST['reportID'],'FaAlarmDevicePlace.servicecall_id'=>$_REQUEST['serviceCallID'],'FaAlarmDevicePlace.client_id'=>$_REQUEST['clientID'])));
    $this->set('device_place',$device_place);	
    ###########################################
    #Code to get devices added by a given sp
   	$this->loadModel('Schedule');
     $this->loadModel('ScheduleService'); 
   	$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id',"Schedule.sch_freq"),'recursive'=> -1));
	$this->set('scheduledata',$scheduledata);
	$this->set('schFreq',$scheduledata['Schedule']['sch_freq']);
   	$this->ScheduleService->bindModel(
                          array('belongsTo' => array(
                            'Service' => array(
                              'className' => 'Service'
                            )
                          )
            )
          );
       
     if(!empty($scheduledata)){
      $servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
    }
    
    $this->set('servicedata',$servicedata);
   	############################################
    $device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
    $this->set('device_type',$device_type);		
	$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
	$this->set('record',$record);
	$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
	$this->set('attachmentdata',$attachmentdata);
	$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
	$this->set('notifierdata',$notifierdata);
	$this->loadModel('Deficiency');
	$this->Deficiency->bindModel(array('belongsTo' => array(
												'Code'=> array(
															'className' => 'Code',
															'foreignKey' => 'code_id',
															'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
															)
												)
							 )
					   );
	$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
	$this->set('Def_record',$Def_record);
	$this->set('Def_record_recomm',$Def_record);
	
		
	}
  }
/***************************************************************************************
	#THIS FUNCTION IS USED FOR firealarm REPORT to review the report
	Reports controller firealam reportsfunction 
****************************************************************************************/  
  function client_firealarm(){
    $this->layout='';
    $this->loadModel('User');
	$this->loadModel('FirealarmReport');
	$this->loadModel('Attachment');
	$this->loadModel('Service');
	$this->loadModel('FaAlarmDevicePlace');
	$this->loadModel('Notification');
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
    $device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'])));
    $this->set('device_place',$device_place);	
    ###########################################
    #Code to get devices added by a given sp
   	$this->loadModel('Schedule');
     $this->loadModel('ScheduleService'); 
   	$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
   	$this->ScheduleService->bindModel(
                          array('belongsTo' => array(
                            'Service' => array(
                              'className' => 'Service'
                            )
                          )
            )
          );
       
     if(!empty($scheduledata)){
      $servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
    }
    
    $this->set('servicedata',$servicedata);
   	############################################
    $device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
    $this->set('device_type',$device_type);		
		$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('record',$record);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
													'Code'=> array(
																'className' => 'Code',
																'foreignKey' => 'code_id',
																'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																)
													)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
	$this->loadModel('ServiceCall');
	$servicecallResult=$this->ServiceCall->findById($_REQUEST['serviceCallID'],array('ServiceCall.sp_site_address_id'));
	$this->loadModel('SpSiteAddress');
	$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
	$this->set('siteaddrResult',$siteaddrResult);	
	$this->render('inspector_firealarmView');	
		
	}
  }
  
 /***************************************************************************************
	#THIS FUNCTION IS USED FOR firealarm REPORT to review the report
	Reports controller firealam reportsfunction 
****************************************************************************************/  
  function sp_firealarmView(){
    $this->layout='';
    $this->loadModel('User');
	$this->loadModel('FirealarmReport');
	$this->loadModel('Attachment');
	$this->loadModel('Service');
	$this->loadModel('FaAlarmDevicePlace');
	$this->loadModel('Notification');
	
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
    $device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'])));
    $this->set('device_place',$device_place);	
    ###########################################
    #Code to get devices added by a given sp
   	$this->loadModel('Schedule');
	//$schData = $this->Schedule->find('first',array('conditions'=>array('Schedule.report_id'=>,'Schedule.service_call_id'=>)));
     $this->loadModel('ScheduleService'); 
   	$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
	
   	$this->ScheduleService->bindModel(
                          array('belongsTo' => array(
                            'Service' => array(
                              'className' => 'Service'
                            )
                          )
            )
          );
       
     if(!empty($scheduledata)){
      $servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
    }
    
    $this->set('servicedata',$servicedata);
   	############################################
    $device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
    $this->set('device_type',$device_type);		
		$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('record',$record);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
													'Code'=> array(
																'className' => 'Code',
																'foreignKey' => 'code_id',
																'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																)
													)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
	$this->render('inspector_firealarmView');	
		
	}
  } 
/***************************************************************************************
		 	 function to export firealam REPORT  as PDF
****************************************************************************************/ 	 
	function pdfFirealarmReport(){
	   $this->layout='';
	   set_time_limit(0);
    $this->loadModel('User');
	$this->loadModel('FirealarmReport');
	$this->loadModel('Attachment');
	$this->loadModel('Service');
	$this->loadModel('FaAlarmDevicePlace');
	$this->loadModel('Notification');
	
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
    $device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'])));
    $this->set('device_place',$device_place);	
    ###########################################
    #Code to get devices added by a given sp
   	$this->loadModel('Schedule');
     $this->loadModel('ScheduleService'); 
   	$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
   	$this->ScheduleService->bindModel(
                          array('belongsTo' => array(
                            'Service' => array(
                              'className' => 'Service'
                            )
                          )
            )
          );
     if(!empty($scheduledata)){
      $servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
    }
    
    $this->set('servicedata',$servicedata);
   	############################################
    $device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
    $this->set('device_type',$device_type);		
		$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('record',$record);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		  $this->set('attachmentdata',$attachmentdata);
		  $notifierdata= $this->Notification->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		  $this->set('notifierdata',$notifierdata);
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
													'Code'=> array(
																'className' => 'Code',
																'foreignKey' => 'code_id',
																'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																)
													)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
		
		
	}
  }	  
/***************************************************************************************
	#THIS FUNCTION IS USED FOR FIREALARM REPORTS
	Reports controller firealarm reportsfunction 
****************************************************************************************/
	function sp_firealarm(){
	 
	   $this->layout='sp';
	   $firealarmarr = Configure::read('firealarmarr');
	   $this->set('firealarmarr',$firealarmarr);
	   $frequency = Configure::read('frequency');
	   $this->set('frequency',$frequency);
	   
	   $this->loadModel('FaAlarmDeviceType');
	   $this->loadModel('FaAlarmDevicePlace');
	   $this->loadModel('FirealarmReport');
	   $this->loadModel('FaAlarmFunction');
	   $this->loadModel('FaAlarmPanelSupervisoryFunction');
	   $this->loadModel('FaAuxiliaryFunction');
	   $this->loadModel('FaPumpSupervisoryFunction');
	   $this->loadModel('FaGeneratorSupervisoryFunction');
	   $this->loadModel('FaLocationDescription');
	   $this->loadModel('Service');
	   $this->loadModel('Notification');
	   $this->loadModel('Attachment');
	   
	   
	   $inspectorses = $this->Session->read('Log');
	   $inspector_id = $inspectorses['User']['id'];
	   
	  if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
  		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
  		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
  		$inspectorses = $this->Session->read('Log');
  	  $inspector_id = $inspectorses['User']['id'];
  	  $this->set('sessionInspectorId',$inspector_id); 
      $this->set('spResult',$spResult);
  		$this->set('spId',$_REQUEST['spID']);
  		$this->set('clientId',$_REQUEST['clientID']);
  		$this->set('clientResult',$clientResult);
  		$this->set('servicecallId',$_REQUEST['serviceCallID']);		
  		$this->set('reportId',$_REQUEST['reportID']);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
      		$this->set('attachmentdata',$attachmentdata);
      		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
      		$this->set('notifierdata',$notifierdata);
      $device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'])));
		  $this->set('device_place',$device_place);	
     // $device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
	   
	    ###########################################
      #Code to get devices added by a given sp
     	$this->loadModel('Schedule');
      $this->loadModel('ScheduleService'); 
     	$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
     	$this->ScheduleService->bindModel(
                            array('belongsTo' => array(
                              'Service' => array(
                                'className' => 'Service'
                              )
                            )
              )
            );
      if(!empty($scheduledata)){
        $servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
      }
      
      $this->set('servicedata',$servicedata);
      //pr($servicedata);
      $device_type_temp=array();
      foreach($servicedata as $value){
        $device_type_temp[$value['Service']['id']] = $value['Service']['name'];
      }
      $this->set('device_type',$device_type_temp);		
     	############################################
	  }
		
		if(isset($this->data) && !empty($this->data)){
		  if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				$this->data['FirealarmReport']['finish_date'] = date('Y-m-d H:i:s');
				$action="Finished the Firealarm report";
		  }
      if(isset($this->data['FirealarmReport']['id']) && !empty($this->data['FirealarmReport']['id'])){
//pr($this->data); die;
  			$this->data['FirealarmReport']['id'] = $this->data['FirealarmReport']['id'];
  	# delete data from all associated tables before saving in case of update		
  			$this->FaAlarmFunction->deleteAll(array('FaAlarmFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			$this->FaAlarmPanelSupervisoryFunction->deleteAll(array('FaAlarmPanelSupervisoryFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			$this->FaAuxiliaryFunction->deleteAll(array('FaAuxiliaryFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			$this->FaPumpSupervisoryFunction->deleteAll(array('FaPumpSupervisoryFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			$this->FaGeneratorSupervisoryFunction->deleteAll(array('FaGeneratorSupervisoryFunction.firealarm_report_id' => $this->data['FirealarmReport']['id'])); 
  			if($this->FirealarmReport->save($this->data['FirealarmReport'],false)){
  				// save functionality for FaAlarmFunction Model
  				//$lastInsertId = $this->FirealarmReport->getLastInsertId();
  				$this->data['FaAlarmFunction']['firealarm_report_id'] = $this->data['FirealarmReport']['id'];
  				$this->FaAlarmFunction->save($this->data['FaAlarmFunction'],false); 
  				// save functionality for FaAlarmPanelSupervisoryFunction Model
  				foreach($this->data['FaAlarmPanelSupervisoryFunction'] as $key=>$val){
  					$data['FaAlarmPanelSupervisoryFunction']['firealarm_report_id']=$this->data['FirealarmReport']['id']; 
  					$data['FaAlarmPanelSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaAlarmPanelSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaAlarmPanelSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaAlarmPanelSupervisoryFunction->create();
  					$this->FaAlarmPanelSupervisoryFunction->save($data['FaAlarmPanelSupervisoryFunction'],false);
  				}
  				// save functionality for FaAuxiliaryFunction Model
  				foreach($this->data['FaAuxiliaryFunction'] as $key=>$val){
  					$data['FaAuxiliaryFunction']['firealarm_report_id']=$this->data['FirealarmReport']['id']; 
  					$data['FaAuxiliaryFunction']['function_name'] =  $val['function_name'];
  					$data['FaAuxiliaryFunction']['description'] =  $val['description'];
  					$data['FaAuxiliaryFunction']['result'] =  $val['result'];
  					$this->FaAuxiliaryFunction->create();
  					$this->FaAuxiliaryFunction->save($data['FaAuxiliaryFunction'],false);
  				}
  				// save functionality for FaPumpSupervisoryFunction Model
  				foreach($this->data['FaPumpSupervisoryFunction'] as $key=>$val){
  					$data['FaPumpSupervisoryFunction']['firealarm_report_id']=$this->data['FirealarmReport']['id']; 
  					$data['FaPumpSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaPumpSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaPumpSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaPumpSupervisoryFunction->create();
  					$this->FaPumpSupervisoryFunction->save($data['FaPumpSupervisoryFunction'],false);
  				}
  				// save functionality for FaGeneratorSupervisoryFunction Model
  				foreach($this->data['FaGeneratorSupervisoryFunction'] as $key=>$val){
  					$data['FaGeneratorSupervisoryFunction']['firealarm_report_id']=$this->data['FirealarmReport']['id']; 
  					$data['FaGeneratorSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaGeneratorSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaGeneratorSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaGeneratorSupervisoryFunction->create();
  					$this->FaGeneratorSupervisoryFunction->save($data['FaGeneratorSupervisoryFunction'],false);
  				}
  				// save functionality for FaLocationDescription Model
  				foreach($this->data['FaLocationDescription'] as $key=>$val){
  					foreach($val as $key1 => $val1)	{
    					if(!empty($val1['location_id']) && !empty($val1['fa_alarm_device_type_id']) && !empty($val1['zone_address']) && !empty($val1['model'])){
    					  $data['FaLocationDescription']['firealarm_report_id']=$this->data['FirealarmReport']['id'];
    						$data['FaLocationDescription']['fa_alarm_device_place_id'] = $val1['location_id'];
    						$data['FaLocationDescription']['fa_alarm_device_type_id'] = $val1['fa_alarm_device_type_id'];
    						$data['FaLocationDescription']['family'] = $val1['family'];
    						$data['FaLocationDescription']['zone_address'] = $val1['zone_address'];
    						$data['FaLocationDescription']['location'] = $val1['location'];
    						$data['FaLocationDescription']['model'] = $val1['model'];
    						$data['FaLocationDescription']['physical_condition'] = $val1['physical_condition'];
    						$data['FaLocationDescription']['service'] = $val1['service'];
    						$data['FaLocationDescription']['functional_test'] = $val1['functional_test'];
    						$data['FaLocationDescription']['comment'] = $val1['comment'];
    						$this->FaLocationDescription->create();
    						$this->FaLocationDescription->save($data['FaLocationDescription'],false);
    					}
  					}
  				}
  				 
  			    
  				if(!isset($action)){
  					$action="Edited the firealarm report";
  				}				
  				$this->saveLog($this->data['FirealarmReport']['sp_id'],$this->data['FirealarmReport']['report_id'],$this->data['FirealarmReport']['client_id'],$this->data['FirealarmReport']['servicecall_id'],$action);
  			     $this->Session->setFlash('Report has been updated successfully.');
  			     $this->redirect(array('controller'=>'schedules','action'=>'schedule'));
  		  }
      }else{      
  
  			if($this->FirealarmReport->save($this->data['FirealarmReport'],false)){
  				// save functionality for FaAlarmFunction Model
  				$lastInsertId = $this->FirealarmReport->getLastInsertId();
  				$this->data['FaAlarmFunction']['firealarm_report_id'] = $lastInsertId;
  				$this->FaAlarmFunction->save($this->data['FaAlarmFunction'],false); 
  				// save functionality for FaAlarmPanelSupervisoryFunction Model
  				foreach($this->data['FaAlarmPanelSupervisoryFunction'] as $key=>$val){
  					$data['FaAlarmPanelSupervisoryFunction']['firealarm_report_id']=$lastInsertId; 
  					$data['FaAlarmPanelSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaAlarmPanelSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaAlarmPanelSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaAlarmPanelSupervisoryFunction->create();
  					$this->FaAlarmPanelSupervisoryFunction->save($data['FaAlarmPanelSupervisoryFunction'],false);
  				}
  				// save functionality for FaAuxiliaryFunction Model
  				foreach($this->data['FaAuxiliaryFunction'] as $key=>$val){
  					$data['FaAuxiliaryFunction']['firealarm_report_id']=$lastInsertId; 
  					$data['FaAuxiliaryFunction']['function_name'] =  $val['function_name'];
  					$data['FaAuxiliaryFunction']['description'] =  $val['description'];
  					$data['FaAuxiliaryFunction']['result'] =  $val['result'];
  					$this->FaAuxiliaryFunction->create();
  					$this->FaAuxiliaryFunction->save($data['FaAuxiliaryFunction'],false);
  				}
  				// save functionality for FaPumpSupervisoryFunction Model
  				foreach($this->data['FaPumpSupervisoryFunction'] as $key=>$val){
  					$data['FaPumpSupervisoryFunction']['firealarm_report_id']=$lastInsertId; 
  					$data['FaPumpSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaPumpSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaPumpSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaPumpSupervisoryFunction->create();
  					$this->FaPumpSupervisoryFunction->save($data['FaPumpSupervisoryFunction'],false);
  				}
  				// save functionality for FaGeneratorSupervisoryFunction Model
  				foreach($this->data['FaGeneratorSupervisoryFunction'] as $key=>$val){
  					$data['FaGeneratorSupervisoryFunction']['firealarm_report_id']=$lastInsertId; 
  					$data['FaGeneratorSupervisoryFunction']['function_name'] =  $val['function_name'];
  					$data['FaGeneratorSupervisoryFunction']['description'] =  $val['description'];
  					$data['FaGeneratorSupervisoryFunction']['result'] =  $val['result'];
  					$this->FaGeneratorSupervisoryFunction->create();
  					$this->FaGeneratorSupervisoryFunction->save($data['FaGeneratorSupervisoryFunction'],false);
  				}
  				// save functionality for FaLocationDescription Model
  				foreach($this->data['FaLocationDescription'] as $key=>$val){
  					foreach($val as $key1 => $val1)	{
    					if(!empty($val1['location_id']) && !empty($val1['fa_alarm_device_type_id']) && !empty($val1['zone_address']) && !empty($val1['model'])){
    					  $data['FaLocationDescription']['firealarm_report_id']=$lastInsertId;
    						$data['FaLocationDescription']['fa_alarm_device_place_id'] = $val1['location_id'];
    						$data['FaLocationDescription']['fa_alarm_device_type_id'] = $val1['fa_alarm_device_type_id'];
    						$data['FaLocationDescription']['family'] = $val1['family'];
    						$data['FaLocationDescription']['zone_address'] = $val1['zone_address'];
    						$data['FaLocationDescription']['location'] = $val1['location'];
    						$data['FaLocationDescription']['model'] = $val1['model'];
    						$data['FaLocationDescription']['physical_condition'] = $val1['physical_condition'];
    						$data['FaLocationDescription']['service'] = $val1['service'];
    						$data['FaLocationDescription']['functional_test'] = $val1['functional_test'];
    						$data['FaLocationDescription']['comment'] = $val1['comment'];
    						$this->FaLocationDescription->create();
    						$this->FaLocationDescription->save($data['FaLocationDescription'],false);
    					}
  					}
  				}
  				$action="Edited the fire alarm report";
  				$this->saveLog($this->data['FirealarmReport']['sp_id'],$this->data['FirealarmReport']['report_id'],$this->data['FirealarmReport']['client_id'],$this->data['FirealarmReport']['servicecall_id'],$action);
  			  $this->Session->setFlash('New record has been saved successfully.');
    			$this->redirect(array('controller'=>'schedules','action'=>'schedule')); 
		    } 				 
  				
  			}
			}
		else{	        
			$this->data = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'],'FirealarmReport.report_id'=>$_REQUEST['reportID'])));	
	  }
	}

/***************************************************************************************
	#THIS FUNCTION IS USED FOR extinguishers REPORTS
	Reports controller extinguishers reportsfunction 
****************************************************************************************/ 		
	function inspector_extinguishers(){
	   $this->layout='inspector';
	   $this->loadModel('User');
	   $this->loadModel('ExtinguisherReport');
	   $this->loadModel('ScheduleService');
	   $this->loadModel('ExtinguisherReportRecord');
	   $this->loadModel('Schedule');
	   $this->loadModel('Attachment');
	   $this->loadModel('Notification');
	   $this->loadModel('Service');
	   
	   $this->loadModel('Schedule');
	    
		/*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/
	    	
		
	    $daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
	    $daysRemaining = $daysData[0]['days'];
	    if($daysRemaining!=null || $daysRemaining!=""){
	    }else{
	        $daysRemaining="";
	    }
	    $this->set(compact('daysRemaining'));
	   
	   $inspectorses = $this->Session->read('Log');
	   $inspector_id = $inspectorses['User']['id'];
	    $this->set('session_inspector_id',$inspector_id);
	   if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
		
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);
		
		$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		$reqScheduleId = $schedule[0]['Schedule']['id'];
		$this->set('scheduleId',$reqScheduleId);
		$options = array();
		 foreach($schedule[0]['ScheduleService'] as $schedule){
			$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			$options[$ServiceOptions['Service']['id']] = $ServiceOptions['Service']['name'];
		}
		$this->set('options',$options);
		
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		 
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
	   }
	    if(isset($this->data) && !empty($this->data)){
		
		if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				
				$this->loadModel('ReportCert');
				$chkCert = $this->ReportCert->find('count',array('conditions'=>array('ReportCert.schedule_id'=>$reqScheduleId,'ReportCert.ins_cert_form'=>'')));			
				if($chkCert>0){
					$this->Session->setFlash('Please attach all the required certificates');
					$this->redirect($this->referer());
				}
				
				/*Chk Signature*/
				$this->loadModel('Signature');
				$chkSign = $this->Signature->find('count',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
				if($chkSign==0){
					$this->Session->setFlash('Please add your signature first.');
					$this->redirect($this->referer());
				}
				/*Chk Signature*/	
			
				$this->data['ExtinguisherReport']['finish_date'] = date('Y-m-d H:i:s');
				$action="Finished the extinguisher report";
		}
		if(isset($this->data['ExtinguisherReport']['id']) && !empty($this->data['ExtinguisherReport']['id'])){
		
			$data['ExtinguisherReportRecord']['extinguisher_report_id'] = $this->data['ExtinguisherReport']['id'];
			$this->ExtinguisherReport->id=$this->data['ExtinguisherReport']['id'];
			if($this->ExtinguisherReport->save($this->data['ExtinguisherReport'],false))
		        {
			    if(isset($this->data['ExtinguisherReportRecord']) && !empty($this->data['ExtinguisherReportRecord']))
			    {
			    foreach($this->data['ExtinguisherReportRecord'] as $key=>$val)
			    {
				 $data['ExtinguisherReportRecord']['extinguisher_report_id']=$this->data['ExtinguisherReport']['id']; 
				 $data['ExtinguisherReportRecord']['location'] =  $val['location'];
				 $data['ExtinguisherReportRecord']['ext_type'] =  $val['ext_type'];
				 $data['ExtinguisherReportRecord']['model'] =  $val['model'];
				 $data['ExtinguisherReportRecord']['born_date'] =  $val['born_date'];
				 $data['ExtinguisherReportRecord']['deficiency'] =  $val['deficiency'];
				 //$data['ExtinguisherReportRecord']['deficiency_reason'] =  $val['deficiency_reason'];
				 $data['ExtinguisherReportRecord']['last_maintenace_date'] =  $val['last_maintenace_date'];
				 $data['ExtinguisherReportRecord']['last_maintenace_performed_by'] =  $val['last_maintenace_performed_by'];
				 $data['ExtinguisherReportRecord']['last_recharge_date'] =  $val['last_recharge_date'];
				 $data['ExtinguisherReportRecord']['last_recharge_performed_by'] =  $val['last_recharge_performed_by'];
				 $data['ExtinguisherReportRecord']['last_hydrotest_date'] =  $val['last_hydrotest_date'];
				 $data['ExtinguisherReportRecord']['last_hydrotest_performed_by'] =  $val['last_hydrotest_performed_by'];
				 $this->ExtinguisherReportRecord->create();
				 $this->ExtinguisherReportRecord->save($data['ExtinguisherReportRecord'],false);
			    }
			    }
				if(!isset($action)){
					$action="Edited the extinguisher report";
				}				
				$this->saveLog($this->data['ExtinguisherReport']['sp_id'],$this->data['ExtinguisherReport']['report_id'],$this->data['ExtinguisherReport']['client_id'],$this->data['ExtinguisherReport']['servicecall_id'],$action);
			     $this->Session->setFlash('New record has been saved successfully.');
			     $this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
			}
		}
		else{
		     if($this->ExtinguisherReport->save($this->data['ExtinguisherReport'],false))
		     {			
			foreach($this->data['ExtinguisherReportRecord'] as $key=>$val)
			{
			     $lastInsertId = $this->ExtinguisherReport->getLastInsertId();
			     $data['ExtinguisherReportRecord']['extinguisher_report_id'] = $lastInsertId;
			     $data['ExtinguisherReportRecord']['location'] =  $val['location'];
			     $data['ExtinguisherReportRecord']['ext_type'] =  $val['ext_type'];
			     $data['ExtinguisherReportRecord']['model'] =  $val['model'];
			     $data['ExtinguisherReportRecord']['born_date'] =  $val['born_date'];
			     $data['ExtinguisherReportRecord']['deficiency'] =  $val['deficiency'];
			     $data['ExtinguisherReportRecord']['deficiency_reason'] =  $val['deficiency_reason'];
			     $data['ExtinguisherReportRecord']['last_maintenace_date'] =  $val['last_maintenace_date'];
			     $data['ExtinguisherReportRecord']['last_maintenace_performed_by'] =  $val['last_maintenace_performed_by'];
			     $data['ExtinguisherReportRecord']['last_recharge_date'] =  $val['last_recharge_date'];
			     $data['ExtinguisherReportRecord']['last_recharge_performed_by'] =  $val['last_recharge_performed_by'];
			     $data['ExtinguisherReportRecord']['last_hydrotest_date'] =  $val['last_hydrotest_date'];
			     $data['ExtinguisherReportRecord']['last_hydrotest_performed_by'] =  $val['last_hydrotest_performed_by'];
			     $this->ExtinguisherReportRecord->create();
			     $this->ExtinguisherReportRecord->save($data['ExtinguisherReportRecord'],false);
			}
			if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				// this is the function for sending mail after press finish button
				$this->SendMailAfterFinish($this->data['ExtinguisherReport']['client_id'],$this->data['ExtinguisherReport']['report_id'],$this->data['ExtinguisherReport']['finish_date']);
			}
				$action="Edited the extinguisher report";
				$this->saveLog($this->data['ExtinguisherReport']['sp_id'],$this->data['ExtinguisherReport']['report_id'],$this->data['ExtinguisherReport']['client_id'],$this->data['ExtinguisherReport']['servicecall_id'],$action);
			     $this->Session->setFlash('New record has been saved successfully.');
			     $this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
		    }
		}
	    }
	    else{	    
			$result = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
			$this->set('result',$result['ExtinguisherReport']);
			$data = $result['ExtinguisherReportRecord'];
			$this->set('data',$data);
	    }
	 }
/***************************************************************************************
	#THIS FUNCTION IS USED FOR extinguishers REPORTS FOR THE SP PANEL
	Reports controller extinguishers reportsfunction 
****************************************************************************************/ 		
	function sp_extinguishers(){
	   //$this->layout='inspector';
	   $this->loadModel('User');
	   $this->loadModel('ExtinguisherReport');
	   $this->loadModel('ScheduleService');
	   $this->loadModel('ExtinguisherReportRecord');
	   $this->loadModel('Schedule');
	   $this->loadModel('Attachment');
	   $this->loadModel('Notification');
	   $inspectorses = $this->Session->read('Log');
	   $inspector_id = $inspectorses['User']['id'];
	   if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);		
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		 
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
	   }
	    if(isset($this->data) && !empty($this->data)){
		
		if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				$this->data['ExtinguisherReport']['finish_date'] = date('Y-m-d H:i:s');
		}
		if(isset($this->data['ExtinguisherReport']['id']) && !empty($this->data['ExtinguisherReport']['id'])){
			$data['ExtinguisherReportRecord']['extinguisher_report_id'] = $this->data['ExtinguisherReport']['id'];
			$this->ExtinguisherReport->id=$this->data['ExtinguisherReport']['id'];
			if($this->ExtinguisherReport->save($this->data['ExtinguisherReport'],false))
		        {
			    if(isset($this->data['ExtinguisherReportRecord']) && !empty($this->data['ExtinguisherReportRecord']))
			    {
			    foreach($this->data['ExtinguisherReportRecord'] as $key=>$val)
			    {
				 $data['ExtinguisherReportRecord']['extinguisher_report_id']=$this->data['ExtinguisherReport']['id']; 
				 $data['ExtinguisherReportRecord']['location'] =  $val['location'];
				 $data['ExtinguisherReportRecord']['ext_type'] =  $val['ext_type'];
				 $data['ExtinguisherReportRecord']['model'] =  $val['model'];
				 $data['ExtinguisherReportRecord']['born_date'] =  $val['born_date'];
				 $data['ExtinguisherReportRecord']['deficiency'] =  $val['deficiency'];
				 $data['ExtinguisherReportRecord']['deficiency_reason'] =  $val['deficiency_reason'];
				 $data['ExtinguisherReportRecord']['last_maintenace_date'] =  $val['last_maintenace_date'];
				 $data['ExtinguisherReportRecord']['last_maintenace_performed_by'] =  $val['last_maintenace_performed_by'];
				 $data['ExtinguisherReportRecord']['last_recharge_date'] =  $val['last_recharge_date'];
				 $data['ExtinguisherReportRecord']['last_recharge_performed_by'] =  $val['last_recharge_performed_by'];
				 $data['ExtinguisherReportRecord']['last_hydrotest_date'] =  $val['last_hydrotest_date'];
				 $data['ExtinguisherReportRecord']['last_hydrotest_performed_by'] =  $val['last_hydrotest_performed_by'];
				 $this->ExtinguisherReportRecord->create();
				 $this->ExtinguisherReportRecord->save($data['ExtinguisherReportRecord'],false);
			    }
			    }
			     $this->Session->setFlash('New record has been saved successfully.');
			     $this->redirect(array('controller'=>'schedules','action'=>'completedschedule'));
			}
		}
		else{
		     if($this->ExtinguisherReport->save($this->data['ExtinguisherReport'],false))
		     {			
			foreach($this->data['ExtinguisherReportRecord'] as $key=>$val)
			{
			     $lastInsertId = $this->ExtinguisherReport->getLastInsertId();
			     $data['ExtinguisherReportRecord']['extinguisher_report_id'] = $lastInsertId;
			     $data['ExtinguisherReportRecord']['location'] =  $val['location'];
			     $data['ExtinguisherReportRecord']['ext_type'] =  $val['ext_type'];
			     $data['ExtinguisherReportRecord']['model'] =  $val['model'];
			     $data['ExtinguisherReportRecord']['born_date'] =  $val['born_date'];
			     $data['ExtinguisherReportRecord']['deficiency'] =  $val['deficiency'];
			     $data['ExtinguisherReportRecord']['deficiency_reason'] =  $val['deficiency_reason'];
			     $data['ExtinguisherReportRecord']['last_maintenace_date'] =  $val['last_maintenace_date'];
			     $data['ExtinguisherReportRecord']['last_maintenace_performed_by'] =  $val['last_maintenace_performed_by'];
			     $data['ExtinguisherReportRecord']['last_recharge_date'] =  $val['last_recharge_date'];
			     $data['ExtinguisherReportRecord']['last_recharge_performed_by'] =  $val['last_recharge_performed_by'];
			     $data['ExtinguisherReportRecord']['last_hydrotest_date'] =  $val['last_hydrotest_date'];
			     $data['ExtinguisherReportRecord']['last_hydrotest_performed_by'] =  $val['last_hydrotest_performed_by'];
			     $this->ExtinguisherReportRecord->create();
			     $this->ExtinguisherReportRecord->save($data['ExtinguisherReportRecord'],false);
			} 
			     $this->Session->setFlash('New record has been saved successfully.');
			     $this->redirect(array('controller'=>'schedules','action'=>'completedschedule'));
		    }
		}
	    }
	    else{	    
			$result = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
			$this->set('result',$result['ExtinguisherReport']);
			$data = $result['ExtinguisherReportRecord'];
			$this->set('data',$data);
	    }
	 }
/***************************************************************************************
	#THIS FUNCTION IS USED FOR delete extinguishers Record REPORTS
	Reports controller extinguishers reportsfunction 
****************************************************************************************/ 		
	 function delrecord($id = null){
		$this->layout = null;
		
		$this->loadModel('ExtinguisherReportRecord');
		$this->ExtinguisherReportRecord->bindModel(array('belongsTo'=>array('ExtinguisherReport'=>array('foreignKey'=>'extinguisher_report_id'))));
		$recordData = $this->ExtinguisherReportRecord->findById($id,array('ExtinguisherReport.servicecall_id','ExtinguisherReport.report_id'));
		$this->loadModel('Deficiency');
		$this->Deficiency->deleteAll(array('Deficiency.row_no'=>$id,'Deficiency.servicecall_id'=>$recordData['ExtinguisherReport']['servicecall_id'],'Deficiency.report_id'=>$recordData['ExtinguisherReport']['report_id']));
		
		if($this->ExtinguisherReportRecord->delete($id)){
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	 }
/***************************************************************************************
	#THIS FUNCTION IS USED FOR delete emergencyexit Record REPORTS
	Reports controller emergencyexit reportsfunction 
****************************************************************************************/ 		
	  function delexitrecord($id = null){
		$this->layout = null;
		$this->loadModel('EmergencyexitReportRecord');
		
		$this->EmergencyexitReportRecord->bindModel(array('belongsTo'=>array('EmergencyexitReport'=>array('foreignKey'=>'emergencyexit_report_id '))));
		$recordData = $this->EmergencyexitReportRecord->findById($id,array('EmergencyexitReport.servicecall_id','EmergencyexitReport.report_id'));
		$this->loadModel('Deficiency');
		$this->Deficiency->delete(array('Deficiency.row_no'=>$id,'Deficiency.servicecall_id'=>$recordData['EmergencyexitReport']['servicecall_id'],'Deficiency.report_id'=>$recordData['EmergencyexitReport']['report_id']));
		
		if($this->EmergencyexitReportRecord->delete($id)){
			echo "1";
		}
		else{
			echo "0";
		}
		exit;
	  }


/*****************************************************************************************
  * Ajax Upload attachment
*****************************************************************************************/
  function ajaxuploadAttachment(){
    $this->autoRender = false;
    if(isset($_FILES['qqfile'])){
	      $result = $this->Customupload->uploadattachment($_FILES['qqfile']);
	      if($result != "0") {
		    //echo '{"success":true,"attachment:test"}';
		    echo '{"success":true,"attachment":"'.$result.'"}';
	      }else{
		    echo '{"success":false}';
	      }
    }    
}
/***************************************************************************************
	#THIS FUNCTION IS USED FOR SPRINKLER REPORTS
	Reports controller sprinkler reportsfunction 
****************************************************************************************/ 	
	function inspector_sprinklerinspection($rid = null,$cid = null){
		$cid = base64_decode($cid);
		$rid = base64_decode($rid);
		$this->layout='inspector';
		$this->loadModel('SprinklerReport');
		$this->loadModel('SprinklerReportAlarm');
		$this->loadModel('SprinklerReportConnection');
		$this->loadModel('SprinklerReportControlvalve');
		$this->loadModel('SprinklerReportDrysystem');
		$this->loadModel('SprinklerReportGeneral');
		$this->loadModel('SprinklerReportPiping');
		$this->loadModel('SprinklerReportSpecialsystem');
		$this->loadModel('SprinklerReportWatersupply');
		$this->loadModel('SprinklerReportWetsystem');
		$this->loadModel('Attachment');
		$this->loadModel('Notification');
		$this->loadModel('Schedule');
		
		/*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/
		
		$daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
		$daysRemaining = $daysData[0]['days'];
		if($daysRemaining!=null || $daysRemaining!=""){
		}else{
		    $daysRemaining="";
		}
		$this->set(compact('daysRemaining'));
		
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		
		$this->set('session_inspector_id',$inspector_id);
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);
			$this->set('spId',$_REQUEST['spID']);
			$this->set('clientId',$_REQUEST['clientID']);
			$this->set('clientResult',$clientResult);
			$this->set('servicecallId',$_REQUEST['serviceCallID']);
			$this->set('inspector_id',$inspector_id);
			$this->set('reportId',$_REQUEST['reportID']);
			
			$Result = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $Result[0]['Schedule']['id'];
			
			$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('attachmentdata',$attachmentdata);
			$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('notifierdata',$notifierdata);
		}
		 if(isset($this->data) && !empty($this->data)){
			
			if(isset($this->params['form']['finish']) && ($this->params['form']['finish'] == "FINISH")){
				
				$this->loadModel('ReportCert');
				$chkCert = $this->ReportCert->find('count',array('conditions'=>array('ReportCert.schedule_id'=>$reqScheduleId,'ReportCert.ins_cert_form'=>'')));			
				if($chkCert>0){
					$this->Session->setFlash('Please attach all the required certificates');
					$this->redirect($this->referer());
				}
				
				/*Chk Signature*/
				$this->loadModel('Signature');
				$chkSign = $this->Signature->find('count',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
				if($chkSign==0){
					$this->Session->setFlash('Please add your signature first.');
					$this->redirect($this->referer());
				}
				/*Chk Signature*/				
				
				$this->data['SprinklerReport']['finish_date'] = date('Y-m-d H:i:s');
				$action='Finished the sprinkler inspection report';
			}
			if(isset($this->data['SprinklerReport']['id']) && !empty($this->data['SprinklerReport']['id'])){
				
				$this->SprinklerReport->id=$this->data['SprinklerReport']['id'];
				$this->SprinklerReport->save($this->data['SprinklerReport'],false);
				if(isset($this->data['SprinklerReportGeneral']['id']) && ($this->data['SprinklerReportGeneral']['id'] != "")){
					$this->data['SprinklerReportGeneral']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportGeneral->save($this->data['SprinklerReportGeneral'],false);
				 }
				 if(isset($this->data['SprinklerReportControlvalve']['id']) && ($this->data['SprinklerReportControlvalve']['id'] != "")){
					$this->data['SprinklerReportControlvalve']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportControlvalve->save($this->data['SprinklerReportControlvalve'],false);
				 }
				 if(isset($this->data['SprinklerReportWatersupply']['id']) && ($this->data['SprinklerReportWatersupply']['id'] != "")){
					$this->data['SprinklerReportWatersupply']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportWatersupply->save($this->data['SprinklerReportWatersupply'],false);
				 }
				 if(isset($this->data['SprinklerReportConnection']['id']) && ($this->data['SprinklerReportConnection']['id'] != "")){
					$this->data['SprinklerReportConnection']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportConnection->save($this->data['SprinklerReportConnection'],false);
				 }
				 if(isset($this->data['SprinklerReportWetsystem']['id']) && ($this->data['SprinklerReportWetsystem']['id'] != "")){
					$this->data['SprinklerReportWetsystem']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportWetsystem->save($this->data['SprinklerReportWetsystem'],false);
				 }
				 if(isset($this->data['SprinklerReportDrysystem']['id']) && ($this->data['SprinklerReportDrysystem']['id'] != "")){
					$this->data['SprinklerReportDrysystem']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportDrysystem->save($this->data['SprinklerReportDrysystem'],false);
				 }
				 if(isset($this->data['SprinklerReportSpecialsystem']['id']) && ($this->data['SprinklerReportSpecialsystem']['id'] != "")){
					$this->data['SprinklerReportSpecialsystem']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportSpecialsystem->save($this->data['SprinklerReportSpecialsystem'],false);
				 }
				 if(isset($this->data['SprinklerReportAlarm']['id']) && ($this->data['SprinklerReportAlarm']['id'] != "")){
					$this->data['SprinklerReportAlarm']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportAlarm->save($this->data['SprinklerReportAlarm'],false);
				 }
				 if(isset($this->data['SprinklerReportPiping']['id']) && ($this->data['SprinklerReportPiping']['id'] != "")){
					$this->data['SprinklerReportPiping']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportPiping->save($this->data['SprinklerReportPiping'],false);
				 }
				 if(!isset($action)){
					$action='Edited the sprinkler inspection report';
				 }
				 $this->saveLog($this->data['SprinklerReport']['sp_id'],$this->data['SprinklerReport']['report_id'],$this->data['SprinklerReport']['client_id'],$this->data['SprinklerReport']['servicecall_id'],$action);
					$this->Session->setFlash('Report has been updated successfully');
					$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
			}
			else{
				if($this->SprinklerReport->save($this->data['SprinklerReport'],false)){
				$lastInsertId = $this->SprinklerReport->getLastInsertId();
				 if(isset($this->data['SprinklerReportGeneral']) && ($this->data['SprinklerReportGeneral'] != "")){
					$this->data['SprinklerReportGeneral']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportGeneral->save($this->data['SprinklerReportGeneral'],false);
				 }
				 if(isset($this->data['SprinklerReportControlvalve']) && ($this->data['SprinklerReportControlvalve'] != "")){
					$this->data['SprinklerReportControlvalve']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportControlvalve->save($this->data['SprinklerReportControlvalve'],false);
				 }
				 if(isset($this->data['SprinklerReportWatersupply']) && ($this->data['SprinklerReportWatersupply'] != "")){
					$this->data['SprinklerReportWatersupply']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportWatersupply->save($this->data['SprinklerReportWatersupply'],false);
				 }
				 if(isset($this->data['SprinklerReportConnection']) && ($this->data['SprinklerReportConnection'] != "")){
					$this->data['SprinklerReportConnection']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportConnection->save($this->data['SprinklerReportConnection'],false);
				 }
				 if(isset($this->data['SprinklerReportWetsystem']) && ($this->data['SprinklerReportWetsystem'] != "")){
					$this->data['SprinklerReportWetsystem']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportWetsystem->save($this->data['SprinklerReportWetsystem'],false);
				 }
				 if(isset($this->data['SprinklerReportDrysystem']) && ($this->data['SprinklerReportDrysystem'] != "")){
					$this->data['SprinklerReportDrysystem']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportDrysystem->save($this->data['SprinklerReportDrysystem'],false);
				 }
				 if(isset($this->data['SprinklerReportSpecialsystem']) && ($this->data['SprinklerReportSpecialsystem'] != "")){
					$this->data['SprinklerReportSpecialsystem']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportSpecialsystem->save($this->data['SprinklerReportSpecialsystem'],false);
				 }
				 if(isset($this->data['SprinklerReportAlarm']) && ($this->data['SprinklerReportAlarm'] != "")){
					$this->data['SprinklerReportAlarm']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportAlarm->save($this->data['SprinklerReportAlarm'],false);
				 }
				 if(isset($this->data['SprinklerReportPiping']) && ($this->data['SprinklerReportPiping'] != "")){
					$this->data['SprinklerReportPiping']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportPiping->save($this->data['SprinklerReportPiping'],false);
				 }
				 $action='Edited the sprinkler inspection report';
					$this->saveLog($this->data['SprinklerReport']['sp_id'],$this->data['SprinklerReport']['report_id'],$this->data['SprinklerReport']['client_id'],$this->data['SprinklerReport']['servicecall_id'],$action);
					$this->Session->setFlash('Report has been saved successfully');
					$this->redirect(array('controller'=>'inspectors','action'=>'schedule'));
			 }
			}	 
		 }else{
			$loginuserinfo = $this->Session->read('Log');
			$result = $this->User->find('first',array('conditions'=>array('User.id'=>$loginuserinfo['User']['sp_id'])));
			$this->set('result',$result);
			$this->data = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
		 }
	}
	
/***************************************************************************************
	#THIS FUNCTION IS USED FOR SPRINKLER REPORTS IN SP PANEL
	Reports controller sprinkler reportsfunction FOR EDIT FUNCTIONALITY IN SP PANEL
****************************************************************************************/ 	
	function sp_sprinklerinspection($rid = null,$cid = null){
		$cid = base64_decode($cid);
		$rid = base64_decode($rid);
		$this->layout='sp';
		$this->loadModel('SprinklerReport');
		$this->loadModel('SprinklerReportAlarm');
		$this->loadModel('SprinklerReportConnection');
		$this->loadModel('SprinklerReportControlvalve');
		$this->loadModel('SprinklerReportDrysystem');
		$this->loadModel('SprinklerReportGeneral');
		$this->loadModel('SprinklerReportPiping');
		$this->loadModel('SprinklerReportSpecialsystem');
		$this->loadModel('SprinklerReportWatersupply');
		$this->loadModel('SprinklerReportWetsystem');
		$this->loadModel('Attachment');
		$this->loadModel('Notification');		
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);
			$this->set('spId',$_REQUEST['spID']);
			$this->set('clientId',$_REQUEST['clientID']);
			$this->set('clientResult',$clientResult);
			$this->set('servicecallId',$_REQUEST['serviceCallID']);
			//$this->set('inspector_id',$inspector_id);
			$this->set('reportId',$_REQUEST['reportID']);
			$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('attachmentdata',$attachmentdata);
			$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('notifierdata',$notifierdata);
		}
		 if(isset($this->data) && !empty($this->data)){
			if(isset($this->data['SprinklerReport']['id']) && !empty($this->data['SprinklerReport']['id'])){
				
				$this->SprinklerReport->id=$this->data['SprinklerReport']['id'];
				$this->SprinklerReport->save($this->data['SprinklerReport'],false);
				if(isset($this->data['SprinklerReportGeneral']['id']) && ($this->data['SprinklerReportGeneral']['id'] != "")){
					$this->data['SprinklerReportGeneral']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportGeneral->save($this->data['SprinklerReportGeneral'],false);
				 }
				 if(isset($this->data['SprinklerReportControlvalve']['id']) && ($this->data['SprinklerReportControlvalve']['id'] != "")){
					$this->data['SprinklerReportControlvalve']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportControlvalve->save($this->data['SprinklerReportControlvalve'],false);
				 }
				 if(isset($this->data['SprinklerReportWatersupply']['id']) && ($this->data['SprinklerReportWatersupply']['id'] != "")){
					$this->data['SprinklerReportWatersupply']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportWatersupply->save($this->data['SprinklerReportWatersupply'],false);
				 }
				 if(isset($this->data['SprinklerReportConnection']['id']) && ($this->data['SprinklerReportConnection']['id'] != "")){
					$this->data['SprinklerReportConnection']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportConnection->save($this->data['SprinklerReportConnection'],false);
				 }
				 if(isset($this->data['SprinklerReportWetsystem']['id']) && ($this->data['SprinklerReportWetsystem']['id'] != "")){
					$this->data['SprinklerReportWetsystem']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportWetsystem->save($this->data['SprinklerReportWetsystem'],false);
				 }
				 if(isset($this->data['SprinklerReportDrysystem']['id']) && ($this->data['SprinklerReportDrysystem']['id'] != "")){
					$this->data['SprinklerReportDrysystem']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportDrysystem->save($this->data['SprinklerReportDrysystem'],false);
				 }
				 if(isset($this->data['SprinklerReportSpecialsystem']['id']) && ($this->data['SprinklerReportSpecialsystem']['id'] != "")){
					$this->data['SprinklerReportSpecialsystem']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportSpecialsystem->save($this->data['SprinklerReportSpecialsystem'],false);
				 }
				 if(isset($this->data['SprinklerReportAlarm']['id']) && ($this->data['SprinklerReportAlarm']['id'] != "")){
					$this->data['SprinklerReportAlarm']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportAlarm->save($this->data['SprinklerReportAlarm'],false);
				 }
				 if(isset($this->data['SprinklerReportPiping']['id']) && ($this->data['SprinklerReportPiping']['id'] != "")){
					$this->data['SprinklerReportPiping']['sprinkler_report_id'] = $this->data['SprinklerReport']['id'];
					$this->SprinklerReportPiping->save($this->data['SprinklerReportPiping'],false);
				 }
					$this->Session->setFlash('Report has been updated successfully');
					$this->redirect(array('controller'=>'schedules','action'=>'completedschedule'));
			}
			else{
				if($this->SprinklerReport->save($this->data['SprinklerReport'],false)){
				$lastInsertId = $this->SprinklerReport->getLastInsertId();
				 if(isset($this->data['SprinklerReportGeneral']) && ($this->data['SprinklerReportGeneral'] != "")){
					$this->data['SprinklerReportGeneral']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportGeneral->save($this->data['SprinklerReportGeneral'],false);
				 }
				 if(isset($this->data['SprinklerReportControlvalve']) && ($this->data['SprinklerReportControlvalve'] != "")){
					$this->data['SprinklerReportControlvalve']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportControlvalve->save($this->data['SprinklerReportControlvalve'],false);
				 }
				 if(isset($this->data['SprinklerReportWatersupply']) && ($this->data['SprinklerReportWatersupply'] != "")){
					$this->data['SprinklerReportWatersupply']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportWatersupply->save($this->data['SprinklerReportWatersupply'],false);
				 }
				 if(isset($this->data['SprinklerReportConnection']) && ($this->data['SprinklerReportConnection'] != "")){
					$this->data['SprinklerReportConnection']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportConnection->save($this->data['SprinklerReportConnection'],false);
				 }
				 if(isset($this->data['SprinklerReportWetsystem']) && ($this->data['SprinklerReportWetsystem'] != "")){
					$this->data['SprinklerReportWetsystem']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportWetsystem->save($this->data['SprinklerReportWetsystem'],false);
				 }
				 if(isset($this->data['SprinklerReportDrysystem']) && ($this->data['SprinklerReportDrysystem'] != "")){
					$this->data['SprinklerReportDrysystem']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportDrysystem->save($this->data['SprinklerReportDrysystem'],false);
				 }
				 if(isset($this->data['SprinklerReportSpecialsystem']) && ($this->data['SprinklerReportSpecialsystem'] != "")){
					$this->data['SprinklerReportSpecialsystem']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportSpecialsystem->save($this->data['SprinklerReportSpecialsystem'],false);
				 }
				 if(isset($this->data['SprinklerReportAlarm']) && ($this->data['SprinklerReportAlarm'] != "")){
					$this->data['SprinklerReportAlarm']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportAlarm->save($this->data['SprinklerReportAlarm'],false);
				 }
				 if(isset($this->data['SprinklerReportPiping']) && ($this->data['SprinklerReportPiping'] != "")){
					$this->data['SprinklerReportPiping']['sprinkler_report_id'] = $lastInsertId;
					$this->SprinklerReportPiping->save($this->data['SprinklerReportPiping'],false);
				 }
					$this->Session->setFlash('Report has been saved successfully');
					$this->redirect(array('controller'=>'schedules','action'=>'completedschedule'));
			 }
			}	 
		 }else{
			$loginuserinfo = $this->Session->read('Log');
			$result = $this->User->find('first',array('conditions'=>array('User.id'=>$loginuserinfo['User']['sp_id'])));
			$this->set('result',$result);
			$this->data = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
		 }
	}	
/* 
*************************************************************************
*Function Name		 :	sp_reportlisting
*Functionality		 :	Functionality to  use  listing  for various reports
*Created                 :      Amit Kumar
**************************************************************************/
	function sp_reportlisting(){
	  $this->loadModel('Report');
	  $this->Report->recursive=-1;
	  $result = $this->Report->find('all',array('order'=>'Report.id DESC'));
	  $this->set('result',$result);	
        }		
/* 
*************************************************************************
*Function Name		 :	sp_reportdetail
*Functionality		 :	Functionality to  use  for listing  sevices for various reports
*Created                 :      Amit Kumar
*************************************************************************
*/			
	function sp_reportdetail($report_id=null){
		$this->loadModel('Service');
		$report_id=base64_decode($report_id);
		$result = $this->Service->find('all',array('conditions'=>array('Service.report_id'=>$report_id)));
		$this->set('result',$result);		
	}				
/* 
*************************************************************************
*Function Name		 :	sp_addReport
*Functionality		 :	Functionality to  use  for add new  reports
*Created                 :      Amit Kumar
*************************************************************************/	
	function sp_addReport(){
			$this->loadModel('Report');         
			if(isset($this->data) && !empty($this->data)){
			$sp = $this->Session->read('Log');
			$spId = $sp['User']['id'];  
			$this->data['Report']['sp_id'] = $spId;  
			$this->Report->set($this->data['User']);	    
			if($this->Report->validates()){      
				$this->Report->save($this->data);
				$this->Session->setFlash('New report has been saved successfully.');
				$this->redirect(array('controller'=>'reports','action'=>'reportlisting'));
			}
	 }		    						    
	}	

/***************************************************************************
*Function Name		 :	EditInfo
*Functionality		 :	use to Edit information of report section.
*created                 :      Amit kumar
***************************************************************************
*/
	function sp_editReport($id=null){
	      $this->loadModel('Report');
	  if(isset($this->data) && !empty($this->data)){
		$this->Report->set($this->data['User']);  
		  if($this->Report->validates()){
		    $this->Report->save($this->data);
		    $this->Session->setFlash('Report has been updated successfully.');
		    $this->redirect(array('controller'=>'reports','action'=>'reportlisting'));
		  }
		}else{
		    $this->Report->id = base64_decode($id);
		    $this->data = $this->Report->read();
	       }
	}
/***************************************************************************************
	#THIS FUNCTION IS USED FOR Emergency REPORTS
	Reports controller sprinkler reportsfunction 
****************************************************************************************/ 	
	function inspector_emergency(){
	   $this->layout='inspector';	
	}
	
/***************************************************************************************
	#THIS FUNCTION IS USED FOR EXTINGUISHER 
	Function: inspector_extinguisher() 
****************************************************************************************/ 	
  function inspector_extinguisher(){
  	   $this->layout='inspector';
	   
  }

/***************************************************************************************
	#THIS FUNCTION IS USED FOR extinguisher view in sp panel
	Function: sp_extinguisherView() 
****************************************************************************************/
function sp_extinguisherView(){
	$this->layout='';
	$this->loadModel('User');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');
	$this->loadModel('ExtinguisherReport');
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		//$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
		$record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('record',$record);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		
		#CODE for deficiency page in view
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
									'Code'=> array(
												'className' => 'Code',
												'foreignKey' => 'code_id',
												'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
												)
									)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
		//pr($Def_record);die;
	   }
	   $this->viewPath = 'reports';
	   $this->render('inspector_extinguisherView');
  }
/***************************************************************************************
	#THIS FUNCTION IS USED FOR extinguisher view in INSPECTOR panel
	Function: sp_extinguisherView() 
****************************************************************************************/  
  function inspector_extinguisherView(){
	$this->layout='';
	$inspectorses = $this->Session->read('Log');
	$inspector_id = $inspectorses['User']['id'];
	$this->loadModel('User');
	$this->loadModel('Schedule');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');
	$this->loadModel('ExtinguisherReport');
	$this->loadModel('Service');
	$this->loadModel('Quote');
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code for quotes		
		$chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
		$this->set('chkRecord',$chkRecord);
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
		
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
		$record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('record',$record);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		
		// start of script for find the summary page
		$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		$reqScheduleId = $schedule[0]['Schedule']['id'];
		$this->set('schId',$reqScheduleId);
		$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
		// pr($schedule);
		$result = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
		 //pr()
		 foreach($result['ExtinguisherReportRecord'] as $key1=>$record){
			     if($record['deficiency'] == "Yes"){
					$arr1[$record['ext_type']]['fail'][$key1] = 1;
				 }
				 if($record['deficiency'] == "No"){
					$arr1[$record['ext_type']]['pass'][$key1] = 1;
				 }				  
					$arr[$key1] = $record['ext_type'];
			}
		$arr_count = array_count_values($arr);
		$options = array();
		$val = "";
		 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
			$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			$options[$key]['name'] = $ServiceOptions['Service']['name'];
			$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
			$options[$key]['amount'] = $schedule['amount'];
		    $options[$key]['amount'] = $schedule['amount'];
			if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
				$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
			}else{
				$options[$key]['served'] = 0;
			}
			 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
				 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
					$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
				 }
				 else{
					$options[$key]['pass'] = 0;	
				 }
				if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
					$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
				 }else{
					$options[$key]['fail'] = 0;	
				}	
			 }else{
					$options[$key]['pass'] = 0;	
					$options[$key]['fail'] = 0;	
			 }
		}
		$this->set('options',$options);
		//End of script of sumary page
		#CODE for deficiency page in view
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
									'Code'=> array(
												'className' => 'Code',
												'foreignKey' => 'code_id',
												'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
												)
									)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
		
	   }
  }



/***************************************************************************************
	function for sprinkler report view for CLient Panel
****************************************************************************************/ 
     function client_sprinklerinspection(){
        $this->layout='';      	
      	$this->loadModel('User');
      	$this->loadModel('Attachment');
      	$this->loadModel('Notification');
      	$this->loadModel('SprinklerReport');
      	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
      		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
      		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
      		$this->set('spResult',$spResult);
      		$this->set('spId',$_REQUEST['spID']);
      		$this->set('clientId',$_REQUEST['clientID']);
      		$this->set('clientResult',$clientResult);
      		$this->set('servicecallId',$_REQUEST['serviceCallID']);      		
      		$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
      		$record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
      		$this->set('record',$record);
      		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
			#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
														'Code'=> array(
																	'className' => 'Code',
																	'foreignKey' => 'code_id',
																	'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																	)
														)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
		$this->viewPath = 'reports';
		$this->render('inspector_sprinklerView');
        }
     }
/***************************************************************************************
	function for sprinkler report view for SP Panel
****************************************************************************************/ 
     function sp_sprinklerView(){
        $this->layout='';      	
      	$this->loadModel('User');
      	$this->loadModel('Attachment');
      	$this->loadModel('Notification');
      	$this->loadModel('SprinklerReport');
      	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
      		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
      		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
      		$this->set('spResult',$spResult);
      		$this->set('spId',$_REQUEST['spID']);
      		$this->set('clientId',$_REQUEST['clientID']);
      		$this->set('clientResult',$clientResult);
      		$this->set('servicecallId',$_REQUEST['serviceCallID']);      		
      		$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
      		$record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
      		$this->set('record',$record);
      		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
			#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
														'Code'=> array(
																	'className' => 'Code',
																	'foreignKey' => 'code_id',
																	'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																	)
														)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
		$this->viewPath = 'reports';
		$this->render('inspector_sprinklerView');
        }
     }

/***************************************************************************************
	#THIS FUNCTION IS USED AS EXTINGUISHER REPORT VIEW for CLIENT PANEL 
	Function: client_extinguisher() 
****************************************************************************************/  
  function client_extinguishers(){
	$this->layout='';
	$this->loadModel('User');
	$this->loadModel('Attachment');
	$this->loadModel('Notification');
	$this->loadModel('ExtinguisherReport');
	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('spResult',$spResult);
		$this->set('spId',$_REQUEST['spID']);
		$this->set('clientId',$_REQUEST['clientID']);
		$this->set('clientResult',$clientResult);
		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		//$this->set('inspector_id',$inspector_id);
		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
		$record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('record',$record);
		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		
		#CODE for deficiency page in view
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
									'Code'=> array(
												'className' => 'Code',
												'foreignKey' => 'code_id',
												'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
												)
									)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
	   }
	   $this->viewPath = 'reports';
	   $this->render('inspector_extinguisherView');
  }

# function to add fire alarm attachment  
	function inspector_firealarm_attachment(){
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('FirealarmReportAttachment');		
		//$this->loadModel('FirealarmReportNotification');
						
		 if(isset($this->data) && !empty($this->data))
		 {			
		   $this->data['FirealarmReportAttachment']['user_id'] = $inspector_id;
		   $this->data['FirealarmReportAttachment']['added_by'] = 'Inspector';
		   if($this->FirealarmReportAttachment->save($this->data['FirealarmReportAttachment']))
		   {
			$action='Added a new attachment for firealarm report';
			$this->saveLog($this->data['FirealarmReportAttachment']['sp_id'],$this->data['FirealarmReportAttachment']['report_id'],$this->data['FirealarmReportAttachment']['client_id'],$this->data['FirealarmReportAttachment']['servicecall_id'],$action);
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/inspector/reports/firealarm?reportID='.$this->data['FirealarmReportAttachment']['report_id'].'&clientID='.$this->data['FirealarmReportAttachment']['client_id'].'&spID='.$this->data['FirealarmReportAttachment']['sp_id'].'&serviceCallID='.$this->data['FirealarmReportAttachment']['servicecall_id']);
		   }
		 }
		
	}
	function inspector_firealarm_notification()
	{
		
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('FirealarmReportNotification');
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['FirealarmReportNotification']['user_id'] = $inspector_id;
		   if($this->FirealarmReportNotification->save($this->data['FirealarmReportNotification']))
		   {
			$action='Added a new notification for firealarm report';
			$this->saveLog($this->data['FirealarmReportNotification']['sp_id'],$this->data['FirealarmReportNotification']['report_id'],$this->data['FirealarmReportNotification']['client_id'],$this->data['FirealarmReportNotification']['servicecall_id'],$action);
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/inspector/reports/firealarm?reportID='.$this->data['FirealarmReportNotification']['report_id'].'&clientID='.$this->data['FirealarmReportNotification']['client_id'].'&spID='.$this->data['FirealarmReportNotification']['sp_id'].'&serviceCallID='.$this->data['FirealarmReportNotification']['servicecall_id']);
		   }
		 }
	}
# function to add fire alarm attachment  
	function sp_firealarm_attachment(){
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('FirealarmReportAttachment');		
		//$this->loadModel('FirealarmReportNotification');
						
		 if(isset($this->data) && !empty($this->data))
		 {			
		   $this->data['FirealarmReportAttachment']['user_id'] = $inspector_id;
		   $this->data['FirealarmReportAttachment']['added_by'] = 'SP';
		   if($this->FirealarmReportAttachment->save($this->data['FirealarmReportAttachment']))
		   {
			$action='Added a new attachment for firealarm report';
			$this->saveLog($this->data['FirealarmReportAttachment']['sp_id'],$this->data['FirealarmReportAttachment']['report_id'],$this->data['FirealarmReportAttachment']['client_id'],$this->data['FirealarmReportAttachment']['servicecall_id'],$action);
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/sp/reports/firealarm?reportID='.$this->data['FirealarmReportAttachment']['report_id'].'&clientID='.$this->data['FirealarmReportAttachment']['client_id'].'&spID='.$this->data['FirealarmReportAttachment']['sp_id'].'&serviceCallID='.$this->data['FirealarmReportAttachment']['servicecall_id']);
		   }
		 }
		
	}
	function sp_firealarm_notification()
	{
		
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('FirealarmReportNotification');
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['FirealarmReportNotification']['user_id'] = $inspector_id;
		     $this->data['FirealarmReportNotification']['added_by'] = 'SP';
		   if($this->FirealarmReportNotification->save($this->data['FirealarmReportNotification']))
		   {
			$action='Added a new notification for firealarm report';
			$this->saveLog($this->data['FirealarmReportNotification']['sp_id'],$this->data['FirealarmReportNotification']['report_id'],$this->data['FirealarmReportNotification']['client_id'],$this->data['FirealarmReportNotification']['servicecall_id'],$action);
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/sp/reports/firealarm?reportID='.$this->data['FirealarmReportNotification']['report_id'].'&clientID='.$this->data['FirealarmReportNotification']['client_id'].'&spID='.$this->data['FirealarmReportNotification']['sp_id'].'&serviceCallID='.$this->data['FirealarmReportNotification']['servicecall_id']);
		   }
		 }
	}		
	

/***************************************************************************************
	#THIS FUNCTION IS USED In fire alarm REPORT FOR ATTACHMENT DELETION 	
****************************************************************************************/ 	    
	function delfirealarmattachment($id = null){
		$this->layout = null;
		$this->loadModel('FirealarmReportAttachment');
		$this->loadModel('ServiceCall');		
		$data=$this->FirealarmReportAttachment->find('first',array('conditions'=>array('id'=>$id)));	
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['FirealarmReportAttachment']['servicecall_id'])));	
		$action='deleted an attachment of kitchenhood report';		
		# function saveLog to Maintain inspector's log
		$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['FirealarmReportAttachment']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['FirealarmReportAttachment']['servicecall_id'],$action);
		if(file_exists(WWW_ROOT."img/uploaded_attachments/".$data['FirealarmReportAttachment']['attach_file'])){			
		  unlink(WWW_ROOT."img/uploaded_attachments/".$data['FirealarmReportAttachment']['attach_file']); 		}		
		if($this->FirealarmReportAttachment->delete($id)){
			
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	} 
/***************************************************************************************
	#THIS FUNCTION IS USED In fire alarm REPORT FOR NOTIFICATION DELETION 
	
****************************************************************************************/ 	  
	function delfirealarmnotifier($id = null){
		$this->layout = null;
		$this->loadModel('FirealarmReportNotification');
		$this->loadModel('ServiceCall');
		$data=$this->FirealarmReportNotification->find('first',array('conditions'=>array('id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['FirealarmReportNotification']['servicecall_id'])));	
		$action='deleted a notification of firealarm report';
		# function saveLog to Maintain inspector's log
		$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['FirealarmReportNotification']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['FirealarmReportNotification']['servicecall_id'],$action);
		if($this->FirealarmReportNotification->delete($id)){			
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	}
	function inspector_attachment()
	{
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('Attachment');				
		 if(isset($this->data) && !empty($this->data))
		 {		
		   $this->data['Attachment']['user_id'] = $inspector_id;
		   $this->data['Attachment']['added_by'] = 'Inspector';
		   if($this->Attachment->save($this->data['Attachment']))
		   {
			if($this->data['Attachment']['report_id'] == 1){
				$action='Added a new attachment for firealarm report';
			}
			if($this->data['Attachment']['report_id'] == 2){
				$action='Added a new attachment for sprinkler report';
			}
			if($this->data['Attachment']['report_id'] == 3){
				$action='Added a new attachment for kitchenhood report';
			}
			if($this->data['Attachment']['report_id'] == 4){
				$action='Added a new attachment for emergencyexit report';
			}
			if($this->data['Attachment']['report_id'] == 5){
				$action='Added a new attachment for extinguisher report';
			}
			if($this->data['Attachment']['report_id'] == 6){
				$action='Added a new attachment for special hazard report';
			}
			$this->saveLog($this->data['Attachment']['sp_id'],$this->data['Attachment']['report_id'],$this->data['Attachment']['client_id'],$this->data['Attachment']['servicecall_id'],$action);
			$this->Session->setFlash('attachment has been uploaded successfully.');
			if($this->data['Attachment']['report_id'] == 1){
				$this->redirect('/inspector/reports/firealarm?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 2){
				$this->redirect('/inspector/reports/sprinklerinspection?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 3){
				$this->redirect('/inspector/reports/kitchenhood?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 4){
				$this->redirect('/inspector/reports/emergencyexit?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 5){
				$this->redirect('/inspector/reports/extinguishers?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 6){
				$this->redirect('/inspector/reports/specialhazard?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
		   }
		 }
	}

	function inspector_notification()
	{
		$this->loadModel('Notification');
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('Notification');
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['Notification']['user_id'] = $inspector_id;
		   if($this->Notification->save($this->data['Notification']))
		   {
			if($this->data['Notification']['report_id'] == 1){
				$action='Added a new notification for firealarm report';
			}
			if($this->data['Notification']['report_id'] == 2){
				$action='Added a new notification for sprinkler report';
			}
			if($this->data['Notification']['report_id'] == 3){
				$action='Added a new notification for kitchenhood report';
			}
			if($this->data['Notification']['report_id'] == 4){
				$action='Added a new notification for emergencyexit report';
			}
			if($this->data['Notification']['report_id'] == 5){
				$action='Added a new notification for extinguisher report';
			}
			if($this->data['Notification']['report_id'] == 6){
				$action='Added a new notification for special hazard report';
			}
			
			$this->saveLog($this->data['Notification']['sp_id'],$this->data['Notification']['report_id'],$this->data['Notification']['client_id'],$this->data['Notification']['servicecall_id'],$action);
			$this->Session->setFlash('Notification has been saved successfully.');
			if($this->data['Notification']['report_id'] == 1){
				$this->redirect('/inspector/reports/firealarm?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 2){
				$this->redirect('/inspector/reports/sprinklerinspection?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 3){
				$this->redirect('/inspector/reports/kitchenhood?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 4){
				$this->redirect('/inspector/reports/emergencyexit?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 5){
				$this->redirect('/inspector/reports/extinguishers?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 6){
				$this->redirect('/inspector/reports/specialhazard?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
		   }
		 }
	}	  

/***************************************************************************************
	#THIS FUNCTION IS USED In kitchenhood REPORT FOR ATTACHMENT DELETION 
	Function: delkitchenhoodattachment() 
****************************************************************************************/ 	    
	function delattachment($id = null){
		$this->layout = null;
		$this->loadModel('Attachment');
		$this->loadModel('ServiceCall');
		$loginuser = $this->Session->read('Log');
		$checkId = $loginuser['User']['user_type_id'];
		$data=$this->Attachment->find('first',array('conditions'=>array('id'=>$id)));	
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['Attachment']['servicecall_id'])));	
		if($checkId == 2){
			if($data['Attachment']['report_id'] == 1){
				$action='deleted an attachment of firealarm report';
			}
			if($data['Attachment']['report_id'] == 2){
				$action='deleted an attachment of sprinkler report';
			}
			if($data['Attachment']['report_id'] == 3){
				$action='deleted an attachment of kitchenhood report';
			}
			if($data['Attachment']['report_id'] == 4){
				$action='deleted an attachment of emergencyexit report';
			}
			if($data['Attachment']['report_id'] == 5){
				$action='deleted an attachment of extinguisher report';
			}
			if($data['Attachment']['report_id'] == 6){
				$action='deleted an attachment of special hazard report';
			}
			
			# function saveLog to Maintain inspector's log
			$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['Attachment']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['Attachment']['servicecall_id'],$action);
		}
		if(file_exists(WWW_ROOT."img/uploaded_attachments/".$data['Attachment']['attach_file'])){			
		  unlink(WWW_ROOT."img/uploaded_attachments/".$data['Attachment']['attach_file']); 		}		
		if($this->Attachment->delete($id)){
			
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	} 
/***************************************************************************************
	#THIS FUNCTION IS USED In kitchenhood REPORT FOR NOTIFICATION DELETION 
	Function: delkitchenhoodnotifier() 
****************************************************************************************/ 	  
	function delnotifier($id = null){
		$this->layout = null;
		$this->loadModel('Notification');
		$this->loadModel('ServiceCall');
		$loginuser = $this->Session->read('Log');
		$checkId = $loginuser['User']['user_type_id'];
		$data=$this->Notification->find('first',array('conditions'=>array('id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['Notification']['servicecall_id'])));	
		if($checkId == 2){
			if($data['Notification']['report_id'] == 1){
				$action='deleted a notification of firealarm report';
			}
			if($data['Notification']['report_id'] == 2){
				$action='deleted a notification of sprinkler report';
			}
			if($data['Notification']['report_id'] == 3){
				$action='deleted a notification of kitchenhood report';
			}
			if($data['Notification']['report_id'] == 4){
				$action='deleted a notification of emergencyexit report';
			}
			if($data['Notification']['report_id'] == 5){
				$action='deleted a notification of extinguisher report';
			}
			if($data['Notification']['report_id'] == 6){
				$action='deleted a notification of special hazard report';
			}
			# function saveLog to Maintain inspector's log
			$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['Notification']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['Notification']['servicecall_id'],$action);
		}
		if($this->Notification->delete($id)){			
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	}

/***************************************************************************************
	#THIS FUNCTION IS USED to add attacment in sp panel 
	
****************************************************************************************/ 	  
	function sp_attachment()
	{
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('Attachment');				
		 if(isset($this->data) && !empty($this->data))
		 {		
		   $this->data['Attachment']['user_id'] = $inspector_id;
		   $this->data['Attachment']['added_by'] = 'SP';
		   if($this->Attachment->save($this->data['Attachment']))
		   {
			
			$this->Session->setFlash('attachment has been uploaded successfully.');
			if($this->data['Attachment']['report_id'] == 1){
				$this->redirect('/sp/reports/firealarm?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 2){
				$this->redirect('/sp/reports/sprinklerinspection?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 3){
				$this->redirect('/sp/reports/kitchenhood?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 4){
				$this->redirect('/sp/reports/emergencyexit?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 5){
				$this->redirect('/sp/reports/extinguishers?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			if($this->data['Attachment']['report_id'] == 6){
				$this->redirect('/sp/reports/specialhazard?reportID='.$this->data['Attachment']['report_id'].'&clientID='.$this->data['Attachment']['client_id'].'&spID='.$this->data['Attachment']['sp_id'].'&serviceCallID='.$this->data['Attachment']['servicecall_id']);
			}
			
		   }
		 }
	}

	function sp_notification()
	{
		$this->loadModel('Notification');
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('Notification');
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['Notification']['user_id'] = $inspector_id;
		   $this->data['Notification']['added_by'] = 'SP';
		   if($this->Notification->save($this->data['Notification']))
		   {
			
			$this->Session->setFlash('Notification has been saved successfully.');
			if($this->data['Notification']['report_id'] == 1){
				$this->redirect('/sp/reports/firealarm?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 2){
				$this->redirect('/sp/reports/sprinklerinspection?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 3){
				$this->redirect('/sp/reports/kitchenhood?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 4){
				$this->redirect('/sp/reports/emergencyexit?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 5){
				$this->redirect('/sp/reports/extinguishers?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
			if($this->data['Notification']['report_id'] == 6){
				$this->redirect('/sp/reports/specialhazard?reportID='.$this->data['Notification']['report_id'].'&clientID='.$this->data['Notification']['client_id'].'&spID='.$this->data['Notification']['sp_id'].'&serviceCallID='.$this->data['Notification']['servicecall_id']);
			}
		   }
		 }
	}	  

/***************************************************************************************
	#THIS FUNCTION IS USED In EXTINGUISHER REPORT FOR ATTACHMENT DELETION 
	Function: delextinguisherattachment() 
****************************************************************************************/
	function delextinguisherattachment($id = null){
		$this->layout = null;
		$this->loadModel('ExtinguisherReportAttachment');
		$this->loadModel('ServiceCall');		
		$data=$this->ExtinguisherReportAttachment->find('first',array('conditions'=>array('ExtinguisherReportAttachment.id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['ExtinguisherReportAttachment']['servicecall_id'])));	
		$action='deleted an attachment of extinguisher report';		
		
		if(file_exists(WWW_ROOT."img/uploaded_attachments/".$data['ExtinguisherReportAttachment']['attach_file'])){
		  unlink(WWW_ROOT."img/uploaded_attachments/".$data['ExtinguisherReportAttachment']['attach_file']); 
		}
		if($this->ExtinguisherReportAttachment->delete($id)){
			# function saveLog to Maintain inspector's log
		$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['ExtinguisherReportAttachment']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['ExtinguisherReportAttachment']['servicecall_id'],$action);
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	}

/***************************************************************************************
	#THIS FUNCTION IS USED In EXTINGUISHER REPORT FOR NOTIFICATION DELETION 
	Function: delextinguishernotifier() 
****************************************************************************************/ 	  
	function delextinguishernotifier($id = null){
		$this->layout = null;
		$this->loadModel('ExtinguisherReportNotification');
		$this->loadModel('ServiceCall');
		$data=$this->ExtinguisherReportNotification->find('first',array('conditions'=>array('ExtinguisherReportNotification.id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['ExtinguisherReportNotification']['servicecall_id'])));	
		$action='deleted a notification of extinguisher report';
		# function saveLog to Maintain inspector's log
		$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['ExtinguisherReportNotification']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['ExtinguisherReportNotification']['servicecall_id'],$action);
		if($this->ExtinguisherReportNotification->delete($id)){
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	}
/***************************************************************************************
	#THIS FUNCTION IS USED In EMERGENCYEXIT REPORT FOR ADDDING NEW ATTACHMENT 
	Function: inspector_emergency_attachment() 
****************************************************************************************/ 
	  function inspector_emergency_attachment()
	  {
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('EmergencyexitReportAttachment');		
		 if(isset($this->data) && !empty($this->data))
		 { 
		   $this->data['EmergencyexitReportAttachment']['user_id'] = $inspector_id;
		   $this->data['EmergencyexitReportAttachment']['added_by'] = 'Inspector';
		   if($this->EmergencyexitReportAttachment->save($this->data['EmergencyexitReportAttachment']))
		   {
			$action='Added a new attachment for emergency exit report';
			$this->saveLog($this->data['EmergencyexitReportAttachment']['sp_id'],$this->data['EmergencyexitReportAttachment']['report_id'],$this->data['EmergencyexitReportAttachment']['client_id'],$this->data['EmergencyexitReportAttachment']['servicecall_id'],$action);
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/inspector/reports/emergencyexit?reportID='.$this->data['EmergencyexitReportAttachment']['report_id'].'&clientID='.$this->data['EmergencyexitReportAttachment']['client_id'].'&spID='.$this->data['EmergencyexitReportAttachment']['sp_id'].'&serviceCallID='.$this->data['EmergencyexitReportAttachment']['servicecall_id']);
		   }
		 }
	  }	  
/***************************************************************************************
	#THIS FUNCTION IS USED In EMERGENCYEXIT REPORT FOR ADDDING NEW NOTIFICATION 
	Function: inspector_emergency_notification() 
****************************************************************************************/ 	  
	  function inspector_emergency_notification()
	  {
		$this->loadModel('EmergencyexitReportNotification');
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
	
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['EmergencyexitReportNotification']['user_id'] = $inspector_id;
		   if($this->EmergencyexitReportNotification->save($this->data['EmergencyexitReportNotification']))
		   {
			$action='Added a new notification for emergency exit report';
			$this->saveLog($this->data['EmergencyexitReportNotification']['sp_id'],$this->data['EmergencyexitReportNotification']['report_id'],$this->data['EmergencyexitReportNotification']['client_id'],$this->data['EmergencyexitReportNotification']['servicecall_id'],$action);
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/inspector/reports/emergencyexit?reportID='.$this->data['EmergencyexitReportNotification']['report_id'].'&clientID='.$this->data['EmergencyexitReportNotification']['client_id'].'&spID='.$this->data['EmergencyexitReportNotification']['sp_id'].'&serviceCallID='.$this->data['EmergencyexitReportNotification']['servicecall_id']);
		   }
		 }
	  }
/***************************************************************************************
	#THIS FUNCTION IS USED In SPECIALHAZARD REPORT FOR ADDDING NEW NOTIFICATION 
	Function: inspector_specialhazard_notification() 
****************************************************************************************/
	  function inspector_specialhazard_notification()
	  {
		$this->loadModel('SpecialhazardReportNotification');
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		//$this->loadModel('ServiceCall');
		//$data=$this->SpecialhazardReportNotification->find('first',array('conditions'=>array('SpecialhazardReportNotification.id'=>$id)));
		//$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['SpecialhazardReportNotification']['servicecall_id'])));	
		//$action='uploaded an attachement for special hazard report';
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['SpecialhazardReportNotification']['user_id'] = $inspector_id;
		   if($this->SpecialhazardReportNotification->save($this->data['SpecialhazardReportNotification']))
		   {
			$action='added a notification for special hazard report';
			$this->saveLog($this->data['SpecialhazardReportNotification']['sp_id'],$this->data['SpecialhazardReportNotification']['report_id'],$this->data['SpecialhazardReportNotification']['client_id'],$this->data['SpecialhazardReportNotification']['servicecall_id'],$action);
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/inspector/reports/specialhazard?reportID='.$this->data['SpecialhazardReportNotification']['report_id'].'&clientID='.$this->data['SpecialhazardReportNotification']['client_id'].'&spID='.$this->data['SpecialhazardReportNotification']['sp_id'].'&serviceCallID='.$this->data['SpecialhazardReportNotification']['servicecall_id']);
		   }
		 }
	  }
/***************************************************************************************
	#THIS FUNCTION IS USED In EMERGENCYEXIT REPORT FOR ATTACHMENT DELETION 
	Function: delemergencyattachment() 
****************************************************************************************/ 	  
	function delemergencyattachment($id = null){
		$this->layout = null;
		$this->loadModel('EmergencyexitReportAttachment');
		$this->loadModel('ServiceCall');
		$data=$this->EmergencyexitReportAttachment->find('first',array('conditions'=>array('EmergencyexitReportAttachment.id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['EmergencyexitReportAttachment']['servicecall_id'])));	
		$action='deleted an attachement of emergency exit report';
		# function saveLog to Maintain inspector's log	
		
		if(file_exists(WWW_ROOT."img/uploaded_attachments/".$data['EmergencyexitReportAttachment']['attach_file'])){
		  unlink(WWW_ROOT."img/uploaded_attachments/".$data['EmergencyexitReportAttachment']['attach_file']); 
		}		
		if($this->EmergencyexitReportAttachment->delete($id)){
			$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['EmergencyexitReportAttachment']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['EmergencyexitReportAttachment']['servicecall_id'],$action);
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	} 
/***************************************************************************************
	#THIS FUNCTION IS USED In EMERGENCYEXIT REPORT FOR NOTIFICATION DELETION 
	Function: delemergencynotifier() 
****************************************************************************************/
	function delemergencynotifier($id = null){
		$this->layout = null;
		$this->loadModel('EmergencyexitReportNotification');
		$this->loadModel('ServiceCall');
		$data=$this->EmergencyexitReportNotification->find('first',array('conditions'=>array('id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['EmergencyexitReportNotification']['servicecall_id'])));	
		$action='deleted a notification of emergency exit report';
		if($this->EmergencyexitReportNotification->delete($id)){
			$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['EmergencyexitReportNotification']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['EmergencyexitReportNotification']['servicecall_id'],$action);
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	}
/***************************************************************************************
	#THIS FUNCTION IS USED In SPECIAL HAZARD REPORT FOR NOTIFICATION DELETION 
	Function: delemergencynotifier() 
****************************************************************************************/
	function delspecialhzardnotifier($id = null){
		$this->layout = null;
		$this->loadModel('SpecialhazardReportNotification');
		$this->loadModel('ServiceCall');
		$data=$this->SpecialhazardReportNotification->find('first',array('conditions'=>array('SpecialhazardReportNotification.id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['SpecialhazardReportNotification']['servicecall_id'])));	
		$action='deleted a notification of special hazard report';
		
		if($this->SpecialhazardReportNotification->delete($id)){
			# function saveLog to Maintain inspector's log
			$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['SpecialhazardReportNotification']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['SpecialhazardReportNotification']['servicecall_id'],$action);
			echo "1";
		}
		else{
			echo "0";
		}
		exit;
	}
/***************************************************************************************
	#THIS FUNCTION IS USED FOR delete delspecialhzardattachment Record REPORTS
	Reports controller emergencyexit reportsfunction 
****************************************************************************************/
	  function delspecialhzardattachment($id = null){
		$this->layout = null;
		$this->loadModel('SpecialhazardReportAttachment');
		$this->loadModel('ServiceCall');
		$data=$this->SpecialhazardReportAttachment->find('first',array('conditions'=>array('SpecialhazardReportAttachment.id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['SpecialhazardReportAttachment']['servicecall_id'])));	
		$action='deleted an attachment of special hazard report';		
		if(file_exists(WWW_ROOT."img/uploaded_attachments/".$data['SpecialhazardReportAttachment']['attach_file'])){
		  unlink(WWW_ROOT."img/uploaded_attachments/".$data['SpecialhazardReportAttachment']['attach_file']); 
		}		
		if($this->SpecialhazardReportAttachment->delete($id)){
			# function saveLog to Maintain inspector's log
			$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['SpecialhazardReportAttachment']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['SpecialhazardReportAttachment']['servicecall_id'],$action);
			echo "1";
		}
		else{
			echo "0";
		}
		exit;
	  }
/***************************************************************************************
	#THIS FUNCTION IS USED In Sprinkler Report FOR ADDDING NEW ATTACHMENT 
	Function: inspector_emergency_attachment() 
****************************************************************************************/ 
	  function inspector_sprinkler_attachment()
	  {
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('SprinklerReportAttachment');				
		 if(isset($this->data) && !empty($this->data))
		 { 
		   $this->data['SprinklerReportAttachment']['user_id'] = $inspector_id;
		   $this->data['SprinklerReportAttachment']['added_by'] = 'Inspector';
		   if($this->SprinklerReportAttachment->save($this->data['SprinklerReportAttachment']))
		   {
			$action='uploaded an attachment for sprinkler report';
			$this->saveLog($this->data['SprinklerReportAttachment']['sp_id'],$this->data['SprinklerReportAttachment']['report_id'],$this->data['SprinklerReportAttachment']['client_id'],$this->data['SprinklerReportAttachment']['servicecall_id'],$action);
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/inspector/reports/sprinklerinspection?reportID='.$this->data['SprinklerReportAttachment']['report_id'].'&clientID='.$this->data['SprinklerReportAttachment']['client_id'].'&spID='.$this->data['SprinklerReportAttachment']['sp_id'].'&serviceCallID='.$this->data['SprinklerReportAttachment']['servicecall_id']);
		   }
		 }
	  }
/***************************************************************************************
	#THIS FUNCTION IS USED In specialhazard Report FOR ADDDING NEW ATTACHMENT 
	Function: inspector_specialhazard_attachment() 
****************************************************************************************/ 	  
	  function inspector_specialhazard_attachment(){
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('SpecialhazardReportAttachment');				
		 if(isset($this->data) && !empty($this->data)){ 
		   $this->data['SpecialhazardReportAttachment']['user_id'] = $inspector_id;
		   $this->data['SpecialhazardReportAttachment']['added_by'] = 'Inspector';
		   if($this->SpecialhazardReportAttachment->save($this->data['SpecialhazardReportAttachment']))
		   {
				$action='uploaded an attachement for special hazard report';
				$this->saveLog($this->data['SpecialhazardReportAttachment']['sp_id'],$this->data['SpecialhazardReportAttachment']['report_id'],$this->data['SpecialhazardReportAttachment']['client_id'],$this->data['SpecialhazardReportAttachment']['servicecall_id'],$action);
				$this->Session->setFlash('attachment has been uploaded successfully.');
				$this->redirect('/inspector/reports/specialhazard?reportID='.$this->data['SpecialhazardReportAttachment']['report_id'].'&clientID='.$this->data['SpecialhazardReportAttachment']['client_id'].'&spID='.$this->data['SpecialhazardReportAttachment']['sp_id'].'&serviceCallID='.$this->data['SpecialhazardReportAttachment']['servicecall_id']);
		   }
		 }
	  }
/***************************************************************************************
	#THIS FUNCTION IS USED In Sprinkler Report FOR ADDDING NEW NOTIFICATION 
	Function: inspector_sprinkler_notification() 
****************************************************************************************/ 	  
 function inspector_sprinkler_notification()
	  {
		$this->loadModel('SprinklerReportNotification');
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
	
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['SprinklerReportNotification']['user_id'] = $inspector_id;
		   if($this->SprinklerReportNotification->save($this->data['SprinklerReportNotification']))
		   {
			$action='added a notification for sprinkler report';
			$this->saveLog($this->data['SprinklerReportNotification']['sp_id'],$this->data['SprinklerReportNotification']['report_id'],$this->data['SprinklerReportNotification']['client_id'],$this->data['SprinklerReportNotification']['servicecall_id'],$action);
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/inspector/reports/sprinklerinspection?reportID='.$this->data['SprinklerReportNotification']['report_id'].'&clientID='.$this->data['SprinklerReportNotification']['client_id'].'&spID='.$this->data['SprinklerReportNotification']['sp_id'].'&serviceCallID='.$this->data['SprinklerReportNotification']['servicecall_id']);
		   }
		 }
	  }	 
     
/***************************************************************************************
	#THIS FUNCTION IS USED In Sprinkler Report FOR ATTACHMENT DELETION 
	Function: delsprinklerattachment() 
****************************************************************************************/ 	  
	function delsprinklerattachment($id = null){
		$this->layout = null;
		$this->loadModel('SprinklerReportAttachment');
		$this->loadModel('ServiceCall');
		$data=$this->SprinklerReportAttachment->find('first',array('conditions'=>array('SprinklerReportAttachment.id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['SprinklerReportAttachment']['servicecall_id'])));	
		$action='deleted an attachment of sprinkler report';		
		
		if(file_exists(WWW_ROOT."img/uploaded_attachments/".$data['SprinklerReportAttachment']['attach_file'])){
		  unlink(WWW_ROOT."img/uploaded_attachments/".$data['SprinklerReportAttachment']['attach_file']); 
		}		
		if($this->SprinklerReportAttachment->delete($id)){
		# function saveLog to Maintain inspector's log
		$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['SprinklerReportAttachment']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['SprinklerReportAttachment']['servicecall_id'],$action);
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	}

/***************************************************************************************
	#THIS FUNCTION IS USED In EMERGENCYEXIT REPORT FOR NOTIFICATION DELETION 
	Function: delemergencynotifier() 
****************************************************************************************/
	function delsprinklernotifier($id = null){
		$this->layout = null;
		$this->loadModel('SprinklerReportNotification');
		$this->loadModel('ServiceCall');
		$data=$this->SprinklerReportNotification->find('first',array('conditions'=>array('SprinklerReportNotification.id'=>$id)));
		$dataForLog=$this->ServiceCall->find('first',array('conditions'=>array('id'=>$data['SprinklerReportNotification']['servicecall_id'])));	
		$action='deleted a notification of sprinkler report';		
		if($this->SprinklerReportNotification->delete($id)){
			# function saveLog to Maintain inspector's log
		$this->saveLog($dataForLog['ServiceCall']['sp_id'],$data['SprinklerReportNotification']['report_id'],$dataForLog['ServiceCall']['client_id'],$data['SprinklerReportNotification']['servicecall_id'],$action);
			echo "1";
		}
		else {
			echo "0";
		}
		exit;
	}
/***************************************************************************************
	#THIS FUNCTION IS USED TO DOWNLOAD A ATTACHMENT 
****************************************************************************************/ 	  
	function inspector_download ($fileName = null){
		$mimeType = array('bmp'=>'image/bmp','docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','gif'=>'image/gif','jpg'=>'image/jpeg','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT."img/uploaded_attachments/"
		);
		$this->set($params);
	}
/***************************************************************************************
	#THIS FUNCTION IS USED TO DOWNLOAD A ATTACHMENT 
****************************************************************************************/ 	  
	function sp_download ($fileName = null){
		$mimeType = array('bmp'=>'image/bmp','docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','gif'=>'image/gif','jpg'=>'image/jpeg','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT."img/uploaded_attachments/"
		);
		$this->set($params);
	}
	
	 	
	
	 
	 

/***************************************************************************************
		 	 function to export EXTINGUISHER REPORT  as PDF
****************************************************************************************/ 	
    function pdfExtinguisherReport(){
      $inspectorses = $this->Session->read('Log');
    	$inspector_id = $inspectorses['User']['id'];
    	$this->loadModel('User');
    	$this->loadModel('Attachment');
    	$this->loadModel('Notification');
    	$this->loadModel('ExtinguisherReport');
    	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
    		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
    		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
    		$this->set('spResult',$spResult);
    		$this->set('spId',$_REQUEST['spID']);
    		$this->set('clientId',$_REQUEST['clientID']);
    		$this->set('clientResult',$clientResult);
    		$this->set('servicecallId',$_REQUEST['serviceCallID']);
    		$this->set('inspector_id',$inspector_id);
    		$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
    		$record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
    		$this->set('record',$record);
    		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
			#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
														'Code'=> array(
																	'className' => 'Code',
																	'foreignKey' => 'code_id',
																	'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																	)
														)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
    	}    
    }

/***************************************************************************************
	function to export Sprinkler REPORT  as PDF
****************************************************************************************/ 	   
     function pdfSprinklerReport(){
     	set_time_limit(0);
      	$this->loadModel('User');
      	$this->loadModel('Attachment');
      	$this->loadModel('Notification');
      	$this->loadModel('SprinklerReport');
      	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
      		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
      		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
      		$this->set('spResult',$spResult);
      		$this->set('spId',$_REQUEST['spID']);
      		$this->set('clientId',$_REQUEST['clientID']);
      		$this->set('clientResult',$clientResult);
      		$this->set('servicecallId',$_REQUEST['serviceCallID']);      		
      		$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
      		$record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
      		$this->set('record',$record);
		
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult);  
		
      		//pr($record); die;
      		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
			#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
														'Code'=> array(
																	'className' => 'Code',
																	'foreignKey' => 'code_id',
																	'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																	)
														)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
        }
     }    
    
     function inspector_sprinklerView(){
        $this->layout='';      	
      	$this->loadModel('User');
	$this->loadModel('Schedule');
      	$this->loadModel('Attachment');
      	$this->loadModel('Notification');
      	$this->loadModel('SprinklerReport');
      	if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
		#code on 4 oct,2012 for Site address display
		$this->loadModel('ServiceCall');
		$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		$this->loadModel('SpSiteAddress');
		$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		$this->set('siteaddrResult',$siteaddrResult); 
  		#end code
  		
      		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
      		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
      		$this->set('spResult',$spResult);
      		$this->set('spId',$_REQUEST['spID']);
      		$this->set('clientId',$_REQUEST['clientID']);
      		$this->set('clientResult',$clientResult);
      		$this->set('servicecallId',$_REQUEST['serviceCallID']);
		//$this->set('inspector_id',$inspector_id);
      		$this->set('reportId',$_REQUEST['reportID']);
		$this->set('reportType',Configure::read('reportType'));
      		$record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
      		$this->set('record',$record);
		
		$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array()));
		$reqScheduleId = $schedule[0]['Schedule']['id'];
		$this->set('schId',$reqScheduleId);
		$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
		
      		$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('attachmentdata',$attachmentdata);
		$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		$this->set('notifierdata',$notifierdata);
		#CODE for deficiency page in view
		$this->loadModel('Deficiency');
		$this->Deficiency->bindModel(array('belongsTo' => array(
													'Code'=> array(
																'className' => 'Code',
																'foreignKey' => 'code_id',
																'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																)
													)
								 )
						   );
		$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		$this->set('Def_record',$Def_record);
		$this->set('Def_record_recomm',$Def_record);
			
        }
     }

/***************************************************************************************
	#THIS FUNCTION IS USED FOR extinguishers attachment Record REPORTS IN SP PANEL
 # To add attachment in sp panel
****************************************************************************************/
	  function sp_extinguishers_attachment(){
		$spData = $this->Session->read('Log');
		$sp_id = $spData['User']['id'];
		$this->loadModel('ExtinguisherReportAttachment');				
		 if(isset($this->data) && !empty($this->data)){ 
		   $this->data['ExtinguisherReportAttachment']['user_id'] = $sp_id;
		   $this->data['ExtinguisherReportAttachment']['added_by'] = 'SP';
		   if($this->ExtinguisherReportAttachment->save($this->data['ExtinguisherReportAttachment'])){
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/sp/reports/extinguishers?reportID='.$this->data['ExtinguisherReportAttachment']['report_id'].'&clientID='.$this->data['ExtinguisherReportAttachment']['client_id'].'&spID='.$this->data['ExtinguisherReportAttachment']['sp_id'].'&serviceCallID='.$this->data['ExtinguisherReportAttachment']['servicecall_id']);
		   }
		 }
	  }
	  
	  function sp_extinguishers_notification(){
		$spData = $this->Session->read('Log');
		$sp_id = $spData['User']['id'];
		$this->loadModel('ExtinguisherReportNotification');
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['ExtinguisherReportNotification']['user_id'] = $sp_id;
		   $this->data['ExtinguisherReportNotification']['added_by'] = 'SP';
		   if($this->ExtinguisherReportNotification->save($this->data['ExtinguisherReportNotification']))
		   {			
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/sp/reports/extinguishers?reportID='.$this->data['ExtinguisherReportNotification']['report_id'].'&clientID='.$this->data['ExtinguisherReportNotification']['client_id'].'&spID='.$this->data['ExtinguisherReportNotification']['sp_id'].'&serviceCallID='.$this->data['ExtinguisherReportNotification']['servicecall_id']);
		   }
		 }
	  }
/***************************************************************************************
	#THIS FUNCTION IS USED In EMERGENCYEXIT REPORT FOR ADDDING NEW ATTACHMENT IN SP PANEL
	Function: sp_emergency_attachment() 
****************************************************************************************/ 
	  function sp_emergency_attachment()
	  {
		$spData = $this->Session->read('Log');
		$sp_id = $spData['User']['id'];
		$this->loadModel('EmergencyexitReportAttachment');				
		 if(isset($this->data) && !empty($this->data))
		 { 
		   $this->data['EmergencyexitReportAttachment']['user_id'] = $sp_id;
		   $this->data['EmergencyexitReportAttachment']['added_by'] = 'SP';
		   if($this->EmergencyexitReportAttachment->save($this->data['EmergencyexitReportAttachment']))
		   {
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/sp/reports/emergencyexit?reportID='.$this->data['EmergencyexitReportAttachment']['report_id'].'&clientID='.$this->data['EmergencyexitReportAttachment']['client_id'].'&spID='.$this->data['EmergencyexitReportAttachment']['sp_id'].'&serviceCallID='.$this->data['EmergencyexitReportAttachment']['servicecall_id']);
		   }
		 }
	  }	  
/***************************************************************************************
	#THIS FUNCTION IS USED In EMERGENCYEXIT REPORT FOR ADDDING NEW NOTIFICATION 
	Function: sp_emergency_notification() 
****************************************************************************************/ 	  
	  function sp_emergency_notification()
	  {
		$this->loadModel('EmergencyexitReportNotification');
		$spData = $this->Session->read('Log');
		$sp_id = $spData['User']['id'];
	
		 if(isset($this->data) && !empty($this->data))
		 {
		  $this->data['EmergencyexitReportNotification']['added_by'] = 'SP';
		   $this->data['EmergencyexitReportNotification']['user_id'] = $sp_id;
		   if($this->EmergencyexitReportNotification->save($this->data['EmergencyexitReportNotification']))
		   {
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/sp/reports/emergencyexit?reportID='.$this->data['EmergencyexitReportNotification']['report_id'].'&clientID='.$this->data['EmergencyexitReportNotification']['client_id'].'&spID='.$this->data['EmergencyexitReportNotification']['sp_id'].'&serviceCallID='.$this->data['EmergencyexitReportNotification']['servicecall_id']);
		   }
		 }
	  }
/***************************************************************************************
	#THIS FUNCTION IS USED In KITCHENHOOD REPORT FOR ADDDING NEW ATTACHMENT IN SP PANEL 
	Function: sp_kitchenhood_attachment() 
****************************************************************************************/ 	  
	function sp_kitchenhood_attachment()
	{
		$spData = $this->Session->read('Log');
		$sp_id = $spData['User']['id'];
		$this->loadModel('KitchenhoodReportAttachment');				
		 if(isset($this->data) && !empty($this->data))
		 { 
		   $this->data['KitchenhoodReportAttachment']['user_id'] = $sp_id;
		   $this->data['KitchenhoodReportAttachment']['added_by'] = 'SP';
		   if($this->KitchenhoodReportAttachment->save($this->data['KitchenhoodReportAttachment']))
		   {
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/sp/reports/kitchenhood?reportID='.$this->data['KitchenhoodReportAttachment']['report_id'].'&clientID='.$this->data['KitchenhoodReportAttachment']['client_id'].'&spID='.$this->data['KitchenhoodReportAttachment']['sp_id'].'&serviceCallID='.$this->data['KitchenhoodReportAttachment']['servicecall_id']);
		   }
		 }
	}
/***************************************************************************************
	#THIS FUNCTION IS USED In KITCHENHOOD FOR ADDDING NEW NOTIFICATION IN SP PANEL
	Function: sp_kitchenhood_notification() 
****************************************************************************************/ 
	function sp_kitchenhood_notification()
	{
		$this->loadModel('KitchenhoodReportNotification');
		$spData = $this->Session->read('Log');
		$sp_id = $spData['User']['id'];
		$this->loadModel('KitchenhoodReportNotification');
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['KitchenhoodReportNotification']['user_id'] = $sp_id;
		   $this->data['KitchenhoodReportNotification']['added_by'] = 'SP';
		   if($this->KitchenhoodReportNotification->save($this->data['KitchenhoodReportNotification']))
		   {
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/sp/reports/kitchenhood?reportID='.$this->data['KitchenhoodReportNotification']['report_id'].'&clientID='.$this->data['KitchenhoodReportNotification']['client_id'].'&spID='.$this->data['KitchenhoodReportNotification']['sp_id'].'&serviceCallID='.$this->data['KitchenhoodReportNotification']['servicecall_id']);
		   }
		 }
	}	
  /***************************************************************************************
	#THIS FUNCTION IS USED In SPECIALHAZARD REPORT FOR ADDDING NEW NOTIFICATION IN SP PANEL
	Function: sp_specialhazard_notification() 
****************************************************************************************/
	  function sp_specialhazard_notification()
	  {
  		$this->loadModel('SpecialhazardReportNotification');
  		$spData = $this->Session->read('Log');
  		$sp_id = $spData['User']['id'];
	
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['SpecialhazardReportNotification']['user_id'] = $sp_id;
		   $this->data['SpecialhazardReportNotification']['added_by'] = 'SP';
		   if($this->SpecialhazardReportNotification->save($this->data['SpecialhazardReportNotification']))
		   {
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/sp/reports/specialhazard?reportID='.$this->data['SpecialhazardReportNotification']['report_id'].'&clientID='.$this->data['SpecialhazardReportNotification']['client_id'].'&spID='.$this->data['SpecialhazardReportNotification']['sp_id'].'&serviceCallID='.$this->data['SpecialhazardReportNotification']['servicecall_id']);
		   }
		 }
	  } 
/***************************************************************************************
	#THIS FUNCTION IS USED In specialhazard Report FOR ADDDING NEW NOTIFICATION 
	Function: sp_specialhazard_attachment() 
****************************************************************************************/ 	  
	  function sp_specialhazard_attachment(){
		$spData = $this->Session->read('Log');
  		$sp_id = $spData['User']['id'];
		$this->loadModel('SpecialhazardReportAttachment');				
		 if(isset($this->data) && !empty($this->data)){ 
		   $this->data['SpecialhazardReportAttachment']['user_id'] = $sp_id;
		   $this->data['SpecialhazardReportAttachment']['added_by'] = 'SP';
		   if($this->SpecialhazardReportAttachment->save($this->data['SpecialhazardReportAttachment']))
		   {
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/sp/reports/specialhazard?reportID='.$this->data['SpecialhazardReportAttachment']['report_id'].'&clientID='.$this->data['SpecialhazardReportAttachment']['client_id'].'&spID='.$this->data['SpecialhazardReportAttachment']['sp_id'].'&serviceCallID='.$this->data['SpecialhazardReportAttachment']['servicecall_id']);
		   }
		 }
	  }
  /***************************************************************************************
	#THIS FUNCTION IS USED In SPRINKLER REPORT FOR ADDDING NEW NOTIFICATION IN SP PANEL
	Function: sp_sprinkler_notification() 
****************************************************************************************/
	  function sp_sprinkler_notification()
	  {
  		$this->loadModel('SprinklerReportNotification');
  		$spData = $this->Session->read('Log');
  		$sp_id = $spData['User']['id'];
	
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['SprinklerReportNotification']['user_id'] = $sp_id;
		   $this->data['SprinklerReportNotification']['added_by'] = 'SP';
		   if($this->SprinklerReportNotification->save($this->data['SprinklerReportNotification']))
		   {
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/sp/reports/sprinklerinspection?reportID='.$this->data['SprinklerReportNotification']['report_id'].'&clientID='.$this->data['SprinklerReportNotification']['client_id'].'&spID='.$this->data['SprinklerReportNotification']['sp_id'].'&serviceCallID='.$this->data['SprinklerReportNotification']['servicecall_id']);
		   }
		 }
	  } 
/***************************************************************************************
	#THIS FUNCTION IS USED In SPRINKLER Report FOR ADDDING NEW NOTIFICATION 
	Function: sp_sprinkler_attachment() 
****************************************************************************************/ 	  
	  function sp_sprinkler_attachment(){
		$spData = $this->Session->read('Log');
  		$sp_id = $spData['User']['id'];
		$this->loadModel('SprinklerReportAttachment');				
		 if(isset($this->data) && !empty($this->data)){ 
		   $this->data['SprinklerReportAttachment']['user_id'] = $sp_id;
		   $this->data['SprinklerReportAttachment']['added_by'] = 'SP';
		   if($this->SprinklerReportAttachment->save($this->data['SprinklerReportAttachment']))
		   {
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/sp/reports/sprinklerinspection?reportID='.$this->data['SprinklerReportAttachment']['report_id'].'&clientID='.$this->data['SprinklerReportAttachment']['client_id'].'&spID='.$this->data['SprinklerReportAttachment']['sp_id'].'&serviceCallID='.$this->data['SprinklerReportAttachment']['servicecall_id']);
		   }
		 }
	  }
	  
/***************************************************************************************
	#THIS FUNCTION IS USED FOR extinguishers attachment Record REPORTS
	Reports controller extinguishers reportsfunction 
****************************************************************************************/
	  function inspector_extinguishers_attachment(){
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('ExtinguisherReportAttachment');				
		 if(isset($this->data) && !empty($this->data)){			
		   $this->data['ExtinguisherReportAttachment']['user_id'] = $inspector_id;
		   $this->data['ExtinguisherReportAttachment']['added_by'] = 'Inspector';
		   if($this->ExtinguisherReportAttachment->save($this->data['ExtinguisherReportAttachment'])){
			$action='uploaded an attachment for extinguisher report';
			$this->saveLog($this->data['ExtinguisherReportAttachment']['sp_id'],$this->data['ExtinguisherReportAttachment']['report_id'],$this->data['ExtinguisherReportAttachment']['client_id'],$this->data['ExtinguisherReportAttachment']['servicecall_id'],$action);
			$this->Session->setFlash('attachment has been uploaded successfully.');
			$this->redirect('/inspector/reports/extinguishers?reportID='.$this->data['ExtinguisherReportAttachment']['report_id'].'&clientID='.$this->data['ExtinguisherReportAttachment']['client_id'].'&spID='.$this->data['ExtinguisherReportAttachment']['sp_id'].'&serviceCallID='.$this->data['ExtinguisherReportAttachment']['servicecall_id']);
		   }
		 }
	  }
	  
	  function inspector_extinguishers_notification(){
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->loadModel('ExtinguisherReportNotification');
		 if(isset($this->data) && !empty($this->data))
		 {
		   $this->data['ExtinguisherReportNotification']['user_id'] = $inspector_id;
		   if($this->ExtinguisherReportNotification->save($this->data['ExtinguisherReportNotification']))
		   {
			$action='added a notification for extinguisher report';
			$this->saveLog($this->data['ExtinguisherReportNotification']['sp_id'],$this->data['ExtinguisherReportNotification']['report_id'],$this->data['ExtinguisherReportNotification']['client_id'],$this->data['ExtinguisherReportNotification']['servicecall_id'],$action);
			$this->Session->setFlash('Notification has been saved successfully.');
			$this->redirect('/inspector/reports/extinguishers?reportID='.$this->data['ExtinguisherReportNotification']['report_id'].'&clientID='.$this->data['ExtinguisherReportNotification']['client_id'].'&spID='.$this->data['ExtinguisherReportNotification']['sp_id'].'&serviceCallID='.$this->data['ExtinguisherReportNotification']['servicecall_id']);
		   }
		 }
	  }	  
/***************************************************************************************
	#THIS FUNCTION IS USED In Special Hazard Report FOR ADDDING DDEFICIENCIES/RECOMMENDATIONS 
	Function: sp_sprinkler_attachment() 
****************************************************************************************/ 
		function inspector_defAndRec(){
			$this->layout='';
			$inspectorses = $this->Session->read('Log');			
			$inspector_id = $inspectorses['User']['id'];
			$this->loadModel('Deficiency');
			if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
				
				$this->loadModel('Code');
				
				$this->loadModel('User');
				$userCodeType=$this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID']),'fields'=>'User.code_type_id'));
			
			  $this->Code->bindModel(array('hasMany' => array(
																'Deficiency'=> array(
																					 'className' => 'Deficiency',
																					 'foreignKey' => 'code_id',
																					 'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																					 )
																)
											 )
									   );
				
				//$defData=$this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
				//$this->set('defData',$defData);
				
				$codeData=$this->Code->find('all',array('conditions'=>array('Code.status'=>1,'Code.code_type_id'=>$userCodeType['User']['code_type_id'],'Code.report_id'=>$_REQUEST['reportID'])));
				
				$this->set('codeData',$codeData);
				
				
				
				
				$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
				$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
				$this->set('spResult',$spResult);				
				$this->set('clientResult',$clientResult);
				$this->set('spId',$_REQUEST['spID']);
				$this->set('clientId',$_REQUEST['clientID']);
				$this->set('clientResult',$clientResult);
				$this->set('servicecallId',$_REQUEST['serviceCallID']);
				$this->set('inspector_id',$inspector_id);
				$this->set('reportId',$_REQUEST['reportID']);
				
			}
			if(!empty($this->data) && isset($this->data)){
				$arr=array();
				foreach($this->data['Deficiency']['recommendation'] as $key=>$recomm):
				 $arr[$key]=$recomm;				
				endforeach;
				
				#DELETE the PREVIOUS DATA BEFORE SAVE IN Deficiency table
				$this->Deficiency->deleteAll(array('Deficiency.servicecall_id'=>$this->data['Deficiency']['servicecall_id'],'Deficiency.report_id'=>$this->data['Deficiency']['report_id'],'Deficiency.client_id'=>$this->data['Deficiency']['client_id'],'Deficiency.sp_id'=>$this->data['Deficiency']['sp_id']));
				
				foreach($this->data['Deficiency']['check'] as $key=>$check):
				//foreach($this->data['Deficiency']['recommendation'] as $recommkey=>$recommendation):
					if($check != 0){
						
						$this->data['Deficiency']['code_id']=$key;
						if(array_key_exists($key,$arr))
						{
							$this->data['Deficiency']['recommendation']=$arr[$key];
						}						
						$this->Deficiency->save($this->data['Deficiency'],array('validate'=>false));
						$this->Deficiency->id=NULL;
					}					
				//endforeach;
				endforeach;
			switch($this->data['Deficiency']['report_id'])
			{
				case "1":
				$filename = 'firealarm';
			   break;
			   case "2":
				$filename = 'sprinklerinspection';
			   break;            
			   case "3":
				$filename = 'kitchenhood';
			   break;
			   case "4":
				$filename = 'emergencyexit';
			   break;
			   case "5":
				$filename = 'extinguishers';
			   break;
			   case "6":
				$filename = 'specialhazard';
			   break;
			}
			$this->Session->setFlash('Deficiencies and recommendations has been saved successfully.');
			$this->redirect('/inspector/reports/'.$filename.'?reportID='.$this->data['Deficiency']['report_id'].'&clientID='.$this->data['Deficiency']['client_id'].'&spID='.$this->data['Deficiency']['sp_id'].'&serviceCallID='.$this->data['Deficiency']['servicecall_id']);
			
			}
			
			/*else{
				//read the data from deficiency table
				$defData=$this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
				$this->set('defData',$defData);
			}*/
			
			//$this->autoRender = false;
		}
/***************************************************************************************
	#THIS FUNCTION IS USED IN FIRE ALARM TO ADD NEW PLACE(LOCATION) 
	Function: sp_sprinkler_attachment() 
****************************************************************************************/ 	  
		function inspector_addFireAlarmLoc(){
			$this->loadModel('FaAlarmDevicePlace');
			if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
				$inspectorses = $this->Session->read('Log');
				$session_inspector_id = $inspectorses['User']['id'];
				$this->set('session_inspector_id',$session_inspector_id);				
				$this->set('spId',$_REQUEST['spID']);
				$this->set('clientId',$_REQUEST['clientID']);				
				$this->set('servicecallId',$_REQUEST['serviceCallID']);		
				$this->set('reportId',$_REQUEST['reportID']);	
        $locData=$this->FaAlarmDevicePlace->find('all',array('conditions'=>array('servicecall_id'=>$_REQUEST['serviceCallID'],'sp_id'=>$_REQUEST['spID'])));
        $this->set('locData',$locData);			
			}
			if(!empty($this->data) && isset($this->data)){
				# to check whether name already exists
				$checkname=$this->FaAlarmDevicePlace->find('count',array('conditions'=>array('FaAlarmDevicePlace.name'=>$this->data['FaAlarmDevicePlace']['name'])));
				
				if($checkname > 0){					
					$this->Session->setFlash('Given location already exists.');
					$this->redirect('/inspector/reports/firealarm/?reportID='.$this->data['FaAlarmDevicePlace']['report_id'].'&clientID='.$this->data['FaAlarmDevicePlace']['client_id'].'&spID='.$this->data['FaAlarmDevicePlace']['sp_id'].'&serviceCallID='.$this->data['FaAlarmDevicePlace']['servicecall_id']);	
				}else{
				#save functionality	
					if($this->FaAlarmDevicePlace->save($this->data['FaAlarmDevicePlace'],false)){
						$this->Session->setFlash('New location has been saved successfully.');
						$this->redirect('/inspector/reports/firealarm?reportID='.$this->data['FaAlarmDevicePlace']['report_id'].'&clientID='.$this->data['FaAlarmDevicePlace']['client_id'].'&spID='.$this->data['FaAlarmDevicePlace']['sp_id'].'&serviceCallID='.$this->data['FaAlarmDevicePlace']['servicecall_id']);	
					}
				}				
			}
			
		}
/***************************************************************************************
	#THIS FUNCTION IS USED IN FIRE ALARM TO DELETE A PLACE(LOCATION) 
****************************************************************************************/ 	  		
		function delFaLoc($id = null){		
      $this->layout = 'ajax';     
    	$this->loadModel('FaLocationDescription');
    	$this->loadModel('FaAlarmDevicePlace');
    	$this->autoRender = false;
    	$data=$this->FaAlarmDevicePlace->find('first',array('conditions'=>array('FaAlarmDevicePlace.id'=>$id)));
  		
      $this->loadModel('FaAlarmDevicePlace');  		
    	if($this->FaAlarmDevicePlace->delete($id)){
    		    $this->FaLocationDescription->deleteAll(array('FaLocationDescription.fa_alarm_device_place_id' => $id));  			  
            $this->Session->setFlash('Location has been deleted successfully.');				
  					echo "1"; exit;
    	}
  	}
/***************************************************************************************
	#THIS FUNCTION IS USED IN FIRE ALARM TO Edit A PLACE(LOCATION) 
****************************************************************************************/ 	    
    function inspector_editFireAlarmLoc(){
      if(isset($this->data['FaAlarmDevicePlace']['id']) && !empty($this->data['FaAlarmDevicePlace']['id'])){
        	$this->loadModel('FaAlarmDevicePlace');
        	$this->data['FaAlarmDevicePlace']['id']=$this->data['FaAlarmDevicePlace']['id'];
        	if($this->FaAlarmDevicePlace->save($this->data['FaAlarmDevicePlace'],false)){        	
						$this->Session->setFlash('Location has been edited successfully.');
						$this->redirect('/inspector/reports/firealarm?reportID='.$this->data['FaAlarmDevicePlace']['report_id'].'&clientID='.$this->data['FaAlarmDevicePlace']['client_id'].'&spID='.$this->data['FaAlarmDevicePlace']['sp_id'].'&serviceCallID='.$this->data['FaAlarmDevicePlace']['servicecall_id']);	
					}
      }
    }
	
	function inspector_print_preview(){
		
	}
	
	function reportTest(){
		$this->layout = '';
		set_time_limit(0);
		$this->loadModel('User');
		$this->loadModel('Attachment');
		$this->loadModel('Notification');
		$this->loadModel('SprinklerReport');
		$this->loadModel('Schedule');
		
		/*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/		
		
		$daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
		$daysRemaining = $daysData[0]['days'];
		if($daysRemaining!=null || $daysRemaining!=""){
		}else{
		    $daysRemaining="";
		}
		$this->set(compact('daysRemaining'));
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			
			$this->loadModel('Signature');
			$chkSign = $this->Signature->find('first',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
			$this->set('chkSign',$chkSign);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);
			$this->set('spId',$_REQUEST['spID']);
			$this->set('clientId',$_REQUEST['clientID']);
			$this->set('clientResult',$clientResult);
			$this->set('servicecallId',$_REQUEST['serviceCallID']);      		
			$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
			$record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
			$this->set('record',$record);
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);  
			
			//pr($record); die;
			$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('attachmentdata',$attachmentdata);
			$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('notifierdata',$notifierdata);
				#CODE for deficiency page in view
				$this->loadModel('Deficiency');
				$this->Deficiency->bindModel(array('belongsTo' => array(
															'Code'=> array(
																		'className' => 'Code',
																		'foreignKey' => 'code_id',
																		'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																		)
															)
										 )
								   );
				$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
				$this->set('Def_record',$Def_record);
				$this->set('Def_record_recomm',$Def_record);
		}  
	}
	
	function inspector_reportPreview($reportName=null){
		$this->set('reportName',$reportName);
		if(isset($_REQUEST['spID']) && isset($_REQUEST['clientID']) && isset($_REQUEST['reportID']) && isset($_REQUEST['serviceCallID'])){
			set_time_limit(0);
			$this->layout="";
			$spId = $_REQUEST['spID'];
			$clientID = $_REQUEST['clientID'];
			$reportID = $_REQUEST['reportID'];
			$serviceCallID = $_REQUEST['serviceCallID'];
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID']),'fields'=>array('Company.company_logo')));			
			$this->set('spResult',$spResult);
			
			$this->set('spId',$spId);
			$this->set('clientID',$clientID);
			$this->set('reportId',$reportID);
			$this->set('serviceCallID',$serviceCallID);
		}else{
			$this->Session->setFlash('Some error is there. Please try again');
			$this->redirect($this->referer());
		}	
	}
//Function inspector_emergencyReportPreview()
//Description: used for the PDF preview of the emergeny exit report
//Created By: Manish Kumar
	
	function inspector_emergencyReportPreview(){
		if(isset($_REQUEST['spID']) && isset($_REQUEST['clientID']) && isset($_REQUEST['reportID']) && isset($_REQUEST['serviceCallID'])){
			set_time_limit(0);
			$this->layout="";
			$spId = $_REQUEST['spID'];
			$clientID = $_REQUEST['clientID'];
			$reportID = $_REQUEST['reportID'];
			$serviceCallID = $_REQUEST['serviceCallID'];
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID']),'fields'=>array('Company.company_logo')));			
			$this->set('spResult',$spResult);
			
			$this->set('spId',$spId);
			$this->set('clientID',$clientID);
			$this->set('reportId',$reportID);
			$this->set('serviceCallID',$serviceCallID);
		}else{
			$this->Session->setFlash('Some error is there. Please try again');
			$this->redirect($this->referer());
		}	
	}
//Function emergencyReportTest
//desc: for te pdf preview of emergencyReportTest
//Manish Kumar
	function emergencyReportTest(){
		$this->layout = '';
		set_time_limit(0);		
		$this->loadModel('User');
		$this->loadModel('EmergencyexitReport');
		$this->loadModel('EmergencyexitReportRecord');
		$this->loadModel('Attachment');
		$this->loadModel('Notification');
		$this->loadModel('ScheduleService');
		$this->loadModel('Schedule');
		$this->loadModel('Report');
		$this->loadModel('Service');
		
		/*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/
		
		$daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
		$daysRemaining = $daysData[0]['days'];
		if($daysRemaining!=null || $daysRemaining!=""){
		}else{
			$daysRemaining="";
		}
		$this->set(compact('daysRemaining'));	   
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		$this->set('session_inspector_id',$inspector_id);
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$record = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
			$this->set('record',$record);			
			
			$this->loadModel('Signature');
			$chkSign = $this->Signature->find('first',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
			$this->set('chkSign',$chkSign);
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);
			$this->set('spId',$_REQUEST['spID']);
			$this->set('clientId',$_REQUEST['clientID']);
			$this->set('clientResult',$clientResult);
			$this->set('servicecallId',$_REQUEST['serviceCallID']);
			$this->set('inspector_id',$inspector_id);
			$this->set('reportId',$_REQUEST['reportID']);	
			$attachmentdata= $this->Attachment->find('all',array('conditions'=>array('Attachment.user_id'=>$inspector_id,'Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('attachmentdata',$attachmentdata);
			$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.user_id'=>$inspector_id,'Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('notifierdata',$notifierdata);
			$Result = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$option1 = array();
			$option2 = array();
		 
			#EXIT-SIGN SERVICE ID=75
			#Emergency Light SERVICE ID=76
			#FIND COUNT FOR EXIT-SIGN and EMERGENCY LIGHT SERVICE TAKEN BY CLIENT
			foreach($Result as $Res):
				$exit_sign=0;
				$emergency=0;
				foreach($Res['ScheduleService'] as $sch_services):
					if($sch_services['service_id']==75){
						$optionName = $this->Service->find('first',array('conditions'=>array('Service.id'=>$sch_services['service_id']),'fields'=>'Service.name'));
						$option1[$sch_services['service_id']] = $optionName['Service']['name'];
						$this->set('exit_amount',$sch_services['amount']);			
					}else{
						$this->set('exit_amount',0);
					}
					if($sch_services['service_id']==76){
						$optionName = $this->Service->find('first',array('conditions'=>array('Service.id'=>$sch_services['service_id']),'fields'=>'Service.name'));
						$option2[$sch_services['service_id']] = $optionName['Service']['name'];
						$this->set('emergency_amount',$sch_services['amount']);
						$this->set('option2',$option2);
					}else{
						$this->set('emergency_amount',0);
					}
		
				endforeach;
			endforeach;
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);
			$this->set('spId',$_REQUEST['spID']);
			$this->set('clientId',$_REQUEST['clientID']);
			$this->set('clientResult',$clientResult);
			$this->set('servicecallId',$_REQUEST['serviceCallID']);
			$this->set('inspector_id',$inspector_id);
			$this->set('reportId',$_REQUEST['reportID']);
			$this->set('option1',$option1);
			$this->set('reportType',Configure::read('reportType'));
		}	    
		$result = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
		$this->set('result',$result);
		$this->set('data',$result['EmergencyexitReportRecord']);	    
	}
	
//Function inspector_emergencyReportPreview()
//Description: used for the PDF preview of the emergeny exit report
//Created By: Manish Kumar
	
	function inspector_extinguisherReportPreview(){
		if(isset($_REQUEST['spID']) && isset($_REQUEST['clientID']) && isset($_REQUEST['reportID']) && isset($_REQUEST['serviceCallID'])){
			set_time_limit(0);
			$this->layout="";
			$spId = $_REQUEST['spID'];
			$clientID = $_REQUEST['clientID'];
			$reportID = $_REQUEST['reportID'];
			$serviceCallID = $_REQUEST['serviceCallID'];
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID']),'fields'=>array('Company.company_logo')));			
			$this->set('spResult',$spResult);
			
			$this->loadModel('Schedule');
			$schData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id')));
			$this->set('scheduleId',$schData['Schedule']['id']);
			
			$this->set('spId',$spId);
			$this->set('clientID',$clientID);
			$this->set('reportId',$reportID);
			$this->set('serviceCallID',$serviceCallID);
		}else{
			$this->Session->setFlash('Some error is there. Please try again');
			$this->redirect($this->referer());
		}	
	}
//Function emergencyReportTest
//desc: for te pdf preview of emergencyReportTest
//Manish Kumar
	function extinguisherReportTest(){
		$this->layout = '';
		set_time_limit(0);		
		$this->loadModel('User');
		$this->loadModel('ExtinguisherReport');
		$this->loadModel('ScheduleService');
		$this->loadModel('ExtinguisherReportRecord');
		$this->loadModel('Schedule');
		$this->loadModel('Attachment');
		$this->loadModel('Notification');
		$this->loadModel('Service');
		
		$this->loadModel('Schedule');
		 
		 /*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/
		     
		 $daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
		 $daysRemaining = $daysData[0]['days'];
		 if($daysRemaining!=null || $daysRemaining!=""){
		 }else{
		     $daysRemaining="";
		 }
		 $this->set(compact('daysRemaining'));
		
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		 $this->set('session_inspector_id',$inspector_id);
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		     
		     #code on 4 oct,2012 for Site address display
		     $this->loadModel('ServiceCall');
		     $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		     $this->loadModel('SpSiteAddress');
		     $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		     $this->set('siteaddrResult',$siteaddrResult); 
		     #end code
		     
		     $record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
		     $this->set('record',$record);
		     
		     $this->loadModel('Signature');
		     $chkSign = $this->Signature->find('first',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
		     $this->set('chkSign',$chkSign);		     
		     $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		     $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		     $this->set('spResult',$spResult);
		     $this->set('spId',$_REQUEST['spID']);
		     $this->set('clientId',$_REQUEST['clientID']);
		     $this->set('clientResult',$clientResult);
		     $this->set('servicecallId',$_REQUEST['serviceCallID']);
		     $this->set('inspector_id',$inspector_id);
		     $this->set('reportId',$_REQUEST['reportID']);
		     
		     $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array()));
		     //$this->set('scheduleId',$schedule[0]['Schedule']['id']);
		     $options = array();
		      foreach($schedule[0]['ScheduleService'] as $schedule){
			     $ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			     $options[$ServiceOptions['Service']['id']] = $ServiceOptions['Service']['name'];
		     }
		     $this->set('options',$options);
		     
		     $attachmentdata = $this->Attachment->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		      
		     $this->set('attachmentdata',$attachmentdata);
		     $notifierdata= $this->Notification->find('all',array('conditions'=>array('report_id'=>$_REQUEST['reportID'],'servicecall_id'=>$_REQUEST['serviceCallID'])));
		     $this->set('notifierdata',$notifierdata);
		     $this->set('reportType',Configure::read('reportType'));
		}
		    
		$result = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
		$this->set('result',$result['ExtinguisherReport']);
		$data = $result['ExtinguisherReportRecord'];
		$this->set('data',$data);
		 	    
	}
	
//Function inspector_firealarmReportPreview()
//Description: used for the PDF preview of the firealarm report
//Created By: Manish Kumar
	
	function inspector_firealarmReportPreview(){
		if(isset($_REQUEST['spID']) && isset($_REQUEST['clientID']) && isset($_REQUEST['reportID']) && isset($_REQUEST['serviceCallID'])){
			set_time_limit(0);
			$this->layout="";
			$spId = $_REQUEST['spID'];
			$clientID = $_REQUEST['clientID'];
			$reportID = $_REQUEST['reportID'];
			$serviceCallID = $_REQUEST['serviceCallID'];
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID']),'fields'=>array('Company.company_logo')));			
			$this->set('spResult',$spResult);
			
			$this->set('spId',$spId);
			$this->set('clientID',$clientID);
			$this->set('reportId',$reportID);
			$this->set('serviceCallID',$serviceCallID);
		}else{
			$this->Session->setFlash('Some error is there. Please try again');
			$this->redirect($this->referer());
		}	
	}
//Function firealarmReportTest
//desc: for te pdf preview of firealarmReportTest
//Manish Kumar
	function firealarmReportTest(){
		$this->layout = '';
		set_time_limit(0);		
		
		$firealarmarr = Configure::read('firealarmarr');
		$this->set('firealarmarr',$firealarmarr);
		$frequency = Configure::read('frequency');
		$this->set('frequency',$frequency);		
		$this->loadModel('FaAlarmDevicePlace');
		$this->loadModel('FirealarmReport');
		$this->loadModel('Notification');
		$this->loadModel('Attachment');
		$this->loadModel('Schedule');
		
		/*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/
		     
		$daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
		$daysRemaining = $daysData[0]['days'];
		if($daysRemaining!=null || $daysRemaining!=""){
		}else{
		    $daysRemaining="";
		}
		$this->set(compact('daysRemaining'));
		$inspectorses = $this->Session->read('Log');
		$inspector_id = $inspectorses['User']['id'];
		
	       if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		     
		     #code on 4 oct,2012 for Site address display
		     $this->loadModel('ServiceCall');
		     $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		     $this->loadModel('SpSiteAddress');
		     $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		     $this->set('siteaddrResult',$siteaddrResult); 
		     #end code
		     
		     
		     $this->loadModel('Signature');
		     $chkSign = $this->Signature->find('first',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
		     $this->set('chkSign',$chkSign);		     
		     $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		     $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));	     
		     $this->set('spResult',$spResult);
		     $this->set('spId',$_REQUEST['spID']);
		     $this->set('clientId',$_REQUEST['clientID']);
		     $this->set('clientResult',$clientResult);
		     $this->set('servicecallId',$_REQUEST['serviceCallID']);		
		     $this->set('reportId',$_REQUEST['reportID']);
		     $attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		     $this->set('attachmentdata',$attachmentdata);
		     $notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		     $this->set('notifierdata',$notifierdata);
		     $device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'],'FaAlarmDevicePlace.report_id'=>$_REQUEST['reportID'],'FaAlarmDevicePlace.servicecall_id'=>$_REQUEST['serviceCallID'],'FaAlarmDevicePlace.client_id'=>$_REQUEST['clientID'],'FaAlarmDevicePlace.inspector_id'=>$inspector_id)));
		     $this->set('device_place',$device_place);	
		     // $device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
		
		     ###########################################
	             #Code to get devices added by a given sp
	             $this->loadModel('Schedule');
	             $this->loadModel('ScheduleService'); 
	             $scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
	             $this->ScheduleService->bindModel(
				 array('belongsTo' => array(
				   'Service' => array(
				     'className' => 'Service'
				   )
				 )
		         )
		      );
		      if(!empty($scheduledata)){
			$servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));
	     
			$this->set('scheduleId',$scheduledata['Schedule']['id']);
		      }
	   
			$this->set('servicedata',$servicedata);
			//pr($servicedata);
			$device_type_temp=array();
			foreach($servicedata as $value){
				$device_type_temp[$value['Service']['id']] = $value['Service']['name'];
			}
			$this->set('device_type',$device_type_temp);
			$this->set('reportType',Configure::read('reportType'));
		############################################
	       }
	       $fireData = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'],'FirealarmReport.report_id'=>$_REQUEST['reportID'])));
	       $this->set('fireData',$fireData);
	}	
	
//Function reportTestKitchenhood
//desc: for the pdf preview of inspector panel reports 
//Manish Kumar
	function reportTestKitchenhood(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('Report');
		$this->loadModel('KitchenhoodReport');
		$this->loadModel('KitchenhoodReportRecord');
		$this->loadModel('Attachment');
		$this->loadModel('Notification');
		$this->loadModel('ScheduleService');
		$this->loadModel('Schedule');
		$this->loadModel('Service');
		
		/*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/
		
		$daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
		$daysRemaining = $daysData[0]['days'];
		if($daysRemaining!=null || $daysRemaining!=""){
		}else{
		    $daysRemaining="";
		}
		$this->set(compact('daysRemaining'));			
		 if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		     
		        $this->loadModel('Signature');
			$chkSign = $this->Signature->find('first',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
			$this->set('chkSign',$chkSign);
		     
		     #code on 4 oct,2012 for Site address display
		     $this->loadModel('ServiceCall');
		     $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		     $this->loadModel('SpSiteAddress');
		     $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		     $this->set('siteaddrResult',$siteaddrResult); 
		     #end code
		     
		     $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		     $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID']))); 
		     $this->set('spRes',$spResult);
		     $this->set('spId',$_REQUEST['spID']);
		     $this->set('clientId',$_REQUEST['clientID']);
		     $this->set('clientRes',$clientResult);
		     $this->set('servicecallId',$_REQUEST['serviceCallID']);
		     //$this->set('inspector_id',$inspector_id);
		     $this->set('reportId',$_REQUEST['reportID']);
		     $this->set('reportType',Configure::read('reportType'));
		     
		     $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		     $reqScheduleId = $schedule[0]['Schedule']['id'];
		     $this->set('scheduleId',$reqScheduleId);
		     $options = array();
		     foreach($schedule[0]['ScheduleService'] as $schedule){
			     $ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			     $options[$ServiceOptions['Service']['id']] = $ServiceOptions['Service']['name'];
		     }
		     $this->set('options',$options);
		     
		     $attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		     $this->set('attachmentdata',$attachmentdata);
		     $notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		     $this->set('notifierdata',$notifierdata);;
		}
		$this->data = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'],'KitchenhoodReport.report_id'=>$_REQUEST['reportID'])));
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['KitchenhoodReport']['sp_id'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$this->data['KitchenhoodReport']['client_id'])));
		$this->set('clientResult',$clientResult);
		$this->set('spResult',$spResult);
		
		$result=$this->data['KitchenhoodReport'];
		$this->set('result',$result);
		
		$data = $this->data['KitchenhoodReportRecord'];
		$this->set('data',$data);
	}
	
//Function reportTestKitchenhood
//desc: for the pdf preview of inspector panel reports 
//Manish Kumar
	function reportTestSpecialhazard(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('Report');
		$this->loadModel('SpecialhazardReport');
		$this->loadModel('SpecialhazardReportRecord');
		/*$this->loadModel('Attachment');
		$this->loadModel('Notification');*/
		$this->loadModel('ScheduleService');
		$this->loadModel('Schedule');
		$this->loadModel('Service');
		
		/*To Fetch and set Report Text*/
		$this->loadModel('SpReportText');
		$texts = $this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$_REQUEST['spID'])));
		$this->set('texts',nl2br($texts['SpReportText']['report_text']));
		/*To Fetch and set Report Text*/
		
		$daysData = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('DATEDIFF(Schedule.submission_date,now()) as days')));
		$daysRemaining = $daysData[0]['days'];
		if($daysRemaining!=null || $daysRemaining!=""){
		}else{
		    $daysRemaining="";
		}
		$this->set(compact('daysRemaining'));			
		 if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		     
		     #code on 4 oct,2012 for Site address display
		     $this->loadModel('ServiceCall');
		     $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		     $this->loadModel('SpSiteAddress');
		     $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		     $this->set('siteaddrResult',$siteaddrResult); 
		     #end code
		     
		     $this->loadModel('Signature');
		     $chkSign = $this->Signature->find('first',array('conditions'=>array('Signature.servicecall_id'=>$_REQUEST['serviceCallID'],'Signature.report_id'=>$_REQUEST['reportID'])));			
		     $this->set('chkSign',$chkSign);
		     $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		     $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID']))); 
		     $this->set('spRes',$spResult);
		     $this->set('spId',$_REQUEST['spID']);
		     $this->set('clientId',$_REQUEST['clientID']);
		     $this->set('clientRes',$clientResult);
		     $this->set('servicecallId',$_REQUEST['serviceCallID']);
		     //$this->set('inspector_id',$inspector_id);
		     $this->set('reportId',$_REQUEST['reportID']);
		     $this->set('reportType',Configure::read('reportType'));
		     
		     $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		     $reqScheduleId = $schedule[0]['Schedule']['id'];
		     $this->set('scheduleId',$reqScheduleId);
		     $options = array();
		     foreach($schedule[0]['ScheduleService'] as $schedule){
			     $ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			     $options[$ServiceOptions['Service']['id']] = $ServiceOptions['Service']['name'];
		     }
		     $this->set('options',$options);
		     
		     /*$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
		     $this->set('attachmentdata',$attachmentdata);
		     $notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
		     $this->set('notifierdata',$notifierdata);;*/
		}
		$this->data = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
		$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		$this->set('clientResult',$clientResult);
		$this->set('spResult',$spResult);
		
		$result=$this->data['SpecialhazardReport'];
		$this->set('result',$result);
		
		$data = $this->data['SpecialhazardReportRecord'];
		$this->set('data',$data);
	}	
	
//Function coverPagePrint
//desc: for te pdf preview of cover page of reports 
//Manish Kumar	
	function coverPagePrint(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('FirealarmReport');
		$this->loadModel('Schedule');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.lead_inspector_id','Schedule.sch_freq'),'recursive'=>-1));
			$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
			$inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
			$this->set('inspector',$inspectorResult);
			
			$this->set('reportType',Configure::read('reportType'));
			
			$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
			
		}
	}
	
//Function summaryPagePrint
//desc: for te pdf preview of summary page of reports 
//Manish Kumar	
	function summaryPagePrint(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('FirealarmReport');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
			
			$this->loadModel('Schedule');
			$this->loadModel('ScheduleService'); 
			$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
			$this->set('scheduledata',$scheduledata);
			$this->ScheduleService->bindModel(
						array('belongsTo' => array(	
							'Service' => array(
								'className' => 'Service'
							)
							)
						)
			);
       
			if(!empty($scheduledata)){
				$servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
			}
			$this->set('servicedata',$servicedata);
		}
	}
	
//Function reportPagePrint
//desc: for te pdf preview of summary page of reports 
//Manish Kumar	
	function reportPagePrint(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('FirealarmReport');
		$this->loadModel('Service');
		$this->loadModel('FaAlarmDevicePlace');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
			
			$device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'],'FaAlarmDevicePlace.report_id'=>$_REQUEST['reportID'],'FaAlarmDevicePlace.servicecall_id'=>$_REQUEST['serviceCallID'],'FaAlarmDevicePlace.client_id'=>$_REQUEST['clientID'])));
			$this->set('device_place',$device_place);
			
			$device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
			$this->set('device_type',$device_type);
		}
	}
	
//Function deficiencyPagePrint
//desc: for the pdf preview of deficiency tab of reports 
//Manish Kumar	
	function deficiencyPagePrint($reportType = null){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',$reportType);
			
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array(
							   'belongsTo' => array(
										'Code'=> array(
											'className' => 'Code',
											'foreignKey' => 'code_id',
											'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
		}
	}
	
//Function recommendationPagePrint
//desc: for the pdf preview of recommendation tab of reports 
//Manish Kumar	
	function recommendationPagePrint($reportType=null){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',$reportType);
			
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array(
							   'belongsTo' => array(
										'Code'=> array(
											'className' => 'Code',
											'foreignKey' => 'code_id',
											'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));			
			$this->set('Def_record_recomm',$Def_record);
		}
	}
	
//Function certAttachedPagePrint
//desc: for the pdf preview of cert attached tab of reports 
//Manish Kumar	
	function certAttachedPagePrint($reportType=null){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',$reportType);
			
			$this->loadModel('Schedule');			
			$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
			$this->set('scheduledata',$scheduledata);
		}
	}
	
//Function quotesPagePrint
//desc: for the pdf preview of Quotes tab of reports 
//Manish Kumar	
	function quotesPagePrint($reportType=null){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('Quote');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',$reportType);
			
			#code for quotes		
			$chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
			$this->set('chkRecord',$chkRecord);
		}
	}
	
//Function missingItemsPagePrint
//desc: for the pdf preview of Missing Items tab of reports 
//Manish Kumar	
	function missingItemsPagePrint(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('FirealarmReport');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$this->loadModel('ScheduleService'); 
			$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
			$this->set('scheduledata',$scheduledata);
			$this->ScheduleService->bindModel(
					  array('belongsTo' => array(
					    'Service' => array(
					      'className' => 'Service'
					    )
					  )
			    )
			  );
		       
			if(!empty($scheduledata)){
				$servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
			}		    
			$this->set('servicedata',$servicedata);
			
			$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
		}
	}
	
//Function additionalItemsPagePrint
//desc: for the pdf preview of Additional Items tab of reports 
//Manish Kumar	
	function additionalItemsPagePrint(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('FirealarmReport');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$this->loadModel('ScheduleService'); 
			$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
			$this->set('scheduledata',$scheduledata);
			$this->ScheduleService->bindModel(
					  array('belongsTo' => array(
					    'Service' => array(
					      'className' => 'Service'
					    )
					  )
			    )
			  );
		       
			if(!empty($scheduledata)){
				$servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
			}		    
			$this->set('servicedata',$servicedata);
			
			$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
		}
	}
	
//Function signaturePagePrint
//desc: for the pdf preview of Signature Page tab of reports 
//Manish Kumar	
	function signaturePagePrint($modelName=null){
		$this->layout='';		
		$this->loadModel('User');
		$this->loadModel($modelName);
		$this->set('modelName',$modelName);
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);
			
			$record = $this->$modelName->find('first',array('conditions'=>array($modelName.'.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
		}
	}
//Function firealarmFullReport
//desc: for the pdf preview of full firealarm reports 
//Manish Kumar	
	function firealarmFullReport(){
		set_time_limit(0);
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('FirealarmReport');
		$this->loadModel('Attachment');
		$this->loadModel('Service');
		$this->loadModel('FaAlarmDevicePlace');
		$this->loadModel('Notification');
		$this->loadModel('Quote');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			
			#code for quotes		
			$chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
			$this->set('chkRecord',$chkRecord);
			
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);
			$this->set('spId',$_REQUEST['spID']);
			$this->set('clientId',$_REQUEST['clientID']);
			$this->set('clientResult',$clientResult);
			$this->set('servicecallId',$_REQUEST['serviceCallID']);
			$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
			$device_place = $this->FaAlarmDevicePlace->find('list',array('conditions'=>array('FaAlarmDevicePlace.sp_id'=>$_REQUEST['spID'],'FaAlarmDevicePlace.report_id'=>$_REQUEST['reportID'],'FaAlarmDevicePlace.servicecall_id'=>$_REQUEST['serviceCallID'],'FaAlarmDevicePlace.client_id'=>$_REQUEST['clientID'])));
			$this->set('device_place',$device_place);	
			###########################################
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$this->loadModel('ScheduleService'); 
			$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id','Schedule.lead_inspector_id','Schedule.sch_freq'),'recursive'=> -1));
			$this->set('scheduledata',$scheduledata);
			$this->set('schFreq',$scheduledata['Schedule']['sch_freq']);
			
			$inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$scheduledata['Schedule']['lead_inspector_id']),'recursive'=>1));
			$this->set('inspector',$inspectorResult);
			
			$this->ScheduleService->bindModel(
					  array('belongsTo' => array(
					    'Service' => array(
					      'className' => 'Service'
					    )
					  )
			    )
			  );
	       
			if(!empty($scheduledata)){
				$servicedata = $this->ScheduleService->find('all',array('conditions'=>array('ScheduleService.schedule_id'=>$scheduledata['Schedule']['id'])));  
		        }
	    
			$this->set('servicedata',$servicedata);
			############################################
			$device_type = $this->Service->find('list',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'])));	  
			$this->set('device_type',$device_type);		
			$record = $this->FirealarmReport->find('first',array('conditions'=>array('FirealarmReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
			$attachmentdata = $this->Attachment->find('all',array('conditions'=>array('Attachment.report_id'=>$_REQUEST['reportID'],'Attachment.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('attachmentdata',$attachmentdata);
			$notifierdata= $this->Notification->find('all',array('conditions'=>array('Notification.report_id'=>$_REQUEST['reportID'],'Notification.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('notifierdata',$notifierdata);
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
														'Code'=> array(
																	'className' => 'Code',
																	'foreignKey' => 'code_id',
																	'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
																	)
														)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);			
		}
	}
	
//Function coverPagePrintExtinguishers
//desc: for the pdf preview of cover page of extinguishers reports 
//Manish Kumar	
	function coverPagePrintExtinguishers(){
		$this->layout='';		
		$this->loadModel('User');
		$this->loadModel('Schedule');
		$this->loadModel('ExtinguisherReport');		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.lead_inspector_id','Schedule.sch_freq'),'recursive'=>-1));
			$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
			$inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
			$this->set('inspector',$inspectorResult);
			$this->set('reportType',Configure::read('reportType'));
			$record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);	
			
		   }
	}		
//Function summaryPagePrintExtinguisher
//desc: for te pdf preview of summary page of extinguisher reports 
//Manish Kumar	
	function summaryPagePrintExtinguisher(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('Schedule');
		$this->loadModel('Service');
		$this->loadModel('ExtinguisherReport');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			
			$result = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
		 
			foreach($result['ExtinguisherReportRecord'] as $key1=>$record){
				    if($record['deficiency'] == "Yes"){
					       $arr1[$record['ext_type']]['fail'][$key1] = 1;
					}
					if($record['deficiency'] == "No"){
					       $arr1[$record['ext_type']]['pass'][$key1] = 1;
					}				  
					       $arr[$key1] = $record['ext_type'];
			       }
		        $arr_count = array_count_values($arr);
			
			$options = array();
			$val = "";
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
				$options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
						$options[$key]['pass'] = 0;	
						$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
	
//Function reportPagePrintExtinguisher
//desc: for te pdf preview of summary page of extinguisher reports 
//Manish Kumar	
	function reportPagePrintExtinguisher(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('ExtinguisherReport');		
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			$record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);			
			
		}
	}
	
//Function deficiencyPagePrintExtinguisher
//desc: for the pdf preview of deficiency tab of extinguisher reports 
//Manish Kumar	
	function deficiencyPagePrintExtinguisher(){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array(
							   'belongsTo' => array(
										'Code'=> array(
											'className' => 'Code',
											'foreignKey' => 'code_id',
											'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
		}
	}
	
//Function recommendationPagePrintExtinguisher
//desc: for the pdf preview of recommendation tab of extinguisher reports 
//Manish Kumar	
	function recommendationPagePrintExtinguisher(){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array(
							   'belongsTo' => array(
										'Code'=> array(
											'className' => 'Code',
											'foreignKey' => 'code_id',
											'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));			
			$this->set('Def_record_recomm',$Def_record);
		}
	}
	
//Function missingItemsPagePrintExtinguisher
//desc: for the pdf preview of Missing Items tab of extinguisher reports 
//Manish Kumar	
	function missingItemsPagePrintExtinguisher(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('ExtinguisherReport');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			// pr($schedule);
			$result = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
			 //pr()
			 foreach($result['ExtinguisherReportRecord'] as $key1=>$record){
				     if($record['deficiency'] == "Yes"){
						$arr1[$record['ext_type']]['fail'][$key1] = 1;
					 }
					 if($record['deficiency'] == "No"){
						$arr1[$record['ext_type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['ext_type'];
				}
			$arr_count = array_count_values($arr);
			$options = array();
			$val = "";
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
			    $options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
						$options[$key]['pass'] = 0;	
						$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
	
//Function additionalItemsPagePrintExtinguisher
//desc: for the pdf preview of Additional Items tab of Extinguisher reports 
//Manish Kumar	
	function additionalItemsPagePrintExtinguisher(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('ExtinguisherReport');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			// pr($schedule);
			$result = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
			 //pr()
			 foreach($result['ExtinguisherReportRecord'] as $key1=>$record){
				     if($record['deficiency'] == "Yes"){
						$arr1[$record['ext_type']]['fail'][$key1] = 1;
					 }
					 if($record['deficiency'] == "No"){
						$arr1[$record['ext_type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['ext_type'];
				}
			$arr_count = array_count_values($arr);
			$options = array();
			$val = "";
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
			    $options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
						$options[$key]['pass'] = 0;	
						$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
	
//Function signaturePagePrintExtinguisher
//desc: for the pdf preview of Signature Page tab of extinguisher reports 
//Manish Kumar	
	function signaturePagePrintExtinguisher(){
		$this->layout='';		
		$this->loadModel('User');
		$this->loadModel('ExtinguisherReport');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);
			
			$record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
		}
	}
	
//Function certAttachedPagePrintExtinguisher
//desc: for the pdf preview of cert attached tab of extinguisher reports 
//Manish Kumar	
	function certAttachedPagePrintExtinguisher(){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			$this->loadModel('Schedule');			
			$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
			$this->set('scheduledata',$scheduledata);
		}
	}
	
//Function quotesPagePrintExtinguisher
//desc: for the pdf preview of Quotes tab of extinguisher reports 
//Manish Kumar	
	function quotesPagePrintExtinguisher(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('Quote');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#code for quotes		
			$chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
			$this->set('chkRecord',$chkRecord);
		}
	}
	
//Function extinguisherFullReport
//desc: for the pdf preview of full extinguisher reports 
//Manish Kumar	
	function extinguisherFullReport(){
		set_time_limit(0);
		$this->layout='';		
		$this->loadModel('User');
		$this->loadModel('Schedule');
		$this->loadModel('Attachment');
		$this->loadModel('Notification');
		$this->loadModel('ExtinguisherReport');
		$this->loadModel('Service');
		$this->loadModel('Quote');
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		
			#code for quotes		
			$chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
			$this->set('chkRecord',$chkRecord);
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);
			$this->set('spId',$_REQUEST['spID']);
			$this->set('clientId',$_REQUEST['clientID']);
			$this->set('clientResult',$clientResult);
			$this->set('servicecallId',$_REQUEST['serviceCallID']);			
			$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
			$record = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);		
			
			// start of script for find the summary page
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
			 
			 $inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
			 $this->set('inspector',$inspectorResult);
			 //pr($schedule);die;
			 
			$result = $this->ExtinguisherReport->find('first',array('conditions'=>array('ExtinguisherReport.servicecall_id'=>$_REQUEST['serviceCallID'],'ExtinguisherReport.report_id'=>$_REQUEST['reportID'])));
			 //pr()
			foreach($result['ExtinguisherReportRecord'] as $key1=>$record){
				if($record['deficiency'] == "Yes"){
					$arr1[$record['ext_type']]['fail'][$key1] = 1;
				}
				if($record['deficiency'] == "No"){
					$arr1[$record['ext_type']]['pass'][$key1] = 1;
				}				  
				$arr[$key1] = $record['ext_type'];
			}
			$arr_count = array_count_values($arr);
			$options = array();
			$val = "";
			foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
				$options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				}else{
						$options[$key]['pass'] = 0;	
						$options[$key]['fail'] = 0;	
				}
			}
			$this->set('options',$options);
			//End of script of sumary page
			#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
										'Code'=> array(
													'className' => 'Code',
													'foreignKey' => 'code_id',
													'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
													)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);		
		}
	}
	
//Function inspector_fillDeficiency
//Desc To fill the deficiency report
//Created By Manish Kumar
	function inspector_fillDeficiency(){
		$this->set('title_for_layout',"Fill Deficiency");
		$this->layout='';
		$this->loadModel('Code');
		$codes = $this->Code->find('list',array('conditions'=>array('Code.report_id'=>$_REQUEST['reportID']),'fields'=>array('Code.id','Code.description')));
		$this->set('codes',$codes);
		$this->loadModel('Deficiency');
		$chkExistance = $this->Deficiency->find('first',array('conditions'=>array('Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.sp_id'=>$_REQUEST['spId'],'Deficiency.client_id'=>$_REQUEST['clientId'],'Deficiency.servicecall_id'=>$_REQUEST['serviceCallId'],'Deficiency.row_no'=>$_REQUEST['rowId'])));
		$this->set('chkExistance',$chkExistance);
		if(!empty($this->data)){
			$this->data['Deficiency']['report_id'] = $_REQUEST['reportID'];
			$this->data['Deficiency']['client_id'] = $_REQUEST['clientId'];
			$this->data['Deficiency']['sp_id'] = $_REQUEST['spId'];
			$this->data['Deficiency']['servicecall_id'] = $_REQUEST['serviceCallId'];
			
			$this->data['Deficiency']['row_no'] = $_REQUEST['rowId'];
			if(isset($_REQUEST['locationData']) && $_REQUEST['locationData']!=""){
				$this->data['Deficiency']['location'] = $_REQUEST['locationData'];
			}
			if(isset($_REQUEST['modelData']) && $_REQUEST['modelData']!=""){
				$this->data['Deficiency']['model_field'] = $_REQUEST['modelData'];
			}
			if(isset($_REQUEST['mfdDate']) && $_REQUEST['mfdDate']!=""){
				$this->data['Deficiency']['mfd_date'] = $_REQUEST['mfdDate'];
			}
			
			$logininfo = $this->Session->read('Log');				
			$ins_id = $logininfo['User']['id'];
			
			$this->data['Deficiency']['inspector_id'] = $ins_id;
			$this->data['Deficiency']['service_id'] = $_REQUEST['serviceId'];
			
			/**/
			if(isset($this->data['Deficiency']['attachdata']['name']) && !empty($this->data['Deficiency']['attachdata']['name'])){
				$temp_exp = explode('.',$this->data['Deficiency']['attachdata']['name']);
				$extPart = end($temp_exp);
				// Start of file uploading script		
				$allowedExts = array("pdf", "xls", "docx", "xlsx","doc","png","jpg","jpeg","gif","bmp");
				if(in_array($extPart, $allowedExts)){
					$target = WWW_ROOT.'img/defImage/';
					//$filenametosave= uniqid('KITCHEN').".".$temp_exp[1];
					$randomName = md5(time()).'.'.$extPart;
					$target = $target.$randomName;
					if(move_uploaded_file($this->data['Deficiency']['attachdata']['tmp_name'], $target)){						
						$this->data['Deficiency']['attachment'] = $randomName;	
					} 
				}
				else{
					$this->Session->setFlash(__('Sorry ! Please upload valid extension file',true));
					$this->redirect($this->referer());
				}
			}
			/**/
			
			$this->Deficiency->save($this->data);
			$this->Session->setFlash('Deficiency has been added successfully');
			$this->redirect($this->referer());
		}
	}
	
//Function inspector_deleteDeficiency
//Desc to delete the added deficiency
//Created By Manish Kumar On Dec 18, 2012
	function inspector_deleteDeficiency(){
		$reportId = $_POST['reportID'];
		$clientId = $_POST['clientId'];
		$spId = $_POST['spId'];
		$serviceCallId = $_POST['serviceCallId'];		
		$rowId = $_POST['rowId'];
		$this->loadModel('Deficiency');
		$chkExistance = $this->Deficiency->deleteAll(array('Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.sp_id'=>$_REQUEST['spId'],'Deficiency.client_id'=>$_REQUEST['clientId'],'Deficiency.servicecall_id'=>$_REQUEST['serviceCallId'],'Deficiency.row_no'=>$_REQUEST['rowId']));
		if($chkExistance){
			echo 'Success';
		}else{
			echo 'Fail';
		}
		die;
	}
	
//Function coverPagePrintEmergency
//desc: for the pdf preview of cover page of emergency/exit reports 
//Manish Kumar	
	function coverPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('EmergencyexitReport');
		$this->loadModel('Schedule');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.lead_inspector_id','Schedule.sch_freq'),'recursive'=>-1));
			$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
			$inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
			$this->set('inspector',$inspectorResult);
			
			$this->set('reportType',Configure::read('reportType'));
			
			$record = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
			
		}
	}
	
//Function summaryPagePrintEmergency
//desc: for the pdf preview of summary page emergency exit reports 
//Manish Kumar	
	function summaryPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');		
		$this->loadModel('Schedule');
		$this->loadModel('Service');
		$this->loadModel('EmergencyexitReport');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			//$this->set('reportType',Configure::read('reportType'));
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$result = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
			//$this->set('record',$result);
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array()));
			//$reqScheduleId = $schedule[0]['Schedule']['id'];
			//$this->set('schId',$reqScheduleId);			 
			 
			foreach($result['EmergencyexitReportRecord'] as $key1=>$record){
				     if($record['status'] == "Pass"){
						$arr1[$record['type']]['fail'][$key1] = 1;
					 }
					 if($record['status'] == "Fail"){
						$arr1[$record['type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['type'];
				}
			$arr_count = array_count_values($arr);
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			 
			
			$options = array();
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
			    $options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
						$options[$key]['pass'] = 0;	
						$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
	
//Function reportPagePrintEmergency
//desc: for the pdf preview of report page of emergency exit report 
//Manish Kumar	
	function reportPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('EmergencyexitReport');	
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);						
			
			$record = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);		
		}
	}
	
//Function deficiencyPagePrintEmergency
//desc: for the pdf preview of deficiency tab of emergency reports 
//Manish Kumar	
	function deficiencyPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);						
			
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array(
							   'belongsTo' => array(
										'Code'=> array(
											'className' => 'Code',
											'foreignKey' => 'code_id',
											'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
		}
	}
	
//Function recommendationPagePrintEmergency
//desc: for the pdf preview of recommendation tab of emergency reports 
//Manish Kumar	
	function recommendationPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array(
							   'belongsTo' => array(
										'Code'=> array(
											'className' => 'Code',
											'foreignKey' => 'code_id',
											'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));			
			$this->set('Def_record_recomm',$Def_record);
		}
	}
	
//Function missingItemsPagePrintEmergency
//desc: for the pdf preview of Missing Items tab of emergency exit reports 
//Manish Kumar	
	function missingItemsPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('EmergencyexitReport');
		$this->loadModel('Schedule');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			
			$result = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
			//$this->set('record',$result);
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array()));
			//$reqScheduleId = $schedule[0]['Schedule']['id'];
			//$this->set('schId',$reqScheduleId);			 
			 
			foreach($result['EmergencyexitReportRecord'] as $key1=>$record){
				     if($record['status'] == "Pass"){
						$arr1[$record['type']]['fail'][$key1] = 1;
					 }
					 if($record['status'] == "Fail"){
						$arr1[$record['type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['type'];
				}
			$arr_count = array_count_values($arr);
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			 
			
			$options = array();
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
			    $options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
						$options[$key]['pass'] = 0;	
						$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
	
//Function additionalItemsPagePrintEmergency
//desc: for the pdf preview of Additional Items tab of Emergency Exit reports 
//Manish Kumar	
	function additionalItemsPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('EmergencyexitReport');
		$this->loadModel('Schedule');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);						
			
			$result = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));			
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array()));			
			 
			foreach($result['EmergencyexitReportRecord'] as $key1=>$record){
				     if($record['status'] == "Pass"){
						$arr1[$record['type']]['fail'][$key1] = 1;
					 }
					 if($record['status'] == "Fail"){
						$arr1[$record['type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['type'];
				}
			$arr_count = array_count_values($arr);
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			 
			
			$options = array();
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
			    $options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
						$options[$key]['pass'] = 0;	
						$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}	
//Function signaturePagePrintEmergency
//desc: for the pdf preview of Signature Page tab of emergency exit reports 
//Manish Kumar	
	function signaturePagePrintEmergency(){
		$this->layout='';		
		$this->loadModel('User');
		$this->loadModel('EmergencyexitReport');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);
			
			$record = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
		}
	}	
//Function certAttachedPagePrintEmergency
//desc: for the pdf preview of cert attached tab of emergency exit reports 
//Manish Kumar	
	function certAttachedPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');							
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);						
			
			$this->loadModel('Schedule');			
			$scheduledata = $this->Schedule->find('first',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.id'),'recursive'=> -1));
			$this->set('scheduledata',$scheduledata);
		}
	}
//Function quotesPagePrintEmergency
//desc: for the pdf preview of Quotes tab of emergency exit reports 
//Manish Kumar	
	function quotesPagePrintEmergency(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('Quote');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);						
			
			#code for quotes		
			$chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
			$this->set('chkRecord',$chkRecord);
		}
	}
//Function emergencyexitFullReport
//Desc: to print the pdf preview of the full report of emergency exit report
//Created By Manish Kumar
	function emergencyexitFullReport(){
		set_time_limit(0);
		$this->layout='';		
		$this->loadModel('User');
		$this->loadModel('EmergencyexitReport');
		$this->loadModel('EmergencyexitReportRecord');		
		$this->loadModel('Schedule');
		$this->loadModel('Service');
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			#code on 4 oct,2012 for Site address display
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult); 
			#end code
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);
			$this->set('spId',$_REQUEST['spID']);
			$this->set('clientId',$_REQUEST['clientID']);
			$this->set('clientResult',$clientResult);
			$this->set('servicecallId',$_REQUEST['serviceCallID']);			
			$this->set('reportId',$_REQUEST['reportID']);
			$this->set('reportType',Configure::read('reportType'));
				
			$result = $this->EmergencyexitReport->find('first',array('conditions'=>array('EmergencyexitReport.servicecall_id'=>$_REQUEST['serviceCallID'],'EmergencyexitReport.report_id'=>$_REQUEST['reportID'])));
			$this->set('record',$result);
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array()));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
			
			$inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
			$this->set('inspector',$inspectorResult);
			 
			foreach($result['EmergencyexitReportRecord'] as $key1=>$record){
				if($record['status'] == "Pass"){
					$arr1[$record['type']]['fail'][$key1] = 1;
				}
				if($record['status'] == "Fail"){
					$arr1[$record['type']]['pass'][$key1] = 1;
				}				  
				$arr[$key1] = $record['type'];
			}
			$arr_count = array_count_values($arr);			 
			$options = array();
			foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
				$options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				}else{
						$options[$key]['pass'] = 0;	
						$options[$key]['fail'] = 0;	
				}
			}
			$this->set('options',$options);
			
			#CODE for deficiency page in view
			$this->loadModel('Deficiency');
			$this->Deficiency->bindModel(array('belongsTo' => array(
										'Code'=> array(
													'className' => 'Code',
													'foreignKey' => 'code_id',
													'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
													)
										)
									 )
							   );
			$Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
			
			$this->set('Def_record',$Def_record);
			$this->set('Def_record_recomm',$Def_record);
		}
	}
	
	function downloadImage($fileName=null){
		$mimeType = array('bmp'=>'image/bmp','docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','gif'=>'image/gif','jpg'=>'image/jpeg','html'=>'text/html','mp3'=>'audio/mpeg','mpeg'=>'video/mpeg','ppt'=>'application/vnd.ms-powerpoint','txt'=>'text/plain','zip'=>'application/x-compressed-zip','xml'=>'application/xml','xls'=>'application/vnd.ms-excel');
		$getname =  explode(".",$fileName);
		$this->view = 'Media';
		$params = array(
		'id' => $fileName,
		'name' => $getname[0],
		'download' => true,
		'extension' => $getname[1], // must be lower case
		'mimeType' => $mimeType,
		//'path' => APP . 'files' . DS // don't forget terminal 'DS'
		'path' => WWW_ROOT."img/defImage/"
		);
		$this->set($params);
	}
	
	function deleteRecord($fileName=null){
		if($fileName!=null){
			if(file_exists(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$fileName)){
				unlink(ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'defImage'.DS.$fileName);
				$this->loadModel('Deficiency');
				$this->Deficiency->updateAll(array('Deficiency.attachment'=>'""'),array('Deficiency.attachment'=>$fileName));
			}
			$this->Session->setFlash('Attahcment deleted successfully');
			$this->redirect($this->referer());
		}
	}
	
//Function coverPagePrintCommon
//desc: for te pdf preview of cover page of reports 
//Manish Kumar	On Jan 16, 2013
	function coverPagePrintCommon($modelName){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel($modelName);
		$this->loadModel('Schedule');
		$this->set('modelName',$modelName);
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);
			
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array('Schedule.lead_inspector_id','Schedule.sch_freq'),'recursive'=>-1));
			$this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
			$inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
			$this->set('inspector',$inspectorResult);
			
			$this->set('reportType',Configure::read('reportType'));
			
			$record = $this->$modelName->find('first',array('conditions'=>array($modelName.'.servicecall_id'=>$_REQUEST['serviceCallID'])));
			$this->set('record',$record);
			
		}
	}	

}