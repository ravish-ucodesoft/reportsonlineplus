<?php
    class ReportPrintsController extends AppController{
	var $name = 'ReportPrints';
	var $uses = array('User');
	var $components = array('Customupload','Calendar','Email','Session','Cookie');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session','Calendar','Common','Text','Number','Time');

	function beforeFilter(){
            parent::beforeFilter();
	}
        
        //Function reportPagePrintSprinkler
        //Desc: to print the report page of sprinkler report
        //Created By Manish Kumar On Jan 21, 2013
	function reportPagePrintSprinkler(){
	    $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }	
	}
        
        function generalPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function controlPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function waterPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function connectionPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function wetPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function dryPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function specialPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function alarmPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function sprinklerpipingPagePrintSprinkler(){
            $this->layout='';
            $this->loadModel('User');
            $this->loadModel('SprinklerReport');       
            
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
                    
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult);
                    
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);			
                    $this->set('clientResult',$clientResult);                    
                    
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
            }
        }
        
        function sprinklerFullReport(){
            set_time_limit(0);
            $this->layout='';      	
            $this->loadModel('User');
            $this->loadModel('Schedule');            
            $this->loadModel('SprinklerReport');
            $this->loadModel('Quote');
            if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
                    
                    $chkRecord = $this->Quote->find('first',array('conditions'=>array('Quote.servicecall_id'=>$_REQUEST['serviceCallID'],'Quote.report_id'=>$_REQUEST['reportID'])));
                    $this->set('chkRecord',$chkRecord);
                    
                    #code on 4 oct,2012 for Site address display
                    $this->loadModel('ServiceCall');
                    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
                    $this->loadModel('SpSiteAddress');
                    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
                    $this->set('siteaddrResult',$siteaddrResult); 
                    #end code
                    
                    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
                    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
                    $this->set('spResult',$spResult);
                    $this->set('spId',$_REQUEST['spID']);
                    $this->set('clientId',$_REQUEST['clientID']);
                    $this->set('clientResult',$clientResult);
                    $this->set('servicecallId',$_REQUEST['serviceCallID']);
                    //$this->set('inspector_id',$inspector_id);
                    $this->set('reportId',$_REQUEST['reportID']);
                    $this->set('reportType',Configure::read('reportType'));
                    $record = $this->SprinklerReport->find('first',array('conditions'=>array('SprinklerReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SprinklerReport.report_id'=>$_REQUEST['reportID'])));
                    $this->set('record',$record);
                    
                    $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID']),'fields'=>array()));
                    $reqScheduleId = $schedule[0]['Schedule']['id'];
                    $this->set('schId',$reqScheduleId);
                    $this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
                    
                    $inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
                    $this->set('inspector',$inspectorResult);
                    
                    #CODE for deficiency page in view
                    $this->loadModel('Deficiency');
                    $this->Deficiency->bindModel(array('belongsTo' => array(
                                                                            'Code'=> array(
                                                                                            'className' => 'Code',
                                                                                            'foreignKey' => 'code_id',
                                                                                            'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
                                                                                            )
                                                                            )
                                                                     )
                                                       );
                    $Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
                    $this->set('Def_record',$Def_record);
                    $this->set('Def_record_recomm',$Def_record);
                            
            }
        }
	
	function summaryPagePrintKitchenhood(){
	    $this->layout='';
	    $this->loadModel('User');
	    $this->loadModel('Schedule');
	    $this->loadModel('Service');
	    $this->loadModel('KitchenhoodReport');
	    
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
		    
		    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		    $this->set('spResult',$spResult);			
		    $this->set('clientResult',$clientResult);			
		    $this->set('reportType',Configure::read('reportType'));
		    
		    #code on 4 oct,2012 for Site address display
		    $this->loadModel('ServiceCall');
		    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		    $this->loadModel('SpSiteAddress');
		    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		    $this->set('siteaddrResult',$siteaddrResult); 
		    #end code
		    
		    $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		    
		    $result = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'],'KitchenhoodReport.report_id'=>$_REQUEST['reportID'])));
	     
		    foreach($result['KitchenhoodReportRecord'] as $key1=>$record){
				if($record['deficiency'] == "Yes"){
					   $arr1[$record['type']]['fail'][$key1] = 1;
				    }
				    if($record['deficiency'] == "No"){
					   $arr1[$record['type']]['pass'][$key1] = 1;
				    }				  
					   $arr[$key1] = $record['type'];
			   }
		    $arr_count = array_count_values($arr);
		    
		    $options = array();
		    $val = "";
		     foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
			    $ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			    $options[$key]['name'] = $ServiceOptions['Service']['name'];
			    $options[$key]['service_id'] = $ServiceOptions['Service']['id'];
			    $options[$key]['amount'] = $schedule['amount'];
			    $options[$key]['amount'] = $schedule['amount'];
			    if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
				    $options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
			    }else{
				    $options[$key]['served'] = 0;
			    }
			     if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
				     if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
					    $options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
				     }
				     else{
					    $options[$key]['pass'] = 0;	
				     }
				    if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
					    $options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
				     }else{
					    $options[$key]['fail'] = 0;	
				    }	
			     }else{
					    $options[$key]['pass'] = 0;	
					    $options[$key]['fail'] = 0;	
			     }
		    }
		    $this->set('options',$options);
	    }
	}
	
	function reportPagePrintKitchenhood(){
	    $this->layout='';
	    $this->loadModel('User');
	    $this->loadModel('KitchenhoodReport');		
	    
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
		    
		    $this->loadModel('ServiceCall');
		    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		    $this->loadModel('SpSiteAddress');
		    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		    $this->set('siteaddrResult',$siteaddrResult);
		    
		    
		    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		    $this->set('spResult',$spResult);			
		    $this->set('clientResult',$clientResult);			
		    $this->set('reportType',Configure::read('reportType'));
		    
		    $record = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		    $this->set('record',$record);			
		    
	    }
	}
	
	function missingItemsPagePrintKitchenhood(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('KitchenhoodReport');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			// pr($schedule);
			$result = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'],'KitchenhoodReport.report_id'=>$_REQUEST['reportID'])));
			 //pr()
			 foreach($result['KitchenhoodReportRecord'] as $key1=>$record){
				     if($record['deficiency'] == "Yes"){
						$arr1[$record['type']]['fail'][$key1] = 1;
					 }
					 if($record['deficiency'] == "No"){
						$arr1[$record['type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['type'];
				}
			$arr_count = array_count_values($arr);
			$options = array();
			$val = "";
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
				$options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
					$options[$key]['pass'] = 0;	
					$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
	
	function additionalItemsPagePrintKitchenhood(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('KitchenhoodReport');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			// pr($schedule);
			$result = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'],'KitchenhoodReport.report_id'=>$_REQUEST['reportID'])));
			 //pr()
			 foreach($result['KitchenhoodReportRecord'] as $key1=>$record){
				     if($record['deficiency'] == "Yes"){
						$arr1[$record['type']]['fail'][$key1] = 1;
					 }
					 if($record['deficiency'] == "No"){
						$arr1[$record['type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['type'];
				}
			$arr_count = array_count_values($arr);
			$options = array();
			$val = "";
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
				$options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
					$options[$key]['pass'] = 0;	
					$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
        
	
	function kitchenhoodFullReport(){
            set_time_limit(0);
            $this->layout='';
	    $inspectorses = $this->Session->read('Log');	    
	    $this->loadModel('User');
	    $this->loadModel('Schedule');
	    $this->loadModel('KitchenhoodReport');	    
	    $this->loadModel('Service');
	    $this->loadModel('Quote');
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		    
		    #code on 4 oct,2012 for Site address display
		    $this->loadModel('ServiceCall');
		    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		    $this->loadModel('SpSiteAddress');
		    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		    $this->set('siteaddrResult',$siteaddrResult); 
		    #end code
		    
		    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		    $this->set('spResult',$spResult);
		    $this->set('spId',$_REQUEST['spID']);
		    $this->set('clientId',$_REQUEST['clientID']);
		    $this->set('clientResult',$clientResult);
		    $this->set('servicecallId',$_REQUEST['serviceCallID']);
		    
		    $this->set('reportId',$_REQUEST['reportID']);
		    $this->set('reportType',Configure::read('reportType'));
		    $record = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		    $this->set('record',$record);    
		    
		    // start of script for find the summary page
		    $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		    $reqScheduleId = $schedule[0]['Schedule']['id'];
		    $this->set('schId',$reqScheduleId);
		    $this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
		    
		    $inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
                    $this->set('inspector',$inspectorResult);
		    
		    $result = $this->KitchenhoodReport->find('first',array('conditions'=>array('KitchenhoodReport.servicecall_id'=>$_REQUEST['serviceCallID'],'KitchenhoodReport.report_id'=>$_REQUEST['reportID'])));		    
		    foreach($result['KitchenhoodReportRecord'] as $key1=>$record){
			if($record['deficiency'] == "Yes"){
				   $arr1[$record['type']]['fail'][$key1] = 1;
			    }
			    if($record['deficiency'] == "No"){
				   $arr1[$record['type']]['pass'][$key1] = 1;
			    }				  
			   $arr[$key1] = $record['type'];
		    }
		    $arr_count = array_count_values($arr);
		    $options = array();
		    $val = "";
		     foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
			    $ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			    $options[$key]['name'] = $ServiceOptions['Service']['name'];
			    $options[$key]['service_id'] = $ServiceOptions['Service']['id'];
			    $options[$key]['amount'] = $schedule['amount'];			
			    if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
				    $options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
			    }else{
				    $options[$key]['served'] = 0;
			    }
			     if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
				     if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
					    $options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
				     }
				     else{
					    $options[$key]['pass'] = 0;	
				     }
				    if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
					    $options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
				     }else{
					    $options[$key]['fail'] = 0;	
				    }	
			     }else{
				    $options[$key]['pass'] = 0;	
				    $options[$key]['fail'] = 0;	
			     }
		    }
		    $this->set('options',$options);
		    //End of script of sumary page
		    
		    $this->loadModel('Deficiency');
		    $this->Deficiency->bindModel(array('belongsTo' => array(
									    'Code'=> array(
											    'className' => 'Code',
											    'foreignKey' => 'code_id',
											    'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											    )
									    )
								     )
						       );
		    $Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		    $this->set('Def_record',$Def_record);
		    $this->set('Def_record_recomm',$Def_record);
	    }
	}
	
	//SpecialHazard Report print functions
	function summaryPagePrintSh(){
	    $this->layout='';
	    $this->loadModel('User');
	    $this->loadModel('Schedule');
	    $this->loadModel('Service');
	    $this->loadModel('SpecialhazardReport');
	    
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
		    
		    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		    $this->set('spResult',$spResult);			
		    $this->set('clientResult',$clientResult);			
		    $this->set('reportType',Configure::read('reportType'));
		    
		    #code on 4 oct,2012 for Site address display
		    $this->loadModel('ServiceCall');
		    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		    $this->loadModel('SpSiteAddress');
		    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		    $this->set('siteaddrResult',$siteaddrResult); 
		    #end code
		    
		    $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		    
		    $result = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
	     
		    foreach($result['SpecialhazardReportRecord'] as $key1=>$record){
				if($record['deficiency'] == "Yes"){
					   $arr1[$record['type']]['fail'][$key1] = 1;
				    }
				    if($record['deficiency'] == "No"){
					   $arr1[$record['type']]['pass'][$key1] = 1;
				    }				  
					   $arr[$key1] = $record['type'];
			   }
		    $arr_count = array_count_values($arr);
		    
		    $options = array();
		    $val = "";
		     foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
			    $ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			    $options[$key]['name'] = $ServiceOptions['Service']['name'];
			    $options[$key]['service_id'] = $ServiceOptions['Service']['id'];
			    $options[$key]['amount'] = $schedule['amount'];
			    $options[$key]['amount'] = $schedule['amount'];
			    if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
				    $options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
			    }else{
				    $options[$key]['served'] = 0;
			    }
			     if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
				     if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
					    $options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
				     }
				     else{
					    $options[$key]['pass'] = 0;	
				     }
				    if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
					    $options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
				     }else{
					    $options[$key]['fail'] = 0;	
				    }	
			     }else{
					    $options[$key]['pass'] = 0;	
					    $options[$key]['fail'] = 0;	
			     }
		    }
		    $this->set('options',$options);
	    }
	}
	
	function reportPagePrintSh(){
	    $this->layout='';
	    $this->loadModel('User');
	    $this->loadModel('SpecialhazardReport');		
	    
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
		    
		    $this->loadModel('ServiceCall');
		    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		    $this->loadModel('SpSiteAddress');
		    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		    $this->set('siteaddrResult',$siteaddrResult);
		    
		    
		    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		    $this->set('spResult',$spResult);			
		    $this->set('clientResult',$clientResult);			
		    $this->set('reportType',Configure::read('reportType'));
		    
		    $record = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		    $this->set('record',$record);			
		    
	    }
	}
	
	function missingItemsPagePrintSh(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('SpecialhazardReport');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			// pr($schedule);
			$result = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
			 //pr()
			 foreach($result['SpecialhazardReportRecord'] as $key1=>$record){
				     if($record['deficiency'] == "Yes"){
						$arr1[$record['type']]['fail'][$key1] = 1;
					 }
					 if($record['deficiency'] == "No"){
						$arr1[$record['type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['type'];
				}
			$arr_count = array_count_values($arr);
			$options = array();
			$val = "";
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
				$options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
					$options[$key]['pass'] = 0;	
					$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
	
	function additionalItemsPagePrintSh(){
		$this->layout='';
		$this->loadModel('User');
		$this->loadModel('SpecialhazardReport');
		$this->loadModel('Service');
		
		if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){			
			
			$this->loadModel('ServiceCall');
			$servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
			$this->loadModel('SpSiteAddress');
			$siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
			$this->set('siteaddrResult',$siteaddrResult);
			
			$spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
			$clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
			$this->set('spResult',$spResult);			
			$this->set('clientResult',$clientResult);			
			$this->set('reportType',Configure::read('reportType'));
			
			#Code to get devices added by a given sp
			$this->loadModel('Schedule');
			$schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
			$reqScheduleId = $schedule[0]['Schedule']['id'];
			$this->set('schId',$reqScheduleId);
			// pr($schedule);
			$result = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));
			 //pr()
			 foreach($result['SpecialhazardReportRecord'] as $key1=>$record){
				     if($record['deficiency'] == "Yes"){
						$arr1[$record['type']]['fail'][$key1] = 1;
					 }
					 if($record['deficiency'] == "No"){
						$arr1[$record['type']]['pass'][$key1] = 1;
					 }				  
						$arr[$key1] = $record['type'];
				}
			$arr_count = array_count_values($arr);
			$options = array();
			$val = "";
			 foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
				$ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
				$options[$key]['name'] = $ServiceOptions['Service']['name'];
				$options[$key]['service_id'] = $ServiceOptions['Service']['id'];
				$options[$key]['amount'] = $schedule['amount'];
				$options[$key]['amount'] = $schedule['amount'];
				if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
					$options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
				}else{
					$options[$key]['served'] = 0;
				}
				 if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
					 if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
					 }
					 else{
						$options[$key]['pass'] = 0;	
					 }
					if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
						$options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
					 }else{
						$options[$key]['fail'] = 0;	
					}	
				 }else{
					$options[$key]['pass'] = 0;	
					$options[$key]['fail'] = 0;	
				 }
			}
			$this->set('options',$options);
		}
	}
	
	function specialhazardFullReport(){
            set_time_limit(0);
            $this->layout='';
	    $inspectorses = $this->Session->read('Log');	    
	    $this->loadModel('User');
	    $this->loadModel('Schedule');
	    $this->loadModel('SpecialhazardReport');	    
	    $this->loadModel('Service');
	    $this->loadModel('Quote');
	    if(isset($_REQUEST['spID']) && ($_REQUEST['clientID']) && ($_REQUEST['reportID']) && $_REQUEST['serviceCallID']){
		    
		    #code on 4 oct,2012 for Site address display
		    $this->loadModel('ServiceCall');
		    $servicecallResult = $this->ServiceCall->find('first',array('conditions'=>array('ServiceCall.id'=>$_REQUEST['serviceCallID'])));
		    $this->loadModel('SpSiteAddress');
		    $siteaddrResult = $this->SpSiteAddress->find('first',array('conditions'=>array('SpSiteAddress.id'=>$servicecallResult['ServiceCall']['sp_site_address_id'])));
		    $this->set('siteaddrResult',$siteaddrResult); 
		    #end code
		    
		    $spResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['spID'])));
		    $clientResult = $this->User->find('first',array('conditions'=>array('User.id'=>$_REQUEST['clientID'])));
		    $this->set('spResult',$spResult);
		    $this->set('spId',$_REQUEST['spID']);
		    $this->set('clientId',$_REQUEST['clientID']);
		    $this->set('clientResult',$clientResult);
		    $this->set('servicecallId',$_REQUEST['serviceCallID']);
		    
		    $this->set('reportId',$_REQUEST['reportID']);
		    $this->set('reportType',Configure::read('reportType'));
		    $record = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'])));
		    $this->set('record',$record);    
		    
		    // start of script for find the summary page
		    $schedule = $this->Schedule->find('all',array('conditions'=>array('Schedule.service_call_id'=>$_REQUEST['serviceCallID'],'Schedule.report_id'=>$_REQUEST['reportID'])));
		    $reqScheduleId = $schedule[0]['Schedule']['id'];
		    $this->set('schId',$reqScheduleId);
		    $this->set('schFreq',$schedule[0]['Schedule']['sch_freq']);
		    
		    $inspectorResult = $this->User->find('first',array('conditions'=>array('User.id'=>$schedule[0]['Schedule']['lead_inspector_id']),'recursive'=>1));
                    $this->set('inspector',$inspectorResult);
		    
		    $result = $this->SpecialhazardReport->find('first',array('conditions'=>array('SpecialhazardReport.servicecall_id'=>$_REQUEST['serviceCallID'],'SpecialhazardReport.report_id'=>$_REQUEST['reportID'])));		    
		    foreach($result['SpecialhazardReportRecord'] as $key1=>$record){
			if($record['deficiency'] == "Yes"){
				   $arr1[$record['type']]['fail'][$key1] = 1;
			    }
			    if($record['deficiency'] == "No"){
				   $arr1[$record['type']]['pass'][$key1] = 1;
			    }				  
			   $arr[$key1] = $record['type'];
		    }
		    $arr_count = array_count_values($arr);
		    $options = array();
		    $val = "";
		     foreach($schedule[0]['ScheduleService'] as $key=>$schedule){
			    $ServiceOptions = $this->Service->find('first',array('conditions'=>array('Service.report_id'=>$_REQUEST['reportID'],'Service.id'=>$schedule['service_id']),'fields'=>array('Service.id','Service.name')));
			    $options[$key]['name'] = $ServiceOptions['Service']['name'];
			    $options[$key]['service_id'] = $ServiceOptions['Service']['id'];
			    $options[$key]['amount'] = $schedule['amount'];			
			    if(array_key_exists($ServiceOptions['Service']['id'],$arr_count)){
				    $options[$key]['served'] = $arr_count[$ServiceOptions['Service']['id']];
			    }else{
				    $options[$key]['served'] = 0;
			    }
			     if(array_key_exists($ServiceOptions['Service']['id'],$arr1)){
				     if(array_key_exists("pass",$arr1[$ServiceOptions['Service']['id']])){
					    $options[$key]['pass'] = count($arr1[$ServiceOptions['Service']['id']]['pass']);	
				     }
				     else{
					    $options[$key]['pass'] = 0;	
				     }
				    if(array_key_exists('fail',$arr1[$ServiceOptions['Service']['id']])){
					    $options[$key]['fail'] = count($arr1[$ServiceOptions['Service']['id']]['fail']);				
				     }else{
					    $options[$key]['fail'] = 0;	
				    }	
			     }else{
				    $options[$key]['pass'] = 0;	
				    $options[$key]['fail'] = 0;	
			     }
		    }
		    $this->set('options',$options);
		    //End of script of sumary page
		    
		    $this->loadModel('Deficiency');
		    $this->Deficiency->bindModel(array('belongsTo' => array(
									    'Code'=> array(
											    'className' => 'Code',
											    'foreignKey' => 'code_id',
											    'conditions' =>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])
											    )
									    )
								     )
						       );
		    $Def_record = $this->Deficiency->find('all',array('conditions'=>array('Deficiency.servicecall_id'=>$_REQUEST['serviceCallID'],'Deficiency.report_id'=>$_REQUEST['reportID'],'Deficiency.client_id'=>$_REQUEST['clientID'],'Deficiency.sp_id'=>$_REQUEST['spID'])));
		    $this->set('Def_record',$Def_record);
		    $this->set('Def_record_recomm',$Def_record);
	    }
	}
	
	#made on 30th Jan-2013 by Manish Kumar
	function sp_reportText()
	{
	    $this->set('title_for_layout',"Add Report Text");
	    $logininfo = $this->Session->read('Log');
	    $sp_id = $logininfo['User']['id'];
	    $this->loadModel('SpReportText');
	    $res=$this->SpReportText->find('first',array('conditions'=>array('SpReportText.sp_id'=>$sp_id)));
	    $this->set('res',$res);
	
	    if(!empty($this->data))
	    {
		if(!empty($res))
		{
			$this->SpReportText->id=$res['SpReportText']['id'];
		}		
		$data['SpReportText']['sp_id']=$sp_id;
		$data['SpReportText']['report_text']=$this->data['SpReportText']['report_text'];		
		$this->SpReportText->save($data['SpReportText']);
		$this->Session->setFlash(__('Report Text has been saved successfully',true));
		$this->redirect('reportText');
	    }  
	}
    }