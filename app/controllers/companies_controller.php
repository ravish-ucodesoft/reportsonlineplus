<?php
class CompaniesController extends AppController {

	var $name = 'Companies';
	var $components = array('Email','Session','Cookie');
	var $helpers = array('Html','Ajax','Javascript','Crumb','Session');
  
/*
*************************************************************************
*Function Name		 :	manger_login
*Functionality		 :	Login to Admin Panel.
*************************************************************************
*/ 		
	function manager_login() {
    $this->layout='manager';
    $this->set('title_for_layout',"Manager Login");   
     
    if(isset($this->data)) {
    		if($this->data['Company']['username']=='') {
           $this->Session->setFlash('Please enter Username!');
           $this->redirect(array('controller'=>'companies','action'=>'login'));
        } else if($this->data['Company']['pwd']=='') {
           $this->Session->setFlash('Please enter Password!');
           $this->redirect(array('controller'=>'companies','action'=>'login'));
        }
			$admin = $this->Company->find('first',array('fields'=>array('id','username'),'conditions'=>array('Company.activated'=>'1','Company.pwd'=> md5($this->data['Company']['pwd']),'Company.username'=>$this->data['Company']['username'])));
	
	 		if($admin){	
	 				$this->Session->write('AdminId',$admin['Company']['id']);
  				$this->Session->write('Company',$admin);
  				$this->redirect(array('controller'=>'companies','action'=>'index'));
  			} else {
  				$this->Session->setFlash('Login Failed !');
  			}
  			$this->data['admin']['username']='';
  			$this->data['admin']['pwd']='';
		}
  }

  function manager_index() { 
    $this->layout='manager';
    $this->set('title_for_layout', 'Manager Panel'); 
  }                                                         
      
 /*
*************************************************************************
*Function Name		 :	admin_account
*Functionality		 :	Admin setting.
*************************************************************************
*/ 

function manager_account() {
		$this->layout='admin';

		if(empty($this->data)) {
			$adminData=$this->Admin->find('first',array('conditions'=>array('Admin.id'=>'1')));
			$adminData['Admin']['pwd']='';
			$this->data=$adminData;
				
		$userTypes=$this->Admin->find("list",array("conditions"=>array("id ="=>1)));
		$this->set('userTypes',$userTypes);
		} 
    else if(!empty($this->data))
		{			
		$userTypes=$this->Admin->find("list",array("conditions"=>array("id ="=>1)));
		
		  	$this->Admin->set($this->data);
  		  
        if($this->Admin->validates()) {	
        			
  			  $this->data['Admin']['password'] = md5($this->data['Admin']['pwd']);
  				$this->Admin->save($this->data);				
  				$this->Session->setFlash('Admin account has been updated.','success');
      		$this->redirect(array('action' => 'account'));
      	
        }
		}		
	}
      
/*
*************************************************************************
*Function Name		 :	admin_changepasswd
*Functionality		 :	use to change password of Admin Panel.
*************************************************************************
*/ 
function admin_changepasswd() {
    
		if(isset($this->data)) {
		$adminses=$this->Session->read('Admin');	
		$admindata=$this->Admin->find('first',array("conditions"=>array("username"=>$adminses['Admin']['username'],'Admin.pwd'=> md5($this->data['Admin']['oldpwd']))));	
		if(!$admindata)	{
		 $this->Session->setFlash('Password is not correct.','message');
     $this->redirect(array('controller'=>'admins','action'=>'changepasswd'));
    }
		
		if($this->data['Admin']['pwd'])	{
    		if($this->data['Admin']['pwd']==$this->data['Admin']['cpwd'])	{
    		 $this->data['Admin']['id']=$admindata['Admin']['id'];
    		 $this->data['Admin']['pwd']=md5($this->data['Admin']['pwd']);
        }	else {
         $this->Session->setFlash('New password and confirm password is not same.','message');
         $this->redirect(array('controller'=>'admins','action'=>'changepasswd'));
        }
    		
    } else {
        $this->Session->setFlash('Please enter new password.','message');
        $this->redirect(array('controller'=>'admins','action'=>'changepasswd'));
    }
  
	 if($this->Admin->save($this->data)) {		
		  $this->Session->setFlash('Password has been changed.','success');
    	$this->redirect(array('controller'=>'admins','action'=>'index'));
    }  
   $this->data['Admin']['pwd']='';
  }  
	$this->set('title_for_layout', 'Admin panel - Change password');
}
 
/*
*************************************************************************
*Function Name		 :	admin_logout
*Functionality		 :	Logout from Admin Panel.
**************************************************************************/
 
function admin_logout() {
		$this->Session->delete('Admin');
		$this->redirect(array('controller'=>'admins','action'=>'login'));
}  

/**
 ************************************************************************
 * Lists all subscribers or company owners.
 * @param  n/a
 * @return n/a
 * @access public
 ************************************************************************* 
 */

function admin_subscriberslist() {
    $this->layout='admin';
    $this->set('title_for_layout',"Manage Subscribers");
    $this->paginate = array(
                       'conditions'=>'',
                        'limit' => 30,
                        'order' => array(
                        'Company.id' => 'DESC'
                                        ));
    $this->set('listing',$this->paginate('Company'));   
}
/**************************************************************************
   *Function Name		 :	changeStatus
   *Functionality		 :	use to change company status.
  ***************************************************************************/  
   private function changeStatus($id,$state) {
    
    		if(empty($id) || !$subs =$this->Company->find('first',array('conditions'=>array('Company.id'=>$id))))	{
    			$this->Session->setFlash('Invalid Company');	
    			$this->redirect('subscriberslist');
    		}	
        	
        if($state=='active') {
        	$this->data['Company']['status']=1;
        } else if($state=='deactive') {
        $this->data['Company']['status']=0;
        } else {
              if($subs['Company']['status']==0) {
          			  $this->data['Company']['status']=1;
          		}	else {
          			  $this->data['Company']['status']=0;
           		}		
        }
      $this->data['Company']['id'] = $id;
      //pr($this->data);die;
    	if($this->Company->save($this->data,$validate = false)) {
    		   return true;
    		} else {
           return false;
        }
	} 	
	 /**************************************************************************
        *Function Name : admin_activate
        *Functionality : use to change single member status.
    **************************************************************************/  
  
  function admin_activate($id,$state=null) {			 	
		if($this->changeStatus($id,$state)) {
		   $data =$this->Company->findById($id);
		      if($data['Company']['status']==1) {
		           $this->Session->setFlash('Status has been activate.','success');
	         	 } else {
			         $this->Session->setFlash('Status has been deactive.','success');
		        }      
     }
		$this->redirect(array('action'=>'subscriberslist'));
	}	
	
	

/**************************************************************************
 * Used for the edition of subscriber or company owner.
 * @param  int $id subscribers id whose data is being edited.
 * @return n/a
 * @access public
**************************************************************************/
 
function admin_editsubscriber($id) {
  
   $this->layout='admin';
   $this->set('title_for_layout',"Edit Subscribers");
        
   if(isset($this->data) && !empty($this->data)) {
      	 $this->loadModel('Owner');
         $this->User->set($this->data['User']);
		   	 	   	 
		   	 $this->Company->set($this->data['Company']);
		   	            	 
		   	 if($this->Company->validates() || $this->Owner->validates()) {
		   	 $this->Company->begin();
          if($this->Company->save($this->data['Company'])) {
		if($this->Owner->save($this->data['Owner'])){
		   	$this->Company->commit();
			$this->Session->setFlash('Member updated successfully','success' );
			$this->redirect(array('action'=>'subscriberslist')); 
         }               	
         } else {
 	            $this->Session->setFlash('Member can not be updated.','message');              
         }
       }			
  	   	 
    } else {
         $users=$this->Company->find("first",array("conditions"=>array("User.id"=>$id)));
         $this->data = $users;
    }
}

/**************************************************************************
 * Used for the adding/edit/view of subscriber duration and charges company owner.
 * @param  n/a
 * @return n/a
 * @access public
**************************************************************************/
  function admin_durationscharge(){
    $this->layout='admin';
    $this->loadModel('SubscriptionDuration');
    $subscription=$this->SubscriptionDuration->find('all');
    $this->set('subscription',$subscription);
  }
	
  function admin_add_subscription()
  {
     $this->layout='admin';
     $this->loadModel('SubscriptionDuration');
     if(!empty($this->data))
     {
          $this->SubscriptionDuration->set($this->data);
              if($this->SubscriptionDuration->validates()){
                 if($this->SubscriptionDuration->save($this->data)) {
		   	    	$this->Session->setFlash('Subscription Charges Added successfully','success' );
				$this->redirect(array('action'=>'durationscharge')); 
              }               	
         }
     }
  }
  
  function admin_edit_subscription($id=null)
  {
	   $this->layout='admin';
	   $this->loadModel('SubscriptionDuration');
     if(isset($this->data) && !empty($this->data)){
         $this->SubscriptionDuration->set($this->data);
		if($this->SubscriptionDuration->validates()){

		   	  //  $this->SubscriptionDuration->id=$this->data['SubscriptionDuration']['id'];
		   	if($this->SubscriptionDuration->save($this->data)){
		   	    $this->Session->setFlash('Subscription updated successfully','success' );
			    $this->redirect(array('action'=>'durationscharge')); 
              }               	
         } else {
 	            $this->Session->setFlash('Member can not be updated.','message');              
         }
      } else {
         $users=$this->SubscriptionDuration->find("first",array("conditions"=>array("SubscriptionDuration.id"=>$id)));
         $this->data = $users;
    }
  }
  
/**************************************************************************
        *Function Name : subscription charge_delete
        *Functionality : use to delete single Member.
**************************************************************************/  
	
	function admin_subscriptionchargedelete($id=null) {
	$this->loadModel('SubscriptionDuration');
		if($this->SubscriptionDuration->delete($id)) {
			$this->Session->setFlash('Subscription Charge deleted successfully','success' );
		} else {
			$this->Session->setFlash('Subscription Charge can not be deleted','message');
		}		
		$this->redirect(array('action'=>'durationscharge'));
	}	 


  function admin_subscribersdelete($id=null) {  	
    
    if($this->Company->delete($id)) {
			$this->Session->setFlash('Subscription Charge deleted successfully','success');
		} else {
			$this->Session->setFlash('Subscription Charge can not be deleted','message');
		}		
	
  }

  
}
?>