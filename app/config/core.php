<?php
/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

# Constants Define Added by Navdeep
  if(!defined('EMAIL_FROM')) {
        //define('EMAIL_FROM', 'info@reportonlineplus.com');
  		define('EMAIL_FROM', 'admin@reportsonlineplus.com');
    }
   
   if(!defined('ADMIN_EMAIL')) {
        define('ADMIN_EMAIL', 'michaelkrusz@gmail.com');
    }
    if (!defined('COMPANYPICSIZE')) {
        define('COMPANYPICSIZE', 315);
    }
    if (!defined('UPLOADATTACHMENT')) {
        define('UPLOADATTACHMENT', '/img/uploaded_attachments/');
    }
    if (!defined('ORGCOMPANYPIC')) {
        define('ORGCOMPANYPIC', '/img/company_logo/');
    }
    if (!defined('COMPANYPICTEMP')) {
        define('COMPANYPICTEMP', '/img/company_logo_temp/');
    }
    if (!defined('SNAPSHOTTEMP')) {
        define('SNAPSHOTTEMP', '/img/snapshots/');
    }
    //creditcard:- 4716914706534228
      //ccv:-192
    /*if (!defined('API_USERNAME')) {
        define('API_USERNAME', 'varunk_1324451334_biz_api1.smartdatainc.net');
    }
    if (!defined('API_PASSWORD')) {
        define('API_PASSWORD', '1324451359');
    }
    if (!defined('API_SIGNATURE')) {
        define('API_SIGNATURE', 'AFHxbQAu711qg9CLqSqWpuFP8r8SAt2nU0jl-GWUgLBYdlUWsYq7nItn');
    }*/
    
# Constants Define (END)
/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 * 	0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 * 	1: Errors and warnings shown, model caches refreshed, flash messages halted.
 * 	2: As in 1, but also with full debug messages and SQL output.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */
	Configure::write('debug', 1);

/**
 * CakePHP Log Level:
 *
 * In case of Production Mode CakePHP gives you the possibility to continue logging errors.
 *
 * The following parameters can be used:
 *  Boolean: Set true/false to activate/deactivate logging
 *    Configure::write('log', true);
 *
 *  Integer: Use built-in PHP constants to set the error level (see error_reporting)
 *    Configure::write('log', E_ERROR | E_WARNING);
 *    Configure::write('log', E_ALL ^ E_NOTICE);
 */
	Configure::write('log', true);

/**
 * Application wide charset encoding
 */
	Configure::write('App.encoding', 'UTF-8');

/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below:
 */
	//Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * Uncomment the define below to use CakePHP prefix routes.
 *
 * The value of the define determines the names of the routes
 * and their associated controller actions:
 *
 * Set to an array of prefixes you want to use in your application. Use for
 * admin or other prefixed routes.
 *
 * 	Routing.prefixes = array('admin', 'manager');
 *
 * Enables:
 *	`admin_index()` and `/admin/controller/index`
 *	`manager_index()` and `/manager/controller/index`
 *
 * [Note Routing.admin is deprecated in 1.3.  Use Routing.prefixes instead]
 */
	Configure::write('Routing.prefixes', array('admin','sp','inspector','client'));

/**
 * Turn off all caching application-wide.
 *
 */
	//Configure::write('Cache.disable', true);

/**
 * Enable cache checking.
 *
 * If set to true, for view caching you must still use the controller
 * var $cacheAction inside your controllers to define caching settings.
 * You can either set it controller-wide by setting var $cacheAction = true,
 * or in each action using $this->cacheAction = true.
 *
 */
	//Configure::write('Cache.check', true);

/**
 * Defines the default error type when using the log() function. Used for
 * differentiating error logging and debugging. Currently PHP supports LOG_DEBUG.
 */
	define('LOG_ERROR', 2);

/**
 * The preferred session handling method. Valid values:
 *
 * 'php'	 		Uses settings defined in your php.ini.
 * 'cake'		Saves session files in CakePHP's /tmp directory.
 * 'database'	Uses CakePHP's database sessions.
 *
 * To define a custom session handler, save it at /app/config/<name>.php.
 * Set the value of 'Session.save' to <name> to utilize it in CakePHP.
 *
 * To use database sessions, run the app/config/schema/sessions.php schema using
 * the cake shell command: cake schema run create Sessions
 *
 */
	Configure::write('Session.save', 'php');

/**
 * The model name to be used for the session model.
 *
 * 'Session.save' must be set to 'database' in order to utilize this constant.
 *
 * The model name set here should *not* be used elsewhere in your application.
 */
	//Configure::write('Session.model', 'Session');

/**
 * The name of the table used to store CakePHP database sessions.
 *
 * 'Session.save' must be set to 'database' in order to utilize this constant.
 *
 * The table name set here should *not* include any table prefix defined elsewhere.
 *
 * Please note that if you set a value for Session.model (above), any value set for
 * Session.table will be ignored.
 *
 * [Note: Session.table is deprecated as of CakePHP 1.3]
 */
	//Configure::write('Session.table', 'cake_sessions');

/**
 * The DATABASE_CONFIG::$var to use for database session handling.
 *
 * 'Session.save' must be set to 'database' in order to utilize this constant.
 */
	//Configure::write('Session.database', 'default');

/**
 * The name of CakePHP's session cookie.
 *
 * Note the guidelines for Session names states: "The session name references
 * the session id in cookies and URLs. It should contain only alphanumeric
 * characters."
 * @link http://php.net/session_name
 */
	Configure::write('Session.cookie', 'CAKEPHP');

/**
 * Session time out time (in minutes).
 * Actual value depends on 'Security.level' setting.
 */
	Configure::write('Session.timeout', '600');

/**
 * If set to false, sessions are not automatically started.
 */
	Configure::write('Session.start', true);

/**
 * When set to false, HTTP_USER_AGENT will not be checked
 * in the session
 */
	Configure::write('Session.checkAgent', true);

/**
 * The level of CakePHP security. The session timeout time defined
 * in 'Session.timeout' is multiplied according to the settings here.
 * Valid values:
 *
 * 'high'   Session timeout in 'Session.timeout' x 10
 * 'medium' Session timeout in 'Session.timeout' x 100
 * 'low'    Session timeout in 'Session.timeout' x 300
 *
 * CakePHP session IDs are also regenerated between requests if
 * 'Security.level' is set to 'high'.
 */
	Configure::write('Security.level', 'medium');

/**
 * A random string used in security hashing methods.
 */
	Configure::write('Security.salt', 'SmartDYhG93b0qyJfIxfs2guVoUubWwvniR2G0FgaC9mi');

/**
 * A random numeric string (digits only) used to encrypt/decrypt strings.
 */
	Configure::write('Security.cipherSeed', '201076859309657453542496749683645');

/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a querystring parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps, when debug = 0, or set to 'force' to always enable
 * timestamping.
 */
	//Configure::write('Asset.timestamp', true);
/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
	//Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JavaScriptHelper::link().
 */
	//Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The classname and database used in CakePHP's
 * access control lists.
 */
	Configure::write('Acl.classname', 'DbAcl');
	Configure::write('Acl.database', 'default');

/**
 * If you are on PHP 5.3 uncomment this line and correct your server timezone
 * to fix the date & time related errors.
 */
	//date_default_timezone_set('UTC');

/**
 *
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'File', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 * 		'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
 * 		'prefix' => 'cake_', //[optional]  prefix every cache file with this string
 * 		'lock' => false, //[optional]  use file locking
 * 		'serialize' => true, [optional]
 *	));
 *
 *
 * APC (http://pecl.php.net/package/APC)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Apc', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Xcache', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 *		'user' => 'user', //user from xcache.admin.user settings
 *      'password' => 'password', //plaintext password (xcache.admin.pass)
 *	));
 *
 *
 * Memcache (http://www.danga.com/memcached/)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Memcache', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 * 		'servers' => array(
 * 			'127.0.0.1:11211' // localhost, default port 11211
 * 		), //[optional]
 * 		'compress' => false, // [optional] compress data in Memcache (slower, but uses less memory)
 *	));
 *
 */
	Cache::config('default', array('engine' => 'File'));
	# $businessType and $ReportType for directly approached client
	$businessType = array("1"=>"Healthcare/Nursing Facility","2"=>"School/Campus Environment","3"=>"Industrial/Commercial Site","4"=>"Utility or Service Company","5"=>"Restaurant/Dining","6"=>"Lodging","7"=>"Management Company","8"=>"Office/Retail Building","9"=>"Retail Store");
	Configure::write('businessType',$businessType);
      
        $reportType = array("1"=>"Fire Alarm","2"=>"Sprinkler","3"=>"Special Hazard","4"=>"Kitchen System","5"=>"Security & Intrusion Detection","6"=>"Extinguishers","7"=>"Emergency Lighting/Exit Signs","8"=>"Office/Retail Building","9"=>"Other (please list details below):");
	Configure::write('reportType',$reportType);
	//type  or the time of inspection like this
	$frequency = array("1"=>"Annual","2"=>"Semi-Annual","3"=>"Quarterly","4"=>"Monthly");
	Configure::write('frequency',$frequency);
        $specialHazardarr = array(1=>'NFPA 11',2=>'NFPA 11A',3=>'NFPA 12',4=>'NFPA 12A',5=>'NFPA 12B',6=>'NFPA 16',7=>'NFPA 16A',8=>'NFPA 17',9=>'NFPA 17A',10=>'NFPA 750',11=>'NFPA 2001',12=>'Other (Specify):');
	Configure::write('specialHazardarr',$specialHazardarr);
        $arr1=array(1=>'Hardwired',2=>'Multiplex',3=>'Addressable',4=>'Other (Specify):');
	Configure::write('controlUnit',$arr1);
	$firealarmarr = array(1=>'NFPA 10',2=>'NFPA 11',3=>'NFPA 11A',4=>'NFPA 12',5=>'NFPA 12A',6=>'NFPA 12B',7=>'NFPA 16',8=>'NFPA 16A',9=>'NFPA 17',10=>'NFPA 17A',11=>'NFPA 13',12=>'NFPA 14',13=>'NFPA 15',14=>'NFPA 20',15=>'NFPA 22',16=>'NFPA 13',17=>'NFPA 24',18=>'NFPA 70',19=>'NFPA 71',20=>'NFPA 72');
	Configure::write('firealarmarr',$firealarmarr);
